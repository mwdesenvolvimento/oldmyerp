<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Caixa</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
 
	</head>
	<body>

<?	
		ob_start();
		session_start();
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		if($_POST['sel_order'] == ''){ $_POST['sel_order'] = "tblfinanceiro_conta_fluxo_marcador"; }
		
		$rsDados = mysql_query("select * from tblempresa_info");
		$rowDados = mysql_fetch_array($rsDados);
		
		$CPF_CNPJDados = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		
		$rsUsuario = mysql_query("select * from tblusuario where fldId=".$_SESSION['usuario_id']);
		$rowUsuario = mysql_fetch_array($rsUsuario);
		//seleciona o tipo de descrição, se é por conta ou marcador
		if($_POST['sel_order'] == 'tblfinanceiro_conta'){
			$cabecalho = 'Marcador';
		}else{
			$cabecalho = 'Conta';
		}
		
		//pega o total para fazer verificação de limite 
		$rsFluxoTotal 	= mysql_query($_SESSION['relatorio_caixa_sql']);
		$totalRegistros = mysql_num_rows($rsFluxoTotal);
		$limite = 26;
		
		($_POST['sel_order'] != 'tblfinanceiro_conta_fluxo_marcador_resumido') ? $pgTotal = $totalRegistros / $limite : $pgTotal = 1;
		$p = 1;
		
		$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
								<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
								<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
							</tr>";			
		$relatorioCabecalho	= "<tr>
										<td>
											<table style='width: 930px' class='table_relatorio_dados' summary='Relat&oacute;rio'>
												<tr>
													<td style='width: 320px;'>Raz&atilde;o Social: ".$rowDados['fldNome']."</td>
													<td style='width: 200px;'>Nome Fantasia: ".$rowDados['fldNome_Fantasia']."</td>
													<td style='width: 320px;'>CPF/CNPJ: ".$CPF_CNPJDados."</td>
													<td style='width: 200px;'>Telefone: ".$rowDados['fldTelefone1']."</td>
												</tr>
											</table>	
										</td>
										<td>        
											<table class='dados_impressao'>
												<tr>
													<td><b>Data: </b><span>".format_date_out(date('Y-m-d'))."</span></td>
													<td><b>Hora: </b><span>".format_time_short(date('H:i:s'))."</span></td>
													<td><b>Usu&aacute;rio: </b><span>".$rowUsuario['fldUsuario']."</span></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr class='total'>
										<td style='width:910px'>&nbsp;</td>
										<td>Movimento ".$_SESSION['txt_conta_data1']." a ".$_SESSION['txt_conta_data2']."</td>
										<td style='width: 20px'>&nbsp;</td>
									</tr>";
									
		if ($_POST['sel_order'] != 'tblfinanceiro_conta_fluxo_marcador_resumido' && $_POST['sel_order'] != 'tblfinanceiro_conta_fluxo'){
		$tableCabecalho = " <tr>
								<td>
									<table class='table_relatorio_paisagem' summary='Relat&oacute;rio'>
										<tr class='total' style='border:none'>
											<td style='width:60px; margin-left: 5px'>C&oacute;d.</td>
											<td style='width:340px'>Descri&ccedil;&atilde;o</td>
											<td style='width:340px'>".$cabecalho."</td>
											<td style='width:80px'>Data</td>
											<td style='width:40px'>Hora</td>
											<td style='width:80px; text-align:right'>Entradas</td>
											<td style='width:80px; text-align:right'>Sa&iacute;das</td>
											<td style='width:80px; text-align:right'>Saldo</td>
										</tr>
									</table>
								</td>
							</tr>";
		}else if ($_POST['sel_order'] == 'tblfinanceiro_conta_fluxo'){
		$tableCabecalho = " <tr>
								<td>
									<table class='table_relatorio_paisagem' summary='Relat&oacute;rio'>
										<tr class='total' style='border:none'>
											<td style='width:785px; margin-left: 5px'>Descrição</td>
											<td style='width:80px; margin-left: 5px'>Data</td>
											<td style='width:80px; text-align:right'>Entradas</td>
											<td style='width:80px; text-align:right'>Sa&iacute;das</td>
											<td style='width:80px; text-align:right'>Saldo</td>
										</tr>
									</table>
								</td>
							</tr>";
		}else{
		$tableCabecalho = " <tr>
								<td>
									<table class='table_relatorio_paisagem' summary='Relat&oacute;rio'>
										<tr class='total' style='border:none'>
											<td style='width:813px; margin-left:5px'>Descri&ccedil;&atilde;o</td>
											<td style='width:20px;'>Entradas</td>
											<td style='width:45px; text-align:center;'>-</td>
											<td style='width:20px;'>Sa&iacute;das</td>
											<td style='width:30px; text-align:center;'>=</td>
											<td style='width:104px;'>Sub total</td>
											<td style='width:20px;'>Saldo</td>
										</tr>
									</table>
								</td>
							</tr>";}
?>
        <div id="no-print">
            <form id="frm-filtro" action="" method="post" style="width:530px; ">
                <fieldset>
                    <legend style="display:none"></legend>
                    <ul style="display:inline">
                        <li>
                            <label for="sel_order">Ordenar por</label>
                            <select id="sel_order" name="sel_order">
                                <option <?=($_POST['sel_order'] === 'tblfinanceiro_conta_fluxo_marcador') ? print "selected='selected'" : '' ?> value="tblfinanceiro_conta_fluxo_marcador">Marcador</option>
								<option <?=($_POST['sel_order'] === 'tblfinanceiro_conta_fluxo_marcador_resumido') ? print "selected='selected'" : '' ?> value="tblfinanceiro_conta_fluxo_marcador_resumido">Marcador (resumido)</option>
                                <option <?=($_POST['sel_order'] === 'tblfinanceiro_conta') ? print "selected='selected'" : '' ?> value="tblfinanceiro_conta">Conta</option>
                                <option <?=($_POST['sel_order'] === 'tblfinanceiro_conta_fluxo') ? print "selected='selected'" : '' ?> value="tblfinanceiro_conta_fluxo">Dias (agrupado)</option>
                            </select>
                        </li>
                        <li>
                            <button style="margin-top: 0;" type="submit" name="btn_exibir" title="Exibir">Exibir</button>
                        </li>
                        <li>
                            <a class="print" href="#" onClick="window.print()">imprimir</a>
                        </li>
                    </ul>
                </fieldset>
            </form>
        </div>
        
        
        <table class='relatorio_print_paisagem' style="page-break-before:avoid">
<?      print $contadorCabecalho.$relatorioCabecalho;
        print $tableCabecalho; 
		///####################################################################################################################################################################///
		if($_POST['sel_order'] == 'tblfinanceiro_conta_fluxo_marcador' || $_POST['sel_order'] == 'tblfinanceiro_conta_fluxo_marcador_resumido'){
			
			if ($_POST['sel_order'] == 'tblfinanceiro_conta_fluxo_marcador'){ #verifica se é marcador normal ou resumido
			
				//primeiro lista as parcelas recebidas
				$rsFluxoRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '3' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsFluxoRegistro)){
					$n = 2; //n vai verificar limite por página
	?>							
					<tr class="h2">
						<td style="width:10px">&nbsp;</td>
						<td>Recebimento de Parcelas</td>
					</tr>
					
					<tr>
						<td>
							<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?							while($rowFluxoRegistro = mysql_fetch_array($rsFluxoRegistro)){
									$x ++; //n vai verificar limite total de registros
									$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxoRegistro['fldConta_Id']);
									$rowConta = mysql_fetch_array($rsConta);
									
									$rsParcela_Baixa = mysql_query("SELECT * FROM tblpedido_parcela_baixa WHERE fldId = ".$rowFluxoRegistro['fldReferencia_Id']);
									$rowParcela_Baixa = mysql_fetch_array($rsParcela_Baixa);
									echo mysql_error();
									
									$rsCliente = mysql_query("SELECT tblpedido.*, tblpedido_parcela.*, tblcliente.fldNome as fldClienteNome 
															FROM tblcliente INNER JOIN 
															(tblpedido LEFT JOIN tblpedido_parcela on tblpedido_parcela.fldPedido_Id = tblpedido.fldId)
															ON tblcliente.fldId = tblpedido.fldCliente_Id
															WHERE tblpedido_parcela.fldId =".$rowParcela_Baixa['fldParcela_Id']);
									echo mysql_error();
									$rowCliente = mysql_fetch_array($rsCliente);
									echo mysql_error();
									
									$Entidade = $rowCliente['fldClienteNome'];
	
									//soma total para rodape do marcador ###############//
									$sdPedidoCredito += $rowFluxoRegistro['fldCredito'];
									$sdPedidoDebito += $rowFluxoRegistro['fldDebito'];
									$SaldoFluxo = $sdPedidoCredito - $sdPedidoDebito;
									
									//soma total para rodape ########################//
									$sdTotalCredito += $rowFluxoRegistro['fldCredito'];
									$sdTotalDebito += $rowFluxoRegistro['fldDebito'];
									$SaldoTotalFluxo = $sdTotalCredito - $sdTotalDebito;
									//##############################################//
	?>
									<tr>
										<td style="width:60px; margin-left: 5px;"><?=$rowFluxoRegistro['fldId']?></td>
										<td style="width:345px">Recebimento de parcela (<?=substr($Entidade,0,35)?>)</td>
										<td style="width:340px"><?=$rowConta['fldNome']?></td>
										<td style="width:80px"><?=format_date_out($rowFluxoRegistro['fldData'])?></td>
										<td style="width:40px"><?=format_time_short($rowFluxoRegistro['fldHora'])?></td>
										<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldCredito'])?></td>
										<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldDebito'])?></td>
										<td style="width:80px; text-align:right"><?=format_number_out($SaldoFluxo)?></td>
									</tr>											
	<?											
									//verifica limite	
									if(($n == $limite) or ($x == $totalRegistros)){
	?>												</table>    
												</td>
											</tr>
										</table>
	<?        							$n = 1;
										if($p < $pgTotal){
										$p += 1;
										}
										if($x < $totalRegistros){
											$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
											<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
											<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
											</tr>";
											print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
											print $tableCabecalho;
	?>                                      <tr>
												<td>
													<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?									}
									}else{
										$n ++;
									}
								}// end while
				}// if num_rows
				//pagamentos de parcela
				$rsFluxoRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '4' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsFluxoRegistro)){
				//verifica limite de novo por causa do h2
					if(($n == $limite) or ($x == $totalRegistros)){
						$n = 1;
	?>								</table>    
								</td>
							</tr>
						</table>
						<?
						$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
						<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
						<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
						</tr>";
						print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
						?>
						<tr class="h2">
							<td style="width:10px">&nbsp;</td>
							<td>Pagamento de Parcelas</td>
						</tr>
						<? print $tableCabecalho;?>
						<tr>
							<td>
								<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?				}else{
						$n ++;
	?>					<tr class="h2">
							<td style="width:10px">&nbsp;</td>
							<td>Pagamento de Parcelas</td>
						</tr>														
	<?					if($n == 1){ //se o relatorio comeca aqui, tem que abrir a table
	?>						<tr>
								<td>
									<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?	
						}														
					}
								
					while($rowFluxoRegistro = mysql_fetch_array($rsFluxoRegistro)){
						$x ++;
						
						$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxoRegistro['fldConta_Id']);
						$rowConta = mysql_fetch_array($rsConta);
						echo mysql_error();
						
						$rsParcela_Baixa = mysql_query("SELECT * FROM tblcompra_parcela_baixa WHERE fldId = ".$rowFluxoRegistro['fldReferencia_Id']);
						$rowParcela_Baixa = mysql_fetch_array($rsParcela_Baixa);
						echo mysql_error();
						
						$rsFornecedor = mysql_query("SELECT tblcompra.*, tblcompra_parcela.*, tblfornecedor.fldNomeFantasia as fldFornecedorNome 
												FROM tblfornecedor INNER JOIN 
												(tblcompra LEFT JOIN tblcompra_parcela on tblcompra_parcela.fldCompra_Id = tblcompra.fldId)
												ON tblfornecedor.fldId = tblcompra.fldFornecedor_Id
												WHERE tblcompra_parcela.fldId =".$rowParcela_Baixa['fldParcela_Id']);
						echo mysql_error();
						$rowFornecedor = mysql_fetch_array($rsFornecedor);
						echo mysql_error();
						
						$Entidade = $rowFornecedor['fldFornecedorNome'];
						
						//soma total para rodape do marcador ###############//
						$sdCompraCredito += $rowFluxoRegistro['fldCredito'];
						$sdCompraDebito += $rowFluxoRegistro['fldDebito'];
						$SaldoFluxo = $sdCompraCredito - $sdCompraDebito;
						
						//soma total para rodape ########################//
						$sdTotalCredito += $rowFluxoRegistro['fldCredito'];
						$sdTotalDebito += $rowFluxoRegistro['fldDebito'];
						$SaldoTotalFluxo = $sdTotalCredito - $sdTotalDebito;
						//##############################################//
			 
	?>					<tr>
							<td style="width:60px; margin-left: 5px;"><?=$rowFluxoRegistro['fldId']?></td>
							<td style="width:345px">Pagamento de parcela (<?=substr($Entidade,0,35)?>)</td>
							<td style="width:340px"><?=$rowConta['fldNome']?></td>
							<td style="width:80px"><?=format_date_out($rowFluxoRegistro['fldData'])?></td>
							<td style="width:40px"><?=format_time_short($rowFluxoRegistro['fldHora'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldCredito'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldDebito'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($SaldoFluxo)?></td>
						</tr>
	<?                 	
						if(($n == $limite) or ($x == $totalRegistros)){
	?>									</table>    
									</td>
								</tr>
							</table>
	<?       				$n = 1;
				if($p < $pgTotal){
						$p += 1;
				}
							if($x < $totalRegistros){
							$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
							<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
							<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
							</tr>";
							print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
							echo $tableCabecalho;
	?>                     	 	<tr>
									<td>
										<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?						}
						}else{
							$n ++;
						}
					}// end while
				}// end IF $rows
							
				//pagamentos de funcionario
				$rsFluxoRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '7' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsFluxoRegistro)){//verifica limite de novo por causa do h2
					if(($n == $limite) or ($x == $totalRegistros)){
						$n = 1;
	?>					</table>    
								</td>
							</tr>
						</table>
						<?
						$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
						<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
						<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
						</tr>";
						print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
						?>
						<tr class="h2">
							<td style="width:20x">&nbsp;</td>
							<td>Pagamento de Comiss&otilde;es</td>
						</tr>
						<? print $tableCabecalho;?>
						<tr>
							<td>
								<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?				}else{
						$n ++;
	?>					<tr class="h2">
							<td style="width:10px">&nbsp;</td>
							<td>Pagamento de Comiss&otilde;es</td>
						</tr>																
	<?					if($n == 1){ //se o relatorio comeca aqui, tem que abrir a table
	?>						<tr>
								<td>
									<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?					}												
					}
					while($rowFluxoRegistro = mysql_fetch_array($rsFluxoRegistro)){
						$x ++;
						
						$rsFuncionario = mysql_query("SELECT tblfuncionario_conta_fluxo.fldId,
												  tblfuncionario.fldNome
												  FROM tblfuncionario INNER JOIN tblfuncionario_conta_fluxo ON tblfuncionario.fldId = tblfuncionario_conta_fluxo.fldFuncionario_Id
												  WHERE tblfuncionario_conta_fluxo.fldId = ".$rowFluxoRegistro['fldReferencia_Id']);
						$rowFuncionario = mysql_fetch_array($rsFuncionario);
						echo mysql_error();
						$Entidade = $rowFuncionario['fldNome'];
						
						$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxoRegistro['fldConta_Id']);
						$rowConta = mysql_fetch_array($rsConta);
						echo mysql_error();
						
						//soma total para rodape do marcador ###############//
						$sdComissaoCredito += $rowFluxoRegistro['fldCredito'];
						$sdComissaoDebito += $rowFluxoRegistro['fldDebito'];
						$SaldoFluxo = $sdComissaoCredito - $sdComissaoDebito;
						
						//soma total para rodape ########################//
						$sdTotalCredito += $rowFluxoRegistro['fldCredito'];
						$sdTotalDebito += $rowFluxoRegistro['fldDebito'];
						$SaldoTotalFluxo = $sdTotalCredito - $sdTotalDebito;
						//##############################################//
										
	?>					<tr>
							<td style="width:60px; margin-left: 5px;"><?=$rowFluxoRegistro['fldId']?></td>
							<td style="width:345px">Comiss&atilde;o de funcionario (<?=substr($Entidade,0,35)?>)</td>
							<td style="width:340px"><?=$rowConta['fldNome']?></td>
							<td style="width:80px"><?=format_date_out($rowFluxoRegistro['fldData'])?></td>
							<td style="width:40px"><?=format_time_short($rowFluxoRegistro['fldHora'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldCredito'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldDebito'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($SaldoFluxo)?></td>
						</tr>
	<?                 	
						if(($n == $limite) or ($x == $totalRegistros)){
	?>									</table>    
									</td>
								</tr>
							</table>
	<?						$n = 1;
					if($p < $pgTotal){
						$p += 1;
				}
							if($x < $totalRegistros){
								$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
								<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
								<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
								</tr>";
								print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
								print $tableCabecalho;
	?>                  			<tr>
										<td>
											<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?						}	
						}else{
							$n ++;
						}
					}// end while
				}// end IF $rows;
		
				//estornos
				$rsFluxoRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '5' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsFluxoRegistro)){
					//verifica limite de novo por causa do h2
					
					if(($n == $limite) or ($x == $totalRegistros)){
						$n = 1;
	?>								</table>    
								</td>
							</tr>
						</table>
						<?
						$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
						<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
						<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
						</tr>";
						print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
						?>
						<tr class="h2">
							<td style="width:10px">&nbsp;</td>
							<td>Estornos</td>
						</tr>
						<? print $tableCabecalho;?>
						<tr>
							<td>
								<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?				}else{
						$n ++;
	?>					<tr class="h2">
							<td style="width:10px">&nbsp;</td>
							<td>Estornos</td>
						</tr>														
	<?					if($n == 1){ //se o relatorio comeca aqui, tem que abrir a table
	?>						<tr>
								<td>
									<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?	
						}
					}
					while($rowFluxoRegistro = mysql_fetch_array($rsFluxoRegistro)){
						$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxoRegistro['fldConta_Id']);
						$rowConta = mysql_fetch_array($rsConta);
						$x ++;
						
						//soma total para rodape do marcador ###############//
						$sdCredito += $rowFluxoRegistro['fldCredito'];
						$sdDebito += $rowFluxoRegistro['fldDebito'];
						$SaldoFluxo = $sdCredito - $sdDebito;
						
						//soma total para rodape ########################//
						$sdTotalCredito += $rowFluxoRegistro['fldCredito'];
						$sdTotalDebito += $rowFluxoRegistro['fldDebito'];
						$SaldoTotalFluxo = $sdTotalCredito - $sdTotalDebito;
						//##############################################//
				
	?>					<tr>
							<td style="width:60px; margin-left: 5px;"><?=$rowFluxoRegistro['fldId']?></td>
							<td style="width:345px"><?=$rowFluxoRegistro['fldDescricao']?></td>
							<td style="width:340px"><?=$rowConta['fldNome']?></td>
							<td style="width:80px"><?=format_date_out($rowFluxoRegistro['fldData'])?></td>
							<td style="width:40px"><?=format_time_short($rowFluxoRegistro['fldHora'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldCredito'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldDebito'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($SaldoFluxo)?></td>
						</tr>
	<?					if(($n == $limite) or ($x == $totalRegistros)){
							$n = 1;
				if($p < $pgTotal){
						$p += 1;
				}
	?>									</table>    
									</td>
							   </tr>
							</table>
	<?						
							if($x < $totalRegistros){
								$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
								<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
								<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
								</tr>";
								print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
								print $tableCabecalho;
	?>                      	<tr>
									<td>
										<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">	
		<?					}
						}else{
							$n ++;
						}
					}// end while
				}// end IF $rows
				
				$sdCredito = 0;
				$sdDebito = 0;
				$SaldoFluxo = 0;
				
				//transferencias
				$rsFluxoRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '2' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsFluxoRegistro)){
					//verifica limite	 de novo por causa do h2
					if(($n == $limite) or ($x == $totalRegistros)){
						$n = 1;
	?>							</table>    
							   </td>
							</tr>
						</table>
						<?
						$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
						<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
						<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
						</tr>";
						print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
						?>
						<tr class="h2">
							<td style="width:10px">&nbsp;</td>
							<td>Transfer&ecirc;ncias</td>
						</tr>
						<? print $tableCabecalho;?>
						<tr>
							<td>
								<table  class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?				}else{
						$n ++;
	?>					<tr class="h2">
							<td style="width:10px">&nbsp;</td>
							<td>Transfer&ecirc;ncias</td>
						</tr>															
	<?					if($n == 1){ //se o relatorio comeca aqui, tem que abrir a table
	?>						<tr>
								<td>
									<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?	
						}													
					}
					while($rowFluxoRegistro = mysql_fetch_array($rsFluxoRegistro)){
						$x ++;
						
						$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxoRegistro['fldConta_Id']);
						$rowConta = mysql_fetch_array($rsConta);
						
						$rsContaOrigem = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxoRegistro['fldConta_Origem']);
						$rowContaOrigem = mysql_fetch_array($rsContaOrigem);
						echo mysql_error();
						
						$rsContaDestino = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxoRegistro['fldConta_Destino']);
						$rsContaDestino = mysql_fetch_array($rsContaDestino);
						echo mysql_error();
					
						if($rowFluxoRegistro['fldConta_Origem'] != 0){	
							$descricao = "Recebimento de conta ".$rowContaOrigem['fldNome'];
						}else{
							$descricao = "Pagamento para conta ".$rsContaDestino['fldNome'];					
						}
						
						//soma total para rodape do marcador ###############//
						$sdCredito += $rowFluxoRegistro['fldCredito'];
						$sdDebito += $rowFluxoRegistro['fldDebito'];
						$SaldoFluxo = $sdCredito - $sdDebito;
						
						//soma total para rodape ########################//
						$sdTotalCredito += $rowFluxoRegistro['fldCredito'];
						$sdTotalDebito += $rowFluxoRegistro['fldDebito'];
						$SaldoTotalFluxo = $sdTotalCredito - $sdTotalDebito;
						//##############################################//
					 
	?>					<tr>
							<td style="width:60px; margin-left: 5px;"><?=$rowFluxoRegistro['fldId']?></td>
							<td style="width:345px"><?=$descricao?></td>
							<td style="width:340px"><?=$rowConta['fldNome']?></td>
							<td style="width:80px"><?=format_date_out($rowFluxoRegistro['fldData'])?></td>
							<td style="width:40px"><?=format_time_short($rowFluxoRegistro['fldHora'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldCredito'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldDebito'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($SaldoFluxo)?></td>
						</tr>
	<?                   
						if(($n == $limite) or ($x == $totalRegistros)){
							$n = 1;
											if($p < $pgTotal){
						$p += 1;
				}
	?>									</table>    
									</td>
								</tr>
							</table>
	<?						if($x < $totalRegistros){
								$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
								<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
								<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
								</tr>";
								print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
								echo $tableCabecalho;
	?>                     		<tr>
									<td>
										<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?						}
						}else{
							$n ++;
						}
					}// end while
					
				}// end IF $rows
				
				//pagamentos de conta programada
				$rsFluxoRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '6' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsFluxoRegistro)){
					//verifica limite de novo por causa do h2
					if(($n == $limite) or ($x == $totalRegistros)){
						$n = 1;
	?>								</table>    
								</td>
							</tr>
						</table>
						<?
						$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
						<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
						<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
						</tr>";
						print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
						?>
						<tr class="h2">
							<td style="width:10px">&nbsp;</td>
							<td>Pagamentos de Conta Programada</td>
						</tr>
						<? print $tableCabecalho;?>
						<tr>
							<td>
								<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?				}else{
						$n ++;
	?>					<tr class="h2">
							<td style="width:10px">&nbsp;</td>
							<td>Pagamentos de Conta Programada</td>
						</tr>														
	<?					if($n == 1){ //se o relatorio comeca aqui, tem que abrir a table
	?>						<tr>
								<td>
									<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?					}														
					}
					while($rowFluxoRegistro = mysql_fetch_array($rsFluxoRegistro)){
						$x ++;
						
						$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxoRegistro['fldConta_Id']);
						$rowConta = mysql_fetch_array($rsConta);
						echo mysql_error();
						
						$rsContaProg = mysql_query("SELECT tblfinanceiro_conta_pagar_programada.*, 
												   tblfinanceiro_conta_pagar_programada_baixa.fldId FROM 
												   tblfinanceiro_conta_pagar_programada INNER JOIN tblfinanceiro_conta_pagar_programada_baixa
												   ON tblfinanceiro_conta_pagar_programada_baixa.fldContaProgramada_Id = tblfinanceiro_conta_pagar_programada.fldId
												   WHERE tblfinanceiro_conta_pagar_programada_baixa.fldId =".$rowFluxoRegistro['fldReferencia_Id']);
						$rowContaProg = mysql_fetch_array($rsContaProg);
						
						$sdCompraPCredito += $rowFluxoRegistro['fldCredito'];
						$sdCompraPDebito += $rowFluxoRegistro['fldDebito'];
						$SaldoFluxo = $sdCompraPCredito - $sdCompraPDebito;
						
						//soma total para rodape ########################//
						$sdTotalCredito += $rowFluxoRegistro['fldCredito'];
						$sdTotalDebito += $rowFluxoRegistro['fldDebito'];
						$SaldoTotalFluxo = $sdTotalCredito - $sdTotalDebito;
						//##############################################//
										 
	?>					<tr>
							<td style="width:60px; margin-left: 5px;"><?=$rowFluxoRegistro['fldId']?></td>
							<td style="width:345px"><?=$rowContaProg['fldNome']?></td>
							<td style="width:340px"><?=$rowConta['fldNome']?></td>
							<td style="width:80px"><?=format_date_out($rowFluxoRegistro['fldData'])?></td>
							<td style="width:40px"><?=format_time_short($rowFluxoRegistro['fldHora'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldCredito'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($rowFluxoRegistro['fldDebito'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($SaldoFluxo)?></td>
						</tr>
	<?           	        
						if(($n == $limite) or ($x == $totalRegistros)){
							$n = 1;
											if($p < $pgTotal){
						$p += 1;
				}
	?>									</table>    
									</td>
								</tr>
							</table>
	<?						if($x < $totalRegistros){
								$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
								<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
								<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
								</tr>";
								print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
								echo $tableCabecalho;
	?>							<tr>
									<td>
										<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	<?						}
						}else{
						  $n ++;
						}
					}// end while
				}// end IF $rows
			
			}
			
			else { #marcador resumido
				
				$sdTotalCredito 	= 0;
				$sdTotalDebito 		= 0;
				$SaldoTotalFluxo 	= 0;
				$saldoAtual			= 0;
				
				#FAZ COMO NO MARCADOR NORMAL...
				#É USADO UM substr NA SESSION relatorio caixa PARA REMOVER O "SELECT" DO MESMO
				
				#TRANSFERENCIA DE CONTA (2)
				$rsTransferenciaRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '2' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsTransferenciaRegistro)){
				
					$sqlMarcadorResumido = mysql_query("SELECT SUM(fldCredito) AS fldEntrada, SUM(fldDebito) AS fldSaida, SUM(fldCredito-fldDebito) AS fldSubTotal, ".substr($_SESSION['relatorio_caixa_sql'], 6)."  AND fldMovimento_Tipo = '2'");
					$rowMarcadorResumido = mysql_fetch_assoc($sqlMarcadorResumido);
					
					$sdTotalCredito += $rowMarcadorResumido['fldEntrada'];
					$sdTotalDebito  += $rowMarcadorResumido['fldSaida'];
					$saldoAtual += $rowMarcadorResumido['fldEntrada'];
					$saldoAtual -= $rowMarcadorResumido['fldSaida'];
				
				?>
					<tr class="h2" style="border-bottom: 1px solid #000; padding:0;">
							<td style="font-size:14px; font-weight:normal; width:775px; margin: 0 5px 3px 5px;">Transferência de Conta</td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldEntrada'])?></td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSaida'])?></td>
							<td style="font-size:12px; font-weight:normal; width:82px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSubTotal'])?></td>
							<td style="border-right: 1px dotted #CCC; margin: 0 5px 0px 5px; width:7px;">&nbsp;</td>
							<td style="font-size:12px; font-weight:normal; text-align:right; width:60px; margin: 0 5px 3px 5px;"><?=format_number_out($saldoAtual)?></td>
					</tr>
				<?					
				}
				
				#RECEBIMENTO DE PARCELA (3)
				$rsRecebimentoRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '3' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsRecebimentoRegistro)){
				
					$sqlMarcadorResumido = mysql_query("SELECT SUM(fldCredito) AS fldEntrada, SUM(fldDebito) AS fldSaida, SUM(fldCredito-fldDebito) AS fldSubTotal, ".substr($_SESSION['relatorio_caixa_sql'], 6)."  AND fldMovimento_Tipo = '3'");
					$rowMarcadorResumido = mysql_fetch_assoc($sqlMarcadorResumido);
					
					$sdTotalCredito += $rowMarcadorResumido['fldEntrada'];
					$sdTotalDebito  += $rowMarcadorResumido['fldSaida'];
					$saldoAtual += $rowMarcadorResumido['fldEntrada'];
					$saldoAtual -= $rowMarcadorResumido['fldSaida'];
				
				?>
					<tr class="h2" style="border-bottom: 1px solid #000; padding:0;">
							<td style="font-size:14px; font-weight:normal; width:775px; margin: 0 5px 3px 5px;">Recebimento de Parcela</td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldEntrada'])?></td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSaida'])?></td>
							<td style="font-size:12px; font-weight:normal; width:82px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSubTotal'])?></td>
							<td style="border-right: 1px dotted #CCC; margin: 0 5px 0px 5px; width:7px;">&nbsp;</td>
							<td style="font-size:12px; font-weight:normal; text-align:right; width:60px; margin: 0 5px 3px 5px;"><?=format_number_out($saldoAtual)?></td>
					</tr>
				<?					
				}
				
				#PAGAMENTO DE PARCELA (4)
				$rsPgParcelaRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '4' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsPgParcelaRegistro)){
				
					$sqlMarcadorResumido = mysql_query("SELECT SUM(fldCredito) AS fldEntrada, SUM(fldDebito) AS fldSaida, SUM(fldCredito-fldDebito) AS fldSubTotal, ".substr($_SESSION['relatorio_caixa_sql'], 6)."  AND fldMovimento_Tipo = '4'");
					$rowMarcadorResumido = mysql_fetch_assoc($sqlMarcadorResumido);
					
					$sdTotalCredito += $rowMarcadorResumido['fldEntrada'];
					$sdTotalDebito  += $rowMarcadorResumido['fldSaida'];
					$saldoAtual += $rowMarcadorResumido['fldEntrada'];
					$saldoAtual -= $rowMarcadorResumido['fldSaida'];
				
				?>
					<tr class="h2" style="border-bottom: 1px solid #000; padding:0;">
							<td style="font-size:14px; font-weight:normal; width:775px; margin: 0 5px 3px 5px;">Pagamento de Parcela</td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldEntrada'])?></td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSaida'])?></td>
							<td style="font-size:12px; font-weight:normal; width:82px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSubTotal'])?></td>
							<td style="border-right: 1px dotted #CCC; margin: 0 5px 0px 5px; width:7px;">&nbsp;</td>
							<td style="font-size:12px; font-weight:normal; text-align:right; width:60px; margin: 0 5px 3px 5px;"><?=format_number_out($saldoAtual)?></td>
					</tr>
				<?					
				}
				
				#ESTORNO (5)
				$rsEstornoRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '5' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsEstornoRegistro)){
				
					$sqlMarcadorResumido = mysql_query("SELECT SUM(fldCredito) AS fldEntrada, SUM(fldDebito) AS fldSaida, SUM(fldCredito-fldDebito) AS fldSubTotal, ".substr($_SESSION['relatorio_caixa_sql'], 6)."  AND fldMovimento_Tipo = '5'");
					$rowMarcadorResumido = mysql_fetch_assoc($sqlMarcadorResumido);
					
					$sdTotalCredito += $rowMarcadorResumido['fldEntrada'];
					$sdTotalDebito  += $rowMarcadorResumido['fldSaida'];
					$saldoAtual += $rowMarcadorResumido['fldEntrada'];
					$saldoAtual -= $rowMarcadorResumido['fldSaida'];
				
				?>
					<tr class="h2" style="border-bottom: 1px solid #000; padding:0;">
							<td style="font-size:14px; font-weight:normal; width:775px; margin: 0 5px 3px 5px;">Estorno</td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldEntrada'])?></td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSaida'])?></td>
							<td style="font-size:12px; font-weight:normal; width:82px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSubTotal'])?></td>
							<td style="border-right: 1px dotted #CCC; margin: 0 5px 0px 5px; width:7px;">&nbsp;</td>
							<td style="font-size:12px; font-weight:normal; text-align:right; width:60px; margin: 0 5px 3px 5px;"><?=format_number_out($saldoAtual)?></td>
					</tr>
				<?					
				}
				
				#PAGAMENTO DE CONTA PROGRAMADA (6)
				$rsPgProgramadaRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '6' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsPgProgramadaRegistro)){
				
					$sqlMarcadorResumido = mysql_query("SELECT SUM(fldCredito) AS fldEntrada, SUM(fldDebito) AS fldSaida, SUM(fldCredito-fldDebito) AS fldSubTotal, ".substr($_SESSION['relatorio_caixa_sql'], 6)."  AND fldMovimento_Tipo = '6'");
					$rowMarcadorResumido = mysql_fetch_assoc($sqlMarcadorResumido);
					
					$sdTotalCredito += $rowMarcadorResumido['fldEntrada'];
					$sdTotalDebito  += $rowMarcadorResumido['fldSaida'];
					$saldoAtual += $rowMarcadorResumido['fldEntrada'];
					$saldoAtual -= $rowMarcadorResumido['fldSaida'];
				
				?>
					<tr class="h2" style="border-bottom: 1px solid #000; padding:0;">
							<td style="font-size:14px; font-weight:normal; width:775px; margin: 0 5px 3px 5px;">Pagamento de Conta Programada</td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldEntrada'])?></td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSaida'])?></td>
							<td style="font-size:12px; font-weight:normal; width:82px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSubTotal'])?></td>
							<td style="border-right: 1px dotted #CCC; margin: 0 5px 0px 5px; width:7px;">&nbsp;</td>
							<td style="font-size:12px; font-weight:normal; text-align:right; width:60px; margin: 0 5px 3px 5px;"><?=format_number_out($saldoAtual)?></td>
					</tr>
				<?					
				}
				
				#COMISSAO FUNCIONARIO (7)
				$rsComissaoRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '7' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsComissaoRegistro)){
				
					$sqlMarcadorResumido = mysql_query("SELECT SUM(fldCredito) AS fldEntrada, SUM(fldDebito) AS fldSaida, SUM(fldCredito-fldDebito) AS fldSubTotal, ".substr($_SESSION['relatorio_caixa_sql'], 6)."  AND fldMovimento_Tipo = '7'");
					$rowMarcadorResumido = mysql_fetch_assoc($sqlMarcadorResumido);
					
					$sdTotalCredito += $rowMarcadorResumido['fldEntrada'];
					$sdTotalDebito  += $rowMarcadorResumido['fldSaida'];
					$saldoAtual += $rowMarcadorResumido['fldEntrada'];
					$saldoAtual -= $rowMarcadorResumido['fldSaida'];
				
				?>
					<tr class="h2" style="border-bottom: 1px solid #000; padding:0;">
							<td style="font-size:14px; font-weight:normal; width:775px; margin: 0 5px 3px 5px;">Comissão Funcionário</td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldEntrada'])?></td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSaida'])?></td>
							<td style="font-size:12px; font-weight:normal; width:82px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSubTotal'])?></td>
							<td style="border-right: 1px dotted #CCC; margin: 0 5px 0px 5px; width:7px;">&nbsp;</td>
							<td style="font-size:12px; font-weight:normal; text-align:right; width:60px; margin: 0 5px 3px 5px;"><?=format_number_out($saldoAtual)?></td>
					</tr>
				<?					
				}
				
				#VALE FUNCIONARIO (8)
				$rsValeRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '8' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsValeRegistro)){
				
					$sqlMarcadorResumido = mysql_query("SELECT SUM(fldCredito) AS fldEntrada, SUM(fldDebito) AS fldSaida, SUM(fldCredito-fldDebito) AS fldSubTotal, ".substr($_SESSION['relatorio_caixa_sql'], 6)."  AND fldMovimento_Tipo = '8'");
					$rowMarcadorResumido = mysql_fetch_assoc($sqlMarcadorResumido);
					
					$sdTotalCredito += $rowMarcadorResumido['fldEntrada'];
					$sdTotalDebito  += $rowMarcadorResumido['fldSaida'];
					$saldoAtual += $rowMarcadorResumido['fldEntrada'];
					$saldoAtual -= $rowMarcadorResumido['fldSaida'];
					
				
				?>
					<tr class="h2" style="border-bottom: 1px solid #000; padding:0;">
							<td style="font-size:14px; font-weight:normal; width:775px; margin: 0 5px 3px 5px;">Vale Funcionário</td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldEntrada'])?></td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSaida'])?></td>
							<td style="font-size:12px; font-weight:normal; width:82px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido['fldSubTotal'])?></td>
							<td style="border-right: 1px dotted #CCC; margin: 0 5px 0px 5px; width:7px;">&nbsp;</td>
							<td style="font-size:12px; font-weight:normal; text-align:right; width:60px; margin: 0 5px 3px 5px;"><?=format_number_out($saldoAtual)?></td>
					</tr>
				<?					
				}
				
				#MOVIMENTO DO CAIXA (1)
				$rsFluxoRegistro = mysql_query($_SESSION['relatorio_caixa_sql']." AND fldMovimento_Tipo = '1' ORDER BY vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora");
				if($rows = mysql_num_rows($rsFluxoRegistro)){
					
					$sqlMarcadorResumido = mysql_query($_SESSION['relatorio_caixa_sql']."  AND fldMovimento_Tipo = '1' GROUP BY fldMarcador_Id");
					$replace_session = "FROM vwfinanceiro_conta_fluxo INNER JOIN tblfinanceiro_conta_fluxo_marcador ON vwfinanceiro_conta_fluxo.fldMarcador_Id = tblfinanceiro_conta_fluxo_marcador.fldId";
					$consulta = str_replace("FROM vwfinanceiro_conta_fluxo", $replace_session, $_SESSION['relatorio_caixa_sql']);
					while($rowMarcadorResumido = mysql_fetch_assoc($sqlMarcadorResumido)){
						
					$consMarcadorResumido_dinamico = mysql_query("SELECT
														SUM(fldCredito) AS fldEntrada,
														SUM(fldDebito) AS fldSaida,
														SUM(fldCredito-fldDebito) AS fldSubTotal,
														tblfinanceiro_conta_fluxo_marcador.fldMarcador AS fldTituloMarcador,
														".substr($consulta, 6)."
														AND fldMarcador_Id = tblfinanceiro_conta_fluxo_marcador.fldId
														AND fldMarcador_Id = ".$rowMarcadorResumido['fldMarcador_Id']."
														GROUP BY tblfinanceiro_conta_fluxo_marcador.fldId");

					$rowMarcadorResumido_dinamico = mysql_fetch_assoc($consMarcadorResumido_dinamico);
					
					$sdTotalCredito += $rowMarcadorResumido_dinamico['fldEntrada'];
					$sdTotalDebito  += $rowMarcadorResumido_dinamico['fldSaida'];
					$saldoAtual += $rowMarcadorResumido_dinamico['fldEntrada'];
					$saldoAtual -= $rowMarcadorResumido_dinamico['fldSaida'];
				
				?>
					<tr class="h2" style="border-bottom: 1px solid #000; padding:0;">
							<td style="font-size:14px; font-weight:normal; width:775px; margin: 0 5px 3px 5px;"><?=$rowMarcadorResumido_dinamico['fldTituloMarcador']?></td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido_dinamico['fldEntrada'])?></td>
							<td style="font-size:12px; font-weight:normal; width:80px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido_dinamico['fldSaida'])?></td>
							<td style="font-size:12px; font-weight:normal; width:82px; text-align:right; margin: 0 5px 3px 5px;"><?=format_number_out($rowMarcadorResumido_dinamico['fldSubTotal'])?></td>
							<td style="border-right: 1px dotted #CCC; margin: 0 5px 0px 5px; width:7px;">&nbsp;</td>
							<td style="font-size:12px; font-weight:normal; text-align:right; width:60px; margin: 0 5px 3px 5px;"><?=format_number_out($saldoAtual)?></td>
					</tr>
				<?
					}
				}
				
				$SaldoTotalFluxo =	$sdTotalCredito - $sdTotalDebito;
				
			}
			
		}
			
		elseif($_POST['sel_order'] != 'tblfinanceiro_conta_fluxo_marcador'){
?>			<tr>
				<td>
					<table style="border-top:0" class="table_relatorio_paisagem" summary="Relat&oacute;rio">			
<?		}
	///####################################################################################################################################################################///
	if($_POST['sel_order'] != 'tblfinanceiro_conta_fluxo_marcador_resumido' && $_POST['sel_order'] != ''){ #SE FOR DIFERENTE DE RESUMIDO
		
		$sdCredito	= 0;
		$sdDebito 	= 0;
		$SaldoFluxo = 0;
		
		if($_POST['sel_order'] == 'tblfinanceiro_conta'){
			$movimento = '';
			$tblFiltro = "fldConta_Id";
			$n = 1;
		}else{
			$movimento =  " AND fldMovimento_Tipo = 1";
			$tblFiltro = "fldMarcador_Id";
		}
		
		$order = 0;
		//faz a consulta por conta ou marcador
		$rsFluxo = mysql_query($_SESSION['relatorio_caixa_sql']. $movimento." ORDER BY  $tblFiltro, vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora, fldMovimento_Tipo, fldDescricao");
		while($rowFluxo = mysql_fetch_array($rsFluxo)){
			$x ++;
			if($order != $rowFluxo[$tblFiltro]){
				$order = $rowFluxo[$tblFiltro];
				
				if($_POST['sel_order'] == 'tblfinanceiro_conta'){
					$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId = ".$rowFluxo['fldConta_Id']);
					$rowConta = mysql_fetch_array($rsConta);
					$descricaoH2 = $rowConta['fldNome'];
				}elseif($_POST['sel_order'] == 'tblfinanceiro_conta_fluxo_marcador'){
					$rsMarcador = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_marcador WHERE fldId = ".$rowFluxo['fldMarcador_Id']);
					$rowMarcador = mysql_fetch_array($rsMarcador);
					$descricaoH2 = $rowMarcador['fldMarcador'];
				}
	
				//verifica limite de novo por causa do h2
				if($n == $limite){
					$n = 1;
					
					$sdCredito 	= 0;
					$sdDebito 	= 0;
					$SaldoFluxo = 0;
?>								</table>    
							</td>
						</tr>
                    </table>
                    <?
					$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
					<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
					<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
					</tr>";
					print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
					?>
                    <tr class="h2">
                        <td style="width:10px">&nbsp;</td>
                        <td><?=$descricaoH2?></td>
                    </tr>	
                    <? print $tableCabecalho;?>
                    <tr>
                        <td>
							<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
<?					
				}else{
					
					$sdCredito 	= 0;
					$sdDebito 	= 0;
					$SaldoFluxo = 0;
					$n ++;
?>					<tr class="h2">
                        <td style="width:10px">&nbsp;</td>
                        <td><?=$descricaoH2?></td>
                    </tr>														
<?				}
			}// and if order
				
			$sdCredito += $rowFluxo['fldCredito'];
			$sdDebito += $rowFluxo['fldDebito'];
			$SaldoFluxo = $sdCredito - $sdDebito;
			
			//soma total para rodape ########################//
			$sdTotalCredito += $rowFluxo['fldCredito'];
			$sdTotalDebito += $rowFluxo['fldDebito'];
			$SaldoTotalFluxo = $sdTotalCredito - $sdTotalDebito;
			//##############################################//
	
			if($_POST['sel_order'] == 'tblfinanceiro_conta'){
				if($rowFluxo['fldMarcador_Id']){
					$rsMarcador = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_marcador WHERE fldId =".$rowFluxo['fldMarcador_Id']);
					$rowMarcador = mysql_fetch_array($rsMarcador);
					
					$descCampo = $rowMarcador['fldMarcador'];
				}else{
					$descCampo = '----------------------------------';
				}
			}else{				
				$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxo['fldConta_Id']);
				$rowConta = mysql_fetch_array($rsConta);
				
				$descCampo = $rowConta['fldNome'];
			}

			//busca a descricao pelo tipo de movimento, como pargamento de parcela
			if($rowFluxo['fldMovimento_Tipo'] == '3' or $rowFluxo['fldMovimento_Tipo'] == '4' or $rowFluxo['fldMovimento_Tipo'] == '7'){
				$rsDescricao = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_tipo WHERE fldId=". $rowFluxo['fldMovimento_Tipo']);
				$rowDescricao = mysql_fetch_array($rsDescricao);
				
				if($rowFluxo['fldMovimento_Tipo'] == '3'){//RECEBIMENTO DE PARCELA
					$rsParcela_Baixa = mysql_query("SELECT * FROM tblpedido_parcela_baixa WHERE fldId = ".$rowFluxo['fldReferencia_Id']);
					$rowParcela_Baixa = mysql_fetch_array($rsParcela_Baixa);
					echo mysql_error();
					
					$rsCliente = mysql_query("SELECT tblpedido.*, tblpedido_parcela.*, tblcliente.fldNome as fldClienteNome 
											FROM tblcliente INNER JOIN 
											(tblpedido LEFT JOIN tblpedido_parcela on tblpedido_parcela.fldPedido_Id = tblpedido.fldId)
											ON tblcliente.fldId = tblpedido.fldCliente_Id
											WHERE tblpedido_parcela.fldId =".$rowParcela_Baixa['fldParcela_Id']);
					echo mysql_error();
					$rowCliente = mysql_fetch_array($rsCliente);
					echo mysql_error();
					$Entidade = $rowCliente['fldClienteNome'];
					
				}elseif($rowFluxo['fldMovimento_Tipo'] == '3'){//PAGAMENTO DE PARCELA
					$rsParcela_Baixa = mysql_query("SELECT * FROM tblcompra_parcela_baixa WHERE fldId = ".$rowFluxo['fldReferencia_Id']);
					$rowParcela_Baixa = mysql_fetch_array($rsParcela_Baixa);
					echo mysql_error();
					
					$rsFornecedor = mysql_query("SELECT tblcompra.*, tblcompra_parcela.*, tblfornecedor.fldNomeFantasia as fldFornecedorNome 
											FROM tblfornecedor INNER JOIN 
											(tblcompra LEFT JOIN tblcompra_parcela on tblcompra_parcela.fldCompra_Id = tblcompra.fldId)
											ON tblfornecedor.fldId = tblcompra.fldFornecedor_Id
											WHERE tblcompra_parcela.fldId =".$rowParcela_Baixa['fldParcela_Id']);
					echo mysql_error();
					$rowFornecedor = mysql_fetch_array($rsFornecedor);
					echo mysql_error();
					
					$Entidade = $rowFornecedor['fldFornecedorNome'];
					
				}elseif($rowFluxo['fldMovimento_Tipo'] == '7'){//COMISSAO DE VENDEDOR
					$rsFuncionario = mysql_query("SELECT tblfuncionario_conta_fluxo.fldId,
											  tblfuncionario.fldNome
											  FROM tblfuncionario INNER JOIN tblfuncionario_conta_fluxo ON tblfuncionario.fldId = tblfuncionario_conta_fluxo.fldFuncionario_Id
											  WHERE tblfuncionario_conta_fluxo.fldId = ".$rowFluxo['fldReferencia_Id']);
					$rowFuncionario = mysql_fetch_array($rsFuncionario);
					echo mysql_error();
					$Entidade = $rowFuncionario['fldNome'];
				}
				
				$descricao = $rowDescricao['fldTipo']." (".substr($Entidade,0,35).")";
				
			//conta programada pega o nome da conta	
			}elseif($rowFluxo['fldMovimento_Tipo'] == '6'){
				$rsContaProg = mysql_query("SELECT tblfinanceiro_conta_pagar_programada.*, 
											   tblfinanceiro_conta_pagar_programada_baixa.fldId FROM 
											   tblfinanceiro_conta_pagar_programada INNER JOIN tblfinanceiro_conta_pagar_programada_baixa
											   ON tblfinanceiro_conta_pagar_programada_baixa.fldContaProgramada_Id = tblfinanceiro_conta_pagar_programada.fldId
											   WHERE tblfinanceiro_conta_pagar_programada_baixa.fldId =".$rowFluxo['fldReferencia_Id']);
				$rowContaProg = mysql_fetch_array($rsContaProg);
				
				$descricao = $rowContaProg['fldNome'];
			//transferencia	
			}elseif($rowFluxo['fldMovimento_Tipo'] == '2'){
				
				$rsContaOrigem = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxo['fldConta_Origem']);
				$rowContaOrigem = mysql_fetch_array($rsContaOrigem);
				echo mysql_error();
				
				$rsContaDestino = mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxo['fldConta_Destino']);
				$rsContaDestino = mysql_fetch_array($rsContaDestino);
				echo mysql_error();
			
				if($rowFluxoRegistro['fldConta_Origem'] != 0){	
					$descricao = "Recebimento de conta ".$rowContaOrigem['fldNome'];
				}else{
					$descricao = "Pagamento para conta ".$rsContaDestino['fldNome'];					
				}
			}else{
				$descricao = $rowFluxo['fldDescricao'];
			}
			
?>			<tr>
                <td style="width:60px; margin-left: 5px;"><?=$rowFluxo['fldId']?></td>
                <td style="width:345px"><?=$descricao?></td>
                <td style="width:340px"><?=$descCampo?></td>
                <td style="width:80px"><?=format_date_out($rowFluxo['fldData'])?></td>
                <td style="width:40px"><?=format_time_short($rowFluxo['fldHora'])?></td>
                <td style="width:80px; text-align:right"><?=format_number_out($rowFluxo['fldCredito'])?></td>
                <td style="width:80px; text-align:right"><?=format_number_out($rowFluxo['fldDebito'])?></td>
                <td style="width:80px; text-align:right"><?=format_number_out($SaldoFluxo)?></td>
            </tr>
<?         	
			if(($n == $limite) or ($x == $totalRegistros)){
				$n = 1;
								if($p < $pgTotal){
						$p += 1;
				}
?>							</table>    
						</td>
                    </tr>
                </table>
<?				if($x < $totalRegistros){
					$contadorCabecalho = "<tr style='border-bottom: 2px solid'>
					<td style='width: 950px'><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o do Caixa</h1></td>
					<td style='width: 200px'><p class='pag'>p&aacute;g. ".$p." de ".ceil($pgTotal)."</p></td>
					</tr>";
					print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$relatorioCabecalho;
					print $tableCabecalho;
?>					<tr>
						<td>
							<table class="table_relatorio_paisagem" summary="Relat&oacute;rio">
<?				}
			}else{
				$n ++;
			}

		}// end while
	}
	
	###############################################################	###############################################################	###############################################################
	if($_POST['sel_order'] == 'tblfinanceiro_conta_fluxo')
	{
		$sdCredito	= 0;
		$sdDebito 	= 0;
		$SaldoFluxo = 0;
		
		$query = str_replace("SELECT ", "SELECT SUM(tblfinanceiro_conta_fluxo.fldCredito) AS fldCredito_Soma, SUM(vwfinanceiro_conta_fluxo.fldDebito) AS fldDebito_Soma, ", $_SESSION['relatorio_caixa_sql']);
		$rsFluxo = mysql_query($query." GROUP BY fldData ORDER BY  $tblFiltro, vwfinanceiro_conta_fluxo.fldData, vwfinanceiro_conta_fluxo.fldHora, fldMovimento_Tipo, fldDescricao");
		while($rowFluxo = mysql_fetch_assoc($rsFluxo)){ 
			$sdCredito += $rowFluxo['fldCredito_Soma'];
			$sdDebito += $rowFluxo['fldDebito_Soma'];
			$SaldoFluxo = $sdCredito - $sdDebito;
			//soma total para rodape ########################//
			$sdTotalCredito += $rowFluxo['fldCredito_Soma'];
			$sdTotalDebito += $rowFluxo['fldDebito_Soma'];
			$SaldoTotalFluxo = $sdTotalCredito - $sdTotalDebito;
			//##############################################//
		?>
			<tr>
                <td style="width:772px; margin-left:5px">Soma do dia <?=format_date_out($rowFluxo['fldData'])?></td>
                <td style="width:80px;"><?=format_date_out($rowFluxo['fldData'])?></td>
                <td style="width:13px"></td>
                <td style="width:80px; text-align:right"><?=format_number_out($rowFluxo['fldCredito_Soma'])?></td>
                <td style="width:80px; text-align:right"><?=format_number_out($rowFluxo['fldDebito_Soma'])?></td>
                <td style="width:80px; text-align:right"><?=format_number_out($SaldoFluxo)?></td>
            </tr>
<?		}
		
		
	}

?>		<table style="width:1152px;" name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
			<tr>
				<?	$saldoFinal = $SaldoTotalFluxo + $_SESSION['saldo_anterior_valor'];	?>
				<td style="width:360px;">&nbsp;</td>
				<td style="width:70px">Entradas</td>
				<td style="width:60px; text-align:right; margin-right:10px;"><?=format_number_out($sdTotalCredito)?></td>
				<td style="width:20px; border-left:1px solid">&nbsp;</td>
				<td style="width:70px">Saidas</td>
				<td style="width:60px; text-align:right;  margin-right:10px;"><?=format_number_out($sdTotalDebito)?></td>
				<td style="width:20px; border-left:1px solid">&nbsp;</td>
				<td style="width:60px">Saldo</td>
				<td style="width:80px; text-align:right;  margin-right:10px;"><?=format_number_out($SaldoTotalFluxo)?></td>
				<td style="width:150px; text-align:right; border-left:1px solid">Saldo + Saldo Anterior</td>
				<td style="width:80px; text-align:right;"><?=format_number_out($saldoFinal)?></td>
			</tr>
		</table>
	</body>
</html>