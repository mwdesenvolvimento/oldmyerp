<div id="voltar">
    <p><a href="index.php?p=produto">n&iacute;vel acima</a></p>
</div>	

<h2>Novo Produto</h2>

<div class="alert">
	<p class="ok">O c&oacute;digo ser&aacute; auto-numerado se n&atilde;o for prenchido<p>
</div>
<div id="produto_codigo">
<?	
	$codigoTipo = fnc_sistema('produto_codigo');
	$barcode_tamanho = fnc_sistema('produto_barcode_tamanho');
	//a��es
	if(isset($_POST['txt_nome'])){
		
		$Codigo 			= $_POST['txt_codigo'];
		$Nome 				= trim(mysql_real_escape_string($_POST['txt_nome']));
		$Descricao	 		= mysql_real_escape_string($_POST['txt_descricao']);
		$Tipo 				= $_POST['sel_tipo'];
		$UN_Medida 			= $_POST['sel_unidade'];
		$Marca 				= $_POST['sel_marca'];
		$Categoria 			= $_POST['sel_categoria'];
		$SubCategoria 		= $_POST['sel_sub_categoria'];
		$Fornecedor 		= $_POST['sel_fornecedor'];
		$Estoque 			= format_number_in($_POST['txt_estoque']);
		$Val_Compra 		= format_number_in($_POST['txt_valor_compra']);
		$Lucro 				= format_number_in($_POST['txt_lucro_margem']);
		$Val_Venda 			= format_number_in($_POST['txt_valor_venda']);
		$Cod_Barras 		= $_POST['txt_codigo_barras'];
		$Obs 				= mysql_real_escape_string($_POST['txt_observacao']);
		$Data 				= date("Y-m-d");
		$EstoqueMinimo 		= format_number_in($_POST['txt_estoque_minimo']);
		$EstoqueInicial 	= format_number_in($_POST['txt_estoque_inicial']);
		$EstoqueControle 	= $_POST['sel_estoque'];
		$Ecf_Aliquota_Id 	= $_POST['sel_ecf_aliquota'];
		$Balanca			= ($_POST['chk_balanca'] ? 1 : 0);
		$Peso 				= format_number_in($_POST['txt_peso']);

		###################################################################################
		# ESPECIFICACOES
		###################################################################################
		$especificacoes_n_porta_cliche 						= (isset($_POST['txt_especificacao_porta_cliche'])) 						? $_POST['txt_especificacao_porta_cliche'] 						: '';
		$especificacoes_largura 							= (isset($_POST['txt_especificacao_largura'])) 								? $_POST['txt_especificacao_largura'] 							: '';
		$especificacoes_altura 								= (isset($_POST['txt_especificacao_altura'])) 								? $_POST['txt_especificacao_altura'] 							: '';
		$especificacoes_largura_papel 						= (isset($_POST['txt_especificacao_largura_papel'])) 						? $_POST['txt_especificacao_largura_papel'] 					: '';
		$especificacoes_altura_papel						= (isset($_POST['txt_especificacao_altura_papel'])) 						? $_POST['txt_especificacao_altura_papel'] 						: '';
		$especificacoes_regua								= (isset($_POST['txt_especificacao_regua'])) 								? $_POST['txt_especificacao_regua'] 							: '';
		$especificacoes_tam_calculo_papel 					= (isset($_POST['txt_especificacao_tamanho_calculo_papel'])) 				? $_POST['txt_especificacao_tamanho_calculo_papel'] 			: '';
		$especificacoes_velocidade_maquina					= (isset($_POST['txt_especificacao_vel_maquina'])) 							? $_POST['txt_especificacao_vel_maquina']						: '';
		$especificacoes_faca_numero							= (isset($_POST['txt_especificacao_faca_numero'])) 							? $_POST['txt_especificacao_faca_numero'] 						: '';
		$especificacoes_numero_carreiras					= (isset($_POST['txt_especificacao_numero_carreiras'])) 					? $_POST['txt_especificacao_numero_carreiras'] 					: '';
		$especificacoes_numero_repeticoes_cliche			= (isset($_POST['txt_especificacao_numero_repeticoes_cliche'])) 			? $_POST['txt_especificacao_numero_repeticoes_cliche'] 			: '';
		$especificacoes_cod_cliche							= (isset($_POST['txt_especificacao_cod_cliche'])) 							? $_POST['txt_especificacao_cod_cliche'] 						: '';
		$especificacoes_acerto_maquina						= (isset($_POST['txt_especificacao_acerto_maquina'])) 						? $_POST['txt_especificacao_acerto_maquina'] 					: '';
		$especificacoes_tempo_producao						= (isset($_POST['txt_especificacao_tempo_producao'])) 						? $_POST['txt_especificacao_tempo_producao'] 					: '';
		$especificacoes_rolo								= (isset($_POST['sel_especificacao_rolo'])) 								? $_POST['sel_especificacao_rolo'] 								: '';
		$especificacoes_etiqueta_cortada					= (isset($_POST['txt_especificacao_etiqueta_cortada'])) 					? $_POST['txt_especificacao_etiqueta_cortada'] 					: '';
		$especificacoes_qtd_etiqueta_rolo					= (isset($_POST['txt_especificacao_qtd_etiquetas_rolo'])) 					? $_POST['txt_especificacao_qtd_etiquetas_rolo'] 				: '';
		$especificacoes_verniz								= (isset($_POST['txt_especificacao_verniz'])) 								? $_POST['txt_especificacao_verniz'] 							: '';
		$especificacoes_tempo_acerto						= (isset($_POST['txt_especificacao_tempo_acerto'])) 						? $_POST['txt_especificacao_tempo_acerto'] 						: '';
		$especificacoes_operador							= (isset($_POST['txt_especificacao_operador'])) 							? $_POST['txt_especificacao_operador'] 							: '';
		$especificacoes_rebobinamento_largura_papel 		= (isset($_POST['txt_especificacao_rebobinamento_largura_papel'])) 			? $_POST['txt_especificacao_rebobinamento_largura_papel'] 		: '';
		$especificacoes_rebobinamento_numero_carreira 		= (isset($_POST['txt_especificacao_rebobinamento_n_carreiras'])) 			? $_POST['txt_especificacao_rebobinamento_n_carreiras'] 		: '';
		$especificacoes_rebobinamento_tempo_rebobinamento 	= (isset($_POST['txt_especificacao_rebobinamento_tempo_rebobinamento'])) 	? $_POST['txt_especificacao_rebobinamento_tempo_rebobinamento'] : '';
		$especificacoes_rebobinamento_tubete				= (isset($_POST['txt_especificacao_rebobinamento_tubete'])) 				? $_POST['txt_especificacao_rebobinamento_tubete'] 				: '';
		$especificacoes_rebobinamento_diametro_rolo			= (isset($_POST['txt_especificacao_rebobinamento_diametro_rolo'])) 			? $_POST['txt_especificacao_rebobinamento_diametro_rolo'] 		: '';
		$especificacoes_rebobinamento_velocidade_maquina	= (isset($_POST['txt_especificacao_rebobinamento_velocidade_maquina'])) 	? $_POST['txt_especificacao_rebobinamento_velocidade_maquina'] 	: '';
		$especificacoes_rebobinamento_faca_numero			= (isset($_POST['txt_especificacao_rebobinamento_faca_numero'])) 			? $_POST['txt_especificacao_rebobinamento_faca_numero'] 		: '';
		$especificacoes_rebobinamento_acerto_maquina		= (isset($_POST['txt_especificacao_rebobinamento_acerto_maquina'])) 		? $_POST['txt_especificacao_rebobinamento_acerto_maquina'] 		: '';
		$especificacoes_rebobinamento_qtd_etiqueta			= (isset($_POST['txt_especificacao_rebobinamento_qtd_etiqueta'])) 			? $_POST['txt_especificacao_rebobinamento_qtd_etiqueta']		: '';
		$especificacoes_rebobinamento_ir					= (isset($_POST['txt_especificacao_rebobinamento_ir'])) 					? $_POST['txt_especificacao_rebobinamento_ir'] 					: '';
		$especificacoes_rebobinamento_ca					= (isset($_POST['txt_especificacao_rebobinamento_ca'])) 					? $_POST['txt_especificacao_rebobinamento_ca'] 					: '';
		$especificacoes_rebobinamento_qtd_metros_rolo		= (isset($_POST['txt_especificacao_rebobinamento_qtd_metros_rolo'])) 		? $_POST['txt_especificacao_rebobinamento_qtd_metros_rolo'] 	: '';
		$especificacoes_rebobinamento_qtd_etiqueta_metro	= (isset($_POST['txt_especificacao_rebobinamento_qtd_etiqueta_metro'])) 	? $_POST['txt_especificacao_rebobinamento_qtd_etiqueta_metro'] 	: '';
		$especificacoes_embalagem_altura_rolo				= (isset($_POST['txt_especificacao_embalagem_altura_rolo'])) 				? $_POST['txt_especificacao_embalagem_altura_rolo'] 			: '';
		$especificacoes_embalagem_caixa_numero				= (isset($_POST['txt_especificacao_embalagem_caixa_numero'])) 				? $_POST['txt_especificacao_embalagem_caixa_numero'] 			: '';
		$especificacoes_embalagem_enchimento_caixa			= (isset($_POST['txt_especificacao_embalagem_enchimento_caixa'])) 			? $_POST['txt_especificacao_embalagem_enchimento_caixa'] 		: '';
		###################################################################################
		
		//GRAVAR ACAO DE GRAVAR EM SESSAO PRO CASO DE ESTAR CADASTRANDO VARIOS EM SEQUENCIA
		$_SESSION['acao_cadastro_produto'] = $_POST['sel_acao'];
		
		if($Codigo == NULL){
			if($codigoTipo == 'categoria'){
				
				//O SISTEMA VAI VERIFICAR QUAL O C�DIGO DE REFERENCIA, UTILIZADO PELO ULTIMO PRODUTO CADSTRADO COM A MESMA CATEGORIA E SUBCATEGORIA, SOMA +1 PARA GERAR O C�DIGO DESSE NOVO PRODUTO, EM ORDEM.
				//EM SEGUIDA, PASSA PELA FUN��O PARA VERIFICAR SE ESSE C�DIGO J� EXISTE, SE EXISTIR, ADICIONA +1 E CONTINUA VERIFICANDO, AT� QUE ACHE UM C�DIGO QUE POSSA SER UTILIZADO.
				$Codigo_ref 	= mysql_fetch_array(mysql_query("SELECT MAX(fldRefCodigo) AS ultimoCodigo FROM tblproduto WHERE fldCategoria_Id = " . $Categoria . " AND fldSubCategoria_Id = " . $SubCategoria . " LIMIT 1"));
				$Codigo_ref 	= $Codigo_ref['ultimoCodigo'] + 1;
				$Codigo 		= str_pad($Categoria, 2, "0", STR_PAD_LEFT) . str_pad($SubCategoria, 3, "0", STR_PAD_LEFT) . str_pad($Codigo_ref, 4, "0", STR_PAD_LEFT);
				
				$verificacao	= explode(',', produtoCodigo($Codigo, $Codigo_ref));
				$Codigo_ref		= $verificacao[0];
				$Codigo			= str_pad($verificacao[1], 9, "0", STR_PAD_LEFT);
				
			}elseif($codigoTipo == 'marca'){
				//ASSIM COMO EM CATEGORIA, VERIFICA A REFERENCIA DO ULTIMO PRODUTO CADASTRADO COM ESSA MARCA, SOMA +1 E VERIFICA PELA FUNCAO SE PODE SER UTILIZADO.
				$Codigo_ref 	= mysql_fetch_array(mysql_query("SELECT MAX(fldRefCodigo) AS ultimoCodigo FROM tblproduto WHERE fldMarca_Id = " . $Marca . " LIMIT 1"));
				$Codigo_ref 	= $Codigo_ref['ultimoCodigo'] + 1;
				$Codigo 		= str_pad($Marca, 4, "0", STR_PAD_LEFT) . str_pad($Codigo_ref, 5, "0", STR_PAD_LEFT);
				
				$verificacao	= explode(',', produtoCodigo($Codigo, $Codigo_ref));
				$Codigo_ref		= $verificacao[0];
				$Codigo			= str_pad($verificacao[1], 9, "0", STR_PAD_LEFT);
			}
		}
		
		$sqlInsert = "INSERT INTO tblproduto
		(fldCodigo, fldNome, fldDescricao, fldUN_Medida_Id, fldMarca_Id, fldRefCodigo, fldCategoria_Id, fldSubCategoria_Id, fldFornecedor_Id, fldValorCompra, fldLucroMargem, fldValorVenda, fldCodigoBarras, 
		 fldObservacao, fldCadastroData, fldEstoque_Minimo, fldEstoque_Controle, fldEcf_Aliquota_Id, fldTipo_Id, fldBalanca, fldPeso)
		VALUES(
		'$Codigo', 
		'$Nome', 
		'$Descricao', 
		'$UN_Medida', 
		'$Marca',
		'$Codigo_ref',
		'$Categoria', 
		'$SubCategoria', 
		'$Fornecedor', 
		'$Val_Compra',  
		'$Lucro', 
		'$Val_Venda', 
		'$Cod_Barras', 
		'$Obs', 
		'$Data',
		'$EstoqueMinimo',
		'$EstoqueControle',
		'$Ecf_Aliquota_Id',
		'$Tipo',
		'$Balanca',
		'$Peso')";
		
		if($produto_id = fnc_log_insert($sqlInsert, 'tblproduto')){
	
			/*$sqlInsert_Esp = "INSERT INTO tblproduto_especificacao
			(fldProduto_Id, fldNumero_Porta_Cliche, fldLargura, fldAltura, fldLargura_Papel, fldAltura_Papel, fldRegua, fldTamanho_Calculo_Papel,
			fldVelocidade_Maquina, fldFaca_Numero, fldNumero_Carreiras, fldNumero_Repeticoes_Cliche, fldCodigo_Cliche, fldAcerto_Maquina, fldTempo_Producao,
			fldRolo, fldEtiqueta_Cortada, fldQtd_Etiquetas_Rolo, fldVerniz, fldTempo_Acerto, fldOperador, fldLargura_Papel_Rebobinamento, fldNumero_Carreiras_Rebobinamento,
			fldTempo_Rebobinamento, fldTubete, 	fldDiametro_Rolo, fldVelocidade_Maquina_Rebobinamento, fldFaca_Numero_Rebobinamento, fldAcerto_Maquina_Rebobinamento,
			fldQtd_Etiqueta, fldIR, fldCA, fldQtd_Metros_Rolo, fldQtd_Etiquetas_Metro, fldAltura_Rolo, fldCaixa_Numero, fldEnchimento_Caixa) VALUES
			('$produto_id', '$especificacoes_n_porta_cliche', '$especificacoes_largura', '$especificacoes_altura', '$especificacoes_largura_papel',
			'$especificacoes_altura_papel', '$especificacoes_regua', '$especificacoes_tam_calculo_papel', '$especificacoes_velocidade_maquina',
			'$especificacoes_faca_numero', '$especificacoes_numero_carreiras', '$especificacoes_numero_repeticoes_cliche', '$especificacoes_cod_cliche',
			'$especificacoes_acerto_maquina', '$especificacoes_tempo_producao', '$especificacoes_rolo', '$especificacoes_etiqueta_cortada',
			'$especificacoes_qtd_etiqueta_rolo', '$especificacoes_verniz', '$especificacoes_tempo_acerto', '$especificacoes_operador', '$especificacoes_rebobinamento_largura_papel',
			'$especificacoes_rebobinamento_numero_carreira', '$especificacoes_rebobinamento_tempo_rebobinamento', '$especificacoes_rebobinamento_tubete',
			'$especificacoes_rebobinamento_diametro_rolo', '$especificacoes_rebobinamento_velocidade_maquina', '$especificacoes_rebobinamento_faca_numero', '$especificacoes_rebobinamento_acerto_maquina',
			'$especificacoes_rebobinamento_qtd_etiqueta', '$especificacoes_rebobinamento_ir', '$especificacoes_rebobinamento_ca', '$especificacoes_rebobinamento_qtd_metros_rolo', 
			'$especificacoes_rebobinamento_qtd_etiqueta_metro', '$especificacoes_embalagem_altura_rolo', '$especificacoes_embalagem_caixa_numero', 
			'$especificacoes_embalagem_enchimento_caixa')";
			
			mysql_query($sqlInsert_Esp) or die(mysql_error());*/
		
			//CASO NAO SERJA POR CATEGORIA OU MARCA, OU CLIENTE N�O SELECIONOU MARCA/CATEGORIA E CAMPO DE CODIGO ESTA NULO
			//TB VERIFICA SE HE CONFIGURADO COMO ID NO SISTEMA
			//COPIA O ID E MANDA GRAVAR COMO CODIGO
			if(($codigoTipo == 'id' && $Codigo == NULL) || $Codigo == NULL){
				//VERIFICA SE NAO TEM NENHUM CODIGO J� COM ESSE NUMERO
				$Codigo 		=	$produto_id;
				$verificacao	=	explode(',', produtoCodigo($Codigo, ''));
				$Codigo_ref		=	$verificacao[0];
				$Codigo			=	$verificacao[1];
				
				mysql_query("UPDATE tblproduto SET fldCodigo = '$Codigo' WHERE fldId = $produto_id");
			}
			
			//ATUALIZA O CODIGO DE BARRAS AUTOMATICAMENTE COPIANDO O C�DIGO 
			if($Cod_Barras == NULL){
				$Cod_Barras = str_pad($produto_id, $barcode_tamanho, "0", STR_PAD_LEFT);
				
				mysql_query("UPDATE tblproduto SET fldCodigoBarras = '$Cod_Barras' WHERE fldId = $Codigo");
			}
			
			
			//INSERINDO ESTOQUE INICIAL
			fnc_estoque_movimento_lancar($produto_id, 'estoque inicial', $EstoqueInicial, $saida, 7, '', '1', '1');
			
			//redirecionando
			switch($_SESSION['acao_cadastro_produto']){
				case '1'	:  $filtro ='&mensagem=ok&codigo='.$Codigo.'&produto='.$Nome;   				break;
				case '2'	:  $filtro = "_detalhe&id=".$produto_id."&codigo=".$Codigo."&produto=".$Nome; 	break;
				case '3'	:  $filtro = '_novo';															break;
			}
			header("location:index.php?p=produto".$filtro);
		}
		else {
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados. Por favor, entre em contato com o suporte imediatamente!</p>
				<a class="voltar" href="index.php?p=produto_novo">voltar</a>
			</div>
<?			echo mysql_error();
		}
	}
	else{
		$tela_id = 3;
?>		
		<form class="frm_detalhe" name="frm_general" style="width:890px" id="frm_produto" action="" method="post" >
        	<fieldset>
            	<legend>Cupom Fiscal</legend>
                <ul>
					<li>
                       <label for="sel_ecf_aliquota">Al&iacute;quota</label>
                       <select style="width: 200px" id="sel_ecf_aliquota" name="sel_ecf_aliquota" >
                        	<option value="">selecionar</option>
<?							$rsECF_aliquota = mysql_query("SELECT * FROM tblecf_aliquota WHERE fldDisabled = 0");
							while($rowECF_aliquota= mysql_fetch_array($rsECF_aliquota)){
?>								<option <? if($rowProduto['fldEcf_Aliquota_Id']== $rowECF_aliquota['fldId']){print('selected="selected"');}?> value="<?=$rowECF_aliquota['fldId']?>"><?=$rowECF_aliquota['fldDescricao']?></option>
<?							}
?>						</select>
                	</li>
            	</ul>
            </fieldset>
			<div class="form">
				
				<div style="margin:20px auto; width:870px;">
					
					<ul>
						<li class="form">
							<label for="txt_codigo">Codigo</label>
							<input type="text" style="width: 80px" id="txt_codigo" name="txt_codigo" value="" />
						</li>
						<li class="form">
							<label for="txt_nome">Produto</label>
							<input <?= formPermissao("txt_nome",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:320px" id="txt_nome" name="txt_nome" value="" />
						</li>
						<li class="form">
							<label for="txt_descricao">Descricao</label>
							<input <?= formPermissao("txt_descricao",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:280px;" id="txt_descricao" name="txt_descricao" value="" />
						</li>
						<li class="form">
							<label for="sel_tipo">Tipo</label>
							<select style="width:125px" id="sel_tipo" name="sel_tipo" >
								<option value="">selecionar</option>
	<?							$rsTipo = mysql_query("SELECT * FROM tblproduto_tipo WHERE fldInativo = 0 ORDER BY fldId DESC");
								while($rowTipo= mysql_fetch_array($rsTipo)){
	?>								<option value="<?=$rowTipo['fldId']?>"><?=$rowTipo['fldTipo']?></option>
	<?							}
	?>						</select>
						</li>
						<li class="form">
							<label for="sel_unidade">Unidade de Medida</label>
							<select style="width: 125px" id="sel_unidade" name="sel_unidade" >
								<option value="">selecionar</option>
	<?							$rsUnidade = mysql_query("SELECT * FROM tblproduto_unidade_medida ORDER BY fldNome");
								while($rowUnidade= mysql_fetch_array($rsUnidade)){
	?>								<option value="<?=$rowUnidade['fldId']?>"><?=$rowUnidade['fldNome']?></option>
	<?							}
	?>						</select>
						</li>
						<li class="form">
							<label for="sel_marca">Marca</label>
							<select style="width:160px" <?=formPermissao("sel_marca",$tela_id) ? print "class='obrigatorio'" : "" ?> id="sel_marca" name="sel_marca" >
								<option value="0">selecionar</option>
	<?							$rsMarca = mysql_query("SELECT * FROM tblmarca WHERE fldDisabled = '0' ORDER BY fldNome");
								while($rowMarca= mysql_fetch_array($rsMarca)){
	?>								<option value="<?=$rowMarca['fldId']?>"><?=$rowMarca['fldNome']?></option>
	<?							}
	?>						</select>
						</li>
						<li class="form">
							<label for="sel_categoria">Categoria</label>
							<select <?=formPermissao("sel_categoria",$tela_id) ? print "class='obrigatorio'" : "" ?> id="sel_categoria" name="sel_categoria" >
								<option style="width:160px" value="0">selecionar</option>
	<?							$rsCategoria = mysql_query("SELECT * FROM tblcategoria WHERE fldExcluido = '0' and fldDisabled = '0' ORDER BY fldNome ");
								while($rowCategoria= mysql_fetch_array($rsCategoria)){
	?>								<option value="<?= $rowCategoria['fldId'] ?>"><?= $rowCategoria['fldNome'] ?></option>
	<?							}
	?>						</select>
						</li>
						<li class="form">
							<label for="sel_sub_categoria">Sub categoria</label>
							<select style="width:160px" <?= formPermissao("sel_sub_categoria",$tela_id) ? print "class='obrigatorio'" : "" ?> id="sel_sub_categoria" name="sel_sub_categoria" >
								<option value="0">selecione uma categoria</option>
							</select>
						</li>
						<li class="form">
							<label for="sel_fornecedor">Fornecedor</label>
							<select style="width:160px" <?= formPermissao("sel_fornecedor",$tela_id) ? print "class='obrigatorio'" : "" ?> id="sel_fornecedor" name="sel_fornecedor" >
								<option value="0">selecionar</option>
	<?							$rsFornecedor = mysql_query("SELECT * FROM tblfornecedor WHERE fldDisabled = '0' ORDER BY fldNomeFantasia");
								while($rowFornecedor= mysql_fetch_array($rsFornecedor)){
	?>								<option value="<?= $rowFornecedor['fldId'] ?>"><?= $rowFornecedor['fldNomeFantasia'] ?></option>
	<?							}
	?>						</select>
						</li>
						<li class="form">
							<label for="txt_estoque_minimo">Estoque M&iacute;nimo</label>
							<input type="text" style="width:100px; text-align:right" id="txt_estoque_minimo" name="txt_estoque_minimo" value="" />
						</li>
						<li class="form">
							<label for="txt_valor_compra">Valor compra</label>
							<input <?= formPermissao("txt_valor_compra",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style="width:80px; text-align:right" id="txt_valor_compra" name="txt_valor_compra" value="" />
						</li>
						<li class="form">
							<label for="txt_lucro_margem">Marg. Lucro %</label>
							<input <?= formPermissao("txt_lucro_margem",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style="width:100px; text-align:right" id="txt_lucro_margem" name="txt_lucro_margem" value="0" />
						</li>
						<li class="form">
							<label for="txt_valor_venda">Valor venda</label>
							<input <?= formPermissao("txt_valor_venda",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style="width:80px; text-align:right" id="txt_valor_venda" name="txt_valor_venda" value="" />
						</li>
						<li class="form">
							<label for="txt_codigo_barras">C&oacute;d. barras</label>
							<input <?= formPermissao("txt_codigo_barras",$tela_id) ? print "class='obrigatorio prev'" : "class='prev'" ?> type="text" style="width:120px" id="txt_codigo_barras" name="txt_codigo_barras" maxlength="<?=$barcode_tamanho?>" value="" />
						</li>
						<li class="form">
							<label for="txt_peso">Peso (KG)</label>
							<input type="text" style="width:100px;" id="txt_peso" name="txt_peso" value="" />
						</li>
						<li class="form">
							<label for="chk_balanca">Balan&ccedil;a</label>
							<input type="checkbox" style="width:50px" id="chk_balanca" name="chk_balanca" <? ($rowProduto['fldBalanca']==1 ? print('checked="checked"') : '')?> />
						</li>
						<li class="form">
							<label for="sel_estoque">Controlar estoque</label>
							<select style="width:120px" id="sel_estoque" name="sel_estoque" >
								<option value="1">sim</option>
								<option value="2">n&atilde;o</option>
								<option value="3">sim (permitir estoque negativo)</option>
							</select>
						</li>
						<li class="form">
							<label for="txt_estoque_inicial">Estoque Inicial</label>
							<input type="text" style="width:100px; text-align:right" id="txt_estoque_inicial" name="txt_estoque_inicial" value="" />
						</li>
						<li class="form">
							<label for="txt_observacao">Observa&ccedil;&atilde;o</label>
							<textarea style="width:865px; height:80px;" id="txt_observacao" name="txt_observacao" ></textarea>
						</li>
                        
                       	<span style="clear:both; display:block"></span>
                        
						<? if($_SESSION['ordem_producao'] != 0){ ?>
                        <fieldset style="margin-bottom:30px; padding:0 15px;">
                        
                            <legend>Especifica&ccedil;&otilde;es</legend>
                        
                            <li class="form">
                                <label for="txt_especificacao_porta_cliche">N. do porta clich&ecirc;</label>
                                <input type="text" style="width:130px" id="txt_especificacao_porta_cliche" name="txt_especificacao_porta_cliche" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_largura">Largura</label>
                                <input type="text" style="width:90px" name="txt_especificacao_largura" id="txt_especificacao_largura" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_altura">Altura</label>
                                <input type="text" style="width:90px" name="txt_especificacao_altura" id="txt_especificacao_altura" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_largura_papel">Largura do papel (cm)</label>
                                <input type="text" style="width:160px" name="txt_especificacao_largura_papel" id="txt_especificacao_largura_papel" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_altura_papel">Altura do papel (cm)</label>
                                <input type="text" style="width:160px" name="txt_especificacao_altura_papel" id="txt_especificacao_altura_papel" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_regua">R&eacute;gua (cm)</label>
                                <input type="text" style="width:112px" name="txt_especificacao_regua" id="txt_especificacao_regua" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_tamanho_calculo_papel">Tam. para calculo do papel (cm)</label>
                                <input type="text" style="width:200px" name="txt_especificacao_tamanho_calculo_papel" id="txt_especificacao_tamanho_calculo_papel" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_vel_maquina">Velocidade da maquina</label>
                                <input type="text" style="width:150px" name="txt_especificacao_vel_maquina" id="txt_especificacao_vel_maquina" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_faca_numero">Faca n.</label>
                                <input type="text" style="width:110px" name="txt_especificacao_faca_numero" id="txt_especificacao_faca_numero" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_numero_carreiras">N. de carreiras</label>
                                <input type="text" style="width:120px" name="txt_especificacao_numero_carreiras" id="txt_especificacao_numero_carreiras" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_numero_repeticoes_cliche">N. de repeti&ccedil;&otilde;es do clich&ecirc;</label>
                                <input type="text" style="width:180px" name="txt_especificacao_numero_repeticoes_cliche" id="txt_especificacao_numero_repeticoes_cliche" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_cod_cliche">C&oacute;d. clich&ecirc;</label>
                                <input type="text" style="width:90px" name="txt_especificacao_cod_cliche" id="txt_especificacao_cod_cliche" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_acerto_maquina">Acerto m&aacute;quina (mt)</label>
                                <input type="text" style="width:130px" name="txt_especificacao_acerto_maquina" id="txt_especificacao_acerto_maquina" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_tempo_producao">Tempo de produ&ccedil;&atilde;o (min)</label>
                                <input type="text" style="width:180px" name="txt_especificacao_tempo_producao" id="txt_especificacao_tempo_producao" />
                            </li>
                            
                            <li class="form">
                                <label for="sel_especificacao_rolo">Rolo</label>
                                <select style="width:87px" name="sel_especificacao_rolo" id="sel_especificacao_rolo">
                                    <option></option>
                                    <option>500 mts</option>
                                    <option>1000 mts</option>
                                </select>
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_etiqueta_cortada">Etiqueta cortada</label>
                                <input type="text" style="width:110px" name="txt_especificacao_etiqueta_cortada" id="txt_especificacao_etiqueta_cortada" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_qtd_etiquetas_rolo">Qtd etiquetas por rolo</label>
                                <input type="text" style="width:150px" name="txt_especificacao_qtd_etiquetas_rolo" id="txt_especificacao_qtd_etiquetas_rolo" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_verniz">Verniz</label>
                                <input type="text" style="width:100px" name="txt_especificacao_verniz" id="txt_especificacao_verniz" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_tempo_acerto">Tempo de acerto</label>
                                <input type="text" style="width:130px" name="txt_especificacao_tempo_acerto" id="txt_especificacao_tempo_acerto" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_operador">Operador</label>
                                <input type="text" style="width:100px" name="txt_especificacao_operador" id="txt_especificacao_operador" />
                            </li>
                            
                        </fieldset>
                        
                        <span style="clear:both; display:block;"></span>
                        
                        <fieldset style="margin:0 0 30px 0; padding:0 15px;">
                        
                            <legend>Rebobinamento</legend>
                            
                            <li class="form">
                                <label for="txt_especificacao_rebobinamento_largura_papel">Largura do papel</label>
                                <input type="text" style="width:145px" name="txt_especificacao_rebobinamento_largura_papel" id="txt_especificacao_rebobinamento_largura_papel" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_rebobinamento_n_carreiras">N. de carreiras</label>
                                <input type="text" style="width:145px" name="txt_especificacao_rebobinamento_n_carreiras" id="txt_especificacao_rebobinamento_n_carreiras" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_rebobinamento_tempo_rebobinamento">Tempo de rebobinamento</label>
                                <input type="text" style="width:180px" name="txt_especificacao_rebobinamento_tempo_rebobinamento" id="txt_especificacao_rebobinamento_tempo_rebobinamento" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_rebobinamento_tubete">Tubete</label>
                                <input type="text" style="width:145px" name="txt_especificacao_rebobinamento_tubete" id="txt_especificacao_rebobinamento_tubete" />
                            </li>
                            
                            <li class="form">
                                <label for="txt_especificacao_rebobinamento_diametro_rolo">Diametro do rolo</label>
                                <input type="text" style="width:146px" name="txt_especificacao_rebobinamento_diametro_rolo" id="txt_especificacao_rebobinamento_diametro_rolo" />
                            </li>
                            
                            <span style="clear:both; display:block;"></span>
                            
                            <fieldset style="margin:0 0 15px 0">
                            
                                <legend>Rebobinamento comum</legend>
                            
                                <li class="form">
                                    <label for="txt_especificacao_rebobinamento_velocidade_maquina">Velocidade da maquina</label>
                                    <input type="text" style="width:145px" name="txt_especificacao_rebobinamento_velocidade_maquina" id="txt_especificacao_rebobinamento_velocidade_maquina" />
                                </li>
                                
                                <li class="form">
                                    <label for="txt_especificacao_rebobinamento_faca_numero">Faca numero</label>
                                    <input type="text" style="width:116px" name="txt_especificacao_rebobinamento_faca_numero" id="txt_especificacao_rebobinamento_faca_numero" />
                                </li>
                                
                                <li class="form">
                                    <label for="txt_especificacao_rebobinamento_acerto_maquina">Acerto maquina</label>
                                    <input type="text" style="width:130px" name="txt_especificacao_rebobinamento_acerto_maquina" id="txt_especificacao_rebobinamento_acerto_maquina" />
                                </li>
                                
                                <li class="form">
                                    <label for="txt_especificacao_rebobinamento_qtd_etiqueta">Qtd de etiqueta</label>
                                    <input type="text" style="width:116px" name="txt_especificacao_rebobinamento_qtd_etiqueta" id="txt_especificacao_rebobinamento_qtd_etiqueta" />
                                </li>
                                
                                <li class="form">
                                    <label for="txt_especificacao_rebobinamento_ir">IR</label>
                                    <input type="text" style="width:116px" name="txt_especificacao_rebobinamento_ir" id="txt_especificacao_rebobinamento_ir" />
                                </li>
                                
                                <li class="form">
                                    <label for="txt_especificacao_rebobinamento_ca">CA</label>
                                    <input type="text" style="width:116px" name="txt_especificacao_rebobinamento_ca" id="txt_especificacao_rebobinamento_ca" />
                                </li>
                            
                            </fieldset>
                            
                            <fieldset style="margin:15px 0">
                            
                                <legend>Rebobinamento (rolinho)</legend>
                            
                                <li class="form">
                                    <label for="txt_especificacao_rebobinamento_qtd_metros_rolo">Qtd de metros por rolo</label>
                                    <input type="text" style="width:180px" name="txt_especificacao_rebobinamento_qtd_metros_rolo" id="txt_especificacao_rebobinamento_qtd_metros_rolo" />
                                </li>
                                
                                <li class="form">
                                    <label for="txt_especificacao_rebobinamento_qtd_etiqueta_metro">Qtd de etiquetas por metro</label>
                                    <input type="text" style="width:180px" name="txt_especificacao_rebobinamento_qtd_etiqueta_metro" id="txt_especificacao_rebobinamento_qtd_etiqueta_metro" />
                                </li>
                            
                            </fieldset>
                            
                            <fieldset style="margin:15px 0">
                            
                                <legend>Embalagem</legend>
                            
                                <li class="form">
                                    <label for="txt_especificacao_embalagem_altura_rolo">Altura do rolo (mm)</label>
                                    <input type="text" style="width:180px" name="txt_especificacao_embalagem_altura_rolo" id="txt_especificacao_embalagem_altura_rolo" />
                                </li>
                                
                                <li class="form">
                                    <label for="txt_especificacao_embalagem_caixa_numero">Caixa n.</label>
                                    <input type="text" style="width:180px" name="txt_especificacao_embalagem_caixa_numero" id="txt_especificacao_embalagem_caixa_numero" />
                                </li>
                                
                                <li class="form">
                                    <label for="txt_especificacao_embalagem_enchimento_caixa">Enchimento da caixa</label>
                                    <input type="text" style="width:180px" name="txt_especificacao_embalagem_enchimento_caixa" id="txt_especificacao_embalagem_enchimento_caixa" />
                                </li>
                            
                            </fieldset>
                        </fieldset>
                        <?	} ?>
						
						<div style="float:right; margin-right:4px; margin-top:-10px;">
						
							<li style="margin:-14px 5px 0 0;">
								<label for="sel_acao">&nbsp;</label>
								<select style=" background:#FDDC9D" id="sel_acao" name="sel_acao" >
									<option <?=($_SESSION['acao_cadastro_produto'] == 1)? "selected='selected'" : ''?> value="1">salvar e voltar</option>
									<option <?=($_SESSION['acao_cadastro_produto'] == 2)? "selected='selected'" : ''?> value="2">salvar e continuar editando</option>
									<option <?=($_SESSION['acao_cadastro_produto'] == 3)? "selected='selected'" : ''?> value="3">salvar e novo</option>
								</select>
							</li>
							<li style="margin-top:0px">
								<input type="submit" style="margin: 0" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="salvar" title="Salvar" />
							</li>
							<li style="margin-top:0px">
								<a href="index.php?p=produto" class="btn_cancel">cancelar</a>
							</li>
							
						</div>
						
						
					</ul>
					
				</div>
					
        	</div>
		</form>
<?	}
?>        
</div>