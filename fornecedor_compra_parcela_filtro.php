<?php
	
	//recebendo a data do calendário de período	
	$_SESSION['txt_vencimento1'] = (isset($_POST['txt_calendario_data_inicial'])) ? $_POST['txt_calendario_data_inicial'] : '';
	$_SESSION['txt_vencimento2'] = (isset($_POST['txt_calendario_data_final'])) ? $_POST['txt_calendario_data_final'] : '';
	
	$data = explode("-",date("Y-m-d"));
	$ano = $data[0];
	$mes = $data[1];
		
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_vencimento1'] = "";
		$_SESSION['txt_vencimento2'] = "";
		$_SESSION['sel_fornecedor_compra_parcela'] = "";
		$_SESSION['sel_mes'] = 'todos';
	}
	else{
		$_SESSION['txt_vencimento1'] = (isset($_POST['txt_vencimento1']) ? $_POST['txt_vencimento1'] : $_SESSION['txt_vencimento1']);
		$_SESSION['txt_vencimento2'] = (isset($_POST['txt_vencimento2']) ? $_POST['txt_vencimento2'] : $_SESSION['txt_vencimento2']);
		$_SESSION['sel_fornecedor_compra_parcela'] = (isset($_POST['sel_fornecedor_compra_parcela']) ? $_POST['sel_fornecedor_compra_parcela'] : $_SESSION['sel_fornecedor_compra_parcela']);
		$_SESSION['sel_mes'] = (isset($_POST['sel_mes']) ? $_POST['sel_mes'] : $_SESSION['sel_mes']);
		//deixar padrao no mes 'todos'
		($_SESSION['sel_mes'] == '') ? $_SESSION['sel_mes'] = 'todos' :  '';
	}
	
?>
<form id="frm-filtro" action="<?=$endereco_raiz?>&amp;modo=compra_parcela" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
    	<li>
            <select id="sel_mes" name="sel_mes">
                <option value="todos"<? ($_SESSION['sel_mes'] == "todos") ? print "selected" : "" ?>>MÊS</option>
                <option value="01" 	<? 	($_SESSION['sel_mes'] == "01") ? print "selected" : "" ?>>JANEIRO</option>
                <option value="02" 	<? 	($_SESSION['sel_mes'] == "02") ? print "selected" : "" ?>>FEVEREIRO</option>
                <option value="03" 	<? 	($_SESSION['sel_mes'] == "03") ? print "selected" : "" ?>>MAR&Ccedil;O</option>
                <option value="04" 	<? 	($_SESSION['sel_mes'] == "04") ? print "selected" : "" ?>>ABRIL</option>
                <option value="05" 	<? 	($_SESSION['sel_mes'] == "05") ? print "selected" : "" ?>>MAIO</option>
                <option value="06" 	<? 	($_SESSION['sel_mes'] == "06") ? print "selected" : "" ?>>JUNHO</option>
                <option value="07" 	<? 	($_SESSION['sel_mes'] == "07") ? print "selected" : "" ?>>JULHO</option>
                <option value="08" 	<? 	($_SESSION['sel_mes'] == "08") ? print "selected" : "" ?>>AGOSTO</option>
                <option value="09" 	<? 	($_SESSION['sel_mes'] == "09") ? print "selected" : "" ?>>SETEMBRO</option>
                <option value="10" 	<? 	($_SESSION['sel_mes'] == "10") ? print "selected" : "" ?>>OUTUBRO</option>
                <option value="11" 	<? 	($_SESSION['sel_mes'] == "11") ? print "selected" : "" ?>>NOVEMBRO</option>
                <option value="12" 	<? 	($_SESSION['sel_mes'] == "12") ? print "selected" : "" ?>>DEZEMBRO</option>
        	</select>	
      	</li>
    	<li>
      		<label for="txt_vencimento1">Vencimento: </label>
<?			$vencimento1 = $_SESSION['txt_vencimento1'];
?>      	<input style="width: 100px; text-align: center;" type="text" class="calendario-mask" name="txt_vencimento1" id="txt_vencimento1" value="<?=$vencimento1?>"/>
		</li>
    	<li>
      		<label for="txt_vencimento2">&nbsp;</label>
<?			$vencimento2 = $_SESSION['txt_vencimento2'];
?>     		<input style="width: 100px; text-align: center;" type="text" class="calendario-mask" name="txt_vencimento2" id="txt_vencimento2" value="<?=$vencimento2?>"/>
			<a href="calendario_periodo,<?=format_date_in($vencimento1) . ',' . format_date_in($vencimento2)?>,fornecedor_detalhe&id=<?=$fornecedor_id?>&modo=compra_parcela" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
		</li>
   		<li>
      		<label for="sel_fornecedor_compra_parcela">Status</label>
            
            <select id="sel_fornecedor_compra_parcela" name="sel_fornecedor_compra_parcela">
                <option value="todos" 		<? ($_SESSION['sel_fornecedor_compra_parcela'] == "todos") 		  ? print "selected" : "" ?>>todos</option>
                <option value="em aberto" 	<? ($_SESSION['sel_fornecedor_compra_parcela'] == "em aberto") 	  ? print "selected" : "" ?>>em aberto</option>
                <option value="pago" 		<? ($_SESSION['sel_fornecedor_compra_parcela'] == "pago") 		  ? print "selected" : "" ?>>pago</option>
                <option value="pago parcial"<? ($_SESSION['sel_fornecedor_compra_parcela'] == "pago parcial") ? print "selected" : "" ?>>pago parcial</option>
                <option value="vencido" 	<? ($_SESSION['sel_fornecedor_compra_parcela'] == "vencido") 	  ? print "selected" : "" ?>>vencido</option>
        	</select>
      	</li>
    </ul>
    
    <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
    <button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
  </fieldset>
</form>

<?

	if(($_SESSION['txt_vencimento1']) != ""){
		$filtro .= " and fldVencimento >= '" . format_date_in($_SESSION['txt_vencimento1']) . "'";
	}

	if(($_SESSION['txt_vencimento2']) != ""){
		$filtro .= ($filtro ? " and" : "");
		$filtro .= " fldVencimento <= '" . format_date_in($_SESSION['txt_vencimento2']) . "'";
	}
	
	if(($_SESSION['sel_fornecedor_compra_parcela']) != ""){
				
		switch($_SESSION['sel_fornecedor_compra_parcela']){
			case "em aberto":
				$filtro_status .= " HAVING COUNT(tblcompra_parcela_baixa.fldValor) < 1";
				$filtro  .=  " AND tblcompra_parcela.fldVencimento >= '".date("Y-m-d")."'";
			break;
			
			case "pago":
				$filtro_status .= " HAVING tblcompra_parcela.fldValor <= (fldValorBaixa + fldBaixaDesconto)";
			break;

			case "pago parcial":
				$filtro_status .= " HAVING tblcompra_parcela.fldValor > (fldValorBaixa + fldBaixaDesconto) and (fldValorBaixa + fldBaixaDesconto) > 0";
			break;
			
			case "vencido":
				$filtro_status .= " HAVING tblcompra_parcela.fldValor > (fldValorBaixa + fldBaixaDesconto) OR COUNT(tblcompra_parcela_baixa.fldValor) < 1";
				$filtro  .=  "and tblcompra_parcela.fldVencimento < '".date("Y-m-d")."'";
			break;
				
			case "todos":
				$filtro_status .= "";
			break;
		}
	}
		
	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_fornecedor_parcela'] = $filtro;
		$_SESSION['filtro_fornecedor_parcela_status'] = $filtro_status;
	}
	elseif(isset($_POST['btn_limpar'])){
		$_SESSION['filtro_fornecedor_parcela'] = "";
		$_SESSION['filtro_fornecedor_parcela_status'] = "";
	}

?>
