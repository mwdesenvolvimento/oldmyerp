<?
function show_prog_bar($width, $percent, $type = 'green', $color = '#000') {
	$font =			'Tahoma';
	$font_size =		'8px';
	$font_weight =		'bold';	// bold, normal
	$imgs_folder =		'images/progress';

	// == Don't edit below ==
	$percent = min($percent, 100);
	$width -= 2;
	$result = (($percent*$width) / 100);
	$return = '';
	$return .= '<div name="progress">';
	$return .= '<div style="background: url('.$imgs_folder.'/progress.gif) no-repeat; height: 20px; width: 1px; display: block; float: left"><!-- --></div>';
	$return .= '<div style="background: url('.$imgs_folder.'/bg.gif); height: 20px; width: '.$width.'px; display: block; float: left">';

	$return .= '<span style="background: url('.$imgs_folder.'/on_'.strtolower($type).'.gif); display: block; float: left; width: '.$result.'px; height: 18px; margin: 1px 0; font-size: '.$font_size.'; font-family: \''.$font.'\'; line-height: 11px; font-weight: '.$font_weight.'; text-align: right; color: '.$color.'; letter-spacing: 1px;">&nbsp;'.$percent.'%&nbsp;</span>';

	$return .= '</div>';
	$return .= '<div style="background: url('.$imgs_folder.'/progress.gif) no-repeat; height: 20px; width: 1px; display: block; float: left"><!-- --></div>';
	$return .= '</div>';
	return $return;
}
?>