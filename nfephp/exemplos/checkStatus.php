<?php

$pubKey = 'http://www.mwdesenvolvimento.com.br/myerp/mw/nfephp/certs/04615918000104_pubKEY.pem';
$priKey = 'http://www.mwdesenvolvimento.com.br/myerp/mw/nfephp/certs/04615918000104_priKEY.pem';

$urlsefaz = 'https://nfe.fazenda.sp.gov.br/nfeweb/services/nfeconsulta2.asmx?wsdl';

//inicia comunicação com curl
$oCurl = curl_init();
curl_setopt($oCurl, CURLOPT_CONNECTTIMEOUT, 1000);
curl_setopt($oCurl, CURLOPT_URL, $urlsefaz.'');
curl_setopt($oCurl, CURLOPT_PORT , 443);
curl_setopt($oCurl, CURLOPT_VERBOSE, 1);
curl_setopt($oCurl, CURLOPT_HEADER, 1); //retorna o cabeçalho de resposta
curl_setopt($oCurl, CURLOPT_SSLVERSION, 3);
curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($oCurl, CURLOPT_SSLCERT, $pubKey);
curl_setopt($oCurl, CURLOPT_SSLKEY, $priKey);
curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
$__xml = curl_exec($oCurl);
$info = curl_getinfo($oCurl);

header('Content-type: text/html; charset=UTF-8');
echo '<PRE>';
echo htmlspecialchars($__xml);
echo '</PRE><BR>';
echo '<BR>';
print_r($info);

curl_close($oCurl);
?>