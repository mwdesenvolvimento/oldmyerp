
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
      	<title>myERP - Imprimir venda</title>
        
	</head>
	<body>
<?php
		ob_start();
		session_start();
		
		require_once("inc/con_db.php");
		require_once("inc/fnc_ibge.php");
		require_once("inc/fnc_general.php");
		require_once("inc/fnc_imprimir.php");
		require_once("inc/fnc_identificacao.php");
		$usuario_sessao = $_SESSION['usuario_id'];
		
		$impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
		if(!$impressao_local){
			$impressao_local = fnc_estacao_impressora('todos');
		}
		
		$desconto_porcent_exibir = fnc_sistema('pedido_desconto_porcent_impressao');
		$desconto_reais_exibir	 = fnc_sistema('pedido_desconto_reais_impressao');		
		$servico_terceiros_exibir= fnc_sistema('pedido_terceiros_impressao');
		
		$texto 		= $impressao_local." \r\n";
		$pedido_id  = $_GET['id'];
		$raiz 		= $_GET['raiz'];
		$data 		= date("Y-m-d");
		
		$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
		$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
		
		$rsPedido  			= mysql_query("SELECT tblpedido.*, SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) as fldTotalItem,
								tblpedido.fldVeiculo_Id, tblpedido.fldEndereco AS fldEnderecoPedido, tblpedido.fldReferencia,
								 tblpedido.fldPedidoData, tblpedido.fldServico, tblpedido.fldDependente_Id, tblpedido.fldDesconto, tblpedido.fldValor_Terceiros, 
								 tblpedido.fldId as fldPedidoId, tblpedido.fldDescontoReais, tblcliente.fldTipo, tblcliente.fldCPF_CNPJ, tblcliente.fldNome, tblcliente.fldTelefone1, tblcliente.fldId AS clienteId,
								 tblcliente.fldEndereco, tblcliente.fldNumero, tblcliente.fldBairro
								FROM tblpedido 
								LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
								INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								WHERE tblpedido.fldId = $pedido_id GROUP BY tblpedido_item.fldPedido_Id");
		$rowPedido 			= mysql_fetch_array($rsPedido);
		$cliente_id			= $rowPedido['clienteId'];
		/*----------------------------------------------------------------------------------------------*/
		$limite_carac = 56;
		$rsEmpresa  = mysql_query("SELECT * FROM tblempresa_info");
		$rowEmpresa = mysql_fetch_array($rsEmpresa);
		
		$municipio 	= fnc_ibge_municipio($rowEmpresa['fldMunicipio_Codigo']);
		$siglaUF	= fnc_ibge_uf_sigla($rowEmpresa['fldMunicipio_Codigo']);

		$endereco 	= $rowEmpresa['fldEndereco'];
		$numero 	= $rowEmpresa['fldNumero'];
		$bairro 	= $rowEmpresa['fldBairro'];
		$cidade 	= $rowMunicipio_Empresa['fldNome'];
		$uf 		= $rowMunicipio_Empresa['fldSigla'];
		$site		= $rowEmpresa['fldWebsite'];
		
		$texto .= format_margem_print(acentoRemover($rowEmpresa['fldNome_Fantasia']),$limite_carac,'esquerda')." \r\r\n";
		$texto .="Fone: ". format_margem_print($rowEmpresa['fldTelefone1'],26, 'esquerda').format_margem_print($rowEmpresa['fldTelefone2'],25, 'direita')." \r\n";
		$texto .= format_margem_print(substr($endereco." ".$numero." - ".$bairro,0,$limite_carac),$limite_carac, 'esquerda')." \r\n";
		$texto .="Data: ".format_date_out(date("Y-m-d"))." Hora: ".date("H:i:s")." \r\n\r\n";
		
		$texto .="Venda..: ".format_margem_print(str_pad($rowPedido['fldPedidoId'], 6, "0", STR_PAD_LEFT),8,'esquerda');
		$texto .="Data venda: ".format_margem_print(format_date_out($rowPedido['fldPedidoData']),14,'esquerda');
		$texto .="Ref: ".format_margem_print($rowPedido['fldReferencia'],10,'esquerda')." \r\n";
		$texto .="Cliente: ".format_margem_print(str_pad($rowPedido['clienteId'], 6, 0, STR_PAD_LEFT)."  ".acentoRemover($rowPedido['fldNome']),33,'esquerda');
		$texto .= format_margem_print($rowPedido['fldTelefone1'],10,'direita');
		if(fnc_sistema('pedido_rgcpf_cliente_impressao') > 0){
			
			$CPFCNPJ= formatCPFCNPJTipo_out($rowPedido['fldCPF_CNPJ'], $rowPedido['fldTipo']);
			$texto .= "\nCPF....: ".$CPFCNPJ."\n";
		}
		if(fnc_sistema('pedido_exibir_endereco') > 0){
			
			$texto .= "\r\nEnd....: ".format_margem_print(substr(acentoRemover($rowPedido['fldEndereco'])." ".$rowPedido['fldNumero']." - ".acentoRemover($rowPedido['fldBairro']),0,$limite_carac),$limite_carac, 'esquerda');
			
		}
		
		if($_SESSION['sistema_tipo'] == 'automotivo'){
			$rowPlaca = mysql_fetch_array(mysql_query("SELECT fldVeiculo,fldPlaca FROM tblcliente_veiculo WHERE fldId =".$rowPedido['fldVeiculo_Id']));
			$texto .="\r\nVeiculo: ".$rowPlaca['fldVeiculo']." ".$rowPlaca['fldPlaca'];
		}
		## PARCELAMENTO
		
		$rsParcelas = mysql_query('SELECT fldVencimento FROM tblpedido_parcela WHERE fldPedido_Id = '.$rowPedido['fldPedidoId']);
		$rows = mysql_num_rows($rsParcelas);
		if( $rows > 1 ){
			$parcelamento = $rows.' VEZES';
		}elseif($rows == 1){
			$rowParcelas = mysql_fetch_array($rsParcelas);
			$vencimento = $rowParcelas['fldVencimento'];
			$diferenca  = intervalo_data($rowPedido['fldPedidoData'], $vencimento);
			$parcelamento = $diferenca.' DIAS ';
		}
		$texto .= "\rPgto...: ".$parcelamento." \r\n";
		
		$texto .="\r\n\r\n";
		$texto .=format_margem_print("COD.",9, 'esquerda');
		$texto .=format_margem_print("ITEM",19, 'esquerda');
		$texto .=format_margem_print("UN",3, 'centro');
		$texto .=format_margem_print("QTDE",7, 'direita');
		$texto .=format_margem_print("VALOR",10, 'direita');
		$texto .=format_margem_print("TOTAL",10, 'direita');
		$texto .="\r\n=========================================================\r\n";
						
		$rsItem = mysql_query("SELECT tblpedido_item.*, tblproduto.fldCodigo, tblproduto_unidade_medida.fldSigla
							  FROM tblpedido_item 
							  INNER JOIN tblproduto ON tblpedido_item.fldProduto_Id = tblproduto.fldId
							  LEFT JOIN tblproduto_unidade_medida ON tblproduto.fldUN_Medida_Id = tblproduto_unidade_medida.fldId
							  WHERE tblpedido_item.fldPedido_Id = ".$rowPedido['fldPedidoId']);
		while($rowItem = mysql_fetch_array($rsItem)){
		
			$quantidade 	= $rowItem['fldQuantidade'];
			$valor 			= $rowItem['fldValor'];
			$total 			= $valor * $quantidade;
			$desconto 		= ($desconto_porcent_exibir == '1') ? $rowItem['fldDesconto'] : 0;
			$descontoItem 	= ($total * $desconto) / 100;
			$totalItem 		= $total - $descontoItem;
			$unidade_medida = $rowItem['fldSigla'];
			/*** gravando no txt ********************************************************/
			$texto.= format_margem_print(str_pad(substr($rowItem['fldCodigo'],0,8), 8,'0'),8)." ";
			$texto.= format_margem_print(substr(acentoRemover($rowItem['fldDescricao']),0, 18),18)." ";
			$texto.= format_margem_print(substr($unidade_medida,0,2),2)." ";
			$texto.= format_margem_print(format_number_out($quantidade,$quantidadeDecimal),6,'direita')." ";
			$texto.= format_margem_print(format_number_out($valor,$vendaDecimal),9,'direita')." ";
			$texto.= format_margem_print(format_number_out($total,$vendaDecimal),9,'direita')."\r\n";
			if($desconto_porcent_exibir == '1'){
				$texto.= format_margem_print(format_number_out($desconto),46,'direita')."% ";
				$texto.= format_margem_print(format_number_out($totalItem,$vendaDecimal),9,'direita')."\r\n";
			}
			$total_item += $totalItem;
		}
		
		
		
		$texto .="\r\n=========================================================\r\n";
		$texto .=format_margem_print("VALOR TOTAL DE PRODUTOS",30, 'esquerda');
		$texto .=format_margem_print(format_number_out($total_item),27, 'direita');
		$texto .="\r\n\r\n";
		/**SERVICOS ******************************************************************************************************************************************************/
		
		$texto .="\r\n";
		$texto .=format_margem_print("SERVICOS PRESTADOS",57, 'esquerda');
		$texto .="\r\n=========================================================\r\n";
		$texto .=format_margem_print("SERVICO",46, 'esquerda');
		$texto .=format_margem_print("VALOR",11, 'direita');
		$texto .="\r\n=========================================================\r\n";
		
		$sSQL		= "SELECT fldServico, fldValor FROM tblpedido_funcionario_servico WHERE fldPedido_Id = '$pedido_id' AND fldFuncao_Tipo = 2";
		$rsServico  = mysql_query($sSQL);
		while($rowServico = mysql_fetch_array($rsServico)){
			
			
			#PRA PODER SAIR A DESCRICAO COMPLETA DOS SERVICOS, VAI CORTANDO DE ACORDO COM O LIMITE DE CARACTERES
			//$servicoDescricao		= acentoRemover($rowServico['fldServico']);
			$limiteColunaServico 	= 46;
			$servicoExplode 		= explode("\r\n", acentoRemover($rowServico['fldServico']));
			
			if(count($servicoExplode) > 1){
				$n = 0;
				//unset($servicoDescricao);
				#ABAIXO VAI CONCATENANDO COM OS ESPACOS PRA PODER GERAR O TXT DEPOIS.
				while($servicoExplode[$n]){
					$total_carac = strlen($servicoExplode[$n]);
					
					$total_espaco = ceil($total_carac / $limiteColunaServico);
					$servicoDescricao .= str_pad($servicoExplode[$n],($limiteColunaServico*$total_espaco)," ", STR_PAD_RIGHT);
					
					$n++;
				}
			}else{
				$servicoDescricao = $rowServico['fldServico'];
			}
			
			$limiteLinhaServico		= ceil(strlen($servicoDescricao) / $limiteColunaServico);
			for($x = 0; $x <= $limiteLinhaServico; $x++){
				#AQUI DEVERIA PRINTAR O 'TESTE1 + OS ESPAÇOS EM BRANCO E ENTAO QUEBRAR LINHA, EM SEGUIDA O TESTE2  E ASSIM POR DIANTE
				#ACONTECE QUE APARENTEMENTE NAO ESTA CONTANDO TODOS OS ESPACOS EM BRANCO, POIS CONTA O TESTE1 TESTE2 E PARA, COMO SE TIVESSE TERMINADO OS CARACTERES.
				$texto .= substr(acentoRemover($servicoDescricao), $x * $limiteColunaServico, $limiteColunaServico);
				if($x == 0){
					$texto .=format_margem_print(format_number_out($rowServico['fldValor']),11, 'direita');	
				}
				$texto .="\r\n";
			}
			$total_servico += $rowServico['fldValor'];
		}
		
		$texto .="=========================================================\r\n";
		$texto .=format_margem_print("VALOR TOTAL DE SERVICOS",30, 'esquerda');
		$texto .=format_margem_print(format_number_out($total_servico),27, 'direita');
		$texto .="\r\n\r\n";
		
		/**PARCELAS ******************************************************************************************************************************************************/
		$sSQL = "SELECT tblpedido_parcela.fldId, SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor
				FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
				WHERE tblpedido_parcela.fldPedido_Id = $pedido_id GROUP BY tblpedido_parcela.fldPedido_Id";
		$rsBaixa = mysql_query($sSQL);
		
		$rowBaixa 				= mysql_fetch_array($rsBaixa);
		echo mysql_error();
		
		
		$total_terceiros		= ($servico_terceiros_exibir == '1') ? $rowPedido['fldValor_Terceiros'] : 0;		
		$total_pedido 			= $total_item + $total_servico + $total_terceiros;
		$desconto_pedido 		= ($desconto_porcent_exibir == '1') ? $rowPedido['fldDesconto'] : 0;
		$desconto_reais_pedido 	= ($desconto_reais_exibir == '1') 	? $rowPedido['fldDescontoReais'] : 0;
		$desconto 				= ($total_pedido * $desconto_pedido) / 100;
		$total_descontos 		= $desconto + $desconto_reais_pedido;
		$total_pedido_apagar 	= ($total_pedido - $total_descontos);
		
		$valorBaixa 			= $rowBaixa['fldBaixaValor'];
		$valorDevedor 			= $total_pedido_apagar - $rowBaixa['fldBaixaValor'];		
		
		
		$texto .="\r\n";
		if($desconto_porcent_exibir == '1' || $desconto_reais_exibir == '1'){
			$texto .=format_margem_print("DESCONTO VENDA",30, 'esquerda');
			$texto .=format_margem_print(format_number_out($total_descontos),27, 'direita')."\r\n";
		}
		
		if($servico_terceiros_exibir == '1'){
			$texto .=format_margem_print("SERVICOS TERCEIROS",30, 'esquerda');
			$texto .=format_margem_print(format_number_out($total_terceiros),27, 'direita')."\r\n";
		}
		$texto .=format_margem_print("TOTAL VENDA",30, 'esquerda');
		$texto .=format_margem_print(format_number_out($total_pedido_apagar),27, 'direita')."\r\n";
		$texto .=format_margem_print("TOTAL PAGO",30, 'esquerda');
		$texto .=format_margem_print(format_number_out($valorBaixa),27, 'direita')."\r\n";
		$texto .=format_margem_print("TOTAL DEVEDOR",30, 'esquerda');
		$texto .=format_margem_print(format_number_out($valorDevedor),27, 'direita')."\r\n";
		$texto .="\r\n\r\n";
		/*
		$data 	= date("d/m/Y");
		$rodape = strlen($municipio.", ".$data);

		$texto .= format_margem_print($municipio.", ".date("d/m/Y"),$limite_carac, 'centro')."\r\n\r\n\r\n";
		$texto .= format_margem_print("------------------------------", $limite_carac, 'centro')." \r\n";
		$texto .= format_margem_print(acentoRemover($rowPedido['fldNome']),$limite_carac,'centro')."\r\n\r\n\r\n";
		
		if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema_impressao_campo WHERE fldCampo = 'entregador' AND fldImpressao = 'venda' AND fldExibir = 1"))){
			$texto .= format_margem_print("------------------------------", $limite_carac, 'centro')." \r\n";
			$texto .= format_margem_print("ENTREGADOR", $limite_carac, 'centro')." \r\n";
		}
*/
		 
		#ASSINATURA DO CLIENTE #############################################################################################################################################
		
		$texto .= "\n\n-------------------------------\r\n";
		$texto .= acentoRemover($rowPedido['fldNome'])."\r\n\n\n";
		
		
		
				
		#APARECER DEVEDOR DE OUTRAS VENDAS  #####################################################################################################################
		if(fnc_sistema('pedido_exibir_devedor') > 0 ){
			
			$texto .= "OUTRAS PENDENCIAS";
			$texto .="\r\n=========================================================\r\n";

			$sSQL = "SELECT tblpedido_parcela.*, 
				SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa,
				SUM(tblpedido_parcela_baixa.fldJuros * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldJuros,
				SUM(tblpedido_parcela_baixa.fldMulta * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldMulta,
				SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldDesconto,
				tblpedido.fldId as fldPedidoId
				FROM 
				(tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id)
				INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
				WHERE tblpedido.fldCliente_Id = $cliente_id
				AND tblpedido_parcela.fldStatus = '1' 
				AND tblpedido_parcela.fldExcluido = '0'
				AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0) GROUP BY tblpedido_parcela.fldId";
			$rsParcela = mysql_query($sSQL);
			while($rowParcela = mysql_fetch_array($rsParcela)){	
				$valorBaixa = $rowParcela['fldValorBaixa'] + $rowParcela['fldJuros'] + $rowParcela['fldMulta'];
				
				$rsDevedor = mysql_query("SELECT fldDevedor FROM tblpedido_parcela_baixa WHERE fldParcela_Id = ".$rowParcela['fldId']." AND fldExcluido = 0 ORDER BY fldId desc LIMIT 1 ");
				$rowDevedor = mysql_fetch_array($rsDevedor);
				if(mysql_num_rows($rsDevedor)){
					$devedor = $rowDevedor['fldDevedor'];
				}else{
					$devedor = $rowParcela['fldValor'];
				}
				
				$pedido_id 	= format_number_out($rowParcela['fldPedidoId']);  
				$valor	 	= format_number_out($rowParcela['fldValor']); 
				
				$totalDevedor += $devedor;
				$pedido_id 	= str_pad($rowParcela['fldPedidoId'], 6, "0", STR_PAD_LEFT);
				
				if($devedor > 0){
					
					/*** gravando no txt ********************************************************/
					$rodape_devedor.= format_margem_print($pedido_id,6,'direita');
					$rodape_devedor.= format_margem_print(format_date_out($rowParcela['fldVencimento']),15,'centro');
					$rodape_devedor.= format_margem_print($valor,12, 'direita');
					$rodape_devedor.= format_margem_print(format_number_out($valorBaixa),12, 'direita');
					$rodape_devedor.= format_margem_print(format_number_out($devedor),12, 'direita')."\r\n";
					
				}
			}
			
			if($totalDevedor > 0 ){
				
				$texto .=format_margem_print("VENDA",6, 'esquerda');
				$texto .=format_margem_print("VENCIMENTO",15, 'centro');
				$texto .=format_margem_print("VALOR",12, 'direita');
				$texto .=format_margem_print("PAGO",12, 'direita');
				$texto .=format_margem_print("DEVEDOR",12, 'direita');
				$texto .="\r\n=========================================================\r\n";
				$texto	.=$rodape_devedor."\n\n\n\n";	
			}
		}
		
		
		
		# MENSAGENS #############################################################################################################################################
		$rsMensagem = mysql_query("SELECT * FROM tblsistema_impressao_mensagem WHERE fldImpressao = 'venda' ORDER BY fldOrdem");
		while($rowMensagem = mysql_fetch_array($rsMensagem)){
			$mensagem = strtoupper(acentoRemover($rowMensagem['fldMensagem']));
			$x = 0; //quebrando linha a cada 36 caracteres
			while(strlen(substr($mensagem,$x * 50, 50)) > 0){
				$texto .= format_margem_print(substr($mensagem,$x * 50, 50),$limite_carac, 'centro')." \r\n";
				$x +=1;
			}
		}
		
		if($_GET['np'] > 0){
			$texto .="\r\n\r\n\r\n";
		}else{
			$texto .="\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n";
		}
		#########################################################################################################################################################
		
		$timestamp  = date("Ymd_His");
		//$local_file = "impressao\inbox\imprimir_pedido_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		$local_file = "impressao///inbox///imprimir_pedido_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		$fp 		= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
		$salva		= fwrite($fp, $texto);
		fclose($fp);
	
		unset($total_item);
		if($_GET['np'] > 0){
			require('pedido_imprimir_np_nfiscal.php');
		}
?>    
	<script type="text/javascript">
		var local = '<?= $local_file ?>';
		//window.location=local;
		window.close();
	</script>
</html>	
