<div id="voltar">
    <p><a href="index.php?p=pedido">n&iacute;vel acima</a></p>
</div>	

<h2>Editar Venda</h2>

<div id="principal">
<?
	$usuario_id 	= $_SESSION['usuario_id'];
	$pedido_id 		= $_GET['id'];
	$impressao 		= fnc_sistema('sistema_impressao');
	$vendaDecimal 	= fnc_sistema('venda_casas_decimais');
	$qtdeDecimal 	= fnc_sistema('quantidade_casas_decimais');
	$servico_valor 	= fnc_sistema('venda_servico_valor');
	
	if(isset($impressao)){
		$rowImpressao 	= mysql_fetch_array(mysql_query("SELECT * FROM tblsistema_impressao WHERE fldId = $impressao"));
	
		$impressao 			= $rowImpressao['fldModelo'];
		$modelo_impressao	= $rowImpressao['fldModelo'];
		if($rowImpressao['fldVariacao'] != ''){
			$impressao .= "_".$rowImpressao['fldVariacao'];
		}
	}
	
	$rsPedido = mysql_query("SELECT tblpedido.*, tblcliente_veiculo.fldAno, tblcliente_veiculo.fldChassi FROM tblpedido
							LEFT JOIN tblcliente_veiculo ON tblpedido.fldVeiculo_Id = tblcliente_veiculo.fldId 
							WHERE tblpedido.fldId = $pedido_id AND fldExcluido = 0");
	//caso nao exista a venda ou tenha sido excluida
	echo mysql_error();
	if(!mysql_num_rows($rsPedido)){ ?>		
    	<div class="alert"><p class="erro">Houve um erro ao encontrar essa venda, ela pode ter sido exclu&iacute;da!<p></div>
<?		die;
	}
	
	$rowPedido = mysql_fetch_array($rsPedido);
	
	//a��es ###############################################################################################################################################################################################################################################################################################################################################################################################
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		$status_novo 		= $_POST['sel_status'];
		$status_venda 		= $rowPedido['fldStatus'];

		$Referencia 		= $_POST['txt_referencia'];
		$Dependente_Id 		= ($_POST['hid_dependente_id'] > 0 ) ? $_POST['hid_dependente_id'] : "NULL" ;
		$Cliente_Id 		= $_POST['hid_cliente_id'];
		$ContaBancaria_Id 	= $_POST['sel_conta_bancaria'];
		$Veiculo_Id			= $_POST['sel_veiculo']; //automotivo
		$Veiculo_Km			= $_POST['txt_veiculo_km']; //automotivo
		$Desconto			= format_number_in($_POST['txt_pedido_desconto']);
		$Desconto_Reais 	= format_number_in($_POST['txt_pedido_desconto_reais']);
		$Obs 				= mysql_escape_string($_POST['txt_pedido_obs']);
		$PedidoData 		= format_date_in($_POST['txt_pedido_data']);
		$EntregaData 		= format_date_in($_POST['txt_pedido_entrega']);
		$Retirado_Por 		= $_POST['txt_retirado_por'];

		$Endereco			= $_POST['txt_pedido_endereco'];
		$Marcador			= $_POST['sel_marcador'];
		$ClientePedido		= $_POST['txt_cliente_pedido'];
		$Outros_Servicos	= mysql_escape_string($_POST['txa_outros_servicos']);
		$Servicos_Terceiros	= format_number_in($_POST['txt_pedido_terceiros']);
		$Faturado			= ($_POST['chk_faturado'] == 'faturado') ? 1 : 0;
		
		$Codigo_Cliente  	= (isset($_POST['txt_codigo_pedido_cliente'])) ? $_POST['txt_codigo_pedido_cliente'] : "";

		$sqlUpdate_pedido 	= "UPDATE tblpedido SET
		fldCliente_Id 		= '$Cliente_Id',		fldDependente_Id	= $Dependente_Id,		
		fldReferencia 		= '$Referencia',		fldVeiculo_Id 		= '$Veiculo_Id',	
		fldVeiculo_Km 		= '$Veiculo_Km',		fldContaBancaria_Id = '$ContaBancaria_Id',	
		fldDesconto 		= '$Desconto',			fldDescontoReais 	= '$Desconto_Reais',	
		fldObservacao 		= '$Obs',				fldStatus 			= '$status_novo',		
		fldMarcador_Id		= '$Marcador',			fldPedidoData 		= '$PedidoData', 
		fldEndereco 		= '$Endereco',		
		fldEntregaData 		= '$EntregaData',		fldCliente_Pedido 	= '$ClientePedido',		
		fldServico			= '$Outros_Servicos',	fldValor_Terceiros	= '$Servicos_Terceiros',
		fldFaturado			= '$Faturado',			fldRetirado_Por 	= '$Retirado_Por',
		fldCodigo_Cliente 	= '$Codigo_Cliente'
		WHERE fldId 		= $pedido_id";
		
		#ARRAY PARA ARMAZENAR OS DADOS DE OTICA. OS CAMPOS DE MATERIAL E ARMACAO, ARMAZENAM A ORDEM DO ITEM NA LISTAGEM. MAIS PRA BAIXO VERIFICA A ORDEM E GRAVA O ID DO ITEM
		if($_SESSION['sistema_tipo'] == 'otica'){
			$otica_valores = array(
				'perto' => array(
					'armacao' 			=>	$_POST['hid_pedido_otica_perto_armacao'],
					'material'			=>	$_POST['hid_pedido_otica_perto_material'],	
					'OD' => array(
						'esferico' 		 => $_POST['txt_pedido_otica_perto_od_esferico'],
						'cilindrico'	 => $_POST['txt_pedido_otica_perto_od_cilindrico'],
						'eixo' 			 => $_POST['txt_pedido_otica_perto_od_eixo'],
						'DP'			 => $_POST['txt_pedido_otica_perto_od_dp']
					),
					'OE' => array(
						'esferico' 		 => $_POST['txt_pedido_otica_perto_oe_esferico'],
						'cilindrico'	 => $_POST['txt_pedido_otica_perto_oe_cilindrico'],
						'eixo' 			 => $_POST['txt_pedido_otica_perto_oe_eixo'],
						'DP'			 => $_POST['txt_pedido_otica_perto_oe_dp']
					)
				),
				'longe' => array(
					'armacao' 			=>	$_POST['hid_pedido_otica_longe_armacao'],
					'material'			=>	$_POST['hid_pedido_otica_longe_material'],	
					'OD' => array(
						'esferico' 		 => $_POST['txt_pedido_otica_longe_od_esferico'],
						'cilindrico'	 => $_POST['txt_pedido_otica_longe_od_cilindrico'],
						'eixo' 			 => $_POST['txt_pedido_otica_longe_od_eixo'],
						'DP'			 => $_POST['txt_pedido_otica_longe_od_dp']
					),
					'OE' => array(
						'esferico' 		 => $_POST['txt_pedido_otica_longe_oe_esferico'],
						'cilindrico'	 => $_POST['txt_pedido_otica_longe_oe_cilindrico'],
						'eixo' 			 => $_POST['txt_pedido_otica_longe_oe_eixo'],
						'DP'			 => $_POST['txt_pedido_otica_longe_oe_dp']
					)
				),
				'bifocal' => array(
					'marca' 			=>	$_POST['sel_otica_marca'],
					'lente'				=>	$_POST['sel_otica_lente'],	
					'OD' => array(
						'altura'		=>	$_POST['txt_pedido_otica_bifocal_od_altura'],
						'DNP'			=>	$_POST['txt_pedido_otica_bifocal_od_dnp'],
						'adicao'		=>	$_POST['txt_pedido_otica_bifocal_od_adicao']
					),
					'OE' => array(
						'altura'		=>	$_POST['txt_pedido_otica_bifocal_oe_altura'],
						'DNP'			=>	$_POST['txt_pedido_otica_bifocal_oe_dnp'],
						'adicao'		=>	$_POST['txt_pedido_otica_bifocal_oe_adicao']
					)
				)
			);
		}
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		if(mysql_query($sqlUpdate_pedido)){
			mysql_query("DELETE FROM tblpedido_funcionario_servico WHERE fldPedido_Id = $pedido_id");
			for($x=1; $x <= 3; $x++){
				$Funcionario_Id			= $_POST['sel_funcionario'.$x];
				if($Funcionario_Id > 0){
					
					$Funcionario_Servico	= mysql_escape_string($_POST['txa_funcionario'.$x.'_servico']);
					$Funcionario_Tempo		= format_number_in($_POST['txt_funcionario'.$x.'_tempo']);
					$Funcionario_Valor		= format_number_in($_POST['txt_funcionario'.$x.'_valor']);
					
					$rsComissao  			= mysql_query("SELECT fldFuncao2_Comissao FROM tblfuncionario WHERE fldId = $Funcionario_Id");
					$rowComissao 			= mysql_fetch_array($rsComissao);				
					$Comissao				= $rowComissao['fldFuncao2_Comissao'];
					
					$insertServico			= "INSERT INTO tblpedido_funcionario_servico (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldFuncao_Tipo, fldOrdem)
												VALUES ($pedido_id, $Funcionario_Id, '$Funcionario_Servico', $Funcionario_Tempo, $Funcionario_Valor, $Comissao, '2', $x)";
					mysql_query($insertServico) or mysql_error(die);
				}
			}
			
			if($status_novo != $status_venda){
				$data_atual = date("Y-m-d");
				$hora_atual = date("H:i:s");
				$sqlHistorico_Status = "INSERT INTO tblpedido_status_historico (fldPedido_Id, fldStatus_Id, fldData, fldHora, fldUsuario_Id) VALUES ('$pedido_id','$status_novo','$data_atual','$hora_atual','$usuario_id')";
				mysql_query($sqlHistorico_Status) or mysql_error(die);
			}
			
			$itens_id = '0,';
			$limite = $_POST["hid_controle"];
					
			for($n=1; $n <= $limite; $n++){
				
				$item_id			= $_POST['hid_pedido_item_id_'.$n];
				$produto_id 		= $_POST['hid_item_produto_id_'.$n];
				$item_prod_cod_cli 	= (isset($_POST['txt_item_codigo_cliente_'.$n])) ? $_POST['txt_item_codigo_cliente_'.$n] : "";
				$item_quantidade 	= format_number_in($_POST['txt_item_quantidade_'.$n]);
				$item_valor			= format_number_in($_POST['txt_item_valor_'.$n]);
				$item_desconto 		= format_number_in($_POST['txt_item_desconto_'.$n]);
				$item_nome 			= mysql_escape_string($_POST['txt_item_nome_'.$n]);
				$item_tabela_preco 	= $_POST['hid_item_tabela_preco_'.$n];
				$item_fardo	 		= $_POST['hid_item_fardo_'.$n];
				$item_estoque_id	= $_POST['hid_item_estoque_id_'.$n];
				$item_entregue		= ($_POST['chk_entregue_'.$n] == true) ? '1' : '0';
				$item_referencia	= $_POST['txt_item_referencia_'.$n];
				$item_lote			= (isset($_POST['txt_item_lote_'.$n])) ? $_POST['txt_item_lote_'.$n] : "";
				
				if(isset($produto_id)){
					echo mysql_error();
					if(isset($item_id)){

						$rsItem  = mysql_query("SELECT fldEntregueData, fldEntregue, fldQuantidade FROM tblpedido_item WHERE fldId = $item_id");
						$rowItem = mysql_fetch_array($rsItem);
						/*/pegar infos antigas para historico
						$dadosAntigos = mysql_fetch_assoc(mysql_query("select * from tblpedido_item left join tblproduto on tblpedido_item.fldProduto_Id = tblproduto.fldId where tblpedido_item.fldId = $item_id "));
						if($dadosAntigos['fldValor'] != $item_valor){
							$desc = "Alterado o valor do produto de c�digo: {$dadosAntigos['fldCodigo']}";
							mysql_query("INSERT INTO tblpedido_historico (fldPedido_Id, fldDescricao, fldUsuario_Id, fldData) VALUES ('$pedido_id', '$desc', '$usuario_id', '".time()."')") or die('ok1');
						}

						if($dadosAntigos['fldQuantidade'] != $item_quantidade){
							$desc = "Alterado a quantidade do produto de c�digo: {$dadosAntigos['fldCodigo']}";
							mysql_query("INSERT INTO tblpedido_historico (fldPedido_Id, fldDescricao, fldUsuario_Id, fldData) VALUES ('$pedido_id', '$desc', '$usuario_id', '".time()."')") or die('ok2');
						}*/
						
						#EDITAR VALOR E	QUANTIDADE						
						mysql_query("UPDATE tblpedido_item SET fldValor = '$item_valor', fldQuantidade = '$item_quantidade' WHERE fldId = $item_id");


						if(fnc_sistema('pedido_comissao') == '2'){
							$item_valor_comissao 	= $item_valor * $item_quantidade;
							$func_id				= $_POST['txt_funcionario_codigo'];
							$rsDadosComissao 		= mysql_query("SELECT * FROM tblproduto_comissao WHERE fldProduto_Id = '$produto_id' AND fldFuncionario_Id = '$func_id'");
							$rowComissao_Produto 	= mysql_fetch_assoc($rsDadosComissao);
							$comissao_porc 			= ($rowComissao_Produto['fldComissao_Tipo'] == '2') ? $rowComissao_Produto['fldComissao_Valor'] : '';
							$comissao_reais			= ($rowComissao_Produto['fldComissao_Tipo'] == '1') ? $rowComissao_Produto['fldComissao_Valor'] : '';
							mysql_query("INSERT INTO tblpedido_funcionario_servico 
													  (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldComissao_Reais, fldFuncao_Tipo)
													  VALUES ('$pedido_id', '$func_id', '', '', '$item_valor_comissao', '$comissao_porc', '$comissao_reais', '1')");
						}
						
						#CASO TEHA ALTERADO O STATUS DE ENTREGUE DO ITEM
						//checar se j� existe data de entregue gravada no banco de dados
						if($item_entregue == '1' && empty($rowItem['fldEntregueData'])){ $item_entregue_data = "'".date('Y-m-d')."'"; }
						elseif($item_entregue == '0'){ $item_entregue_data = "NULL"; }
						else{ $item_entregue_data = "'" . $rowItem['fldEntregueData'] . "'"; }
						$bd_entregue = $rowItem['fldEntregue']; //o entregue antigo
						mysql_query("UPDATE tblpedido_item SET fldEntregue = '$item_entregue', fldEntregueData = $item_entregue_data, fldExibicaoOrdem = '0' WHERE fldId = '$item_id'");
						if($bd_entregue == 0 and $item_entregue == 1){
							//se n�o estava entregue, e agora est�, insiro a movimenta��o
							//caso nao estava entregue anteriormente, significa que nao tinha nenhum registro de estoque.
							fnc_estoque_movimento_lancar($produto_id, '', '',  $item_quantidade, 1, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
						}
						
						else if ($bd_entregue == 1 and $item_entregue == 0){
							//se estava entregue, e foi removido o entregue, exclui da movimenta��o
							//pq pode ter sido um clique errado, algo do tipo...
							//automaticamente vai excluir os componentes!
							mysql_query("DELETE FROM tblproduto_estoque_movimento WHERE fldItem_Id = '$item_id'");
						}
						
						else if ($bd_entregue == 1 and $item_entregue == 1){
							//aqui verifico se foi alterado a quantidade, caso tenha alterado, insiro a diferen�a
							$rowEstoque		= mysql_fetch_assoc(mysql_query("SELECT SUM((fldEntrada - fldSaida) * -1) as fldQuantidade 
   											FROM tblproduto_estoque_movimento WHERE fldProduto_Id = '$produto_id' AND fldItem_Id = '$item_id'")); //onde nao � componente
							$estoque_qtd 	= $rowEstoque['fldQuantidade'];
							if($estoque_qtd != $item_quantidade){
								if($estoque_qtd > $item_quantidade){
									//excluiu alguns itens... (EX: trocou qtd de 10 para 5)
									$diferenca = $rowEstoque['fldQuantidade'] - $item_quantidade;
									fnc_estoque_movimento_lancar($produto_id, '', $diferenca,  '', 11, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
								}
								else if ($estoque_qtd < $item_quantidade) {
									//inseriu mais itens... (EX: trocou de 5 para 10)
									$diferenca = $item_quantidade - $rowEstoque['fldQuantidade'];
									fnc_estoque_movimento_lancar($produto_id, '', '',  $diferenca, 1, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
								}
							}
						}
						

						//exclui os dados dos componentes deste item e recadastra independente se � entregue ou �
						mysql_query("DELETE FROM tblpedido_item_componente WHERE fldItem_Id = '$item_id'");
						$componentes = array(); //$componente[0] = array("id" => X, "qtd" => X);
						############################################################################################################################################################################################					
						//agora tem 2 tipos de relacionamento, pesquiso por um, dps por outro!
						//primeiro o comum
						$sqlComponente = mysql_query("SELECT tblproduto_componente.* FROM tblproduto_componente WHERE fldProduto_Id = $produto_id");
						if(mysql_num_rows($sqlComponente) > 0){ //normal
							while($rowComponente = mysql_fetch_assoc($sqlComponente)){
								//insere todos os componentes, al�m de mexer no estoque!
								$componente_id		= $rowComponente['fldProduto_Componente_Id'];
								$componente_qtd		= $rowComponente['fldComponente_Qtd'] * $item_quantidade;
	
								//insert na tblpedido_item_componente
								mysql_query("INSERT INTO tblpedido_item_componente
											(fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Id, fldQtd, fldExcluido)
											VALUES ('$produto_id', '$item_id', '$pedido_id', '$componente_id', '$componente_qtd', '0')") or die(mysql_error());
								array_push($componentes, array("id" => $componente_id, "qtd" => $componente_qtd));
							}
						}
						
						##############################################################################################################################################################################################
						//agora inverso
						$sqlComponente = mysql_query("SELECT tblproduto_componente.* FROM tblproduto_componente WHERE fldProduto_Componente_Id = $produto_id AND fldRelacionamento = 2");
						if(mysql_num_rows($sqlComponente) > 0){ //inverso
							while($rowComponente = mysql_fetch_assoc($sqlComponente)){
								//insere todos os componentes, al�m de mexer no estoque!
								$componente_id		= $rowComponente['fldProduto_Id'];
								$componente_qtd		= $rowComponente['fldQtd_Proporcional'] * $item_quantidade;
								//insert na tblpedido_item_componente
								mysql_query("INSERT INTO tblpedido_item_componente
											(fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Id, fldQtd, fldExcluido)
											VALUES ('$produto_id', '$item_id', '$pedido_id', '$componente_id', '$componente_qtd', '0')") or die(mysql_error());
								array_push($componentes, array("id" => $componente_id, "qtd" => $componente_qtd));
							}
						}
						######################### FIM DO COMPONENTE ##################################################################################################################################################
						//� necess�rio fazer as verificacoes como o do produto, pq as vezes o produto � controla estoque, e o componente sim, a� d� problema... por isso nao fa�o junto com o produto
						if($bd_entregue == 0 and $item_entregue == 1){
							//se n�o estava entregue, e agora est�, insiro a movimenta��o
							//caso nao estava entregue anteriormente, significa que nao tinha nenhum registro de estoque.
							foreach($componentes as $componente){
								$componente_id 	= $componente['id'];
								$componente_qtd = $componente['qtd'];
								fnc_estoque_movimento_lancar($componente_id, '', '', $componente_qtd, 12, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
							}
						}
						
						else if ($bd_entregue == 1 and $item_entregue == 1){
							//aqui verifico se foi alterado a quantidade, caso tenha alterado, insiro a diferen�a
							foreach($componentes as $componente){
								$componente_id 	= $componente['id'];
								$componente_qtd = $componente['qtd'];
								$rowEstoque 	= mysql_fetch_assoc(mysql_query("SELECT SUM((fldEntrada - fldSaida) * -1) as fldQuantidade FROM tblproduto_estoque_movimento 
													WHERE fldProduto_Id = '$componente_id' AND fldItem_Id = '$item_id'")); //onde nao � componente
								$estoque_qtd 	= $rowEstoque['fldQuantidade'];
								if($estoque_qtd != $componente_qtd){
									if($estoque_qtd > $componente_qtd){
										//excluiu alguns itens... (EX: trocou qtd de 10 para 5)
										$diferenca = $rowEstoque['fldQuantidade'] - $componente_qtd;
										fnc_estoque_movimento_lancar($componente_id, '', $diferenca, '', 13, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
									}
									else if ($estoque_qtd < $componente_qtd) {
										//inseriu mais itens... (EX: trocou de 5 para 10)
										$diferenca = $componente_qtd - $rowEstoque['fldQuantidade'];
										fnc_estoque_movimento_lancar($componente_id, '', '', $diferenca, 12, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
									}
								}
							}
						}
						
					}else{
						//item novo!
						$item_entregue_data = ($item_entregue == '1') ? "'".date('Y-m-d')."'" : "NULL";
						$rsDadosProduto		= mysql_query("SELECT fldEstoque_Controle, fldTipo_Id, fldValorCompra FROM tblproduto WHERE fldId = '$produto_id'");
						$rowDadosProduto 	= mysql_fetch_array($rsDadosProduto);
						$valor_compra 		= $rowDadosProduto['fldValorCompra'];
						$estoque_controle	= $rowDadosProduto['fldEstoque_Controle'];
						$tipo_id 			= $rowDadosProduto['fldTipo_Id'];

						if(fnc_sistema('pedido_comissao') == '2'){
							$item_valor_comissao 	= $item_valor * $item_quantidade;
							$func_id				= $_POST['txt_funcionario_codigo'];
							$rsDadosComissao 		= mysql_query("SELECT * FROM tblproduto_comissao WHERE fldProduto_Id = '$produto_id' AND fldFuncionario_Id = '$func_id'");
							$count_comissao			= mysql_num_rows($rsDadosComissao);

							if($count_comissao > 0){
								$rowComissao_Produto 	= mysql_fetch_assoc($rsDadosComissao);
								$comissao_porc 			= ($rowComissao_Produto['fldComissao_Tipo'] == '2') ? $rowComissao_Produto['fldComissao_Valor'] : '';
								$comissao_reais			= ($rowComissao_Produto['fldComissao_Tipo'] == '1') ? $rowComissao_Produto['fldComissao_Valor'] : '';
								mysql_query("INSERT INTO tblpedido_funcionario_servico 
														  (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldComissao_Reais, fldFuncao_Tipo)
														  VALUES ('$pedido_id', '$func_id', '', '', '$item_valor_comissao', '$comissao_porc', '$comissao_reais', '1')");	
							}
						}
						
						$sql = "INSERT INTO tblpedido_item
						(fldProduto_Id, fldPedido_Id, fldQuantidade, fldValor, fldValor_Compra, fldDesconto, fldDescricao, fldFardo_Id, fldTabela_Preco_Id,
						fldEstoque_Id, fldProduto_Tipo_Id, fldReferencia, fldLote, fldProduto_Codigo_Cliente, fldEntregue, fldEntregueData)
						VALUES(
							'$produto_id',
							'$pedido_id',
							'$item_quantidade',
							'$item_valor',
							'$valor_compra',
							'$item_desconto',
							'$item_nome',
							'$item_fardo',
							'$item_tabela_preco',
							'$item_estoque_id',
							'$tipo_id',
							'$item_referencia',
							'$item_lote',
							'$item_prod_cod_cli',
							'$item_entregue',
							$item_entregue_data)";
						
						if(mysql_query($sql)){
							$LastId 	= mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID "));
							$item_id 	= $LastId['lastID'];
							
							if($item_entregue == 1){ fnc_estoque_movimento_lancar($produto_id, '', '', $item_quantidade, 1, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id); }

							############################################################################################################################################################################################					
							//agora tem 2 tipos de relacionamento, pesquiso por um, dps por outro!
							//primeiro o comum
							$sqlComponente = mysql_query("SELECT 
															tblproduto_componente.*
														FROM tblproduto_componente 
													 WHERE fldProduto_Id = $produto_id");
							if(mysql_num_rows($sqlComponente) > 0){ //normal
								while($rowComponente = mysql_fetch_assoc($sqlComponente)){
									//insere todos os componentes, al�m de mexer no estoque!
									$componente_id		= $rowComponente['fldProduto_Componente_Id'];
									$componente_qtd		= $rowComponente['fldComponente_Qtd'] * $item_quantidade;
		
									//insert na tblpedido_item_componente
									mysql_query("INSERT INTO tblpedido_item_componente
												(fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Id, fldQtd, fldExcluido)
												VALUES ('$produto_id', '$item_id', '$pedido_id', '$componente_id', '$componente_qtd', '0')") or die(mysql_error());
												
									if($item_entregue == 1){ fnc_estoque_movimento_lancar($componente_id, '', '', $componente_qtd, 12, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id); }
								}
							}
							
							##############################################################################################################################################################################################
							//agora inverso
							$sqlComponente = mysql_query("SELECT tblproduto_componente.* FROM tblproduto_componente WHERE fldProduto_Componente_Id = $produto_id AND fldRelacionamento = 2");
							if(mysql_num_rows($sqlComponente) > 0){ //inverso
								while($rowComponente = mysql_fetch_assoc($sqlComponente)){
									//insere todos os componentes, al�m de mexer no estoque!
									$componente_id		= $rowComponente['fldProduto_Id'];
									$componente_qtd		= $rowComponente['fldQtd_Proporcional'] * $item_quantidade;
									//insert na tblpedido_item_componente
									mysql_query("INSERT INTO tblpedido_item_componente
												(fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Id, fldQtd, fldExcluido)
												VALUES ('$produto_id', '$item_id', '$pedido_id', '$componente_id', '$componente_qtd', '0')") or die(mysql_error());
												
									if($item_entregue == 1){ fnc_estoque_movimento_lancar($componente_id, '', '', $componente_qtd, 12, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id); }
								}
							}
							######################### FIM DO COMPONENTE ##################################################################################################################################################
						}else{
							echo mysql_error();
							#'erro ao inserir item';
							die();
						}
					}

					if($otica_valores['perto']['armacao'] == $n){
						$Otica_Perto_Armacao = $item_id;
					}
					if($otica_valores['perto']['material'] == $n){
						$Otica_Perto_Material = $item_id;
					}
					if($otica_valores['longe']['armacao'] == $n){
						$Otica_Longe_Armacao = $item_id;
					}
					if($otica_valores['longe']['material'] == $n){
						$Otica_Longe_Material = $item_id;
					}

					$itens_id .= (isset($item_id)) ? $item_id.',' :'';
				}

				$sub_total		= $item_valor * $item_quantidade;
				$total_desconto	= ($item_desconto / 100) * $sub_total;
				$total_item		+= $sub_total - $total_desconto;
			}
		}

		//INSERIR FUNCIONARIO DE VENDA - TOTAL DE ITENS PARA ARMAZENAR JUNTO
		if(fnc_sistema('pedido_comissao') != '2'){
			$Funcionario_Id		= $_POST['txt_funcionario_codigo'];
			if($Funcionario_Id > 0){
				$Funcionario_Valor		= $total_item;

				$rsComissao  			= mysql_query("SELECT fldFuncao1_Comissao FROM tblfuncionario WHERE fldId = $Funcionario_Id");
				$rowComissao 			= mysql_fetch_array($rsComissao);				
				$Comissao				= $rowComissao['fldFuncao1_Comissao'];
				$insertServico			= "INSERT INTO tblpedido_funcionario_servico (fldPedido_Id, fldFuncionario_Id, fldValor, fldComissao, fldFuncao_Tipo)
											VALUES ($pedido_id, $Funcionario_Id, '$Funcionario_Valor', '$Comissao', '1')";

				mysql_query($insertServico);
				echo mysql_error();
			}
		}
		
		#CONFIRO SE AINDA EXISTE NO BANCO ALGUM ITEM DESSA VENDA, MAS QUE NAO EXISTE NA LISTAGEM => ITEM EXCLUIDO NA EDI��O
		$itens_id 	 =  substr($itens_id, 0, strlen($itens_id) -1);
		$rsExcluidos = mysql_query("SELECT * FROM tblpedido_item WHERE fldId NOT IN ($itens_id) AND fldPedido_Id = '$pedido_id' AND fldExcluido = '0'");
		while($rowExcluidos  = mysql_fetch_array($rsExcluidos)){
			$item_id		 = $rowExcluidos['fldId'];
			$item_estoque_id = $rowExcluidos['fldEstoque_Id'];
			$item_quantidade = $rowExcluidos['fldQuantidade'];
			$produto_id 	 = $rowExcluidos['fldProduto_Id'];
			fnc_estoque_movimento_lancar($produto_id, '', $item_quantidade,'', 11, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
			mysql_query("UPDATE tblpedido_item SET fldExcluido = '1' WHERE fldId = ".$rowExcluidos['fldId']);
			/** DELETA OS COMPONENTES DOS PRODUTOS EXCLUIDOS **/
			$rowComponente_Excluido = mysql_query("SELECT * FROM tblpedido_item_componente WHERE fldItem_Id = $item_id");
			if(mysql_num_rows($rowComponente_Excluido)){
				$rowComponente_Excluido = mysql_fetch_assoc($rowComponente_Excluido);
				$componente_id 			= $rowComponente_Excluido['fldComponente_Id'];
				$componente_qtd 		= $rowComponente_Excluido['fldQtd'];
				fnc_estoque_movimento_lancar($componente_id, '', $componente_qtd, '', 13, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
			}
		}
		
		//Inser��o dos dados recebidos do Pedido (�tica)
		if($_SESSION["sistema_tipo"] == "otica"){
			//excluir todos os parametros de otica antigos
			mysql_query("DELETE FROM tblpedido_otica WHERE fldPedido_Id = $pedido_id");
		
			$inserir_dados_otica = mysql_query("
			INSERT INTO tblpedido_otica (
			fldPedido_Id,				fldResponsavel,
			fldPerto_Armacao_Item_Id,	fldPerto_Material_Item_Id,
			fldPerto_OD_Esferico,		fldPerto_OD_Cilindro,
			fldPerto_OD_Eixo,			fldPerto_OD_DP,
			fldPerto_OE_Esferico,		fldPerto_OE_Cilindro,
			fldPerto_OE_Eixo,			fldPerto_OE_DP,
			fldLonge_Armacao_Item_Id,	fldLonge_Material_Item_Id,
			fldLonge_OD_Esferico,		fldLonge_OD_Cilindro,
			fldLonge_OD_Eixo,			fldLonge_OD_DP,
			fldLonge_OE_Esferico,		fldLonge_OE_Cilindro,
			fldLonge_OE_Eixo,			fldLonge_OE_DP,
			fldBifocal_Marca_Id,		fldBifocal_Lente_Id,
			fldBifocal_OD_Altura,		fldBifocal_OD_DNP,
			fldBifocal_OD_Adicao,		fldBifocal_OE_Altura,
			fldBifocal_OE_DNP,			fldBifocal_OE_Adicao,
			fldMedico,					fldMedico_crm
			)VALUES (
			'".$pedido_id."',
			'".$_POST['sel_cliente_responsavel']."',
			'".$Otica_Perto_Armacao."',
			'".$Otica_Perto_Material."',
			'".$otica_valores['perto']['OD']['esferico']."',
			'".$otica_valores['perto']['OD']['cilindrico']."',
			'".$otica_valores['perto']['OD']['eixo']."',
			'".$otica_valores['perto']['OD']['DP']."',
			'".$otica_valores['perto']['OE']['esferico']."',
			'".$otica_valores['perto']['OE']['cilindrico']."',
			'".$otica_valores['perto']['OE']['eixo']."',
			'".$otica_valores['perto']['OE']['DP']."',
			'".$Otica_Longe_Armacao."',
			'".$Otica_Longe_Material."',
			'".$otica_valores['longe']['OD']['esferico']."',
			'".$otica_valores['longe']['OD']['cilindrico']."',
			'".$otica_valores['longe']['OD']['eixo']."',
			'".$otica_valores['longe']['OD']['DP']."',
			'".$otica_valores['longe']['OE']['esferico']."',
			'".$otica_valores['longe']['OE']['cilindrico']."',
			'".$otica_valores['longe']['OE']['eixo']."',
			'".$otica_valores['longe']['OE']['DP']."',
			'".$otica_valores['bifocal']['marca']."',
			'".$otica_valores['bifocal']['lente']."',
			'".$otica_valores['bifocal']['OD']['altura']."',
			'".$otica_valores['bifocal']['OD']['DNP']."',
			'".$otica_valores['bifocal']['OD']['adicao']."',
			'".$otica_valores['bifocal']['OE']['altura']."',
			'".$otica_valores['bifocal']['OE']['DNP']."',
			'".$otica_valores['bifocal']['OE']['adicao']."',
			'".$_POST['txt_pedido_otica_medico']."',
			'".$_POST['txt_pedido_otica_medico_crm']."'
			)") or die (mysql_error());
		}
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//atualizando parcelas na tabela
		//DEFINIR O STATUS DE NOVAS PARCELAS SE O PEDIDO N�O ESTIVER EM OR�AMENTO OU RECUSADO/*
		($status_novo > 1 && $status_novo < 5) ? $status = 1 : $status = 0;
		
		//checando caso haja parcela(s) pagas
		if(isset($_POST['hid_parcelas_pagas_ids'])) $deleteSQL = "DELETE FROM tblpedido_parcela WHERE fldPedido_Id = $pedido_id AND fldId NOT IN(". $_POST['hid_parcelas_pagas_ids'] .")";
		else $deleteSQL = "DELETE FROM tblpedido_parcela WHERE fldPedido_Id = $pedido_id";
		
		mysql_query($deleteSQL);
		
		$y = 1;
		$limite = $_POST["hid_controle_parcela"];
		while($y <= $limite){
			if(isset($_POST['txt_parcela_numero_'.$y])){
				$parcela_numero 	= $_POST['txt_parcela_numero_'.$y];
				$parcela_vencimento = format_date_in($_POST['txt_parcela_data_'.$y]);
				$parcela_valor 		= format_number_in($_POST['txt_parcela_valor_'.$y]);
				$pagamento_tipo 	= $_POST['sel_pagamento_tipo_'.$y];
				$parcela_obs		= mysql_escape_string($_POST['txt_pedido_obs']);
				
				mysql_query ("INSERT INTO tblpedido_parcela
				(fldPedido_Id, 			fldParcela, 
				 fldVencimento,			fldValor, 
				 fldPagamento_Id, 		fldObservacao, 
				 fldStatus
				 )VALUES(
				'$pedido_id',			'$parcela_numero',
				'$parcela_vencimento',	'$parcela_valor',
				'$pagamento_tipo',		'$parcela_obs',
				'$status'
				)");
				
				$LastId = mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID FROM tblpedido_parcela"));
				$Data 	= ("Y-m-d");
				//ACOES DE PAGAMENTO CONFORME TIPO 
				$rowPagamentoTipo 	= mysql_fetch_array(mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldId = $pagamento_tipo"));
				$sigla 				= $rowPagamentoTipo['fldSigla'];
				echo mysql_error();

				$acao = fnc_sistema("pedido_parcela_acao_$sigla");
				//verifica se eh a vista, se for substitui o $acao
				if($Data == $parcela_vencimento){
					$acao = fnc_sistema("pedido_parcela_acao_AV");
				}
					
				if($acao == 2){ 
					$insertBaixa = mysql_query("INSERT INTO tblpedido_parcela_baixa
						(fldParcela_Id, fldDataCadastro, fldValor, fldDataRecebido)
						VALUES(
						'".$LastId['lastID']."',
						'$Data',
						'$parcela_valor',
						'$Data'
					)");
					
					if($insertBaixa && $status == 1){// se nao for orcamento
						//lan�ar no caixa
						$conta = fnc_sistema("pedido_parcela_conta_$sigla");
						$LastIdBaixa = mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID"));
						//$descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta
						fnc_financeiro_conta_fluxo_lancar('', $parcela_valor, 0, '', $pagamento_tipo, $LastIdBaixa['lastID'], 3, '', $conta);
					}
				}
			}
			$y += 1;			
		}
		
		//cheques----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		if(!empty($_SESSION['ref_timestamp'])){ //CASO A SESSAO ESTIVER EM BRANCO, PRA NAO ATUALIZAR TODOS OS CHEQUES 
			/*
			$origem_movimento_id = '3'; //recebimento de parcela
			$timestamp			= $_SESSION['ref_timestamp'];
			$origem_id 			= $pedido_id;
			fnc_cheque_update($origem_id, '', '', $origem_movimento_id, '', $timestamp);
			*/
			###############################################################################################################################################
			
			$timestamp	 = $_SESSION['ref_timestamp'];
			$movimento_id= '3'; //recebimento de parcela
			$registro_id = $pedido_id;
			$conta_id	 = $conta; //JA DEFINIDO NAS PARCELAS ACIMA
			fnc_cheque_movimento_lancar($timestamp, $movimento_id, $registro_id, $conta_id);
		}
		unset($_SESSION['ref_timestamp']);
		if(!mysql_error()){
			header("location:index.php?p=pedido&mensagem=ok");
		}
		else {
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
				<a class="voltar" href="index.php?p=pedido">voltar</a>
			</div>
<?			echo mysql_error();
		}
	}else{
		
		$remote_ip 		= gethostbyname($REMOTE_ADDR);
		$_SESSION['ref_timestamp'] = $remote_ip.date("YmdHis");
	
        $rowUsuario = mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId = ".$rowPedido['fldUsuario_Id']));

		//se financeira
		if($_SESSION['sistema_tipo'] == 'financeira' && $rowPedido['fldContaBancaria_Id'] != NULL){
			$rsBanco = mysql_query("SELECT tblcliente_contabancaria.*, tblcliente_contabancaria.fldId as fldContaId, tblbanco_codigo.fldBanco 
								   FROM tblcliente_contabancaria INNER JOIN tblbanco_codigo ON tblcliente_contabancaria.fldBanco_Numero = tblbanco_codigo.fldNumero
								   WHERE tblcliente_contabancaria.fldId =".$rowPedido['fldContaBancaria_Id']);
			$rowBanco = mysql_fetch_array($rsBanco);
		}

		//FAZ OUTRA ARRAY COM OS VALORES QUE PEGOU DO BANCO
		if($_SESSION["sistema_tipo"] == "otica"){
	
			$rowOtica = mysql_fetch_array(mysql_query("SELECT * FROM tblpedido_otica WHERE fldPedido_Id = $pedido_id"));
			$otica_retorno = array(
				'perto' => array(
					'armacao' 			=>	$rowOtica['fldPerto_Armacao_Item_Id'],
					'material'			=>	$rowOtica['fldPerto_Material_Item_Id'],	
					'OD' => array(
						'esferico' 		 => $rowOtica['fldPerto_OD_Esferico'],
						'cilindrico'	 => $rowOtica['fldPerto_OD_Cilindro'],
						'eixo' 			 => $rowOtica['fldPerto_OD_Eixo'],
						'DP'			 => $rowOtica['fldPerto_OD_DP']
					),
					'OE' => array(
						'esferico' 		 => $rowOtica['fldPerto_OE_Esferico'],
						'cilindrico'	 => $rowOtica['fldPerto_OE_Cilindro'],
						'eixo' 			 => $rowOtica['fldPerto_OE_Eixo'],
						'DP'			 => $rowOtica['fldPerto_OE_DP']
					)
				),
				'longe' => array(
					'armacao' 			=>	$rowOtica['fldLonge_Armacao_Item_Id'],
					'material'			=>	$rowOtica['fldLonge_Material_Item_Id'],	
					'OD' => array(
						'esferico' 		 => $rowOtica['fldLonge_OD_Esferico'],
						'cilindrico'	 => $rowOtica['fldLonge_OD_Cilindro'],
						'eixo' 			 => $rowOtica['fldLonge_OD_Eixo'],
						'DP'			 => $rowOtica['fldLonge_OD_DP']
					),
					'OE' => array(
						'esferico' 		 => $rowOtica['fldLonge_OE_Esferico'],
						'cilindrico'	 => $rowOtica['fldLonge_OE_Cilindro'],
						'eixo' 			 => $rowOtica['fldLonge_OE_Eixo'],
						'DP'			 => $rowOtica['fldLonge_OE_DP']
					)
				),
				'bifocal' => array(
					'marca' 			=>	$rowOtica['fldBifocal_Marca_Id'],
					'lente'				=>	$rowOtica['fldBifocal_Lente_Id'],	
					'OD' => array(
						'altura'		=>	$rowOtica['fldBifocal_OD_Altura'],
						'DNP'			=>	$rowOtica['fldBifocal_OD_DNP'],
						'adicao'		=>	$rowOtica['fldBifocal_OD_Adicao']
					),
					'OE' => array(
						'altura'		=>	$rowOtica['fldBifocal_OE_Altura'],
						'DNP'			=>	$rowOtica['fldBifocal_OE_DNP'],
						'adicao'		=>	$rowOtica['fldBifocal_OE_Adicao']
					)
				)
			);
		}
?>
		<ul class="header_bar">
			<li><a class="print_carne" 	href="pedido_imprimir_carne_A4.php?id=<?=$pedido_id?>"			 		rel="externo" title="imprimir carn&ecirc;"></a></li>
			<? if($modelo_impressao == 'A4') { ?>
			<li><a class="print_os" 	href="pedido_imprimir_ordem_servico_A4.php?id=<?=$pedido_id?>" 		rel="externo" title="imprimir OS"></a></li>
			<? }else{ ?>
			<li><a class="print_os" 	href="pedido_imprimir_ordem_servico.php?id=<?=$pedido_id?>" 		rel="externo" title="imprimir OS"></a></li>
			<? } ?>
			<li><a class="print" 		href="pedido_imprimir_<?=$impressao?>.php?id=<?=$pedido_id?>" 		rel="externo" title="imprimir venda"></a></li>
            <li><a class="print_np" 	href="pedido_imprimir_np_<?=$impressao?>.php?id=<?=$pedido_id?>" 	rel="externo" title="imprimir nota promiss&oacute;ria"></a></li>
<?
			$sql = "SELECT * FROM tblpedido_parcela INNER JOIN tblpagamento_tipo
					ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
					WHERE fldPedido_Id = $pedido_id and fldSigla = 'BCR'";
			$rsBoletoCR = mysql_query($sql);
			if(mysql_num_rows($rsBoletoCR)){
?>				<li><a class="btn_print_barcode modal"  rel="400-380" href="pedido_imprimir_boleto_cr,<?=$pedido_id?>" title="boleto CR"></a></li>
<?			}
?>			<li><a class="btn_print_cupom_fiscal modal" href="ecf_documento,<?=$pedido_id?>,listar" rel="345-130" title="emitir Cupom Fiscal"></a></li>
		</ul>
<?		if($rowPedido['fldPedido_Destino_Nfe_Id'] > 0){        
?>			<div class="alert">
                <p class="erro">Essa venda foi convertida em uma NFe. N&atilde;o ser&aacute; poss&iacute;vel fazer edi&ccedil;&otilde;es.<p>
            </div>
<?		}
?>
        <div class="form">
            <form class="frm_detalhe" style="width:890px" id="frm_pedido_novo" action="" method="post">
                <ul>
                    <li>
                        <label for="txt_codigo">C&oacute;d. Venda</label>
                        <input type="text" style="width:70px; text-align:right" class="txt_codigo_pedido" id="txt_codigo" name="txt_codigo" readonly="readonly" value="<?=$rowPedido['fldId']?>"/>
                    </li>
                    <li>
                        <label for="txt_pedido_data">Data de Venda</label>
                        <input type="text" style="width:100px;text-align:center" class="calendario-mask" id="txt_pedido_data" name="txt_pedido_data" value="<?=format_date_out($rowPedido['fldPedidoData'])?>" />
                        <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                    </li>
                    <li>
                        <label for="txt_pedido_hora">Hora</label>
                        <input type="text" style="width:60px;text-align: right" id="txt_pedido_hora" name="txt_pedido_hora" value="<?=$rowPedido['fldCadastroHora']?>" readonly="readonly" />
                    </li>
                    <li>
                        <label for="txt_usuario">Usu&aacute;rio</label>
                        <input type="text" style="width:130px" id="txt_usuario" name="txt_usuario" value="<?=$rowUsuario['fldUsuario']?>" readonly="readonly"/>
                    </li>
                    <li>
                        <label for="sel_status">Status</label>
                        <select class="sel_status" style="width:130px" id="sel_status" name="sel_status" >
<?							($rowPedido["fldStatus"] > 1 ? $filtro = "WHERE fldId > 1" : $filtro ='');
							$rsStatus = mysql_query("SELECT * FROM tblpedido_status ".$filtro);
							while($rowStatus = mysql_fetch_array($rsStatus)){                            
?>                      	    <option <?=($rowStatus['fldId'] == $rowPedido["fldStatus"]) ? 'selected="selected"' : '' ?> value="<?=$rowStatus['fldId']?>"><?=$rowStatus['fldStatus']?></option>
<?							}
?>                 	 </select>
						<a class="modal status_historico" href="pedido_historico_status,<?=$pedido_id?>" rel="600-200"></a>
            		</li>
                    <li>
                        <label for="sel_marcador">Marcador</label>
                        <select style="width:160px;" id="sel_marcador" name="sel_marcador" >
	                        <option value="0">Selecionar</option>
<?							$rsMarcador = mysql_query("SELECT * FROM tblpedido_marcador WHERE fldExcluido = 0 ORDER BY fldMarcador");
							while($rowMarcador = mysql_fetch_array($rsMarcador)){                            
?>                      	    <option <?=($rowPedido['fldMarcador_Id'] == $rowMarcador["fldId"]) ? 'selected="selected"' : '' ?> value="<?=$rowMarcador['fldId']?>"><?=$rowMarcador['fldMarcador']?></option>
<?							}
?>                 	 </select>
            		</li>
                    <li>
                        <label for="txt_referencia">Referencia</label>
                        <input type="text" style="width:90px" id="txt_referencia" name="txt_referencia" value="<?= ($rowPedido['fldReferencia'] > 0) ? $rowPedido['fldReferencia'] : '' ?>" onkeyup="numOnly(this)"/>
                    </li>
                </ul>
                <ul>
<?					$rowCliente	= mysql_fetch_array(mysql_query("SELECT * FROM tblcliente WHERE fldId = ".$rowPedido['fldCliente_Id']));
?>             		<li>
                        <label for="txt_cliente_codigo">Cliente</label>
                        <input type="text" style="width:70px; text-align:right;" id="txt_cliente_codigo" name="txt_cliente_codigo" value="<?=$rowCliente['fldCodigo']?>" />
                        <a href="cliente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
                    <li>
                        <label for="txt_cliente_nome">&nbsp;</label>
                        <input type="text" style=" width:315px" id="txt_cliente_nome" name="txt_cliente_nome" readonly="readonly" value="<?=$rowCliente['fldNome']?>" />
                        <input type="hidden" id="hid_cliente_id" name="hid_cliente_id" value="<?=$rowCliente['fldId']?>" />
                    </li>
<?						$rsFuncionario= mysql_query("SELECT tblfuncionario.fldNome, fldFuncionario_Id 
							 FROM tblpedido_funcionario_servico LEFT JOIN tblfuncionario
							 ON tblfuncionario.fldId = tblpedido_funcionario_servico.fldFuncionario_Id
							 WHERE tblpedido_funcionario_servico.fldFuncao_Tipo = 1 
							 AND tblpedido_funcionario_servico.fldPedido_Id = $pedido_id");
						$rowFuncionario = mysql_fetch_array($rsFuncionario);
?>      	        <li>
            	    	<label for="txt_funcionario_codigo">Funcion&aacute;rio</label>
                        <input type="text" style="width:70px; text-align:right;" id="txt_funcionario_codigo" name="txt_funcionario_codigo" value="<?=$rowFuncionario['fldFuncionario_Id']?>" />
                        <a href="funcionario_busca,1" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
                    <li style="margin-right:50px">
                        <label for="txt_funcionario_nome">&nbsp;</label>
                        <input type="text" style=" width:315px;" id="txt_funcionario_nome" name="txt_funcionario_nome" value="<?=$rowFuncionario['fldNome']?>" readonly="readonly" />
                    </li>
<?					if($_SESSION["exibir_dependente"] > 0){
						if($rowPedido['fldDependente_Id'] > 0){
							$rowDependente 		= mysql_fetch_array(mysql_query("SELECT fldId, fldCodigo, fldNome FROM tblcliente WHERE fldId = ".$rowPedido['fldDependente_Id']));
							$dependente_id	 	= $rowDependente['fldId'];
							$dependente_codigo 	= $rowDependente['fldCodigo'];
							$dependente_nome 	= $rowDependente['fldNome'];
						}
?>                    
                        <li>
                            <label for="txt_dependente_codigo">Dependente</label>
                            <input type="text" style="width:70px; text-align:right" id="txt_dependente_codigo" name="txt_dependente_codigo" value="<?=$dependente_codigo?>" />
                            <a href="dependente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                        </li>
                        <li>
                            <label for="txt_dependente_nome">&nbsp;</label>
                            <input type="text"	 id="txt_dependente_nome" 	name="txt_dependente_nome" 	value="<?=$dependente_nome?>" style=" width:315px" readonly="readonly" />
                            <input type="hidden" id="hid_dependente_id" 	name="hid_dependente_id" 	value="<?=$dependente_id?>" />
                        </li>
<?                  }  
?>					<li>
                 	   <label for="txt_cliente_pedido">N&ordm; Servi&ccedil;o</label>
                       <input type="text" style=" width:70px; text-align:right" name="txt_cliente_pedido" id="txt_cliente_pedido" value="<?=$rowPedido['fldCliente_Pedido']?>" readonly="readonly"  />
                    </li>
<?					if($_SESSION["sistema_tipo"]=="automotivo"){
						$exibir = fnc_sistema('venda_exibir_veiculo');
?>						<li>
                            <label for="sel_veiculo">Ve&iacute;culo</label>
                            <select class="sel_veiculo" style="width:200px;" id="sel_veiculo" name="sel_veiculo">
								<option <?= ($rowPedido['fldVeiculo_Id'] == '' || $rowPedido['fldVeiculo_Id'] == '0') ? 'selected="selected"' : '';?> value="0">selecionar</option>
                            	<? if($rowPedido['fldCliente_Id'] != '0'){ ?>
                            	<option value="novo" style="font-weight:bold;background-color:#FC0">novo veiculo</option>
                            	<? }
								$exibir = fnc_sistema('venda_exibir_veiculo');
								$order = ($exibir == 'placa') ? "fldPlaca" : "fldVeiculo, fldPlaca";
                            	$rsVeiculo = mysql_query("SELECT * FROM tblcliente_veiculo WHERE fldCliente_Id = ". $rowPedido['fldCliente_Id']." ORDER BY $order");
                            	while($rowVeiculo = mysql_fetch_assoc($rsVeiculo)){
                            	?>
                            	<option <?= ($rowVeiculo['fldId'] == $rowPedido['fldVeiculo_Id']) ? 'selected="selected"' : '';?> value="<?=$rowVeiculo['fldId']?>"><?= ($exibir == 'placa') ? $rowVeiculo['fldPlaca'] : $rowVeiculo['fldVeiculo']." [".$rowVeiculo['fldPlaca']."]"?></option>
                            	<? } ?>
                            </select>
						</li>
                        <a class="modal" style="display:none" id="modal_veiculo" href="cliente_veiculo_cadastro_venda" rel="680-200" title=""></a>
                        <li>
                            <label for="txt_veiculo_km">KM</label>
                            <input type="text" style="width:135px" id="txt_veiculo_km" name="txt_veiculo_km" value="<?=$rowPedido['fldVeiculo_Km']?>"/>
                        </li>
                        <li>
                            <label for="txt_veiculo_ano">Ano</label>
                            <input type="text" style="width:70px" id="txt_veiculo_ano" name="txt_veiculo_ano" value="<?=$rowPedido['fldAno']?>" readonly="readonly"/>
                        </li>
                        <li>
                            <label for="txt_veiculo_chassi">Chassi</label>
                            <input type="text" style="width:150px" id="txt_veiculo_chassi" name="txt_veiculo_chassi" value="<?=$rowPedido['fldChassi']?>" readonly="readonly"/>
                        </li>
<?					}
					if($_SESSION["sistema_tipo"]=="financeira"){
?>						<li>
                            <label for="sel_conta_bancaria">Conta Banc&aacute;ria</label>
                            <select style="width:260px" id="sel_conta_bancaria" name="sel_conta_bancaria" >
                            	<option selected="selected" value="<?=$rowBanco['fldContaId']?>"><?=$rowBanco['fldBanco']." - ".$rowBanco['fldConta']?></option>
                            </select>
						</li>
                        <li>
                            <label for="txt_agencia">Ag&ecirc;ncia</label>
                            <input style="width: 60px" type="text" id="txt_agencia" name="txt_agencia" readonly="readonly" value="<?=$rowBanco['fldAgencia']?>"/>
						</li>
                        <li>
                            <label for="txt_titular">Titular</label>
                            <input type="text" name="txt_titular" id="txt_titular" value="<?=$rowBanco['fldTitular']?>" readonly="readonly" />
						</li>
<?					}
?>                  
<?					if($_SESSION["sistema_tipo"]=="otica"){   
						$rowResponsavel = mysql_fetch_array(mysql_query("SELECT fldResponsavel FROM tblcliente_dados_adicionais WHERE fldCliente_Id = ". $rowPedido['fldCliente_Id']));
?>						
                        <li>
                           	<label for="sel_cliente_responsavel">Respons&aacute;vel</label>
                           	<select style="width:200px" id="sel_cliente_responsavel" name="sel_cliente_responsavel" >
                           		<option value="1" <?= ($rowOtica['fldResponsavel'] == '1')? "selected = 'selected'" : '' ?>><?=$rowCliente['fldNome']?></option>
                           		<option value="2" <?= ($rowOtica['fldResponsavel'] == '2')? "selected = 'selected'" : '' ?>><?=$rowResponsavel['fldResponsavel']?></option>
                           	</select>
                        </li>
						<li>
						   	<label for="txt_pedido_entrega">Entrega</label>
                    		<input type="text" style=" width:70px" name="txt_pedido_entrega" id="txt_pedido_entrega" class="calendario-mask" value="<?=format_date_out($rowPedido['fldEntregaData'])?>" />
						</li>
<?					}
					if(fnc_sistema('pedido_exibir_endereco') > 0){
?>	              	
                    <li>
                        <label for="txt_pedido_endereco" <?= ($_SESSION["sistema_tipo"]=="financeira") ? 'style="width:800px"' : '' ?>>Endere&ccedil;o</label>
                        <input type="text" style=" width:422px" name="txt_pedido_endereco" id="txt_pedido_endereco" value="<?=$rowPedido['fldEndereco']?>" />
                    </li>
<?php				}
	                if(fnc_sistema("pedido_retirado_por") == "1") { ?>
	                <li>
	                    <label for="txt_retirado_por">Retirado por</label>
	                    <input type="text" class="enter" style="width:370px" id="txt_retirado_por" name="txt_retirado_por" maxlength="62" value="<?=$rowPedido['fldRetirado_Por'];?>">
	                </li>
                    <? } 
					if($_SESSION['ordem_producao'] != "0") {
					?>
                    <li>
	                    <label for="txt_codigo_pedido_cliente">C&oacute;d Pedido (Cliente)</label>
	                    <input type="text" class="enter" style="width:150px" id="txt_codigo_pedido_cliente" name="txt_codigo_pedido_cliente" maxlength="100" value="<?=$rowPedido['fldCodigo_Cliente']?>">
	                </li>
                   	<? } ?>
                    <li>
                 	   <label for="txt_pedido_obs">Observa&ccedil;&atilde;o</label>
                        <textarea style=" <?=($_SESSION["sistema_tipo"]=="otica")? 'width:480px;' : 'width:859px;' ?> height:80px" id="txt_pedido_obs" name="txt_pedido_obs"><?=$rowPedido['fldObservacao']?></textarea>
                    </li>
                </ul>
                <ul id="pedido_modo_aba" class="menu_modo" style="width:952px;float:left; background:#FFF">
                    <li><a href="produto">produtos</a></li>
                    <li><a href="servico">servi&ccedil;os</a></li>
                </ul>
                <div id="modo_aba_produto" style="width:960px; display:table">
                    <!---aqui uma lista para, conforme excluir algum item que ja estava na venda, ele calcular o estoque devolvido-->
                    <div id="hid_item" style="display:none">
                        <ul class="item_estoque">
                            <li><input type="hidden" class="hid_controle_item_produto_id"	id="hid_controle_item_produto_id"	name="hid_controle_item_produto_id"		value="" /></li>
                            <li><input type="hidden" class="hid_controle_item_estoque" 		id="hid_controle_item_estoque" 		name="hid_controle_item_estoque" 		value="" /></li>
                            <li><input type="hidden" class="hid_controle_item_estoque_id"	id="hid_controle_item_estoque_id"	name="hid_controle_item_estoque_id" 	value="" /></li>
                        </ul>
                        <input type="hidden" id="hid_calculo_estoque_controle" name="hid_calculo_estoque_controle" value="0" />
                    </div>
                    <!---->
                    <div id="pedido_produto" style="width:962px">
                    <?	if($_SESSION['ordem_producao'] != "0") { ?>
                        <ul style="width:962px">
                            <li>
                                <label for="txt_produto_codigo">C&oacute;digo</label>
                                <input type="text" class="codigo" style="width: 60px" id="txt_produto_codigo" name="txt_produto_codigo" value="" />
                                <a href="produto_busca" title="Localizar" class="modal" rel="950-450"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                                <input type="hidden" id="hid_produto_id" 				name="hid_produto_id" 				value="" />
                                <input type="hidden" id="hid_estoque_limite" 			name="hid_estoque_limite" 			value="0" />
                                <input type="hidden" id="hid_estoque_controle" 			name="hid_estoque_controle" 		value="0" />
                                <input type="hidden" id="hid_estoque_negativo_alerta" 	name="hid_estoque_negativo_alerta" 	value="<?= fnc_sistema('alerta_estoque_negativo')?>" />
                                <input type="hidden" id="hid_item_repetido_alerta"	 	name="hid_item_repetido_alerta" 	value="<?=$_SESSION["sistema_pedido_item_repetido"]?>" />
                            </li>
                            <li>
                                <label for="txt_produto_nome">Produto</label>
                                <textarea style="width:400px; height:65px" id="txt_produto_nome" name="txt_produto_nome"></textarea>
                            </li>
                            <li>
                                <label for="sel_produto_fardo">Fardo</label>
                                <SELECT name="sel_produto_fardo" id="sel_produto_fardo" style="width:80px">
                                </SELECT>
                            </li>
                            <li>
                                <label for="sel_produto_tabela">Tabela</label>
                                <SELECT name="sel_produto_tabela" id="sel_produto_tabela" style="width:90px">
                                    <option value="null">padr&atilde;o</option>
<?  	                        	$filtro_tabela = (fnc_sistema('cliente_produto_preco') == 0 ) ? 'AND fldId > 0' : '';
									$rsTabela = mysql_query("SELECT * FROM tblproduto_tabela WHERE fldExcluido = '0' $filtro_tabela ");
									while($rowTabela = mysql_fetch_array($rsTabela)){
?>          	                  		<option value="<?=$rowTabela['fldId']?>"><?=$rowTabela['fldSigla']?></option>
<?									}
?>								</SELECT>
                            </li>
                            <li>
                                <label for="sel_produto_estoque">Estoque</label>
                                <SELECT name="sel_produto_estoque" id="sel_produto_estoque" style="width:90px">
<? 	        	                 	$rsEstoque = mysql_query("SELECT * FROM tblproduto_estoque WHERE fldExcluido = '0'");
									while($rowEstoque = mysql_fetch_array($rsEstoque)){
?>      	                      		<option value="<?=$rowEstoque['fldId']?>"><?=$rowEstoque['fldNome']?></option>
<?									}
?>								</SELECT>
                        	</li>
                        	<li>
                                <label for="txt_produto_valor">Valor Un.</label>
                                <input type="text" style="width:70px; text-align:right;" id="txt_produto_valor" name="txt_produto_valor" value="" />
                                <input type="hidden" name="hid_venda_decimal" id="hid_venda_decimal" value="<?=$vendaDecimal?>" />
                            </li>
                            <li>
                                <label for="txt_produto_quantidade">Qtde</label>
                                <input type="text" style="width:50px; text-align:right;" id="txt_produto_quantidade" name="txt_produto_quantidade" value="" />
                                <input type="hidden" name="hid_quantidade_decimal" id="hid_quantidade_decimal" value="<?=$qtdeDecimal?>" />
                            </li>
                            <li>
                                <label for="txt_produto_desconto">Desc (%)</label>
                                <input type="text" style="width:70px; text-align:right;" id="txt_produto_desconto" name="txt_produto_desconto" value="" />
                            </li>
                            <li>
                            	<label for="txt_produto_referencia">Referencia</label>
                                <input type="text" style="width:80px; text-align:right;" id="txt_produto_referencia" name="txt_produto_referencia" value="" />
                            </li>
                            <li>
                                <label for="txt_produto_codigo_cliente">C&oacute;d (Cliente)</label>
                                <input type="text" style="width: 85px" id="txt_produto_codigo_cliente" name="txt_produto_codigo_cliente" value="" />
                            </li>
                            <li>
                            	<label for="txt_produto_lote">Lote</label>
                                <input type="text" style="width:85px;" id="txt_produto_lote" name="txt_produto_lote" value="" />
                            </li>
                            <li>
                                <input type="hidden" id="hid_produto_UM" name="hid_produto_UM" value="" />
                            	<button class="btn_sub_small" name="btn_item_inserir" id="btn_item_inserir" title="Inserir" >ok</button>
                            </li>
                        </ul>
                    <?	} else { ?>
                        <ul style="width:962px">
                            <li>
                                <label for="txt_produto_codigo">C&oacute;digo</label>
                                <input type="text" class="codigo" style="width: 60px" id="txt_produto_codigo" name="txt_produto_codigo" value="" />
                                <a href="produto_busca" title="Localizar" class="modal" rel="950-450"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                                <input type="hidden" id="hid_produto_id" 				name="hid_produto_id" 				value="" />
                                <input type="hidden" id="hid_estoque_limite" 			name="hid_estoque_limite" 			value="0" />
                                <input type="hidden" id="hid_estoque_controle" 			name="hid_estoque_controle" 		value="0" />
                                <input type="hidden" id="hid_estoque_negativo_alerta" 	name="hid_estoque_negativo_alerta" 	value="<?= fnc_sistema('alerta_estoque_negativo')?>" />
                                <input type="hidden" id="hid_item_repetido_alerta"	 	name="hid_item_repetido_alerta" 	value="<?=$_SESSION["sistema_pedido_item_repetido"]?>" />
                            </li>
                            <li>
                                <label for="txt_produto_nome">Produto</label>
                                <textarea style="width:220px; height:40px" id="txt_produto_nome" name="txt_produto_nome"></textarea>
                            </li>
                            <li>
                                <label for="sel_produto_fardo">Fardo</label>
                                <SELECT name="sel_produto_fardo" id="sel_produto_fardo" style="width:80px">
                                </SELECT>
                            </li>
                            <li>
                                <label for="sel_produto_tabela">Tabela</label>
                                <SELECT name="sel_produto_tabela" id="sel_produto_tabela" style="width:90px">
                                    <option value="null">padr&atilde;o</option>
<?  	                        	$filtro_tabela = (fnc_sistema('cliente_produto_preco') == 0 ) ? 'AND fldId > 0' : '';
									$rsTabela = mysql_query("SELECT * FROM tblproduto_tabela WHERE fldExcluido = '0' $filtro_tabela ");
									while($rowTabela = mysql_fetch_array($rsTabela)){
?>          	                  		<option value="<?=$rowTabela['fldId']?>"><?=$rowTabela['fldSigla']?></option>
<?									}
?>								</SELECT>
                            </li>
                            <li>
                                <label for="sel_produto_estoque">Estoque</label>
                                <SELECT name="sel_produto_estoque" id="sel_produto_estoque" style="width:90px">
<? 	        	                 	$rsEstoque = mysql_query("SELECT * FROM tblproduto_estoque WHERE fldExcluido = '0'");
									while($rowEstoque = mysql_fetch_array($rsEstoque)){
?>      	                      		<option value="<?=$rowEstoque['fldId']?>"><?=$rowEstoque['fldNome']?></option>
<?									}
?>								</SELECT>
                        	</li>
                        	<li>
                                <label for="txt_produto_valor">Valor Un.</label>
                                <input type="text" style="width:60px; text-align:right;" id="txt_produto_valor" name="txt_produto_valor" value="" />
                                <input type="hidden" name="hid_venda_decimal" id="hid_venda_decimal" value="<?=$vendaDecimal?>" />
                            </li>
                            <li>
                                <label for="txt_produto_quantidade">Qtde</label>
                                <input type="text" style="width:50px; text-align:right;" id="txt_produto_quantidade" name="txt_produto_quantidade" value="" />
                                <input type="hidden" name="hid_quantidade_decimal" id="hid_quantidade_decimal" value="<?=$qtdeDecimal?>" />
                            </li>
                            <li>
                                <label for="txt_produto_desconto">Desc (%)</label>
                                <input type="text" style="width:55px; text-align:right;" id="txt_produto_desconto" name="txt_produto_desconto" value="" />
                            </li>
                            <li>
                            	<label for="txt_produto_referencia">Referencia</label>
                                <input type="text" style="width:60px; text-align:right;" id="txt_produto_referencia" name="txt_produto_referencia" value="" />
                            </li>
                            <li>
                                <input type="hidden" id="hid_produto_UM" name="hid_produto_UM" value="" />
                            	<button class="btn_sub_small" name="btn_item_inserir" id="btn_item_inserir" title="Inserir" >ok</button>
                            </li>
                        </ul>
                    <?	} ?>
                    </div>
				
                    <div id="pedido_lista" style="width:960px">
                    <?	if($_SESSION['ordem_producao'] != "0") { ?>
                        <ul id="pedido_lista_cabecalho" style="width:960px">
                            <li style="width:20px">&nbsp;</li>
                            <li style="width:20px" class="entregue" title="entregue"></li>
                            <li style="width:65px">C&oacute;digo</li>
                            <li style="width:75px">C&oacute;d (Cliente)</li>
                            <li style="width:220px">Produto</li>
                            <li style="width:45px">Fardo</li>
                            <li style="width:50px">Tabela</li>
                            <li style="width:70px">Valor Un.</li>
                            <li style="width:55px">Qtde</li>
                            <li style="width:60px">Estoque</li>
                            <li style="width:30px">U.M.</li>
                            <li style="width:45px">Desc(%)</li>
                            <li style="width:65px">Subtotal</li>
                            <li style="width:70px">Referencia</li>
                            <li style="width:55px">Lote</li>
                        </ul>
                    <?	} else { ?>
                        <ul id="pedido_lista_cabecalho" style="width:960px">
                            <li style="width:20px">&nbsp;</li>
                            <li style="width:20px" class="entregue" title="entregue"></li>
                            <li style="width:65px">C&oacute;digo</li>
                            <li style="width:260px">Produto</li>
                            <li style="width:55px">Fardo</li>
                            <li style="width:60px">Tabela</li>
                            <li style="width:80px">Valor Un.</li>
                            <li style="width:60px">Qtde</li>
                            <li style="width:80px">Estoque</li>
                            <li style="width:45px">U.M.</li>
                            <li style="width:60px">Desc(%)</li>
                            <li style="width:70px">Subtotal</li>
                            <li style="width:70px">Referencia</li>
                        </ul>
                    <?	} ?>
                        
					<div id="hidden">
                    <?	if($_SESSION['ordem_producao'] != "0") { ?>
                        <ul id="pedido_lista_item" style="width:960px">
                            <li style="width:20px;">
                                <a class="a_excluir" id="excluir_0" href="" title="Excluir item"></a>
                            </li>
                            <li style="width:20px; background:#FFF">
                                <input class="chk_entregue"			type="checkbox" id="chk_entregue_0" 		name="chk_entregue_0" 			style="width:20px" title="item entregue" />
                                <input class="hid_item_id" 			type="hidden" 	id="hid_pedido_item_id" 	name="hid_pedido_item_id" 		value="0" />
                            </li>
                            <li>
                                <input class="txt_item_codigo" 		type="text" 	id="txt_item_codigo_0" 		name="txt_item_codigo" 			value="" style="width:65px" readonly="readonly"/>
                                <input class="hid_item_produto_id" 	type="hidden" 	id="hid_item_produto_id_0"	name="hid_item_produto_id_0" 	value="" />
                                <input class="hid_item_detalhe" 	type="hidden" 	id="hid_item_detalhe_0" 	name="hid_item_detalhe_0" 		value="0" />
                            </li>
                            <li>
                                <input class="txt_item_codigo_cliente" type="text" style="width: 75px" id="txt_item_codigo_cliente" name="txt_item_codigo_cliente" value="" readonly="readonly"/>
                            </li>
                            <li>
                                <input class="txt_item_nome" 		type="text" 	id="txt_item_nome_0" 		name="txt_item_nome" 			value="" style="width:220px;text-align:left" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_fardo"		type="text" 	id="txt_item_fardo_0" 		name="txt_item_fardo" 			value="" style="width:45px" readonly="readonly" />
                                <input class="hid_item_fardo" 		type="hidden" 	id="hid_item_fardo_0" 		name="hid_item_fardo" 			value="" />
                            </li>
                            <li>
                                <input class="txt_item_tabela_sigla" type="text" 	id="txt_item_tabela_sigla_0" name="txt_item_tabela_sigla" 	value="" style="width:50px" readonly="readonly" />
                                <input class="hid_item_tabela_preco" type="hidden" 	id="hid_item_tabela_preco_0" name="hid_item_tabela_preco" 	value="" />
                            </li>
                            <li>
                                <input class="txt_item_valor" 		type="text" 	id="txt_item_valor_0" 		name="txt_item_valor" 			value="0" style="width:70px;text-align:right" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_quantidade"	type="text" 	id="txt_item_quantidade_0" 	name="txt_item_quantidade" 		value="0,00" style="width:55px;text-align:right" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_estoque_nome"type="text" 	id="txt_item_estoque_nome_0"name="txt_item_estoque_nome" 	value="" style="width:60px" readonly="readonly" />
                                <input class="hid_item_estoque_id" 	type="hidden" 	id="hid_item_estoque_id_0" 	name="hid_item_estoque_id" 		value="1" />
                            </li>
                            <li>
                                <input class="txt_item_UM" 			type="text" 	id="txt_item_UM_0" 			name="txt_item_UM" 				value="" style="width:30px;text-align:center" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_desconto" 	type="text" 	id="txt_item_desconto_0" 	name="txt_item_desconto" 		value="" style="width:45px;text-align:right" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_subtotal" 	type="text" 	id="txt_item_subtotal_0" 	name="txt_item_subtotal" 		value="0" style="width:65px;text-align:right" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_referencia" type="text"		id="txt_item_referencia" 	name="txt_item_referencia" 		value="" style="width:70px; text-align:right" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_lote" type="text" style="width:55px; text-align: right" id="txt_item_lote" name="txt_item_lote" value="" readonly="readonly" />
                            </li>
                        </ul>
                    <?	} else { ?>
                        <ul id="pedido_lista_item" style="width:960px">
                            <li style="width:20px;">
                                <a class="a_excluir" id="excluir_0" href="" title="Excluir item"></a>
                            </li>
                            <li style="width:20px; background:#FFF">
                                <input class="chk_entregue"			type="checkbox" id="chk_entregue_0" 		name="chk_entregue_0" 			style="width:20px" title="item entregue" />
                                <input class="hid_item_id" 			type="hidden" 	id="hid_pedido_item_id" 	name="hid_pedido_item_id" 		value="0" />
                            </li>
                            <li>
                                <input class="txt_item_codigo" 		type="text" 	id="txt_item_codigo_0" 		name="txt_item_codigo" 			value="" style="width:65px" readonly="readonly"/>
                                <input class="hid_item_produto_id" 	type="hidden" 	id="hid_item_produto_id_0"	name="hid_item_produto_id_0" 	value="" />
                                <input class="hid_item_detalhe" 	type="hidden" 	id="hid_item_detalhe_0" 	name="hid_item_detalhe_0" 		value="0" />
                            </li>
                            <li>
                                <input class="txt_item_nome" 		type="text" 	id="txt_item_nome_0" 		name="txt_item_nome" 			value="" style="width:260px;text-align:left" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_fardo"		type="text" 	id="txt_item_fardo_0" 		name="txt_item_fardo" 			value="" style="width:55px" readonly="readonly" />
                                <input class="hid_item_fardo" 		type="hidden" 	id="hid_item_fardo_0" 		name="hid_item_fardo" 			value="" />
                            </li>
                            <li>
                                <input class="txt_item_tabela_sigla" type="text" 	id="txt_item_tabela_sigla_0" name="txt_item_tabela_sigla" 	value="" style="width:60px" readonly="readonly" />
                                <input class="hid_item_tabela_preco" type="hidden" 	id="hid_item_tabela_preco_0" name="hid_item_tabela_preco" 	value="" />
                            </li>
                            <li>
                                <input class="txt_item_valor" 		type="text" 	id="txt_item_valor_0" 		name="txt_item_valor" 			value="0" style="width:80px;text-align:right" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_quantidade"	type="text" 	id="txt_item_quantidade_0" 	name="txt_item_quantidade" 		value="0,00" style="width:60px;text-align:right" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_estoque_nome"type="text" 	id="txt_item_estoque_nome_0"name="txt_item_estoque_nome" 	value="" style="width:80px" readonly="readonly" />
                                <input class="hid_item_estoque_id" 	type="hidden" 	id="hid_item_estoque_id_0" 	name="hid_item_estoque_id" 		value="1" />
                            </li>
                            <li>
                                <input class="txt_item_UM" 			type="text" 	id="txt_item_UM_0" 			name="txt_item_UM" 				value="" style="width:45px;text-align:center" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_desconto" 	type="text" 	id="txt_item_desconto_0" 	name="txt_item_desconto" 		value="" style="width:60px;text-align:right" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_subtotal" 	type="text" 	id="txt_item_subtotal_0" 	name="txt_item_subtotal" 		value="0" style="width:70px;text-align:right" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_referencia" type="text"		id="txt_item_referencia" 	name="txt_item_referencia" 		value="" style="width:70px; text-align:right" readonly="readonly" />
                            </li>
                        </ul>
                    <?	} ?>
                    </div>
						
<?
                        $rsItem = mysql_query("SELECT * FROM tblpedido_item WHERE fldPedido_Id = $pedido_id AND fldExcluido = '0'");
                        $rows 	= mysql_num_rows($rsItem);
                        $i = 1;
                        while($rowItem = mysql_fetch_array($rsItem)){
                                
                            $rowProduto = mysql_fetch_array(mysql_query("SELECT tblproduto.*, 
                                                         tblproduto_unidade_medida.fldSigla
                                                         FROM tblproduto LEFT JOIN tblproduto_unidade_medida ON tblproduto.fldUN_Medida_Id = tblproduto_unidade_medida.fldId
                                                         WHERE tblproduto.fldId = ".$rowItem['fldProduto_Id']." ORDER BY tblproduto.fldCodigo DESC"));
                            
                            $rowEstoque = mysql_fetch_array(mysql_query("SELECT tblproduto_estoque.fldId, tblproduto_estoque.fldNome
                                                        FROM tblproduto_estoque INNER JOIN tblpedido_item ON tblproduto_estoque.fldId = tblpedido_item.fldEstoque_Id
                                                        WHERE tblproduto_estoque.fldId = ".$rowItem['fldEstoque_Id']));
                            $rowFardo = mysql_fetch_array(mysql_query("SELECT * FROM tblproduto_fardo WHERE fldId = ".$rowItem['fldFardo_Id']));
                            
                            $item_id 		= $rowItem['fldId'];
                            $valor_item	 	= $rowItem['fldValor'];
                            $qtd 			= $rowItem['fldQuantidade'];
                            $total_item 	= $valor_item * $qtd;
                            $desconto 		= $rowItem['fldDesconto'];
                            $totalDesconto 	= ($total_item 	* $desconto)/100;
                            $totalValor 	= $total_item 	- $totalDesconto;
                            
                            if($_SESSION["sistema_tipo"] == "otica"){
                        
                                if($otica_retorno['perto']['armacao'] == $item_id){
                                    $Otica_Perto_Armacao_Descricao = $rowItem['fldDescricao'];
                                    $Otica_Perto_Armacao = $i;
                                }
                                if($otica_retorno['perto']['material'] == $item_id){
                                    $Otica_Perto_Material_Descricao = $rowItem['fldDescricao'];
                                    $Otica_Perto_Material = $i;
                                }
                                if($otica_retorno['longe']['armacao'] == $item_id){
                                    $Otica_Longe_Armacao_Descricao = $rowItem['fldDescricao'];
                                    $Otica_Longe_Armacao = $i;
                                }
                                if($otica_retorno['longe']['material'] == $item_id){
                                    $Otica_Longe_Material_Descricao = $rowItem['fldDescricao'];
                                    $Otica_Longe_Material = $i;
                                }
                                if($otica_retorno['bifocal']['armacao'] == $item_id){
                                    $Otica_Bifocal_Armacao_Descricao = $rowItem['fldDescricao'];
                                    $Otica_Bifocal_Armacao = $i;
                                }
                                if($otica_retorno['bifocal']['material'] == $item_id){
                                    $Otica_Bifocal_Material_Descricao = $rowItem['fldDescricao'];
                                    $Otica_Bifocal_Material = $i;
                                }
                            }
?>
                            <ul id="pedido_lista_item" style="width:960px">
                            <?	if($_SESSION['ordem_producao'] != "0") { ?>
								<? if($rowPedido['fldPedido_Destino_Nfe_Id'] == 0){	?> 
                                        <li style="width:15px;">
                                            <a class="a_excluir" id="excluir" href="" title="Excluir item"></a>
                                        </li>
                                <?	}												?>
                                <li style="width:20px; background:#FFF">
                                    <input class="chk_entregue"			type="checkbox" id="chk_pedido_<?=$i?>" 			name="chk_entregue_<?=$i?>" <?= ($rowItem['fldEntregue'] == 1) ? "checked='checked'" : ''?>  style="width:20px" title="item entregue"  <?= ($rowPedido['fldStatus'] == 1) ? "onClick='return false'" : ''?>/>
                                    <input class="hid_pedido_item_id" 	type="hidden" 	id="hid_pedido_item_id_<?=$i?>" 	name="hid_pedido_item_id_<?=$i?>"	value="<?=$rowItem['fldId']?>" />
                                </li>
                                <li>
                                    <input class="txt_item_codigo" 		type="text" 	id="txt_item_codigo_<?=$i?>" 		name="txt_item_codigo_<?=$i?>" 		value="<?=$rowProduto['fldCodigo']?>" style="width: 65px"  readonly="readonly" />
                                    <input class="hid_item_produto_id" 	type="hidden" 	id="hid_item_produto_id_<?=$i?>"	name="hid_item_produto_id_<?=$i?>" 	value="<?=$rowProduto['fldId']?>" />
                                    <input class="hid_item_detalhe" 	type="hidden" 	id="hid_item_detalhe_<?=$i?>" 		name="hid_item_detalhe_<?=$i?>" 	value="1" /><? //parametro pra identificar item que ja estava na venda ?>
                                </li>
                                <li>
                                    <input class="txt_item_codigo_cliente" type="text" style="width: 75px" id="txt_item_codigo_cliente" name="txt_item_codigo_cliente" value="<?=$rowItem['fldProduto_Codigo_Cliente']?>" readonly="readonly"/>
                                </li>
                                <li>
                                    <input class="txt_item_nome" 		type="text" 	id="txt_item_nome_<?=$i?>" 			name="txt_item_nome_<?=$i?>" 		value="<?=$rowItem['fldDescricao']?>" style="width:220px;text-align:left" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_fardo" 		type="text" 	id="txt_item_fardo_<?=$i?>" 		name="txt_item_fardo_<?=$i?>" 		value="<?=$rowFardo['fldCodigo']?>" style="width:45px" readonly="readonly" />
                                    <input class="hid_item_fardo" 		type="hidden" 	id="hid_item_fardo_<?=$i?>" 		name="hid_item_fardo_<?=$i?>" 		value="<?=$rowFardo['fldId']?>" />
                                </li>
                                <li>
                                    <?	if($rowItem['fldTabela_Preco_Id'] != NULL){
                                                $rowTabela 	= mysql_fetch_array(mysql_query("SELECT * FROM tblproduto_tabela WHERE fldId = ".$rowItem['fldTabela_Preco_Id'])); 
                                                $sigla 		= $rowTabela['fldSigla'];
                                        }else{	$sigla 		= 'padr&atilde;o';	}
                                    ?>
                                    <input class="txt_tabela_sigla" 	type="text" 	id="txt_tabela_sigla_<?=$i?>" 		name="txt_tabela_sigla_<?=$i?>" 	value="<?=$sigla?>" style="width:50px" readonly="readonly" />
                                    <input class="hid_item_tabela_preco"type="hidden" 	id="hid_item_tabela_preco_<?=$i?>" 	name="hid_item_tabela_preco_<?=$i?>"value="<?=$rowItem['fldTabela_Preco_Id']?>" />
                                </li>
                                <li>
                                    <input class="txt_item_valor" 		type="text" 	id="txt_item_valor_<?=$i?>" 		name="txt_item_valor_<?=$i?>" 		value="<?=format_number_out($rowItem['fldValor'],$vendaDecimal)?>" style="width:70px;text-align:right"  />
                                </li>
                                <li>
                                    <input class="txt_item_quantidade" 	type="text" 	id="txt_item_quantidade_<?=$i?>"	name="txt_item_quantidade_<?=$i?>" 	value="<?=format_number_out($rowItem['fldQuantidade'],$qtdeDecimal)?>" style="width:55px;text-align:right"/>
                                </li>
                                <li>
                                    <input class="txt_item_estoque_nome"type="text" 	id="txt_item_estoque_nome_<?=$i?>" 	name="txt_item_estoque_nome_<?=$i?>"value="<?=$rowEstoque['fldNome']?>" style="width:60px" readonly="readonly" />
                                    <input class="hid_item_estoque_id" 	type="hidden" 	id="hid_item_estoque_id_<?=$i?>" 	name="hid_item_estoque_id_<?=$i?>" 	value="<?=$rowEstoque['fldId']?>" />
                                </li>
                                <li>
                                    <input class="txt_item_UM" 			type="text" 	id="txt_item_UM_<?=$i?>" 			name="txt_item_UM_<?=$i?>" 			value="<?=$rowProduto['fldSigla']?>" style="width:30px;text-align:center" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_desconto" 	type="text" 	id="txt_item_desconto_<?=$i?>" 		name="txt_item_desconto_<?=$i?>" 	value="<?=format_number_out($rowItem['fldDesconto'])?>" 	style="width:45px;text-align:right" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_subtotal" 	type="text" 	id="txt_item_subtotal_<?=$i?>" 		name="txt_item_subtotal_<?=$i?>"	value="<?=format_number_out($totalValor,$vendaDecimal)?>" 	style="width:65px;text-align:right" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_referencia" 	type="text"		id="txt_item_referencia_<?=$i?>" 	name="txt_item_referencia_<?=$i?>"	value="<?=$rowItem['fldReferencia']?>"						style="width:70px; text-align:right" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_lote" 	type="text"		id="txt_item_lote_<?=$i?>" 	name="txt_item_lote_<?=$i?>"	value="<?=$rowItem['fldLote']?>"						style="width:55px; text-align:right" readonly="readonly" />
                                </li>
                            <?	} else { ?>
								<? if($rowPedido['fldPedido_Destino_Nfe_Id'] == 0){	?> 
                                        <li style="width:15px;">
                                            <a class="a_excluir" id="excluir" href="" title="Excluir item"></a>
                                        </li>
                                <?	}												?>
                                <li style="width:20px; background:#FFF">
                                    <input class="chk_entregue"			type="checkbox" id="chk_pedido_<?=$i?>" 			name="chk_entregue_<?=$i?>" <?= ($rowItem['fldEntregue'] == 1) ? "checked='checked'" : ''?>  style="width:20px" title="item entregue"  <?= ($rowPedido['fldStatus'] == 1) ? "onClick='return false'" : ''?>/>
                                    <input class="hid_pedido_item_id" 	type="hidden" 	id="hid_pedido_item_id_<?=$i?>" 	name="hid_pedido_item_id_<?=$i?>"	value="<?=$rowItem['fldId']?>" />
                                </li>
                                <li>
                                    <input class="txt_item_codigo" 		type="text" 	id="txt_item_codigo_<?=$i?>" 		name="txt_item_codigo_<?=$i?>" 		value="<?=$rowProduto['fldCodigo']?>" style="width: 65px"  readonly="readonly" />
                                    <input class="hid_item_produto_id" 	type="hidden" 	id="hid_item_produto_id_<?=$i?>"	name="hid_item_produto_id_<?=$i?>" 	value="<?=$rowProduto['fldId']?>" />
                                    <input class="hid_item_detalhe" 	type="hidden" 	id="hid_item_detalhe_<?=$i?>" 		name="hid_item_detalhe_<?=$i?>" 	value="1" /><? //parametro pra identificar item que ja estava na venda ?>
                                </li>
                                <li>
                                    <input class="txt_item_nome" 		type="text" 	id="txt_item_nome_<?=$i?>" 			name="txt_item_nome_<?=$i?>" 		value="<?=$rowItem['fldDescricao']?>" style="width:260px;text-align:left" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_fardo" 		type="text" 	id="txt_item_fardo_<?=$i?>" 		name="txt_item_fardo_<?=$i?>" 		value="<?=$rowFardo['fldCodigo']?>" style="width:55px" readonly="readonly" />
                                    <input class="hid_item_fardo" 		type="hidden" 	id="hid_item_fardo_<?=$i?>" 		name="hid_item_fardo_<?=$i?>" 		value="<?=$rowFardo['fldId']?>" />
                                </li>
                                <li>
                                    <?	if($rowItem['fldTabela_Preco_Id'] != NULL){
                                                $rowTabela 	= mysql_fetch_array(mysql_query("SELECT * FROM tblproduto_tabela WHERE fldId = ".$rowItem['fldTabela_Preco_Id'])); 
                                                $sigla 		= $rowTabela['fldSigla'];
                                        }else{	$sigla 		= 'padr&atilde;o';	}
                                    ?>
                                    <input class="txt_tabela_sigla" 	type="text" 	id="txt_tabela_sigla_<?=$i?>" 		name="txt_tabela_sigla_<?=$i?>" 	value="<?=$sigla?>" style="width:60px" readonly="readonly" />
                                    <input class="hid_item_tabela_preco"type="hidden" 	id="hid_item_tabela_preco_<?=$i?>" 	name="hid_item_tabela_preco_<?=$i?>"value="<?=$rowItem['fldTabela_Preco_Id']?>" />
                                </li>
                                <li>
                                    <input class="txt_item_valor" 		type="text" 	id="txt_item_valor_<?=$i?>" 		name="txt_item_valor_<?=$i?>" 		value="<?=format_number_out($rowItem['fldValor'],$vendaDecimal)?>" style="width:80px;text-align:right"  />
                                </li>
                                <li>
                                    <input class="txt_item_quantidade" 	type="text" 	id="txt_item_quantidade_<?=$i?>"	name="txt_item_quantidade_<?=$i?>" 	value="<?=format_number_out($rowItem['fldQuantidade'],$qtdeDecimal)?>" style="width:60px;text-align:right"/>
                                </li>
                                <li>
                                    <input class="txt_item_estoque_nome"type="text" 	id="txt_item_estoque_nome_<?=$i?>" 	name="txt_item_estoque_nome_<?=$i?>"value="<?=$rowEstoque['fldNome']?>" style="width:80px" readonly="readonly" />
                                    <input class="hid_item_estoque_id" 	type="hidden" 	id="hid_item_estoque_id_<?=$i?>" 	name="hid_item_estoque_id_<?=$i?>" 	value="<?=$rowEstoque['fldId']?>" />
                                </li>
                                <li>
                                    <input class="txt_item_UM" 			type="text" 	id="txt_item_UM_<?=$i?>" 			name="txt_item_UM_<?=$i?>" 			value="<?=$rowProduto['fldSigla']?>" style="width:45px;text-align:center" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_desconto" 	type="text" 	id="txt_item_desconto_<?=$i?>" 		name="txt_item_desconto_<?=$i?>" 	value="<?=format_number_out($rowItem['fldDesconto'])?>" 	style="width:60px;text-align:right" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_subtotal" 	type="text" 	id="txt_item_subtotal_<?=$i?>" 		name="txt_item_subtotal_<?=$i?>"	value="<?=format_number_out($totalValor,$vendaDecimal)?>" 	style="width:70px;text-align:right" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_referencia" 	type="text"		id="txt_item_referencia_<?=$i?>" 	name="txt_item_referencia_<?=$i?>"	value="<?=$rowItem['fldReferencia']?>"						style="width:70px; text-align:right" readonly="readonly" />
                                </li>
                            <?	} ?>
                            </ul>
<?	                     	$totalProdutos += $totalValor;
                            $i += 1;
                        }
?>						</ul>
					</div>
                    <div>
                        <input type="hidden" name="hid_controle" id="hid_controle" value="<?=$rows?>" />
                    </div>
                    
<?					if($_SESSION["sistema_tipo"]=="otica"){	?>	
                        <div id="pedido_otica_opcoes">
                            <!-- �culos longe -->
                            <div class="pedido_otica_opcoes_detalhe">
                                <span>LONGE</span>
                                <ul class="pedido_otica_opcoes_dados" style="margin-left: 25px">
                                    <li><input style="width:235px;background:#F5F4D8" type="text" 	name="txt_pedido_otica_longe_armacao" value="<?=($Otica_Longe_Armacao_Descricao != '') ? $Otica_Longe_Armacao_Descricao : "Arma&ccedil;&atilde;o";?>" readonly="true"></li>
                                    <li><input type="hidden" name="hid_pedido_otica_longe_armacao" 	value="<?=substr($Otica_Longe_Armacao,0,30)?>"></li>
                                    <li><input style="width:235px;background:#D6F2F8" type="text" 	name="txt_pedido_otica_longe_material" value="<?=($Otica_Longe_Material_Descricao != '') ? $Otica_Longe_Material_Descricao : "Material";?>" readonly="true"></li>
                                    <li><input type="hidden" name="hid_pedido_otica_longe_material" value="<?=substr($Otica_Longe_Material,0,30)?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_cabecalho">
                                    <li style="width: 57px; text-align: right;">Esf&eacute;rico</li>
                                    <li style="width: 57px; text-align: left;">Cil&iacute;ndrico</li>
                                    <li style="width: 57px; text-align: left;">Eixo</li>
                                    <li style="width: 57px; text-align: left;">DP</li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.D</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_esferico" 	style="width:54px; height:19px;text-align:right"	value="<?=$otica_retorno['longe']['OD']['esferico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_cilindrico" style="width:54px; height:19px;text-align:right"	value="<?=$otica_retorno['longe']['OD']['cilindrico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_eixo" 		style="width:54px; height:19px;text-align:right" 	value="<?=$otica_retorno['longe']['OD']['eixo']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_dp"			style="width:54px; height:19px;text-align:right" 	value="<?=$otica_retorno['longe']['OD']['DP']?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.E</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_esferico" 	style="width:54px; height:19px;text-align:right" 	value="<?=$otica_retorno['longe']['OE']['esferico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_cilindrico" style="width:54px; height:19px;text-align:right" 	value="<?=$otica_retorno['longe']['OE']['cilindrico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_eixo"		style="width:54px; height:19px;text-align:right"	value="<?=$otica_retorno['longe']['OE']['eixo']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_dp" 		style="width:54px; height:19px;text-align:right" 	value="<?=$otica_retorno['longe']['OE']['DP']?>"></li>
                                </ul>
                            </div>
                            <!-- �culos perto -->
                            <a class="modal" style="display:none" id="modal_otica" href="pedido_otica_select" rel="345-130" title=""></a>
                            <div class="pedido_otica_opcoes_detalhe">
                                <span>PERTO</span>
                                <ul class="pedido_otica_opcoes_dados" style="margin-left: 25px">
                                    <li><input type="text" 	 name="txt_pedido_otica_perto_armacao" 	value="<?=($Otica_Perto_Armacao_Descricao != '')  ? $Otica_Perto_Armacao_Descricao : "Arma&ccedil;&atilde;o";?>" style="width:235px;background:#F5F4D8" readonly="true"></li>
                                    <li><input type="hidden" name="hid_pedido_otica_perto_armacao" 	value="<?=substr($Otica_Perto_Armacao,0,30)?>"></li>
                                    <li><input type="text"	 name="txt_pedido_otica_perto_material" value="<?=($Otica_Perto_Material_Descricao != '') ? $Otica_Perto_Material_Descricao : "Material";?>" style="width:235px;background:#D6F2F8" readonly="true"></li>
                                    <li><input type="hidden" name="hid_pedido_otica_perto_material" value="<?=substr($Otica_Perto_Material,0,30)?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_cabecalho">
                                    <li style="width: 57px; text-align: right;">Esf&eacute;rico</li>
                                    <li style="width: 57px; text-align: left;">Cil&iacute;ndrico</li>
                                    <li style="width: 57px; text-align: left;">Eixo</li>
                                    <li style="width: 57px; text-align: left;">DP</li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.D</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_esferico" 	style="width:54px; height: 19px;text-align:right" 	value="<?=$otica_retorno['perto']['OD']['esferico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_cilindrico" style="width:54px; height: 19px;text-align:right"	value="<?=$otica_retorno['perto']['OD']['cilindrico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_eixo" 		style="width:54px; height: 19px;text-align:right" 	value="<?=$otica_retorno['perto']['OD']['eixo']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_dp" 		style="width:54px; height: 19px;text-align:right" 	value="<?=$otica_retorno['perto']['OD']['DP']?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.E</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_esferico" 	style="width:54px; height:19px; text-align:right"	value="<?=$otica_retorno['perto']['OE']['esferico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_cilindrico" style="width:54px; height:19px; text-align:right"	value="<?=$otica_retorno['perto']['OE']['cilindrico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_eixo" 		style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['perto']['OE']['eixo']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_dp"			style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['perto']['OE']['DP']?>"></li>
                                </ul>
                            </div>
                            <!-- �culos Bifocal -->
                            <div class="pedido_otica_opcoes_detalhe">
                                <span>BIFOCAL / MULTIFOCAL</span>
                                <ul class="pedido_otica_opcoes_dados" style="margin-left: 25px">
                                    <li>
                                        <select style="width:180px;width:235px;background:#F5F4D8"" id="sel_otica_marca" name="sel_otica_marca" >
                                            <option value="0">Selecionar</option>
<?											$rsMarca = mysql_query("SELECT * FROM tblmarca WHERE fldDisabled = '0' ORDER BY fldNome");
                                            while($rowMarca = mysql_fetch_array($rsMarca)){                            
?>      	             	       				<option value="<?=$rowMarca['fldId']?>" <?= ($otica_retorno['bifocal']['marca'] == $rowMarca['fldId'])?"selected = 'selected'" : '' ?> ><?=$rowMarca['fldNome']?></option>
<?											}
?>  	               					</select>
                                    </li>
                                    <li>
                                        <select style="width:180px;width:235px;background:#D6F2F8"" id="sel_otica_lente" name="sel_otica_lente" >
                                            <option value="0">Selecionar</option>
<?											$rsLente = mysql_query("SELECT * FROM tblproduto_otica_lente ORDER BY fldLente");
                                            while($rowLente = mysql_fetch_array($rsLente)){                            
?>		                   	       				<option value="<?=$rowLente['fldId']?>" <?= ($otica_retorno['bifocal']['lente'] == $rowLente['fldId'])?"selected = 'selected'" : '' ?>><?=$rowLente['fldLente']?></option>
<?											}
?>	                 					</select>
                                    </li>
                                </ul>
                                <ul class="pedido_otica_opcoes_cabecalho">
                                    <li style="width: 66px; text-align: right;">Altura</li>
                                    <li style="width: 60px; text-align: center;">DNP</li>
                                    <li style="width: 60px; text-align: center;">Adi&ccedil;&atilde;o</li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.D</li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_od_altura" 	style="width: 56px; height: 19px; text-align: right" 	value="<?=$otica_retorno['bifocal']['OD']['altura']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_od_dnp" 		style="width: 56px; height: 19px; text-align: right" 	value="<?=$otica_retorno['bifocal']['OD']['DNP']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_od_adicao" 	style="width: 56px; height: 19px; text-align: right" 	value="<?=$otica_retorno['bifocal']['OD']['adicao']?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.E</li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_oe_altura" 	style="width: 56px; height: 19px; text-align: right" 	value="<?=$otica_retorno['bifocal']['OE']['altura']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_oe_dnp"		style="width: 56px; height: 19px; text-align: right" 	value="<?=$otica_retorno['bifocal']['OE']['DNP']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_oe_adicao" 	style="width: 56px; height: 19px; text-align: right" 	value="<?=$otica_retorno['bifocal']['OE']['adicao']?>"></li>
                                </ul>
                            </div>
                            <ul class="pedido_otica_opcoes_dados" style="margin:0">
                                <li>
                                    <label for="txt_pedido_otica_medico">M&eacute;dico</label>
                                    <input type="text" name="txt_pedido_otica_medico" 		style="width: 625px" value="<?=$rowOtica['fldMedico']?>" />
                                </li>
                                <li>
                                    <label for="txt_pedido_otica_medico_crm">CRM</label>
                                    <input type="text" name="txt_pedido_otica_medico_crm" 	style="width: 300px" value="<?=$rowOtica['fldMedico_crm']?>" />
                                </li>
                            </ul>
                        </div>
<?					}
?>    			</div>

				<div id="modo_aba_servico" style="width:962px;height:274px;display:table;background:#D3D3D3">
                	<input type="hidden" name="hid_servico_valor" id="hid_servico_valor" value="<?=$servico_valor?>" />
                	
<?					$sqlFuncaionario_Venda = "SELECT * FROM tblpedido_funcionario_servico WHERE fldPedido_Id = $pedido_id";
					$sqlFuncionario = "SELECT tblfuncionario.fldNome, tblfuncionario.fldId
															 FROM tblfuncionario INNER JOIN tblfuncionario_funcao 
															 ON tblfuncionario.fldFuncao2_Id = tblfuncionario_funcao.fldId
															 WHERE tblfuncionario_funcao.fldTipo = '2'";
?>					<fieldset style="width:310px;float:left;margin:7px 7px 0px 0px;border:1px solid #FFF">
                        <ul style="width:310px">
                            <li style="margin-bottom:0">
                                <label for="sel_funcionario1">Funcionario 1</label>
                                <select style="width:300px" id="sel_funcionario1" name="sel_funcionario1" class="sel_funcionario">
                                	<option value="0">selecionar</option>
<?									$rowFuncionarioVenda 	= mysql_fetch_array(mysql_query($sqlFuncaionario_Venda." AND fldOrdem = 1"));
									$rsFuncionario 			= mysql_query($sqlFuncionario);
									while($rowFuncionario 	= mysql_fetch_array($rsFuncionario)){
?>										<option value="<?=$rowFuncionario['fldId']?>" <?= ($rowFuncionarioVenda['fldFuncionario_Id'] == $rowFuncionario['fldId'])? "selected='selected'" : '' ?>><?=$rowFuncionario['fldNome']?></option>
<?									}
?>              	        	</select>	
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txa_funcionario1_servicos">Servi&ccedil;o</label>
                                <textarea style="width:295px; height:50px" 				id="txa_funcionario1_servico" name="txa_funcionario1_servico" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>><?=$rowFuncionarioVenda['fldServico']?></textarea>
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txt_funcionario1_tempo">Tempo</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario1_tempo" name="txt_funcionario1_tempo" value="<?=format_number_out($rowFuncionarioVenda['fldTempo'])?>" class="txt_funcionario_tempo" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                            <li>
                                <label for="txt_funcionario1_valor">Valor</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario1_valor" name="txt_funcionario1_valor" value="<?=format_number_out($rowFuncionarioVenda['fldValor'])?>" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                        </ul>
                    </fieldset>
<?					$totalServicos += $rowFuncionarioVenda['fldValor'];
?>
                    <fieldset style="width:310px;float:left;margin:7px 7px 0px 0px;border:1px solid #FFF">
                    	<ul style="width:310px">
                    		<li style="margin-bottom:0">
                            	<label for="sel_funcionario2">Funcionario 2</label>
                            	<select style="width:300px" id="sel_funcionario2" name="sel_funcionario2" class="sel_funcionario">
                                	<option value="0">selecionar</option>
<?									$rowFuncionarioVenda 	= mysql_fetch_array(mysql_query($sqlFuncaionario_Venda." AND fldOrdem = 2"));
									$rsFuncionario 			= mysql_query($sqlFuncionario);                        	
									while($rowFuncionario 	= mysql_fetch_array($rsFuncionario)){
?>										<option value="<?=$rowFuncionario['fldId']?>" <?= ($rowFuncionarioVenda['fldFuncionario_Id'] == $rowFuncionario['fldId'])? "selected='selected'" : '' ?>><?=$rowFuncionario['fldNome']?></option>
<?									}
?>              	    	    </select>	
							</li>
                            <li style="margin-bottom:0">
                                <label for="txa_funcionario2_servico">Servi&ccedil;o</label>
                                <textarea style="width:295px; height:50px" id="txa_funcionario2_servico" name="txa_funcionario2_servico" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>><?=$rowFuncionarioVenda['fldServico']?></textarea>
                            </li>
                            <li>
                                <label for="txt_funcionario2_tempo">Tempo</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario2_tempo" name="txt_funcionario2_tempo" value="<?=format_number_out($rowFuncionarioVenda['fldTempo'])?>" class="txt_funcionario_tempo" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txt_funcionario2_valor">Valor</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario2_valor" name="txt_funcionario2_valor" value="<?=format_number_out($rowFuncionarioVenda['fldValor'])?>" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                        </ul>
                    </fieldset>
<?					$totalServicos += $rowFuncionarioVenda['fldValor'];
?>
                    <fieldset style="width:310px;float:left; margin-top:7px;border:1px solid #FFF">
                    	<ul style="width:310px">
                            <li style="margin-bottom:0">
                                <label for="sel_funcionario3">Funcionario 3</label>
                                <select style="width:300px" id="sel_funcionario3" name="sel_funcionario3" class="sel_funcionario">
                                	<option value="0">selecionar</option>
<?									$rowFuncionarioVenda 	= mysql_fetch_array(mysql_query($sqlFuncaionario_Venda." AND fldOrdem = 3"));
									$rsFuncionario 			= mysql_query($sqlFuncionario);                        	
									while($rowFuncionario 	= mysql_fetch_array($rsFuncionario)){
?>										<option value="<?=$rowFuncionario['fldId']?>" <?= ($rowFuncionarioVenda['fldFuncionario_Id'] == $rowFuncionario['fldId'])? "selected='selected'" : '' ?>><?=$rowFuncionario['fldNome']?></option>
<?									}
?>              	       		</select>	
							</li>
                            <li style="margin-bottom:0">
                                <label for="txa_funcionario3_servico">Servi&ccedil;o</label>
                                <textarea style="width:295px; height:50px" id="txa_funcionario3_servico" name="txa_funcionario3_servico" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>><?=$rowFuncionarioVenda['fldServico']?></textarea>
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txt_funcionario3_tempo">Tempo</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario3_tempo" name="txt_funcionario3_tempo" value="<?=format_number_out($rowFuncionarioVenda['fldTempo'])?>" class="txt_funcionario_tempo" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                            <li>
                                <label for="txt_funcionario3_valor">Valor</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario3_valor" name="txt_funcionario3_valor" value="<?=format_number_out($rowFuncionarioVenda['fldValor'])?>" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                        </ul>
                    </fieldset>
<?					$totalServicos += $rowFuncionarioVenda['fldValor'];
?>
                    <ul>
                    	<li>
                            <label for="txa_outros_servicos">Outros servi&ccedil;os</label>
                            <textarea style="width:935px; height:65px" id="txa_outros_servicos" name="txa_outros_servicos"><?= $rowPedido['fldServico']?></textarea>
						</li>
					</ul>
                </div>
                
<?				//total pedido
				$totalTerceiros		= $rowPedido['fldValor_Terceiros'];
				
				$pedidoDesc 		= $rowPedido['fldDesconto'];
				$pedidoDescReais 	= $rowPedido['fldDescontoReais'];
				$pedidoSubTotal		= $totalProdutos + $totalServicos + $totalTerceiros;
				
				$pedidoDescTotal 	= ($pedidoSubTotal * $pedidoDesc) / 100;
				$pedidoTotal		= $pedidoSubTotal - $pedidoDescTotal;
				$pedidoTotal 		= $pedidoTotal - $pedidoDescReais;
?>
                
                <div id="pedido_pagamento" style="width:962px">
					<div class="pagamento_bottom" style=" width:515px;float:left">
                        <ul style="float:right; margin-right: 10px">
                            <li>
                                <label class="pedido" for="txt_pedido_produtos">Produtos</label>
                            	<input type="text" id="txt_pedido_produtos" name="txt_pedido_produtos" value="<?=format_number_out($totalProdutos)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_servicos">Servi&ccedil;os</label>
                            	<input type="text" id="txt_pedido_servicos" name="txt_pedido_servicos" value="<?=format_number_out($totalServicos)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_terceiros">Terceiros</label>
                            	<input style="background:#FEF9BA; height:25px; font-size:18px" type="text" id="txt_pedido_terceiros" name="txt_pedido_terceiros" value="<?=format_number_out($totalTerceiros)?>" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_subtotal">Subtotal</label>
                            	<input style="background:#D7F9C6; height:25px; font-size:18px" type="text" id="txt_pedido_subtotal" name="txt_pedido_subtotal" value="<?=format_number_out($pedidoSubTotal)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label for="txt_pedido_desconto">Desconto (%)</label>
                                <input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_desconto" name="txt_pedido_desconto" value="<?=format_number_out($pedidoDesc)?>" />
                            </li>
                            <li>
                                <label for="txt_pedido_desconto_reais">Desconto (R$)</label>
                                <input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_desconto_reais" name="txt_pedido_desconto_reais" value="<?=format_number_out($pedidoDescReais)?>" />
                            </li>
                            <li>
                                <label class="total" for="txt_pedido_total">Total</label>
                                <input type="text" class="total" id="txt_pedido_total" name="txt_pedido_total" value="<?=format_number_out($pedidoTotal)?>" readonly="readonly" />
                            </li>
                            <li>
                       			<a style="margin:0; float:right" class="modal btn_cheque" name="btn_cheque" id="btn_cheque" title="exibir cheques" href="financeiro_cheque_listar,3,<?=$pedido_id?>" rel="780-420">cheques</a>
                            </li>
						</ul>
                    </div>
<?
				require_once('componentes/parcelamento/parcelas_editar.php');
?>
                </div>
                <div style="float:right">
                    <input type="submit" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
                </div>
            </form>
        </div>
<?	}
?>        
    </div>
    
	<script type="text/javascript">
		var venda_origem = '<?= $rowPedido['fldPedido_Destino_Nfe_Id']?>';
		if(venda_origem > 0 ){
			$("form#frm_pedido_novo").find("input, select, checkbox, button, textarea").attr({"disabled":"disabled"});
		}
	</script>
