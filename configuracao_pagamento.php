<?Php
	require('configuracao_pagamento_action.php');
?>		
			<!-- TIPOS DE PAGAMENTO -->
			<div class="form" style="width:470px; float: left; margin: 10px 0 0 10px;">
				<h3>Tipos de Pagamento</h3>
				
				<div id="table" style="width:440px;">
					<div id="table_container" style="width:440px; height:200px; margin: 0 0 20px 0;">      
						<table id="table_general" class="table_general" summary="Tipos de pagamentos">
<?							$linha = "row";
							$sSQL = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0 ORDER BY fldTipo");
							while($rowPagamento = mysql_fetch_array($sSQL)){
?>								<tr class="<?= $linha; ?>">
									<td style="width:3px"></td>
									<td style="width:280px"><?=$rowPagamento['fldTipo']?></td>
									<td style="width:150px"><?=$rowPagamento['fldSigla']?></td>
<?									if($rowPagamento['fldId'] > 12){			                                
?>                              		<td style="width:20px"><a class="edit modal" href="configuracao_pagamento_tipo,<?=$rowPagamento['fldId']?>" rel="500-150" title="editar"></a></td>
										<td><a class="delete" style="width:20px" title="Excluir" href="?p=configuracao&modo=pagamento&delete=<?=$rowPagamento['fldId']?>" onclick="return confirm('Confirmar excluir?')"></a></td>
<?									}else{
										echo "<td style='width:20px'></td><td></td>";
									}
?>								</tr>
<?                     			$linha = ($linha == "row") ? "dif-row" : "row";
						   }
?>			 			</table>
					</div>
					
					<a style="float: right;" href="configuracao_pagamento_tipo" rel="500-160" class="btn_novo modal">novo</a>
				</div>
			</div>
			
			<!-- PERFIS DE PARCELAMENTO -->
			<div class="form" style="width:470px; float: right; margin: 10px 10px 0 0;">
				<h3>Perfis de Parcelamento</h3>
				
				<div id="table" style="width:440px;">
					<div id="table_container" style="width:440px;height:200px; margin: 0 0 20px 0;">      
						<table id="table_general" class="table_general" summary="Perfis de pagamentos">
<?							$linha = "row";
							$sSQL = mysql_query("SELECT * FROM tblsistema_pagamento_perfil WHERE fldId > 2 ORDER BY fldPerfil");
							while($rowIntervalo = mysql_fetch_array($sSQL)){
?>								<tr class="<?= $linha; ?>">
									<td style="width:3px"></td>
									<td style="width:99%"><?=$rowIntervalo['fldPerfil']?></td>
<?									if($rowIntervalo['fldId'] > 2){			                                
?>                              		<td style="width:20px"><a class="edit modal" href="configuracao_pagamento_perfil,<?=$rowIntervalo['fldId']?>" rel="500-360" title="Editar"></a></td>
										<td><a class="delete" style="width:20px" title="Excluir" href="?p=configuracao&modo=pagamento&delete-perfil=<?=$rowIntervalo['fldId']?>" onclick="return confirm('Confirmar excluir?')"></a></td>
<?									} else {
										echo "<td style='width:20px'></td><td></td>";
									}
?>								</tr>
<?                     			$linha = ($linha == "row") ? "dif-row" : "row";
							}
?>			 		</table>
					</div>
					
					<a style="float: right;" href="configuracao_pagamento_perfil" rel="500-360" class="btn_novo modal">novo</a>
				</div>
			</div>
			
			<div class="form" style="width: 470px; float: left; margin: 30px 0 0 10px;">
				<h3>Dia pr&eacute;-definido de parcelamento</h3>
				<form id="frm_dia_predefinido_parcelamento" class="frm_detalhe" style="width: 100%; float: left; margin: 0 0 0 6px;" action="" method="post">
					<ul>
						<li style="padding: 8px; background:#D7D7D7"> 
							<label for="txt_dia_parcelamento">Dia</label>
							<input type="text" id="txt_dia_predefinido_parcelamento" name="txt_dia_predefinido_parcelamento" value="<?= (fnc_sistema('parcelamento_dia_predefinido') != 'd') ? fnc_sistema('parcelamento_dia_predefinido') : 'Autom&aacute;tico'; ?>" style="width: 100px; text-align: center;" onkeyup="numOnly(this);" />
						</li>
					</ul>
					<input style="float: right; margin: 20px 20px 0 0" type="submit" class="btn_enviar" name="btn_enviar_dia_predefinido" value="Resetar Dia" >
					<input style="float: right; margin: 20px 20px 0 0" type="submit" class="btn_enviar" name="btn_enviar_dia_predefinido" value="Gravar" >
				</form>
			</div>
			
<script type="text/javascript">
	
	//validar intervalo de dias na configuração de Dia de Parcelamento
	$('input[value="Gravar"]').click(function() {
		
		var dia = parseInt($('#txt_dia_predefinido_parcelamento').val(), 10);
		
		if(isNaN(dia)) { alert('Dia inserido não é válido! Digite apenas números inteiros.'); return false; }
		if(dia < 1 || dia > 31) { alert('Dia inserido não é válido como data!'); return false; }
		
		return true;
		
	});
	
</script>