<?php

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_data1'] = "";
		$_SESSION['txt_data2'] = "";
		$_SESSION['sel_fornecedor'] = "";
		$_SESSION['txt_valor1'] = "";
		$_SESSION['txt_valor2'] = "";
	}
	else{
		$_SESSION['txt_data1'] = (isset($_POST['txt_data1']) ? $_POST['txt_data1'] : $_SESSION['txt_data1']);
		$_SESSION['txt_data2'] = (isset($_POST['txt_data2']) ? $_POST['txt_data2'] : $_SESSION['txt_data2']);
		$_SESSION['sel_fornecedor'] = (isset($_POST['sel_fornecedor']) ? $_POST['sel_fornecedor'] : $_SESSION['sel_fornecedor']);
		$_SESSION['txt_valor1'] = (isset($_POST['txt_valor1']) ? $_POST['txt_valor1'] : $_SESSION['txt_valor1']);
		$_SESSION['txt_valor2'] = (isset($_POST['txt_valor2']) ? $_POST['txt_valor2'] : $_SESSION['txt_valor2']);
	}

?>
<form id="frm-filtro" action="" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
        <li>
            <select style="width:300px" id="sel_fornecedor" name="sel_fornecedor" >
                <option value="">Fornecedor</option>
<?				$rsFornecedor = mysql_query("SELECT tblfornecedor.fldId as FornecedorId,
												tblfornecedor.fldNomeFantasia,
										   		tblproduto_cotacao.*
												FROM tblproduto_cotacao INNER JOIN tblfornecedor ON tblproduto_cotacao.fldFornecedor_Id = tblfornecedor.fldId");
				while($rowFornecedor = mysql_fetch_array($rsFornecedor)){
?>					<option <?=($_SESSION['sel_fornecedor'] == $rowFornecedor['FornecedorId']) ? 'selected="selected"' : '' ?> value="<?=$rowFornecedor['FornecedorId']?>"><?=$rowFornecedor['fldNomeFantasia']?></option>
<?				}
?>			</select>
		</li>
        <li>
<?			$valor1 = ($_SESSION['txt_valor1'] ? $_SESSION['txt_valor1'] : "");        
?>        	<label for="txt_valor1">Valor entre:</label>
    		<input style="width: 100px" type="text" name="txt_valor1" id="txt_valor1" value="<?=$valor1?>"/>
        </li>
        <li>
<?			$valor2 = ($_SESSION['txt_valor2'] ? $_SESSION['txt_valor2'] : "");   
?>    		<input style="width: 100px" type="text" name="txt_valor2" id="txt_valor2" value="<?=$valor2?>"/>
        </li>
        <li>
      		<label for="txt_data1">Per&iacute;odo: </label>
<?			$data1 = ($_SESSION['txt_data1'] ? $_SESSION['txt_data1'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 80px" type="text" name="txt_data1" id="txt_data1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
		<li>
<?			$data2 = ($_SESSION['txt_data2'] ? $_SESSION['txt_data2'] : "");
?>     		<input title="Data final" style="text-align:center;width: 80px" type="text" name="txt_data2" id="txt_data2" class="calendario-mask" value="<?=$data2?>"/>
      	</li>
    </ul>

    <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
    <button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
  </fieldset>
</form>

<?
	
	if(($_SESSION['sel_fornecedor']) != ""){
		
		$filtro .= " AND tblproduto_cotacao.fldFornecedor_Id = ".$_SESSION['sel_fornecedor'];
	}
	
	
	if($_SESSION['txt_valor1'] != ""){

		if($_SESSION['txt_valor2'] != ""){
			$filtro .= " AND tblproduto_cotacao.fldValor BETWEEN ".$_SESSION['txt_valor1']." AND ".$_SESSION['txt_valor2'];
		}else{
			$filtro .= " AND tblproduto_cotacao.fldValor <= ".$_SESSION['txt_valor1'];
		}
	}
	
	if(format_date_in($_SESSION['txt_data1']) != ""){

		if(format_date_in($_SESSION['txt_data2']) != ""){
			$filtro .= " AND tblproduto_cotacao.fldData BETWEEN '".format_date_in($_SESSION['txt_data1'])."' AND '".format_date_in($_SESSION['txt_data2'])."'";
		}
		else{
			$filtro .= " AND tblproduto_cotacao.fldData = '".format_date_in($_SESSION['txt_data1'])."'";
		}
	}
	
	
	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_produto_cotacao'] = $filtro;
	}

	else{
		$_SESSION['filtro_produto_cotacao'] = $filtro;
	}
?>
