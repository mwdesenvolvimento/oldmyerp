<?

require("inc/con_db.php");
require("inc/fnc_general.php");
require('fpdf/fpdf.php');
####################################### cabeçalho #################################################
$consulta= mysql_query("SELECT fldRazao_Social,fldCPF_CNPJ,fldEndereco,fldNumero,fldBairro,fldTelefone1 FROM tblempresa_info");
	$rowResultado = mysql_fetch_assoc($consulta);
	$razaoSocal=	$rowResultado['fldRazao_Social'];
class PDF extends FPDF
{
// Page header{$dados_empresa['fldNome_Fantasia']} (".format_cnpj_out($dados_empresa['fldCPF_CNPJ']).")", 0, 0, 'R');
function Header()
	
	
{
	$consulta= mysql_query("SELECT fldRazao_Social,fldCPF_CNPJ,fldEndereco,fldNumero,fldBairro,fldTelefone1 FROM tblempresa_info");
	$rowResultado = mysql_fetch_assoc($consulta);
	$razaoSocial=	$rowResultado['fldRazao_Social'];
	$cpf_cnpj=	format_cnpj_out($rowResultado['fldCPF_CNPJ']);
	$endereco=	$rowResultado['fldEndereco'];
	$numero=	$rowResultado['fldNumero'];
	$bairro=	$rowResultado['fldBairro'];
	$telefone=	$rowResultado['fldTelefone1'];
	
	$this->SetFont('Arial', '',10);
	$this->Cell(200,5,"$razaoSocial.    CNPJ $cpf_cnpj",'','','C');
        $this->Ln();
        $this->Cell(200,5,"$endereco,$numero - $bairro, Telefone: $telefone",'','','C');
        $this->Ln(3);
	
	
	$dataInicial=	$_GET["dataInicial"];
	$dataFinal=	$_GET["dataFinal"];
	
	$dataInicial_convertida = format_date_in($dataInicial);
	$dataFinal_convertida = format_date_in($dataFinal);
	
	$this->Ln(10);
	$this->SetFont('Arial','B',13);
	$this->Cell(137,5,"Relatório de Produtos Vendidos Por Cliente",'','','L');
	$this->SetFont('Arial','B',11);
	$this->Cell(17,5,"Período:");
	$this->Cell(70,5,"$dataInicial a $dataFinal");
	$this->Ln(9);	
	
	
	
	
	
		
}

	
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,$this->PageNo().'/{nb}',0,0,'C');
}
}

###################################################################################################
     
        $pdf = new PDF('P', 'mm', 'A4');
	//pode ser P (portrait) ou L (landscape)
	
	$pdf->AliasNbPages();
	//isso faz com que funcione o total de paginas
	
	$pdf->SetMargins(6,6,6);
	//defino as margens do documento...
	
	$pdf->AddPage();
	
	
	
	
	$dataInicial=	$_GET["dataInicial"];
	$dataFinal=	$_GET["dataFinal"];
	
	$dataInicial_convertida = format_date_in($dataInicial);
	$dataFinal_convertida = format_date_in($dataFinal);
	
	
	
##################################### consulta 	#############################################

	$consultaProduto= mysql_query("SELECT tblproduto.fldNome,tblpedido_item.fldPedido_Id,tblpedido_item.fldValor,tblproduto.fldCodigo,
					tblproduto.fldId as idProduto,
					SUM(tblpedido_item.fldQuantidade) as quantidade,
					SUM(tblpedido_item.fldValor*tblpedido_item.fldQuantidade) as total
					
					FROM tblpedido
					INNER JOIN tblpedido_item
					ON tblpedido.fldId= tblpedido_item.fldPedido_Id
					LEFT JOIN tblproduto
					ON tblproduto.fldId=tblpedido_item.fldProduto_Id
					WHERE tblpedido.fldExcluido=0
					AND tblpedido_item.fldExcluido=0
					AND tblpedido.fldStatus !=1
					AND tblpedido.fldPedido_Destino_Nfe_Id is null
					AND fldPedidoData between '$dataInicial_convertida' and '$dataFinal_convertida'
					GROUP BY tblproduto.fldId");
							
#############################################################################################
	
	
	

	
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(19,6,"Código",'B','','');
	$pdf->Cell(143,6,"Produto",'B','','');
	$pdf->Cell(18,6,"Qtde",'B','','C');
	$pdf->Cell(19,6,"Total",'B','','R');
	
	
	$resultado=array();
	
	
	
	while($rowResultado = mysql_fetch_assoc($consultaProduto)){
		$idProduto=	$rowResultado['idProduto'];
		$cod= $rowResultado['fldCodigo'];
		$produto= $rowResultado['fldNome'];
		$quantidade= format_number_out($rowResultado['quantidade']);
		$total= format_number_out($rowResultado['total']);
		
	
	$pdf->Ln(7);
	
	$pdf->SetFont('Arial','B',9);
	$pdf->Cell(19,6,"$cod",'','','');
	$pdf->Cell(143,6,"$produto",'','','');
	$pdf->Cell(18,6,"$quantidade",'','','C');
	$pdf->Cell(19,6,"$total",'','','R');
	$pdf->Ln();
	
	
	
	
	
	
	
	
	
########################################### clientes #########################################
			$consultaCliente= mysql_query("SELECT tblcliente.fldNomeFantasia,tblcliente.fldNome,tblpedido_item.fldPedido_Id,tblpedido_item.fldValor,tblproduto.fldCodigo,
						SUM(tblpedido_item.fldQuantidade) as quantidade,
						SUM(tblpedido_item.fldValor*tblpedido_item.fldQuantidade) as total
                                                
                                                FROM tblpedido
                                                INNER JOIN tblpedido_item
                                                ON tblpedido.fldId= tblpedido_item.fldPedido_Id
                                                LEFT JOIN tblcliente
                                                ON tblcliente.fldId=tblpedido.fldCliente_Id
                                                INNER JOIN tblproduto
                                                ON tblpedido_item.fldProduto_Id=tblproduto.fldId
                                                WHERE tblpedido.fldExcluido=0
                                                AND tblpedido_item.fldExcluido=0
                                                AND tblpedido.fldStatus !=1
                                                AND tblpedido.fldPedido_Destino_Nfe_Id is null
                                                AND fldPedidoData between  '$dataInicial_convertida' and '$dataFinal_convertida'
                                                AND tblpedido_item.fldProduto_Id = $idProduto
                                                GROUP BY tblpedido.fldCliente_Id");
			
					
				while($rowResultado = mysql_fetch_assoc($consultaCliente)){
	
					
					$cliente=	$rowResultado['fldNomeFantasia'];
					$clienteNome=	$rowResultado['fldNome'];
					$qtde=		format_number_out($rowResultado['quantidade']);
					$totalCliente=	format_number_out($rowResultado['total']);
					
					
					
					
					if($cliente==''&& $cliente==''){
						$pdf->SetFont('Arial','',10);
						$pdf->Cell(162,5,"$clienteNome");
						$pdf->Cell(18,5,"$qtde",'','','C');
						$pdf->Cell(19,5,"$totalCliente",'','','R');
						$pdf->Ln();
						
					}
					else{
					
						$pdf->SetFont('Arial','',10);
						$pdf->Cell(162,5,"$cliente");
						$pdf->Cell(18,5,"$qtde",'','','C');
						$pdf->Cell(19,5,"$totalCliente",'','','R');
						$pdf->Ln();
					}
					
				$quantidadeTotal=$quantidadeTotal+$rowResultado['quantidade'];
				$quantidadeTotalFormatada= format_number_out($quantidadeTotal);
				$valorTotal=$valorTotal+$rowResultado['total'];
				$valorTotalFormatado =format_number_out($valorTotal);
				
	
		
				}
			
	
		
		
		
	}
	
	
	
	
	
	$pdf->Ln(13);
	$pdf->SetX(115);
	$pdf->SetFont('Arial','B',11);
	$pdf->Cell(23,5,"Total Geral:");
	$pdf->SetFont('Arial','',11);
	$pdf->Cell(30,5,"$quantidadeTotalFormatada");
	$pdf->SetFont('Arial','B',11);
	$pdf->Cell(7,5,"Total:");
	$pdf->SetFont('Arial','',11);
	$pdf->Cell(30,5,"R$ $valorTotalFormatado",'','','R');
	$pdf->Ln(13);
	
	
	
	
	
	
	$pdf->Output();
?>


