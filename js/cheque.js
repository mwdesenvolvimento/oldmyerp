
$(document).ready(function(){
	var urlDestino = 'filtro_ajax.php';
					   
	$('tr.tr_cheque_listar').live('click',function(){
		var chequeId = $(this).attr('id');
		var telaId = $('#hid_tela').attr('value');
		var referenciaId = $('#hid_ref').attr('value');
		
		$('tr.tr_cheque_listar').removeClass('selected');
		$(this).addClass("selected");
		
		$("[name=btn_seta]").attr({
			"href": chequeId
		});
		
		$.post("filtro_ajax.php", {cheque_editar:chequeId, telaId:telaId, referenciaId:referenciaId}, function(theXML){
			$('dados',theXML).each(function(){
			
				var banco 		= $(this).find("banco").text();
				var agencia 	= $(this).find("agencia").text();
				var conta 		= $(this).find("conta").text();
				var cheque 		= $(this).find("cheque").text();
				var valor 		= $(this).find("valor").text();
				var vencimento 	= $(this).find("vencimento").text();
				var nome 		= $(this).find("nome").text();
				var cpfCNPJ 	= $(this).find("cpfCNPJ").text();
				var entidade	= $(this).find("entidade").text();
					
				$('#txt_banco').val(banco);	
				$('#txt_agencia').val(agencia);	
				$('#txt_conta').val(conta);	
				$('#txt_cheque').val(cheque);	
				$('#txt_valor').val(valor);	
				$('#txt_vencimento').val(vencimento);	
				$('#txt_nome').val(nome);	
				$('#txt_CPF_CNPJ').val(cpfCNPJ);	
				$('#txt_entidade').val(entidade);	
				$('#hid-edit').val(chequeId);	
			});
		});
	});					   
	/*					   
	/****************************************************************************************/
	
	$('[name=btn_seta]').live('click',function(event){
		event.preventDefault();
		
		var chequeId = $(this).attr('href');
		var classe 	 = $(this).attr('class');
		
		var telaId 		 = $('#hid_tela').attr('value');
		var referenciaId = $('#hid_ref').attr('value');
		
		if(classe == 'seta_right'){
			$.post(urlDestino,{cheque_inserir:chequeId, telaId:telaId, referenciaId:referenciaId},function(valor){
				$("table#tbl_cheque_inserido tbody").html(valor);
			})
			$('table#tbl_cheque_exibir tr#'+chequeId).remove();
		}else if(classe == 'seta_left'){
			$.post(urlDestino,{cheque_remover:chequeId},function(valor){
				$("table#tbl_cheque_exibir tbody").html(valor);
			})
			$('table#tbl_cheque_inserido tr#'+chequeId).remove();
		}
		
		
		//limpa os campos do form de edicao
		$('#txt_banco').val('');	
		$('#txt_agencia').val('');	
		$('#txt_conta').val('');	
		$('#txt_cheque').val('');	
		$('#txt_valor').val('');	
		$('#txt_vencimento').val('');	
		$('#txt_nome').val('');	
		$('#txt_CPF_CNPJ').val('');	
		$('#txt_entidade').val('');	
		$('#hid-edit').val('');	

	});	
	
});