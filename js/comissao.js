
$(document).ready(function(){
	
	$('form#frm_funcionario :input[id^=sel_funcao]').change(function(){
		funcao_id 	= $(this).val();	
		input_name 	= $(this).attr('name').split('funcao');		
		
		$.get('filtro_ajax.php', {sel_funcionario_funcao_comissao:funcao_id}, function(theXML){
			$('dados',theXML).each(function(){
											
				var comissao 	= $(this).find("comissao").text();	
				$('form#frm_funcionario #txt_comissao'+input_name[1]).val(comissao);
			});
		});
		
		if($(this).val() > 0){
			$('#txt_comissao'+input_name[1]).attr({"readonly":""});
		}else{
			$('#txt_comissao'+input_name[1]).attr({"readonly":"readonly"});
		}
	});
	
	$('input[id^=txt_comissao]').change(function(){
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
	});
	
	//COMISSAO DO FUNCIONARIO - BAIXA
	//mudando o valor dos campos select das parcelas
	$('form select#sel_comissao').change(function(){
		var x = 1;
		var totalComissoes	= $('table.tbl_comissao tr').length;
		TotalBaixaValor = 0;
		for(x = 1; x <= totalComissoes; x++) {
			
			var comissao 	 		= Number($("form#frm_comissao_baixa input#hid_comissao_" + x).val()).toFixed(2);
			var comissao_valor 		= Number($("form#frm_comissao_baixa input#hid_comissao_valor_" + x).val()).toFixed(2);
			var valor_total 		= Number($("form#frm_comissao_baixa input#hid_valor_pedido_" + x).val()).toFixed(2);
			var valor_pedido_pago	= Number($("form#frm_comissao_baixa input#hid_valor_pedido_pago_" + x).val()).toFixed(2);
			var valor_comissao_pago	= Number($("form#frm_comissao_baixa input#hid_valor_comissao_pago_" + x).val()).toFixed(2);
			
			if($(this).val() == 'pago'){
				porcentagemLiberada 	= (100 / valor_total) * valor_pedido_pago;
				comissaoLiberadaValor	= (porcentagemLiberada / 100 ) * comissao_valor;
				comissaoLiberadaValor 	= comissaoLiberadaValor.toFixed(2);
			}else{
				comissaoLiberadaValor = comissao_valor;
			}
			
			baixaValor 	= comissaoLiberadaValor - valor_comissao_pago;
			if(baixaValor < 0){
				baixaValor = 0;
			}
			$('form input#hid_valor_comissao_baixa_' + x).val(float2br(baixaValor.toFixed(2)));
			$('form input#txt_comissao_liberada_' + x).val(float2br(comissaoLiberadaValor));
			$('form input#txt_valor_baixa_' + x).val(float2br(baixaValor.toFixed(2)));
			
			TotalBaixaValor += baixaValor;
		}
		
		$('form input#txt_comissao_liberada_' + x).val(float2br(comissaoLiberadaValor));
		$('form input#txt_total_pagar').val(float2br(TotalBaixaValor.toFixed(2)));
		$('form input#txt_total_baixa').val(float2br(TotalBaixaValor.toFixed(2)));
	});
	
	$('form#frm_comissao_baixa .valor_baixa').change(function(){
		input_name 		= $(this).attr('name').split('_');
		valor_digitado 	= br2float(float2br(ifNumberNull($(this).val())));
		valor_baixa 	= br2float($("#hid_valor_comissao_baixa_"+input_name[3]).val()).toFixed(2);
		
		if(valor_digitado > valor_baixa){
			$(this).val(float2br(Number(valor_baixa).toFixed(2)));
		}else{
			$(this).val(float2br(valor_digitado.toFixed(2)));
		}
		
		var x = 1;
		var totalComissoes	= $('table.tbl_comissao tr').length;
		TotalBaixaValor = 0;
		for(x = 1; x <= totalComissoes; x++) {
			TotalBaixaValor += br2float($('form input#txt_valor_baixa_' + x).val());
		}
		$('form input#txt_total_baixa').val(float2br(TotalBaixaValor.toFixed(2)));
	});
	
	
	//VERIFICA SE NAO ESTA TENTANDO DAR BAIXA EM PARCELAS TOTALMENTE PAGAS
	$("#frm_funcionario_comissao #btn_baixa").click(function(){
		var checkboxs = document.getElementsByTagName("INPUT");// varre todas as inputs conferindo se � um checkbox e se est� checked
		for (loop = 0; loop < checkboxs.length; loop++){
			var item = checkboxs[loop];
			if (item.type == "checkbox" && item.checked && $(item).attr('class') == 'pago'){            
				alert('Uma ou mais comiss�es selecionadas j� est� paga!');  
				return false;
			}
		}
	});
	
});