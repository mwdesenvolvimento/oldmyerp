$(document).ready(function(){
	
	$('.modal').live('click', function(e){
		e.preventDefault();
		winModal($(this));
	});
	
});

function winModal(anchor){
	//capturando os valores de altura e largura da janela modal
	var medidasJanelaModal = $(anchor).attr('rel').split('-');
	
	
	//altura e largura
	var lar 	= $(window).width(); 	//largura da janela -> usado no #modal-body
	var alt 	= $(document).height(); //altura do documento -> usado no #modal-body
	var winAlt  = $(window).height(); 	//altura da janela -> para centralizar o modal referente a janela
	
	//objeto literal com propriedades css para centralizar a janela modal
	cssCentralizar = {
		'width' : medidasJanelaModal[0],
		'height' : medidasJanelaModal[1],
		'position' : 'absolute',
		'top' : (winAlt/2),
		'left' : '50%',
		'margin' : '-' + (medidasJanelaModal[1]/2) + 'px 0 0 ' + '-' + (medidasJanelaModal[0]/2) + 'px'
	}
	
	cssBody = {
		'width' : '100%',
		'height' : '100%',
		'position' : 'fixed',
		'background' : 'rgba(0, 0, 0, 0.5)',
		'z-index' : '100000'
	}
	
		//alert("len: "+$('div.modal-body').length);
		//if(!$('div.modal-body').length){
		
		var id = $('div.modal-body').size() + 1;
		
		$('body').append('<div id="modal-body_'+id+'" class="modal-body"></div>');
		$('div.modal-body:last').css(cssBody);
		
		$('div.modal-body:last').append('<div class="janela-modal"></div>');
		
		$('div.janela-modal:last').css(cssCentralizar);
		
		//pegando os parametros da url para passar ao arquivo a ser carregado e campo para foco no retorno
		var params = $(anchor).attr('href').split(',');
		
		$('div.janela-modal:last').append('<div class="modal-cabecalho"><h3>MyERP</h3><a href="#" rel="'+params[1]+'" class="modal-fechar" title="Fechar">Fechar</a></div>');
		$('div.janela-modal:last').append('<div class="modal-conteudo"></div>');
		
		
		
		//carregando o arquivo com o conte�do da janela modal
		$('div.modal-conteudo:last').load('modal/'+params[0]+'.php', {'params' : params});
		
		
	/****** FECHAR MODAL ******/
	//para fechar a janela modal ao clicar no link
	$('a.modal-fechar').live('click', function(e){
		e.preventDefault();
		$(this).parents('div.modal-body:last').remove();
		var foco_retorno = $(this).attr('rel');
		$('#'+foco_retorno).focus();
		
	});
	
	//para fechar a janela modal ao teclar esc
	$('body').live('keyup', function(event){
		if(event.keyCode == 27) {
			if($('div.modal-body').length) {
				//obtendo nome do campo que deve receber o foco ao sair do modal
				var foco_retorno = $('a.modal-fechar:last').attr('rel');
				
				$('div.modal-body:last').remove();
				//anulando o keycode para evitar propaga��o. Ex.: Duas janelas modais sobrepostas, ao fechar a �ltima aberta evita de imediatamente fechar a de baixo.
				event.keyCode = null;
				//dando foco ao campo correto
				$('#'+foco_retorno).focus();
			}
		}
	});

}