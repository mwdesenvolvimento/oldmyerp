
	 function varGlobal(){
		vendaDecimal 									= 	$("#hid_venda_decimal").val();
		quantidadeDecimal 								= 	$("#hid_quantidade_decimal").val();
		nfe_item_principal_quantidade_comercial       	=   br2float($("#txt_nfe_item_principal_quantidade_comercial").val());
		nfe_item_principal_valor_unitario_comercial		=   br2float($("#txt_nfe_item_principal_valor_unitario_comercial").val());
		
		nfe_icms_base_calculo     						=	br2float($("#txt_nfe_item_tributos_icms_vBC").val());
		nfe_icms_aliquota           					=	br2float($("#txt_nfe_item_tributos_icms_pICMS").val());
		
		nfe_icms_valor									=   br2float($("#txt_nfe_item_tributos_icms_vICMS").val());
		nfe_icms_reducao_base_calculo					=   br2float($("#txt_nfe_item_tributos_icms_pRedBC").val());
		
		nfe_icms_st_aliquota	               			=   br2float($("#txt_nfe_item_tributos_icms_st_pICMSST").val());
		
		nfe_icms_st_base_calculo               			=   br2float($("#txt_nfe_item_tributos_icms_st_vBCST").val());
		nfe_icms_st_reducao_base_calculo       			=   br2float($("#txt_nfe_item_tributos_icms_st_pRedBCST").val());
		nfe_icms_st_mva									=   br2float($("#txt_nfe_item_tributos_icms_st_pMVAST").val());
		
		nfe_ipi											=   br2float($("#txt_nfe_item_tributos_ipi_valor").val());

		itemQuantidade 									= br2float($('#txt_nfe_item_principal_quantidade_comercial').val());
		itemValor 										= br2float($('#txt_nfe_item_principal_valor_unitario_comercial').val());
		itemDespesasAcess								= br2float($('#txt_nfe_item_principal_outras_despesas_acessorias').val());
		itemSeguro										= br2float($('#txt_nfe_item_principal_total_seguro').val());
		itemDesconto									= br2float($('#txt_nfe_item_principal_desconto').val());
		itemFrete										= br2float($('#txt_nfe_item_principal_total_seguro').val());
		valorAliquotaIPI								= br2float($('#txt_nfe_item_tributos_ipi_aliquota').val());
	 }
	 
	//CALCULANDO BASE DO IPI
	function calculoIPI(){
		varGlobal();
		var baseIPI = (itemValor * itemQuantidade) + itemDespesasAcess + itemSeguro;																   
		var valorIPI = (valorAliquotaIPI / 100) * baseIPI;
		
		if($('#txt_nfe_item_tributos_ipi_valor_base_calculo').attr("disabled") == false){
			$('#txt_nfe_item_tributos_ipi_valor_base_calculo').val(float2br(baseIPI.toFixed(2)));
			
			if(!isNaN(valorIPI)){
				$('#txt_nfe_item_tributos_ipi_valor').val(float2br(valorIPI.toFixed(2)));
			}else{
				valorIPI = 0;
				$('#txt_nfe_item_tributos_ipi_valor').val('');
			}
		}
		/*CALCULANDO BASE DO ICMS
		/###### esse seria o calculo que pegamos do blog, mas segundo contador, a BC do ICMS seria apenas o total bruto do produto, e tb, soh em algus casos entra o IPI, entao segue novo calculo abaixo
			var baseICMS = itemValor + itemDespesasAcess + itemSeguro + itemFrete - itemDesconto;
			if($('#txt_nfe_item_tributos_ipi_valor_base_calculo').attr("disabled") == false){
				baseICMS = baseICMS + valorIPI;
			}
		
		if(!isNaN(baseICMS)){
			$('#txt_nfe_item_tributos_icms_vBC').val(float2br(baseICMS.toFixed(2)));
		}else{
			$('#txt_nfe_item_tributos_icms_vBC').val('');
		}
		$('#txt_nfe_item_tributos_icms_pRedBC').val('');
		$('#txt_nfe_item_tributos_icms_pICMS').val('');
		$('#txt_nfe_item_tributos_icms_vICMS').val('');
		*/
	}
	
	function calculoBCICMS(){
		//CALCULANDO BASE DO ICMS
		varGlobal();
		var baseICMS = itemQuantidade * itemValor;
		
		if(!isNaN(baseICMS)){
			$('#txt_nfe_item_tributos_icms_vBC').val(float2br(baseICMS.toFixed(2)));
		}else{
			$('#txt_nfe_item_tributos_icms_vBC').val('');
		}
	}
	
    //Calcular o valor total bruto.
    function total_nfe_valor_total_bruto(){
		varGlobal();
		
		var nfe_calculo_valor_total_bruto = nfe_item_principal_quantidade_comercial * nfe_item_principal_valor_unitario_comercial;
		
		if(!isNaN(nfe_calculo_valor_total_bruto)){
			$("#txt_nfe_item_principal_valor_total_bruto").val(float2br(nfe_calculo_valor_total_bruto.toFixed(2)));
		}else{
			$("#txt_nfe_item_principal_valor_total_bruto").val('');
		}
    }
    
    //Calcular ICMS 00 - CONFIRMADO!!!
    function total_nfe_icms00(){
		varGlobal();
		
        var nfe_icms_calculo_valor = nfe_icms_base_calculo * (nfe_icms_aliquota / 100);
		if(!isNaN(nfe_icms_calculo_valor)){
			$("#txt_nfe_item_tributos_icms_vICMS").val(float2br(nfe_icms_calculo_valor.toFixed(2)));
		}else{
			$("#txt_nfe_item_tributos_icms_vICMS").val('');
		}
    }
	
	//Calcular ICMS 10. -> parte do ST - CONFIRMADO!!!
	//esse calculo tb pode ser usado no CSOSN 201
    function total_nfe_icms1030(){
		varGlobal();
		
		//aqui vou calcular o valor do ICMS proprio, pois de acordo com contador, ele pega esse icms pra calcular o ST depois - qualquer coisa diferente na hora de emitir a nota, rever essa parte
		//fazer uma verificacao pra ver se entra o valor do ipi ou nao, segundo contador, em algumas situacoes entra ou nao, nda especifico por empresa
		//esse era o calculo que peguei do blog =>
		
		//segundo contador, esse seria o calculo =>
		var valorBruto = nfe_item_principal_quantidade_comercial * nfe_item_principal_valor_unitario_comercial;
		var baseCalculoST = valorBruto + (valorBruto * (nfe_icms_st_mva/100)); 
		$("#txt_nfe_item_tributos_icms_st_vBCST").val(float2br(baseCalculoST.toFixed(2)));
		
		var valorAliquota 	= nfe_icms_st_aliquota / 100;
		var valorICMSST 	= (baseCalculoST * valorAliquota);
		if(!isNaN(valorICMSST)){
			$("#txt_nfe_item_tributos_icms_st_vICMSST").val(float2br(valorICMSST.toFixed(2)));
		}else{
			$("#txt_nfe_item_tributos_icms_st_vICMSST").val('');
		}
    }
	
	
	 function total_nfe_icms202(){
		varGlobal();
		
		//aqui vou calcular o valor do ICMS proprio, pois de acordo com contador, ele pega esse icms pra calcular o ST depois - qualquer coisa diferente na hora de emitir a nota, rever essa parte
		//fazer uma verificacao pra ver se entra o valor do ipi ou nao, segundo contador, em algumas situacoes entra ou nao, nda especifico por empresa
		//esse era o calculo que peguei do blog =>
		
		//segundo contador, esse seria o calculo =>
		var valorBruto = nfe_item_principal_quantidade_comercial * nfe_item_principal_valor_unitario_comercial;
		var baseCalculoST = valorBruto + (valorBruto * (nfe_icms_st_mva/100)); 
		$("#txt_nfe_item_tributos_icms_st_vBCST").val(float2br(baseCalculoST.toFixed(2)));
		
		var valorAliquota 	= nfe_icms_st_aliquota / 100;
		
		var valorICMSNormal = valorBruto * ((nfe_icms_st_aliquota)/100);
		var valorICMSST = (baseCalculoST * valorAliquota) - valorICMSNormal;
		if(!isNaN(valorICMSST)){
			$("#txt_nfe_item_tributos_icms_st_vICMSST").val(float2br(valorICMSST.toFixed(2)));
		}else{
			$("#txt_nfe_item_tributos_icms_st_vICMSST").val('');
		}
    }
	
	//Calcular ICMS 20 - CONFIRMADO!!!
    function total_nfe_icms20(){
		varGlobal();
		
		var reducaoBC		= (nfe_icms_reducao_base_calculo/100) * nfe_icms_base_calculo ;
		var	valorBCICMS   	= nfe_icms_base_calculo - reducaoBC;
		var	valorICMS   	= (nfe_icms_aliquota/100) * valorBCICMS;
		
		//TIREI A REDUCAO DO CALCULO ACIMA PQ NAO BATIA CO O EMISSOR, DEIXEI APENAS O CALCULO SIMPLES DO ICMS	
		
		//var valorICMS = nfe_icms_base_calculo * (nfe_icms_aliquota / 100);
		 if(!isNaN(reducaoBC)){
			$("#txt_nfe_item_tributos_icms_vBC").val(float2br(valorBCICMS.toFixed(2)));
		}else{
			$("#txt_nfe_item_tributos_icms_pRedBC").val('0,00');
		}
			
		if(!isNaN(valorICMS)){
			$("#txt_nfe_item_tributos_icms_vICMS").val(float2br(valorICMS.toFixed(2)));
		}else{
			$("#txt_nfe_item_tributos_icms_vICMS").val('');
		}
    }
	/*
	//Calcular ICMS 30 - CONFIRMADO!!!
    function total_nfe_icms30(){
		varGlobal();
		
		//aqui vou calcular o valor do ICMS proprio, pois de acordo com contador, ele pega esse icms pra calcular o ST depois - qualquer coisa diferente na hora de emitir a nota, rever essa parte
		//fazer uma verificacao pra ver se entra o valor do ipi ou nao, segundo contador, em algumas situacoes entra ou nao, nda especifico por empresa
		//esse era o calculo que peguei do blog =>
		/*
		var nfe_icms_valor = 0;
		var baseCalculoST = (nfe_icms_st_base_calculo + nfe_ipi) * ((100 + nfe_icms_st_mva)/100); 
		
		
		//segundo contador, esse seria o calculo =>
		var valorBruto = nfe_item_principal_quantidade_comercial * nfe_item_principal_valor_unitario_comercial;
		var baseCalculoST = valorBruto * ((100 + nfe_icms_st_mva)/100); 
		$("#txt_nfe_item_tributos_icms_st_vBCST").val(float2br(baseCalculoST.toFixed(2)));
		
		var valorAliquota = nfe_icms_st_aliquota / 100;
		var valorICMSNormal = 100 * valorAliquota;
	
		var valorICMSST = (baseCalculoST * valorAliquota) - valorICMSNormal;
		if(!isNaN(valorICMSST)){
			$("#txt_nfe_item_tributos_icms_st_vICMSST").val(float2br(valorICMSST.toFixed(2)));
		}else{
			$("#txt_nfe_item_tributos_icms_st_vICMSST").val('');
		}
    }
	*/
	//Calcular ICMS 51 - CONFIRMADO!!!
    function total_nfe_icms51(){
		varGlobal();
		
		var aliquotaICMS	= nfe_icms_aliquota/100;
		var	valorICMS   	= nfe_icms_base_calculo * aliquotaICMS;
		if(!isNaN(valorICMS)){
			$("#txt_nfe_item_tributos_icms_vICMS").val(float2br(valorICMS.toFixed(2)));
		}else{
			$("#txt_nfe_item_tributos_icms_vICMS").val('');
		}
    }
	
	/*
	//Calcular ICMS 60
    function total_nfe_icms60(){
		varGlobal();
		
		$("#txt_nfe_item_tributos_icms_vICMS").val(float2br(valorICMS.toFixed(2)));//Inserir o resultado no campo de ICMS.
		
		$BaseCalculo = ((nfe_item_principal_quantidade_comercial * $BaseCalculoUnit)+($Qtd2 * $BaseCalculoUnit2));
		$ValorST  = (($Qtd * $VlICMSST)+($Qtd2 * $VlICMSST2));
		/*
		
		$Operacao = "Entarda";
		$Operacao2 = "Entarda";
		$Qtd	= 10;  	 	
		$Qtd2	= 10;
		$BaseCalculoUnit  = 150;
		$BaseCalculoUnit2 = 300;
		$VlICMSST = 8.5;
		$VlICMSST2 = 17;
		
		$BaseCalculo = (($Qtd * $BaseCalculoUnit)+($Qtd2 * $BaseCalculoUnit2));
		$ValorST  = (($Qtd * $VlICMSST)+($Qtd2 * $VlICMSST2));
		
    }
	*/
	
	//Calcular ICMS 70 - CONFIRMADO!!!
    function total_nfe_icms70(){ 
		varGlobal();
		
		//Calculo Base de ICMS Pr�prio com Redu��o:
		baseReduzida = ((nfe_icms_base_calculo * (100 - nfe_icms_reducao_base_calculo))/100 );
		
		//Calculo Base ICMS ST
		baseICMSST = (nfe_icms_base_calculo + nfe_ipi)*(100 + nfe_icms_st_mva)/100;
		if(!isNaN(baseICMSST)){
			$("#txt_nfe_item_tributos_icms_st_vBCST").val(float2br(baseICMSST.toFixed(2)));
		}else{
			$("#txt_nfe_item_tributos_icms_st_vBCST").val('');
		}
		
		//Calculo Base Reduzida ICMS ST
		baseReduzidaST = baseICMSST * (100 - nfe_icms_st_reducao_base_calculo)/100;
		
		//Valor ICMS Pr�prio
		valorICMSSTProprio = (baseReduzida * nfe_icms_aliquota) /100;
		
		//Valor ICMS ST
		valorICMSST = ((baseReduzidaST * nfe_icms_st_aliquota) /100) - valorICMSSTProprio;
		if(!isNaN(valorICMSST)){
			$("#txt_nfe_item_tributos_icms_st_vBCST").val(float2br(valorICMSST.toFixed(2)));
		}else{
			$("#txt_nfe_item_tributos_icms_st_vBCST").val('');
		}
    }
	
	/*
	//Calcular CSOSN 101
    function total_nfe_icms20(){
		varGlobal();
		
		var aliquotaICMS	= nfe_icms_aliquota/100;
		var reducaoICMS 	= nfe_icms_base_calculo * (1 - nfe_icms_reducao_base_calculo);
		var	valorICMS   	= reducaoICMS * aliquotaICMS;
		$("#txt_nfe_item_tributos_icms_vICMS").val(float2br(valorICMS.toFixed(2)));//Inserir o resultado no campo de ICMS.
    }
	*/
	
	function total_tributos(){
		//
	}

$(document).ready(function(){
	
//### Calculo o desconto ###################################################################################################################################################################################################################################################################################################################
	$('#txt_nfe_item_principal_desconto_percent, #txt_nfe_item_principal_quantidade_comercial').live('change',function(){
		 varGlobal();
		 
		if($(this).val() != ''){
			desconto = Number(br2float($('#txt_nfe_item_principal_desconto_percent').val()));
			desconto_total = ((nfe_item_principal_valor_unitario_comercial * desconto) / 100) * nfe_item_principal_quantidade_comercial;
			
			$('#txt_nfe_item_principal_desconto_percent').val(float2br(br2float($('#txt_nfe_item_principal_desconto_percent').val()).toFixed(2)));
			$('#txt_nfe_item_principal_desconto').val(float2br(desconto_total.toFixed(2)));
		}else{
			$('#txt_nfe_item_principal_desconto_percent').val('0,00');
		}
	});
	
	$('#txt_nfe_item_principal_desconto').live('change',function(){
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		$('#txt_nfe_item_principal_desconto_percent').val('0,00');
	
	});

//### Calculo ICMS 00 ###############################################################################################################################################################################################################################################################################################################################################
	$('#txt_nfe_item_tributos_icms_vBC, #txt_nfe_item_tributos_icms_pICMS').live('change',function(){
		total_nfe_icms00();
		if($(this).attr('type') == 'text'){
			if($(this).val() != ''){
				$(this).val(float2br(br2float($(this).val()).toFixed(2)));
			}
		}
	});

//### Calculo ICMS 10 ST e 20 ###############################################################################################################################################################################################################################################################################################################################################
	$("#txt_nfe_item_principal_quantidade_comercial, #txt_nfe_item_principal_valor_unitario_comercial, #txt_nfe_item_tributos_icms_vBC, #txt_nfe_item_tributos_icms_vICMS, #sel_nfe_item_tributos_icms_situacao_tributaria, #txt_nfe_item_tributos_icms_pICMS, #txt_nfe_item_tributos_icms_pRedBCST, #txt_nfe_item_tributos_icms_st_pICMSST, #txt_nfe_item_tributos_icms_st_pRedBCST, #txt_nfe_item_tributos_icms_pRedBC, #txt_nfe_item_tributos_icms_st_pMVAST").live('blur',function(){
		
		total_nfe_valor_total_bruto();
		var selValor = $("#sel_nfe_item_tributos_icms_situacao_tributaria").val();
		calculoBCICMS();
		$('#icms_st_202').hide();
		if(selValor == 2 || selValor == 12 || selValor == 13 || selValor == 14 || selValor == 21 || selValor == 25){
			total_nfe_icms1030();
			total_nfe_icms00();
		}else if(selValor == 22 || selValor == 23){
			$('#icms_st_202').show();
			total_nfe_icms202();
		}else if(selValor == 3){
			total_nfe_icms20();
		}else if(selValor == 4){
			total_nfe_icms1030();
		}else if(selValor == 10){
			total_nfe_icms20();
			total_nfe_icms70();
		}else if(selValor == 8){
			total_nfe_icms51();
		}
		
		if($(this).attr('type') == 'text'){
			if($(this).attr("id") == 'txt_nfe_item_principal_valor_unitario_comercial'){
				$(this).val(float2br(br2float($(this).val()).toFixed(vendaDecimal)));
			}else if($(this).attr("id") == 'txt_nfe_item_principal_quantidade_comercial'){
				$(this).val(float2br(br2float($(this).val()).toFixed(quantidadeDecimal)));
			}else{
				$(this).val(float2br(br2float($(this).val()).toFixed(2)));
			}
		}
	});

	//### Calculo IPI ###############################################################################################################################################################################################################################################################################################################################################
		$('#txt_nfe_item_principal_quantidade_comercial, #txt_nfe_item_principal_valor_unitario_comercial, #txt_nfe_item_principal_outras_despesas_acessorias, #txt_nfe_item_principal_desconto, #txt_nfe_item_principal_frete, #txt_nfe_item_principal_total_seguro, #txt_nfe_item_tributos_ipi_valor_base_calculo, #txt_nfe_item_tributos_ipi_aliquota, #sel_nfe_item_tributos_ipi_calculo_tipo').live('blur',function(){
			calculoIPI();
		});
		
		//AO SELECIONAR SITUA��O TRIBUTARIA, HABILITA/DESABILITA SELECT DE TIPO DE CALCULO
		$('[name$=_ipi_situacao_tributaria]').live('change',function(){
																			   
			$('[name$=_ipi_calculo_tipo]').attr("disabled",true);
			$('[name$=_ipi_calculo_tipo]').val('');
			$('#li_calculo_ipi_js ~ li input:not(#txt_nfe_item_tributos_ipi_valor)').attr("disabled",true);
			$('#li_calculo_ipi_js ~ li input:disabled').css({backgroundColor: '#F1F1F1'});
			$('#li_calculo_ipi_js ~ li input').val('');
			
			if($(this).val() == 1 || $(this).val() == 7 || $(this).val() == 8 || $(this).val() == 14){
				$('[name$=_ipi_calculo_tipo]').removeAttr("disabled");
			}
		});
																			
		//OP��ES DO SELECT, HABILITAR E DESABILITAR CAMPOS QUE SERAO USADOS
		$('[name$=_ipi_calculo_tipo]').live('change',function(){
			//remove todos os readonly dos inputs, menos o do valor do ipi, que tem o id abaixo
			$('#li_calculo_ipi_js ~ li input:not(#txt_nfe_item_tributos_ipi_valor)').removeAttr("disabled");
			$('#li_calculo_ipi_js ~ li input').css({backgroundColor: '#FFF'});
			$('#li_calculo_ipi_js ~ li input').val("");
			
			if($(this).attr('value') == 0){
				
				$('#li_calculo_ipi_js ~ li input:not(#txt_nfe_item_tributos_ipi_valor)').attr("disabled",true);
				$('#li_calculo_ipi_js ~ li input').css({backgroundColor: '#F1F1F1'});
				
			}else if($(this).attr('value') == 1){
	
				$('#txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total').attr("disabled",true);
				$('#txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total').css({backgroundColor: '#F1F1F1'});
				$('[name$=_ipi_unidade_valor]').attr("disabled",true);
				$('[name$=_ipi_unidade_valor]').css({backgroundColor: '#F1F1F1'});
				calculoIPI();
			}else if($(this).attr('value') == 2){
				
				$('#txt_nfe_item_tributos_ipi_valor_base_calculo').attr("disabled",true);
				$('#txt_nfe_item_tributos_ipi_valor_base_calculo').css({backgroundColor: '#F1F1F1'});
				$('[name$=_ipi_aliquota]').attr("disabled",true);
				$('[name$=_ipi_aliquota]').css({backgroundColor: '#F1F1F1'});
			}
		});
		
		//calculo do valor do ipi
		$('#txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total, #txt_nfe_item_tributos_ipi_unidade_valor').live('blur',function(){
			var qtdeTotalUnidade 	= br2float($('#txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total').val());
			var valorUnidade 		= br2float($('#txt_nfe_item_tributos_ipi_unidade_valor').val());
			valorIPI = valorUnidade  * qtdeTotalUnidade;
			
			if(!isNaN(valorIPI)){
				$('#txt_nfe_item_tributos_ipi_valor').val(float2br(valorIPI.toFixed(2)));
			}else{
				$('#txt_nfe_item_tributos_ipi_valor').val('');
			}
		});	
	
	//### Calculo PIS ###############################################################################################################################################################################################################################################################################################################################################
		//AO SELECIONAR SITUA��O TRIBUTARIO, HABILITA/DESABILITA SELECT DE TIPO DE CALCULO
		$('[name$=_pis_situacao_tributaria]').live('change',function(){
																				
			$('[name$=_pis_calculo_tipo]').attr("disabled",true);
			$('[name$=_pis_calculo_tipo]').val('');
			
			$('#li_calculo_pis_js ~ li input:not(#txt_nfe_item_tributos_pis_valor)').attr("disabled",true);
			$('#li_calculo_pis_js ~ li input:disabled').css({backgroundColor: '#F1F1F1'});
			$('#li_calculo_pis_js ~ li input').val('');
			
			if($(this).val() < 3){
				$('#txt_nfe_item_tributos_pis_valor_base_calculo').removeAttr("disabled");
				$('#txt_nfe_item_tributos_pis_valor_base_calculo').css({backgroundColor: '#FFF'});
				$('[name$=_pis_aliquota_percentual]').removeAttr("disabled");
				$('[name$=_pis_aliquota_percentual]').css({backgroundColor: '#FFF'});
				
			}else if($(this).val() == 3){
				$('#txt_nfe_item_tributos_pis_quantidade_vendida').removeAttr("disabled");
				$('#txt_nfe_item_tributos_pis_quantidade_vendida').css({backgroundColor: '#FFF'});
				$('[name$=_pis_aliquota_reais]').removeAttr("disabled");
				$('[name$=_pis_aliquota_reais]').css({backgroundColor: '#FFF'});
				
			}else if($(this).val() >= 9){
				$('[name$=_pis_calculo_tipo]').removeAttr("disabled");
			}
		});
		
		//OP��ES DO SELECT, HABILITAR E DESABILITAR CAMPOS QUE SERAO USADOS
		$('[name$=_pis_calculo_tipo]').live('change',function(){
			
			$('#li_calculo_pis_js ~ li input:not(#txt_nfe_item_tributos_pis_valor)').removeAttr("disabled");
			$('#li_calculo_pis_js ~ li input').css({backgroundColor: '#FFF'});
			$('#li_calculo_pis_js ~ li input').val('');
			
			if($(this).attr('value') == 0){
				$('#li_calculo_pis_js ~ li input:not(#txt_nfe_item_tributos_pis_valor)').attr("disabled",true);
				$('#li_calculo_pis_js ~ li input').css({backgroundColor: '#F1F1F1'});
				
			}else if($(this).attr('value') == 1){
	
				$('#txt_nfe_item_tributos_pis_quantidade_vendida').attr("disabled",true);
				$('#txt_nfe_item_tributos_pis_quantidade_vendida').css({backgroundColor: '#F1F1F1'});
				$('[name$=_pis_aliquota_reais]').attr("disabled",true);
				$('[name$=_pis_aliquota_reais]').css({backgroundColor: '#F1F1F1'});
				
			}else if($(this).attr('value') == 2){
				
				$('#txt_nfe_item_tributos_pis_valor_base_calculo').attr("disabled",true);
				$('#txt_nfe_item_tributos_pis_valor_base_calculo').css({backgroundColor: '#F1F1F1'});
				$('[name$=_pis_aliquota_percentual]').attr("disabled",true);
				$('[name$=_pis_aliquota_percentual]').css({backgroundColor: '#F1F1F1'});
			}
		});
		
		//calculo valor do pis por reais
		$('#txt_nfe_item_tributos_pis_quantidade_vendida, #txt_nfe_item_tributos_pis_aliquota_reais').live('blur',function(){
			var qtdeTotalUnidade 	= br2float($('#txt_nfe_item_tributos_pis_quantidade_vendida').val());
			var valorAliquota 		= br2float($('#txt_nfe_item_tributos_pis_aliquota_reais').val());
			valorPIS = valorAliquota  * qtdeTotalUnidade;
			
			if(!isNaN(valorPIS)){
				$('#txt_nfe_item_tributos_pis_valor').val(float2br(valorPIS.toFixed(2)));
			}else{
				$('#txt_nfe_item_tributos_pis_valor').val('');
			}
		});	
		
		//calculo valor do pis por porcentagem
		$('#txt_nfe_item_tributos_pis_valor_base_calculo, #txt_nfe_item_tributos_pis_aliquota_percentual').live('blur',function(){
			var valorBC 			= br2float($('#txt_nfe_item_tributos_pis_valor_base_calculo').val());
			var valorAliquota		= br2float($('#txt_nfe_item_tributos_pis_aliquota_percentual').val());
			valorPIS = (valorAliquota / 100) * valorBC;
			
			if(!isNaN(valorPIS)){
				$('#txt_nfe_item_tributos_pis_valor').val(float2br(valorPIS.toFixed(2)));
			}else{
				$('#txt_nfe_item_tributos_pis_valor').val('');
			}
		});	
		
		//### Calculo PIS ST ###############################################################################################################################################################################################################################################################################################################################################
		//OP��ES DO SELECT, HABILITAR E DESABILITAR CAMPOS QUE SERAO USADOS
		$('[name$=_pis_st_calculo_tipo]').live('change',function(){
			//remove todos os readonly dos inputs, menos o do valor do ipi, que tem o id abaixo
			$('#li_calculo_pisST_js ~ li input:not(#txt_nfe_item_tributos_pis_st_valor)').removeAttr("disabled");
			$('#li_calculo_pisST_js ~ li input').val("");
			$('#li_calculo_pisST_js ~ li input').css({backgroundColor: '#FFF'});
			
			if($(this).attr('value') == 0){
				$('#li_calculo_pisST_js ~ li input:not(#txt_nfe_item_tributos_pis_st_valor)').attr("disabled",true);
				$('#li_calculo_pisST_js ~ li input').css({backgroundColor: '#F1F1F1'});
				
			}else if($(this).attr('value') == 1){
	
				$('#txt_nfe_item_tributos_pis_st_quantidade_vendida').attr("disabled",true);
				$('#txt_nfe_item_tributos_pis_st_quantidade_vendida').css({backgroundColor: '#F1F1F1'});
				$('[name$=_pis_st_aliquota_reais]').attr("disabled",true);
				$('[name$=_pis_st_aliquota_reais]').css({backgroundColor: '#F1F1F1'});
				
			}else if($(this).attr('value') == 2){
				
				$('#txt_nfe_item_tributos_pis_st_valor_base_calculo').attr("disabled",true);
				$('#txt_nfe_item_tributos_pis_st_valor_base_calculo').css({backgroundColor: '#F1F1F1'});
				$('[name$=_pis_st_aliquota_percentual]').attr("disabled",true);
				$('[name$=_pis_st_aliquota_percentual]').css({backgroundColor: '#F1F1F1'});
			}
		});
		
		//calculo pis ST por reais
		$('#txt_nfe_item_tributos_pis_st_quantidade_vendida, #txt_nfe_item_tributos_pis_st_aliquota_reais').live('blur',function(){
			var qtdeTotalUnidade 	= br2float($('#txt_nfe_item_tributos_pis_st_quantidade_vendida').val());
			var valorAliquota 		= br2float($('#txt_nfe_item_tributos_pis_st_aliquota_reais').val());
			valorPISST = valorAliquota  * qtdeTotalUnidade;
			
			if(!isNaN(valorPISST)){
				$('#txt_nfe_item_tributos_pis_st_valor').val(float2br(valorPISST.toFixed(2)));
			}else{
				$('#txt_nfe_item_tributos_pis_st_valor').val('');
			}
		});	
		//calculo pis ST por porcentagem
		$('#txt_nfe_item_tributos_pis_st_valor_base_calculo, #txt_nfe_item_tributos_pis_st_aliquota_percentual').live('blur',function(){
			var valorBC 			= br2float($('#txt_nfe_item_tributos_pis_st_valor_base_calculo').val());
			var valorAliquota		= br2float($('#txt_nfe_item_tributos_pis_st_aliquota_percentual').val());
			
			valorPISST = (valorAliquota / 100) * valorBC;
			if(!isNaN(valorPISST)){
				$('#txt_nfe_item_tributos_pis_st_valor').val(float2br(valorPISST.toFixed(2)));
			}else{
				$('#txt_nfe_item_tributos_pis_st_valor').val('');
			}
		});	
	
	//### Calculo COFINS ###############################################################################################################################################################################################################################################################################################################################################
		//AO SELECIONAR SITUA��O TRIBUTARIO, HABILITA/DESABILITA SELECT DE TIPO DE CALCULO
	
		//alguns seletores pego pelo final do nome, pois funciona no modal e na tela de cadastro do produto, e soh o fim do nome � igual
		$('[name$=_cofins_situacao_tributaria]').live('change',function(){
			$('[name$=_cofins_calculo_tipo]').attr("disabled",true);
			$('[name$=_cofins_calculo_tipo]').val('');
			$('#li_calculo_cofins_js ~ li input:not(#txt_nfe_item_tributos_cofins_valor)').attr("disabled",true);
			$('#li_calculo_cofins_js ~ li input:disabled').css({backgroundColor: '#F1F1F1'});
			$('#li_calculo_cofins_js ~ li input').val('');
			
			if($(this).val() == 1 || $(this).val() == 2){
				$('#txt_nfe_item_tributos_cofins_valor_base_calculo').removeAttr("disabled");
				$('#txt_nfe_item_tributos_cofins_valor_base_calculo').css({backgroundColor: '#FFF'});
				$('[name$=_cofins_aliquota_percentual]').removeAttr("disabled");
				$('[name$=_cofins_aliquota_percentual]').css({backgroundColor: '#FFF'});
				
			}else if($(this).val() == 3){
				$('#txt_nfe_item_tributos_cofins_quantidade_vendida').removeAttr("disabled");
				$('#txt_nfe_item_tributos_cofins_quantidade_vendida').css({backgroundColor: '#FFF'});
				$('[name$=_cofins_aliquota_reais]').removeAttr("disabled");
				$('[name$=_cofins_aliquota_reais]').css({backgroundColor: '#FFF'});
				
			}else if($(this).val() >= 10){
				$('[name$=_cofins_calculo_tipo]').removeAttr("disabled");
			}
		});		
		
		//OP��ES DO SELECT, HABILITAR E DESABILITAR CAMPOS QUE SERAO USADOS
		$('[name$=_cofins_calculo_tipo]').live('change',function(){
			//remove todos os readonly dos inputs, menos o do valor do ipi, que tem o id abaixo
			$('#li_calculo_cofins_js ~ li input:not(#txt_nfe_item_tributos_cofins_valor)').removeAttr("disabled");
			$('#li_calculo_cofins_js ~ li input').val("");
			$('#li_calculo_cofins_js ~ li input').css({backgroundColor: '#FFF'});
			
			if($(this).attr('value') == 0){
				$('#li_calculo_cofins_js ~ li input:not(#txt_nfe_item_tributos_cofins_valor)').attr("disabled",true);
				$('#li_calculo_cofins_js ~ li input').css({backgroundColor: '#F1F1F1'});
				
			}else if($(this).attr('value') == 1){
	
				$('#txt_nfe_item_tributos_cofins_quantidade_vendida').attr("disabled",true);
				$('#txt_nfe_item_tributos_cofins_quantidade_vendida').css({backgroundColor: '#F1F1F1'});
				$('[name$=_cofins_aliquota_reais]').attr("disabled",true);
				$('[name$=_cofins_aliquota_reais]').css({backgroundColor: '#F1F1F1'});
				
			}else if($(this).attr('value') == 2){
				
				$('#txt_nfe_item_tributos_cofins_valor_base_calculo').attr("disabled",true);
				$('#txt_nfe_item_tributos_cofins_valor_base_calculo').css({backgroundColor: '#F1F1F1'});
				$('[name$=_cofins_aliquota_percentual]').attr("disabled",true);
				$('[name$=_cofins_aliquota_percentual]').css({backgroundColor: '#F1F1F1'});
			}
		});
		
		//calculo cofins por reais
		$('#txt_nfe_item_tributos_cofins_quantidade_vendida, #txt_nfe_item_tributos_cofins_aliquota_reais').live('blur',function(){
			var qtdeTotalUnidade 	= br2float($('#txt_nfe_item_tributos_cofins_quantidade_vendida').val());
			var valorAliquota 		= br2float($('#txt_nfe_item_tributos_cofins_aliquota_reais').val());
			
			valorCOFINS = valorAliquota  * qtdeTotalUnidade;
			if(!isNaN(valorCOFINS)){
				$('#txt_nfe_item_tributos_cofins_valor').val(float2br(valorCOFINS.toFixed(2)));
			}else{
				$('#txt_nfe_item_tributos_cofins_valor').val('');
			}
		});	
		//calculo cofins por porcentagem
		$('#txt_nfe_item_tributos_cofins_valor_base_calculo, #txt_nfe_item_tributos_cofins_aliquota_percentual').live('blur',function(){
			var valorBC 			= br2float($('#txt_nfe_item_tributos_cofins_valor_base_calculo').val());
			var valorAliquota		= br2float($('#txt_nfe_item_tributos_cofins_aliquota_percentual').val());
			
			valorCOFINS = (valorAliquota / 100) * valorBC;
			if(!isNaN(valorCOFINS)){
				$('#txt_nfe_item_tributos_cofins_valor').val(float2br(valorCOFINS.toFixed(2)));
			}else{
				$('#txt_nfe_item_tributos_cofins_valor').val('');
			}
		});	
		
	//### Calculo COFINS ST###############################################################################################################################################################################################################################################################################################################################################
		//OP��ES DO SELECT, HABILITAR E DESABILITAR CAMPOS QUE SERAO USADOS
		$('[name$=_cofins_st_calculo_tipo]').live('change',function(){
			//remove todos os readonly dos inputs, menos o do valor do ipi, que tem o id abaixo
			$('#li_calculo_cofinsST_js ~ li input:not(#txt_nfe_item_tributos_cofins_st_valor)').removeAttr("disabled");
			$('#li_calculo_cofinsST_js ~ li input').val("");
			$('#li_calculo_cofinsST_js ~ li input').css({backgroundColor: '#FFF'});
			
			if($(this).attr('value') == 0){
				$('#li_calculo_cofinsST_js ~ li input:not(#txt_nfe_item_tributos_cofins_st_valor)').attr("disabled",true);
				$('#li_calculo_cofinsST_js ~ li input').css({backgroundColor: '#F1F1F1'});
				
			}else if($(this).attr('value') == 1){
	
				$('#txt_nfe_item_tributos_cofins_st_quantidade_vendida').attr("disabled",true);
				$('#txt_nfe_item_tributos_cofins_st_quantidade_vendida').css({backgroundColor: '#F1F1F1'});
				$('[name$=_cofins_st_aliquota_reais]').attr("disabled",true);
				$('[name$=_cofins_st_aliquota_reais]').css({backgroundColor: '#F1F1F1'});
				
			}else if($(this).attr('value') == 2){
				
				$('#txt_nfe_item_tributos_cofins_st_valor_base_calculo').attr("disabled",true);
				$('#txt_nfe_item_tributos_cofins_st_valor_base_calculo').css({backgroundColor: '#F1F1F1'});
				$('[name$=_cofins_st_aliquota_percentual]').attr("disabled",true);
				$('[name$=_cofins_st_aliquota_percentual]').css({backgroundColor: '#F1F1F1'});
			}
		});
		
		//calculo cofins st por reais
		$('#txt_nfe_item_tributos_cofins_st_quantidade_vendida, #txt_nfe_item_tributos_cofins_st_aliquota_reais').live('blur',function(){
			var qtdeTotalUnidade 	= br2float($('#txt_nfe_item_tributos_cofins_st_quantidade_vendida').val());
			var valorAliquota 		= br2float($('#txt_nfe_item_tributos_cofins_st_aliquota_reais').val());
			
			valorCOFINSST = valorAliquota  * qtdeTotalUnidade;
			if(!isNaN(valorCOFINSST)){
				$('#txt_nfe_item_tributos_cofins_st_valor').val(float2br(valorCOFINSST.toFixed(2)));
			}else{
				$('#txt_nfe_item_tributos_cofins_st_valor').val('');
			}
		});	
		//calculo cofins st por porcentagem
		$('#txt_nfe_item_tributos_cofins_st_valor_base_calculo, #txt_nfe_item_tributos_cofins_st_aliquota_percentual').live('blur',function(){
			var valorBC 			= br2float($('#txt_nfe_item_tributos_cofins_st_valor_base_calculo').val());
			var valorAliquota		= br2float($('#txt_nfe_item_tributos_cofins_st_aliquota_percentual').val());
			
			valorCOFINSST = (valorAliquota / 100) * valorBC;
			if(!isNaN(valorCOFINSST)){
				$('#txt_nfe_item_tributos_cofins_st_valor').val(float2br(valorCOFINSST.toFixed(2)));
			}else{
				$('#txt_nfe_item_tributos_cofins_st_valor').val('');
			}
		});	
		
		//### Calculo ISSQN ###############################################################################################################################################################################################################################################################################################################################################
		$('#txt_nfe_item_tributos_issqn_valor_base_calculo, #txt_nfe_item_tributos_issqn_aliquota').live('blur',function(){
			var valorBC 			= br2float($('#txt_nfe_item_tributos_issqn_valor_base_calculo').val());
			var valorAliquota		= br2float($('#txt_nfe_item_tributos_issqn_aliquota').val());
			
			valorISSQN = (valorAliquota / 100) * valorBC;
			if(!isNaN(valorISSQN)){
				$('#txt_nfe_item_tributos_issqn_valor').val(float2br(valorISSQN.toFixed(2)));
			}else{
				$('#txt_nfe_item_tributos_issqn_valor').val('');
			}
		});	
		
		
	//### ESCONDENDO ABAS EM TIPO DE TRIBUTACAO ###############################################################################################################################################################################################################################################################################################################################################
	$('[name=rad_imposto]').live('click',function(){
			//FAZ SUMIR AS ABAS E DIVS QUE NAO TEM EM CADA TIPO DE IMPOSTO
			rad 	 = $(this).val();
			divAtual = $('#pedido_nfe_item_tributos_abas li a.ativo').attr('href');
			
			if($('#pedido_nfe_item_tributos_abas li a.ativo').parent('li').attr('class') != rad){
				$('div#pedido_nfe_item_tributos_'+divAtual).hide();
			}
			
			$('#pedido_nfe_item_tributos_abas li a').removeClass('ativo');
			$('#pedido_nfe_item_tributos_abas li').hide();
			$('li.'+rad).show();
				
			divNova = $('#pedido_nfe_item_tributos_abas > li:visible a').attr('href');
			$('#pedido_nfe_item_tributos_'+divNova).show();
			$('#pedido_nfe_item_tributos_abas li a[href='+divNova+']').addClass('ativo');
					  
			//TIRA A CLASS DE INVISIVEL E DEPOIS RECOLOCA S� NOS INPUTS QUE NAO TIVEREM SENDO USADOS, PRA NA HR DE GRAVAR, SEREM ZERADOS 
			if(rad == 'icms'){
				$.each($("#pedido_nfe_item_tributos_ISSQN :input"),function() {
					$('#pedido_nfe_item_tributos_ISSQN :input').addClass('invisible');
				});
				
			}else if(rad == 'issqn'){
				$.each($("#pedido_nfe_item_tributos_ISSQN :input"),function() {
					$('#pedido_nfe_item_tributos_ISSQN :input').removeClass('invisible');
				});
			}	
		});
		
	//# CALCULAR CREDITO ICMS QUE PODE SER APROVEITADO ###################################################################################################################################################################################################################################################################################################################################################
	$('#txt_nfe_item_tributos_icms_pCredSN, #txt_nfe_item_principal_valor_unitario_comercial').live('blur',function(){
		var total_bruto = br2float($('#txt_nfe_item_principal_valor_total_bruto').val());
		var aliquota	= br2float($('#txt_nfe_item_tributos_icms_pCredSN').val());
		
		var credito = (total_bruto * aliquota) / 100;
		$('#txt_nfe_item_tributos_icms_vCredICMSSN').val(float2br(ifNumberNull(credito.toFixed(2))));
	});
			
	//FORMATANDO CAMPOS ###############################################################################################################################################################################################################################################################################################################################################
	var campos = " [id$='icms_pCredSN'], [id$='icms_pRedBC'], [id$='icms_st_pRedBCST'], [id$='icms_pICMS'], [id$='icms_st_pICMSST'], [id$='txt_icms_pBCOp'], [id$='icms_st_pMVAST'],#txt_nfe_item_principal_valor_unidade_tributavel, #txt_nfe_item_tributos_icms_st_vBCST, [id$='icms_st_pICMSST'], #txt_nfe_item_tributos_ipi_selo_controle_quantidade, #txt_nfe_item_tributos_ipi_valor_base_calculo, #txt_nfe_item_tributos_ipi_aliquota, #txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total, #txt_nfe_item_tributos_ipi_unidade_valor, ";
	var campos = campos + "#txt_nfe_item_tributos_pis_valor_base_calculo, #txt_nfe_item_tributos_pis_aliquota_percentual, #txt_nfe_item_tributos_pis_aliquota_reais, #txt_nfe_item_tributos_pis_quantidade_vendida, ";
	var campos = campos + "#txt_nfe_item_tributos_pis_st_valor_base_calculo, #txt_nfe_item_tributos_pis_st_aliquota_percentual, #txt_nfe_item_tributos_pis_st_aliquota_reais, #txt_nfe_item_tributos_pis_st_quantidade_vendida, ";
	var campos = campos + "#txt_nfe_item_tributos_cofins_valor_base_calculo, #txt_nfe_item_tributos_cofins_aliquota_percentual, #txt_nfe_item_tributos_cofins_aliquota_reais, #txt_nfe_item_tributos_cofins_quantidade_vendida, ";
	var campos = campos + "#txt_nfe_item_tributos_cofins_st_valor_base_calculo, #txt_nfe_item_tributos_cofins_st_aliquota_percentual, #txt_nfe_item_tributos_cofins_st_aliquota_reais, #txt_nfe_item_tributos_cofins_st_quantidade_vendida, ";
	var campos = campos + "#txt_nfe_item_tributos_ii_valor_base_calculo, #txt_nfe_item_tributos_ii_valor_aduaneiras, #txt_nfe_item_tributos_ii_valor_iof, #txt_nfe_item_tributos_ii_valor, ";
	var campos = campos + "#txt_nfe_item_tributos_issqn_valor_base_calculo, #txt_nfe_item_tributos_issqn_aliquota, ";
	var campos = campos + "#txt_nfe_item_principal_valor_total_bruto, #txt_nfe_item_principal_frete, #txt_nfe_item_principal_desconto, #txt_nfe_item_principal_outras_despesas_acessorias, #txt_nfe_item_principal_quantidade_comercial, #txt_nfe_item_principal_total_seguro, #txt_nfe_item_principal_quantidade_tributavel, ";
	var campos = campos + "#txt_ipi_aliquota, #txt_ipi_unidade_padrao_quantidade_total, #txt_ipi_unidade_valor";
	
	$(campos).live('blur',function(){
	
		if($(this).attr('type') == 'text'){
			
			if($(this).val() != ''){
				
				if($(this).attr("id") == 'txt_nfe_item_principal_quantidade_comercial'){
					$(this).val(float2br(br2float($(this).val()).toFixed(quantidadeDecimal)));
				}else{
					$(this).val(float2br(br2float($(this).val()).toFixed(2)));
				}
			}
		}
	});
	
	//####################################################################################################################################################################################################################################################################################################################################################
})