
function PadDigits(n){
	n = n.toString();
	var pd = '';
	if (2 > n.length) { 
		for (i=0; i < (2 - n.length); i++) { 
			pd += '0'; 
		 } 
	} 
	 return pd + n.toString();
} 

function varGlobalPedido(){
	vendaDecimal 				= $("#hid_venda_decimal").val();
	quantidadeDecimal 			= $("#hid_quantidade_decimal").val();

	if($("ul#pedido_modo_aba").length > 0){
		valorFuncionario1		= Number(ifNumberNull(br2float($('#txt_funcionario1_valor').val())));
		valorFuncionario2		= Number(ifNumberNull(br2float($('#txt_funcionario2_valor').val())));		
		valorFuncionario3		= Number(ifNumberNull(br2float($('#txt_funcionario3_valor').val())));	
	}
	
	valorTotalProdutos 			= Number(ifNumberNull(br2float($('#txt_pedido_produtos').val())));
	valorServicoTerceiros		= Number(ifNumberNull(br2float($('#txt_pedido_terceiros').val())));
	valorSubTotalPedido 		= Number(ifNumberNull(br2float($('#txt_pedido_subtotal').val())));
	
	valorDesconto_moeda	 		= Number(ifNumberNull(br2float($('#txt_pedido_desconto_reais').val())));
	valorDesconto_percentual	= Number(ifNumberNull(br2float($('#txt_pedido_desconto').val())));
 	pedidoProdutos 				= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_produtos').val())));
	pedidoBCICMS				= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_bc_icms').val())));
	pedidoICMS 					= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_icms').val())));
	pedidoBCICMSST				= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_bc_icms_substituicao').val())));
	pedidoICMSST				= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_icms_substituicao').val())));
	pedidoFrete					= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_frete').val())));
	pedidoSeguro				= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_seguro').val())));
	pedidoDesconto				= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_desconto').val())));
	pedidoII					= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_ii').val())));
	pedidoIPI					= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_ipi').val())));
	pedidoPIS					= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_pis').val())));
	pedidoCOFINS				= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_cofins').val())));
	pedidoBCISS					= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_bc_iss').val())));
	pedidoISS					= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_iss').val())));
	pedidoOutDespAcess			= Number(ifNumberNull(br2float($('#txt_pedido_nfe_outras_despesas_acessorias').val())));
	pedidoPISTotalServicos		= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_pis_servicos').val())));
	pedidoCOFINSTotalServicos	= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_cofins_servicos').val())));
	pedidoTotal					= Number(ifNumberNull(br2float($('#txt_pedido_total').val())));
	pedidoTotalNFe				= Number(ifNumberNull(br2float($('#txt_pedido_nfe_total_nfe').val())));
}
 
//PEDIDO
$(document).ready(function(){
	/*----------------------------------------------------------------------------------------------*/	
		//BUSCA DE CFOP
		$('#txt_nfe_item_principal_cfop').live('keyup',function(event){
			if (event.keyCode == '120'){
				anchor = $(this).next('a');
				winModal(anchor);
			}
		});
		
	/*----------------------------------------------------------------------------------------------*/	
	//APAGA O QUE ESTA ESCRITO A GANHAR FOCO
	textboxes = $("input[type=text], input[type=submit], input[type=password], select, textarea, checkbox, button");
	$(textboxes).focus(function(){
		$(this).select(); 
	});				
	
	/*----------------------------------------------------------------------------------------------*/
	//BUSCAR DADOS DO PRODUTO

	var urlDestino = 'filtro_ajax.php';
	//COMPLETAR LOCAL DE ENTREGA PADRAO
	$('#txt_cliente_codigo').change(function(){
		
		var codigo = $(this).val();
		if($('#txt_codigo').length){
			var pedidoId = $('#txt_codigo').val();
			if(pedidoId == 'novo'){ pedidoId = 0};
		}else{
			pedidoId = 0
		}
		
			$.get(urlDestino, {busca_cliente:codigo, pedido_nfe:'true', pedidoId:pedidoId}, function(theXML){
				$('dados',theXML).each(function(){
							
					var cliente_id 			= $(this).find("cliente_id").text();
					var entregaEndereco 	= $(this).find("entregaEndereco").text();
					var entregaNumero 		= $(this).find("entregaNumero").text();
					var entregaComplemento 	= $(this).find("entregaComplemento").text();
					var entregaBairro 		= $(this).find("entregaBairro").text();
					var entregaCEP 			= $(this).find("entregaCEP").text();
					var entregaCodigo	 	= $(this).find("entregaCodigo").text();
					var entregaMunicipio 	= $(this).find("entregaMunicipio").text();
					var entregaCPF_CNPJ 	= $(this).find("entregaCPF_CNPJ").text();
					
					
					//DEIXO UM HID PARA CALCULO DE ALIQUOTAS DE ACORDO COM O ESTADO DE DESTINO
					$('#hid_pedido_nfe_municipio_entrega').val(entregaCodigo);
					if(entregaEndereco != ''){
						//exibe o valor
						$('#txt_pedido_nfe_entrega_endereco').val(entregaEndereco);
						$('#txt_pedido_nfe_entrega_numero').val(entregaNumero);
						$('#txt_pedido_nfe_entrega_complemento').val(entregaComplemento);
						$('#txt_pedido_nfe_entrega_bairro').val(entregaBairro);
						$('#txt_pedido_nfe_entrega_cep').val(entregaCEP);
						$('#txt_municipio_codigo_pedido_nfe').val(entregaCodigo);
						$('#txt_pedido_nfe_municipio').val(entregaMunicipio);
						$('#txt_pedido_nfe_entrega_cpf_cnpj').val(entregaCPF_CNPJ);
						
						$("#chk_local_entrega_diferente").attr('checked', 'checked');
						$('#txt_pedido_nfe_entrega_endereco').attr('readonly', '');
						$('#txt_pedido_nfe_entrega_numero').attr('readonly', '');
						$('#txt_pedido_nfe_entrega_complemento').attr('readonly', '');
						$('#txt_pedido_nfe_entrega_bairro').attr('readonly', '');
						$('#txt_pedido_nfe_entrega_cep').attr('readonly', '');
						$('#txt_municipio_codigo_pedido_nfe').attr('readonly', '');
						$('#txt_pedido_nfe_municipio').attr('readonly', '');
						$('#txt_pedido_nfe_entrega_cpf_cnpj').attr('readonly', '');
					}
					else{
						$("#chk_local_entrega_diferente").attr('checked', '');
						$('#txt_pedido_nfe_entrega_endereco').attr('readonly', 'readonly');
						$('#txt_pedido_nfe_entrega_numero').attr('readonly', 'readonly');
						$('#txt_pedido_nfe_entrega_complemento').attr('readonly', 'readonly');
						$('#txt_pedido_nfe_entrega_bairro').attr('readonly', 'readonly');
						$('#txt_pedido_nfe_entrega_cep').attr('readonly', 'readonly');
						$('#txt_municipio_codigo_pedido_nfe').attr('readonly', 'readonly');
						$('#txt_pedido_nfe_municipio').attr('readonly', 'readonly');
						$('#txt_pedido_nfe_entrega_cpf_cnpj').attr('readonly', 'readonly');
					}

					$("#pedido_nfe_entrega_busca").attr({
						"href":"pedido_nfe_entrega_busca,"+cliente_id
					});
					
					
					/* IMPORTAR VENDAS COMUNS PARA NFe - COLOCAR ID DO CLIENTE NO LINK PARA MODAL ****/ 
					var href = $('#importar-vendas').attr('href').split(',');
					$('#importar-vendas').attr('href', href[0] + ',' + cliente_id + ',' + href[1]);
					/*********************************************************************************/
					
				});
			});
			
		
	});
	
	//ALTERADO $('#txt_produto_codigo').change(function(){
	$('#txt_produto_codigo').blur(function(){
		var codigo 		= $(this).attr('value');
		var cliente_id 	= $('#hid_cliente_id').attr('value');
		var estoque_id 	= $('#sel_produto_estoque').attr('value');
		var nfe_estoque_controlar = $('#hid_estoque_controlar').attr('value');
		$.get(urlDestino, {busca_produto:codigo, cliente_id:cliente_id, estoque_id:estoque_id}, function(theXML){
			$('dados',theXML).each(function(){
										
				var produto_id 	 	= $(this).find("produto_id").text();
				var produto 	 	= $(this).find("produto").text();
				var tabela_sigla 	= $(this).find("tabela_sigla").text();
				var tabela_preco 	= $(this).find("tabela_preco").text();
				var valor 		 	= $(this).find("valor").text();
				var desconto 	 	= $(this).find("desconto").text();
				var valor_compra	= $(this).find("valor_compra").text();
				var UnidadeMedida	= $(this).find("unidade_medida").text();
				var estoque 		= parseInt($(this).find("estoque").text());
				var estoqueMinimo 	= parseInt($(this).find("estoqueMinimo").text());
				var estoqueControle = parseInt($(this).find("estoqueControle").text());
				
				//exibe o valor
				$('#hid_produto_id').val(produto_id);
				$('#txt_produto_nome').val(unescape(produto));
				$('#txt_produto_valor').val(valor);
				$('#sel_produto_tabela').val(tabela_preco);
				$('#hid_tabela_sigla').val(tabela_sigla);
				$('#hid_produto_UM').val(UnidadeMedida);
				$('#txt_produto_desconto').val(float2br(desconto));
				$('#txt_produto_valor_compra').val(float2br(valor_compra));
				
				$('#hid_estoque_limite').val(estoque);
				$('#hid_estoque_controle').val(estoqueControle); // se vai controlar estoque desse produto ou n�o
				
				if(($('#txt_produto_quantidade').attr('value')) == ''){
					if(($('#txt_produto_codigo').attr('value')) != ''){
						$('#txt_produto_quantidade').val(1);
					}
				}

				$("#btn_nfe").attr('href', 'pedido_nfe_item,'+produto_id);
				
				// CONTROLE DE ESTOQUE #####################################
				if(estoqueControle == 1 && nfe_estoque_controlar == 1){
						
					var estoqueInserido = 0;
					var vendaRemovido = 0;
					
					//verifica no caso de edicao de venda #####################################################################################################
					var controle = $('#hid_calculo_estoque_controle').val();
					n = 0;
					while(n <= controle){
						var hidItemId = $('input[name=hid_item_id_'+n+']').attr('value'); //qual id do item que esta listado no momento
						var hidEstoqueId = $('input[name=hid_controle_item_estoque_id_'+n+']').attr('value'); //id do estoque
						if(hidItemId == produto_id && hidEstoqueId == 1){
							var vendaRemovido = parseInt($('input[name=hid_controle_item_estoque_'+n+']').attr('value'));
						}
						n++;
					}
					//#####################################################################################################
					
					// PRA CALCULO DO ESTOQUE MINIMO (vou ter que repetir o mesmo trecho em baixo)#######################################################################################################################################
					var limite = $('#hid_controle').attr('value'); 
					n = 1;
					while(n <= limite){
						var itemDetalhe = $('input[name=hid_item_detalhe_'+n+']').attr('value'); //verifica se o item j� tinha sido inserido na venda, no caso de estar no editar venda - soh tem no pedido_detalhe
						var itemId = $('input[name=hid_item_produto_id_'+n+']').attr('value'); //qual id do item que esta listado no momento
						var itemQtd = $('input[name=txt_item_quantidade_'+n+']').attr('value');// quantidade
						var hidEstoqueId = $('input[name=hid_item_estoque_id_'+n+']').attr('value'); //id do estoque
						
						if(itemDetalhe != 1){
							if(itemQtd != null){ // verifica se tem qtd, pq estava dando erro no meio do arquivo quando excluia algum item da lista, na hora de passar por esse numero (n), a quantidade ficava 'undefined' e dava erro no calculo
								itemQtd = br2float($('input[name=txt_item_quantidade_'+n+']').attr('value'));
							}
							if(produto_id == itemId && hidEstoqueId == 1){ //se o produto que esta sendo inserido no momento, tiver o mesmo id de algum do loop de itens j� inseridos, ele soma as quantidades
								estoqueInserido = estoqueInserido + itemQtd;
							}
						}
						n++;
					}
					estoqueAtual = estoque + vendaRemovido;
					estoqueAtual = estoqueAtual - estoqueInserido;
					
					if(estoqueMinimo == estoqueAtual){
						alert("ATEN��O! Estoque m�nimo alcan�ado!");
					}else if(estoqueMinimo > estoqueAtual){
						alert("ATEN��O! Estoque m�nimo excedido.");
					}
				}
				//#############################################################
				if(produto !=''){
					$("#txt_produto_nome").attr({"readonly":""});
							
					//VERIFICA SE � NA VENDA RAPIDA
					if(($('form:first').attr('id')) == 'frm_pedido_rapido_novo'){
						$('#btn_item_inserir').click();
					}
				}
			});
		});
	});
	/*----------------------------------------------------------------------------------------------*/
	//AO TROCAR A TABELA DE PRE�OS, CARREGAR O VALOR QUE ESTA CADASTRADO NELA
	 $("select[name=sel_produto_tabela]").change(function(){
				
		var id = $(this).attr('value');		
		var produto_id = $('#hid_produto_id').attr('value');
		
		$.get(urlDestino, {sel_pedido_tabela_preco:id, produto_id:produto_id}, function(theXML){
			$('dados',theXML).each(function(){

				var valor = float2br(parseFloat($(this).find("valor").text()).toFixed(2));
				var tabela_sigla = $(this).find("tabela_sigla").text();
				//exibe o valor
				$('#txt_produto_valor').val(valor);
				$('#hid_tabela_sigla').val(tabela_sigla);
			});
		});
	})
	
	 /*----------------------------------------------------------------------------------------------*/
	//AO TROCAR ESTOQUE
	 $("select[name=sel_produto_estoque]").change(function(){
		var id = $(this).attr('value');		
		var produtoId = $('#hid_produto_id').attr('value');		
		
		$.get(urlDestino, {produto_estoque_id:id, produtoId:produtoId}, function(theXML){
			$('dados',theXML).each(function(){
				var estoqueLimite = $(this).find("estoque").text();
				$('#hid_estoque_limite').val(estoqueLimite);
			});
		});
		
		$('#txt_produto_valor').focus();
	})
	
	/*----------------------------------------------------------------------------------------------*/
	//INSERIR PRODUTO NO PEDIDO
	$('#btn_item_inserir').live('click',function(event){
		$(this).attr('disabled', 'disabled');
		event.preventDefault();
		
		
		//VERIFICA SE JA EXISTE MESMO ITEM INSERIDO ANTERIOR ############################################################################################################
			item_repedito_alerta 	= $('#hid_item_repetido_alerta').val();
			var produto_codigo 		= $('#txt_produto_codigo').attr('value');
			if(item_repedito_alerta == 1 && produto_codigo != ''){
				
				var produto_descricao 	= $('#txt_produto_nome').attr('value');
							
				n = 0;
				$('input[name^="txt_item_codigo"]').each(function(){
					item_codigo		= $(this).val();
					item_descricao 	= $(this).parents("#pedido_lista_item").find("input[name^='txt_item_nome']").val();
					
					if(item_codigo == produto_codigo && item_descricao == produto_descricao){
						if(confirm('Item j� inserido em Linha '+n+'. Deseja inserir novamente? ') == false ){
							$('#txt_produto_codigo').select();
							
							$('#btn_item_inserir').attr('disabled', '');
							$('#btn_item_inserir').die(click);
						}
					}
					
					n++;
				});
			}
		
		//controle de estoque //########################################################################################################################################
		var quantidade 		= br2float($('#txt_produto_quantidade').attr('value'));
		var produto_id 		= $('#hid_produto_id').attr('value');
		var limite 			= $('#hid_controle').attr('value'); // quantos itens foram inseridos na listagem da venda at� agora
		var estoqueControle = $('#hid_estoque_controle').attr('value'); // verifica se � pra controlar estoque ou nao deste produto
		var estoqueId 		= $('#sel_produto_estoque').attr('value');
		
		//SE CONTROLA ESTOQUE DE CORDO COM NATUREZA OPERACAO
		var nfe_estoque_controlar = $('#hid_estoque_controlar').attr('value');
		
		//se o produto estiver configurado para controlar estoque ###################################################################################################
		if(estoqueControle == 1 && nfe_estoque_controlar == 1){
			var estoqueInserido = 0;
			var vendaRemovido = 0;
			
			//verifica no caso de edicao de venda #####################################################################################################
			var controle = $('#hid_calculo_estoque_controle').val();
			n = 0;
			y = 0;
			while(n <= controle){
				var hidItemId 	 = $('input[name=hid_item_id_'+n+']').attr('value'); //qual id do item que esta listado no momento
				var hidEstoqueId = $('input[name=hid_controle_item_estoque_id_'+n+']').attr('value'); //id do estoque
				
				if(hidItemId == produto_id && estoqueId == hidEstoqueId){
					var vendaRemovido = parseInt($('input[name=hid_controle_item_estoque_'+n+']').attr('value'));
				}
				n++;
			}
			// fim da verificacao ######################################################################################################################
			
			//varre itens inseridos e verifica se tem o mesmo id do produto pra fazer o calculo do estoque
			n = 1;
			while(n <= limite){
				var itemDetalhe  = $('input[name=hid_item_detalhe_'+n+']').attr('value'); //verifica se o item j� tinha sido inserido na venda, no caso de estar no editar venda - soh tem no pedido_detalhe
				var itemId 		 = $('input[name=hid_item_produto_id_'+n+']').attr('value'); //qual id do item que esta listado no momento
				var itemQtd  	 = $('input[name=txt_item_quantidade_'+n+']').attr('value');// quantidade
				var hidEstoqueId = $('input[name=hid_item_estoque_id_'+n+']').attr('value'); //id do estoque
				
				if(itemDetalhe != 1){
					if(itemQtd){ // verifica se tem qtd, pq estava dando erro no meio do arquivo quando excluia algum item da lista, na hora de passar por esse numero (n), a quantidade ficava 'undefined' e dava erro no calculo
						itemQtd = br2float($('input[name=txt_item_quantidade_'+n+']').attr('value'));
					}
					if(produto_id == itemId && estoqueId == hidEstoqueId){ //se o produto que esta sendo inserido no momento, tiver o mesmo id de algum do loop de itens j� inseridos, ele soma as quantidades
						estoqueInserido += itemQtd;
					}
				}
				n++;
			}
			
			var estoqueLimite = parseInt($('#hid_estoque_limite').attr('value'));
			var estoqueLimite = estoqueLimite + vendaRemovido;
			var controleAtual = parseInt(estoqueInserido) + parseInt(quantidade);
	
			if(controleAtual > estoqueLimite){
				alert("N�o h� estoque suficiente!");
				return false;
			}
		}
		//end if(estoqueControle == 1)######################################################################################################################################################################################################
		if($("#txt_produto_nome").val()!=""){
			anchor = $('#btn_nfe');
			winModal(anchor);
		}
		$(this).attr('disabled', '');
	}); 
	
	/*----------------------------------------------------------------------------------------------*/
	//EXCLUIR ITEM
	$('#pedido_lista_item .a_excluir').click(function(event){
		event.preventDefault();
		
		//TUDO ISSO pra verificar estoque que ja foi lan�ado �� //#################################################################################
			var fltItemId		= $(this).parents("#pedido_lista_item").find(".hid_item_produto_id").val();
			var fltItemQtd 		= parseInt(br2float($(this).parents("#pedido_lista_item").find(".txt_item_quantidade").val()));
			var detalheControle = $(this).parents("#pedido_lista_item").find(".hid_item_detalhe").val(); // pra verificar se item ja estava na venda
			
			var estoqueId	= $(this).parents("#pedido_lista_item").find(".hid_item_estoque_id").val(); 
			var controle	= $('#hid_calculo_estoque_controle').val();
			
			if(detalheControle == 1){
				n = 0;
				var x = 0;
				while(n <= controle){
					var hidItemId 	 = $('input[name=hid_item_id_'+n+']').attr('value'); //qual id do item que esta listado no momento
					var hidItemQtd 	 = parseInt($('input[name=hid_controle_item_estoque_'+n+']').attr('value'));//quantidade
					var hidEstoqueId = parseInt($('input[name=hid_controle_item_estoque_id_'+n+']').attr('value'));//quantidade
					
					if(hidItemId == fltItemId && hidEstoqueId == estoqueId){
						calculoEstoque = hidItemQtd + fltItemQtd;
						$('input[name=hid_controle_item_estoque_'+n+']').val(calculoEstoque);					
						var x = 1;
					}
					n++;
				}
				if(x == 0){
					$('.item_estoque:first').clone(true).appendTo('#hid_item');
					$('.hid_item_id:last').val(fltItemId);
					$('.hid_controle_item_estoque:last').val(fltItemQtd);
					$('.hid_controle_item_estoque_id:last').val(estoqueId);
					
					var n = parseInt(controle) + 1;
					$('.hid_item_id:last').attr('name', $('#hid_item_id:last').attr('class') + "_" + n);
					$('.hid_controle_item_estoque:last').attr('name', $('#hid_controle_item_estoque:last').attr('class') + "_" + n);
					$('.hid_controle_item_estoque_id:last').attr('name', $('#hid_controle_item_estoque_id:last').attr('class') + "_" + n);
					$('#hid_calculo_estoque_controle').val(n);
				}
			}
			
		//###################################################################################################################################
		//EXCLUI VALORES DOS TOTAIS
		//subtrai do total o valor do item a excluir
		varGlobalPedido();
	
		var itemValorBruto		= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_principal_valor_total_bruto').val())));
		var itemBCICMS			= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_tributos_icms_vBC').val())));
		var itemICMS 			= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_tributos_icms_vICMS').val())));
		var itemBCICMSST		= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_tributos_icms_st_vBCST').val())));
		var itemICMSST			= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_tributos_icms_st_vICMSST').val())));
		var itemFrete			= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_principal_frete').val())));
		var itemSeguro			= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_principal_total_seguro').val())));
		var itemDesconto		= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_principal_desconto').val())));
		var itemII				= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_tributos_ii_valor').val())));
		var itemIPI				= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_tributos_ipi_valor').val())));
		var itemPIS				= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_tributos_pis_valor').val())));
		var itemCOFINS			= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_tributos_cofins_valor').val())));
		var itemBCISS			= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_tributos_issqn_valor_base_calculo').val())));
		var itemISS				= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_tributos_issqn_valor').val())));
		var itemOutDespAcess	= Number(ifNumberNull(br2float($(this).parents("#pedido_lista_item").find('.txt_nfe_pedido_item_principal_outras_despesas_acessorias').val())));
		var itemChckTotalCompoe	= $(this).parents("#pedido_lista_item").find('.chk_nfe_pedido_item_principal_valor_total_bruto').attr('checked');
		
		if(itemChckTotalCompoe == 'false'){
			itemValorBruto = '0,00';
		}
			
		valPis 			= 0;
		valCofins 		= 0;
		valPisISSQN 	= 0;
		valCofinsISSQN 	= 0;
		
		//EXCLUO OS VALORES DOS TOTAIS
		totalProdutos 			= pedidoProdutos 	- itemValorBruto;
		totalBCICMS				= pedidoBCICMS 		- itemBCICMS;
		totalICMS				= pedidoICMS 		- itemICMS;
		totalBCICMSST			= pedidoBCICMSST 	- itemBCICMSST;
		totalICMSST				= pedidoICMSST 		- itemICMSST;
		totalFrete				= pedidoFrete 		- itemFrete;
		totalSeguro				= pedidoSeguro 		- itemSeguro;
		totalDesconto			= pedidoDesconto 	- itemDesconto;
		totalII					= pedidoII 			- itemII;
		totalIPI				= pedidoIPI 		- itemIPI;
		totalBCISS				= pedidoBCISS 		- itemBCISS;
		totalISS				= pedidoISS 		- itemISS;
		totalOutDespAcess		= pedidoOutDespAcess - itemOutDespAcess;
		
		totalNFeAntiga			= (itemValorBruto + itemICMSST + itemFrete + itemSeguro + itemOutDespAcess + itemII + itemIPI - itemDesconto).toFixed(2);
		totalNFe				= pedidoTotalNFe - totalNFeAntiga;

		if($(this).parents("#pedido_lista_item").find('.hid_nfe_tributo_rad').val() == 'icms'){
			valPis			= pedidoPIS		- itemPIS;
			valCofins 		= pedidoCOFINS	- itemCOFINS;
			
		}else if($(this).parents("#pedido_lista_item").find('.hid_nfe_tributo_rad').val() == 'issqn'){
			valPisISSQN 	= pedidoPISTotalServicos	- itemPIS;
			valCofinsISSQN 	= pedidoCOFINSTotalServicos - itemCOFINS;
		}
		
		$('#txt_pedido_nfe_total_pis').val(float2br(valPis.toFixed(2)));
		$('#txt_pedido_nfe_total_cofins').val(float2br(valCofins.toFixed(2)));
		$('#txt_pedido_nfe_total_pis_servicos').val(float2br(valPisISSQN.toFixed(2)));
		$('#txt_pedido_nfe_total_cofins_servicos').val((valCofinsISSQN.toFixed(2)));
		
		//NOVOS TOTAIS
		$('#txt_pedido_nfe_total_produtos').val(float2br(totalProdutos.toFixed(2)));
		$('#txt_pedido_nfe_total_bc_icms').val(float2br(totalBCICMS.toFixed(2)));
		$('#txt_pedido_nfe_total_icms').val(float2br(totalICMS.toFixed(2)));
		$('#txt_pedido_nfe_total_bc_icms_substituicao').val(float2br(totalBCICMSST.toFixed(2)));
		$('#txt_pedido_nfe_total_icms_substituicao').val(float2br(totalICMSST.toFixed(2)));
		$('#txt_pedido_nfe_total_frete').val(float2br(totalFrete.toFixed(2)));
		$('#txt_pedido_nfe_total_seguro').val(float2br(totalSeguro.toFixed(2)));
		$('#txt_pedido_nfe_total_desconto').val(float2br(totalDesconto.toFixed(2)));
		$('#txt_pedido_nfe_total_ii').val(float2br(totalII.toFixed(2)));
		$('#txt_pedido_nfe_total_ipi').val(float2br(totalIPI.toFixed(2)));
		$('#txt_pedido_nfe_total_bc_iss').val(float2br(totalBCISS.toFixed(2)));
		$('#txt_pedido_nfe_total_iss').val(float2br(totalISS.toFixed(2)));
		$('#txt_pedido_nfe_outras_despesas_acessorias').val(float2br(totalOutDespAcess.toFixed(2)));
		$('#txt_pedido_nfe_total_nfe').val(float2br(totalNFe.toFixed(2)));
		
		
		if($("ul#pedido_modo_aba").length > 0){
			valorTotalServicos	 	= valorFuncionario1 + valorFuncionario2 + valorFuncionario3;
		}else{
			valorTotalServicos 		= Number(ifNumberNull(br2float($('#txt_pedido_servicos').val())));
		}
		
		valorSubTotalPedido 		= totalNFe + valorTotalServicos + valorServicoTerceiros;
		$('#txt_pedido_produtos').val(float2br(totalNFe.toFixed(2)));
		$('#txt_pedido_subtotal').val(float2br(valorSubTotalPedido.toFixed(2)));
		totalCalcularDesconto();
		
		//remove o parente identificado pela class
		$(this).parents("#pedido_lista_item").remove();
		
		//calcula os tributos
		calculoTributo();
		
	});
	
	/*----------------------------------------------------------------------------------------------*/	
	//ZERANDO O DESCONTO DO PRODUTO CASO FIQUE EM BRANCO, OU VALOR INV�LIDO
	$('#txt_produto_desconto').change(function(){
			
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
	});
	
	/*----------------------------------------------------------------------------------------------*/	
	//V�O TER DOIS CAMPOS DE DESCONTO, EM (%) E EM (R$)... ENT�O S�O ESSES DOIS CAMPOS QUE TEM QUE FUNCIONAR
	$('#txt_pedido_desconto_reais').change(function(){
		//LIMPAR O OUTRO CAMPO DE DESCONTO
		$('#txt_pedido_desconto').val('0');
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		totalCalcularDesconto();
	});
	
	$('#txt_pedido_desconto').change(function(){
		//LIMPAR O OUTRO CAMPO DE DESCONTO
		$('#txt_pedido_desconto_reais').val('0');
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		
		totalCalcularDesconto();
	});

	/*----------------------------------------------------------------------------------------------*/
	//VERIFICAR SE HA CLIENTE NA VENDA
	$('form#frm_pedido_nfe_novo input#btn_gravar').click(function(){
		
		if($('#txt_cliente_codigo').val() == ''){
			alert("O cliente deve ser selecionado!");
			return false;
		}else{
			return true;
		}
	});
	
	$("form#frm_pedido_nfe_novo input#btn_gravar").click(function(event){
		event.preventDefault();
		cliente_id				=	$("#hid_cliente_id").val(); //pega o id do cliente que est� comprando esse produto.	
		exibir_parcela 			=	$('#hid_parcela_exibir').val();
		verificar_limite_credito 	= 	$("#hid_verificar_limite_credito").val(); //pega o par�metro para ver se vai verificar ou n�o o limite de cr�dito.
		if(cliente_id > 0 && verificar_limite_credito == 1 && exibir_parcela == 1){

			pedido_total			=	br2float($("#txt_pedido_total").val()).toFixed(2); //pega o valor total do pedido.
			primeira_parcela  		=	br2float($("#txt_parcela_valor_0").val()); //pega o valor da primeira parcela.
			parcela_vencimento		=	$("#txt_parcela_data_0").val(); //pega a data de vencimento da primeira parcela.
			
			/*
			 * Na verifica��o do Limite de Cr�dito, os valores retornados, s�o os seguintes:
			 * 1 - N�o.
			 * 2 - Sim.
			*/
			
			pedido_resultado = pedido_total - primeira_parcela;
			dados_data		=	new Date(); //faz a inst�ncia do objeto data, para pegar os dados da data atual.
			dia_atual		=	dados_data.getDate(); //pega o dia atual.
			mes_atual		=	dados_data.getMonth() + 1; //pega o m�s atual.
			ano_atual		=	dados_data.getFullYear(); //pega o ano atual.
			data_atual		=	ano_atual + PadDigits(mes_atual) + PadDigits(dia_atual); //junta o dia, o m�s, e o ano em uma vari�vel.
			data_parcela_vencimento =	parcela_vencimento.split('/'); //separa tudo que estiver entre uma / (barra) na data do vencimento, e logo depois ele retorna em uma array. xD.
			vencimento_dia		=	data_parcela_vencimento[0]; //pega o dia de vencimento da parcela.
			vencimento_mes		=	data_parcela_vencimento[1]; //pega o m�s de vencimento da parcela.
			vencimento_ano		=	data_parcela_vencimento[2]; //pega o ano de vencimento da parcela.
			data_vencimento		=	vencimento_ano + vencimento_mes + vencimento_dia;
			
			//mudando o valor dos campos select das parcelas
			var x = 0;
			var totalParcelas	= $('ul.parcela_detalhe').length;
			var numeroAtual 	= $("#txt_parcela_valor_0").attr('name').split('_');
			var tipoPagamento 	= $(this).val();
			
			if(data_atual >= data_vencimento){
				pedido_total	=	parseInt(pedido_total) - parseInt(primeira_parcela); //Desconta o valor da primeira parcela, pois ela � � vista.
			}
			/*
				 * Essa condi��o vai fazer o seguinte:
				 * Verificar se o Cliente n�o � consumidor, para isso, ele vai verificar se o id do cliente escolhido � maior do que zero,
				 * caso seja, significa que ele n�o � um consumidor, pois o consumidor normal, � cadastrado com o ID zero, e n�o precisa
				 * de nenhuma verifica��o.
				 *
				 * Ap�s verificar se ele n�o � um consumidor, ele vai verificar se na configura��o do sistema, est� habilitada ou n�o
				 * para verificar o limite de cr�dito.
				 *
				 * Caso ambas condi��es estejam verdadeiras, ele vai executar a verifica��o abaixo para verificar o limite de cr�dito.
				 *
			*/
			
		
			for(x = numeroAtual[3]; x < totalParcelas; x++) { //faz um loop em todas as parcelas.
				var data_parcela		=	$("#txt_parcela_data_" + x).val().split('/'); //pega a data de vencimento da primeira parcela.
				var tipoPagamento_Valor	= 	$('form select#sel_pagamento_tipo_' + x).attr('value'); //pega o tipo da parcela.
				var parcelaValor 		=	br2float($('form input#txt_parcela_valor_' + x).attr('value')); //pega o valor dessa parcela.
				
				var vencimento_parcela_dia	=	data_parcela[2];
				var vencimento_parcela_mes	=	data_parcela[1];
				var vencimento_parcela_ano	=	data_parcela[0];
				
				var vencimento_parcela			=	vencimento_parcela_dia + vencimento_parcela_mes + vencimento_parcela_ano;
				
				
				if(tipoPagamento_Valor == 2 && vencimento_parcela > data_atual){ //verifica se ela foi paga com cart�o de cr�dito.
					pedido_total = pedido_total - parcelaValor; //desconta o valor da parcela no total.
				}
			}
			//final
			$.get(urlDestino, {totalPedido:pedido_resultado, cliente_id:cliente_id}, function(theXML){
				$('dados',theXML).each(function(){
				
					var valorExcedido  = $(this).find("valorExcedido").text();
					var limiteCredito  = $(this).find("limiteCredito").text();
					var excedido  = $(this).find("excedido").text(); 
					var resultado = valorExcedido - limiteCredito;
					
					if(excedido == 1){
						alert("O limite de cr�dito para este cliente foi excedido em R$ " + valorExcedido);
						return false;
					}else{
						$('form#frm_pedido_nfe_novo').submit();
					}
				});
			});
		}else{
			$('form#frm_pedido_nfe_novo').submit();
		}
	});
	
	// FAZ REALMENTE A A��O DE INSERIR O �TEM
	 $('#btn_enviar_item_nfe').live('click',function(e){ //NFELUCAS
            e.preventDefault();
			varGlobalPedido();
			
			//DOU BLUR PRA PODER ATUALIZAR O CALCULO DOS CAMPOS
			$('#txt_nfe_item_tributos_icms_pCredSN').blur();	
			$('#txt_nfe_item_principal_valor_unitario_comercial').blur();
				
			$.each($("form#pedido_nfe_item_dados .invisible"),function(){
				$(this).val('');
				$(this).text('');
			});
			
			//CALCULAR TOTAIS DO ITEM ############################################################################################################
			var valor 						= br2float(float2br($('#txt_nfe_item_principal_valor_unitario_comercial').val()));
			var desconto_percent			= br2float(float2br($('#txt_nfe_item_principal_desconto_percent').val()));
			var desconto 					= br2float(float2br($('#txt_nfe_item_principal_desconto').val()));
			var quantidade 					= br2float(float2br($('#txt_nfe_item_principal_quantidade_comercial').val()));
		
			var Frete						= br2float(float2br($('#txt_nfe_item_principal_frete').val()));
			var Seguro						= br2float(float2br($('#txt_nfe_item_principal_total_seguro').val()));
			var OutrasDespesasAcessorias 	= br2float(float2br($('#txt_nfe_item_principal_outras_despesas_acessorias').val()));
			
			// ##################################################################################################################################
			
			//##vai pegar os valores que vierem da nfe
			var fltSubtotal 	= (valor * quantidade) - desconto;
		
			//VERIFICA SE NAO ESTA SENDO EDITADO
			if($('#hid_item_fiscal_edit').val() == 0){
			
				//o parametro true em clone faz com que seja copiado tamb�m os eventos do elemento
				$('#pedido_lista_item:first').clone(true).appendTo('#pedido_lista');
				
				//COLANDO NOVA LISTAGEM DE ITENS //####################################################################################################
				var controle = $('#hid_controle').val();
				
				//COPIAR DADOS NO NOVO ITEM
				$('.chk_entregue:last').attr('checked','checked');
				
				$('.hid_item_produto_id:last').val($('#hid_produto_id').val());
				$('.txt_item_codigo:last').val($('#txt_nfe_item_principal_codigo').val());
				
				$('.txt_item_nome:last').val($('#txt_nfe_item_principal_descricao').val());// VAI PEGAR DA NFE
				$('.txt_item_valor:last').val(float2br(br2float($('#txt_nfe_item_principal_valor_unitario_comercial').val()).toFixed(vendaDecimal)));// VAI PEGAR DA NFE
				$('.txt_item_quantidade:last').val(float2br(br2float($('#txt_nfe_item_principal_quantidade_comercial').val()).toFixed(quantidadeDecimal)));// VAI PEGAR DA NFE
				
				$('.txt_item_tabela_sigla:last').val($('#sel_produto_tabela').find('option').filter(':selected').text());
				$('.hid_item_tabela_preco:last').val($('#sel_produto_tabela').val());
				$('.txt_item_UM:last').val($('#hid_produto_UM').val());
				
				$('.txt_item_desconto:last').val(float2br(br2float($('#txt_nfe_item_principal_desconto_percent').val()).toFixed(2)));// VAI PEGAR DA NFE
				$('.txt_item_referencia:last').val	($('#txt_produto_referencia').val());
				
				$('.txt_item_estoque_nome:last').val($('#sel_produto_estoque').find('option').filter(':selected').text());
				$('.hid_item_estoque_id:last').val($('#sel_produto_estoque').val());
				
				// ### NFE! ###############################################################################################################
				
				// ### PRODUTO ESPECIFICO (COMBUSTIVEL) #########################
				$('.txt_nfe_item_produto_especifico:last').val($('#sel_prod_especifico_tipo').val());
				$('.txt_nfe_item_produto_especifico_id:last').val($('#sel_prod_especifico_tipo').find('option:selected').attr('id'));
				$('.txt_nfe_item_produto_especifico_combustivel_anp:last').val($('#sel_produto_especifico_combustivel_codanp').find('option:selected').attr('id'));
				$('.txt_nfe_item_produto_especifico_combustivel_if:last').val($('#txt_produto_especifico_combustivel_codif').val());
				$('.txt_nfe_item_produto_especifico_combustivel_uf:last').val($('#sel_produto_especifico_combustivel_uf').find('option:selected').attr('id'));
				$('.txt_nfe_item_produto_especifico_combustivel_qtd_faturada:last').val($('#txt_produto_especifico_combustivel_qtd_faturada').val());
				$('.txt_nfe_item_produto_especifico_combustivel_cide_bc:last').val($('#txt_produto_especifico_combustivel_cide_bc').val());
				$('.txt_nfe_item_produto_especifico_combustivel_cide_aliquota:last').val($('#txt_produto_especifico_combustivel_cide_aliquota').val());
				$('.txt_nfe_item_produto_especifico_combustivel_cide_valor:last').val($('#txt_produto_especifico_combustivel_cide_valor').val());
				
				// ### TRIBUTOS ##############
				$('.txt_nfe_item_produto_especifico_total_tributos:last').val($('#txt_nfe_item_total_tributos').val());

				// ### PRODUTO ESPECIFICO (COMBUSTIVEL) #########################
				$('.txt_nfe_pedido_item_principal_ncm:last').val($('#txt_nfe_item_principal_ncm').val());
				$('.txt_nfe_pedido_item_principal_ex_tipi:last').val($('#txt_nfe_item_principal_ex_tipi').val());
				$('.txt_nfe_pedido_item_principal_cfop:last').val($('#txt_nfe_item_principal_cfop').val());
				
				$('.txt_nfe_pedido_item_principal_unidade_comercial:last').val($('#txt_nfe_item_principal_unidade_comercial').val());
				$('.txt_nfe_pedido_item_principal_unidade_tributavel:last').val($('#txt_nfe_item_principal_unidade_tributavel').val());
				$('.txt_nfe_pedido_item_principal_quantidade_tributavel:last').val(float2br(br2float($('#txt_nfe_item_principal_quantidade_tributavel').val()).toFixed(quantidadeDecimal)));
				$('.txt_nfe_pedido_item_principal_valor_unidade_tributavel:last').val(float2br(br2float($('#txt_nfe_item_principal_valor_unidade_tributavel').val()).toFixed(vendaDecimal)));
			   
				$('.txt_nfe_pedido_item_principal_total_seguro:last').val(float2br(br2float($('#txt_nfe_item_principal_total_seguro').val()).toFixed(2)));
				$('.txt_nfe_pedido_item_principal_desconto:last').val(float2br(br2float($('#txt_nfe_item_principal_desconto').val()).toFixed(2)));
				$('.txt_nfe_pedido_item_principal_frete:last').val(float2br(br2float($('#txt_nfe_item_principal_frete').val()).toFixed(2)));
				
				$('.txt_nfe_pedido_item_principal_ean:last').val($('#txt_nfe_item_principal_ean').val());
				$('.txt_nfe_pedido_item_principal_ean_tributavel:last').val($('#txt_nfe_item_principal_ean_tributavel').val());
				$('.txt_nfe_pedido_item_principal_outras_despesas_acessorias:last').val(float2br(br2float($('#txt_nfe_item_principal_outras_despesas_acessorias').val()).toFixed(2)));
				$('.txt_nfe_pedido_item_principal_valor_total_bruto:last').val($('#txt_nfe_item_principal_valor_total_bruto').val());
				$('.txt_nfe_pedido_item_principal_pedido_compra:last').val($('#txt_nfe_item_principal_pedido_compra').val());
				$('.txt_nfe_pedido_item_principal_numero_item_pedido_compra:last').val($('#txt_nfe_item_principal_numero_item_pedido_compra').val());
				
				//$('.txt_nfe_pedido_item_principal_produto_especifico:last').val($('#sel_nfe_item_principal_produto_especifico').find('option').filter(':selected').text());
				//$('.hid_nfe_pedido_item_principal_produto_especifico:last').val($('#sel_nfe_item_principal_produto_especifico').val());
				$('.chk_nfe_pedido_item_principal_valor_total_bruto:last').attr('checked', $('#chk_nfe_item_principal_valor_total_bruto').attr('checked'));
				
				$('.hid_nfe_tributo_rad:last').val($('input:radio[name=rad_imposto]:checked').val());
				$('.txt_nfe_pedido_item_tributos_icms_situacao_tributaria:last').val($('#sel_nfe_item_tributos_icms_situacao_tributaria').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_icms_situacao_tributaria:last').val($('#sel_nfe_item_tributos_icms_situacao_tributaria').val());
				$('.txt_nfe_pedido_item_tributos_icms_origem:last').val($('#sel_nfe_item_tributos_icms_origem').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_icms_origem:last').val($('#sel_nfe_item_tributos_icms_origem').val());
				
				$('.txt_nfe_pedido_item_tributos_icms_pCredSN:last').val($('#txt_nfe_item_tributos_icms_pCredSN').val());
				$('.txt_nfe_pedido_item_tributos_icms_vCredICMSSN:last').val($('#txt_nfe_item_tributos_icms_vCredICMSSN').val());
				
				$('.txt_nfe_pedido_item_tributos_icms_modBC:last').val($('#sel_nfe_item_tributos_icms_modBC').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_icms_modBC:last').val($('#sel_nfe_item_tributos_icms_modBC').val());
				$('.txt_nfe_pedido_item_tributos_icms_vBC:last').val($('#txt_nfe_item_tributos_icms_vBC').val());
				$('.txt_nfe_pedido_item_tributos_icms_pRedBC:last').val($('#txt_nfe_item_tributos_icms_pRedBC').val());
				
				$('.txt_nfe_pedido_item_tributos_icms_pICMS:last').val($('#txt_nfe_item_tributos_icms_pICMS').val());
				$('.txt_nfe_pedido_item_tributos_icms_vICMS:last').val($('#txt_nfe_item_tributos_icms_vICMS').val());
				$('.txt_nfe_pedido_item_tributos_icms_pBCOp:last').val($('#txt_nfe_item_tributos_icms_pBCOp').val());
				$('.txt_nfe_pedido_item_tributos_icms_motDesICMS:last').val($('#sel_nfe_item_tributos_icms_motDesICMS').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_icms_motDesICMS:last').val($('#sel_nfe_item_tributos_icms_motDesICMS').val());
				
				$('.txt_nfe_pedido_item_tributos_icms_st_modBCST:last').val($('#sel_nfe_item_tributos_icms_st_modBCST').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_icms_st_modBCST:last').val($('#sel_nfe_item_tributos_icms_st_modBCST').val());
				$('.txt_nfe_pedido_item_tributos_icms_st_pRedBCST:last').val($('#txt_nfe_item_tributos_icms_st_pRedBCST').val());
				$('.txt_nfe_pedido_item_tributos_icms_st_pMVAST:last').val($('#txt_nfe_item_tributos_icms_st_pMVAST').val());
				$('.txt_nfe_pedido_item_tributos_icms_st_vBCST:last').val($('#txt_nfe_item_tributos_icms_st_vBCST').val());
				$('.txt_nfe_pedido_item_tributos_icms_st_pICMSST:last').val($('#txt_nfe_item_tributos_icms_st_pICMSST').val());
				$('.txt_nfe_pedido_item_tributos_icms_st_vICMSST:last').val($('#txt_nfe_item_tributos_icms_st_vICMSST').val());
				$('.txt_nfe_pedido_item_tributos_icms_st_UFST:last').val($('#sel_nfe_item_tributos_icms_st_UFST').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_icms_st_UFST:last').val($('#hid_nfe_item_tributos_icms_st_UFST').val());
				
				$('.txt_nfe_pedido_item_tributos_icms_st_vBCSTRet:last').val($('#txt_nfe_item_tributos_icms_st_vBCSTRet').val());
				$('.txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet:last').val($('#txt_nfe_item_tributos_icms_st_vICMSSTRet').val());
				$('.txt_nfe_pedido_item_tributos_icms_st_vBCSTDest:last').val($('#txt_nfe_item_tributos_icms_st_vBCSTDest').val());
				$('.txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest:last').val($('#txt_nfe_item_tributos_icms_st_vICMSSTDest').val());
	
				$('.txt_nfe_pedido_item_tributos_ipi_situacao_tributaria:last').val($('#sel_nfe_item_tributos_ipi_situacao_tributaria').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_ipi_situacao_tributaria:last').val($('#sel_nfe_item_tributos_ipi_situacao_tributaria').val());
				$('.txt_nfe_pedido_item_tributos_ipi_enquadramento_classe:last').val($('#txt_nfe_item_tributos_ipi_enquadramento_classe').val());
				$('.txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo:last').val($('#txt_nfe_item_tributos_ipi_enquadramento_codigo').val());
				$('.txt_nfe_pedido_item_tributos_ipi_produtor_cnpj:last').val($('#txt_nfe_item_tributos_ipi_produtor_cnpj').val());
				$('.txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo:last').val($('#txt_nfe_item_tributos_ipi_selo_controle_codigo').val());
				$('.txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade:last').val($('#txt_nfe_item_tributos_ipi_selo_controle_quantidade').val());
				$('.txt_nfe_pedido_item_tributos_ipi_calculo_tipo:last').val($('#sel_nfe_item_tributos_ipi_calculo_tipo').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_ipi_calculo_tipo:last').val($('#sel_nfe_item_tributos_ipi_calculo_tipo').val());
				$('.txt_nfe_pedido_item_tributos_ipi_valor_base_calculo:last').val($('#txt_nfe_item_tributos_ipi_valor_base_calculo').val());
				$('.txt_nfe_pedido_item_tributos_ipi_aliquota:last').val($('#txt_nfe_item_tributos_ipi_aliquota').val());
				$('.txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total:last').val($('#txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total').val());
				$('.txt_nfe_pedido_item_tributos_ipi_unidade_valor:last').val($('#txt_nfe_item_tributos_ipi_unidade_valor').val());
				$('.txt_nfe_pedido_item_tributos_ipi_valor:last').val($('#txt_nfe_item_tributos_ipi_valor').val());
				
				$('.txt_nfe_pedido_item_tributos_pis_situacao_tributaria:last').val($('#sel_nfe_item_tributos_pis_situacao_tributaria').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_pis_situacao_tributaria:last').val($('#sel_nfe_item_tributos_pis_situacao_tributaria').val());
				$('.txt_nfe_pedido_item_tributos_pis_calculo_tipo:last').val($('#sel_nfe_item_tributos_pis_calculo_tipo').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_pis_calculo_tipo:last').val($('#sel_nfe_item_tributos_pis_calculo_tipo').val());
				$('.txt_nfe_pedido_item_tributos_pis_valor_base_calculo:last').val($('#txt_nfe_item_tributos_pis_valor_base_calculo').val());
				$('.txt_nfe_pedido_item_tributos_pis_aliquota_percentual:last').val($('#txt_nfe_item_tributos_pis_aliquota_percentual').val());
				$('.txt_nfe_pedido_item_tributos_pis_aliquota_reais:last').val($('#txt_nfe_item_tributos_pis_aliquota_reais').val());
				$('.txt_nfe_pedido_item_tributos_pis_quantidade_vendida:last').val($('#txt_nfe_item_tributos_pis_quantidade_vendida').val());
				$('.txt_nfe_pedido_item_tributos_pis_valor:last').val($('#txt_nfe_item_tributos_pis_valor').val());
				
				$('.txt_nfe_pedido_item_tributos_pis_st_calculo_tipo:last').val($('#sel_nfe_item_tributos_pis_st_calculo_tipo').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_pis_st_calculo_tipo:last').val($('#sel_nfe_item_tributos_pis_st_calculo_tipo').val());
				$('.txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo:last').val($('#txt_nfe_item_tributos_pis_st_valor_base_calculo').val());
				$('.txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual:last').val($('#txt_nfe_item_tributos_pis_st_aliquota_percentual').val());
				$('.txt_nfe_pedido_item_tributos_pis_st_aliquota_reais:last').val($('#txt_nfe_item_tributos_pis_st_aliquota_reais').val());
				$('.txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida:last').val($('#txt_nfe_item_tributos_pis_st_quantidade_vendida').val());
				$('.txt_nfe_pedido_item_tributos_pis_st_valor:last').val($('#txt_nfe_item_tributos_pis_st_valor').val());
				
				$('.txt_nfe_pedido_item_tributos_cofins_situacao_tributaria:last').val($('#sel_nfe_item_tributos_cofins_situacao_tributaria').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_cofins_situacao_tributaria:last').val($('#sel_nfe_item_tributos_cofins_situacao_tributaria').val());
				$('.txt_nfe_pedido_item_tributos_cofins_calculo_tipo:last').val($('#sel_nfe_item_tributos_cofins_calculo_tipo').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_cofins_calculo_tipo:last').val($('#sel_nfe_item_tributos_cofins_calculo_tipo').val());
				$('.txt_nfe_pedido_item_tributos_cofins_valor_base_calculo:last').val($('#txt_nfe_item_tributos_cofins_valor_base_calculo').val());
				$('.txt_nfe_pedido_item_tributos_cofins_aliquota_percentual:last').val($('#txt_nfe_item_tributos_cofins_aliquota_percentual').val());
				$('.txt_nfe_pedido_item_tributos_cofins_aliquota_reais:last').val($('#txt_nfe_item_tributos_cofins_aliquota_reais').val());
				$('.txt_nfe_pedido_item_tributos_cofins_quantidade_vendida:last').val($('#txt_nfe_item_tributos_cofins_quantidade_vendida').val());
				$('.txt_nfe_pedido_item_tributos_cofins_valor:last').val($('#txt_nfe_item_tributos_cofins_valor').val());
				
				$('.txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo:last').val($('#sel_nfe_item_tributos_cofins_st_calculo_tipo').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo:last').val($('#sel_nfe_item_tributos_cofins_st_calculo_tipo').val());
				$('.txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo:last').val($('#txt_nfe_item_tributos_cofins_st_valor_base_calculo').val());
				$('.txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual:last').val($('#txt_nfe_item_tributos_cofins_st_aliquota_percentual').val());
				$('.txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais:last').val($('#txt_nfe_item_tributos_cofins_st_aliquota_reais').val());
				$('.txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida:last').val($('#txt_nfe_item_tributos_cofins_st_quantidade_vendida').val());
				$('.txt_nfe_pedido_item_tributos_cofins_st_valor:last').val($('#txt_nfe_item_tributos_cofins_st_valor').val());
				
				$('.txt_nfe_pedido_item_tributos_ii_valor_base_calculo:last').val($('#txt_nfe_item_tributos_ii_valor_base_calculo').val());
				$('.txt_nfe_pedido_item_tributos_ii_valor_aduaneiras:last').val($('#txt_nfe_item_tributos_ii_valor_aduaneiras').val());
				$('.txt_nfe_pedido_item_tributos_ii_valor_iof:last').val($('#txt_nfe_item_tributos_ii_valor_iof').val());
				$('.txt_nfe_pedido_item_tributos_ii_valor:last').val($('#txt_nfe_item_tributos_ii_valor').val());
				
				$('.txt_nfe_pedido_item_tributos_issqn_tributacao:last').val($('#sel_nfe_item_tributos_issqn_tributacao').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_issqn_tributacao:last').val($('#sel_nfe_item_tributos_issqn_tributacao').val());
				$('.txt_nfe_pedido_item_tributos_issqn_valor_base_calculo:last').val($('#txt_nfe_item_tributos_issqn_valor_base_calculo').val());
				$('.txt_nfe_pedido_item_tributos_issqn_aliquota:last').val($('#txt_nfe_item_tributos_issqn_aliquota').val());
				$('.txt_nfe_pedido_item_tributos_issqn_servico_lista:last').val($('#sel_nfe_item_tributos_issqn_servico_lista').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_issqn_servico_lista:last').val($('#sel_nfe_item_tributos_issqn_servico_lista').val());
				$('.txt_nfe_pedido_item_tributos_issqn_uf:last').val($('#sel_nfe_item_tributos_issqn_uf').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_issqn_uf:last').val($('#sel_nfe_item_tributos_issqn_uf').val());
				$('.txt_nfe_pedido_item_tributos_issqn_municipio:last').val($('#sel_nfe_item_tributos_issqn_municipio').find('option').filter(':selected').text());
				$('.hid_nfe_pedido_item_tributos_issqn_municipio:last').val($('#sel_nfe_item_tributos_issqn_municipio').val());
				$('.txt_nfe_pedido_item_tributos_issqn_valor:last').val($('#txt_nfe_item_tributos_issqn_valor').val());
				
				$('.txt_nfe_item_pedido_informacoes_adicionais:last').val($('#txt_nfe_item_informacoes_adicionais').val());
				$('.txt_item_subtotal:last').val(float2br(fltSubtotal.toFixed(2)));
				
				/*------------------------------------------------------------*/
				//NUMERAR CONTROLE ATUAL
				$('#hid_controle').val(parseInt($('#hid_controle').val()) + 1);
				$('#hid_cursor').val(parseInt($('#hid_cursor').val()) + 1);
				
				$('.chk_entregue:last').attr('name', $('.chk_entregue:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_item_produto_id:last').attr('name', $('.hid_item_produto_id:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_item_codigo:last').attr('name', $('.txt_item_codigo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_item_nome:last').attr('name', $('.txt_item_nome:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_item_tabela_sigla:last').attr('name', $('.txt_item_tabela_sigla:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_item_tabela_preco:last').attr('name', $('.hid_item_tabela_preco:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_item_valor:last').attr('name', $('.txt_item_valor:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_item_quantidade:last').attr('name', $('.txt_item_quantidade:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_item_UM:last').attr('name', $('.txt_item_UM:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_item_subtotal:last').attr('name', $('.txt_item_subtotal:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_item_desconto:last').attr('name', $('.txt_item_desconto:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_item_estoque_id:last').attr('name', $('.hid_item_estoque_id:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_item_referencia:last').attr('name', $('.txt_item_referencia:last').attr('class') + "_" + $('#hid_controle').val());

				// NFE! #########################################################################################################################
				$('.txt_nfe_pedido_item_principal_ncm:last').attr('name', $('.txt_nfe_pedido_item_principal_ncm:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_ex_tipi:last').attr('name', $('.txt_nfe_pedido_item_principal_ex_tipi:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_cfop:last').attr('name', $('.txt_nfe_pedido_item_principal_cfop:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_unidade_comercial:last').attr('name', $('.txt_nfe_pedido_item_principal_unidade_comercial:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_quantidade_comercial:last').attr('name', $('.txt_nfe_pedido_item_principal_quantidade_comercial:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_valor_unitario_comercial:last').attr('name', $('.txt_nfe_pedido_item_principal_valor_unitario_comercial:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_unidade_tributavel:last').attr('name', $('.txt_nfe_pedido_item_principal_unidade_tributavel:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_total_seguro:last').attr('name', $('.txt_nfe_pedido_item_principal_total_seguro:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_quantidade_tributavel:last').attr('name', $('.txt_nfe_pedido_item_principal_quantidade_tributavel:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_valor_unidade_tributavel:last').attr('name', $('.txt_nfe_pedido_item_principal_valor_unidade_tributavel:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_desconto:last').attr('name', $('.txt_nfe_pedido_item_principal_desconto:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_frete:last').attr('name', $('.txt_nfe_pedido_item_principal_frete:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_ean:last').attr('name', $('.txt_nfe_pedido_item_principal_ean:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_ean_tributavel:last').attr('name', $('.txt_nfe_pedido_item_principal_ean_tributavel:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_outras_despesas_acessorias:last').attr('name', $('.txt_nfe_pedido_item_principal_outras_despesas_acessorias:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_valor_total_bruto:last').attr('name', $('.txt_nfe_pedido_item_principal_valor_total_bruto:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_pedido_compra:last').attr('name', $('.txt_nfe_pedido_item_principal_pedido_compra:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_principal_numero_item_pedido_compra:last').attr('name', $('.txt_nfe_pedido_item_principal_numero_item_pedido_compra:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.hid_nfe_tributo_rad:last').attr('name', $('.hid_nfe_tributo_rad:last').attr('class') + "_" + $('#hid_controle').val());
				//$('.txt_nfe_pedido_item_principal_produto_especifico:last').attr('name', $('.txt_nfe_pedido_item_principal_produto_especifico:last').attr('class') + "_" + $('#hid_controle').val());
				//$('.hid_nfe_pedido_item_principal_produto_especifico:last').attr('name', $('.hid_nfe_pedido_item_principal_produto_especifico:last').attr('class') + "_" + $('#hid_controle').val());
				$('.chk_nfe_pedido_item_principal_valor_total_bruto:last').attr('name', $('.chk_nfe_pedido_item_principal_valor_total_bruto:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_icms_situacao_tributaria:last').attr('name', $('.txt_nfe_pedido_item_tributos_icms_situacao_tributaria:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_icms_situacao_tributaria:last').attr('name', $('.hid_nfe_pedido_item_tributos_icms_situacao_tributaria:last').attr('class') + "_" + $('#hid_controle').val());
				
				
				//A PARTIR DAQUI, JOGO O NOME DIRETO, SEM PEGAR DA CLASS, PQ PODE ALTERAR E A CLASS DE ACORDO COM A SITUA��O TRIBUTARIA, FICAR COMO 'VISIBLE' E DAI NAO FUNCIONA MAIS COMO NO ESQUEMA DE CIMA (NAO SEI PQ NAO FOI COLOCADO DIRETO O NOME AOU INVES DE PEGAR A CLASS, ISSO A� � COISA DO IVAN xD)
					$('.txt_nfe_pedido_item_tributos_icms_origem:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_origem_'+$('#hid_controle').val());
					$('.hid_nfe_pedido_item_tributos_icms_origem:last').attr('name', 'hid_nfe_pedido_item_tributos_icms_origem_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_pCredSN:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_pCredSN_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_vCredICMSSN:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_vCredICMSSN_' + $('#hid_controle').val());
					
					$('.txt_nfe_pedido_item_tributos_icms_modBC:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_modBC_' + $('#hid_controle').val());
					$('.hid_nfe_pedido_item_tributos_icms_modBC:last').attr('name', 'hid_nfe_pedido_item_tributos_icms_modBC_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_vBC:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_vBC_' + $('#hid_controle').val());
					
					$('.txt_nfe_pedido_item_tributos_icms_pRedBC:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_pRedBC_' + $('#hid_controle').val());
					
					$('.txt_nfe_pedido_item_tributos_icms_pICMS:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_pICMS_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_vICMS:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_vICMS_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_pBCOp:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_pBCOp_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_motDesICMS:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_motDesICMS_' + $('#hid_controle').val());
					$('.hid_nfe_pedido_item_tributos_icms_motDesICMS:last').attr('name', 'hid_nfe_pedido_item_tributos_icms_motDesICMS_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_modBCST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_modBCST_' + $('#hid_controle').val());
					$('.hid_nfe_pedido_item_tributos_icms_modBCST:last').attr('name', 'hid_nfe_pedido_item_tributos_icms_modBCST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_pRedBCST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_pRedBCST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_pMVAST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_pMVAST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_st_vBCST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_vBCST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_pICMSST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_pICMSST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_st_vICMSST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_vICMSST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_UFST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_UFST_' + $('#hid_controle').val());
					$('.hid_nfe_pedido_item_tributos_icms_UFST:last').attr('name', 'hid_nfe_pedido_item_tributos_icms_UFST_' + $('#hid_controle').val());
		
					$('.txt_nfe_pedido_item_tributos_icms_st_modBCST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_modBCST_' + $('#hid_controle').val());
					$('.hid_nfe_pedido_item_tributos_icms_st_modBCST:last').attr('name', 'hid_nfe_pedido_item_tributos_icms_st_modBCST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_st_pRedBCST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_pRedBCST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_st_pMVAST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_pMVAST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_st_pICMSST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_pICMSST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_st_UFST:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_UFST_' + $('#hid_controle').val());
					$('.hid_nfe_pedido_item_tributos_icms_st_UFST:last').attr('name', 'hid_nfe_pedido_item_tributos_icms_st_UFST_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_st_vBCSTRet:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_st_vBCSTDest:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_' + $('#hid_controle').val());
					$('.txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest:last').attr('name', 'txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_' + $('#hid_controle').val());
				//##########################################################################################################################################################################################################################################################################################
				
				$('.txt_nfe_pedido_item_tributos_ipi_situacao_tributaria:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_situacao_tributaria:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_ipi_situacao_tributaria:last').attr('name', $('.hid_nfe_pedido_item_tributos_ipi_situacao_tributaria:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_ipi_enquadramento_classe:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_enquadramento_classe:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ipi_produtor_cnpj:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_produtor_cnpj:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_ipi_calculo_tipo:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_calculo_tipo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_ipi_calculo_tipo:last').attr('name', $('.hid_nfe_pedido_item_tributos_ipi_calculo_tipo:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_ipi_valor_base_calculo:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_valor_base_calculo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ipi_aliquota:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_aliquota:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ipi_unidade_valor:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_unidade_valor:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ipi_valor:last').attr('name', $('.txt_nfe_pedido_item_tributos_ipi_valor:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_pis_situacao_tributaria:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_situacao_tributaria:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_pis_situacao_tributaria:last').attr('name', $('.hid_nfe_pedido_item_tributos_pis_situacao_tributaria:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_pis_calculo_tipo:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_calculo_tipo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_pis_calculo_tipo:last').attr('name', $('.hid_nfe_pedido_item_tributos_pis_calculo_tipo:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_pis_valor_base_calculo:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_valor_base_calculo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_pis_aliquota_percentual:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_aliquota_percentual:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_pis_aliquota_reais:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_aliquota_reais:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_pis_quantidade_vendida:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_quantidade_vendida:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_pis_valor:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_valor:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_pis_st_calculo_tipo:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_st_calculo_tipo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_pis_st_calculo_tipo:last').attr('name', $('.hid_nfe_pedido_item_tributos_pis_st_calculo_tipo:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_pis_st_aliquota_reais:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_st_aliquota_reais:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_pis_st_valor:last').attr('name', $('.txt_nfe_pedido_item_tributos_pis_st_valor:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_cofins_situacao_tributaria:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_situacao_tributaria:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_cofins_situacao_tributaria:last').attr('name', $('.hid_nfe_pedido_item_tributos_cofins_situacao_tributaria:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_cofins_calculo_tipo:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_calculo_tipo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_cofins_calculo_tipo:last').attr('name', $('.hid_nfe_pedido_item_tributos_cofins_calculo_tipo:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_cofins_valor_base_calculo:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_valor_base_calculo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_cofins_aliquota_percentual:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_aliquota_percentual:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_cofins_aliquota_reais:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_aliquota_reais:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_cofins_quantidade_vendida:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_quantidade_vendida:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_cofins_valor:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_valor:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo:last').attr('name', $('.hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo:last').attr('class') + "_" + $('#hid_controle').val());
	
				$('.txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_cofins_st_valor:last').attr('name', $('.txt_nfe_pedido_item_tributos_cofins_st_valor:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_ii_valor_base_calculo:last').attr('name', $('.txt_nfe_pedido_item_tributos_ii_valor_base_calculo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ii_valor_aduaneiras:last').attr('name', $('.txt_nfe_pedido_item_tributos_ii_valor_aduaneiras:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ii_valor_iof:last').attr('name', $('.txt_nfe_pedido_item_tributos_ii_valor_iof:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_ii_valor:last').attr('name', $('.txt_nfe_pedido_item_tributos_ii_valor:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_issqn_tributacao:last').attr('name', $('.txt_nfe_pedido_item_tributos_issqn_tributacao:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_issqn_tributacao:last').attr('name', $('.hid_nfe_pedido_item_tributos_issqn_tributacao:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_issqn_valor_base_calculo:last').attr('name', $('.txt_nfe_pedido_item_tributos_issqn_valor_base_calculo:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_issqn_aliquota:last').attr('name', $('.txt_nfe_pedido_item_tributos_issqn_aliquota:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_issqn_servico_lista:last').attr('name', $('.txt_nfe_pedido_item_tributos_issqn_servico_lista:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_issqn_servico_lista:last').attr('name', $('.hid_nfe_pedido_item_tributos_issqn_servico_lista:last').attr('class') + "_" + $('#hid_controle').val());
	
				$('.txt_nfe_pedido_item_tributos_issqn_uf:last').attr('name', $('.txt_nfe_pedido_item_tributos_issqn_uf:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_issqn_uf:last').attr('name', $('.hid_nfe_pedido_item_tributos_issqn_uf:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_pedido_item_tributos_issqn_municipio:last').attr('name', $('.txt_nfe_pedido_item_tributos_issqn_municipio:last').attr('class') + "_" + $('#hid_controle').val());
				$('.hid_nfe_pedido_item_tributos_issqn_municipio:last').attr('name', $('.hid_nfe_pedido_item_tributos_issqn_municipio:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_pedido_item_tributos_issqn_valor:last').attr('name', $('.txt_nfe_pedido_item_tributos_issqn_valor:last').attr('class') + "_" + $('#hid_controle').val());
				
				$('.txt_nfe_item_pedido_informacoes_adicionais:last').attr('name', $('.txt_nfe_item_pedido_informacoes_adicionais:last').attr('class') + "_" + $('#hid_controle').val());

				// ### PRODUTO ESPECIFICO (COMBUSTIVEL) #########################
				$('.txt_nfe_item_produto_especifico:last').attr('name', $('.txt_nfe_item_produto_especifico:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_item_produto_especifico_id:last').attr('name', $('.txt_nfe_item_produto_especifico_id:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_item_produto_especifico_combustivel_anp:last').attr('name', $('.txt_nfe_item_produto_especifico_combustivel_anp:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_item_produto_especifico_combustivel_if:last').attr('name', $('.txt_nfe_item_produto_especifico_combustivel_if:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_item_produto_especifico_combustivel_uf:last').attr('name', $('.txt_nfe_item_produto_especifico_combustivel_uf:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_item_produto_especifico_combustivel_qtd_faturada:last').attr('name', $('.txt_nfe_item_produto_especifico_combustivel_qtd_faturada:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_item_produto_especifico_combustivel_cide_bc:last').attr('name', $('.txt_nfe_item_produto_especifico_combustivel_cide_bc:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_item_produto_especifico_combustivel_cide_aliquota:last').attr('name', $('.txt_nfe_item_produto_especifico_combustivel_cide_aliquota:last').attr('class') + "_" + $('#hid_controle').val());
				$('.txt_nfe_item_produto_especifico_combustivel_cide_valor:last').attr('name', $('.txt_nfe_item_produto_especifico_combustivel_cide_valor:last').attr('class') + "_" + $('#hid_controle').val());
				
				// ### TRIBUTOS #################
				$('.txt_nfe_item_produto_especifico_total_tributos:last').attr('name', $('.txt_nfe_item_produto_especifico_total_tributos:last').attr('class') + "_" + $('#hid_controle').val());

				// ### PRODUTO ESPECIFICO (COMBUSTIVEL) #########################
				$('a.edit_js:last').attr('name', "edit_" + $('#hid_controle').val());
				$('a.edit_js:last').attr('href', "pedido_nfe_item,"+$('#hid_produto_id').val()+","+$('#hid_controle').val());

				//LIMPAR CAMPOS PARA LANCAR ITEM
				$('#txt_produto_codigo').val("");
				$('#txt_produto_nome').val("");
				$('#txt_produto_valor').val("");
				$('#txt_produto_quantidade').val("");
				$('#hid_produto_UM').val("");
				$('#txt_produto_desconto').val("");
				$('#txt_item_subtotal').val("");
				$('#sel_produto_tabela').val("1");
				$('#sel_produto_estoque').val("1");

				if($('#modal_otica').length > 0){ winModal($('#modal_otica')); }

				//RETORNAR FOCO PARA LANCAR NOVO ITEM
				$('#txt_produto_codigo').focus();
				totalCalcular();
				
				//calcula os tributos
				calculoTributo();

			} //SE O MODAL ESTIVER SENDO EDITADO:
			else{
				
				var n = $('#hid_item_fiscal_edit').val();
				
				var itemValorBruto			= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_principal_valor_total_bruto_'+n+']').val())));
				var itemBCICMS				= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_tributos_icms_vBC_'+n+']').val())));
				var itemICMS 				= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_tributos_icms_vICMS_'+n+']').val())));
				var itemBCICMSST			= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_tributos_icms_st_vBCST_'+n+']').val())));
				
				var itemICMSST				= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_tributos_icms_st_vICMSST_'+n+']').val())));
				var itemFrete				= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_principal_frete_'+n+']').val())));
				var itemSeguro				= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_principal_total_seguro_'+n+']').val())));
				var itemDesconto			= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_principal_desconto_'+n+']').val())));
				var itemII					= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_tributos_ii_valor_'+n+']').val())));
				var itemIPI					= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_tributos_ipi_valor_'+n+']').val())));
				var itemPIS					= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_tributos_pis_valor_'+n+']').val())));
				var itemCOFINS				= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_tributos_cofins_valor_'+n+']').val())));
				var itemBCISS				= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_'+n+']').val())));
				var itemISS					= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_tributos_issqn_valor_'+n+']').val())));
				var itemOutDespAcess		= Number(ifNumberNull(br2float($('[name=txt_nfe_pedido_item_principal_outras_despesas_acessorias_'+n+']').val())));
				var itemChckTotalCompoe		= $('[name=chk_nfe_pedido_item_principal_valor_total_bruto_'+n+']').attr('checked');
				
				var itemNovoValorBruto		= Number(ifNumberNull(br2float($('#txt_nfe_item_principal_valor_total_bruto').val())));
				var itemNovoBCICMS			= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_icms_vBC').val())));
				var itemNovoICMS 			= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_icms_vICMS').val())));
				
				var itemNovoBCICMSST		= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_icms_st_vBCST').val())));
				var itemNovoICMSST			= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_icms_st_vICMSST').val())));
				var itemNovoFrete			= Number(ifNumberNull(br2float($('#txt_nfe_item_principal_frete').val())));
				var itemNovoSeguro			= Number(ifNumberNull(br2float($('#txt_nfe_item_principal_total_seguro').val())));
				var itemNovoDesconto		= Number(ifNumberNull(br2float($('#txt_nfe_item_principal_desconto').val())));
				var itemNovoII				= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_ii_valor').val())));
				var itemNovoIPI				= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_ipi_valor').val())));
				var itemNovoPIS				= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_pis_valor').val())));
				var itemNovoCOFINS			= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_cofins_valor').val())));
				var itemNovoBCISS			= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_issqn_valor_base_calculo').val())));
				var itemNovoISS				= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_issqn_valor').val())));
				var itemNovoOutDespAcess	= Number(ifNumberNull(br2float($('#txt_nfe_item_principal_outras_despesas_acessorias').val())));
				var itemNovoChckTotalCompoe	= $('#chk_nfe_item_principal_valor_total_bruto').attr('checked');
				
				if(itemChckTotalCompoe == false){
					itemValorBruto = Number(0.00);
				}
				
				if(itemNovoChckTotalCompoe == false){
					itemNovoValorBruto = Number(0.00);
				}
				
				valPis = 0;
				valCofins = 0;
				valPisISSQN = 0;
				valCofinsISSQN = 0;
				//####################################################################################################
				//PRIMEIRO EXCLUO OS VALORES ANTIGOS DOS TOTAIS, E ENTOA LA EMBAIXO EU SOMO OS TOTAIS COM OS NOVOS VALORES
				totalProdutos 			= pedidoProdutos    - itemValorBruto;
				totalBCICMS				= pedidoBCICMS 		- itemBCICMS;
				totalICMS				= pedidoICMS 		- itemICMS;
				
				totalBCICMSST			= pedidoBCICMSST 	- itemBCICMSST;
				
				totalICMSST				= pedidoICMSST 		- itemICMSST;
				totalFrete				= pedidoFrete 		- itemFrete;
				totalSeguro				= pedidoSeguro 		- itemSeguro;
				totalDesconto			= pedidoDesconto 	- itemDesconto;
				totalII					= pedidoII 			- itemII;
				totalIPI				= pedidoIPI 		- itemIPI;
				totalBCISS				= pedidoBCISS 		- itemBCISS;
				totalISS				= pedidoISS 		- itemISS;
				totalOutDespAcess		= pedidoOutDespAcess - itemOutDespAcess;
				
				
				totalNFeAntiga			= (itemValorBruto 		+ itemICMSST	 + itemFrete	 + itemSeguro	 + itemOutDespAcess		+ itemII 	 + itemIPI		- itemDesconto).toFixed(2);
				totalNFeNova			= (itemNovoValorBruto	+ itemNovoICMSST + itemNovoFrete + itemNovoSeguro+ itemNovoOutDespAcess + itemNovoII + itemNovoIPI	- itemNovoDesconto).toFixed(2);
				
				totalNFe				= Number((pedidoTotalNFe - totalNFeAntiga)) + Number(totalNFeNova);
				
				//####################################################################################################
				//verifica se o radio de tipo de imposto anterior, esta igual ou alterou, para jogar os novos valores
				if($('[name=hid_nfe_tributo_rad_'+n+']').val() == 'icms'){
					valPis			= pedidoPIS 				- itemPIS;
					valCofins 		= pedidoCOFINS 				- itemCOFINS;
					
				}else if($('[name=hid_nfe_tributo_rad_'+n+']').val() == 'issqn'){
					valPisISSQN 	= pedidoPISTotalServicos	- itemPIS;
					valCofinsISSQN 	= pedidoCOFINSTotalServicos - itemCOFINS;
				}
				
				if($('input:radio[name=rad_imposto]:checked').val() == 'icms'){
					valPis			= valPis 		+ itemNovoPIS;
					valCofins		= valCofins		+ itemNovoCOFINS;
					
				}else if($('input:radio[name=rad_imposto]:checked').val() == 'issqn'){
					valPisISSQN		= valPisISSQN 	 + itemNovoPIS;
					valCofinsISSQN	= valCofinsISSQN + itemNovoCOFINS;
				}
				
				$('#txt_pedido_nfe_total_pis').val(float2br((valPis).toFixed(2)));
				$('#txt_pedido_nfe_total_cofins').val(float2br((valCofins).toFixed(2)));
				
				$('#txt_pedido_nfe_total_pis_servicos').val(float2br((valPisISSQN).toFixed(2)));
				$('#txt_pedido_nfe_total_cofins_servicos').val(float2br((valCofinsISSQN).toFixed(2)));
				
				//NOVOS TOTAIS
				$('#txt_pedido_nfe_total_produtos').val(float2br((totalProdutos + itemNovoValorBruto).toFixed(2)));
				$('#txt_pedido_nfe_total_bc_icms').val(float2br((totalBCICMS + itemNovoBCICMS).toFixed(2)));
				
				//calcula os tributos
				calculoTributo();
				
				$('#txt_pedido_nfe_total_icms').val(float2br((totalICMS + itemNovoICMS).toFixed(2)));
				$('#txt_pedido_nfe_total_bc_icms_substituicao').val(float2br((totalBCICMSST + itemNovoBCICMSST).toFixed(2)));
				
				$('#txt_pedido_nfe_total_icms_substituicao').val(float2br((totalICMSST + itemNovoICMSST).toFixed(2)));
				$('#txt_pedido_nfe_total_frete').val(float2br((totalFrete + itemNovoFrete).toFixed(2)));
				$('#txt_pedido_nfe_total_seguro').val(float2br((totalSeguro + itemNovoSeguro).toFixed(2)));
				$('#txt_pedido_nfe_total_desconto').val(float2br((totalDesconto + itemNovoDesconto).toFixed(2)));
				$('#txt_pedido_nfe_total_ii').val(float2br((totalII + itemNovoII).toFixed(2)));
				$('#txt_pedido_nfe_total_ipi').val(float2br((totalIPI + itemNovoIPI).toFixed(2)));
				
				$('#txt_pedido_nfe_total_bc_iss').val(float2br((totalBCISS + itemNovoBCISS).toFixed(2)));
				$('#txt_pedido_nfe_total_iss').val(float2br((totalISS + itemNovoISS).toFixed(2)));
				$('#txt_pedido_nfe_outras_despesas_acessorias').val(float2br((totalOutDespAcess + itemNovoOutDespAcess).toFixed(2)));
				$('#txt_pedido_nfe_total_nfe').val(float2br(totalNFe.toFixed(2)));
				
				if($("ul#pedido_modo_aba").length > 0){
					valorTotalServicos	 	= valorFuncionario1 + valorFuncionario2 + valorFuncionario3;
				}else{
					valorTotalServicos 		= Number(ifNumberNull(br2float($('#txt_pedido_servicos').val())));
				}
				
				valorSubTotalPedido 		= totalNFe + valorTotalServicos + valorServicoTerceiros;
				$('#txt_pedido_produtos').val(float2br(totalNFe.toFixed(2)));

				$('#txt_pedido_subtotal').val(float2br(valorSubTotalPedido.toFixed(2)));
				
				totalCalcularDesconto();
				
				//ATUALIZA CAMPOS #######################################################################################################################################################			
				$('[name=txt_item_nome_'+n+']').val($('#txt_nfe_item_principal_descricao').val());// VAI PEGAR DA NFE
				$('[name=txt_item_valor_'+n+']').val(float2br(br2float($('#txt_nfe_item_principal_valor_unitario_comercial').val()).toFixed(vendaDecimal)));// VAI PEGAR DA NFE
				$('[name=txt_item_quantidade_'+n+']').val(float2br(br2float($('#txt_nfe_item_principal_quantidade_comercial').val()).toFixed(quantidadeDecimal)));// VAI PEGAR DA NFE
				$('[name=txt_item_desconto_'+n+']').val(float2br((desconto_percent).toFixed(2)));
				
				// ### NFE! 
				//pra preservar apenas o id do produto e o numero na lsitagem, no caso de item importado, ele atualiza o parametro de 'importacao', que seria o 3�


				editar = $('[name=edit_'+n+']').attr('href').split(',');
				$('[name=edit_'+n+']').attr('href', "pedido_nfe_item,"+editar[1]+","+n);
				$('[name=chk_entregue_'+n+']').attr('checked','checked');
				$('[name=txt_nfe_pedido_item_principal_ncm_'+n+']').val($('#txt_nfe_item_principal_ncm').val());
				$('[name=txt_nfe_pedido_item_principal_ex_tipi_'+n+']').val($('#txt_nfe_item_principal_ex_tipi').val());
				$('[name=txt_nfe_pedido_item_principal_cfop_'+n+']').val($('#txt_nfe_item_principal_cfop').val());

				// ### PRODUTO ESPECIFICO (COMBUSTIVEL) #########################
				$('[name=txt_nfe_item_produto_especifico_'+n+']').val($('#sel_prod_especifico_tipo').val());
				$('[name=txt_nfe_item_produto_especifico_id_'+n+']').val($('#sel_prod_especifico_tipo').find('option:selected').attr('id'));
				$('[name=txt_nfe_item_produto_especifico_combustivel_anp_'+n+']').val($('#sel_produto_especifico_combustivel_codanp').find('option:selected').attr('id'));
				$('[name=txt_nfe_item_produto_especifico_combustivel_if_'+n+']').val($('#txt_produto_especifico_combustivel_codif').val());
				$('[name=txt_nfe_item_produto_especifico_combustivel_uf_'+n+']').val($('#sel_produto_especifico_combustivel_uf').find('option:selected').attr('id'));
				$('[name=txt_nfe_item_produto_especifico_combustivel_qtd_faturada_'+n+']').val($('#txt_produto_especifico_combustivel_qtd_faturada').val());
				$('[name=txt_nfe_item_produto_especifico_combustivel_cide_bc_'+n+']').val($('#txt_produto_especifico_combustivel_cide_bc').val());
				$('[name=txt_nfe_item_produto_especifico_combustivel_cide_aliquota_'+n+']').val($('#txt_produto_especifico_combustivel_cide_aliquota').val());
				$('[name=txt_nfe_item_produto_especifico_combustivel_cide_valor_'+n+']').val($('#txt_produto_especifico_combustivel_cide_valor').val());
				
				// ### TRIBUTOS #############
				$('[name=txt_nfe_item_produto_especifico_total_tributos_'+n+']').val($('#txt_nfe_item_total_tributos').val());

				// ### PRODUTO ESPECIFICO (COMBUSTIVEL) #########################				
				$('[name=txt_nfe_pedido_item_principal_unidade_comercial_'+n+']').val($('#txt_nfe_item_principal_unidade_comercial').val());
				$('[name=txt_nfe_pedido_item_principal_unidade_tributavel_'+n+']').val($('#txt_nfe_item_principal_unidade_tributavel').val());
				$('[name=txt_nfe_pedido_item_principal_quantidade_tributavel_'+n+']').val(float2br(br2float($('#txt_nfe_item_principal_quantidade_tributavel').val()).toFixed(quantidadeDecimal)));
				$('[name=txt_nfe_pedido_item_principal_valor_unidade_tributavel_'+n+']').val(float2br(br2float($('#txt_nfe_item_principal_valor_unidade_tributavel').val()).toFixed(vendaDecimal)));
			   
				$('[name=txt_nfe_pedido_item_principal_total_seguro_'+n+']').val(float2br(br2float($('#txt_nfe_item_principal_total_seguro').val()).toFixed(2)));
				$('[name=txt_nfe_pedido_item_principal_desconto_'+n+']').val(float2br(br2float($('#txt_nfe_item_principal_desconto').val()).toFixed(2)));
				$('[name=txt_nfe_pedido_item_principal_frete_'+n+']').val(float2br(br2float($('#txt_nfe_item_principal_frete').val()).toFixed(2)));
				
				$('[name=txt_nfe_pedido_item_principal_ean_'+n+']').val($('#txt_nfe_item_principal_ean').val());
				$('[name=txt_nfe_pedido_item_principal_ean_tributavel_'+n+']').val($('#txt_nfe_item_principal_ean_tributavel').val());
				$('[name=txt_nfe_pedido_item_principal_outras_despesas_acessorias_'+n+']').val(float2br(br2float($('#txt_nfe_item_principal_outras_despesas_acessorias').val()).toFixed(2)));
				$('[name=txt_nfe_pedido_item_principal_valor_total_bruto_'+n+']').val($('#txt_nfe_item_principal_valor_total_bruto').val());
				$('[name=txt_nfe_pedido_item_principal_pedido_compra_'+n+']').val($('#txt_nfe_item_principal_pedido_compra').val());
				$('[name=txt_nfe_pedido_item_principal_numero_item_pedido_compra_'+n+']').val($('#txt_nfe_item_principal_numero_item_pedido_compra').val());
				
				//$('[name=txt_nfe_pedido_item_principal_produto_especifico_'+n+']').val($('#sel_nfe_item_principal_produto_especifico').find('option').filter(':selected').text());
				//$('[name=hid_nfe_pedido_item_principal_produto_especifico_'+n+']').val($('#sel_nfe_item_principal_produto_especifico').val());
				$('[name=chk_nfe_pedido_item_principal_valor_total_bruto_'+n+']').attr('checked', $('#chk_nfe_item_principal_valor_total_bruto').attr('checked'));
				
				$('[name=hid_nfe_tributo_rad_'+n+']').val($('input:radio[name=rad_imposto]:checked').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_situacao_tributaria_'+n+']').val($('#sel_nfe_item_tributos_icms_situacao_tributaria').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_icms_situacao_tributaria_'+n+']').val($('#sel_nfe_item_tributos_icms_situacao_tributaria').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_origem_'+n+']').val($('#sel_nfe_item_tributos_icms_origem').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_icms_origem_'+n+']').val($('#sel_nfe_item_tributos_icms_origem').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_pCredSN_'+n+']').val($('#txt_nfe_item_tributos_icms_pCredSN').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_vCredICMSSN_'+n+']').val($('#txt_nfe_item_tributos_icms_vCredICMSSN').val());
				
				$('[name=txt_nfe_pedido_item_tributos_icms_modBC_'+n+']').val($('#sel_nfe_item_tributos_icms_modBC').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_icms_modBC_'+n+']').val($('#sel_nfe_item_tributos_icms_modBC').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_vBC_'+n+']').val($('#txt_nfe_item_tributos_icms_vBC').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_pRedBC_'+n+']').val($('#txt_nfe_item_tributos_icms_pRedBC').val());
				
				$('[name=txt_nfe_pedido_item_tributos_icms_pICMS_'+n+']').val($('#txt_nfe_item_tributos_icms_pICMS').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_vICMS_'+n+']').val($('#txt_nfe_item_tributos_icms_vICMS').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_pBCOp_'+n+']').val($('#txt_nfe_item_tributos_icms_pBCOp').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_motDesICMS_'+n+']').val($('#sel_nfe_item_tributos_icms_motDesICMS').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_icms_motDesICMS_'+n+']').val($('#sel_nfe_item_tributos_icms_motDesICMS').val());
				
				$('[name=txt_nfe_pedido_item_tributos_icms_st_modBCST_'+n+']').val($('#sel_nfe_item_tributos_icms_st_modBCST').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_icms_st_modBCST_'+n+']').val($('#sel_nfe_item_tributos_icms_st_modBCST').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_st_pRedBCST_'+n+']').val($('#txt_nfe_item_tributos_icms_st_pRedBCST').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_st_pMVAST_'+n+']').val($('#txt_nfe_item_tributos_icms_st_pMVAST').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_st_vBCST_'+n+']').val($('#txt_nfe_item_tributos_icms_st_vBCST').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_st_pICMSST_'+n+']').val($('#txt_nfe_item_tributos_icms_st_pICMSST').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_st_vICMSST_'+n+']').val($('#txt_nfe_item_tributos_icms_st_vICMSST').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_st_UFST_'+n+']').val($('#sel_nfe_item_tributos_icms_st_UFST').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_icms_st_UFST_'+n+']').val($('#hid_nfe_item_tributos_icms_st_UFST').val());
				
				$('[name=txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_'+n+']').val($('#txt_nfe_item_tributos_icms_st_vBCSTRet').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_'+n+']').val($('#txt_nfe_item_tributos_icms_st_vICMSSTRet').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_'+n+']').val($('#txt_nfe_item_tributos_icms_st_vBCSTDest').val());
				$('[name=txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_'+n+']').val($('#txt_nfe_item_tributos_icms_st_vICMSSTDest').val());
	
				$('[name=txt_nfe_pedido_item_tributos_ipi_situacao_tributaria_'+n+']').val($('#sel_nfe_item_tributos_ipi_situacao_tributaria').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_ipi_situacao_tributaria_'+n+']').val($('#sel_nfe_item_tributos_ipi_situacao_tributaria').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_'+n+']').val($('#txt_nfe_item_tributos_ipi_enquadramento_classe').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_'+n+']').val($('#txt_nfe_item_tributos_ipi_enquadramento_codigo').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_'+n+']').val($('#txt_nfe_item_tributos_ipi_produtor_cnpj').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_'+n+']').val($('#txt_nfe_item_tributos_ipi_selo_controle_codigo').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_'+n+']').val($('#txt_nfe_item_tributos_ipi_selo_controle_quantidade').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_calculo_tipo_'+n+']').val($('#sel_nfe_item_tributos_ipi_calculo_tipo').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_ipi_calculo_tipo_'+n+']').val($('#sel_nfe_item_tributos_ipi_calculo_tipo').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_'+n+']').val($('#txt_nfe_item_tributos_ipi_valor_base_calculo').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_aliquota_'+n+']').val($('#txt_nfe_item_tributos_ipi_aliquota').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_'+n+']').val($('#txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_unidade_valor_'+n+']').val($('#txt_nfe_item_tributos_ipi_unidade_valor').val());
				$('[name=txt_nfe_pedido_item_tributos_ipi_valor_'+n+']').val($('#txt_nfe_item_tributos_ipi_valor').val());
				
				$('[name=txt_nfe_pedido_item_tributos_pis_situacao_tributaria_'+n+']').val($('#sel_nfe_item_tributos_pis_situacao_tributaria').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_pis_situacao_tributaria_'+n+']').val($('#sel_nfe_item_tributos_pis_situacao_tributaria').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_calculo_tipo_'+n+']').val($('#sel_nfe_item_tributos_pis_calculo_tipo').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_pis_calculo_tipo_'+n+']').val($('#sel_nfe_item_tributos_pis_calculo_tipo').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_valor_base_calculo_'+n+']').val($('#txt_nfe_item_tributos_pis_valor_base_calculo').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_aliquota_percentual_'+n+']').val($('#txt_nfe_item_tributos_pis_aliquota_percentual').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_aliquota_reais_'+n+']').val($('#txt_nfe_item_tributos_pis_aliquota_reais').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_quantidade_vendida_'+n+']').val($('#txt_nfe_item_tributos_pis_quantidade_vendida').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_valor_'+n+']').val($('#txt_nfe_item_tributos_pis_valor').val());
				
				$('[name=txt_nfe_pedido_item_tributos_pis_st_calculo_tipo_'+n+']').val($('#sel_nfe_item_tributos_pis_st_calculo_tipo').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_pis_st_calculo_tipo_'+n+']').val($('#sel_nfe_item_tributos_pis_st_calculo_tipo').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_'+n+']').val($('#txt_nfe_item_tributos_pis_st_valor_base_calculo').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_'+n+']').val($('#txt_nfe_item_tributos_pis_st_aliquota_percentual').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_'+n+']').val($('#txt_nfe_item_tributos_pis_st_aliquota_reais').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_'+n+']').val($('#txt_nfe_item_tributos_pis_st_quantidade_vendida').val());
				$('[name=txt_nfe_pedido_item_tributos_pis_st_valor_'+n+']').val($('#txt_nfe_item_tributos_pis_st_valor').val());
				
				$('[name=txt_nfe_pedido_item_tributos_cofins_situacao_tributaria_'+n+']').val($('#sel_nfe_item_tributos_cofins_situacao_tributaria').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_cofins_situacao_tributaria_'+n+']').val($('#sel_nfe_item_tributos_cofins_situacao_tributaria').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_calculo_tipo_'+n+']').val($('#sel_nfe_item_tributos_cofins_calculo_tipo').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_cofins_calculo_tipo_'+n+']').val($('#sel_nfe_item_tributos_cofins_calculo_tipo').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_'+n+']').val($('#txt_nfe_item_tributos_cofins_valor_base_calculo').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_'+n+']').val($('#txt_nfe_item_tributos_cofins_aliquota_percentual').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_aliquota_reais_'+n+']').val($('#txt_nfe_item_tributos_cofins_aliquota_reais').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_'+n+']').val($('#txt_nfe_item_tributos_cofins_quantidade_vendida').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_valor_'+n+']').val($('#txt_nfe_item_tributos_cofins_valor').val());
				
				$('[name=txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo_'+n+']').val($('#sel_nfe_item_tributos_cofins_st_calculo_tipo').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo_'+n+']').val($('#sel_nfe_item_tributos_cofins_st_calculo_tipo').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_'+n+']').val($('#txt_nfe_item_tributos_cofins_st_valor_base_calculo').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_'+n+']').val($('#txt_nfe_item_tributos_cofins_st_aliquota_percentual').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_'+n+']').val($('#txt_nfe_item_tributos_cofins_st_aliquota_reais').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_'+n+']').val($('#txt_nfe_item_tributos_cofins_st_quantidade_vendida').val());
				$('[name=txt_nfe_pedido_item_tributos_cofins_st_valor_'+n+']').val($('#txt_nfe_item_tributos_cofins_st_valor').val());
				
				$('[name=txt_nfe_pedido_item_tributos_ii_valor_base_calculo_'+n+']').val($('#txt_nfe_item_tributos_ii_valor_base_calculo').val());
				$('[name=txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_'+n+']').val($('#txt_nfe_item_tributos_ii_valor_aduaneiras').val());
				$('[name=txt_nfe_pedido_item_tributos_ii_valor_iof_'+n+']').val($('#txt_nfe_item_tributos_ii_valor_iof').val());
				$('[name=txt_nfe_pedido_item_tributos_ii_valor_'+n+']').val($('#txt_nfe_item_tributos_ii_valor').val());
				
				$('[name=txt_nfe_pedido_item_tributos_issqn_tributacao_'+n+']').val($('#sel_nfe_item_tributos_issqn_tributacao').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_issqn_tributacao_'+n+']').val($('#sel_nfe_item_tributos_issqn_tributacao').val());
				$('[name=txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_'+n+']').val($('#txt_nfe_item_tributos_issqn_valor_base_calculo').val());
				$('[name=txt_nfe_pedido_item_tributos_issqn_aliquota_'+n+']').val($('#txt_nfe_item_tributos_issqn_aliquota').val());
				
				$('[name=txt_nfe_pedido_item_tributos_issqn_servico_lista_'+n+']').val($('#sel_nfe_item_tributos_issqn_servico_lista').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_issqn_servico_lista_'+n+']').val($('#sel_nfe_item_tributos_issqn_servico_lista').val());
				
				$('[name=txt_nfe_pedido_item_tributos_issqn_uf_'+n+']').val($('#sel_nfe_item_tributos_issqn_uf').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_issqn_uf_'+n+']').val($('#sel_nfe_item_tributos_issqn_uf').val());
				$('[name=txt_nfe_pedido_item_tributos_issqn_municipio_'+n+']').val($('#sel_nfe_item_tributos_issqn_municipio').find('option').filter(':selected').text());
				$('[name=hid_nfe_pedido_item_tributos_issqn_municipio_'+n+']').val($('#sel_nfe_item_tributos_issqn_municipio').val());
				$('[name=txt_nfe_pedido_item_tributos_issqn_valor_'+n+']').val($('#txt_nfe_item_tributos_issqn_valor').val());
				
				$('[name=txt_nfe_item_pedido_informacoes_adicionais_'+n+']').val($('#txt_nfe_item_informacoes_adicionais').val());
				
				$('[name=txt_item_subtotal_'+n+']').val(float2br(fltSubtotal.toFixed(2)));
			}

			$(this).parents("div.modal-body:last").remove();

        });
	 
		/*----------------------------------------------------------------------------------------------*/
		//CALCULAR O TOTAL
		function totalCalcular(){
			
			// TOTAIS DA NOTA ################################################################################################################
			var itemValorBruto		= Number(ifNumberNull(br2float($('#txt_nfe_item_principal_valor_total_bruto').val())));
			var itemBCICMS			= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_icms_vBC').val())));
			var itemICMS 			= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_icms_vICMS').val())));
			var itemBCICMSST		= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_icms_st_vBCST').val())));
			var itemICMSST			= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_icms_st_vICMSST').val())));
			var itemFrete			= Number(ifNumberNull(br2float($('#txt_nfe_item_principal_frete').val())));
			var itemSeguro			= Number(ifNumberNull(br2float($('#txt_nfe_item_principal_total_seguro').val())));
			var itemDesconto		= Number(ifNumberNull(br2float($('#txt_nfe_item_principal_desconto').val())));
			var itemII				= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_ii_valor').val())));
			var itemIPI				= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_ipi_valor').val())));
			var itemPIS				= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_pis_valor').val())));
			var itemCOFINS			= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_cofins_valor').val())));
			var itemBCISS			= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_issqn_valor_base_calculo').val())));
			var itemISS				= Number(ifNumberNull(br2float($('#txt_nfe_item_tributos_issqn_valor').val())));
			var itemOutDespAcess	= Number(ifNumberNull(br2float($('#txt_nfe_item_principal_outras_despesas_acessorias').val())));
			var itemChckTotalCompoe	= $('#chk_nfe_item_principal_valor_total_bruto').attr('checked');
			
			if(itemChckTotalCompoe == false){
				itemValorBruto = Number(0.00);
			}
			
			varGlobalPedido();
			totalProdutos 			= pedidoProdutos 	+ itemValorBruto;
			totalBCICMS				= pedidoBCICMS		+ itemBCICMS;
			totalICMS				= pedidoICMS 		+ itemICMS;
			totalBCICMSST			= pedidoBCICMSST 	+ itemBCICMSST;
			totalICMSST				= pedidoICMSST 		+ itemICMSST;
			totalFrete				= pedidoFrete 		+ itemFrete;
			totalSeguro				= pedidoSeguro 		+ itemSeguro;
			totalDesconto			= pedidoDesconto	+ itemDesconto;
			totalII					= pedidoII 			+ itemII;
			totalIPI				= pedidoIPI 		+ itemIPI;
			totalBCISS				= pedidoBCISS 		+ itemBCISS;
			totalISS				= pedidoISS 		+ itemISS;
			totalOutDespAcess		= pedidoOutDespAcess + itemOutDespAcess;
			
			totalNFe				= totalProdutos + totalICMSST + totalFrete + totalSeguro + totalOutDespAcess + totalII + totalIPI - totalDesconto;
			
			if($('input:radio[name=rad_imposto]:checked').val() == 'icms'){
				pedidoPIS					= pedidoPIS 	+ itemPIS;
				pedidoCOFINS				= pedidoCOFINS + itemCOFINS;
				
			}else if($('input:radio[name=rad_imposto]:checked').val() == 'issqn'){
				pedidoPISTotalServicos		= pedidoPISTotalServicos	 + itemPIS;
				pedidoCOFINSTotalServicos	= pedidoCOFINSTotalServicos + itemCOFINS;
			}
			
			$('#txt_pedido_nfe_total_produtos').val(float2br(totalProdutos.toFixed(2)));
			$('#txt_pedido_nfe_total_bc_icms').val(float2br(totalBCICMS.toFixed(2)));
			$('#txt_pedido_nfe_total_icms').val(float2br(totalICMS.toFixed(2)));
			$('#txt_pedido_nfe_total_bc_icms_substituicao').val(float2br(totalBCICMSST.toFixed(2)));
			$('#txt_pedido_nfe_total_icms_substituicao').val(float2br(totalICMSST.toFixed(2)));
			$('#txt_pedido_nfe_total_frete').val(float2br(totalFrete.toFixed(2)));
			$('#txt_pedido_nfe_total_seguro').val(float2br(totalSeguro.toFixed(2)));
			$('#txt_pedido_nfe_total_desconto').val(float2br(totalDesconto.toFixed(2)));
			$('#txt_pedido_nfe_total_ii').val(float2br(totalII.toFixed(2)));
			$('#txt_pedido_nfe_total_ipi').val(float2br(totalIPI.toFixed(2)));
			$('#txt_pedido_nfe_total_pis').val(float2br(pedidoPIS.toFixed(2)));
			$('#txt_pedido_nfe_total_cofins').val(float2br(pedidoCOFINS.toFixed(2)));
			$('#txt_pedido_nfe_total_bc_iss').val(float2br(totalBCISS.toFixed(2)));
			$('#txt_pedido_nfe_total_iss').val(float2br(totalISS.toFixed(2)));
			$('#txt_pedido_nfe_outras_despesas_acessorias').val(float2br(totalOutDespAcess.toFixed(2)));
			$('#txt_pedido_nfe_total_pis_servicos').val(float2br(pedidoPISTotalServicos.toFixed(2)));
			$('#txt_pedido_nfe_total_cofins_servicos').val(float2br(pedidoCOFINSTotalServicos.toFixed(2)));				
			$('#txt_pedido_nfe_total_nfe').val(float2br(totalNFe.toFixed(2)));	
			
			if($("ul#pedido_modo_aba").length > 0){
				valorTotalServicos	 	= valorFuncionario1 + valorFuncionario2 + valorFuncionario3;
			}else{
				valorTotalServicos 		= Number(ifNumberNull(br2float($('#txt_pedido_servicos').val())));
			}
			
			valorSubTotalPedido 		= totalNFe + valorTotalServicos + valorServicoTerceiros;
			
			$('#txt_pedido_produtos').val(float2br(totalNFe.toFixed(2)));
			$('#txt_pedido_servicos').val(float2br(valorTotalServicos.toFixed(2)));
			$('#txt_pedido_subtotal').val(float2br(valorSubTotalPedido.toFixed(2)));
			totalCalcularDesconto();
			
		};
		
		//#############################################################################################################################################
		//CALCULANDO DESCONTO
		function totalCalcularDesconto(){
			varGlobalPedido();
			valorDesconto_percentual	= valorSubTotalPedido * valorDesconto_percentual /100;
			valorDesconto 				= valorDesconto_moeda + valorDesconto_percentual;
			
			$('#txt_pedido_total').val(float2br((valorSubTotalPedido - valorDesconto).toFixed(2)));
			
			//FORMATAR COM CASAS DECIMAIS
			$('#txt_pedido_desconto').val(float2br(br2float($('#txt_pedido_desconto').val()).toFixed(2)));
			$('#txt_pedido_desconto_reais').val(float2br(br2float($('#txt_pedido_desconto_reais').val()).toFixed(2)));
		}
		
		//#############################################################################################################################################
		//CALCULANDO OS TRIBUTOS
		function calculoTributo(){
			setTimeout(function(){
				var valor_total = 0;
				$('.txt_nfe_item_produto_especifico_total_tributos').each(function(){
					var valor = br2float($(this).val()).toFixed(2);
					if(!isNaN(valor)){
						valor_total = parseFloat(valor_total) + parseFloat(valor);
					}
				});
				$('#txt_pedido_nfe_total_tributos').val(float2br(valor_total));
			}, 500);
		}
		
		//#############################################################################################################################################
		//EXCLUINDO VOLUMES E REBOQUES DO VEICULO
		$('a[href=excluir_volume]').click(function(event){
			event.preventDefault();
			
			$(this).parents(".volume_listar").remove();
		});
		
		$('.excluir_reboque').click(function(event){
			event.preventDefault();
			$(this).parents(".reboque_listar").remove();
		});
		
		//#############################################################################################################################################
		//VERIFICAR NUMERO DE REBOQUES, SEGUNDO EMISSOR NFE S� SAO PERMITIDOS 5 REBOQUES
		$('#btn_reboque_novo').click(function(event){
			event.preventDefault();
			var limite = $('#_controle').val();
			
			if(limite >= 5){
				alert('Permitido informar somente 5 reboques');
			}else{
				winModal($(this));
			}
		});
		
		//#############################################################################################################################################
		//AO ALTERNAR RADIO DO VEICULO, ELE DESABILITA OS QUE NAO FORAM SELECIONADOS, PRA NAO GRAVAR OS VALORES NO BANCO xD
		$('ul#pedido_nfe_transporte_abas a[href=veiculo]').click(function(){
	 		$('#pedido_nfe_transporte_veiculo div[id^=rad_]').hide();
			$('#pedido_nfe_transporte_veiculo div[id^=rad_] input').attr("disabled", 'disabled');
			
			$('#pedido_nfe_transporte_veiculo div[id^=rad_veiculo]').show();
			$('#pedido_nfe_transporte_veiculo div[id^=rad_veiculo] input').removeAttr('disabled');
		});
		
		$("input[name=rad_transporte]").click(function(){
			var valor = $(this).val();
	 		$('#pedido_nfe_transporte_veiculo div[id^=rad_]').slideUp('fast');
			$('#pedido_nfe_transporte_veiculo div[id^=rad_] input').attr("disabled", 'disabled');
			
			$('#pedido_nfe_transporte_veiculo div[id^=rad_'+valor+']').slideDown();
			$('#pedido_nfe_transporte_veiculo div[id^=rad_'+valor+'] input').removeAttr('disabled');
		});
		
		//#############################################################################################################################################
		//FORMATANDO CASAS AO TIRAR FOCO
		$('#txt_pedido_nfe_transporte_retencao_bc, #txt_pedido_nfe_transporte_retencao_aliquota, #txt_pedido_nfe_transporte_retencao_valor_servico, #txt_pedido_nfe_transporte_retencao_icms_retido, #txt_nfe_item_tributos_icms_pBCOp').live('change',function(){
			$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		});
		
		//#############################################################################################################################################
		//LIMPAR CAMPOS DE ENTREGA
		$('#btn_entrega_limpar').click(function(event){
			event.preventDefault();
			$('#txt_pedido_nfe_entrega_cpf_cnpj').val('');
			$('#txt_pedido_nfe_entrega_endereco').val('');
			$('#txt_pedido_nfe_entrega_numero').val('');
			$('#txt_pedido_nfe_entrega_complemento').val('');
			$('#txt_pedido_nfe_entrega_bairro').val('');
			$('#txt_pedido_nfe_entrega_cep').val('');
			$('#txt_municipio_codigo_pedido_nfe').val('');
			$('#txt_pedido_nfe_municipio').val('');
		});
		//CHECK PARA HABILITAR/DESABILITAR CAMPOS
		$('#chk_local_entrega_diferente').change(function(event){
			if($(this).attr('checked')){
				$('#txt_pedido_nfe_entrega_cpf_cnpj').attr('readonly', '');
				$('#txt_pedido_nfe_entrega_endereco').attr('readonly', '');
				$('#txt_pedido_nfe_entrega_numero').attr('readonly', '');
				$('#txt_pedido_nfe_entrega_complemento').attr('readonly', '');
				$('#txt_pedido_nfe_entrega_bairro').attr('readonly', '');
				$('#txt_pedido_nfe_entrega_cep').attr('readonly', '');
				$('#txt_municipio_codigo_pedido_nfe').attr('readonly', '');
				$('#txt_pedido_nfe_municipio').attr('readonly', '');
			}
			else{
				$('#txt_pedido_nfe_entrega_cpf_cnpj').attr('readonly', 'readonly');
				$('#txt_pedido_nfe_entrega_endereco').attr('readonly', 'readonly');
				$('#txt_pedido_nfe_entrega_numero').attr('readonly', 'readonly');
				$('#txt_pedido_nfe_entrega_complemento').attr('readonly', 'readonly');
				$('#txt_pedido_nfe_entrega_bairro').attr('readonly', 'readonly');
				$('#txt_pedido_nfe_entrega_cep').attr('readonly', 'readonly');
				$('#txt_municipio_codigo_pedido_nfe').attr('readonly', 'readonly');
				$('#txt_pedido_nfe_municipio').attr('readonly', 'readonly');
			}
		});

		/**
		 * Opera��es ao importar v�rias vendas para gerar a nota
		 */
		$('#importar-vendas').click(function() {
			var obj = $('#hid_cliente_id');
			
			if(parseInt(obj.val(), 10) < 1 || obj.val() == '') {
				alert('Antes de importar as vendas, voc� precisa escolher um cliente!');
				$('#txt_cliente_codigo').focus();
				return false;
			}
		});
		//#############################################################################################################################################
		

});