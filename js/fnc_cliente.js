


function verificaCodigoCliente(codigo,cliente_id, form) {
	if(codigo == 'auto'){
		codigo = '';
	}
	$.get('filtro_ajax.php', {verificar_cliente_codigo:'true', codigo:codigo, cliente_id:cliente_id}, function(theXML){
		$('dados',theXML).each(function(){
		
			var codigoErro = $(this).find("codigoErro").text();
			if(codigoErro == 1){
				alert('Código já cadastrado');
				return false;
			}else{
				$('#'+form).submit();
			}
		});
	});
}


$(document).ready(function(){
						   
	$('#frm_cliente #btn_enviar, #frm_chamada_bina #btn_enviar').live('click', function(event){
		event.preventDefault();
		
		cliente_id 	= $('#hid_cliente_id').val();
		codigo 		= $('#txt_codigo').val();
		form 		= $(this).parents('form:first').attr('id');
		
		verificaCodigoCliente(codigo, cliente_id, form);
	});
	
});