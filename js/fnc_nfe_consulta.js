
	function nfe_consulta(){
		documento_tipo = $('table.table_nfe_listar').attr('id');
		documento_tipo = documento_tipo.split('_');
		documento_tipo = documento_tipo[1];
		
		$.get('modal/pedido_nfe_consulta.php', {action:'autoProt'}, function(theXML){
			$('dados',theXML).each(function(){
			
				var retorno = $(this).find("retorno").text();
				var motivo = $(this).find("motivo").text();
				
				//retiro a ultima virgula e separo para separar ids das notas consultadas 
				retorno = retorno.substr(0,(retorno.length -1)); 
				retorno = retorno.split(',');
	
				for (var x=0; x < retorno.length; x++){
	
					nfe =  retorno[x].split('=');
					id 	=  Number(nfe[0]);
					
					if(nfe[0] == '656'){
						$('div.nfe_consulta_sefaz').find('p:last').css({'color':'#F50'});
						$('div.nfe_consulta_sefaz').find('p:last').text('Retorno: 656 '+nfe[1]);
					}else if(nfe[1] == '5'){
						var status_danfe = 'nfe_danfe_disabled';
						$('div.nfe_consulta_sefaz').find('p:last').css({'color':'#F50'});
						$('div.nfe_consulta_sefaz').find('p:last').text(motivo);
						
					}else if(nfe[1] == '4'){
						var status  = 'autorizada';
						var status_danfe = 'nfe_danfe';
					}
					//troco status na listagem
					$('table.table_nfe_listar tr#'+id+' td').find('a[class^=nfe_status]').attr('class', 'nfe_status_'+status+' modal');
					$('table.table_nfe_listar tr#'+id+' td').find('a[class^=nfe_status]').attr('title', status);
					$('table.table_nfe_listar tr#'+id+' td').find('a[class^=nfe_danfe]').attr('class', status_danfe);		
					
				}
			});
		});
		
		//VERIFICO SE NAO FICOU NENHUM REGISTRO NO MEIO DO CAMINHO, SE CASO O XML FOI ENVIADO PARA A PASTA, MAS NAO FOI ALTERADO O STATUS NA LISTAGEM
		$('table.table_nfe_listar tr td').find('a[class^=nfe_status_processo]').each(function(){
			id 	= $(this).attr('href').split(',');
			
			
			$.get('modal/pedido_nfe_consulta.php', {action:'consulta_nfe_db', documento_id : id[1], documento_tipo :documento_tipo}, function(theXML){
				$('dados',theXML).each(function(){
				
					var status = $(this).find("status").text();
					var documento_id = $(this).find("documento_id").text();

					if(status == '5'){
						var status = 'rejeitada';
						var status_danfe = 'nfe_danfe_disabled';
					}else if(status == '4'){
						var status  = 'autorizada';
						var status_danfe = 'nfe_danfe';
					}

					//troco status na listagem
					$('table.table_nfe_listar tr#'+documento_id+' td').find('a[class^=nfe_status]').attr({
						class: "nfe_status_"+status,
						title: status
					});

					$('table.table_nfe_listar tr#'+documento_id+' td').find('a[class^=nfe_danfe]').attr('class', status_danfe);
				});
			});
			
		});
	};
 	
	function fnc_nfe_consulta(){
		if($('table.table_nfe_listar tr td').find('a[class^=nfe_status_processo]').length > 0){
			$('.nfe_consulta_sefaz').show(1500);
		}
		$('a[class^=nfe_status_processo]').parents('tr').find('td a.edit').css("pointer-events","none");
		var intervalo = setInterval(function(){fnc_consultar()}, 10000);
		var i = 0;
		var consulta = 1;
		
		function fnc_consultar(){
			$('a[class^=nfe_status_processo]').parents('tr').find('td a.edit').css("pointer-events","");
			if(i < consulta){
				consulta = 0;
				
				if($('table.table_nfe_listar tr td').find('a[class^=nfe_status_processo]').length > 0){
					nfe_consulta();
					consulta = 1;
				}else{
					$('.nfe_consulta_sefaz').hide(1000);
				}
			}else{
				clearInterval(intervalo);
			}
		}
	}
	
	function fnc_countdown(){
		var count = 10;
		var textarea =  $('#txa_erro').text();
		countdown = setInterval(function(){
		$('#txa_erro').text(textarea + " ( "+count+" )");
		 
		if (count == 0) {
		  	count = 10;
			clearInterval(countdown);
		}
		count--;
	  }, 1000);
	};

$(document).ready(function(){
	
	fnc_nfe_consulta();
	
});