function usuario_permissao_novo() {
	$('a.btn_novo').each(function(){
				
		var this_href  = $(this).attr('href');
		var this_class = $(this).attr('class');
		//se for modal
		
		if(this_class.indexOf("modal") >=0){
			tela_arquivo = this_href.split(",");
			arquivo 	 = tela_arquivo[0];
			nova_class   = 'modal ';
		}else{
			tela_arquivo = this_href.split("p=");
			arquivo 	 = tela_arquivo[1];
			
			//VERIFICA SE TEM MAIS ALGUM PARAMETRO QUE POSSA INTERFERIR NA VERIFICACAO, COMO ID POR EXEMPLO OU MODO
			if(arquivo.indexOf('&') >=0) {
				tela_arquivo = arquivo.split("&");
				arquivo		 = tela_arquivo[0];
			}
			if(tela_arquivo[1].indexOf('modo') >=0) {
				tela_arquivo = tela_arquivo[1].split("modo=");
				arquivo 	 = arquivo+'_'+tela_arquivo[1];
				
			}
		}
		
		$.ajax({
			url: 'permissao_usuario_buscar.php',   // PAGINA A SER CHAMADA
			dataType:'html', // TIPO DE RETORNO
			type:	 'get',    // TIPO DE ENVIO POST OU GET
			async: 	 true,   // REQUISIÇÃO SINCRONA OU ASSINCRONOA
			data:	 {tela_arquivo_novo:arquivo},	// PARAMETROS A SEREM ENVIADOS
		
			success: function(dados) {
				// EXECUTA CASO SEJA REALIZADA COM SUCESSO
				nova_class += $(dados).find("class").text();
				$('a[href="'+this_href+'"]').attr('class', nova_class);
			}
		});
		delete nova_class;
	});
}


function usuario_permissao_editar() {
        
	$('a.edit').each(function(){
				
		var this_href  = $(this).attr('href');
		var this_class = $(this).attr('class');
		//se for modal
		
		if(this_class.indexOf("modal") >=0){
			tela_arquivo = this_href.split(",");
			arquivo 	 = tela_arquivo[0];
			nova_class 	 = 'modal ';
		}else{
			tela_arquivo = this_href.split("p=");
			arquivo = tela_arquivo[1];
			
			//VERIFICA SE TEM MAIS ALGUM PARAMETRO QUE POSSA INTERFERIR NA VERIFICACAO, COMO ID POR EXEMPLO OU MODO
			if(arquivo.indexOf('&') >=0) {
				tela_arquivo = arquivo.split("&");
				arquivo = tela_arquivo[0];
			}
			if(tela_arquivo[1].indexOf('modo') >=0) {
				tela_arquivo = tela_arquivo[1].split("modo=");
				arquivo = arquivo+'_'+tela_arquivo[1];
				
			}
		}
		
		$.ajax({
			url: 'permissao_usuario_buscar.php',   // PAGINA A SER CHAMADA
			dataType: 'html', // TIPO DE RETORNO
			type: 'get',    // TIPO DE ENVIO POST OU GET
			async: true,   // REQUISIÇÃO SINCRONA OU ASSINCRONOA
			data: {tela_arquivo_editar:arquivo},	// PARAMETROS A SEREM ENVIADOS
		
			success: function(dados) {
				// EXECUTA CASO SEJA REALIZADA COM SUCESSO
				nova_class += $(dados).find("class").text();
				$('a[href="'+this_href+'"]').attr('class', nova_class);
			}
		});
		delete nova_class;
	});
}