function Parcelar() {
    
    /****************************** Atributos da Classe ******************************/
	/**
	 * Atributos utilizados para calcular o número de parcelas, em caso de distribuição avulsa
	 */
    this.qtdEntrada = 0;
    this.qtdParcela = 0;
	
	/**
	 * Número inicial da parcela
	 */
    this.numeroParcela = 1;
	
	/**
	 * Número de parcelas antes de realizar o parcelamento
	 */
	this.nParcelasAnterior = 0;
	
	/**
	 * Atributos responsáveis por armazenar valores para o cálculo do valor da parcela
	 */
    this.valorPago  = 0;
    this.valorTotal = 0;
	
	/*** Atributos responsáveis por armazenar valores para calcular o juros por parcela ***/
    this.juros  			 	= 0;
    this.parcelaReferenciaJuros = 0;
    
	/**
	 * Atributo para armazenar a data de referência para o cálculo de datas (dd/mm/aaaa)
	 */
    this.data;
	
	/**
	 * Atributo opcional que armazena o número de parcelas, no caso de parcelamento pré definido
	 */
	this.nparcelas;
    
    /****************************** Métodos da Classe ******************************/
    /**
	 * Calcular o número de parcelas, caso não seja um parcelamento pré definido
	 * @param não recebe parâmetros
	 * @returns {Int} com o número de parcelas
	 */
    this.numeroDeParcelas = function() {
        var parcelas 	  = 0;
		var parcelasPagas = 0;
		
        if(typeof(this.nparcelas) == 'undefined') { parcelas = this.qtdEntrada + this.qtdParcela; }
        else { parcelas = this.nparcelas; }
		
		if($('ul.parcela_detalhe.pago').length) { parcelasPagas = $('ul.parcela_detalhe.pago').length; }
		
        return (parcelas - parcelasPagas);
    }
    
    /**
	 * Calcular o valor total a ser parcelado, já considerando valores pagos e descontos no momento de importar uma venda p/ NFe e parcelar a nfe importada
	 * @param não recebe parâmetros
	 * @returns {Float} com o valor total da venda/compra
	 */
    this.totalPago = function() {
		//alert(this.valorPago);
        return (this.valorTotal - this.valorPago);
    }
    
    /**
	 * Calcular o valor da parcela, total / nparcelas
	 * @param não recebe parâmetros
	 * @returns {Float} com o valor da parcela, fixado em duas casas decimais
	 */
    this.valorDeParcelas = function(juros, parcelaReferencia) {		
        var total    = this.totalPago();
        var parcelas = this.numeroDeParcelas();
		
        return (total / parcelas).toFixed(2);
    }
	
	/**
	 * Calcular o valor da parcela, total / nparcelas
	 * @param não recebe parâmetros
	 * @returns {Float} com o valor da parcela, fixado em duas casas decimais
	 */
    this.valorDeParcelasComJuros = function(juros, parcelaInicialJuros) {
		if(typeof(juros) == 'undefined') { juros = false; }
		if(typeof(parcelaInicialJuros) == 'undefined' || parcelaInicialJuros == this.numeroDeParcelas()) { parcelaInicialJuros = 0; }
		
		var jurosCalculado  = 0.00;
		var jurosPorParcela = 0.00;
        var parcelaTotal    = this.valorDeParcelas();
        var parcelas 	    = this.numeroDeParcelas();
        
		if(juros != false) {
			jurosCalculado  = ((juros * this.totalPago()) / 100).toFixed(2);
			jurosPorParcela = jurosCalculado / (parcelas - parcelaInicialJuros);
		}
		
        return (parseFloat(parcelaTotal) + parseFloat(jurosPorParcela)).toFixed(2);
    }
    
    /**
     * Método responsável por validar se o usuário entrou com um número de parcelas maior que 0
     *
     * @param {String} seletor -> nome do seletor que receberá foco caso de algum erro | padrão: form input#txt_parcela
     * @param {String} msg -> frase informando sobre o erro | padrão: É necessário pelo menos uma entrada ou parcela!
     * @returns {Boolean}
     */
    this.nenhumaParcela = function(seletor, msg) {
        var parcelas = this.numeroDeParcelas();
        
		if($('form input#txt_entrada').attr('disabled') == true) { msg = 'É necessário pelo menos uma parcela!'; }
		
        if(typeof(seletor) == 'undefined') { seletor = 'form input#txt_parcela'; }
        if(typeof(msg) == 'undefined') { msg = 'É necessário pelo menos uma entrada ou parcela!'; }
        
        if(parcelas == 0) {
            alert(msg);
            $(seletor).focus();
            return false;
        }
        return true;
    }
	
	/**
     * Testar se o valor da parcela é inferior a 0,01, caso o usuário de um desconto muito grande e já tenha algum valor pago para as parcelas
     * @param {Float} valor
     * @param {String} msg -> frase informando sobre o erro | padrão: O valor da parcela é inferior a R$ 0,01! Verifique se há valor pago e maior que o total!
     * @returns {Boolean} 
     */
    this.parcelaInvalida = function(valor, msg) {
        if(typeof(msg) == 'undefined'){ msg = 'O valor da parcela é inferior a R$ 0,01! Verifique se há valor pago e maior que o total!'; }
        if(valor < 0.01){ alert(msg); return true; }
        return false; }
    
    /**
     * Método responsável por validar se o total a ser parcelado é um número e maior que 0
     *
     * @param {String} seletor -> nome do seletor do campo onde está o total a ser parcelado | padrão: form input#txt_pedido_total
     * @param {String} msg -> frase informando sobre o erro | padrão: Não há nenhum valor para ser calculado!
     * @returns {Boolean}
     * 
     */
    this.nenhumTotal = function(seletor, msg) {
        if(typeof(seletor) == 'undefined') { seletor = 'form input#txt_pedido_total'; }
        if(typeof(msg) == 'undefined') { msg = 'Não há nenhum valor para ser calculado!'; }
        
        var total = br2float($(seletor).val());
        
        if(isNaN(total) || total <= 0) {
            alert(msg);
            $(seletor).focus();
            return false;
        }
        return true;
    }
	
	/**
     * Método responsável para evitar que o usuário faça novas parcelas em menor quantidade que as atuias, caso haja parcelas pagas fora de sequência
     *
     * @param {Int} numeroDeParcelas -> total de novas parcelas
     * @param {Int} nParcelasAnterior -> total de parcelas atuais
     * @param {String} msg -> frase informando sobre o erro | padrão: Você não pode gerar um número menor de parcelas que as atuais quando há parcelas pagas fora de sequência!
     * @returns {Boolean}
     * 
     */
	this.parcelaPagaForaDeSequencia = function(numeroDeParcelas, nParcelasAnterior, msg) {
        if(typeof(msg) == 'undefined') { msg = 'Você não pode gerar um número menor de parcelas que as atuais quando há parcelas pagas fora de sequência!'; }
		
		var seletor = 'ul.parcela_detalhe.pago';
		var i 		= 1;
		var erro 	= false;
		
		if($(seletor).length) {
			$(seletor).each(function() {
				if($(this).find('input.txt_parcela_numero').val() != i && (numeroDeParcelas + $(seletor).length) < nParcelasAnterior) { erro = true; }
				i++;
			});
		}
		
		if(erro) {
			alert(msg);
			return false;
		}
		
		return true;
	}
    
    /**
     * Responsável por chamar o método parcelar
     * Neste caso será feito um parcelamento 'comum'. As parcelas não estão pre cadastradas, o usuário escolhe no momento quantas parcelas
     *
     * @param Não recebe parâmetros
     * @returns {Void}
     * 
     */
    this.gerarParcelas = function() {
        this.parcelar();
    }
	
	/**
     * Responsável por chamar o método parcelar
     * Neste caso será feito um parcelamento pré definido pelo usuário.
     *
     * @param {Array} pares -> variável que armazena o intervalo e a frequencia que será feito o cálculo da data de vencimento pré definida
     * @returns {Array} com as datas de vencimentos calculadas
     * 
     */
	this.gerarParcelasPreDefinidas = function(pares) {
		var parcelas   = this.numeroDeParcelas();
		var valores    = new Array();
		var vencimento = new Array();
		
		for(i=0; i<parcelas; i++) {
			valores = pares[i].split(';');
			vencimento[i] = calcularDatasFuturas(valores[0], valores[1], this.data.split('/'));
		}
		
		this.parcelar(vencimento);
	}
	
	/**
     * Método responsável por gerar as parcelas, tanto avulsas quanto pré definidas
     *
     * @param {Array} vencimento -> matriz com datas de vencimentos, caso seja um parcelamento pre definido. Se não definido será feito o parcelamento avulso
     * @returns {Boolean}
     * 
     */
	this.parcelar = function(vencimento) {
		var nParcelasAnterior	  = this.nParcelasAnterior;
		var parcelas              = this.numeroDeParcelas();
        var valorParcelas         = this.valorDeParcelas();
        var total                 = this.totalPago();
        
        var valorParcelaFinal 	  = 0;
		var valorParcelaAcumulado = 0;
		var i 				  	  = 0;
		var numParcelaPaga		  = new Array();
		
		if(typeof(vencimento) != 'undefined' && vencimento instanceof Array) { var predefinido = true; }
        else { var y = 0 };
        
		//validando os dados de entrada antes de começar a mexer nas parcelas
		if(this.nenhumaParcela() == false || this.nenhumTotal() == false || this.parcelaPagaForaDeSequencia(parcelas, nParcelasAnterior) == false || this.parcelaInvalida(valorParcelas) == true) { return false; }
		
		//removendo para poder atualizar caso mude o numero de parcelas ou a entrada
        $('ul.parcela_detalhe:not(.pago, :first)').remove();
		
		//armazenar os números de parcelas pagas
		if($('ul.parcela_detalhe.pago').length) {
			$('ul.parcela_detalhe.pago').each(function() {
				numParcelaPaga[i] = $(this).find('input.txt_parcela_numero').val();
				i++;
			});
		}
		
        //preenchendo as parcelas
        for(x = 1; x <= parcelas; x++) {
            
            //copiando o item da lista com inputs e colocando valores
            $('div#hidden ul.parcela_detalhe').clone(true).appendTo('div#parcelas');
            
			//acumulando o valor das parcelas para calcular os centavos
			valorParcelaAcumulado += parseFloat(valorParcelas);
			
			//calculando a última parcela
			if(x == (parcelas)){
				valorParcelaFinal = parseFloat(valorParcelas) + parseFloat((Number(total) - Number(valorParcelaAcumulado)).toFixed(2));
			}
			
            //preenchendo os valores dos inputs
			
			//antes checando se o número da parcela já existe em alguma paga
			if(in_array(this.numeroParcela, numParcelaPaga)) { this.numeroParcela++; }
			
			$('input.txt_parcela_numero:last').attr('value', this.numeroParcela);
            $('input.txt_parcela_valor:last').attr('value', (x == (parcelas)) ? float2br(valorParcelaFinal.toFixed(2)) : float2br(valorParcelas));
			
			//data de vencimento, varia de acordo com o tipo de parcelamento: comum ou pré definido
			if(predefinido) {
				$('input.txt_parcela_data:last').attr('value', vencimento[(x-1)]);
			}
			else {
				$('input.txt_parcela_data:last').attr('value', calcularDatasFuturas(y, 'day', this.data.split('/')));
			}
			
			y = y + 28;
            
            //mudando o nome e id dos campos das parcelas
            $('input.txt_parcela_numero:last').attr('id', 'txt_parcela_numero_' + this.numeroParcela);
            $('input.txt_parcela_numero:last').attr('name', 'txt_parcela_numero_' + this.numeroParcela);
            
            $('input.txt_parcela_valor:last').attr('id', 'txt_parcela_valor_' + this.numeroParcela);
            $('input.txt_parcela_valor:last').attr('name', 'txt_parcela_valor_' + this.numeroParcela);
            
            $('input.txt_parcela_valor_pago:last').attr('id', 'txt_parcela_valor_pago_' + this.numeroParcela);
            $('input.txt_parcela_valor_pago:last').attr('name', 'txt_parcela_valor_pago_' + this.numeroParcela);
            
            $('input.txt_parcela_data:last').attr('id', 'txt_parcela_data_' + this.numeroParcela);
            $('input.txt_parcela_data:last').attr('name', 'txt_parcela_data_' + this.numeroParcela);
            
            $('select.sel_pagamento_tipo:last').attr('id', 'sel_pagamento_tipo_' + this.numeroParcela);
            $('select.sel_pagamento_tipo:last').attr('name', 'sel_pagamento_tipo_' + this.numeroParcela);
            
            //contando e acumulando o número da parcela
            this.numeroParcela++;
        }
        
        //controle com o número total de parcelas
		$('#hid_controle_parcela').val($('ul.parcela_detalhe:not(:first)').length);
		
		return true;
		
	}
}

/******************************************************/
/*** outros procedimentos relativos ao parcelamento ***/
/******************************************************/

$(document).ready(function() {
	
	/**
	 * 'Bloquear' parcelas pagas ou parcialmente pagas
	 */ 
	if($('ul.parcela_detalhe.pago').length) {
		
		var objParcela = $('ul.parcela_detalhe.pago');
		var divCss = {'width' : objParcela.width(),
					  'height' : 22,
					  'background' : '#000',
					  'opacity' : .3,
					  'position' : 'relative',
					  'zIndex' : 1000
					 }
		objParcela.append('<div></div>');
		objParcela.find('div').css(divCss);
		
	}
	
	/**
	 * Rotina para modificar o campo de Entrada, caso exista alguma parcela paga ao editar uma venda/pedido
	 */
	if($('ul.parcela_detalhe.pago').length) { $('form input#txt_entrada').val($('ul.parcela_detalhe.pago').length).attr('disabled', 'disabled'); }
	
	/**
	 * Validando o campo de entrada e parcela para poder apenas aceitar números inteiros e positivos
	 */ 
	$('form input#txt_entrada').live('change', function() {
		$(this).attr('value', Math.abs(Math.round($(this).val())));
		
		if(typeof($(this).val()) == 'undefined' || isNaN($(this).val())) {
			$(this).attr('value', 1);
			alert('Digite apenas números inteiros!');
		}
		else if($(this).val() < 0 || $(this).val() > 1) {
			$(this).attr('value', 1);
		}
		
		//controlando a data do 1º vencimento
		var objData   = new Date();
		var dataAtual = new Date().date(objData.getFullYear() + '-' + ((objData.getMonth() + 1)) + '-' + objData.getDate(), 1, 0, 0).format('d/m/Y');
		
		if($(this).val() == 1) { $('form input#txt_1_vencimento').val(dataAtual); }
		else { $('form input#txt_1_vencimento').val(calcularDatasFuturas(28, 'day', dataAtual.split('/'))); }
	});
	
	$('form input#txt_parcela').live('change', function() {
		$(this).attr('value', Math.abs(Math.round($(this).val())));
		
		if(typeof($(this).val()) == 'undefined' || isNaN($(this).val())) {
			$(this).attr('value', 0);
			$('form input#txt_parcela').focus();
			alert('Digite apenas números inteiros!');
		}
	});
	
	/**
	 * Rotina para recalcular parcelas abaixo de uma que está sendo modificada manualmente
	 */
	//gravando o valor inicial do campo ao receber o foco
	$valorParcelaInicial = 0.00;
	
	$('form input.txt_parcela_valor').live('focus', function() {
		$valorParcelaInicial = br2float($(this).val());
	});
	
	//alterando o valor da parcela e calculando as seguintes
	$('form input.txt_parcela_valor').live('change', function() {
		
		//vars
		var x 						= 1;
		var valorAcumulado 			= 0.00;
		var diferencaParcelaAtual	= 0.00;
		var valorRecalculado		= 0.00;
		var valorRestante			= 0.00;
		var valorParcelaAcumulado	= 0.00;
		var valorTotal				= br2float($('input#txt_pedido_total').val()); //valor total do pedido/compra
		var totalParcelas			= parseInt($('ul.parcela_detalhe input.txt_parcela_numero:last').val(), 10); //número de parcelas, o valor do campo parcelas da última parcela
		
		//inicia-se com 0
		var nParcelaAtual 			= $(this).attr('name').split('_');
			nParcelaAtual			= parseInt(nParcelaAtual[3], 10);
		var nParcelasRestantes		= 0;
		var valorParcelaAtual 		= br2float($(this).val());
		var valorParcelaFinal		= 0.00;
		
		//formatando o valor atual
		$(this).val(float2br(valorParcelaAtual.toFixed(2)));
		
		//checando se é um valor válido
		if(isNaN(valorParcelaAtual) || valorParcelaAtual <= 0 || valorParcelaAtual > valorTotal) {
			$(this).val(float2br($valorParcelaInicial.toFixed(2)));
			alert('O valor inserido na parcela nº ' + (nParcelaAtual) + ' não é válido!');
			
			return false;
		}
		
		//acumulando o valor das parcelas restantes
		for(x = nParcelaAtual + 1; x <= totalParcelas; x++) {
			valorAcumulado += br2float($('form input#txt_parcela_valor_' + x).val());
			nParcelasRestantes++;
		}
		
		//calculando as novas parcelas
		//para fazer corretamente a subtração
		if($valorParcelaInicial > valorParcelaAtual) {
			diferencaParcelaAtual = $valorParcelaInicial - valorParcelaAtual;
			valorRestante	 	  = diferencaParcelaAtual + valorAcumulado;
		}
		else {
			diferencaParcelaAtual = valorParcelaAtual - $valorParcelaInicial;
			valorRestante 		  = valorAcumulado - diferencaParcelaAtual;
		}
		
		if(nParcelasRestantes > 0) {
			valorRecalculado = valorRestante / nParcelasRestantes;	
		}
		
		//atualizando valores a partir da próxima parcela
		for(x = nParcelaAtual + 1; x <= totalParcelas; x++) {
			
			if(valorRecalculado < 0) { valorRecalculado = 0 }
			$('form input#txt_parcela_valor_' + x).val(float2br(valorRecalculado.toFixed(2)));
			
		}
		
		//passando novamente pelas parcelas para calcular os centavos
		for(x = 1; x <= totalParcelas; x++) {
			
			valorParcelaAcumulado += br2float($('form input#txt_parcela_valor_' + x + ':last').val());
			if(x == totalParcelas) {
				valorParcelaFinal = br2float($('form input#txt_parcela_valor_' + x).val()) + (valorTotal - valorParcelaAcumulado);
				$('form input#txt_parcela_valor_' + x).val(float2br(valorParcelaFinal.toFixed(2)));
			}
			
		}
		
		return true;
	
	});
	
	//zerar variável utilizada acima
	valorParcelaInicial = null;
	
	/**
	 * Executar o parcelamento ao clicar no botão OK
	 */
	$('form #btn_calcular').live('click', function(e) {
		e.preventDefault();
		
		var valorPago = 0;
		var parcelas  = new Parcelar();
		
		//alterar para pegar o valor pago!
		if($('ul.parcela_detalhe.pago').length) {
			$('ul.parcela_detalhe.pago').each(function() {
				valorPago += br2float($(this).find('input.txt_parcela_valor').val());
			});
		}
		
		if($('div#jurosParcela').length) {
			parcelas.juros = br2float($('#juros_parcelas').val());
			parcelas.parcelaInicialJuros = parseInt($('#juros_a_partir').val(), 10);
		}
		
		parcelas.nParcelasAnterior = parseInt($('input#hid_controle_estatico_parcela').val(), 10);
		parcelas.qtdEntrada    	   = parseInt($('form input#txt_entrada').val(), 10);
		parcelas.qtdParcela        = parseInt($('form input#txt_parcela').val(), 10);
		parcelas.valorPago         = valorPago;
		parcelas.valorTotal        = br2float($('input#txt_pedido_total').val());
		parcelas.data 		       = $('form input#txt_1_vencimento').val();
		parcelas.numeroParcela     = ($('ul.parcela_detalhe.pago').length) ? ($('ul.parcela_detalhe.pago').length) : 1;
		
		parcelas.gerarParcelas();
        
        $('#txt_parcela_valor_1').focus();
	});
	
	//parcelamento pré definido
	$('form #btn_calcular_pre_definido').live('click', function(e) {
		e.preventDefault();
		
		var valorPago = 0;
		var parcelas  = new Parcelar();
		
		if($('ul.parcela_detalhe.pago').length) {
			$('ul.parcela_detalhe.pago').each(function() {
				valorPago += br2float($(this).find('input.txt_parcela_valor').val());
			});
		}
		
		parcelas.nParcelasAnterior = parseInt($('input#hid_controle_estatico_parcela').val(), 10);
		parcelas.nparcelas	       = parseInt($('select#sel_perfil').val().split('|').length, 10);
		parcelas.valorPago         = valorPago;
		parcelas.valorTotal        = br2float($('input#txt_pedido_total').val());
		parcelas.data 		       = $('form input#txt_data_inicial').val();
		parcelas.numeroParcela     = ($('ul.parcela_detalhe.pago').length) ? ($('ul.parcela_detalhe.pago').length) : 1;
		
		parcelas.gerarParcelasPreDefinidas($('select#sel_perfil').val().split('|'));
	});
	
});