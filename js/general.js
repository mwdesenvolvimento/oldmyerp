


function numOnly(dom){
	dom.value=dom.value.replace(/\D/g,'');
}

function fnc_get(name){
	
	url   = window.location.search.replace("?", "");
	itens = url.split("&");
	
	for(n in itens){
		if(itens[n].match(name)){
		  return decodeURIComponent(itens[n].replace(name+"=", ""));
		}
	}
	return null;
}

//FILTRO DE DATA APAGAR
function limpar (objeto, msg) {
	if (objeto.value == msg)objeto.value = '';
}
function mostrar (objeto, msg) {
	if (objeto.value == '')objeto.value = msg;
}
/*----------------------------------------*/

function abrirPOPUP(URL, width, height) {
	window.open(URL,'janela', 'width='+width+', height='+height+', scrollbars=no, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
}

/*----------------------------------------*/
	/**** fun��o para formatar em valor monet�rio R$ ****/ 
	function float2br(number){
		number = String(number).replace(/\./, ',');
		return number;
	}
	
	/**** mudando para o padr�o americano *****/
	function br2float(number){
		number = number.replace(/\./, '');
		return parseFloat(number.replace(/,/, '.'));
	}
	
/*----------------------------------------*/

	/**** fun��o para retornar 0,00 quando valor for null ****/ 
	function ifNumberNull(number){
		number = String(number);
		if(number == '' || number == 'undefined' || number == 'NaN' ){ number = '0'; }
		return number;
	}
	
/*----------------------------------------*/
	/**** Formatar datas ****/
	/* http://www.jarbs.com.br/formatar-e-instanciar-datas-no-javascript,181.html */
	/* Exemplo:
	  //cria objeto Date, define uma data e a formata
	  alert(new Date().date('2009-12-03 09:05:01').format('H:i:s d/m/Y'));
	*/
	
	Date.prototype.date = function(a) {
	if(m = /(\d{4})-(\d{1,2})-(\d{1,2})(?:\s(\d{1,2}):(\d{1,2}):(\d{1,2}))?/.exec(a)){
		var f = ['', 'setFullYear', 'setMonth', 'setDate', 'setHours', 'setMinutes', 'setSeconds'], t = 0;

		for (var i=1; i < m.length; i++)
			if (m[i] && (t = parseInt(m[i][0] == '0' ? m[i][1] : m[i])))
				eval('this.'+f[i]+'('+ (i == 2 ? --t : t) +')');
		}

		return this;
	}
	
	Date.prototype.format = function(a) {
		var f = {
			d: ((t = this.getDate().toString()) && t.length == 1) ? "0" + t : t,
			j: this.getDate(),
			m: ((t = (this.getMonth() + 1).toString()) && t.length == 1) ? "0" + t : t,
			n: this.getMonth() + 1,
			Y: this.getFullYear(),
			y: this.getFullYear().toString().substring(2),
			H: ((t = this.getHours().toString()) && t.length == 1) ? "0" + t : t,
			G: this.getHours(),
			i: ((t = this.getMinutes().toString()) && t.length == 1) ? "0" + t : t,
			s: ((t = this.getSeconds().toString()) && t.length == 1) ? "0" + t : t,
			u: this.getMilliseconds()
		}, b = a;
	
		for (var i=0; i < a.length; i++)
			if ((t = a.substr(i, 1)) && f[t])
				b = b.replace(t, f[t]);
	
		return b;
	}
	
    
    
    dateFormat = function () {
        var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };
        
        // Regexes and supporting functions are cached through closure
        return function (date, mask, utc) {
            var dF = dateFormat;
        
            // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
            if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
                mask = date;
                date = undefined;
            }
        
            // Passing date through Date applies Date.parse, if necessary
            date = date ? new Date(date) : new Date;
            if (isNaN(date)) throw SyntaxError("invalid date");
        
            mask = String(dF.masks[mask] || mask || dF.masks["default"]);
        
            // Allow setting the utc argument via the mask
            if (mask.slice(0, 4) == "UTC:") {
                mask = mask.slice(4);
                utc = true;
            }
        
            var	_ = utc ? "getUTC" : "get",
                d = date[_ + "Date"](),
                D = date[_ + "Day"](),
                m = date[_ + "Month"](),
                y = date[_ + "FullYear"](),
                H = date[_ + "Hours"](),
                M = date[_ + "Minutes"](),
                s = date[_ + "Seconds"](),
                L = date[_ + "Milliseconds"](),
                o = utc ? 0 : date.getTimezoneOffset(),
                flags = {
                    d:    d,
                    dd:   pad(d),
                    ddd:  dF.i18n.dayNames[D],
                    dddd: dF.i18n.dayNames[D + 7],
                    m:    m + 1,
                    mm:   pad(m + 1),
                    mmm:  dF.i18n.monthNames[m],
                    mmmm: dF.i18n.monthNames[m + 12],
                    yy:   String(y).slice(2),
                    yyyy: y,
                    h:    H % 12 || 12,
                    hh:   pad(H % 12 || 12),
                    H:    H,
                    HH:   pad(H),
                    M:    M,
                    MM:   pad(M),
                    s:    s,
                    ss:   pad(s),
                    l:    pad(L, 3),
                    L:    pad(L > 99 ? Math.round(L / 10) : L),
                    t:    H < 12 ? "a"  : "p",
                    tt:   H < 12 ? "am" : "pm",
                    T:    H < 12 ? "A"  : "P",
                    TT:   H < 12 ? "AM" : "PM",
                    Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                    o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                    S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
                };
        
                return mask.replace(token, function ($0) {
                return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
            });
        };
    }();
    
    
    // Some common format strings
    dateFormat.masks = {
        "default":      "ddd mmm dd yyyy HH:MM:ss",
        shortDate:      "m/d/yy",
        mediumDate:     "mmm d, yyyy",
        longDate:       "mmmm d, yyyy",
        fullDate:       "dddd, mmmm d, yyyy",
        shortTime:      "h:MM TT",
        mediumTime:     "h:MM:ss TT",
        longTime:       "h:MM:ss TT Z",
        isoDate:        "yyyy-mm-dd",
        isoTime:        "HH:MM:ss",
        isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
        isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
    };
    
    // Internationalization strings
    dateFormat.i18n = {
        dayNames: [
            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
            "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
        ],
        monthNames: [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ]
    };
    
    //**************************************************************************
	//COMPARA QUAL DATA � MAIOR
	function comparaData(data1,data2,hora1,hora2){
		//DATA DEVE VIR NO FORMATO DD/MM/AAAA
		data1		= data1.split('/');
		data2		= data2.split('/');
		
		if(typeof(hora1) =="undefined")
			hora1 = '00:00:00';
		
		if(typeof(hora2) =="undefined")
			hora2 = '00:00:00';
		
		
		hora1		= hora1.split(':');
		hora2		= hora2.split(':');
		
		var data1 = new Date(data1[2], data1[1]-1, data1[0], hora1[0], hora1[1]);  
		var data2 = new Date(data2[2], data2[1]-1, data2[0], hora2[0], hora2[1]);  
			
		if(data1.getTime() > data2.getTime()) {
			var retorno = 'maior';
		}else if(data1.getTime() < data2.getTime()) {
			var retorno = 'menor';
		}else if(data1.getTime() == data2.getTime()){
			var retorno = 'igual';
		}
		return retorno;
	}
    //**************************************************************************
    //Calcula diferen�a entre datas
    var dateDif = {
        // Fonte: http://www.bigbold.com/snippets/posts/show/2501
        dateDiff: function(strDate1,strDate2){
        return (((Date.parse(strDate2))-(Date.parse(strDate1)))/(24*60*60*1000)).toFixed(0);
        }
    }
    
    /**
	 * Fun��o respons�vel por validar datas
	 * 
	 * @param {String} data - data separada por barra em dia, m�s e ano
	 * @returns {Boolean}
	 */
    function validaData(data){
        var er = RegExp("(0[1-9]|[012][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
        
        if(er.test(data)){
            var barras = data.split("/");
            var dia = barras[0];
            var mes = barras[1];
            var ano = barras[2];
            var d = new Date(ano, mes-1, dia, 1, 0, 0);
            
            if(dia != d.getDate()) {
                return false;
            } else if(mes != (d.getMonth() + 1)) {
                return false;
            } else if(ano != d.getFullYear()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
   }
   
    /**
	 * Fun��o respons�vel por calcular datas futuras
	 * 
	 * @param {String}{Int} frequencia - quantas vezes ser� aplicado o intervalo
	 * @param {String} intervalo - nome do intervalo que ser� aplicado a soma de datas
	 * @param {Array} dataInicial - data separada em dia, m�s e ano
	 * 
	 * @returns {String} data formatada em dd/mm/yyyy
	 */
	function calcularDatasFuturas(frequencia, intervalo, dataInicial) {
		
        var data = new Date(parseInt(dataInicial[2], 10), parseInt(dataInicial[1], 10) - 1, parseInt(dataInicial[0], 10), 1, 0, 0); //1,0,0 para evitar problemas com o hor�rio de ver�o
        
		switch(intervalo) {
			case 'day': data.setDate(data.getDate() + parseInt(frequencia, 10));
			break;
			
			case 'week': data.setDate(data.getDate() + parseInt(frequencia, 10) * 7);
			break;
			
			case 'month': data.setMonth(data.getMonth() + parseInt(frequencia, 10));
			break;
			
			case 'year': data.setFullYear(data.getFullYear() + parseInt(frequencia, 10));
			break;
		}
		
		return data.format('d/m/Y');
		
	}
	
	/**
	 * Fun��o equivalente ao in_array do PHP, ela verifica se determinado valor est� no array
	 * 
	 * @param {Mixed} valor - valor a ser buscado na matriz
	 * @param {Array} matriz - matriz que ser� examinada
	 * 
	 * @returns {Boolean}
	 */
	function in_array(valor, matriz) {
		for(var i in matriz) {
			if(valor == matriz[i]) {
				return true;
			}
		}
		return false;
	}
   
    //**************************************************************************
    //APENAS NUMEROS
    /*
	function num(dom){
		dom.value=dom.value.replace(/\D/g,'');
	}
	*/
 	//Fun��es para retirar espa�o em branco de uma string
 	   
	
    //in�cio e fim da string
    function trim(str) { return str.replace(/^\s+|\s+$/g,""); }
 
	//left trim
	function ltrim(str) { return str.replace(/^\s+/,""); }
 
	//right trim
	function rtrim(str) { return str.replace(/\s+$/,""); }
    
    
    $(document).ready(function() {
        //**************************************************************************
        //Navega��o em abas
         
        //checando se existe o elemento pai do conte�do das abas
        if($('div.tabs').length) {
            $('div.tab').hide();
            $('div.tab.ativo').show();
            
            var tabLink = $(".tabs").find("ul.menu_modo li a");
            tabLink.live('click', function(e) {
				e.preventDefault();
           		$(this).blur();
                
                var tab = $(this).attr('href');
                $('div.tab.ativo').hide();
                $('div.tab').removeClass('ativo');
                $('#tab_'+tab).addClass('ativo');
                $('#tab_'+tab).show();
                
				$(this).parents(".menu_modo").find("li a").removeClass('atual');
                $(this).addClass('atual');
                
            });
        }
    });