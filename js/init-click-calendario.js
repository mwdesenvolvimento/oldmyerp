$(document).ready(function(){
	
	$('a.exibir-calendario').live('click', function(e){
		
		e.preventDefault();
		var target = $(this).prev('input').attr('id');
		target = '#' + target;
		
	    $(target).calendario({
	        target : $(target),
			top : 0,
	        dateDefault : $(target).val()
	    });
	});
	
	//para campos que não possuem uma data como valor padrão
	//nome de classe: calendario-data-atual
	$('a.exibir-calendario-data-atual').live('click', function(e){
		
		e.preventDefault();
		var target = $(this).prev('input').attr('id');
		target = '#' + target;
		
	    $(target).calendario({
	        target : $(target),
			top : 0
	    });
	});
	
});