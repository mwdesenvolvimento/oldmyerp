

$(document).ready(function(){

/* hidden li */
	$("#sel_tipo_conta").change(function() {
		 $("li.conta_nome").show();	  
		 $("li.conta_btn").show();

		if( $(this).attr("value")== 1){		
			$("li.conta_saldo").show();
			$("li.conta_banco").hide();
			$("li.conta_banco").attr('value', '');
			$("li.conta_bandeira").hide();
			$("li.conta_bandeira").attr('value', '');
			$("li.conta_fecha_fatura").hide();
			$("li.conta_fecha_fatura").attr('value', '');
			$("li.conta_venc_fatura").hide();
			$("li.conta_venc_fatura").attr('value', '');
		}
		if( $(this).attr("value")== 2){										
			$("li.conta_banco").show();
			$("li.conta_bandeira").show();
			$("li.conta_fecha_fatura").show();
			$("li.conta_venc_fatura").show();
			
			$("li.conta_saldo").hide();
			$("li.conta_saldo").attr('value', '');
		}
		if( $(this).attr("value")== 3){										
			$("li.conta_banco").show();
			$("li.conta_saldo").show();
			
			$("li.conta_bandeira").hide();
			$("li.conta_bandeira").attr('value', '');
			$("li.conta_fecha_fatura").hide();
			$("li.conta_fecha_fatura").attr('value', '');
			$("li.conta_venc_fatura").hide();
			$("li.conta_venc_fatura").attr('value', '');
		}
		if( $(this).attr("value")== 4){										
			$("li.conta_banco").show();
			$("li.conta_saldo").show();
			
			$("li.conta_bandeira").hide();
			$("li.conta_bandeira").attr('value', '');
			$("li.conta_fecha_fatura").hide();
			$("li.conta_fecha_fatura").attr('value', '');
			$("li.conta_venc_fatura").hide();
			$("li.conta_venc_fatura").attr('value', '');
		}
	});
	
		
/***************************************************************************************************************/
	
	//selecionar conta no financeiro
	$("select[name=sel_conta_id]").change(function(){
		var id = $(this).attr('value');
		window.location="index.php?p=financeiro&modo=conta_fluxo&conta_id="+id;
	});

});