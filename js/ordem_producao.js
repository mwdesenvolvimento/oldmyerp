var urlDestino = 'filtro_ajax.php';

//OP
$(document).ready(function(){
						   
	/*----------------------------------------------------------------------------------------------*/
	//BUSCAR DADOS DO PRODUTO
	//QUANDO RECEBE O FOCO, LIMPA OS CAMPOS EVITANDO PROBLEMAS PORQUE EXISTEM PRODUTOS QUE PODEM NAO TER COMPONENTES
	
	$('form[id^=frm_op_novo] input#txt_produto_codigo').blur(function(){
		codigo 	= $(this).attr('value');

		if($('.txt_op_codigo_op').val() != 'novo'){op_id = $('.txt_op_codigo_op').val()}
		else{ op_id = 0; }
		
		if(codigo != ''){
			$.get(urlDestino, {busca_produto_op:codigo}, function(theXML){
				$('dados',theXML).each(function(){
					var produto_id 		= $(this).find("produto_id").text();
					var produto 		= $(this).find("produto").text();
					var componente_id	= $(this).find("produto_componentes").text();
					var componente_qtd	= $(this).find("produto_componentes_qtd").text();
					var componente_desc	= $(this).find("produto_componentes_desc").text();
					var componente_um	= $(this).find("produto_componentes_um").text();
					var estoque 		= Number($(this).find("produto_estocado").text());
					var UM				= $(this).find("produto_um").text();

					//exibe os valores
					$('#hid_op_produto_id').val(produto_id);
					$('#txt_op_produto_nome').val(produto);
					$('#hid_op_produto_UM').val(UM);
					$('#hid_op_componente_id').val(componente_id);
					$('#hid_op_componente_qtd').val(componente_qtd);
					$('#hid_op_componente_um').val(componente_um);
					$('#hid_op_estocado').val(estoque);
					$('#txt_op_produto_quantidade').val('1');
					$('#hid_op_componente_desc').val(componente_desc);
					$('#txt_op_produto_quantidade').focus();
					$('#txt_op_produto_quantidade').select();
				});
			});
		}
	});

	//INSERIR PRODUTO NA ORDEM
	if(fnc_get("p") == 'ordem_producao_importar' || fnc_get("p") == 'ordem_producao_novo' || fnc_get("p") == 'ordem_producao_detalhe'){

		$('#sel_status_op').change(function(){
			if($(this).val() == '3'){
				var data = $('#data_atual_op').val();
				$("#txt_finalizado_op").attr('disabled', '');
				$("#txt_finalizado_op").val(data);
				$("#txt_finalizado_op").focus();
			}else{
				$("#txt_finalizado_op").attr('disabled', 'disabled');
				$("#txt_finalizado_op").val('');
			}
		});
		
		$('#chk_lote_op').change(function(){
			var nAtual = $('#hid_numero_lote').val();
			if($(this).attr('checked')){
				$('#txt_lote_op').val(nAtual);
			}
			else{
				$('#txt_lote_op').val('');
			}
		});
		
		$('#btn_op_item_inserir').click(function(event){
			event.preventDefault();

			if($('.txt_codigo_op').val() != 'novo'){op_id = $('.txt_codigo_op').val()}else{op_id = 0}
			var quantidade 				= br2float($('#txt_op_produto_quantidade').attr('value')); //
			var produto_id 				= $('#hid_op_produto_id').attr('value'); //
			var produto_codigo			= $('#txt_produto_codigo').attr('value'); //
			var produto_nome			= $('#txt_op_produto_nome').attr('value'); //
			var componentes_id			= $('#hid_op_componente_id').attr('value');
			var componentes_qtd_orig	= $('#hid_op_componente_qtd').attr('value');
			var componentes_qtd 		= '';
			var arr_componentes_qtd		= componentes_qtd_orig.split(',');

			$(arr_componentes_qtd).each(function(index){
				var nv = arr_componentes_qtd[index] * quantidade;
				componentes_qtd = componentes_qtd+nv+",";
			})

			componentes_qtd = componentes_qtd.substr(0,(componentes_qtd.length - 1));

			var componentes_desc		= $('#hid_op_componente_desc').attr('value');
			var componentes_um			= $('#hid_op_componente_um').attr('value');
			var produto_estocado		= $('#hid_op_estocado').attr('value');
			var produto_um				= $('#hid_op_produto_UM').attr('value');

			if(produto_nome != "" && produto_codigo != ""){
				//o parametro true em clone faz com que seja copiado tamb�m os eventos do elemento
				$('#pedido_lista_item:first').clone(true).appendTo('#pedido_lista');

				//LIMPAR CURSOR DE TODOS OS ITENS
				$('tr[title=pedido_lista_item]').removeClass('cursor');
				//MARCAR CURSOR DO ITEM ATUAL
				$('tr[title=pedido_lista_item]:last').addClass('cursor');

				//EXIBIR LINHA QUE ORIGINALMENTE � DISPLAY:NONE
				$('tr[title=pedido_lista_item]:last').css('display','table-row');

				$('.hid_item_op_produto_id:last').val(produto_id);
				$('.txt_item_op_codigo:last').val(produto_codigo);
				$('.txt_produto_op_desc:last').val(produto_nome);
				$('.hid_item_op_id:last').val(op_id);
				$('.txt_produto_op_qtd:last').val(quantidade);
				$('.hid_produto_componente_op:last').val(componentes_id); 
				$('.hid_produto_componente_qtd_op:last').val(componentes_qtd);
				$('.hid_produto_componente_desc_op:last').val(componentes_desc);
				$('.txt_produto_op_estocado:last').val(produto_estocado);
				$('.txt_produto_op_um:last').val(produto_um);

				/*----------------------------------------------------------------*/
				//NUMERAR CONTROLE ATUAL
				$('#hid_controle').val(parseInt($('#hid_controle').val()) + 1);
				var controle = $('#hid_controle').val();

				$('.hid_item_op_id:last').attr(					{name: $('.hid_item_op_id:last').attr('class') + "_" + controle,					id: $('.hid_item_op_id:last').attr('class') + "_" + controle});
				$('.hid_item_op_produto_id:last').attr(			{name: $('.hid_item_op_produto_id:last').attr('class') + "_" + controle,			id: $('.hid_item_op_produto_id:last').attr('class') + "_" + controle});
				$('.txt_item_op_codigo:last').attr(				{name: $('.txt_item_op_codigo:last').attr('class') + "_" + controle,				id: $('.txt_item_op_codigo:last').attr('class') + "_" + controle});
				$('.txt_produto_op_desc:last').attr(			{name: $('.txt_produto_op_desc:last').attr('class') + "_" + controle,				id: $('.txt_produto_op_desc:last').attr('class') + "_" + controle});
				$('.txt_produto_op_qtd:last').attr(				{name: $('.txt_produto_op_qtd:last').attr('class') + "_" + controle,				id: $('.txt_produto_op_qtd:last').attr('class') + "_" + controle});
				$('.txt_produto_op_um:last').attr(				{name: $('.txt_produto_op_um:last').attr('class') + "_" + controle,					id: $('.txt_produto_op_um:last').attr('class') + "_" + controle});
				$('.txt_produto_op_estocado:last').attr(		{name: $('.txt_produto_op_estocado:last').attr('class') + "_" + controle,			id: $('.txt_produto_op_estocado:last').attr('class') + "_" + controle});
				$('.hid_produto_componente_op:last').attr(		{name: $('.hid_produto_componente_op:last').attr('class') + "_" + controle,			id: $('.hid_produto_componente_op:last').attr('class') + "_" + controle});
				$('.hid_produto_componente_qtd_op:last').attr(	{name: $('.hid_produto_componente_qtd_op:last').attr('class') + "_" + controle,		id: $('.hid_produto_componente_qtd_op:last').attr('class') + "_" + controle});
				$('.hid_produto_componente_desc_op:last').attr(	{name: $('.hid_produto_componente_desc_op:last').attr('class') + "_" + controle,	id: $('.hid_produto_componente_desc_op:last').attr('class') + "_" + controle});
				
				/* especificos */
				$('.hid_item_op_fldMaquina:last').attr(						{name: $('.hid_item_op_fldMaquina:last').attr('class') + "_" + controle,						id: $('.hid_item_op_fldMaquina:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldAcerto_Maquina:last').attr(				{name: $('.hid_item_op_fldAcerto_Maquina:last').attr('class') + "_" + controle,					id: $('.hid_item_op_fldAcerto_Maquina:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldPapel_Produzido:last').attr(				{name: $('.hid_item_op_fldPapel_Produzido:last').attr('class') + "_" + controle,				id: $('.hid_item_op_fldPapel_Produzido:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldRetorno_Papel:last').attr(				{name: $('.hid_item_op_fldRetorno_Papel:last').attr('class') + "_" + controle,					id: $('.hid_item_op_fldRetorno_Papel:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldPapel_Entregue:last').attr(				{name: $('.hid_item_op_fldPapel_Entregue:last').attr('class') + "_" + controle,					id: $('.hid_item_op_fldPapel_Entregue:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldAcerto_Papel:last').attr(				{name: $('.hid_item_op_fldAcerto_Papel:last').attr('class') + "_" + controle,					id: $('.hid_item_op_fldAcerto_Papel:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldPapel_Perdido:last').attr(				{name: $('.hid_item_op_fldPapel_Perdido:last').attr('class') + "_" + controle,					id: $('.hid_item_op_fldPapel_Perdido:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldQtd_Perdida:last').attr(					{name: $('.hid_item_op_fldQtd_Perdida:last').attr('class') + "_" + controle,					id: $('.hid_item_op_fldQtd_Perdida:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldTotal_Etiquetas:last').attr(				{name: $('.hid_item_op_fldTotal_Etiquetas:last').attr('class') + "_" + controle,				id: $('.hid_item_op_fldTotal_Etiquetas:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldAmostra:last').attr(						{name: $('.hid_item_op_fldAmostra:last').attr('class') + "_" + controle,						id: $('.hid_item_op_fldAmostra:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldAmostra_De:last').attr(					{name: $('.hid_item_op_fldAmostra_De:last').attr('class') + "_" + controle,						id: $('.hid_item_op_fldAmostra_De:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldRemetente:last').attr(					{name: $('.hid_item_op_fldRemetente:last').attr('class') + "_" + controle,						id: $('.hid_item_op_fldRemetente:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldContagem_Funcionario_Id:last').attr(		{name: $('.hid_item_op_fldContagem_Funcionario_Id:last').attr('class') + "_" + controle,		id: $('.hid_item_op_fldContagem_Funcionario_Id:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldEmpacotagem_Funcionario_Id:last').attr(	{name: $('.hid_item_op_fldEmpacotagem_Funcionario_Id:last').attr('class') + "_" + controle,		id: $('.hid_item_op_fldEmpacotagem_Funcionario_Id:last').attr('class') + "_" + controle});
				$('.hid_item_op_fldQtd_Pacote:last').attr(					{name: $('.hid_item_op_fldQtd_Pacote:last').attr('class') + "_" + controle,						id: $('.hid_item_op_fldQtd_Pacote:last').attr('class') + "_" + controle});
				
				$('.edit_js:last').attr({href: "ordem_producao_item,"+controle});
				/* especificos */

				componentes_id = componentes_id.split(',');
				componentes_qtd = componentes_qtd.split(',');
				componentes_desc = componentes_desc.split(',');
				componentes_um = componentes_um.split(',');
				
				if(componentes_id[0] != ''){
					$(componentes_id).each(function(index){
					
						if($('.hid_componente_id_'+this)[0]){
							
							var usadoAtual = $('.hid_componente_id_'+componentes_id[index]).parents('#pedido_lista_item_componente').find('.txt_componente_op_qtd').val();
							usadoAtual = usadoAtual.replace('.', '');
							var usadoNovo  = parseFloat(usadoAtual)+parseFloat(componentes_qtd[index]);//heere
							
							var usadoParaAtual = $('.hid_componente_id_'+componentes_id[index]).parents('#pedido_lista_item_componente').find('.txt_componente_usado_para').val();
							var usadoParaNovo = usadoParaAtual + ", " + produto_nome;
							
							$('.hid_componente_id_'+componentes_id[index]).parents('#pedido_lista_item_componente').find('.txt_componente_op_qtd').val(usadoNovo);
							$('.hid_componente_id_'+componentes_id[index]).parents('#pedido_lista_item_componente').find('.txt_componente_usado_para').val(usadoParaNovo);
							
							
						}
						else{
							
							$('#pedido_lista_item_componente:first').clone(true).appendTo('#pedido_lista_componente');
							
							//LIMPAR CURSOR DE TODOS OS ITENS
							$('tr[title=pedido_lista_item_componente]').removeClass('cursor');
							//MARCAR CURSOR DO ITEM ATUAL
							$('tr[title=pedido_lista_item_componente]:last').addClass('cursor');
			
							//EXIBIR LINHA QUE ORIGINALMENTE � DISPLAY:NONE
							$('tr[title=pedido_lista_item_componente]:last').css('display','table-row');
			
							$('.hid_componente_id:last').val(this);
							$('.txt_componente_op_desc:last').val(componentes_desc[index]);
							$('.txt_componente_op_qtd:last').val(componentes_qtd[index]);
							$('.txt_componente_op_um:last').val(componentes_um[index]);
							$('.txt_componente_usado_para:last').val(produto_nome);
			
							/*----------------------------------------------------------------*/
							$('.hid_componente_id:last').attr('class', 'hid_componente_id_'+this);
							$('.hid_componente_id:last').attr('id', 'hid_componente_id_'+this);
							$('.hid_componente_id:last').attr('name', 'hid_componente_id_'+this);

						}

					});
				}
				

				//LIMPAR CAMPOS PARA LANCAR ITEM
				$('#txt_produto_codigo').val("");
				$('#hid_op_produto_id').val("");
				$('#txt_op_produto_nome').val("");
				$('#txt_op_produto_quantidade').val("");
				$('#hid_op_produto_UM').val("");
				$('#hid_op_componente_id').val("");
				$('#hid_op_componente_qtd').val("");
				$('#hid_op_componente_desc').val("");
				$('#hid_op_componente_um').val("");
				$('#hid_op_estocado').val("");

				//RETORNAR FOCO PARA LANCAR NOVO ITEM
				$('#txt_produto_codigo').focus();
			}
		});

		//EXCLUIR ITEM
		$('.a_op_excluir').click(function(event){
			event.preventDefault();

			var item_salvo 		= $(this).parents("#pedido_lista_item").find(".hid_item_op_salvo").val();
			var componentes 	= $(this).parents("#pedido_lista_item").find(".hid_produto_componente_op").val();
			var componentesQtd 	= $(this).parents("#pedido_lista_item").find(".hid_produto_componente_qtd_op").val();
			componentes			= componentes.split(',');
			componentesQtd		= componentesQtd.split(',');
			
			if(componentes[0] != ''){
				$(componentes).each(function(i){
					var usadoAtual = $('.hid_componente_id_'+componentes[i]).parents('#pedido_lista_item_componente').find('.txt_componente_op_qtd').val();
					usadoAtual = usadoAtual.replace('.', '');
					var usadoNovo  = parseFloat(usadoAtual) - parseFloat(componentesQtd[i]);
					$('.hid_componente_id_'+componentes[i]).parents('#pedido_lista_item_componente').find('.txt_componente_op_qtd').val(usadoNovo);
					
					var qtdAtualizado = $('.hid_componente_id_'+componentes[i]).parents('#pedido_lista_item_componente').find('.txt_componente_op_qtd').val();
					if(qtdAtualizado <= '0')
					{ $('.hid_componente_id_'+componentes[i]).parents('#pedido_lista_item_componente').find('input').val('0');
					  $('.hid_componente_id_'+componentes[i]).parents('#pedido_lista_item_componente').remove(); }
				})
			}
			
			if(item_salvo == '1'){$(this).parents("#pedido_lista_item").find(".hid_item_op_excluido").val('1');}
			else{$(this).parents("#pedido_lista_item").find("input").val('0');}
			$(this).parents("#pedido_lista_item").hide(); //remove visualmente
			
			return false;
		});
		
	}//se for OP
	
	
});