
//exibir janelinha legenda ao passar mouse
var marginX = 10; //distancia do mouse em x
var marginY = 15; //distancia do mouse em y
function mostrarPopUp(texto) {
	var objMouse = document.getElementById("mouse");
	objMouse.style.display = "block";
	document.onmousemove = function(event) {

		if (document.all) {//Internet Explorer
			_x = (document.documentElement && document.documentElement.scrollLeft) ? document.documentElement.scrollLeft : document.body.scrollLeft;
			_y = (document.documentElement && document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
			_x += (window.event.clientX+marginX);
			_y += (window.event.clientY+marginY);
		
		} else {//Firefox, Opera
			_x = (event.pageX+marginX);
			_y = (event.pageY+marginY);
		}
			
		objMouse.innerHTML = texto;
		objMouse.style.left = _x+"px";
		objMouse.style.top = _y+"px";
	}
}

function ocultarPopUp() {
	var objMouse = document.getElementById("mouse");
	objMouse.style.display = "none";
}


//CHECAR AS PARCELAS E O VALOR TOTAL PARA NÃO GRAVAR ERRADO !!! #########################################################################################################################//
//crio uma função pra ser usada na venda comum e na venda rapida, feita atraves do modal


/*********************************************************************/
$(document).ready(function(){
	
	//travando o F5
	$(document).keydown(function(event){
		switch(event.keyCode){
			case 116: //F5
				event.preventDefault();
			break;
		}
		//desabilitar CTRL+J de leitores de códigos de barra
		if(event.ctrlKey && ((event.keyCode == 106) || (event.keyCode == 74))){
			return false;
		}
	 });
	
	textboxes = $("input[type=text], input[type=submit], input[type=password], textarea[name=txt_produto_nome], select, input[type=select], input[type=checkbox], button, input[type=button]");
	if ($.browser.mozilla) {
		//$(textboxes).keypress (checkForEnter);
		$(textboxes).live('keypress keydown',checkForEnter);
	}else{
		//$(textboxes).keydown (checkForEnter);
		$(textboxes).live('keydown',checkForEnter);
	}
	
	function checkForEnter (event) {
		//repete a definição dos boxes para quando abrir modal
		console.log('enter');
		if($('div.modal-conteudo:last').length){
			textboxes = $("input[type=text], input[type=submit], input[type=password], select, input[type=select], textarea, input[type=checkbox], button, input[type=button]");
		}
		//console.log(this);
		switch(event.keyCode){
			case 13:
				currentBoxNumber = textboxes.index(this);
				if($(this).attr('type') != 'submit'){
					if (textboxes[currentBoxNumber + 1] != null) {
						nextBox = textboxes[currentBoxNumber + 1]
						nextBox.focus();
						event.preventDefault();
						return false;
					}
				}
				break;
			default:
				//alert(event.keyCode);
				break;
		}
	}
	
	//SELECIONAR AO GANHAR FOCO
	$(textboxes).live('focus',function(){
		$(this).select();
	});

	//ABRIR LINKS EM NOVA JANELA
	$('*[rel=externo]').click(function(){
		$(this).attr('target','_blank');
	});
	
	/***************************************************************************************************************/
	$(":checkbox[name=chk_todos]").live('click', function(){
 		//$(":checkbox[name^='chk_']:visible").attr('checked', $(this).attr('checked'));
 		$(":checkbox[name^='chk_']").attr('checked', $(this).attr('checked'));
	});
   /****************************************************************************************************************/
   
   	var urlDestino = 'filtro_ajax.php';
	$('#txt_cliente_codigo').live('keyup', function(event){
		console.log(this);
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	$('#txt_cliente_codigo').live('change', function(){
		fnc_cliente_fetch($(this).val());
	});
	
	
	function fnc_cliente_fetch(codigo){
		codigo = trim(codigo);
		
		if($('#txt_codigo').length){
			var pedidoId = $('#txt_codigo').val();
			if(pedidoId == 'novo'){ pedidoId = 0};
		}else{
			pedidoId = 0
		}
		
		if(codigo!=0){
			$.get(urlDestino, {busca_cliente:codigo, pedidoId:pedidoId}, function(theXML){
				$('dados',theXML).each(function(){
					var disabled 			= $(this).find("disabled").text();
					var cliente_id 			= $(this).find("cliente_id").text();
					var cliente_nome 		= $(this).find("cliente_nome").text();
					var funcionario_id 		= $(this).find("funcionario_id").text();
					var funcionario_nome 	= $(this).find("funcionario_nome").text();
					var exibir_observacao	= $(this).find("exibir_observacao").text();
					var cliente_observacao 	= $(this).find("cliente_observacao").text(); 
					
					var cliente_endereco 	= $(this).find("cliente_endereco").text();
					var totalVendas 		= $(this).find("totalVendas").text();					
					
					if(disabled == 1){
						alert('O cliente '+unescape(cliente_nome)+' foi desabilitado!');
						$('#txt_cliente_codigo').focus();
						
						//exibe o valor
						$('#hid_cliente_id').val('');
						$('#txt_cliente_nome').val('');
						$('#txt_cliente_codigo').val('');
						
					}else{
						if(totalVendas != ''){
							totalVendas = Number(ifNumberNull(totalVendas)) + 1;
						}
						//alert('teste');
						if(cliente_id > 0 && exibir_observacao == 1 && cliente_observacao != ""){
							alert(cliente_observacao);
						}
						
						//exibe o valor
						$('#hid_cliente_id').val(cliente_id);
						$('#txt_cliente_nome').val(unescape(cliente_nome));
						
						if($('.janela-modal').length == 0 && $('#txt_funcionario_codigo').val() =='' && fnc_get("p") != 'pedido_nfe_detalhe' && fnc_get("p") != 'pedido_detalhe'){//no caso de venda rapida, o cliente eh selecionado depois do funcionario
							$('#txt_funcionario_codigo').val(funcionario_id);
							$('#txt_funcionario_nome').val(unescape(funcionario_nome));
						}
						
						//responsavel
						if($("select[name=sel_cliente_responsavel]").val() != '2'){
							$("select[name=sel_cliente_responsavel]").html('<option value="0">Carregando...</option>');
							
							$.post(urlDestino,{sel_cliente_responsavel:cliente_id},function(valor){
								$("select[name=sel_cliente_responsavel]").html(valor);
							})
						}
						
						//se for automotivo
						if($("select[name=sel_veiculo]").val() == 0 || $("select[name=sel_veiculo] > option").length == 0){
							$("select[name=sel_veiculo]").html('<option value="0">Carregando...</option>');
						}
						var veiculo_id = $("select[name=sel_veiculo]").val();
						
						$.post(urlDestino,{sel_veiculo_id:cliente_id, veiculo_id:veiculo_id},function(valor){
							$("select[name=sel_veiculo]").html(valor);
						})
						
						// se for financeira
						$("select[name=sel_conta_bancaria]").html('<option value="0">Carregando...</option>');
						$.post(urlDestino,{sel_conta_bancaria_id:cliente_id},function(valor){
							$("select[name=sel_conta_bancaria]").html(valor);
						})
						
						if($('input#txt_pedido_endereco').length > 0){
							$('#txt_pedido_endereco').val(unescape(cliente_endereco));
						}
						
						$('#txt_cliente_pedido').val(totalVendas);
						
						//$('#txt_cliente_nome').focus();
					}
					
				});
			});
			$('#txt_entrada').attr('readonly', '');
			$('#txt_parcela').attr('readonly', '');
			$('#txt_1_vencimento').attr('readonly', '');
		}else{
			//caso seja consumidor, nao deixar gerar parcelas, somente pagamentos a vista
			
			$('#txt_entrada').val('1');
			$('#txt_parcela').val('0');
			$('#txt_entrada').attr('readonly', 'readonly');
			$('#txt_parcela').attr('readonly', 'readonly');
			//controlando a data do 1º vencimento
			var objData   = new Date();
			var dataAtual = new Date().date(objData.getFullYear() + '-' + ((objData.getMonth() + 1)) + '-' + objData.getDate(), 1, 0, 0).format('d/m/Y');
			
			$('#txt_1_vencimento').attr('readonly', 'readonly');
			$('#txt_1_vencimento').val(dataAtual);
		}
	}
	
	
   /****************************************************************************************************************/
	
	$('#txt_dependente_codigo').live('keyup', function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	$('#txt_dependente_codigo').live('blur', function(){
		var codigo = $(this).val();
		pedidoId = 0;
		$.get(urlDestino, {busca_cliente:codigo, pedidoId:pedidoId}, function(theXML){
			$('dados',theXML).each(function(){
				var dependente_id		= $(this).find("cliente_id").text();
				var dependente_nome 	= $(this).find("cliente_nome").text();
				var dependete_endereco 	= $(this).find("cliente_endereco").text();
				
				$('#hid_dependente_id').val(dependente_id);
				$('#txt_dependente_nome').val(unescape(dependente_nome));
				
				if($('input#txt_pedido_endereco').length > 0 && $('#txt_pedido_endereco').val() == ''){
					$('#txt_pedido_endereco').val(unescape(dependete_endereco));
				}
				
				//se for automotivo
				if($("select[name=sel_veiculo]").val() == 0 || $("select[name=sel_veiculo] > option").length == 0){
					$("select[name=sel_veiculo]").html('<option value="0">Carregando...</option>');
				}
				var veiculo_id = $("select[name=sel_veiculo]").val();
				
				$.post(urlDestino,{sel_veiculo_id:dependente_id, veiculo_id:veiculo_id},function(valor){
					$("select[name=sel_veiculo]").html(valor);
				});
			});	
		});		
	});
	
	//CARREGANDO DADOS VEICULO
	 $("select[name=sel_veiculo]").change(function(){
														   
		$.get(urlDestino, {sel_veiculo_dados:$(this).val()}, function(theXML){
		$('dados',theXML).each(function(){
		
			var chassi = $(this).find("veiculo_chassi").text();
			var ano	 = $(this).find("veiculo_ano").text();

			$('#txt_veiculo_chassi').val(chassi);
			$('#txt_veiculo_ano').val(ano);
		});
	});
	
	})
	
	
   /****************************************************************************************************************/
	//SE FOR LISTAR CONTA NA VENDA
	$('#sel_conta_bancaria').blur(function(){
		var id = $(this).attr('value');
		
		if($(this).val() != ''){
			$.post(urlDestino, {sel_conta_bancaria_dados:id}, function(theXML){
				$('dados',theXML).each(function(){
				
					var agencia = $(this).find("agencia").text();
					var titular = $(this).find("titular").text();
	
					$('#txt_agencia').val(agencia);
					$('#txt_titular').val(titular);
				});
			});
		}
     });		
		
	/*----------------------------------------------------------------------------------------------*/	
	$('#txt_produto_codigo').keyup(function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	/*
	$('#txt_produto_codigo').focus(function(){
		if($(this).val() != ''){
			//$('#txt_produto_codigo').blur();
			setTimeout(function(){$('#txt_produto_codigo').blur()}, 1000);
		}
	});
	
	$('#txt_produto_codigo').focus(function(){
		
		$.post("modal/produto_busca_retorno.php",
		function(data){
			if(data){
				$('#txt_produto_codigo').val(data);
				$("#txt_produto_codigo").blur();
				$("#txt_produto_nome").focus();
				data = '';
			}
		});
		
	});*/
	/*----------------------------------------------------------------------------------------------*/
	
	$('#txt_municipio_codigo').live('keyup', function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	$('#txt_municipio_codigo').live('focus', function(){
		$.post("modal/municipio_busca_retorno.php",
		function(data){
			if(data){
				$('#txt_municipio_codigo').val(data);
				$('#txt_municipio_codigo').blur();
				$('#txt_municipio_codigo').focus();
			}
		});
	});
		
	$('#txt_municipio_codigo').live('blur', function(){
		var municipio_codigo = $(this).attr('value');	
		$.get(urlDestino, {municipio_codigo_busca:municipio_codigo}, function(theXML){
			$('dados',theXML).each(function(){
				var nome = $(this).find("nome").text();
				var uf 	 = $(this).find("uf").text();
				$('#txt_municipio').val(unescape(nome));
				$('#txt_uf').val(uf);
			});
		});
     });
	
	/*----------------------------------------------------------------------------------------------*/
	$('#txt_endereco_codigo, #txt_endereco').live('keyup', function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	$('#txt_endereco_codigo').live('focus',function(){
		$.post("modal/endereco_busca_retorno.php",
		function(data){
			if(data){
				$('#txt_endereco_codigo').val(data);
				$("#txt_endereco_codigo").blur();
				$('#txt_endereco').focus();
			}
		});
	});
		
	$('#txt_endereco_codigo').live('blur', function(){
		var id = $(this).attr('value');		
		if($(this).val != ''){
			$.get(urlDestino, {endereco_codigo_busca:id}, function(theXML){
				$('dados',theXML).each(function(){
				
					var endereco = $(this).find("endereco").text();
					var bairro	 = $(this).find("bairro").text();
					var setor	 = $(this).find("setor").text();
					var cep		 = $(this).find("cep").text();
					
					$('#txt_endereco').val(unescape(endereco));
					$('#txt_bairro').val(unescape(bairro));
					$('#txt_setor').val(unescape(setor));
					$('#txt_cep').val(unescape(cep));
				});
			});
		}
     });
	
	/*----------------------------------------------------------------------------------------------*/
	//municipio naturalidade
	$('#txt_municipio_codigo_naturalidade').keyup(function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	$('#txt_municipio_codigo_naturalidade').focus(function(){
		$.post("modal/municipio_busca_retorno.php",
		function(data){
			if(data){
				$('#txt_municipio_codigo_naturalidade').val(data);
			}
		});
	});
		
	$('#txt_municipio_codigo_naturalidade').blur(function(){
		var id = $(this).attr('value');
		$.get(urlDestino, {municipio_codigo_busca:id}, function(theXML){
			$('dados',theXML).each(function(){
			
				var nome = $(this).find("nome").text();
				var uf = $(this).find("uf").text();

				$('#txt_municipio_naturalidade').val(unescape(nome));
				$('#txt_uf_naturalidade').val(uf);
			});
		});
     });		
	
	/*----------------------------------------------------------------------------------------------*/
	//municipio pedido_nfe
	$('#txt_municipio_codigo_pedido_nfe').keyup(function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	$('#txt_municipio_codigo_pedido_nfe').focus(function(){
		$.post("modal/municipio_busca_retorno.php",
		function(data){
			if(data){
				$('#txt_municipio_codigo_pedido_nfe').val(data);
			}
		});
	});
		
	$('#txt_municipio_codigo_pedido_nfe').blur(function(){
		var id = $(this).attr('value');
		$.get(urlDestino, {municipio_codigo_busca:id}, function(theXML){
			$('dados',theXML).each(function(){
			
				var nome = $(this).find("nome").text();
				var uf 	 = $(this).find("uf").text();

				$('#txt_pedido_nfe_municipio').val(unescape(nome));
			});
		});
     });		

/*----------------------------------------------------------------------------------------------*/
	
	$('#txt_funcionario_codigo').keyup(function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	$('#txt_funcionario_codigo').focus(function(){
		$.post("modal/funcionario_busca_retorno.php",
		function(data){
			if(data){
				$('#txt_funcionario_codigo').val(data);
				$("#txt_funcionario_codigo").blur();
				$("#txt_funcionario_nome").focus();
			}
		});
	});
		
	
	$('#txt_funcionario_codigo').blur(function(){
		var funcionarioId = $(this).val();
		$.post(urlDestino, {busca_funcionario: funcionarioId}, function(theXML){
			$('dados',theXML).each(function(){
			
				var nome	 = $(this).find("nome").text();
				var comissao = $(this).find("comissao").text();
				
				if ($('#txt_funcionario_codigo').val()!=''){
					$('#txt_funcionario_nome').val(nome);
					
					if ($('#hid_funcionario_comissao').length> 0){
						$('#hid_funcionario_comissao').val(comissao);
					}
				}else{
					$('#txt_funcionario_nome').val('');
					if ($('#hid_funcionario_comissao').length > 0){
						$('#hid_funcionario_comissao').val('');
					}
				}
			});
		});
     });		
	
/*----------------------------------------------------------------------------------------------*/
	
	$('#txt_fornecedor_codigo').keyup(function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	$('#txt_fornecedor_codigo').focus(function(){
		$.post("modal/fornecedor_busca_retorno.php",
		function(data){
			if(data){
				$('#txt_fornecedor_codigo').val(data);
				$("#txt_fornecedor_codigo").blur();
				$("#txt_fornecedor_nome").focus();
				
			}
		});
	});
	
   $('#txt_fornecedor_codigo').blur(function(){
		var fornecedorId =  $('#txt_fornecedor_codigo').attr("value");
		
		$.post(urlDestino,
		{busca_fornecedor: $('#txt_fornecedor_codigo').val()},
		function(data){
			if ($('#txt_fornecedor_codigo').val()!=''){
				$('#txt_fornecedor_nome').val(data);
			}
			else{
				$('#txt_fornecedor_nome').empty();
			}
		});
		
		if(fornecedorId != ''){
			//LIBERA O BOTAO DE IMPORTAR COTACAO
			$("a.btn_import").attr({
				"href": 'cotacao_listar,'+fornecedorId
			});
		}
	});
	
	/*----------------------------------------------------------------------------------------------*/	
	
	$('input[name$="_transportador_codigo"]').keyup(function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	$('input[name$="_transportador_codigo"]').focus(function(){
			$.post("modal/transportador_busca_retorno.php",
			function(data){
				if(data){
					$('input[name$="_transportador_codigo"]').val(data);
					$('input[name$="_transportador_nome"]').focus();
					data = '';
				}
			});
	});
	
	$('#txt_transportador_codigo').blur(function(){
		$.post(urlDestino,
		{busca_transportador: $('#txt_transportador_codigo').val()},
		function(data){
			if ($('#txt_transportador_codigo').val()!=''){
				$('#txt_transportador_nome').val(data);
			}
			else{
				$('#txt_transportador_nome').empty();
			}
		});
	});
	
/********************************************************************************************************************/
	$('#txt_banco_codigo').blur(function(){
		var codigo = $(this).attr('value');
		if(codigo == ''){
			
			$('#txt_banco_nome').val('');
		}else{
			$.get(urlDestino, {banco_codigo:codigo}, function(theXML){
				$('dados',theXML).each(function(){
				
					var nome = $(this).find("nome").text();
	
					$('#txt_banco_nome').val(unescape(nome));
				});
			});
		}
	});		

/*************************************************************************************************************************/
	//CONTAS PROGRAMADAS - COMPORTAMENTO DO FORMULÁRIO DE CADASTRO E ATUALIZAÇÃO
	$('form#frm_conta_programada select#sel_data_termino').change(function(){
		if($(this).val() == 1){
			$('form#frm_conta_programada input#txt_data_termino').removeClass('hidden').addClass('visible');
			$('form#frm_conta_programada a#hidden-calendar').removeClass('hidden').addClass('visible');
			
			$('form#frm_conta_programada input#txt_data_termino').val($('form#frm_conta_programada input#txt_data_inicio').val());
		}
		else{
			$('form#frm_conta_programada input#txt_data_termino').removeClass('visible').addClass('hidden');
			$('form#frm_conta_programada a#hidden-calendar').removeClass('visible').addClass('hidden');
			
			$('form#frm_conta_programada input#txt_data_termino').val('');
		}
	});
	
	/***************************************************************************************************************/
	$("select[name=sel_obrigatorio_tela]").change(function(){
		var id = $(this).attr('value');
		window.location="index.php?p=configuracao&id="+id;
	});

/***************************************************************************************************************/
	$("select[name=sel_usuario_novo]").change(function(){
		var tela_id = $(this).attr('value');
		var usuario_id = $('#hid_usuario_id').val()
		window.location="index.php?p=usuario_novo&id="+usuario_id+"&tela_id="+tela_id;
	});
	
	$("select[name=sel_usuario_detalhe]").change(function(){
		var tela_id = $(this).attr('value');
		var usuario_id = $('#hid_usuario_id').val()
		window.location="index.php?p=usuario_detalhe&id="+usuario_id+"&tela_id="+tela_id;
	});
	/*----------------------------------------------------------------------------------------------*/
	//FOCO NO PRIMEIRO CAMPO
	$('[name=frm_general] input[name=txt_nome]').focus();
	$('[name=frm_usuario] input[name=txt_usuario]').focus();
	$('#frm_fornecedor input[name=txt_nome_fantasia]').focus();
	$('#frm_pedido_novo input[name=txt_cliente_codigo]').focus();
	$('#frm_compra_novo input[name=txt_fornecedor_codigo]').focus();
	
	//definir campo onde começa com foco
	var parametro = $('#sys_venda_rapida_primeiro_campo').val();
	$('#frm_pedido_rapido_novo input[name='+parametro+']').focus();
/***************************************************************************************************************/
//SELECT MUNICIPIO AO CARREGAR UF

	$(".sel_uf").live('change',function(){
		$(".sel_municipio").html('<option value="0">Carregando...</option>');
		$.post("filtro_ajax.php", {action : 'sel_uf', uf : $(this).val() },
		function(valor){
			$(".sel_municipio").html(valor)
		})
	})
/***************************************************************************************************************/
	$("#btn_adicionar").click(function(){
		var tornar_padrao = $("select[name=sel_tornar_padrao]").val();
		if(tornar_padrao == 1){
			$.post("filtro_ajax.php", {verificar_endereco_padrao : ''},
			function(resultado_busca){
				if(resultado_busca){
					var confirmar_continuacao = confirm("O endereço padrão atual será substituído!\n Deseja continuar?");
					if(confirmar_continuacao){
						$("#frm_adicional").submit();
					}else{
						$("select[name=sel_tornar_padrao]").focus();
					}
				}
			})
		}else{
			$("#frm_adicional").submit();
		}
	})

});