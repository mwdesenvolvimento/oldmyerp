
$(document).ready(function(){
						   
				
  //####################################################################################################################################
   
   //Combo dinamico no FILTRO produto para sele��o de sub categoria
   $("select[name=sel_categoria], .sel_categoria").change(function(){
														   
		$("select[name=sel_sub_categoria], .sel_sub_categoria").html('<option value="0">Carregando...</option>');
		$.post("filtro_ajax.php",
		{sel_categoria_id:$(this).val()},
		function(valor){
			$("select[name=sel_sub_categoria], .sel_sub_categoria").html(valor);
		})
	
	})
 		
	//VERIFICA SE O CODIGO OU NOME DO PRODUTO ESTA DUPLICADO
	/**************************************************************/
	$("form#frm_produto input#btn_enviar").click(function(event){
		event.preventDefault(); 
		
		var prefixo		= $('#txt_prefixo').val();
		var codigo 		= $('#txt_codigo').val();
		var produto		= $('#txt_nome').val();
		var produtoId	= ifNumberNull($('#hid_produto_id').val());
		var duplica		= $('#hid_duplica').val();
		var check		= $('#chk_prefixo').attr('checked');
		
		var categoria	= $('#sel_categoria').val();
		var marca		= $('#sel_marca').val();
		
		if(check == true){
			codigo = prefixo + codigo;
		}
		
		if(produto == ''){
			alert("O nome do produto deve ser cadastrado!");
			return false;
		}

		//VERIFICACAO PARA OS CAMPOS ESPECIFICOS OBRIGATORIOS (07/01)
		var ceErros = '';
		$('.CEobrigatorio').each(function(){
			if ($(this).val() == ''){
				ceErros = '1';
			}
		});
		/*************************************************************/
		if(ceErros != ''){
			alert('Preencha os campos especificos obrigatorios');
			$('#especificacoes_link').trigger('click');
			return false;
		}
		
		$.get("filtro_ajax.php", {produto_codigo:codigo, produto:produto, produtoId:produtoId, duplica:duplica, categoriaId:categoria, marcaId:marca}, function(theXML){
			$('dados',theXML).each(function(){
			
				var codigoCheck = $(this).find("codigo").text();
				var nomeCheck 	= $(this).find("nome").text();
				
				if(codigoCheck == 'erro'){
					alert("O C�digo "+ codigo +" j� existe!");
					return false;
				}else if(nomeCheck == 'erro'){
					alert("J� existe um produto com esse nome!");
					return false;
				}else{
					$('form:first').submit();
				}
				
			});
		});
	});
	
/*/-----------------------------------------------------/*/
	
	$("select[name=sel_produto_exibir]").change(function(){

		var currentTime = new Date()
		var month 		= currentTime.getMonth() + 1
		var day 		= currentTime.getDate()
		var year 		= currentTime.getFullYear()
		
		if (day < 10){
			day = "0" + day
		}
		if (month < 10){
			month = "0" + month
		}
		
		var date1 = ("01" + "/" + month + "/" + year)
		var date2 = (day + "/" + month + "/" + year)
		
		if($(this).val() == "todos"){
			$("input#txt_data1").attr({
				"disabled":"disabled"
			});
			$('#txt_data1').val("");
			
			$("input#txt_data2").attr({
				"disabled":"disabled"
			});
			$('#txt_data2').val("");
			
		}else{
			$("input#txt_data1").attr({
				"disabled":""
			});
			$('#txt_data1').val(date1);
			
			$("input#txt_data2").attr({
				"disabled":""
			});
			$('#txt_data2').val(date2);
		}
	});
	
	/**************************************************************/
	$("input.txt_acrescimo").blur(function(){
		if($(this).attr("value") == ""){
			$(this).val('0')
		}
		var tabelaId = ($(this).attr("name")).split('_');
		var compra 	= $("input#hid_valor_compra").attr("value");
		var venda 	= $("input#hid_valor_venda").attr("value");
		var modo 	= $("input#hid_tabela_modo").attr("value");
		
		var lucro 	= br2float($(this).attr("value"));
		
		if(modo == 1){
			valor = compra;
		}else if(modo == 2){
			valor = venda;
		}
		
		var total = (valor * lucro)/100;
		var total = parseInt(valor) + total;
		
		$('#txt_valor_'+(tabelaId[2])).val(float2br((total).toFixed(2)));
		$(this).val(float2br((lucro).toFixed(2)));
	});	
	
	$("input.txt_valor").blur(function(){
									   
		var compra = parseInt($("input#hid_valor_compra").attr("value"));
		
		var tabelaId = ($(this).attr("name")).split('_');
		var preco = br2float($(this).attr("value"));
		
		var total = ((preco / compra) - 1) * 100;
		
		$('#txt_margem_lucro_'+(tabelaId[2])).val(float2br(total.toFixed(2)));
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
	});	
	
	

	//VERIFICA SE SIGLA DA TABELA DE PRECO JA EXISTE
	/**************************************************************/
	$("form#frm_produto_tabela input#btn_salvar").click(function(event){
		event.preventDefault(); 
		
		var nome		= $('#txt_nome').val();
		var sigla		= $('#txt_sigla').val();
		var tabelaId	= $('#hid_id').val();
		
		if(nome == ''){
			alert("O nome da tabela deve ser cadastrada!");
			return false;
		}else if(sigla == ''){
			alert("A sigla deve ser cadastrado!");
			return false;
		}		
		
		$.post("filtro_ajax.php", {tabelaPrecoNome:nome, tabelaId:tabelaId, tabelaSigla:sigla}, function(theXML){
			$('dados',theXML).each(function(){
			
				var nome = $(this).find("nome").text();
				var sigla = $(this).find("sigla").text();
					
				if(nome == 'erro'){
					alert("O Nome cadastrado j� existe!");
					return false;
				}else if(sigla == 'erro'){
					alert("A Sigla cadastrada j� existe!");
					return false;
				}else{
					$('form#frm_produto_tabela').submit();
				}
			});
		});
	});
	
	
	
/************************************************************************************************************/

	$("input#txt_lucro_margem").blur(function(){
		calc();
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
	});	
	
	$("input#txt_valor_compra").blur(function(){
		calc();
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
	});	
	
	$("input#txt_valor_compra").blur(function(){
		calc();
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
	});	
	
   	function calc(){
		if($("input#txt_lucro_margem").attr("value") == ""){
			$('#txt_lucro_margem').val('0')
		}
		var preco = br2float($("input#txt_valor_compra").val()).toFixed(2);
		var lucro = br2float($("input#txt_lucro_margem").val()).toFixed(2);
		var total = (preco * lucro)/100;
		var total_venda = (Number(preco) + Number(total));
		
		if(Number(lucro) > 0){
			$("input#txt_valor_venda").val(float2br(total_venda.toFixed(2)));
		}
	};
	
});

