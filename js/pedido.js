var urlDestino 		= 'filtro_ajax.php';
var venda_decimais	= 0;
var qtd_decimais	= 0;

jQuery.ajax({  
	type: "GET",  
	url: urlDestino,  
	async : false,  
	data : {casas_decimais:0},
	success : function (dados) {  
		venda_decimais		= $(dados).find("venda").text();
		qtd_decimais		= $(dados).find("qtd").text();
	}  
});

function checkfinalizarParcelas() {

	var erro 			   = 0;
	var valorTotalParcelas = 0.00;
	var totalParcelas	   = parseInt($('ul.parcela_detalhe:not(:first)').length, 10);
	//var valorPago		   = ($('ul.parcela_detalhe.pago').length) ? br2float($('ul.parcela_detalhe.pago').find('input.txt_parcela_valor').val()) : 0;
	var valorTotal		   = br2float($('input#txt_pedido_total').val()); 

	for(x = 1; x <= totalParcelas; x++) {

		if(br2float($('input#txt_parcela_valor_' + x).val()) < 0) {
			alert('A parcela n� ' + (x) + ' possui um valor incorreto!');
			$('input#txt_parcela_valor_' + x).select();
			erro = 1;
		}
		else {
			valorTotalParcelas += br2float($('form input#txt_parcela_valor_' + x + ':last').val());
		}
		
		if(!validaData($('input#txt_parcela_data_' + x).val())) {
			alert('A parcela n� ' + (x) + ' possui uma data inv�lida ou nula!');
			$('input#txt_parcela_data_' + x).select();
			erro = 1;
		}
	}
	//if($('ul.parcela_detalhe.pago').length) { valorTotalParcelas = valorTotalParcelas - valorPago; }
	
	if(valorTotalParcelas.toFixed(venda_decimais) != valorTotal.toFixed(venda_decimais) && valorTotalParcelas.toFixed(venda_decimais) != '0.00') {
		alert('O valor total das parcelas (R$ '+ float2br(valorTotalParcelas.toFixed(venda_decimais)) +') n�o � igual ao valor total do pedido (R$ '+ float2br(valorTotal.toFixed(venda_decimais)) +')! Favor corrigir!');
		erro = 1;
	}

	if(erro == 1) { return false; } else { return true; }
}

/*----------------------------------------------------------------------------------------------*/

	//CALCULO PARA VERIFICAR ESTOQUE AO INSERIR ITEM
	function calculaEstoque(produto_id, estoqueId, quantidade, pedido_id){
		totalVendaInserido 	= 0;
		vendaRemovido 		= 0;
		retorno				= true;
		//verifica no caso de edicao de venda #####################################################################################################
		$('hid_controle_item_produto_id[value='+produto_id+']').parents('ul.item_estoque').find('.hid_controle_item_estoque').each(function(){
			if($(this).parents('ul.item_estoque').find('.hid_item_estoque_id').val() == estoqueId){
				vendaRemovido += br2float($(this).val());
			}
		}); 
		
		$('.hid_item_produto_id[value='+produto_id+']').parents('ul#pedido_lista_item').find('.txt_item_quantidade').each(function(){
			if($(this).parents('ul#pedido_lista_item').find('.hid_item_estoque_id').val() == estoqueId){
				totalVendaInserido += br2float($(this).val());
			}
		});
		jQuery.ajax({  
			type	: "GET",  
			url		: "filtro_ajax.php",  
			async 	: false,  
			data 	: {produto_estoque_confere:produto_id, estoque_id:estoqueId, pedido_id:pedido_id},  
			success : function (dados) { 
		
				estoqueSaldo		= $(dados).find("estoqueSaldo").text();
				totalInseridoAntigo	= $(dados).find("totalInseridoAntigo").text(); //pra saber se item ja foi entregue anteriormente, e nao contar como desconto de estoque
				estoqueControle		= $(dados).find("estoqueControle").text(); //pra saber se item ja foi entregue anteriormente, e nao contar como desconto de estoque
				novaQuantidade 		= Number(quantidade) + (Number(totalVendaInserido) - Number(totalInseridoAntigo));
				
				if(novaQuantidade > estoqueSaldo && estoqueControle == 1){
					retorno = 'false';
				}
			}  
		});  
		return retorno;
		
	}
	
/*----------------------------------------------------------------------------------------------*/

	//CALCULO QUE VARRE OS ITENS AO GRAVAR FUNCAO, EH UM POUCO DIFERENTE AO ANTERIOR, SERVE PARA HORA DE FINALIZAR A VENDA
	function verificaEstoque() {
		retorno 	= true;
		quantidade	= 0;
		vendaStatus	= $('#sel_status').find('option').filter(':selected').text();
		limite 		= $('#hid_controle').attr('value'); 
		n = 1;
		
		if($('.txt_codigo_pedido').val() != 'novo'){pedido_id = $('.txt_codigo_pedido').val() }else{pedido_id = 0}
			
		while(n <= limite){
			itemCodigo	 = $('input[name=txt_item_codigo_'+n+']').attr('value'); //verifica se o item j� tinha sido inserido na venda, no caso de estar no editar venda - soh tem no pedido_detalhe
			produto_id	 = $('input[name=hid_item_produto_id_'+n+']').attr('value'); //verifica se o item j� tinha sido inserido na venda, no caso de estar no editar venda - soh tem no pedido_detalhe
			estoque_id	 = $('input[name=hid_item_estoque_id_'+n+']').attr('value'); 
			itemEntregue = $(':checkbox[name=chk_entregue_'+n+']').attr('checked');
			
			if(itemEntregue == 1 && produto_id > 0){
				if(calculaEstoque(produto_id, estoque_id, quantidade, pedido_id) == 'false'){
					alert('Produto '+itemCodigo+' : N�o h� quantidade suficiente em estoque!');
					retorno = false;
				}
			}
			n++;
		}
		return retorno;
	}; 

/*----------------------------------------------------------------------------------------------*/

	function verificaLimiteCredito(){
		erro = 0;
		verificar_limite_credito 	= 	$("#hid_verificar_limite_credito").val(); //pega o par�metro para ver se vai verificar ou n�o o limite de cr�dito.
		cliente_id					=	$("#hid_cliente_id").val(); //pega o id do cliente que est� comprando esse produto.
		if(cliente_id > 0 && verificar_limite_credito == 1){
			//AGORA FAZ A VERIFICACAO LOGO QUE O CLIENTE � INSERIDO, ENTAO NAO TEM MAIS RELACAO COM AS PARCELAS (parte comentada)


			/*********************************************************************************************************************************************************************************************************************************************************************************
			/*pedido_total				=	br2float($("#txt_pedido_total").val()).toFixed(decimais); //pega o valor total do pedido.
			primeira_parcela  			=	br2float($("#txt_parcela_valor_1").val()); //pega o valor da primeira parcela.
			parcela_vencimento			=	$("#txt_parcela_data_1").val(); //pega a data de vencimento da primeira parcela.
					
			/* Na verifica��o do Limite de Cr�dito, os valores retornados, s�o os seguintes:
			 * 1 - N�o.
			 * 2 - Sim.
			 */
			/*pedido_resultado		= 	pedido_total - primeira_parcela;
			dados_data				= 	new Date(); //faz a inst�ncia do objeto data, para pegar os dados da data atual.
			dia_atual				=	dados_data.getDate(); //pega o dia atual.
			mes_atual				=	dados_data.getMonth() + 1; //pega o m�s atual.
			ano_atual				=	dados_data.getFullYear(); //pega o ano atual.
			data_atual				=	ano_atual + PadDigits(mes_atual) + PadDigits(dia_atual); //junta o dia, o m�s, e o ano em uma vari�vel.
			data_parcela_vencimento =	parcela_vencimento.split('/'); //separa tudo que estiver entre uma / (barra) na data do vencimento, e logo depois ele retorna em uma array. xD.
			vencimento_dia			=	data_parcela_vencimento[0]; //pega o dia de vencimento da parcela.
			vencimento_mes			=	data_parcela_vencimento[1]; //pega o m�s de vencimento da parcela.
			vencimento_ano			=	data_parcela_vencimento[2]; //pega o ano de vencimento da parcela.
			data_vencimento			=	vencimento_ano + vencimento_mes + vencimento_dia;
			
			//mudando o valor dos campos select das parcelas
			var x = 0;
			var totalParcelas	= $('ul.parcela_detalhe').length;
			var numeroAtual 	= $("#txt_parcela_valor_1").attr('name').split('_');
			var tipoPagamento 	= $(this).val();
			
			if(data_atual >= data_vencimento){
				pedido_total	=	parseInt(pedido_total) - parseInt(primeira_parcela); //Desconta o valor da primeira parcela, pois ela � � vista.
			}
			/*
			 * Essa condi��o vai fazer o seguinte:
			 * Verificar se o Cliente n�o � consumidor, para isso, ele vai verificar se o id do cliente escolhido � maior do que zero,
			 * caso seja, significa que ele n�o � um consumidor, pois o consumidor normal, � cadastrado com o ID zero, e n�o precisa
			 * de nenhuma verifica��o.
			 * Ap�s verificar se ele n�o � um consumidor, ele vai verificar se na configura��o do sistema, est� habilitada ou n�o
			 * para verificar o limite de cr�dito.
			 * Caso ambas condi��es estejam verdadeiras, ele vai executar a verifica��o abaixo para verificar o limite de cr�dito.
			*/
		
			/*for(x = numeroAtual[3]; x < totalParcelas; x++) { //faz um loop em todas as parcelas.
				var data_parcela		=	$("#txt_parcela_data_" + x).val().split('/'); //pega a data de vencimento da primeira parcela.
				var tipoPagamento_Valor		= 	$('form select#sel_pagamento_tipo_' + x).attr('value'); //pega o tipo da parcela.
				var parcelaValor 		=	br2float($('form input#txt_parcela_valor_' + x).attr('value')); //pega o valor dessa parcela.
				
				var vencimento_parcela_dia	=	data_parcela[2];
				var vencimento_parcela_mes	=	data_parcela[1];
				var vencimento_parcela_ano	=	data_parcela[0];
				
				var vencimento_parcela		=	vencimento_parcela_dia + vencimento_parcela_mes + vencimento_parcela_ano;
				
				if(tipoPagamento_Valor == 2 && vencimento_parcela > data_atual){ //verifica se ela foi paga com cart�o de cr�dito.
					pedido_total = pedido_total - parcelaValor; //desconta o valor da parcela no total.
				}
			}*/
			//final
			/*************************************************************************************************************************************************************************************************************************************************************************************/
			retorno = true;
			jQuery.ajax({  
				type: "GET",  
				url: urlDestino,  
				async : false,  
				data : {totalPedido:0, cliente_id:cliente_id}, //deixei totalpedido como 0 pois agora nao verifica a partir das parcelas 
				success : function (dados) {  
					
					var valorExcedido	= $(dados).find("valorExcedido").text();;
					var limiteCredito	= $(dados).find("limiteCredito").text();
					var excedido		= $(dados).find("excedido").text();
					var resultado 		= valorExcedido - limiteCredito;
					
					if(excedido == 1){
						alert("O limite de cr�dito para este cliente foi excedido em R$ " + valorExcedido);
						retorno = false;
					}
				}  
			});
		}	
		if(retorno == false)return false;
	}
	
	/*----------------------------------------------------------------------------------------------*/
	//CALCULAR O TOTAL
	function totalCalcular(){
		
		valorTotalProdutos 			= br2float($('#txt_pedido_produtos').val());
		valorTotalPedido 			= br2float($('#txt_pedido_subtotal').val());
		valorDesconto_moeda	 		= br2float($('#txt_pedido_desconto_reais').val());
		valorDesconto_percentual	= br2float($('#txt_pedido_desconto').val());		
		
		if($("ul#pedido_modo_aba").length > 0){
			valorFuncionario1			= br2float($('#txt_funcionario1_valor').val());
			valorFuncionario2			= br2float($('#txt_funcionario2_valor').val());		
			valorFuncionario3			= br2float($('#txt_funcionario3_valor').val());	
			valorTotalServicos	 		= valorFuncionario1 + valorFuncionario2 + valorFuncionario3;
		}else{
			valorTotalServicos 		= Number(ifNumberNull(br2float($('#txt_pedido_servicos').val())));
		}
		
		valorServico_terceiro		= br2float($('#txt_pedido_terceiros').val());
		
		//VERIFICA SE NAO ESTA NA VENDA POR COMANDA, ONDE TEM COMISSAO DO GARCOM
		if($('#txt_pedido_comissao').length > 0){
			var comissao 			= $('#hid_funcionario_comissao').val();
			var valComissao 	 	= valorTotalProdutos * comissao /100;
			var valorTotalPedido 	= valorTotalProdutos + valComissao;
			$('#txt_pedido_comissao').val((float2br((valComissao).toFixed(venda_decimais))));
		}
		
		
		valorSubTotalPedido 		= valorTotalProdutos + valorTotalServicos + valorServico_terceiro;
		valorDesconto_percentual	= valorSubTotalPedido * valorDesconto_percentual /100;
		valorDesconto 				= valorDesconto_moeda + valorDesconto_percentual;
		valorTotalPedido 			= valorSubTotalPedido - valorDesconto;

		
		
		//FORMATAR COM CASAS DECIMAIS
		$('#txt_pedido_produtos').val(float2br((valorTotalProdutos).toFixed(venda_decimais)));
		$('#txt_pedido_servicos').val(float2br((valorTotalServicos).toFixed(venda_decimais)));
		$('#txt_pedido_subtotal').val(float2br((valorSubTotalPedido).toFixed(venda_decimais)));
		$('#txt_pedido_total').val	((float2br((valorTotalPedido).toFixed(venda_decimais))));
		
		if($("ul#pedido_modo_aba").length > 0){
			$('#txt_funcionario1_valor').val(float2br(valorFuncionario1.toFixed(venda_decimais)));
			$('#txt_funcionario2_valor').val(float2br(valorFuncionario2.toFixed(venda_decimais)));
			$('#txt_funcionario3_valor').val(float2br(valorFuncionario3.toFixed(venda_decimais)));
		}
		$('#txt_pedido_terceiros').val	(float2br(valorServico_terceiro.toFixed(venda_decimais)));
		
		$('#txt_pedido_desconto').val(float2br(br2float($('#txt_pedido_desconto').val()).toFixed(venda_decimais)));
		$('#txt_pedido_desconto_reais').val(float2br(br2float($('#txt_pedido_desconto_reais').val()).toFixed(venda_decimais)));
		
	};
	
	/* CALCULAR TOTAL EM EDICAO DE VALOR DO ITEM */ 
	function totalCalcularEdicao(){
		var total_itens = 0;
		$('.txt_item_subtotal').each(function(){
			valor_item =  br2float(float2br($(this).val()));
			total_itens +=  parseFloat(valor_item);
		});
		
		$('#txt_pedido_produtos').val(float2br((total_itens)));
		totalCalcular();
	}
	
	
	//####################################################################################################################################################################################################################################################################################
	
	function totalCalcularVendaRapida(){
		
		valorTotalPedido 			= br2float($('#txt_pedido_subtotal').val());
		valorDesconto_moeda	 		= br2float($('#txt_pedido_desconto_reais').val());
		valorDesconto_percentual	= br2float($('#txt_pedido_desconto').val());
		
		//VERIFICA SE NAO ESTA NA VENDA POR COMANDA, ONDE TEM COMISSAO DO GARCOM
		if($('#txt_pedido_comissao').length > 0){
			
			var comissao = $('#hid_funcionario_comissao').val();
			
			var valComissao 	 	= valorTotalPedido * comissao /100;
			var valorTotalPedido 	= valorTotalPedido + valComissao;
			
			$('#txt_pedido_comissao').val((float2br((valComissao).toFixed(venda_decimais))));
		}
		
		valorDesconto_percentual	= valorTotalPedido * valorDesconto_percentual /100;
		valorDesconto 				= valorDesconto_moeda + valorDesconto_percentual;
		$('#txt_pedido_total').val((float2br((valorTotalPedido - valorDesconto).toFixed(venda_decimais))));
							 
		//FORMATAR COM CASAS DECIMAIS
		$('#txt_pedido_desconto').val(float2br(br2float($('#txt_pedido_desconto').val()).toFixed(venda_decimais)));
		$('#txt_pedido_desconto_reais').val(float2br(br2float($('#txt_pedido_desconto_reais').val()).toFixed(venda_decimais)));
		
	};
	
	//####################################################################################################################################################################################################################################################################################
	
	function PadDigits(n){
		n = n.toString();
		var pd = '';
		if (2 > n.length) { 
			for (i=0; i < (2 - n.length); i++) { 
				pd += '0'; 
			 } 
		} 
		 return pd + n.toString();
	 } 
	
	
//PEDIDO
$(document).ready(function(){
	
	$('.txt_item_valor, .txt_item_quantidade').blur(function(){
		var nome = $(this).attr('name');
		var nome = nome.split('_');

		quantidade 	= br2float(float2br($('[name=txt_item_quantidade_'+nome[3]+']').val()));
		desconto	= br2float(float2br($('[name=txt_item_desconto_'+nome[3]+']').val()));
		valor		= br2float(float2br($('[name=txt_item_valor_'+nome[3]+']').val()));
		
		subtotal		= valor * quantidade;
		valor_desconto 	=  subtotal * desconto /100;
		total_item		= subtotal - valor_desconto;
		
		$('[name=txt_item_subtotal_'+nome[3]+']').val(float2br((total_item).toFixed(venda_decimais)));
		$('.txt_item_valor').val(float2br(br2float($(this).val()).toFixed(venda_decimais)));
		$('.txt_item_quantidade').val(float2br(br2float($(this).val()).toFixed(qtd_decimais)));
		totalCalcularEdicao();
	});
	
	//APAGA O QUE ESTA ESCRITO A GANHAR FOCO
	textboxes = $("input[type=text], input[type=submit], input[type=password], select, textarea, checkbox, button");
	$(textboxes).focus(function(){
		$(this).select(); 
	});					
	/*----------------------------------------------------------------------------------------------*/
	//BUSCAR DADOS DO PRODUTO
	$('form[id^=frm_pedido] input#txt_produto_codigo').blur(function(){
	//$('#txt_produto_codigo').blur(function(){
		codigo 			= trim($(this).attr('value'));
		cliente_id 		= $('#hid_cliente_id').val();
		estoque_id 		= $('#sel_produto_estoque').val();
		vendaStatus		= $('#sel_status').find('option').filter(':selected').text();
		
		$('#txt_produto_valor').attr({"readonly":""}); //pro caso de estar travado, pela tabela de preco
		
		if($('.txt_codigo_pedido').val() != 'novo'){pedido_id = $('.txt_codigo_pedido').val() }else{pedido_id = 0}
		if(codigo != ''){
		
			$.get(urlDestino, {busca_produto:codigo, cliente_id:cliente_id, estoque_id:estoque_id}, function(theXML){
				$('dados',theXML).each(function(){
					var error			= $(this).find("error").text();
					if(error){
						
						$('#txt_produto_codigo').focus();
					}
					else{
						var produto_id 		= $(this).find("produto_id").text();
						var produto 		= $(this).find("produto").text();
						var prod_cli_codigo = $(this).find("produto_codigo_cliente").text();
						var tabela_sigla 	= $(this).find("tabela_sigla").text();
						var tabela_preco 	= $(this).find("tabela_preco").text();
						var valor 			= $(this).find("valor").text();
						var desconto 		= $(this).find("desconto").text();
						var valor_compra 	= $(this).find("valor_compra").text();
						var UnidadeMedida 	= $(this).find("unidade_medida").text();
						var estoque 		= Number($(this).find("estoque").text());
						var estoqueMinimo 	= Number($(this).find("estoqueMinimo").text());
						var estoqueControle = parseInt($(this).find("estoqueControle").text());
						var quantidade 		= $(this).find("quantidade").text();
						var comissao_valor  = $(this).find("comissao_valor").text();
						var comissao_tipo 	= $(this).find("comissao_valor_tipo").text();
						
						//preco por cliente
						var cliente_valor	= $(this).find("cliente_valor").text();
						if(cliente_valor == 0){
							alert('Este cliente n�o possui valor cadastrado para esse produto.');
						}
						/////////////////////////////////////////////////////////////
						
						//exibe o valor
						$('#hid_produto_id').val(produto_id);
						$('#txt_produto_nome').val(unescape(produto));
						$('#txt_produto_valor').val(valor);
						$('#sel_produto_tabela').val(tabela_preco);
						$('#hid_tabela_sigla').val(tabela_sigla);
						$('#hid_produto_UM').val(UnidadeMedida);
						$('#txt_produto_desconto').val(float2br(desconto));
						$('#txt_produto_valor_compra').val(float2br(valor_compra));
						$('#txt_produto_codigo_cliente').val(prod_cli_codigo);

						/* para nao mexer mais na estrutura do temp_comissoes e pedido_funcionario_servico
						este js faz a verificacao se � % ou $ e adiciona em seus respectivos campos */

						if(comissao_tipo == '1'){
							$('#hid_comissao_valor_reais').val(comissao_valor);
							$('#hid_comissao_valor_porcentagem').val('');
						}else{
							$('#hid_comissao_valor_porcentagem').val(comissao_valor);
							$('#hid_comissao_valor_reais').val('');
						}


						$('#hid_estoque_limite').val(estoque);
						$('#hid_estoque_controle').val(estoqueControle); // se vai controlar estoque desse produto ou n�o
						
						if(($('#txt_produto_quantidade').val()) == ''){
							if(($('#txt_produto_codigo').attr('value')) != ''){
								$('#txt_produto_quantidade').val(quantidade);
							}
						}
						
						if(vendaStatus != 'Or�amento' && vendaStatus != 'Em andamento' || estoqueControle == 3){
							
							// CONTROLE DE ESTOQUE #####################################
							if(estoqueControle != 2){
								
								calculaEstoque(produto_id, estoque_id, quantidade, pedido_id);
								if(estoqueControle == 1){//se nao for o caso de liberar estoque negativo, ele da os alertas
									
									if(estoqueMinimo == estoqueSaldo && estoqueMinimo != 0){
										alert("ATEN��O! Estoque m�nimo alcan�ado!");
									}else if(estoqueMinimo > estoqueSaldo){
										alert("ATEN��O! Estoque m�nimo excedido.");
									}
								}
							}
						}
						
						//Fardos #############################################################################################
						$("select[name=sel_produto_fardo]").html('<option value="0">Carregando...</option>');
						$.post(urlDestino,{sel_produto_fardo:produto_id},function(valor){
							$("select[name=sel_produto_fardo]").html(valor);
						});
						
						
						$("select[name=sel_produto_fardo]").change(function(){
							$.post(urlDestino, {sel_produto_fardo_qtd:$(this).val()}, function(theXML){
								$('dados',theXML).each(function(){
									var quantidade = float2br(parseFloat($(this).find("quantidade").text()).toFixed(qtd_decimais));
								
									$('#txt_produto_quantidade').val(quantidade);
								});
							});
						});
						
						//###################################################################################################
						if(produto !=''){
							$("#txt_produto_nome").attr({
								"readonly":""
							});
									
							//VERIFICA SE � NA VENDA RAPIDA
							if(($('form:first').attr('id') == 'frm_pedido_rapido_novo') && $('#txt_produto_codigo').val != ''){
								
								//setTimeout(function(){$('#btn_item_inserir').click()}, 1000);
								//verificar comportamento de inser��o de produto
								var parametro = $('#sys_venda_rapida_produto_inserir_automatico').val();
								//alert(parametro);
								if(parametro==1){
									$('#btn_item_inserir').click();
								}
								//stop();
							}
						}
					}
				});
			});
		}
	});
	/*----------------------------------------------------------------------------------------------*/
	//AO TROCAR A TABELA DE PRE�OS, CARREGAR O VALOR QUE ESTA CADASTRADO NELA
	 $("select[name=sel_produto_tabela]").change(function(){
		$('#txt_produto_valor').attr({"readonly":""});
		
		var id 			= $(this).attr('value');		
		var produto_id 	= $('#hid_produto_id').attr('value');
		var cliente_id 	= $('#hid_cliente_id').attr('value');
		
		$.get(urlDestino, {sel_pedido_tabela_preco:id, produto_id:produto_id, cliente_id:cliente_id}, function(theXML){
			$('dados',theXML).each(function(){

				var valor 		 = float2br(parseFloat($(this).find("valor").text()).toFixed(venda_decimais));
				var tabela_sigla = $(this).find("tabela_sigla").text();
				
				if(isNaN(br2float(valor))){
					valor = '0,00';
					$('#txt_produto_valor').attr("readonly", 'readonly');
				}
				//exibe o valor
				$('#txt_produto_valor').val(valor);
				$('#hid_tabela_sigla').val(tabela_sigla);
				
			});
		});
	})
	
	 /*----------------------------------------------------------------------------------------------*/
	//AO TROCAR ESTOQUE
	 $("select[name=sel_produto_estoque]").change(function(){
		var id = $(this).attr('value');		
		var produtoId = $('#hid_produto_id').attr('value');		
		
		$.get(urlDestino, {produto_estoque_id:id, produtoId:produtoId}, function(theXML){
			$('dados',theXML).each(function(){
				var estoqueLimite = $(this).find("estoque").text();
				$('#hid_estoque_limite').val(estoqueLimite);
			});
		});
		
		$('#txt_produto_valor').focus();
	})
	
	/*----------------------------------------------------------------------------------------------*/
	//INSERIR PRODUTO NO PEDIDO
	if(fnc_get("p") != 'pedido_nfe_novo' && fnc_get("p") != 'pedido_nfe_detalhe'  && fnc_get("p") != 'pedido_nfe_importar'){
		$('#btn_item_inserir').click(function(event){
			event.preventDefault();
			//VERIFICA SE JA EXISTE MESMO ITEM INSERIDO ANTERIOR ############################################################################################################
			item_repedito_alerta 	= $('#hid_item_repetido_alerta').val();
			var produto_codigo 		= $('#txt_produto_codigo').attr('value');
			if(item_repedito_alerta == 1 && produto_codigo != ''){
				
				var produto_descricao 	= $('#txt_produto_nome').attr('value');
							
				n = 0;
				$('input[name^="txt_item_codigo"]').each(function(){
					item_codigo		= $(this).val();
					item_descricao 	= $(this).parents("#pedido_lista_item").find("input[name^='txt_item_nome']").val();
					
					if(item_codigo == produto_codigo && item_descricao == produto_descricao){
						if(confirm('Item j� inserido em Linha '+n+'. Deseja inserir novamente? ') == false ){
							$('#txt_produto_codigo').select();
							$('#btn_item_inserir').die(click);
						}
					}
					
					n++;
				});
			}
			//#############################################################################################################################################################
			
			if($('.txt_codigo_pedido').val() != 'novo'){pedido_id = $('.txt_codigo_pedido').val() }else{pedido_id = 0}
			var vendaDecimal 		= $("#hid_venda_decimal").val();
			var quantidadeDecimal 	= $("#hid_quantidade_decimal").val();
			
			//controle de estoque //########################################################################################################################################
			var quantidade 		= br2float($('#txt_produto_quantidade').attr('value'));
			var produto_id 		= $('#hid_produto_id').attr('value');
			
			var estoqueControle = $('#hid_estoque_controle').attr('value'); // verifica se � pra controlar estoque ou nao deste produto
			var estoqueNegativo = $('#hid_estoque_negativo_alerta').attr('value'); // verifica se � pra alertar estoque negativo
			var estoqueId 		= $('#sel_produto_estoque').attr('value'); 
			//se o produto estiver configurado para controlar estoque ###################################################################################################
			if(estoqueControle != 2){
				// fim da verificacao ###################################################################################################################################
				if(calculaEstoque(produto_id, estoqueId, quantidade, pedido_id) == 'false'){
					if(vendaStatus != 'Or�amento' && vendaStatus != 'Em andamento' && estoqueControle == 1){
						alert("N�o h� estoque suficiente!");
						return false;
					}else if(estoqueControle == 3){
						if(estoqueNegativo == 1){
							alert("Item inserido como estoque negativo!");
						}
					}
				}				
			}
	
			//end if(estoqueControle == 1)###############################################################################################################################
			if($("#txt_produto_nome").val()!="" && $("#txt_produto_codigo").val()!=""){
				
				//o parametro true em clone faz com que seja copiado tamb�m os eventos do elemento
				$('#pedido_lista_item:first').clone(true).appendTo('#pedido_lista');
				$('#pedido_lista_item:first').clone(true).appendTo('#table_pedido_rapido');
				
				//LIMPAR CURSOR DE TODOS OS ITENS
				$('tr[title=pedido_lista_item]').removeClass('cursor');
				//MARCAR CURSOR DO ITEM ATUAL
				$('tr[title=pedido_lista_item]:last').addClass('cursor');
				
				//EXIBIR LINHA QUE ORIGINALMENTE � DISPLAY:NONE
				$('tr[title=pedido_lista_item]:last').css('display','table-row');
				
				//COPIAR DADOS NO NOVO ITEM
				if(vendaStatus == "Entregue"){
					$('.chk_entregue:last').attr('checked', 'checked');
					$('.chk_entregue:last').attr('onClick', 'return false');
				}else if(vendaStatus == "Or�amento"){
					$('.chk_entregue:last').attr('onClick', 'return false');
				}
				
				$('.hid_item_produto_id:last').val	($('#hid_produto_id').val());
				$('.txt_item_codigo:last').val		($('#txt_produto_codigo').val());
				$('.txt_item_nome:last').val		($('#txt_produto_nome').val());
				$('.hid_item_pedido_id:last').val	($('#hid_item_pedido_id').val());
				
				if($('#sel_produto_fardo').val() > 0){
					$('.txt_item_fardo:last').val($('#sel_produto_fardo').find('option').filter(':selected').text());
				}
			
				$('.hid_item_fardo:last').val		($('#sel_produto_fardo').val());
				$('.txt_item_tabela_sigla:last').val($('#sel_produto_tabela').find('option').filter(':selected').text());
				$('.hid_item_tabela_preco:last').val($('#sel_produto_tabela').val());
				
				$('.txt_item_valor:last').val			(float2br(br2float($('#txt_produto_valor').val()).toFixed(vendaDecimal)));
				$('.txt_item_quantidade:last').val		(float2br(br2float($('#txt_produto_quantidade').val()).toFixed(quantidadeDecimal)));
				$('.txt_item_UM:last').val				($('#hid_produto_UM').val());
				$('.txt_item_desconto:last').val		(float2br(br2float($('#txt_produto_desconto').val()).toFixed(venda_decimais)));
				$('.txt_item_estoque_nome:last').val	($('#sel_produto_estoque').find('option').filter(':selected').text());
				$('.hid_item_estoque_id:last').val		($('#sel_produto_estoque').val());
				$('.txt_item_referencia:last').val		($('#txt_produto_referencia').val());
				$('.txt_item_lote:last').val			($('#txt_produto_lote').val());
				$('.txt_item_codigo_cliente:last').val	($('#txt_produto_codigo_cliente').val());
				
				
				//CALCULAR SUBTOTAL DO ITEM
				var desconto 	=  br2float($('#txt_produto_desconto').val());
				var fltDesconto =  br2float($('#txt_produto_valor').val()) * desconto / 100;
				var fltSubtotal = (br2float($('#txt_produto_valor').val()) - fltDesconto) * br2float(float2br($('#txt_produto_quantidade').val()));
				
				$('.txt_item_subtotal:last').val(float2br(fltSubtotal.toFixed(vendaDecimal)));

				//SOMAR AO SUBTOTAL GERAL
				if($('#txt_pedido_produtos').length > 0){
					var fltSubtotalAtual = br2float($('#txt_pedido_produtos').val());
					$('#txt_pedido_produtos').val(float2br((fltSubtotalAtual + fltSubtotal).toFixed(venda_decimais)));
				}else{
					var fltSubtotalAtual = br2float($('#txt_pedido_subtotal').val());
					$('#txt_pedido_subtotal').val(float2br((fltSubtotalAtual + fltSubtotal).toFixed(venda_decimais)));
				}
				/*----------------------------------------------------------------*/
				//NUMERAR CONTROLE ATUAL
				$('#hid_controle').val(parseInt($('#hid_controle').val()) + 1);
				$('#hid_cursor').val(parseInt($('#hid_cursor').val()) + 1);
				
				$('.chk_entregue:last').attr(				{name: $('.chk_entregue:last').attr('class') + "_" + $('#hid_controle').val(),				id: $('.chk_entregue:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.hid_item_pedido_id:last').attr(			{name: $('.hid_item_pedido_id:last').attr('class') + "_" + $('#hid_controle').val(),		id: $('.hid_item_pedido_id:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.hid_item_produto_id:last').attr(		{name: $('.hid_item_produto_id:last').attr('class') + "_" + $('#hid_controle').val(),		id: $('.hid_item_produto_id:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_codigo:last').attr(			{name: $('.txt_item_codigo:last').attr('class') + "_" + $('#hid_controle').val(),			id: $('.txt_item_codigo:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_nome:last').attr(				{name: $('.txt_item_nome:last').attr('class') + "_" + $('#hid_controle').val(),				id: $('.txt_item_nome:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_fardo:last').attr(				{name: $('.txt_item_fardo:last').attr('class') + "_" + $('#hid_controle').val(),			id: $('.txt_item_fardo:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.hid_item_fardo:last').attr(				{name: $('.hid_item_fardo:last').attr('class') + "_" + $('#hid_controle').val(),			id: $('.hid_item_fardo:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_tabela_sigla:last').attr(		{name: $('.txt_item_tabela_sigla:last').attr('class') + "_" + $('#hid_controle').val(),		id: $('.txt_item_tabela_sigla:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.hid_item_tabela_preco:last').attr(		{name: $('.hid_item_tabela_preco:last').attr('class') + "_" + $('#hid_controle').val(),		id: $('.hid_item_tabela_preco:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_valor:last').attr(				{name: $('.txt_item_valor:last').attr('class') + "_" + $('#hid_controle').val(),			id: $('.txt_item_valor:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_quantidade:last').attr(		{name: $('.txt_item_quantidade:last').attr('class') + "_" + $('#hid_controle').val(),		id: $('.txt_item_quantidade:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_estoque_nome:last').attr(		{name: $('.txt_item_estoque_nome:last').attr('class') + "_" + $('#hid_controle').val(),		id: $('.txt_item_estoque_nome:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_UM:last').attr(				{name: $('.txt_item_UM:last').attr('class') + "_" + $('#hid_controle').val(),				id: $('.txt_item_UM:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_subtotal:last').attr(			{name: $('.txt_item_subtotal:last').attr('class') + "_" + $('#hid_controle').val(),			id: $('.txt_item_subtotal:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_desconto:last').attr(			{name: $('.txt_item_desconto:last').attr('class') + "_" + $('#hid_controle').val(),			id: $('.txt_item_desconto:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.hid_item_estoque_id:last').attr(		{name: $('.hid_item_estoque_id:last').attr('class') + "_" + $('#hid_controle').val(),		id: $('.hid_item_estoque_id:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_referencia:last').attr(		{name: $('.txt_item_referencia:last').attr('class') + "_" + $('#hid_controle').val(),		id: $('.txt_item_referencia:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_lote:last').attr(			{name: $('.txt_item_lote:last').attr('class') + "_" + $('#hid_controle').val(),				id: $('.txt_item_lote:last').attr('class') + "_" + $('#hid_controle').val()});
				$('.txt_item_codigo_cliente:last').attr(	{name: $('.txt_item_codigo_cliente:last').attr('class') + "_" + $('#hid_controle').val(),	id: $('.txt_item_codigo_cliente:last').attr('class') + "_" + $('#hid_controle').val()});
			
				//LIMPAR CAMPOS PARA LANCAR ITEM
				$('#txt_produto_codigo').val("");
				$('#txt_produto_nome').val("");
				$('#txt_produto_valor').val("");
				$('#txt_produto_quantidade').val("");
				$('#hid_produto_UM').val("");
				$('#txt_produto_desconto').val("");
				//$('#txt_item_subtotal').val("");
				$('#sel_produto_tabela').val("1");
				$('#sel_produto_estoque').val("1");
				$('#txt_produto_referencia').val("");
				$('#txt_produto_lote').val("");
				$('#txt_produto_codigo_cliente').val("");
			
				if($('#modal_otica').length > 0){
					winModal($('#modal_otica'));
				}
				
				
				
				
				if(fnc_get("modo") == 'rapido_novo' || fnc_get("modo") == 'rapido_detalhe' ){
					//RETORNAR FOCO PARA LANCAR NOVO ITEM
					//definir se come�a na quantidade ou no c�digo do produto em venda r�pida
					var parametro = $('#sys_venda_rapida_primeiro_campo').val();
					$('#'+parametro).focus();
					/*
					var parametro = $('#sys_venda_rapida_quantidade_parar').val();
					if(parametro==1){
						$('#txt_produto_quantidade').focus();
					}
					else{
						$('#txt_produto_codigo').focus();
					}
					*/
					totalCalcularVendaRapida();
					
				}else{
					//RETORNAR FOCO PARA LANCAR NOVO ITEM
					$('#txt_produto_codigo').focus();
					totalCalcular();
					
				}
			}
		});
	
		//EXCLUIR ITEM
		$('.a_excluir').click(function(event){
			event.preventDefault();
				
				//subtrai do total o valor do item a excluir
				var fltTotal		= br2float($('#txt_pedido_subtotal').val());
				var fltProdutoTotal	= br2float($('#txt_pedido_produtos').val());
				var fltSubTotal 	= br2float($(this).parents("#pedido_lista_item").find(".txt_item_subtotal").val());
				
				//TUDO ISSO pra verificar estoque que ja foi lan�ado �� //#################################################################################
				var fltProdutoId	= $(this).parents("#pedido_lista_item").find(".hid_item_produto_id").val();
				var fltItemQtd 		= parseInt(br2float($(this).parents("#pedido_lista_item").find(".txt_item_quantidade").val()));
				var detalheControle = $(this).parents("#pedido_lista_item").find(".hid_item_detalhe").val(); // pra verificar se item ja estava na venda
				
				var estoqueId 		= $(this).parents("#pedido_lista_item").find(".hid_item_estoque_id").val(); 
				var controle 		= $('#hid_calculo_estoque_controle').val();
				if(detalheControle == 1){
					n = 0;
					var x = 0;
					
					while(n <= controle){
						var hidProdutoId = $('input[name=hid_controle_item_produto_id_'+n+']').attr('value'); //qual id do item que esta listado no momento
						var hidItemQtd = parseInt($('input[name=hid_controle_item_estoque_'+n+']').attr('value'));//quantidade
						var hidEstoqueId = parseInt($('input[name=hid_controle_item_estoque_id_'+n+']').attr('value'));//quantidade
						
						if(hidProdutoId == fltProdutoId && hidEstoqueId == estoqueId){
							calculoEstoque = hidItemQtd + fltItemQtd;
							$('input[name=hid_controle_item_estoque_'+n+']').val(calculoEstoque);					
							var x = 1;
						}
						n++;
					}
					if(x == 0){
						$('.item_estoque:first').clone(true).appendTo('#hid_item');
						
						$('.hid_controle_item_produto_id:last').val(fltProdutoId);
						$('.hid_controle_item_estoque:last').val(fltItemQtd);
						$('.hid_controle_item_estoque_id:last').val(estoqueId);
						
						var n = parseInt(controle) + 1;
						$('.hid_controle_item_estoque:last').attr('name', $('#hid_controle_item_estoque:last').attr('class') + "_" + n);
						$('.hid_controle_item_estoque_id:last').attr('name', $('#hid_controle_item_estoque_id:last').attr('class') + "_" + n);
						$('#hid_calculo_estoque_controle').val(n);
					}
				}
				//###################################################################################################################################
				$('#txt_pedido_produtos').val(float2br((fltProdutoTotal - fltSubTotal).toFixed(venda_decimais)));
					
				//remove o parente identificado pela class
				$(this).parents("#pedido_lista_item").remove();		
				totalCalcular();
		});
		
	}// se nao for nfe
	
	//ALTERAR STATUS DA VENDA HABILITAR/DESABILITAR CHECKS DE ENTREGUE
	$('#sel_status').change(function(){
		if($(this).find('option').filter(':selected').text() == 'Entregue'){
			$('.chk_entregue').attr('checked', 'checked');
			$('.chk_entregue').attr('onClick', 'return false');
		}else if($(this).find('option').filter(':selected').text() == 'Or�amento'){						 
			$('.chk_entregue').attr('checked', '');
			$('.chk_entregue').attr('onClick', 'return false');
		}else{
			$('.chk_entregue').removeAttr('onClick');
		}
	});
	
	/*----------------------------------------------------------------------------------------------*/	
	//ZERANDO CAMPOS NULL
	$('#txt_produto_valor, #txt_produto_quantidade, #txt_produto_desconto').change(function(){
		if($(this).val() == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0');
		}
	});
	
	$('#frm_pedido_novo #txt_pedido_desconto_reais').change(function(){
		//LIMPAR O OUTRO CAMPO DE DESCONTO
		$('#txt_pedido_desconto').val('0');
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		totalCalcular();
	});
	
	$('#frm_pedido_novo #txt_pedido_desconto').change(function(){
		//LIMPAR O OUTRO CAMPO DE DESCONTO
		$('#txt_pedido_desconto_reais').val('0');
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		totalCalcular();
	});

	//PARCELANDO O PEDIDO
	//validando o campo de entrada e parcela para poder apenas aceitar n�meros inteiros e positivos
	/*$('form input#txt_entrada').live('change', function(){
		$(this).attr('value', Math.abs(Math.round($(this).val())));
		
		if($(this).val() == 'undefined' || isNaN($(this).val())) {
			$(this).attr('value', 1);
			alert('Digite apenas n�meros inteiros!');
		}else if($(this).val() < 0 || $(this).val() > 1) {
			$(this).attr('value', 1);
		}
		
		//controlando a data do 1� vencimento
		var objData   = new Date();
		var dataAtual = objData.getDate() + '/' + ((objData.getMonth() + 1)) + '/' + objData.getFullYear();
		
		if($(this).val() == 1){
			$('form input#txt_1_vencimento').val(formatarData(dataAtual));
		}else {
			$('form input#txt_1_vencimento').val(incrementarData(dataAtual));
		}
	});
	
	$('form input#txt_parcela').live('change', function() {
		$(this).attr('value', Math.abs(Math.round($(this).val())));
		
		if($(this).val() == 'undefined' || isNaN($(this).val())) {
			$(this).attr('value', 0);
			$('form input#txt_parcela').focus();
			alert('Digite apenas n�meros inteiros!');
		}
	});*/
	
	//calculando o desconto para o item do pedido
	$('form input.txt_item_desconto').change(function() {
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		if(br2float($(this).val()) > 100){
			$(this).attr('value', '0,00');
			alert('Porcentagem de desconto inv�lida!');
		}
		
		var valorAtualPedido = parseFloat($('#txt_pedido_total').val());
		var qtdItens 		 = parseInt($(this).parents("#pedido_lista_item").find('.txt_item_quantidade').val());
		var desconto 		 = 0.0;
		var total			 = parseFloat($(this).parents("#pedido_lista_item").find(".txt_item_valor_compra").val()) * qtdItens;
		
		desconto			 = (parseFloat($(this).val()) * total) / 100;
		total				-= desconto;
		
		$(this).parents("#pedido_lista_item").find(".txt_item_subtotal").attr('value',float2br(total.toFixed(venda_decimais)));
		var pedidoTotal = 0.0;
		$('.txt_item_subtotal').each(function(){
			if(isNaN(pedidoTotal)){
				pedidoTotal = parseFloat($(this).val());
			}else{
				pedidoTotal += parseFloat($(this).val());
			}
		});
		
		$('#txt_pedido_total').val(float2br((pedidoTotal).toFixed(venda_decimais)));
	});
	
	//mudando o valor dos campos select das parcelas
	$('form select.sel_pagamento_tipo').live('change', function(){
		var x = 0;
		var totalParcelas	= $('ul.parcela_detalhe').length;
		var numeroAtual 	= $(this).attr('name').split('_');
		var tipoPagamento 	= $(this).val();
		
		for(x = numeroAtual[3]; x < totalParcelas; x++) {
			$('form select#sel_pagamento_tipo_' + x).attr('value', tipoPagamento);
		}
	});
	
	//----------------- EDITANDO E RECALCULANDO PARCELA ----------------//
	//gravando o valor inicial do campo ao receber o foco
	/*$valorParcelaInicial = 0.00;
	
	$('form input.txt_parcela_valor').live('focus', function(){
		$valorParcelaInicial = br2float($(this).val());
	});
	
	//alterando o valor da parcela e calculando as seguintes
	$('form input.txt_parcela_valor').live('change', function(){
		
		//vars
		var x 						= 1;
		var valorAcumulado 			= 0.00;
		var diferencaParcelaAtual	= 0.00;
		var valorRecalculado		= 0.00;
		var valorRestante			= 0.00;
		var valorParcelaAcumulado	= 0.00;
		var valorTotal				= br2float($('input#txt_pedido_total').val()); //valor total do pedido/compra
		var totalParcelas			= parseInt($('ul.parcela_detalhe input.txt_parcela_numero:last').val()); //n�mero de parcelas, o valor do campo parcelas da �ltima parcela
		
		//inicia-se com 0
		var nParcelaAtual 			= $(this).attr('name').split('_');
			nParcelaAtual			= parseInt(nParcelaAtual[3]);
		var nParcelasRestantes		= 0;
		var valorParcelaAtual 		= br2float($(this).val());
		var valorParcelaFinal		= 0.00;
		
		//formatando o valor atual
		$(this).val(float2br(valorParcelaAtual.toFixed(decimais)));
		
		//checando se � um valor v�lido
		if(isNaN(valorParcelaAtual) || valorParcelaAtual <= 0 || valorParcelaAtual > valorTotal) {
			alert('O valor inserido na parcela n� ' + (nParcelaAtual) + ' n�o � v�lido!');
			$(this).val(float2br($valorParcelaInicial.toFixed(decimais)));
			return false;
		}
		
		//acumulando o valor das parcelas restantes
		for(x = nParcelaAtual + 1; x <= totalParcelas; x++) {
			valorAcumulado		+= br2float($('form input#txt_parcela_valor_' + x).val());
			nParcelasRestantes	++;
		}
		
		//calculando as novas parcelas
		//para fazer corretamente a subtra��o
		if($valorParcelaInicial		> valorParcelaAtual) {
			diferencaParcelaAtual	= $valorParcelaInicial - valorParcelaAtual;
			valorRestante	 	  	= diferencaParcelaAtual + valorAcumulado;
			
		}else {
			diferencaParcelaAtual 	= valorParcelaAtual - $valorParcelaInicial;
			valorRestante 			= valorAcumulado - diferencaParcelaAtual;
		}
		
		if(nParcelasRestantes > 0){
			valorRecalculado = valorRestante / nParcelasRestantes;	
		}
		
		//atualizando valores a partir da pr�xima parcela
		for(x = nParcelaAtual + 1; x <= totalParcelas; x++) {
			
			if(valorRecalculado < 0) { valorRecalculado = 0 }
			$('form input#txt_parcela_valor_' + x).val(float2br(valorRecalculado.toFixed(decimais)));
		}
		
		//passando novamente pelas parcelas para calcular os centavos
		for(x = 1; x <=totalParcelas; x++) {
			
			valorParcelaAcumulado += br2float($('form input#txt_parcela_valor_' + x + ':last').val());
			if(x == totalParcelas) {
				valorParcelaFinal = br2float($('form input#txt_parcela_valor_' + x).val()) + (valorTotal - valorParcelaAcumulado);
				$('form input#txt_parcela_valor_' + x).val(float2br(valorParcelaFinal.toFixed(decimais)));
			}
		}
		
		return true;
	});
	
	//zerando a vari�vel global para esse procedimento
	valorParcelaInicial = null;*/
/*----------------------------------------------------------------------------------------------*/

	/*$('form #btn_calcular').live('click', function(event){
		event.preventDefault();

		//capturando os dados do parcelamento
		var qtdEntrada 			= parseInt($('form input#txt_entrada').val());
		var qtdParcela 			= parseInt($('form input#txt_parcela').val());
		var qtdParcelasTotal 	= qtdEntrada + qtdParcela;
		
		//caso entrada e parcela sejam 0
		if(qtdParcelasTotal == 0) {
			alert('� necess�rio pelo menos uma entrada ou parcela!');
			$('form input#txt_parcela').focus();
			return false;
		}
		
		//capturando o valor do sub_total dos itens e dividindo pelo n�mero de parcelas + entrada
		var subTotal = br2float($('input#txt_pedido_total').val());
		
		//caso o subtotal seja nulo
		if(isNaN(subTotal)) {
			alert('N�o h� nenhum valor para ser calculado!');
			$('form input#txt_pedido_total').focus();
			return false;
		}
		
		//criando vari�veis respons�veis pelo c�lculo de centavos
		var valorParcelaAcumulado;
		var valorParcelaFinal;
		var valorParcelas = subTotal / (qtdParcelasTotal);
		
		/************************************************************************************/
			//removendo para poder atualizar caso mude o numero de parcelas ou a entrada
			/*if($('ul.parcela_detalhe').length > 1) {
				for(y = $('ul.parcela_detalhe').length; y > 1; y--) {
					$('ul.parcela_detalhe:last').remove();
				}
				$('input.txt_parcela_valor:first').attr('value', float2br(valorParcelas.toFixed(decimais)));
			}
			
			//instanciando o objeto Date, para poder trabalhar com as datas
			var objData 	= new Date();
			var dataAtual 	= objData.getDate() + '/' + (objData.getMonth() + 1) + '/' + objData.getFullYear(); //data atual | o '+1' � por causa do indice
			dataAtual 		= formatarData(dataAtual);
			var valorParcelaAcumulado = 0;
			
			//preenchendo o resto dar parcelas
			for(x = 1; x <= qtdParcelasTotal; x++) {
				
				//copiando o item da lista com inputs e colocando valores
				$('ul.parcela_detalhe:last').clone(true).appendTo('div#parcelas');
				//acumulando o valor das parcelas para calcular os centavos
				valorParcelaAcumulado += parseFloat(valorParcelas.toFixed(decimais));
				if(x == 1){
					//para inserir a primeira parcela, pois o campo n�o � gerado dinamicamente
					$('input.txt_parcela_numero:last').attr('value', 1);
					$('input.txt_parcela_valor:last').attr('value', float2br(valorParcelas.toFixed(decimais)));
					$('input.txt_parcela_data:last').attr('value', $('form input#txt_1_vencimento').val());
			
				}else{
					//calculando a �ltima parcela
					if(x == (qtdParcelasTotal)) {
						valorParcelaFinal = parseFloat(valorParcelas.toFixed(decimais)) + parseFloat((Number(subTotal) - Number(valorParcelaAcumulado)).toFixed(decimais));
					}
					
					$('input.txt_parcela_numero:last').attr('value', parseInt($('input.txt_parcela_numero:last').val()) + 1);
					$('input.txt_parcela_valor:last').attr('value', (x == (qtdParcelasTotal)) ? float2br(valorParcelaFinal.toFixed(decimais)) : float2br(valorParcelas.toFixed(decimais)));
					
					var diaInicial = $('input.txt_parcela_data:last').val().split('/'); //para gerar sempre para o mesmo dia, menos no caso de fevereiro
					var ultimaData = $('input.txt_parcela_data:last').val().split('/');
					$('input.txt_parcela_data:last').attr('value', incrementarData(diaInicial[0] + '/' + ultimaData[1] + '/' + ultimaData[2]));
				}
				
				//mudando o nome e id dos campos das parcelas
				$('input.txt_parcela_numero:last').attr('id', 'txt_parcela_numero_' + x);
				$('input.txt_parcela_numero:last').attr('name', 'txt_parcela_numero_' + x);
				$('input.txt_parcela_valor:last').attr('id', 'txt_parcela_valor_' + x);
				$('input.txt_parcela_valor:last').attr('name', 'txt_parcela_valor_' + x);
				$('input.txt_parcela_data:last').attr('id', 'txt_parcela_data_' + x);
				$('input.txt_parcela_data:last').attr('name', 'txt_parcela_data_' + x);
				$('select.sel_pagamento_tipo:last').attr('id', 'sel_pagamento_tipo_' + x);
				$('select.sel_pagamento_tipo:last').attr('name', 'sel_pagamento_tipo_' + x);
			}
			
			$('#hid_controle_parcela').val($('ul.parcela_detalhe').length);
		/************************************************************************************/
		/*return true;
	});*/
	
/*----------------------------------------------------------------------------------------------*/
	/*** Calculando as parcelas de uma venda/compra cujo o parcelamento seja um perfil est�tico ***/
	/*$('form #btn_calcular_pre_definido').live('click', function(e) {
		e.preventDefault();
		
		var select 		 = $('select#sel_perfil').val();
		var pares  		 = select.split('|');
		var parcelas 	 = parseInt(pares.length, 10);
		var dataInicial  = $('input#txt_data_inicial').val().split('/');
		var valores 	 = vencimentoCalculado = new Array();
		
		//calculando os vencimentos das parcelas
		for(i=0; i<parcelas; i++) {
			valores = pares[i].split(';');
			vencimentoCalculado[i] = calcularDatasFuturas(valores[0], valores[1], dataInicial);
		}
		
		//capturando o valor do sub_total dos itens e dividindo pelo n�mero de parcelas + entrada
		var subTotal = br2float($('input#txt_pedido_total').val());
		
		//caso o subtotal seja nulo
		if(isNaN(subTotal)) {
			alert('N�o h� nenhum valor para ser calculado!');
			$('form input#txt_pedido_total').focus();
			return false;
		}
		
		//criando vari�veis respons�veis pelo c�lculo de centavos
		var valorParcelaAcumulado;
		var valorParcelaFinal;
		var valorParcelas = subTotal / parcelas;
		
		//removendo para poder atualizar caso mude o numero de parcelas ou a entrada
		if($('ul.parcela_detalhe').length > 1) {
			for(y = $('ul.parcela_detalhe').length; y > 1; y--) {
				$('ul.parcela_detalhe:last').remove();
			}
			$('input.txt_parcela_valor:first').attr('value', float2br(valorParcelas.toFixed(decimais)));
		}
		
		var valorParcelaAcumulado = 0;

		//preenchendo o resto dar parcelas
		for(x = 1; x <= parcelas; x++) {
			
			//copiando o item da lista com inputs e colocando valores
			$('ul.parcela_detalhe:last').clone(true).appendTo('div#parcelas');
			//acumulando o valor das parcelas para calcular os centavos
			valorParcelaAcumulado += parseFloat(valorParcelas.toFixed(decimais));
			if(x == 1){
				//para inserir a primeira parcela, pois o campo n�o � gerado dinamicamente
				$('input.txt_parcela_numero:last').attr('value', 1);
				$('input.txt_parcela_valor:last').attr('value', float2br(valorParcelas.toFixed(decimais)));
				$('input.txt_parcela_data:last').attr('value', vencimentoCalculado[(x-1)]);
		
			}
			else {
				//calculando a �ltima parcela
				if(x == (parcelas)) {
					valorParcelaFinal = parseFloat(valorParcelas.toFixed(decimais)) + parseFloat((Number(subTotal) - Number(valorParcelaAcumulado)).toFixed(decimais));
				}
				
				$('input.txt_parcela_numero:last').attr('value', parseInt($('input.txt_parcela_numero:last').val()) + 1);
				$('input.txt_parcela_valor:last').attr('value', (x == (parcelas)) ? float2br(valorParcelaFinal.toFixed(decimais)) : float2br(valorParcelas.toFixed(decimais)));
				$('input.txt_parcela_data:last').attr('value', vencimentoCalculado[(x-1)]);
			}
			
			//mudando o nome e id dos campos das parcelas
			$('input.txt_parcela_numero:last').attr('id', 'txt_parcela_numero_' + x);
			$('input.txt_parcela_numero:last').attr('name', 'txt_parcela_numero_' + x);
			$('input.txt_parcela_valor:last').attr('id', 'txt_parcela_valor_' + x);
			$('input.txt_parcela_valor:last').attr('name', 'txt_parcela_valor_' + x);
			$('input.txt_parcela_data:last').attr('id', 'txt_parcela_data_' + x);
			$('input.txt_parcela_data:last').attr('name', 'txt_parcela_data_' + x);
			$('select.sel_pagamento_tipo:last').attr('id', 'sel_pagamento_tipo_' + x);
			$('select.sel_pagamento_tipo:last').attr('name', 'sel_pagamento_tipo_' + x);
		}
		
		$('#hid_controle_parcela').val($('ul.parcela_detalhe').length);
		return true;
			
	});*/
	
	/*----------------------------------------------------------------------------------------------*/
	$("form#frm_pedido_novo input#txt_cliente_codigo").blur(function(){
		setTimeout(function(){verificaLimiteCredito()}, 500); //faz a funcao de verificar limite de credito depois de algum tempinho, senao n�o d� tempo de fazer nada...
	});

	$("form#frm_pedido_novo input#btn_gravar").click(function(event){
		event.preventDefault();
		
		if(verificaEstoque() == false || checkfinalizarParcelas() == false || checkfinalizarParcelas() == false){
			alert('error');
			return false;
		}else{
			if($('#txt_cliente_codigo').val() == ''){
				alert("O cliente deve ser selecionado!");
				return false;
			}else{
				$('form#frm_pedido_novo').submit();
			}
		}
	});
	
	//compra
	$("form#frm_compra_novo input#btn_gravar").click(function(event){
		if(checkfinalizarParcelas() == false) { return false; }
	});
	
	//########################################################################################################################//
	//OP��O DE NOVO VEICULO NA VENDA => AUTOMOTIVO
	$("select[name=sel_veiculo]").change(function(){
		if($(this).val() == 'novo') {
			winModal($('#modal_veiculo'));
		}
	});
	
	//########################################################################################################################//
	//V�O TER DOIS CAMPOS DE DESCONTO, EM (%) E EM (R$)... ENT�O S�O ESSES DOIS CAMPOS QUE TEM QUE FUNCIONAR
	$('#txt_funcionario1_valor, #txt_funcionario2_valor, #txt_funcionario3_valor, #txt_pedido_terceiros').change(function(){
		$(this).val(float2br(br2float(ifNumberNull($(this).val())).toFixed(venda_decimais)));
		totalCalcular();
	});
	
	//CALCULA VALOR DE SERVICO, DE ACORDO COM PRE CADASTRO
	$("div#modo_aba_servico .txt_funcionario_tempo").change(function(){
																		   
		input_name 		= $(this).attr('name').split('txt_funcionario');
		input_name		= input_name[1].substring(0,1);
		servico_valor	= $('#hid_servico_valor').val();
		servico_tempo	= br2float(ifNumberNull($(this).val()));
		valor_total		= servico_tempo * servico_valor;
		
		$("div#modo_aba_servico #txt_funcionario"+input_name+"_valor").val(float2br(valor_total.toFixed(venda_decimais)));
		$(this).val(float2br(servico_tempo.toFixed(venda_decimais)));
		totalCalcular();
	});
	
	$("div#modo_aba_servico .sel_funcionario").change(function(){
		
		input_name = $(this).attr('name').split('sel_funcionario');
		if($(this).val() > 0){
			$('#txa_funcionario'+input_name[1]+'_servico').attr({'readonly':''});
			$('#txt_funcionario'+input_name[1]+'_tempo').attr({'readonly':''});
			$('#txt_funcionario'+input_name[1]+'_valor').attr({'readonly':''});
		}else{
			$('#txa_funcionario'+input_name[1]+'_servico').val('').attr({'readonly':'readonly'});
			$('#txt_funcionario'+input_name[1]+'_tempo').val('').attr({'readonly':'readonly'});
			$('#txt_funcionario'+input_name[1]+'_valor').val('0,00').attr({'readonly':'readonly'});
			
			totalCalcular();			
		}
		
	});
	
	//ABAS DA VENDA/SERVICO
	$('div#modo_aba_servico').hide(); 
	$('ul#pedido_modo_aba a[href=produto]').addClass('ativo');
	$('ul#pedido_modo_aba a').click(function(event){
		event.preventDefault();
		
		//marcar aba ativa
		$('ul#pedido_modo_aba a').removeClass('ativo');
		$(this).addClass('ativo');
		$(this).blur();
		//ocultar todas as abas
		$('div#pedido_nfe_transporte div').hide();
		
		//exibir a selecionada
		var aba = $(this).attr('href');
		$('div[id^=modo_aba_]').hide(); 
		$('div#modo_aba_'+aba).show();
	});
	
	
});