<? die(); ?>
<?	include "inc/con_db.php";
	include "inc/fnc_general.php";
	include "inc/fnc_financeiro.php";
	include "inc/fnc_estoque.php";
	
	if(isset($_POST['form'])){
		
		$formId 	= $_POST['formId'];
		$serialize 	= $_POST['form'];
		parse_str($serialize, $formVenda);
		
		$Cliente_Id	 			= '0';
		$ComissaoFuncionario	= '0';
		$retirado_por			= '';
		$endereco_entrega		= '';
		
		if($formId 	!= 'frm_pedido_rapido_novo'){ //se ja houver pagamento, ou seja, se nao for apenas 'fechar comanda'
			
			parse_str($serialize, $formPagamento);
			$hid_form_venda	= $formPagamento['hid_form_venda'];
			parse_str($hid_form_venda, $formVenda);
		
			$imprimir 			= $formPagamento['hid_print'];
			$imprimir_ecf 		= $formPagamento['hid_ecf'];
			$total_venda 		= format_number_in($formPagamento['txt_pedido_total']);
			$Cliente_Id 		= $formPagamento['hid_cliente_id'];
			$retirado_por		= ($formPagamento['txt_retirado_por']) ? $formPagamento['txt_retirado_por'] : '';
			$endereco_entrega 	= ($formPagamento['txt_pedido_endereco']) ? mysql_real_escape_string($formPagamento['txt_pedido_endereco']) : '';
		}
		
		$comanda 		= $formVenda['hid_comanda'];
		$erro 			= '0';
		
		$Funcionario_Id = $formVenda['txt_funcionario_codigo'];
		if($Funcionario_Id > 0){
			$rsComissao  			= mysql_query("SELECT fldFuncao1_Comissao FROM tblfuncionario WHERE fldId = $Funcionario_Id");
			$rowComissao 			= mysql_fetch_array($rsComissao);				
			$ComissaoFuncionario	= $rowComissao['fldFuncao1_Comissao'];
		}
		
		$Desconto 		= format_number_in($formVenda['txt_pedido_desconto']);
		$DescontoReais 	= format_number_in($formVenda['txt_pedido_desconto_reais']);
		$Comissao  		= format_number_in($formVenda['txt_pedido_comissao']);
		$dataAtual 		= date('Y-m-d');
		$horaAtual	 	= date('H:i');
		$tipoVenda 		= 2; //Tipo de venda.
		$pedido_id 		= $formVenda['txt_codigo'];
		$usuario_id		= $_SESSION['usuario_id'];

		if($pedido_id == 'novo'){
		
			$sqlInsert_pedido = "INSERT INTO tblpedido
			(fldCliente_Id, fldDesconto, fldDescontoReais, fldCadastroData, fldPedidoData, fldCadastroHora, fldEndereco, fldStatus, fldTipo_Id, fldComanda_Numero, fldComissao, fldUsuario_Id, fldRetirado_Por)
			VALUES(
			'$Cliente_Id',
			'$Desconto', 
			'$DescontoReais', 
			'$dataAtual',
			'$dataAtual',
			'$horaAtual',
			'$endereco_entrega',
			'4',
			'$tipoVenda',
			'$comanda',
			'$Comissao',
			'$usuario_id',
			'$retirado_por'
			)";

			if(mysql_query($sqlInsert_pedido)){
				$erro += 1; 
				$rsPedido 	= mysql_query("SELECT last_insert_id() as lastID");
				$LastId 	= mysql_fetch_array($rsPedido);
				$pedido_id 	= $LastId['lastID'];
				
				//adicionando os produtos do pedido na tabela
				$n	= 1;
				$limite 	= $formVenda['hid_controle'];
				
				while($n<= $limite){
	
					if(isset($formVenda['hid_item_produto_id_'.$n])){
						
						$produto_id = $formVenda['hid_item_produto_id_'.$n];
						$quantidade = format_number_in($formVenda['txt_item_quantidade_'.$n]);
						$valor 		= format_number_in($formVenda['txt_item_valor_'.$n]);
						$item_nome 	= mysql_escape_string($formVenda['txt_item_nome_'.$n]);
						$estoque_id	= $formVenda['hid_item_estoque_id_'.$n];
						$TipoVenda 	= 2; //Id do tipo de venda. / Venda r�pida.
						
						$rsValorCompra 	= mysql_query("SELECT fldValorCompra FROM tblproduto WHERE fldId = $produto_id ");
						$rowValorCompra = mysql_fetch_array($rsValorCompra);
						$valor_compra 	= $rowValorCompra['fldValorCompra'];

						//comissao produto
						if(fnc_sistema('pedido_comissao') == '2'){
							$func_id				= $formVenda['txt_funcionario_codigo'];
							$rsDadosComissao 		= mysql_query("SELECT * FROM tblproduto_comissao WHERE fldProduto_Id = $produto_id AND fldFuncionario_Id = $func_id");
							$count_comissao			= mysql_num_rows($rsDadosComissao);

							if($count_comissao > 0){
								$rowComissao_Produto 	= mysql_fetch_assoc($rsDadosComissao);
								$comissao_porc 			= ($rowComissao_Produto['fldComissao_Tipo'] == '2') ? $rowComissao_Produto['fldComissao_Valor'] : '';
								$comissao_reais			= ($rowComissao_Produto['fldComissao_Tipo'] == '1') ? $rowComissao_Produto['fldComissao_Valor'] : '';

								mysql_query("INSERT INTO tblpedido_funcionario_servico 
														  (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldComissao_Reais, fldFuncao_Tipo)
														  VALUES ('$pedido_id', '$func_id', '', '', '$valor', '$comissao_porc', '$comissao_reais', '1')");	
							}
						}
						
						$sqlInsert_Item = mysql_query("INSERT INTO tblpedido_item
						(fldProduto_Id, fldPedido_Id, fldQuantidade, fldValor, fldValor_Compra, fldDescricao, fldEstoque_Id, fldEntregue, fldEntregueData)
						values(
						'$produto_id',
						'$pedido_id',
						'$quantidade',
						'$valor',
						'$valor_compra',
						'$item_nome',
						'$estoque_id',
						'1',
						'$dataAtual'
						)");
						
						$LastId 		= mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID"));
						$item_id 		= $LastId['lastID'];						
						
						$total_item += $valor;
						fnc_estoque_movimento_lancar($produto_id, '', '', $quantidade, 1, $pedido_id, $estoque_id, $estoque_id, '', $item_id);

						//REGISTA OS COMPONENTES DOS PRODUTOS
						$sqlInsert_Componente = mysql_query("SELECT tblproduto_componente.*, tblproduto.fldEstoque_Controle FROM tblproduto_componente 
															 LEFT JOIN tblproduto ON tblproduto.fldId = tblproduto_componente.fldProduto_Componente_Id
															 WHERE fldProduto_Id = $produto_id");

						while($rowInsert_Componente = mysql_fetch_assoc($sqlInsert_Componente)){

							$id_componente		= $rowInsert_Componente['fldProduto_Componente_Id'];
							$sqlComponente		= mysql_query("SELECT fldNome FROM tblproduto WHERE fldId = $id_componente");
							$rowComponente		= mysql_fetch_assoc($sqlComponente);
							$componente_qtd		= $rowInsert_Componente['fldComponente_Qtd'] * $quantidade;
							$componente_desc	= $rowComponente['fldNome'];
							$controlar_estoque 	= $rowInsert_Componente['fldEstoque_Controle'];
							
							mysql_query("INSERT INTO tblpedido_item_componente
							(fldComponente_Id, fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Qtd, fldDescricao)
							VALUES(
							'$id_componente',
							'$produto_id',
							'$item_id',
							'$pedido_id',
							'$componente_qtd',
							'$componente_desc'
							)");

							if($controlar_estoque == '1' || $controlar_estoque == '3'){
								fnc_estoque_movimento_lancar($id_componente, '', '', $componente_qtd, 12, $pedido_id, $estoque_id, $estoque_id, '', $item_id);
							}
						}

					}
					$n += 1;
					$erro += 1;
				}
			
				//10%
				if(fnc_sistema('pedido_comissao') != '2'){
					if($Funcionario_Id > 0){
						$Funcionario_Valor		= $total_item;
						$insertServico			= "INSERT INTO tblpedido_funcionario_servico (fldPedido_Id, fldFuncionario_Id, fldValor, fldComissao, fldFuncao_Tipo)
													VALUES ('$pedido_id', '$Funcionario_Id', '$Funcionario_Valor', '$ComissaoFuncionario', '1')";
						mysql_query($insertServico);
						$erro += 1;
					}
				}

			}//INSERT PEDIDO
			echo mysql_error();
		}
		else{ // se editando
		
			//deletando as parcelas que ser�o incluidas de novo la em baixo
			$rsParcelas = mysql_query("SELECT * FROM tblpedido_parcela WHERE fldPedido_Id = $pedido_id");
			if(mysql_num_rows($rsParcelas)){
				mysql_query("DELETE FROM tblpedido_parcela WHERE fldPedido_Id = $pedido_id");
			}
			
			$sqlUpdate_pedido 		= "UPDATE tblpedido SET
			fldCliente_Id 			= '$Cliente_Id',
			fldDesconto 			= '$Desconto',
			fldDescontoReais 		= '$Desconto_Reais',
			fldComissao				= '$Comissao',
			fldUsuario_Id 			= '$usuario_id',
			fldRetirado_Por			= '$retirado_por',
			fldEndereco				= '$endereco_entrega'
			WHERE fldId 			= $pedido_id";
			
			if(mysql_query($sqlUpdate_pedido)){				
			
				$n= 1;
				$limite = $formVenda['hid_controle'];
				while($n <= $limite){
					if(isset($formVenda['hid_item_produto_id_'.$n])){
						
						$produto_id 		= $formVenda['hid_item_produto_id_'.$n];
						$item_id 			= $formVenda['hid_item_id_'.$n];
						$item_quantidade 	= format_number_in($formVenda['txt_item_quantidade_'.$n]);
						$item_valor 		= format_number_in($formVenda['txt_item_valor_'.$n]);
						$item_nome 			= mysql_escape_string($formVenda['txt_item_nome_'.$n]);
						$item_estoque_id	= $formVenda['hid_item_estoque_id_'.$n];
						
						$rsValorCompra  	= mysql_query("SELECT fldValorCompra FROM tblproduto WHERE fldId = $produto_id");
						$rowValorCompra 	= mysql_fetch_array($rsValorCompra);
						$valor_compra  		= $rowValorCompra['fldValorCompra'];

						//comissao produto
						if(fnc_sistema('pedido_comissao') == '2'){
							$func_id				= $formVenda['txt_funcionario_codigo'];
							$rsDadosComissao 		= mysql_query("SELECT * FROM tblproduto_comissao WHERE fldProduto_Id = $produto_id AND fldFuncionario_Id = $func_id");
							$count_comissao			= mysql_num_rows($rsDadosComissao);

							if($count_comissao > 0){
								$rowComissao_Produto 	= mysql_fetch_assoc($rsDadosComissao);
								$comissao_porc 			= ($rowComissao_Produto['fldComissao_Tipo'] == '2') ? $rowComissao_Produto['fldComissao_Valor'] : '';
								$comissao_reais			= ($rowComissao_Produto['fldComissao_Tipo'] == '1') ? $rowComissao_Produto['fldComissao_Valor'] : '';

								mysql_query("INSERT INTO tblpedido_funcionario_servico 
														  (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldComissao_Reais, fldFuncao_Tipo)
														  VALUES ('$pedido_id', '$func_id', '', '', '$item_valor', '$comissao_porc', '$comissao_reais', '1')");	
							}
						}
						
						if(!$item_id){
							$sql = "INSERT INTO tblpedido_item
							(fldProduto_Id, fldPedido_Id, fldQuantidade, fldValor, fldValor_Compra, fldDescricao, fldEstoque_Id, fldEntregue, fldEntregueData)
							VALUES(
							'$produto_id',
							'$pedido_id',
							'$item_quantidade',
							'$item_valor',
							'$valor_compra',
							'$item_nome',
							'$item_estoque_id',
							'1',
							'$dataAtual'
							)";
							
							if(mysql_query($sql)){
								$LastId 	= mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID "));
								$item_id 	= $LastId['lastID'];
								fnc_estoque_movimento_lancar($produto_id, '', '', $item_quantidade, 1, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
							}
						}
						$total_item += $item_valor;
						$itens_id .= $item_id.','; 

						//REGISTA OS COMPONENTES DOS PRODUTOS
						$sqlInsert_Componente = mysql_query("SELECT tblproduto_componente.*, tblproduto.fldEstoque_Controle FROM tblproduto_componente 
															 LEFT JOIN tblproduto ON tblproduto.fldId = tblproduto_componente.fldProduto_Componente_Id
															 WHERE fldProduto_Id = $produto_id");

						while($rowInsert_Componente = mysql_fetch_assoc($sqlInsert_Componente)){

							$id_componente		= $rowInsert_Componente['fldProduto_Componente_Id'];
							$sqlComponente		= mysql_query("SELECT fldNome FROM tblproduto WHERE fldId = $id_componente");
							$rowComponente		= mysql_fetch_assoc($sqlComponente);
							$componente_qtd		= $rowInsert_Componente['fldComponente_Qtd'] * $item_quantidade;
							$componente_desc	= $rowComponente['fldNome'];
							$controlar_estoque 	= $rowInsert_Componente['fldEstoque_Controle'];
							
							mysql_query("INSERT INTO tblpedido_item_componente
							(fldComponente_Id, fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Qtd, fldDescricao)
							VALUES(
							'$id_componente',
							'$produto_id',
							'$item_id',
							'$pedido_id',
							'$componente_qtd',
							'$componente_desc'
							)");

							fnc_estoque_movimento_lancar($id_componente, '', '', $componente_qtd, 12, $pedido_id, $estoque_id, $estoque_id, '', $item_id);
						
						}

					}
					
					$n += 1;
				}
				
				#CONFIRO SE AINDA EXISTE NO BANCO ALGUM ITEM DESSA VENDA, MAS QUE NAO EXISTE NA LISTAGEM => ITEM EXCLUIDO NA EDI��O
				$itens_id 	 =  substr($itens_id, 0, strlen($itens_id) -1);
				$rsExcluidos = mysql_query("SELECT * FROM tblpedido_item WHERE fldId NOT IN ($itens_id) AND fldPedido_Id = $pedido_id");
				while($rowExcluidos  = mysql_fetch_array($rsExcluidos)){
					$item_id		 = $rowExcluidos['fldId'];
					$item_estoque_id = $rowExcluidos['fldEstoque_Id'];
					$item_quantidade = $rowExcluidos['fldQuantidade'];
					$produto_id 	 = $rowExcluidos['fldProduto_Id'];
					fnc_estoque_movimento_lancar($produto_id, '', $item_quantidade, '', 11, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);	
					mysql_query("DELETE FROM tblpedido_item WHERE fldId = ".$rowExcluidos['fldId']);
					/** DELETA OS COMPONENTES DOS PRODUTOS EXCLUIDOS **/
					$rsComponente = mysql_query("SELECT * FROM tblpedido_item_componente WHERE fldProduto_Id = $produto_id AND fldPedido_Id = $pedido_id AND fldItem_Id = $item_id");
					
					if(mysql_num_rows($rsComponente)){
						
						$rowComponente_Excluido = mysql_fetch_assoc($rsComponente);
						$componente_id  = $rowComponente_Excluido['fldComponente_Id'];
						$componente_qtd = $rowComponente_Excluido['fldComponente_Qtd'];
						fnc_estoque_movimento_lancar($componente_id, '', $componente_qtd, '', 13, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
						mysql_query("DELETE FROM tblpedido_item_componente WHERE fldProduto_Id = $produto_id AND fldPedido_Id = $pedido_id AND fldItem_Id = $item_id");
					}
				}
				
				$erro += 1;
				
				//10%
				if(fnc_sistema('pedido_comissao') != '2'){
					if($Funcionario_Id > 0){
						mysql_query("DELETE FROM tblpedido_funcionario_servico WHERE fldPedido_Id = $pedido_id");
						
						$Funcionario_Valor		= $total_item;
						$insertServico			= "INSERT INTO tblpedido_funcionario_servico (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldFuncao_Tipo)
													VALUES ($pedido_id, $Funcionario_Id, '', '', '$Funcionario_Valor', '$ComissaoFuncionario', '1')";
						mysql_query($insertServico);
						echo mysql_error();
					}
				}
			}
		}
		
		//adicionando as parcelas, na tabela
		if($formId == 'frm_pedido_pagamento_parcela'){ //SE FOR PELO MODAL DE PARCELAMENTO
			$limite_parcela = $formPagamento['hid_controle_parcela'];							
			$y = 1;
			while($y <= $limite_parcela){
				if(isset($formPagamento['txt_parcela_numero_'.$y])){
					
					$parcela_numero 	= $formPagamento['txt_parcela_numero_'.$y];
					$parcela_vencimento = format_date_in($formPagamento['txt_parcela_data_'.$y]);
					$parcela_valor		= format_number_in($formPagamento['txt_parcela_valor_'.$y]);
					$pagamento_tipo 	= $formPagamento['sel_pagamento_tipo_'.$y];
					
					($pagamento_tipo  == 0) ? '1' : $pagamento_tipo;
					
					mysql_query("INSERT INTO tblpedido_parcela
					(fldPedido_Id, fldParcela, fldVencimento, fldValor, fldPagamento_Id, fldStatus)
					VALUES(
					'$pedido_id',
					'$parcela_numero',
					'$parcela_vencimento',
					'$parcela_valor',
					'$pagamento_tipo',
					'1'
					)");
					
					//ACOES DE PAGAMENTO CONFORME TIPO 
					$rowPagamentoTipo 	= mysql_fetch_array(mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldId = $pagamento_tipo"));
					$sigla 				= $rowPagamentoTipo['fldSigla'];
					
					$LastId = mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID"));
					$Data 	= date("Y-m-d");
					echo mysql_error();
	
					$acao = fnc_sistema("pedido_parcela_acao_$sigla");
					//verifica se eh a vista, se for substitui o $acao
					if($Data == $parcela_vencimento){
						$acao = fnc_sistema("pedido_parcela_acao_AV");
					}
						
					if($acao == 2){ 
						$insertBaixa = mysql_query("INSERT INTO tblpedido_parcela_baixa
							(fldParcela_Id, fldDataCadastro, fldValor, fldDataRecebido)
							VALUES(
							'".$LastId['lastID']."',
							'$Data',
							'$parcela_valor',
							'$Data'
						)");
						
						//lan�ar no caixa
						$rsConta 	= mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'pedido_parcela_conta_$sigla'");
						$rowConta 	= mysql_fetch_array($rsConta);
						$conta 		= $rowConta['fldValor'];
						
						$LastIdBaixa = mysql_fetch_array(mysql_query("Select last_insert_id() as lastID"));
						
						fnc_financeiro_conta_fluxo_lancar('', $parcela_valor, 0, '', $pagamento_tipo, $LastIdBaixa['lastID'], 3, '', $conta);
						echo mysql_error();
					
					}
					//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				}
				$y += 1;
			}
		}elseif($formId == 'frm_pedido_pagamento'){ //SE FOR PELO MODAL DE PAG A VISTA
			
			$total_recebido =  format_number_in($formPagamento['hid_total_recebido']);
			if($total_recebido > 0){ //VERIFICA SE VEM DO MODAL DE PARCELAS, OU SE � A VISTA COM ALGUM RECEBIMENTO
				$y = 1;
				$limite_parcela = $formPagamento['hid_controle_recebido'];
				while($y <= $limite_parcela){
					
					$pagamento_tipo = $formPagamento['txt_recebido_tipo_'.$y];
					$rsPagamento 	= mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldTipo = '$pagamento_tipo'");
					
					$rowPagamento 	= mysql_fetch_array($rsPagamento);
					$pagamentoTipo	= $rowPagamento['fldId'];
					$sigla 			= $rowPagamento['fldSigla'];
					$valorRecebido 	= format_number_in($formPagamento['txt_recebido_valor_'.$y]);
					
					$insertParcela = "INSERT INTO tblpedido_parcela
								(fldPedido_Id, fldParcela, fldVencimento, fldValor, fldPagamento_Id, fldStatus)
								values(
								'$pedido_id',
								'$y',
								'$dataAtual',
								'$valorRecebido',
								'$pagamentoTipo',
								'1'
								)";
								
					if(mysql_query($insertParcela)){
						
						$LastId 	 = mysql_fetch_array(mysql_query("Select last_insert_id() as lastID"));
						$lastId 	 =  $LastId['lastID'];
						$insertBaixa = mysql_query("INSERT INTO tblpedido_parcela_baixa
												(fldParcela_Id, fldDataCadastro, fldValor, fldDataRecebido)
												values(
												'$lastId',
												'$dataAtual',
												'$valorRecebido',
												'$dataAtual'
												)");
						
						//lan�ar no caixa
						$rsConta 	= mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'pedido_parcela_conta_$sigla'");
						$rowConta 	= mysql_fetch_array($rsConta);
						$conta 		= $rowConta['fldValor'];
						
						$LastIdBaixa = mysql_fetch_array(mysql_query("Select last_insert_id() as lastID"));
						$lastId 	 =  $LastIdBaixa['lastID'];
						//$descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta
						fnc_financeiro_conta_fluxo_lancar('', $valorRecebido, 0, '', $pagamentoTipo, $lastId, 3, '', $conta);
						
						//($descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta, $conta_origem='', $conta_destino=''){
						echo mysql_error();
					}
					
					$y += 1;
					$erro += 1;
				}
				
				//VERIFICA SE TEM VALOR RESTANTE DA PARCELA QUE VAI SER MARCADO NO CLIENTE
				if($total_venda > $total_recebido){
					
					$pagamento_tipo = 6; //credi�rio
					$valorRecebido 	= $total_venda - $total_recebido;
					$vencimento 	= date('Y-m-d',strtotime( "+1 month", strtotime($dataAtual)));
					
					mysql_query("INSERT INTO tblpedido_parcela
								(fldPedido_Id, fldParcela, fldVencimento, fldValor, fldPagamento_Id, fldStatus)
								values(
								'$pedido_id',
								'$y',
								'$vencimento',
								'$valorRecebido',
								'1',
								'1'
								)");
				}
				
			}else{
				$pagamento_tipo = 6; //credi�rio
				$valorRecebido 	= $total_venda;
				$vencimento 	= date('Y-m-d',strtotime( "+1 month", strtotime($dataAtual)));
				
				$insertParcela = mysql_query("INSERT INTO tblpedido_parcela
							(fldPedido_Id, fldParcela, fldVencimento, fldValor, fldPagamento_Id, fldStatus)
							values(
							'$pedido_id',
							'1',
							'$vencimento',
							'$valorRecebido',
							'$pagamentoTipo',
							'1'
							)");
			}
		}
				
		//FECHA A COMANDA SE TIVER SENDO ENVIADO POR ALGUM MODAL DE PAGAMENTO.
		if($formId != 'frm_pedido_rapido_novo' && $comanda > 0){
			$timestamp = date('Y-m-d H:i:s');
			mysql_query("UPDATE tblpedido SET
			fldComanda_Fechamento	= '$timestamp'
			WHERE fldId 			= $pedido_id");
		}
		
		
		$impressao = fnc_sistema('sistema_impressao');
		if(isset($impressao)){
			$rowImpressao = mysql_fetch_array(mysql_query("SELECT * FROM tblsistema_impressao WHERE fldId = $impressao"));
			$impressao = $rowImpressao['fldModelo'];
			if($rowImpressao['fldVariacao'] != ''){
				$impressao .= "_".$rowImpressao['fldVariacao'];
			}
		}
		
		$xml = "\t\t<pedido_id>$pedido_id</pedido_id>\n";
		if(isset($formPagamento['hid_form_venda'])){ 
			$xml .= "\t\t<imprimir>$imprimir</imprimir>\n";
			$xml .= "\t\t<imprimir_ecf>$imprimir_ecf</imprimir_ecf>\n";
		}
		$xml .= "\t\t<impressao>$impressao</impressao>\n";
		$xml .= "\t\t<erro>$erro</erro>\n";
		$xml .= "\t\t<comanda>$comanda</comanda>\n";

		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";
	}
	
?>
