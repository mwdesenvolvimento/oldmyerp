<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relatório de Comiss&otilde;es</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
        
	</head>
	<body>
   		<div id="no-print">
            <ul style="width:800px; display:inline">
                <li><a class="print" href="#" onClick="window.print()">imprimir</a></li>
            </ul>
        </div>
<?
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados		= mysql_fetch_array(mysql_query("SELECT * FROM tblempresa_info"));
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rowUsuario		= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		
		$rowFuncionario	= mysql_fetch_assoc(mysql_query('SELECT fldNome FROM tblfuncionario WHERE fldId ='. $_GET['id']));
		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Comiss&otilde;es</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr>
                <td>
                    <table style="width: 560px;margin-bottom:0" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao" style="margin-bottom:0">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
			<tr class="total">
				<td style="width:580px; margin:0;margin-left:20px">'.$rowFuncionario['fldNome'].'</td>
				<td>Per&iacute;odo '.$_SESSION['txt_funcionario_comissao_data_inicial'].' a '.$_SESSION['txt_funcionario_comissao_data_final'].'</td>
			</tr>
			<tr style="border:none; margin:8px 0 3px 0">
				<td style="width:75px;text-align:right;margin-right:10px;font-weight:bold">Venda</td>
				<td style="width:486px;text-align:left;font-weight:bold">Cliente</td>
				<td class="valor" style="width:103px;text-align:right;font-weight:bold">Valor Total</td>
				<td class="valor" style="width:95px;text-align:right;font-weight:bold">Comissão</td>
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
		##########################################################################################################################################################################################
			//CRIO A TABELA TEMPORARIA COM OS DADOS NECESSARIOS  E EM SEGUIDA FACO A SEGUNDA CONSULTA
			$sSQL 			= mysql_query($_SESSION['funcionario_comissao_temp_relatorio']);
			$sSQL 			= $_SESSION['funcionario_comissao_relatorio'];
			
			$rsRelatorio 	= mysql_query($sSQL);
			$rowsRelatorio	= mysql_num_rows($rsRelatorio);
		
			$n	 			= 1; #DEFINE O NUMERO DA DO BLOCO
			$countRegistro  = 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE 
			$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
			$limite 		= 43;
			$total_array	= mysql_num_rows($rsRelatorio);

			$pgTotal 		= ceil($total_array / $limite);
			$p = 1;
			
			unset($totalVendas);
			unset($totalComissao);
			while($rowRelatorio = mysql_fetch_assoc($rsRelatorio)){
				
				$venda_total 	= $rowRelatorio['fldTotalItem'] + $rowRelatorio['fldTotalServico'] + $rowRelatorio['fldValor_Terceiros'];
		
				if($_SESSION['sel_funcionario_comissao_desconto'] > 0){
					
					$porcentagem 		= ((100 / ($venda_total)) * $rowRelatorio['fldDescontoReais']); 	
					$desconto_comissao 	= ($porcentagem / 100) * $rowRelatorio['fldComissaoTotal']; 		
					
					$comissaoTotal 		= $rowRelatorio['fldComissaoTotal'] - $desconto_comissao;														
					$desconto	 		= ($rowRelatorio['fldDesconto'] / 100); 
					$comissaoTotal 		= ($comissaoTotal - ($desconto * $comissaoTotal));
					
				}else{
					$comissaoTotal 		= $rowRelatorio['fldComissaoTotal'];
				}
				
				
				$totalVendas 	+= $rowRelatorio['fldTotalValor'];
				$totalComissao 	+= $comissaoTotal;
				$pagina[$n] .='
					<tr>
						<td style="width:75px;text-align:right;margin-right:10px;">'.str_pad($rowRelatorio['fldPedidoId'], 6, "0", STR_PAD_LEFT).'</td>
						<td style="width:525px;text-align:left; display:inline; overflow:hidden">'.$rowRelatorio['fldNome'].'</td>
						<td class="valor" style="width:65px;text-align:right;">'.format_number_out($rowRelatorio['fldTotalValor']).'</td>
						<td class="valor" style="width:95px;text-align:right;">'.format_number_out($comissaoTotal).'</td>
					</tr>';
				
				#SE CHEGAR LIMITE, MUDA DE 'BLOCO' E RECMECA CONTAGEM
				if($countRegistro == $limite){
					$countRegistro = 1;
					$n ++;
				}elseif($total_array == $x && $countRegistro < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU TOTAL DE LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
					while($countRegistro <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countRegistro++;}
				}else{
					$countRegistro ++;
				}
				$x ++;
			}

		#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
				
                print '<td style="width: 200px"><p class="pag">'.$p.' de '.$pgTotal.'</p></td>';
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				echo  $pagina[$x];
?>
						</table>
					</td >
				</tr>
			</table>			
<?			$x ++;
			$p ++;
		}
?>        
		<table name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
            <tr style="margin:1px 10px 0 0; float:right">
                <td style="width:50px">&nbsp;</td>
                <td style="width:80px; font-weight:bold">Total Vendas:</td>
                <td style="width:60px; text-align:right"><?=format_number_out($totalVendas)?></td>
                <td style="width:95px; font-weight:bold; margin-left:40px">Total Comissão:</td>
                <td style="width:60px; text-align:right"><?=format_number_out($totalComissao)?></td>
            </tr>
        </table>

	</body>
</html>