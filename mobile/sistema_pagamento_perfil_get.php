<?php
    /* Listar todos os produtos em JSON */
    
    require_once 'class/dados.class.php';
    require_once 'class/sistema_pagamento_perfil.class.php';
     
    // array for JSON response
    $response = array();
     
    $sistema_pagamento_perfil = new sistema_pagamento_perfil();
    
    $sistema_pagamento_perfil->rows_per_page = 10000;
    $sistema_pagamento_perfil->load_all();
    
    if($sistema_pagamento_perfil->rowCount){
        // success
        $response['dados'] = $sistema_pagamento_perfil->dados;
        $response["success"] = 1;
    }
    else{
        // sem registros
        $response["success"] = 0;
        $response["message"] = "Sem registros";
    }

    echo json_encode($response);

?>