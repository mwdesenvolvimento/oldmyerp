<?php
    /* Inserir ou atualizar clientes */
    
    require_once 'class/dados.class.php';
    require_once 'class/pedido.class.php';
    require_once 'class/pedido_funcionario_servico.class.php';
    require_once 'class/pedido_item.class.php';
    require_once 'class/pedido_parcela.class.php';
    require_once '../inc/fnc_general.php';
    //var_dump($_POST);
    
    function fncUTF8toLatin1($valor){
        $valor = mb_convert_encoding($valor,"Latin1","UTF-8");
        //echo $valor . "\n";
        return addslashes($valor);;
        
    }
    
    $json = utf8_encode($_POST['json']);
    //$json = mb_convert_encoding($json,"ISO-8859-1","UTF-8");
    
    $dados = json_decode($json, true);
    
    $response['retornoNovo'] = 0;
    $response['dados'] = array();
    //echo "<hr>";
    //var_dump($dados);
    
    //echo "<hr>";
    
    //aqui parece redundante o nome do array porque n�o consegui melhorar no lado do android
    foreach($dados as $row){
        
            $pedido = new pedido();
            
            $pedido->fldCliente_Id = fncUTF8toLatin1($row['fldCliente_Id']);
            
            $pedido->fldCadastroData = fncUTF8toLatin1($row['fldCadastroData']);
            $pedido->fldCadastroHora = fncUTF8toLatin1($row['fldCadastroHora']);
            
            // n�o tem campo espec�fico no mobile
            $pedido->fldPedidoData = fncUTF8toLatin1($row['fldCadastroData']);
            
            $pedido->fldTipo_Id = fncUTF8toLatin1($row['fldTipo_Id']);
            
            $pedido_id = $pedido->store();
            
            //funcion�rio (vendedor) do pedido
            $pedido_funcionario_servico = new pedido_funcionario_servico();
            $pedido_funcionario_servico->fldPedido_Id = $pedido_id;
            $pedido_funcionario_servico->fldFuncionario_Id = fncUTF8toLatin1($row['fldFuncionario_Id']);
            $pedido_funcionario_servico->fldFuncao_Tipo = 1; //vendedor
            $pedido_funcionario_servico->store();
            
            
            
            //itens do pedido
            foreach($row['itens'] as $row_item){
                
                $pedido_item = new pedido_item();
                $pedido_item->fldPedido_Id          = $pedido_id;
                $pedido_item->fldProduto_Id 	    = fncUTF8toLatin1($row_item['fldProduto_Id']);
                $pedido_item->fldValor 	            = fncUTF8toLatin1($row_item['fldValor']);
                $pedido_item->fldDesconto           = fncUTF8toLatin1($row_item['fldDesconto']);
                $pedido_item->fldQuantidade         = fncUTF8toLatin1($row_item['fldQuantidade']);
                $pedido_item->fldDescricao          = fncUTF8toLatin1($row_item['fldDescricao']);
                $pedido_item->fldTabela_Preco_Id    = fncUTF8toLatin1($row_item['fldTabela_Preco_Id']);
                $pedido_item->fldExibicaoOrdem      = fncUTF8toLatin1($row_item['fldExibicaoOrdem']);
                $pedido_item->store();
            }
            
            //parcelas do pedido
            foreach($row['parcelas'] as $row_parcela){
                
                $pedido_parcela = new pedido_parcela();
                $pedido_parcela->fldPedido_Id       = $pedido_id;
                $pedido_parcela->fldParcela 	    = fncUTF8toLatin1($row_parcela['fldParcela']);
                $pedido_parcela->fldVencimento 	    = fncUTF8toLatin1($row_parcela['fldVencimento']);
                $pedido_parcela->fldValor 	    = fncUTF8toLatin1($row_parcela['fldValor']);
                $pedido_parcela->fldPagamento_Id    = fncUTF8toLatin1($row_parcela['fldPagamento_Id']);
                $pedido_parcela->store();
            }
    }
    
    if(1){
        // success
        $response["success"] = 1;
        $response['message'] = "armazenado";
    }
    else{
        // sem registros
        $response["success"] = 0;
        $response["message"] = "Erro";
    }
    
    echo json_encode($response);
    
?>