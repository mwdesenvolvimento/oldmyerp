<?php
    /* Listar em JSON */
    
    require_once 'class/dados.class.php';
    require_once 'class/sistema_calendario_intervalo.class.php';
     
    // array for JSON response
    $response = array();
     
    $sistema_calendario_intervalo = new sistema_calendario_intervalo();
    
    $sistema_calendario_intervalo->rows_per_page = 10000;
    $sistema_calendario_intervalo->load_all();
    
    if($sistema_calendario_intervalo->rowCount){
        // success
        $response['dados'] = $sistema_calendario_intervalo->dados;
        $response["success"] = 1;
    }
    else{
        // sem registros
        $response["success"] = 0;
        $response["message"] = "Sem registros";
    }

    echo json_encode($response);

?>