<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
        <title>myERP - Relat&oacute;rio de Parcelas</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css"></link>
 
	</head>
	<body>
        <div id="no-print">
            <a style="margin-top: 6px" class="print" href="#" onClick="window.print()">imprimir</a>
        </div>
        
<?	
		$clienteId = $_GET['cliente_id'];
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados 	= mysql_fetch_array(mysql_query("SELECT * FROM tblempresa_info"));
		$rowUsuario = mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		$rowCliente = mysql_fetch_array(mysql_query("SELECT * FROM tblcliente WHERE fldId =".$clienteId));
		echo mysql_error();
		
		//formata se tiver cpf_cnpj cadastrado
		$CPF_CNPJ 	= formatCPFCNPJTipo_out($rowCliente['fldCPF_CNPJ'], $rowCliente['fldTipo']);
		$sqlParcela = $_SESSION['parcela_relatorio'] ;
		$rsParcela 	= mysql_query($sqlParcela);
		
		while($rowParcela 	= mysql_fetch_array($rsParcela)){	
			$sql			= mysql_query("SELECT COUNT(*) as totalParcelas FROM tblpedido_parcela WHERE fldPedido_Id = ".$rowParcela['fldPedido_Id']);
			$rowsParcela	= mysql_fetch_array($sql);
			$linha ++;
			
			$ultimaData = $rowParcela['fldVencimento'];
			$parcela_num = str_pad($rowParcela['fldParcela'], 2, "0", STR_PAD_LEFT).'/'.str_pad($rowsParcela['totalParcelas'], 2, "0", STR_PAD_LEFT);
			$texto .= format_margem_print(str_pad($rowParcela['fldPedido_Id'],6,'0', STR_PAD_LEFT), 7, 'direita');
			$texto .= format_margem_print($parcela_num, 6, 'direita');
			$texto .= format_margem_print(format_date_out4($rowParcela['fldVencimento']), 9, 'direita');
			$texto .= format_margem_print(format_number_out($rowParcela['fldValor']), 8, 'direita');
						
			$sqlBaixa = "SELECT * FROM tblpedido_parcela_baixa WHERE fldParcela_Id = ".$rowParcela['fldId']." AND fldExcluido = 0 ORDER BY fldId";
			$rsBaixa 			= mysql_query($sqlBaixa);
			$totalBaixas 		= mysql_num_rows($rsBaixa);
			echo mysql_error();
			
			$valorDevedor 		= $rowParcela['fldValor'];
			$devedorAnterior 	= 0;
			$parcelaAnterior 	= 0;
			$baixaAnterior 		= 0;
			$valorOld 			= 0;
			
			$saldoParcela += $rowParcela['fldValor'];
			
			#EXIBE AS BAIXAS E VALOR DO JUROS SE HOUVER
			if($totalBaixas > 0 ){
				$texto .= "Juros   Multa   Descon    Pago  Devedor\r\n";
				
				while($rowBaixa = mysql_fetch_array($rsBaixa)){
					
					$ultimaData = $rowBaixa['fldDataRecebido'];
					//confere se ainda esta descrevendo a mesma parcela que anterior, pra pegar o devedor anterior
					if($rowParcela['fldPedido_Id'].$rowParcela['fldParcela'] != $baixaAnterior){
						$devedorAnterior 	= 0;
						$baixaAnterior 		= 0;
					}
					
					$sql		= mysql_query("SELECT SUM(fldValor) as fldValor, SUM(fldDesconto) as fldDesconto, SUM(fldMulta) as fldMulta, SUM(fldJuros) as fldJuros, fldId FROM tblpedido_parcela_baixa 
										WHERE fldParcela_Id = ".$rowBaixa['fldParcela_Id']." AND fldExcluido = 0 AND fldId < ".$rowBaixa['fldId']." ORDER BY fldId");
					$old 		= mysql_fetch_array($sql);
					
					$valorPago 	= $rowBaixa['fldValor'] + $rowBaixa['fldJuros' ] + $rowBaixa['fldMulta'];
					
					#SOMA VALOR PAGO ANTERIORMENTE ATE ESSA BAIXA | TOTAL PAGO ANTERIOR + ATUAL
					$valorOld 	= $old['fldValor'] + $old['fldJuros'] + $old['fldMulta'];
					$totalPago 	= $valorOld + $valorPago;
					
					$totalMulta = $rowBaixa['fldMulta'];
					
					if($devedorAnterior > 0){
						$totalJuros 	= $rowBaixa['fldDevedor'] - (($devedorAnterior + $old['fldDesconto']) - ($rowBaixa['fldValor'] + $rowBaixa['fldMulta'] + $rowBaixa['fldDesconto']));
						$valorReceber 	= $devedorAnterior + $totalJuros + $rowBaixa['fldMulta'];
					}else{
						$totalJuros 	= ($rowBaixa['fldDevedor'] + $rowBaixa['fldValor'] + $rowBaixa['fldDesconto']) - $rowParcela['fldValor'];
						$valorReceber 	= number_format((($totalMulta + $totalJuros + $rowParcela['fldValor']) - $valorOld),2,'.','');
					}

					
					if($rowBaixa['fldJuros'] > 0){
						$totalJuros = $rowBaixa['fldJuros'];
					}
					
					$valorDevedor 	= (($valorReceber + $totalJuros) - $valorPago) - $rowBaixa['fldDesconto'];
					$valorDesconto 	= $rowBaixa['fldDesconto'];
					
					//tipo de pagamento
					$rowPagamentoTipo = mysql_fetch_array(mysql_query("SELECT tblfinanceiro_conta_fluxo.fldPagamento_Tipo_Id, tblpagamento_tipo.* 
								FROM tblfinanceiro_conta_fluxo 
								LEFT JOIN tblpagamento_tipo ON tblfinanceiro_conta_fluxo.fldPagamento_Tipo_Id = tblpagamento_tipo.fldId
								WHERE tblfinanceiro_conta_fluxo.fldReferencia_Id = ".$rowBaixa['fldId']));
					#VERIFICA SE CHEGOU AO LIMITE DE LINHAS PRA TROCAR DE FOLHA
					($linha < $limite)? '': $n ++ ;
					
					$texto .= format_margem_print(format_number_out($totalJuros), 5, 'direita');
					$texto .= format_margem_print(format_number_out($totalMulta), 8, 'direita');
					$texto .= format_margem_print(format_number_out($valorDesconto), 9, 'direita');
					$texto .= format_margem_print(format_number_out($valorPago), 8, 'direita');
					$texto .= format_margem_print(format_number_out($valorDevedor), 9, 'direita')."\r\n";

					$texto .= "---------------------------------------- \r\n";

					$devedorAnterior 	= $rowBaixa['fldDevedor'];
					$baixaAnterior 		= $rowBaixa['fldValor'];
					$baixaAnterior 		= $rowParcela['fldPedido_Id'].$rowParcela['fldParcela'];
					
					$saldoReceber 		+= $valorReceber;
					$saldoEncargos		+= $totalJuros + $totalMulta;
					$saldoPago 			+= $valorPago;
				}// end while
			}
			
			#DEIXO UMA LINHA DE REGISTRO PARA EXIBIR O DEVEDOR, CASO AJA JUROS, JA FAZ O CALCULO PARA VALOR NO DIA ################################
			if($valorDevedor > 0){
				
				#######################################################################################################################################
				$juros 			= fnc_sistema('pedido_juros');
				$multa	 		= fnc_sistema('pedido_multa');
				$jurosTipo 		= fnc_sistema('pedido_juros_tipo');
				
				$dias 			= intervalo_data($ultimaData, date("Y-m-d"));
				$juros_ad 		= number_format((($juros / 30) * $dias), 2, '.', ''); 				// pega o juros ao mes, e divide pelo numero de dias que esta atrasado
				$valorJuros 	= number_format((($juros_ad * $valorDevedor) / 100), 2, '.', ''); 	// valor do juros calculado com o ultimo valor da parcela, o valor total antes da ultima baixa
				
				$valorJuros 	= ($valorJuros > 0) ? $valorJuros :'0.00'; //caso nao tenha vencido aina, o juros fica negativo, dando diferença no calculo do devedor
				$valorMulta 	= ($totalMulta) ? 0 : $multa;	//se ja tiver cobrado multa na baixa, nao cobrar de novo, se nao houver baxa ainda, cobrar multa
				$valorDevedor 	= $valorJuros + $valorDevedor + $valorMulta;

				$texto .= format_margem_print(format_number_out($valorJuros), 5, 'direita');
				$texto .= format_margem_print(format_number_out($valorMulta), 8, 'direita');
				$texto .= format_margem_print(format_number_out($valorDevedor), 9, 'direita');
				$texto .= format_margem_print('--------', 8, 'direita');
				$texto .= format_margem_print(format_number_out($valorDevedor), 9, 'direita')."\r\n";
	
				$texto .= "---------------------------------------- \r\n";
				
				$saldoEncargos += $valorJuros + $valorMulta;
			}
			
			#######################################################################################################################################
			$saldoDevedor += $valorDevedor;
		}
			$timestamp  = date("Ymd_His");
			$local_file = "impressao///inbox///imprimir_recibo_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
			$fp			= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
			$salva 		= fwrite($fp, $texto);
			fclose($fp);
			
?>
		<script type="text/javascript">
			var local = '<?= $local_file ?>';
			window.location=local;
			//window.close();
        </script>


	</body>
</html>
