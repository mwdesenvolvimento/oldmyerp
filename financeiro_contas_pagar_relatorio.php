<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Contas a Pagar</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />

	</head>
	<body>
    
    <div id="no-print">
        <a class="print" href="#" onClick="window.print()">imprimir</a>
    </div>
    
<?	ob_start();
	session_start();
    
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	$rsDados = mysql_query("select * from tblempresa_info");
	$rowDados = mysql_fetch_array($rsDados);
	
	$rsUsuario = mysql_query("select * from tblusuario where fldId=".$_SESSION['usuario_id']);
	$rowUsuario = mysql_fetch_array($rsUsuario);
	
	/*----------------------------------------------------------------------------------*/
	$totalRegistro = count($_SESSION['contas_pagar_relatorio']);
	echo mysql_error();
	$limite = 28;
	$n = 1;
	
	$pgTotal = $totalRegistro / $limite;
	$p = 1;
	
	$CPF_CNPJDados = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
	
							
	$totalContasProgramadas = $_SESSION['contas_pagar_relatorio_total_programada'];			
	$totalCompras = $_SESSION['contas_pagar_relatorio_total_compra'];	
	
	$tabelaCabecalho = '<tr style="border-bottom: 2px solid">
                    <td style="width: 950px"><h1>Relat&oacute;rio de Contas a Pagar</h1></td>
                    <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 930px" name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
                            <tr>
                                <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                                <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                                <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                                <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                            </tr>
                        </table>	
                    </td>
                    <td>        
                        <table class="dados_impressao">
                            <tr>
                                <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                                <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                                <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="total">
                	<td style="width: 930px">&nbsp;</td>
                	<td>Total selecionado: R$ '.format_number_out($totalContasProgramadas + $totalCompras).'</td>
                    <td style="width:10px;">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio_paisagem" class="table_relatorio_paisagem" summary="Relat&oacute;rio">
                            <tr style="border:none">
                                <td style="width:5px">&nbsp;</td>
                                <td style="width:100px">Vencimento</td>
                                <td style="width:380px">Descri&ccedil;&atilde;o</td>
                                <td style="width:260px">Marcador</td>
                                <td style="width:70px; text-align:center">Parcela</td>
                                <td style="width:200px">Pagamento</td>
                                <td style="width:100px; text-align:center">Valor</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio_paisagem" class="table_relatorio_paisagem" summary="Relat&oacute;rio">';
?>	
        <table class="relatorio_print_paisagem" style="page-break-before:avoid">
        
<?		print $tabelaCabecalho;
		$tabelaCabecalho = '<table class="relatorio_print_paisagem">'.$tabelaCabecalho;

		foreach($_SESSION['contas_pagar_relatorio'] as $conta){
			$x+= 1;
			echo mysql_error();
?>
            <tr>
                <td style="width:5px">&nbsp;</td>
                <td style="width:100px; text-align:center"><?=format_date_out($conta['vencimento'])?></td>
                <td style="width:380px;"<?=($conta['valor'] <= 0) ? ' class="conta_paga"' : ''?>><?=$conta['nome']?></td>
                <td style="width:260px;"><?=$conta['marcador']?></td>
                <td style="width:70px; text-align:center"><?=$conta['parcela']?></td>
                <td style="width:200px"><?=$conta['tipoPagamento']?></td>
                <td style="width:100px; text-align:right"<?=($conta['valor'] <= 0) ? ' class="conta_paga"' : ''?>><?=format_number_out($conta['valor'])?></td>
            </tr>
<?		
			if(($n == $limite) or ($x == $totalRegistro)){
?>							</table>    
                        </td>
                    </tr>
                </table>
<?        		$n = 1;
				if($x < $totalRegistro){
					$p += 1;
					print $tabelaCabecalho;
				}
			}else{
				$n += 1;
			}
		}
?>
        <table style="width:1150px" name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
            <tr>
                <td style="width:330px">&nbsp;</td>
                <td style="width:150px">Total contas programadas</td>
                <td style="width:120px; text-align:right"><?=format_number_out($totalContasProgramadas)?></td>
                <td style="width:10px; border-left:1px solid">&nbsp;</td>
                <td style="width:120px">Total Fornecedores</td>
                <td style="width:120px; text-align:right"><?=format_number_out($totalCompras)?></td>
                <td style="width:10px; border-left:1px solid">&nbsp;</td>
                <td style="width:80px">Total</td>
                <td style="width:120px; text-align:right"><?=format_number_out($totalContasProgramadas + $totalCompras)?></td>
            </tr>                   
        </table>
		
	</body>
</html>