<?php

	//inicializando com o dia final e inicial do mês
	$_SESSION['txt_pedido_item_data_inicial'] 	= (isset($_SESSION['txt_pedido_item_data_inicial']))? $_SESSION['txt_pedido_item_data_inicial'] : format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m'), 1, date('Y')) + 1));
	$_SESSION['txt_pedido_item_data_final'] 	= (isset($_SESSION['txt_pedido_item_data_final'])) 	? $_SESSION['txt_pedido_item_data_final'] 	: format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1));
	
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_pedido_item_data_inicial'] 	= (isset($_POST['txt_calendario_data_inicial'])) 	? $_POST['txt_calendario_data_inicial'] : format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m'), 1, date('Y')) + 1));
		$_SESSION['txt_pedido_item_data_final'] 	= (isset($_POST['txt_calendario_data_final'])) 		? $_POST['txt_calendario_data_final'] 	: format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1));
		$_SESSION['txt_pedido_item_cod']			= "";
		$_SESSION['txt_filtro_item_cod']			= "";
		$_SESSION['txt_numero_nfe']			 		= "";
		$_SESSION['txt_pedido_item_desc'] 			= "";
		$_SESSION['txt_pedido_ref'] 				= "";
		$_POST['chk_item_desc'] 					= false;
		$_SESSION['sel_pedido_item_rota']			= "";
		$_SESSION['sel_tipo_venda']					= "";
		$_SESSION['sel_pedido_item_funcionario']	= "";
		$_SESSION['filtro_edido_item_having']		= "";
	}
	else{
		$_SESSION['txt_pedido_item_data_inicial']  	= (isset($_POST['txt_pedido_item_data_inicial']) 	? $_POST['txt_pedido_item_data_inicial']	: $_SESSION['txt_pedido_item_data_inicial']);
		$_SESSION['txt_pedido_item_data_final']  	= (isset($_POST['txt_pedido_item_data_final']) 		? $_POST['txt_pedido_item_data_final'] 		: $_SESSION['txt_pedido_item_data_final']);
		$_SESSION['txt_pedido_item_cod'] 		  	= (isset($_POST['txt_pedido_item_cod']) 			? $_POST['txt_pedido_item_cod'] 			: $_SESSION['txt_pedido_item_cod']);
		$_SESSION['txt_filtro_item_cod'] 		  	= (isset($_POST['txt_filtro_item_cod']) 			? $_POST['txt_filtro_item_cod'] 			: $_SESSION['txt_filtro_item_cod']);
		$_SESSION['txt_numero_nfe']					= (isset($_POST['txt_numero_nfe']) 					? $_POST['txt_numero_nfe'] 					: $_SESSION['txt_numero_nfe']);
		$_SESSION['txt_pedido_item_desc'] 			= (isset($_POST['txt_pedido_item_desc']) 			? $_POST['txt_pedido_item_desc'] 			: $_SESSION['txt_pedido_item_desc']);
		$_SESSION['txt_pedido_ref'] 				= (isset($_POST['txt_pedido_ref']) 					? $_POST['txt_pedido_ref'] 					: $_SESSION['txt_pedido_ref']);
		$_SESSION['sel_pedido_item_rota'] 			= (isset($_POST['sel_pedido_item_rota']) 			? $_POST['sel_pedido_item_rota'] 			: $_SESSION['sel_pedido_item_rota']);
		$_SESSION['sel_tipo_venda'] 				= (isset($_POST['sel_tipo_venda']) 					? $_POST['sel_tipo_venda'] 					: $_SESSION['sel_tipo_venda']);
		$_SESSION['sel_pedido_item_funcionario']	= (isset($_POST['sel_pedido_item_funcionario'])		? $_POST['sel_pedido_item_funcionario'] 	: $_SESSION['sel_pedido_item_funcionario']);
	}
	
	$dataInicialFiltro 	= $_SESSION['txt_pedido_item_data_inicial'];
	$dataFinalFiltro 	= $_SESSION['txt_pedido_item_data_final'];
	
?>
<form id="frm-filtro" action="index.php?p=pedido&amp;modo=item" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
		<li class="large_height">
<?			$codigo_item = ($_SESSION['txt_filtro_item_cod'] ? $_SESSION['txt_filtro_item_cod'] : "c&oacute;d. item");
			($_SESSION['txt_filtro_item_cod'] == "cód. item") ? $_SESSION['txt_filtro_item_cod'] = '' : '';
?>      	<input style="width: 75px" type="text" name="txt_filtro_item_cod" id="txt_filtro_item_cod" onfocus="limpar (this,'c&oacute;d. item');" onblur="mostrar (this, 'c&oacute;d. item');" value="<?=$codigo_item?>"/>
		</li>
		<li class="large_height">
        	<input type="checkbox" name="chk_item_desc" id="chk_item_desc" <?=($_POST['chk_item_desc'] == true ? print 'checked ="checked"' : '')?>/>
<?			$item_descricao = ($_SESSION['txt_pedido_item_desc'] ? $_SESSION['txt_pedido_item_desc'] : "item");
			($_SESSION['txt_pedido_item_desc'] == "item") ? $_SESSION['txt_pedido_item_desc'] = '' : '';
?>      	<input style="width: 165px" type="text" name="txt_pedido_item_desc" id="txt_pedido_item_desc" onfocus="limpar (this,'item');" onblur="mostrar (this, 'item');" value="<?=$item_descricao?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
		<li class="large_height">
			<label for="txt_pedido_item_data_inicial">Per&iacute;odo: </label>
	     	<input title="Data inicial" style="width: 70px; text-align: center;" type="text" class="calendario-mask" name="txt_pedido_item_data_inicial" id="txt_pedido_item_data_inicial" value="<?=$dataInicialFiltro?>" />
		</li>
		<li class="large_height">
	      	<input title="Data final" style="width:70px; text-align: center;" type="text" class="calendario-mask" name="txt_pedido_item_data_final" id="txt_pedido_item_data_final" value="<?=$dataFinalFiltro?>" />
			<a href="pedido_item_calendario,<?=format_date_in($dataInicialFiltro) . ',' . format_date_in($dataFinalFiltro)?>" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
		</li>
		<li class="large_height">
<?			$codigo_compra_item = ($_SESSION['txt_pedido_item_cod'] ? $_SESSION['txt_pedido_item_cod'] : "c&oacute;d. venda");
			($_SESSION['txt_pedido_item_cod'] == "cód. venda") ? $_SESSION['txt_pedido_item_cod'] = '' : '';
?>      	<input style="width:70px" type="text" name="txt_pedido_item_cod" id="txt_pedido_item_cod" onfocus="limpar (this,'c&oacute;d. venda');" onblur="mostrar (this, 'c&oacute;d. venda');" value="<?=$codigo_compra_item?>"/>
		</li>
		<li class="large_height">
<?			$numero_nfe = ($_SESSION['txt_numero_nfe'] ? $_SESSION['txt_numero_nfe'] : "NFe");
			($_SESSION['txt_numero_nfe'] == "NFe") ? $_SESSION['txt_numero_nfe'] = '' : '';
?>      	<input style="width:50px" type="text" name="txt_numero_nfe" id="txt_numero_nfe" onfocus="limpar (this,'NFe');" onblur="mostrar (this, 'NFe');" value="<?=$numero_nfe?>"/>
		</li>
		<li class="large_height">
<?			$pedido_ref = ($_SESSION['txt_pedido_ref'] ? $_SESSION['txt_pedido_ref'] : "Ref. Venda");
			($_SESSION['txt_pedido_ref'] == "Ref. Venda") ? $_SESSION['txt_pedido_ref'] = '' : '';
?>      	<input style="width: 70px" type="text" name="txt_pedido_ref" id="txt_pedido_ref" onfocus="limpar(this,'Ref. Venda');" onblur="mostrar(this, 'Ref. Venda');" value="<?=$pedido_ref?>" />
        </li>
		<li class="large_height">
        	<select name="sel_tipo_venda" id="sel_tipo_venda" style="width:130px">
        		<option>Tipo de venda</option>
        		<option <?=($_SESSION['sel_tipo_venda'] == 'comum')	 ? 'selected="selected"' : '';?> value="comum">Comum</option>
        		<option <?=($_SESSION['sel_tipo_venda'] == 'nfe')	 ? 'selected="selected"' : '';?> value="nfe">NFe</option>
        		<option <?=($_SESSION['sel_tipo_venda'] == 'todas')	 ? 'selected="selected"' : '';?> value="todas">Todas</option>
        	</select>
        </li>
        <li>
       		<select style="width:170px" id="sel_pedido_item_funcionario" name="sel_pedido_item_funcionario" >
                <option value="">funcion&aacute;rio</option>
<?				$rsFuncionario = mysql_query("select * from tblfuncionario ORDER BY fldNome ASC");
				while($rowFuncionario= mysql_fetch_array($rsFuncionario)){
?>					<option <?=($_SESSION['sel_pedido_item_funcionario'] == $rowFuncionario['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowFuncionario['fldId'] ?>"><?= $rowFuncionario['fldNome'] ?></option>
<?				}
?>			</select>
        </li>
<?		if($_SESSION["sistema_rota_controle"] > 0){        
?>			<li>
            	<select id="sel_pedido_item_rota" name="sel_pedido_item_rota" style="width:100px" >
            		<option <?=($_SESSION['sel_pedido_item_rota'] == '') ? 'selected="selected"' : '' ?> value="">rota</option>
<?					$rsRota = mysql_query("SELECT * FROM tblendereco_rota");
					while($rowRota = mysql_fetch_array($rsRota)){
?>          	    	<option <?=($_SESSION['sel_pedido_item_rota'] == $rowRota['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowRota['fldId']?>"><?=$rowRota['fldRota']?></option>
<?					}
?>				</select>
			</li>
<?		}
?>		
		
		<li style="float:right"><button type="submit" name="btn_exibir" title="Exibir">Exibir</button></li>
    	<li style="float:right"><button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button></li>
	</ul>
    
    
  </fieldset>
</form>

<?

	$filtro = "WHERE tblpedido.fldExcluido = '0' "; 
	
	if(format_date_in($_SESSION['txt_pedido_item_data_inicial']) != "" || format_date_in($_SESSION['txt_pedido_item_data_final']) != ""){
		#CRIO DUAS SESSOES, POR CONTA DO RELATORIO, QUE EH FEITO COM DATA DE ITENS LANCADOS E DATA DE ITEM PAGO
		if($_SESSION['txt_pedido_item_data_inicial'] != "" && $_SESSION['txt_pedido_item_data_final'] != ""){
			
			$_SESSION['txt_pedido_item_data_filtro'] 		= " AND tblpedido.fldPedidoData between '".format_date_in($_SESSION['txt_pedido_item_data_inicial'])."' AND '".format_date_in($_SESSION['txt_pedido_item_data_final'])."'";
			$_SESSION['txt_pedido_item_data_pago_filtro'] 	= " AND tblpedido_item_pago.fldDataPago between '".format_date_in($_SESSION['txt_pedido_item_data_inicial'])."' AND '".format_date_in($_SESSION['txt_pedido_item_data_final'])."'";
		
		}elseif($_SESSION['txt_pedido_nfe_data1'] != "" && $_SESSION['txt_pedido_nfe_data2'] == ""){
			
			$_SESSION['txt_pedido_item_data_filtro'] 		= " AND tblpedido.fldPedidoData >= '".format_date_in($_SESSION['txt_pedido_item_data_inicial'])."'";
			$_SESSION['txt_pedido_item_data_pago_filtro']	= " AND tblpedido_item_pago.fldDataPago >= '".format_date_in($_SESSION['txt_pedido_item_data_inicial'])."'";
		
		}elseif($_SESSION['txt_pedido_nfe_data2'] != "" && $_SESSION['txt_pedido_nfe_data1'] == ""){
			
			$_SESSION['txt_pedido_item_data_filtro'] 		= " AND tblpedido.fldPedidoData <= '".format_date_in($_SESSION['txt_pedido_item_data_final'])."'";
			$_SESSION['txt_pedido_item_data_pago_filtro']	= " AND tblpedido_item_pago.fldDataPago <= '".format_date_in($_SESSION['txt_pedido_item_data_final'])."'";
		}
	}
	
	if(($_SESSION['txt_filtro_item_cod']) != ""){
		
		$filtro .= " AND tblproduto.fldCodigo = '".$_SESSION['txt_filtro_item_cod']."'";
	}
	
	if(($_SESSION['txt_pedido_item_cod']) != ""){
		
		$filtro .= " AND tblpedido_item.fldPedido_Id = '".$_SESSION['txt_pedido_item_cod']."'";
	}
	
	if(($_SESSION['txt_numero_nfe']) != ""){
		
		$filtro .= " AND tblpedido_fiscal.fldnumero = '".$_SESSION['txt_numero_nfe']."'";
	}
	
	if(($_SESSION['txt_pedido_item_desc']) != ""){
		
		if($_POST['chk_item_desc'] == true){
			$filtro .= " AND tblpedido_item.fldDescricao like '%".$_SESSION['txt_pedido_item_desc']."%'";
		}else{
			$filtro .= " AND tblpedido_item.fldDescricao like '".$_SESSION['txt_pedido_item_desc']."%'";
		}
	}
	
	if(($_SESSION['txt_pedido_ref']) != ""){
		
		$filtro .= " AND tblpedido.fldReferencia = '".$_SESSION['txt_pedido_ref']."'";
	}
	
	if($_SESSION['sel_pedido_item_rota'] != ""){
		
		$filtro .= " AND tblendereco_bairro.fldRota_Id = '".$_SESSION['sel_pedido_item_rota']."'";
	}

	if($_SESSION['sel_tipo_venda'] != "" && $_SESSION['sel_tipo_venda'] != "todas"){
		
		if($_SESSION['sel_tipo_venda'] == "comum"){
			$filtro .= " AND (tblpedido.fldTipo_Id = 1 OR tblpedido.fldTipo_Id = 2)";
		}

		if($_SESSION['sel_tipo_venda'] == "nfe"){
			$filtro .= " AND (tblpedido.fldTipo_Id = 3) ";
		}

	}	
	
	if(($_SESSION['sel_pedido_item_funcionario']) != ""){
		 $_SESSION['filtro_edido_item_having'] = " AND fldFuncionario_Id = '".$_SESSION['sel_pedido_item_funcionario']."'";
	}
	
	
	//transferir para a sessão
	$_SESSION['filtro_pedido_item'] = $filtro;
	
?>
