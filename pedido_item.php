<? die(); ?>
<?
	require("pedido_item_filtro.php");
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}

/**************************** ORDER BY *******************************************/
	$filtroOrder = 'tblpedido_item.fldPedido_Id ';
	$class 		 = 'desc';
	$order_sessao = explode(" ", $_SESSION['order_pedido_item']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'venda'		:  $filtroOrder = "PedidoId";   		break;
			case 'nfe'			:  $filtroOrder = "NFe";   				break;
			case 'item'			:  $filtroOrder = "PedidoItem"; 		break;
			case 'cliente'		:  $filtroOrder = "NomeCliente"; 		break;
			case 'valor'		:  $filtroOrder = "PedidoValor";		break;
			case 'quantidade'	:  $filtroOrder = "PedidoQuantidade";	break;
			case 'data'			:  $filtroOrder = "PedidoData";			break;						
			case 'data-entregue':  $filtroOrder = "DataItem";			break;						
			case 'ref-venda'	:  $filtroOrder = "PedidoReferencia";	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_pedido_item'] = (!$_SESSION['order_pedido_item'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_pedido_item'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=pedido&amp;modo=item$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_pedido_item']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINA��O *******************************************/
	$dataInicial = ($_SESSION['txt_pedido_item_data_inicial']) 	? format_date_in($_SESSION['txt_pedido_item_data_inicial']) : format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m'), 1, date('Y')) + 1));
	$dataFinal 	 = ($_SESSION['txt_pedido_item_data_final']) 	? format_date_in($_SESSION['txt_pedido_item_data_final']) 	: format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1));
	
	//buscar vendas que viraram NFe. Logo com os IDs de destino, � descartado as NFes da consulta abaixo
	$rsIDNFe = mysql_query("SELECT fldPedido_Destino_Nfe_Id FROM tblpedido WHERE fldPedido_Destino_Nfe_Id <> NULL OR fldPedido_Destino_Nfe_Id > 0");
	
	$x = 0;
	while($rowIDNFe = mysql_fetch_array($rsIDNFe)) {
		$nfeId[$x] = $rowIDNFe['fldPedido_Destino_Nfe_Id'];
		$x++;
	}
	
	//verifica��o para evitar erros quando n�o h� NFes
	$trechoSql = (!empty($nfeId)) ? "AND tblpedido_item.fldPedido_Id NOT IN (". implode(',', $nfeId) .")" : null;
	$_SESSION['filtro_pedido_item'] .= $trechoSql;
	
	$sSQL = "SELECT tblpedido_item.fldPedido_Id as PedidoId,
			tblpedido_item.fldValor as PedidoValor, 
			tblpedido_item.fldProduto_Id as ProdutoId,
			tblpedido_item.fldDescricao as PedidoItem,
			tblpedido_item.fldQuantidade as PedidoQuantidade,
			tblpedido_item.fldEntregueData as DataItem,
			tblpedido_item.fldReferencia,
			tblproduto.fldCodigo,
			tblpedido.fldPedidoData as PedidoData,
			tblpedido.fldReferencia as PedidoReferencia,
			tblpedido.fldTipo_Id,
			tblpedido_fiscal.fldnumero as NFe,
			tblcliente.fldNome as NomeCliente,			
			tblcliente.fldCodigo as fldCodigoCliente,
			(SELECT tblpedido_funcionario_servico.fldFuncionario_Id FROM tblpedido_funcionario_servico WHERE tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId AND tblpedido_funcionario_servico.fldFuncao_Tipo = 1 LIMIT 1) AS fldFuncionario_Id,
			
			tblendereco_bairro.fldRota_Id
			FROM tblpedido_item
			INNER 	JOIN tblproduto			ON tblproduto.fldId 				= tblpedido_item.fldProduto_Id
			INNER 	JOIN tblpedido 			ON tblpedido.fldId 					= tblpedido_item.fldPedido_Id
		
			LEFT 	JOIN tblpedido_fiscal 	ON (tblpedido_fiscal.fldpedido_id 	= tblpedido_item.fldPedido_Id OR tblpedido_fiscal.fldpedido_id = tblpedido.fldPedido_Destino_Nfe_Id) 
			INNER 	JOIN tblcliente 		ON tblcliente.fldId 				= tblpedido.fldCliente_Id
			LEFT 	JOIN tblendereco 		ON tblcliente.fldEndereco_Id 		= tblendereco.fldId
			LEFT 	JOIN tblendereco_bairro ON tblendereco_bairro.fldId 		= tblendereco.fldBairro_Id
			" . $_SESSION['filtro_pedido_item'];
	
	$_SESSION['pedido_item_relatorio'] = $sSQL ;
	$sSQL .= $_SESSION['txt_pedido_item_data_filtro']." GROUP BY tblpedido_item.fldPedido_Id, tblpedido_item.fldProduto_id, tblpedido_item.fldDescricao  ".$_SESSION['filtro_edido_item_having']." ORDER BY ". $_SESSION['order_pedido_item'];
	
	$rsTotal 	= mysql_query($sSQL);
	$rowsTotal 	= mysql_num_rows($rsTotal);
	echo mysql_error();
	
	//defini��o dos limites
	$limite = 80;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " limit " . $inicio . "," . $limite;
	$rsPedidoItem = mysql_query($sSQL);
	
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#########################################################################################
	
?>
    <form class="table_form" id="frm_pedido" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:35px;">
                    	<a <?= ($filtroOrder == 'PedidoId') 		? "class='$class'" : '' ?> style="width:35px;" href="<?=$raiz?>venda">Venda</a>
                    </li>
					<li class="order" style="width:50px; text-align:center;">
						<a <?= ($filtroOrder == 'NFe') 				? "class='$class'" : '' ?> style="width:35px;" href="<?=$raiz?>nfe">NFe</a>
					</li>
                    <li class="order" style="width:226px;">
                    	<a <?= ($filtroOrder == 'PedidoItem') 		? "class='$class'" : '' ?> style="width:226px" href="<?=$raiz?>item">Item</a>
                    </li>
					<li class="order" style="width:210px;">
                    	<a <?= ($filtroOrder == 'NomeCliente') 		? "class='$class'" : '' ?> style="width:195px" href="<?=$raiz?>cliente">Cliente</a>
                    </li>
                    <li class="order" style="width:45px;">
                    	<a <?= ($filtroOrder == 'PedidoValor') 		? "class='$class'" : '' ?> style="width:45px" href="<?=$raiz?>valor">Valor</a>
                    </li>
                    <li class="order" style="width:50px;">
                    	<a <?= ($filtroOrder == 'PedidoQuantidade') ? "class='$class'" : '' ?> style="width:35px" href="<?=$raiz?>quantidade">Qtde</a>
                    </li>
                    <li style="width:40px; text-align:center;">U.M.</li>
                    <li class="order" style="width:60px;">
                    	<a <?= ($filtroOrder == 'PedidoData') 		? "class='$class'" : '' ?> style="width:45px" href="<?=$raiz?>data">Data</a>
                    </li>
					<li class="order" style="width:60px;">
                    	<a <?= ($filtroOrder == 'DataItem') 		? "class='$class'" : '' ?> style="width:55px" href="<?=$raiz?>data-entregue">Entregue</a>
                    </li>
					<li class="order" style="width:70px;text-align:left;">
						<a <?= ($filtroOrder == 'PedidoReferencia') ? "class='$class'" : '' ?> style="width:70px" href="<?=$raiz?>ref-venda">Ref. Venda</a>
					</li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de pedidos realizadas">
<?					
					$linha = "row";
					$rows = mysql_num_rows($rsPedidoItem);
					while ($rowPedidoItem = mysql_fetch_array($rsPedidoItem)){
						
						$rsUnidade = mysql_query("SELECT tblproduto.fldId,
												 tblproduto_unidade_medida.fldSigla, tblproduto_unidade_medida.fldNome
												 FROM tblproduto LEFT JOIN tblproduto_unidade_medida ON tblproduto.fldUN_Medida_Id = tblproduto_unidade_medida.fldId
												 WHERE tblproduto.fldId=".$rowPedidoItem['ProdutoId']);
						$rowUnidade = mysql_fetch_array($rsUnidade);
						
						//linkar para o detalhe da venda ou NFe
						$paginaLink = redirecionarPagina($rowPedidoItem['fldTipo_Id'], 'venda');
						
?>						<tr class="<?= $linha; ?>">
							<td class="cod" style="width:50px; text-align: center;"><a href="?p=<?=$paginaLink;?>&amp;id=<?=$rowPedidoItem['PedidoId']?>"><? print str_pad($rowPedidoItem['PedidoId'], 5, "0", STR_PAD_LEFT)?></a></td>
							<td	style="width:48px; text-align:center; border: 1px solid #ddd; border-bottom: none; border-top: none; font-weight: bold; color:#c90;">
<?
							if($_SESSION["sistema_nfe"] > 0) {
								if(!empty($rowPedidoItem['NFe'])){
									echo str_pad($rowPedidoItem['NFe'], 4, "0", STR_PAD_LEFT);
								}
							}
?>
							</td>
							<td	style="width:241px; padding-left:8px; text-align:left;"><?=$rowPedidoItem['PedidoItem']?></td>
							<td	style="width:210px; text-align:left;"><?=$rowPedidoItem['NomeCliente']?></td>
							<td style="width:60px; text-align:right;"><?=format_number_out($rowPedidoItem['PedidoValor'])?></td>
							<td style="width:50px; text-align:right;"><?=format_number_out($rowPedidoItem['PedidoQuantidade'])?></td>
							<td style="width:40px; text-align:center;" class="tipo" title="<?=$rowUnidade['fldNome']?>"><?=$rowUnidade['fldSigla']?></td>
							<td style="width:60px; text-align:center;"><?=format_date_out3($rowPedidoItem['PedidoData'])?></td>
							<td style="width:70px; text-align:center; color: #06f;"><?=format_date_out3($rowPedidoItem['DataItem'])?></td>
							<td	style="width:70px; text-align:center;"><?=$rowPedidoItem['PedidoReferencia']?></td>
                        </tr>

<?                      $linha = ($linha == "row") ? "dif-row" : "row";
                   }
?>		 		</table>
            </div>

            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />

            <div id="table_paginacao">
<?				$paginacao_destino = "?p=pedido&modo=item";
				include("paginacao.php");
?>
            </div>   

            <div id="table_action" style=" width:660px">
                <ul id="action_button" style="float:right">
                    <li><a id="btn_print" name="btn_print" class="modal btn_print" href="pedido_item_relatorio,<?=$dataInicial?>,<?=$dataFinal?>" rel="350-150" title="Imprimir Relat&oacute;rio">&nbsp;</a></li>
                </ul>
        	</div>

            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>   
            
                
        </div>
       
	</form>