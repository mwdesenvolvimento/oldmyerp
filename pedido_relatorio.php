<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
      	<title>myERP - Relat&oacute;rio de Vendas</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
	</head>
	<body>

    <div id="no-print">
        <a class="print" href="#" onClick="window.print()">imprimir</a>
    </div>

<?	ob_start();
	session_start();

	require("inc/con_db.php");
	require("inc/fnc_general.php");

	$rsDados 	= mysql_query("SELECT * FROM tblempresa_info");
	$rowDados 	= mysql_fetch_array($rsDados);

	$rsUsuario 	= mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']);
	$rowUsuario = mysql_fetch_array($rsUsuario);
	
	$rsPedido 	= mysql_query($_SESSION['pedido_relatorio']);
	
	while($rowTotal = mysql_fetch_array($rsPedido)){
		$pedido_id 	= $rowTotal['fldPedidoId'];

		$sqlItem	= mysql_query("SELECT fldValor_Compra FROM tblpedido_item WHERE fldPedido_Id = $pedido_id");
		$rowItem 	= mysql_fetch_array($sqlItem);

		$totalCompra		+= $rowItem['fldValor_Compra'];
		$subTotalPedido		= (($rowTotal['fldTotalItem'] + $rowTotal['fldComissao']) + $rowTotal['fldValor_Terceiros']) + $rowTotal['fldTotalServico'];
		$descontoPedido 	= ($subTotalPedido 	* $rowTotal['fldDesconto']) / 100;
		$totalPedido 		= $subTotalPedido 	- $descontoPedido;
		$totalPedido 		= $totalPedido 		- $rowTotal['fldDescontoReais'];
		
		
		if($rowTotal['fldnatureza_operacao_id'] == '2'){
			$totalRelatorio		-= $totalPedido;
		}elseif($rowTotal['fldParcelas '] == '0'){
			$totalRelatorio 	= $totalRelatorio;
		}else{
			$totalRelatorio 	+= $totalPedido;
		}
	}

	unset($subTotalPedido);	
	/*----------------------------------------------------------------------------------------*/
	//print_r($_SESSION['pedido_relatorio']);
	//exit();
	$rsPedido 		= mysql_query($_SESSION['pedido_relatorio']);
	$totalRegistro 	= mysql_num_rows($rsPedido);
	echo mysql_error();
	$limite = 28;
	$n 		= 1;

	$pgTotal = $totalRegistro / $limite;
	$p = 1;

	$CPF_CNPJDados = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);

	//define se vai aparecer funcionario ou descricao da venda
	if ($_GET['desc'] == 'descricao'){
		$descricao = 'Descri&ccedil;&atilde;o';
	}
	elseif ($_GET['desc'] == 'funcionario'){
		$descricao = 'Funcion&aacute;rio';
	}

		
		$contadorCabecalho = '<tr style="border-bottom: 2px solid">
				<td style="width: 950px"><h1>Relat&oacute;rio de Vendas</h1></td>
				<td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
			</tr>';
		$tabelaCabecalho ='
			<tr>
				<td>
					<table style="width: 930px" name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
						<tr>
							<td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
							<td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
							<td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
							<td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
						</tr>
					</table>	
				</td>
				<td>        
					<table class="dados_impressao">
						<tr>
							<td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
							<td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
							<td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="total">
				<td style="width: 950px">&nbsp;</td>
				<td>Total selecionado: R$ '.format_number_out($totalRelatorio).'</td>
				<td style="width:10px;">&nbsp;</td>
			</tr>
			<tr>
				<td>
					<table name="table_relatorio_paisagem" class="table_relatorio_paisagem" summary="Relat&oacute;rio">
						<tr style="border:none; margin-top:8px; margin-bottom:2px;">';
						#testa o valor do desc e adiciona os textos ao cabecalho
						if($_GET['desc'] == 'lucros'){ #textos do cabe�alho para Relatorios de Lucro
						$tabelaCabecalho .='
							<td style="width:37px; margin-left: 5px;">C&oacute;d.</td>
							<td style="width:60px; text-align:center">Data</td>
							<td style="width:170px; margin-left:1px">Cliente</td>
							<td style="width:93px;">Status</td>
							<td style="width:55px;" title="Primeira Parcela">1&ordf; Parc.</td>
							<td style="width:55px;" title="Total de Parcelas">T. Parc.</td>
							<td style="width:50px;">A pagar</td>
							<td style="width:53px;">Desc.(%)</td>
							<td style="width:75px">Desc.(R$)</td>
							<td style="width:55px">T. Item</td>
							<td style="width:60px">Luc. Item</td>
							<td style="width:76px">T. Servi&ccedil;os</td>
							<td style="width:114px">T. Terceiros</td>
							<td style="width:48px">Total</td>
							<td style="width:64px;">Lucro Total</td>';
						}else { #Textos do cabe�alho para Relatorios de Descri��o e Funcionarios
						$tabelaCabecalho .='
							<td style="width:60px; margin-left: 5px;">C&oacute;d.</td>
							<td style="width:80px">Data venda</td>
							<td style="width:190px">Cliente</td>
							<td style="width:240px">'.$descricao.'</td>
							<td style="width:90px">Status</td>
							<td style="width:70px; text-align:right">A pagar</td>
							<td style="width:60px; text-align:right">Desc.(%)</td>
							<td style="width:60px; text-align:right">Desc.(R$)</td>
							<td style="width:60px; text-align:right">Total</td>
							<td style="width:60px; text-align:right">Lucro</td>
							<td style="width:55px; text-align:right">Pago</td>
							<td style="width:55px; text-align:right">Devedor</td>';
						}
		$tabelaCabecalho .='
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table name="table_relatorio_paisagem" class="table_relatorio_paisagem" summary="Relat&oacute;rio">';

?>	
	<table class="relatorio_print_paisagem" style="page-break-before:avoid">
<?      print $contadorCabecalho.$tabelaCabecalho;
		while($rowPedido = mysql_fetch_array($rsPedido)){
			$x+= 1;
			$pedido_id 		= $rowPedido['fldPedidoId'];

			$rsStatus 		= mysql_query("SELECT * FROM tblpedido_status WHERE fldId =". $rowPedido['fldStatus']);
			$rowStatus		= mysql_fetch_array($rsStatus);

			unset($subTotalPedido);
			unset($totalPedido);
			unset($totalCompra);
			unset($totalLucro);
			unset($totalItem);

			$sqlServico =  mysql_query("SELECT SUM(fldValor) as fldValorServico FROM tblpedido_funcionario_servico WHERE fldFuncao_Tipo = 2 AND fldPedido_Id = $pedido_id");
			$rowServico = mysql_fetch_array($sqlServico);				

			$sqlItem = mysql_query("SELECT group_concat(fldDescricao SEPARATOR ', ') as fldDescricao, SUM((fldValor * fldQuantidade) - (((fldValor * fldQuantidade) * fldDesconto) / 100)) AS fldTotalItem,
					SUM(fldValor_Compra * fldQuantidade) as fldValor_Compra
					FROM tblpedido_item WHERE fldPedido_Id = $pedido_id");
			$rowItem = mysql_fetch_array($sqlItem);


			$totalItem 			+= $rowItem['fldTotalItem'];
			$descricao 			= $rowItem['fldDescricao'];
			$totalCompra 		= $rowItem['fldValor_Compra'];
			$descPedido 		= $rowPedido['fldDesconto'];
			$descPedidoReais 	= $rowPedido['fldDescontoReais'];
			$subTotalPedido		= $totalItem + $rowPedido['fldComissao'] + $rowPedido['fldValor_Terceiros'] + $rowServico['fldValorServico'];
			$descontoPedido 	= ($subTotalPedido 	* $descPedido) / 100;
			$totalPedido 		= $subTotalPedido 	- $descontoPedido;
			$totalPedido 	 	= $totalPedido 		- $descPedidoReais;

			
			$sSQL = "SELECT tblpedido_parcela.fldId, SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor,
					(SELECT tblpedido_parcela.fldVencimento FROM tblpedido_parcela WHERE tblpedido_parcela.fldPedido_id = $pedido_id ORDER BY fldVencimento ASC LIMIT 1) AS primeiraParcela
					FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
					WHERE tblpedido_parcela.fldPedido_Id = $pedido_id  GROUP BY tblpedido_parcela.fldPedido_Id";

			$rsBaixa  = mysql_query($sSQL);
			$rowBaixa = mysql_fetch_array($rsBaixa);
			echo mysql_error();

			#Consultas que s� seram feitas para o relatorio de lucros
			if($_GET['desc'] == 'lucros'){

				$sqlTotalParcelasPedido = mysql_query("SELECT COUNT(tblpedido_parcela.fldId) AS fldTotalParcelas FROM tblpedido_parcela WHERE fldPedido_Id = $pedido_id");
				$rowTotalParcelas = mysql_fetch_assoc($sqlTotalParcelasPedido);

				$sqlTotalItensPedido = mysql_query("SELECT tblpedido_item.fldProduto_Id, (SUM(fldValor - (fldValor * (fldDesconto / 100))) * fldQuantidade) AS totalItem, IF(fldValor_Compra != 0, SUM(fldValor_Compra * fldQuantidade), SUM(tblproduto.fldValorCompra * fldQuantidade)) AS totalCompra FROM tblpedido_item INNER JOIN tblproduto ON tblpedido_item.fldProduto_Id = tblproduto.fldId WHERE fldPedido_Id = $pedido_id");
				$rowTotalItens = mysql_fetch_assoc($sqlTotalItensPedido);

				$tItens			= $rowTotalItens['totalItem'];
				$tItensCompra 	= $rowTotalItens['totalCompra'];
				$lucroItens 	= $tItens - $tItensCompra;

			}

			$valorBaixa 		= $rowBaixa['fldBaixaValor'];
			$valorDevedor 		= $totalPedido - $rowBaixa['fldBaixaValor'];
			$pParcela			= $rowBaixa['primeiraParcela'];
			/*
			$totalLucro 		+= ($totalPedido - $totalCompra) - $rowPedido['fldComissao'];
			$totalTerceiros		+= $rowPedido['fldValor_Terceiros'];
			$totalItens 		+= $totalItem;
			$totalServicos 		+= $rowServico['fldValorServico'];
			$totalPedidos 		+= $subTotalPedido;
			$totalLucros 		+= $totalLucro;
			$totalPagar 		+= $totalPedido;
			$totalBaixa 		+= $valorBaixa;
			$totalDevedor 		+= $valorDevedor;
			*/
			if($rowPedido['fldnatureza_operacao_id'] == '2'){
				$totalLucro 		-= ($totalPedido - $totalCompra) - $rowPedido['fldComissao'];
				$totalItens 		-= $totalItem;
				$totalPedidos 		-= $subTotalPedido;
				$totalLucros 		-= $totalLucro;
				$totalPagar 		-= $totalPedido;
				
			}elseif($rowPedido['fldParcelas '] == '0'){
				$totalLucro 		= $totalLucro;
				$totalTerceiros		= $totalTerceiros;
				$totalItens 		= $totalItens;
				$totalServicos 		= $totalServicos;
				$totalPedidos 		= $totalPedidos;
				$totalLucros 		= $totalLucros;
				$totalPagar 		= $totalPagar;
				$totalBaixa 		= $totalBaixa;
				$totalDevedor 		= $totalDevedor;
			}else{
				$totalLucro 		+= ($totalPedido - $totalCompra) - $rowPedido['fldComissao'];
				$totalTerceiros		+= $rowPedido['fldValor_Terceiros'];
				$totalItens 		+= $totalItem;
				$totalServicos 		+= $rowServico['fldValorServico'];
				$totalPedidos 		+= $subTotalPedido;
				$totalLucros 		+= $totalLucro;
				$totalPagar 		+= $totalPedido;
				$totalBaixa 		+= $valorBaixa;
				$totalDevedor 		+= $valorDevedor;
			}

			echo mysql_error();

			//define se vai aparecer a descri��o ou o nome do funcionario
			if ($_GET['desc'] == 'descricao'){
				$descricao = substr($descricao,0,36);
			}
			elseif ($_GET['desc'] == 'funcionario'){
				$descricao = substr($rowPedido['fldFuncionarioNome'],0,36);
			}


			#verifica se � relatorio de lucros ou de funcionario/desc
			if($_GET['desc'] == 'lucros'){ #CONTEUDO DO RELATORIO DE LUCROS ?>

                <tr>
                    <td style="width:42px; margin-left: 5px;"><?=str_pad($rowPedido['fldPedidoId'], 6, "0", STR_PAD_LEFT)?></td>
                    <td style="width:55px"><?=format_date_out3($rowPedido['fldPedidoData'])?></td>
                    <td style="width:170px"><?=substr($rowPedido['fldClienteNome'],0, 25)?></td>
                    <td style="width:90px"><?=$rowStatus['fldStatus']?></td>
                    <td style="width:71px;"><?=format_date_out3($pParcela)?></td>
                    <td style="width:0px; text-align:center;"><?=str_pad($rowTotalParcelas['fldTotalParcelas'], 2, 0, STR_PAD_LEFT)?></td>
                    <td style="width:2px;"></td>
<?					if($rowPedido['fldParcelas'] != '0'){                    
?>						<td style="width:64px; text-align:right"><?=format_number_out($subTotalPedido)?></td>
                        <td style="width:46px; text-align:right"><?=format_number_out($rowPedido['fldDesconto'])?></td>
                        <td style="width:66px; text-align:right"><?=format_number_out($rowPedido['fldDescontoReais'])?></td>
                        <td style="width:64px; text-align:right"><?=format_number_out($tItens)?></td>
                        <td style="width:70px; text-align:right"><?=format_number_out($lucroItens)?></td>
                        <td style="width:66px; text-align:right"><?=format_number_out($rowServico['fldValorServico'])?></td>
                        <td style="width:85px; text-align:right"><?=format_number_out($rowPedido['fldValor_Terceiros'])?></td>
                        <td style="width:75px; text-align:right"><?=format_number_out($totalPedido)?></td>
                        <td style="width:84px; text-align:right"><?=format_number_out($totalLucro)?></td>
<?					
					}elseif($rowPedido['fldnatureza_operacao_id'] == '2'){
						echo '<td style="width:64px; text-align:right"> -'.format_number_out($subTotalPedido).'</td>';
						echo '<td style="width:595px"></td>';
					}else{
						echo '<td style="width:560px"></td>';
					}
?>              </tr>				
<?php		} else { #CONTEUDO DO RELATORIO DE FUNCIONARIO/DESC ?>
                <tr>
                    <td style="width:60px; margin-left: 5px;"><?=str_pad($rowPedido['fldPedidoId'], 6, "0", STR_PAD_LEFT)?></td>
                    <td style="width:80px"><?=format_date_out($rowPedido['fldPedidoData'])?></td>
                    <td style="width:190px"><?=substr($rowPedido['fldClienteNome'],0, 28)?></td>
                    <td style="width:240px"><?=$descricao?></td>
                    <td style="width:90px"><?=$rowStatus['fldStatus']?></td>
<?					if($rowPedido['fldParcelas'] != '0'){                    
?>						<td style="width:70px; text-align:right"><?=format_number_out($subTotalPedido)?></td>
                        <td style="width:60px; text-align:right"><?=format_number_out($rowPedido['fldDesconto'])?></td>
                        <td style="width:60px; text-align:right"><?=format_number_out($rowPedido['fldDescontoReais'])?></td>
                        <td style="width:60px; text-align:right"><?=format_number_out($totalPedido)?></td>
                        <td style="width:60px; text-align:right"><?=format_number_out($totalLucro)?></td>
                        <td style="width:55px; text-align:right"><?=format_number_out($valorBaixa)?></td>
                        <td style="width:55px; text-align:right"><?=format_number_out($valorDevedor)?></td>
<?					}elseif($rowPedido['fldnatureza_operacao_id'] == '2'){
						echo '<td style="width:64px; text-align:right"> -'.format_number_out($subTotalPedido).'</td>';
						echo '<td style="width:595px"></td>';
					}else{
						echo '<td style="width:560px"></td>';
					}                      
?>              </tr>
<?			}

			if(($n == $limite) or ($x == $totalRegistro)){
?>							</table>
						</td>
					</tr>
				</table>
<?				$n = 1;
				if($p < $pgTotal){
						$p += 1;
				}
				if($x < $totalRegistro){
					$contadorCabecalho = '<tr style="border-bottom: 2px solid">
					<td style="width: 950px"><h1>Relat&oacute;rio de Vendas</h1></td>
					<td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
					</tr>';
					print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$tabelaCabecalho;
				}
			}else{
				$n += 1;
			}
		}
?>
        <table style="width:1152px" name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
            <tr>
                <td style="width:50px">Produtos</td>
                <td style="width:70px; text-align:right"><?=format_number_out($totalItens)?></td>
                <td style="border-right:1px solid">&nbsp;</td> 

                <td style="width:50px">Servi&ccedil;os</td>
                <td style="width:70px; text-align:right"><?=format_number_out($totalServicos)?></td>
                <td style="border-left:1px solid">&nbsp;</td>

                <td style="width:50px">Terceiros</td>
                <td style="width:70px; text-align:right"><?=format_number_out($totalTerceiros)?></td>
                <td style="border-left:1px solid">&nbsp;</td>

                <td style="width:50px">Total</td>
                <td style="width:80px; text-align:right"><?=format_number_out($totalPedidos)?></td>
                <td style="border-left:1px solid">&nbsp;</td>

                <td style="width:50px">A Pagar</td>
                <td style="width:60px; text-align:right"><?=format_number_out($totalPagar)?></td>
                <td style="border-left:1px solid">&nbsp;</td>

                <td style="width:50px">Lucro</td>
                <td style="width:60px; text-align:right"><?=format_number_out($totalLucros)?></td>
                <td style="width:2px; border-left:1px solid">&nbsp;</td>

                <td style="width:50px">Pago</td>
                <td style="width:60px; text-align:right"><?=format_number_out($totalBaixa)?></td>
                <td style="border-left:1px solid">&nbsp;</td>

                <td style="width:50px">Devedor</td>
                <td style="width:60px; text-align:right"><?=format_number_out($totalDevedor)?></td>
				<? if($_GET['desc'] == 'lucros') { ?>
				<td style="font-size:11px; text-align:right; width:1145px;">Obs: O lucro do item &eacute; baseado no valor de compra e venda na hora em que o pedido do mesmo foi feito, caso no momento citado o valor de compra seja "0", o sistema atribui o valor de custo atual para o relat&oacute;rio.</td>
				<? } ?>
            </tr>                   
        </table>

	</body>
</html>