<?php
	
	unset($filtro);
	unset($_SESSION['funcionario_filtro_data_pedido'] );
	unset($_SESSION['funcionario_filtro_data_status'] );
	//recebendo a data do calendário de período
	if(isset($_POST['txt_calendario_data_inicial']) && isset($_POST['txt_calendario_data_final'])){
	    $_SESSION['txt_funcionario_comissao_data_inicial'] 	= (isset($_POST['txt_calendario_data_inicial'])) ? $_POST['txt_calendario_data_inicial'] : '';
	    $_SESSION['txt_funcionario_comissao_data_final'] 	= (isset($_POST['txt_calendario_data_final']))	 ? $_POST['txt_calendario_data_final']  	: '';
	}
	$_SESSION['txt_funcionario_comissao_data_inicial'] 		= (isset($_SESSION['txt_funcionario_comissao_data_inicial']))? $_SESSION['txt_funcionario_comissao_data_inicial'] : date('d') . '/' . date('m/Y', mktime(0, 0, 0, date('m') - 3));
	$_SESSION['txt_funcionario_comissao_data_final'] 		= (isset($_SESSION['txt_funcionario_comissao_data_final']))	 ? $_SESSION['txt_funcionario_comissao_data_final']   : date('d/m/Y');
	
	
	$_SESSION['sel_funcionario_comissao_desconto'] 			= (!isset($_SESSION['sel_funcionario_comissao_desconto'])) 				? '1'															: $_SESSION['sel_funcionario_comissao_desconto'];
	$_SESSION['sel_funcionario_comissao_status'] 			= (!isset($_SESSION['sel_funcionario_comissao_status'])) 				? 'em aberto' 													: $_SESSION['sel_funcionario_comissao_status'];
	$_SESSION['sel_funcionario_comissao_periodo_status'] 	= (isset($_SESSION['sel_funcionario_comissao_periodo_status']))			? $_SESSION['sel_funcionario_comissao_periodo_status'] 			: 'emitido';
	$_SESSION['sel_funcionario_comissao_pedido_status']		= (isset($_SESSION['sel_funcionario_comissao_pedido_status'])) 			? $_SESSION['sel_funcionario_comissao_pedido_status']			: 'status';
	
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_funcionario_comissao_data_inicial'] 		= date('d') . '/' . date('m/Y', mktime(0, 0, 0, date('m') - 3));
		$_SESSION['txt_funcionario_comissao_data_final'] 		= date('d/m/Y');
		$_SESSION['sel_funcionario_comissao_periodo_status'] 	= "emitido";
		$_SESSION['txt_funcionario_comissao_pedido'] 			= "";
		$_SESSION['sel_funcionario_comissao_desconto'] 			= "1";
		$_SESSION['sel_funcionario_comissao_status'] 			= "";
		$_SESSION['sel_funcionario_comissao_pedido_status']		= "status";
	}else{
		$_SESSION['txt_funcionario_comissao_data_inicial'] 		= (isset($_POST['txt_funcionario_comissao_data_inicial']))	? $_POST['txt_funcionario_comissao_data_inicial'] 	: $_SESSION['txt_funcionario_comissao_data_inicial'];
		$_SESSION['txt_funcionario_comissao_data_final'] 		= (isset($_POST['txt_funcionario_comissao_data_final'])) 	? $_POST['txt_funcionario_comissao_data_final'] 	: $_SESSION['txt_funcionario_comissao_data_final'];
		$_SESSION['sel_funcionario_comissao_periodo_status'] 	= (isset($_POST['sel_funcionario_comissao_periodo_status']) ? $_POST['sel_funcionario_comissao_periodo_status'] : $_SESSION['sel_funcionario_comissao_periodo_status']);
		$_SESSION['txt_funcionario_comissao_pedido'] 			= (isset($_POST['txt_funcionario_comissao_pedido'])) 		? $_POST['txt_funcionario_comissao_pedido'] 		: $_SESSION['txt_funcionario_comissao_pedido'];
		$_SESSION['sel_funcionario_comissao_desconto'] 			= (isset($_POST['sel_funcionario_comissao_desconto']))		? $_POST['sel_funcionario_comissao_desconto']		: $_SESSION['sel_funcionario_comissao_desconto'];
		$_SESSION['sel_funcionario_comissao_status'] 			= (isset($_POST['sel_funcionario_comissao_status'])) 		? $_POST['sel_funcionario_comissao_status']			: $_SESSION['sel_funcionario_comissao_status'];
		$_SESSION['sel_funcionario_comissao_pedido_status']		= (isset($_POST['sel_funcionario_comissao_pedido_status'])) ? $_POST['sel_funcionario_comissao_pedido_status']	: $_SESSION['sel_funcionario_comissao_pedido_status'];
	}

?>
<form id="frm-filtro" action="<?=$endereco_raiz?>&amp;modo=comissao" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
        <li>
      		<label for="txt_funcionario_comissao_pedido">Venda: </label>
<?			$pedido = $_SESSION['txt_funcionario_comissao_pedido'];
?>      	<input title="Pedido" style="width: 50px" type="text" name="txt_funcionario_comissao_pedido" id="txt_funcionario_comissao_pedido" value="<?=$pedido?>"/>
		</li>
        <li>
            <select id="sel_funcionario_comissao_pedido_status" name="sel_funcionario_comissao_pedido_status" style="width:85px" >
            	<option <?=($_SESSION['sel_funcionario_comissao_pedido_status'] == 'status') ? 'selected="selected"' : '' ?> value="status">status</option>
<?				$rsStatus = mysql_query("SELECT * FROM tblpedido_status");
				while($rowStatus = mysql_fetch_array($rsStatus)){
?>              	<option <?=($_SESSION['sel_funcionario_comissao_pedido_status'] == $rowStatus['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowStatus['fldId']?>"><?=$rowStatus['fldStatus']?></option>
<?				}
?>			</select>
		</li>
    	<li>
      		<label for="txt_funcionario_comissao_data_inicial">Per&iacute;odo: </label>
            <select id="sel_funcionario_comissao_periodo_status" name="sel_funcionario_comissao_periodo_status" style="width: 80px" >
            	<option <?=($_SESSION['sel_funcionario_comissao_periodo_status'] == 'emitido') ? 'selected="selected"' : '' ?> value="emitido">Emiss&atilde;o</option>
<?				$rsStatus = mysql_query("SELECT * FROM tblpedido_status");
				while($rowStatus = mysql_fetch_array($rsStatus)){
?>              	<option <?=($_SESSION['sel_funcionario_comissao_periodo_status'] == $rowStatus['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowStatus['fldId']?>"><?=$rowStatus['fldStatus']?></option>
<?				}
?>			</select>
<?			$vencimento1 = $_SESSION['txt_funcionario_comissao_data_inicial'];
?>      	<input title="Data inicial" style="text-align: center; width: 70px" type="text" class="calendario-mask" name="txt_funcionario_comissao_data_inicial" id="txt_funcionario_comissao_data_inicial" value="<?=$vencimento1?>"/>
		</li>
    	<li>
<?			$vencimento2 = $_SESSION['txt_funcionario_comissao_data_final'];
?>     		<input title="Data final" style="text-align: center; width: 70px" type="text" class="calendario-mask" name="txt_funcionario_comissao_data_final" id="txt_funcionario_comissao_data_final" value="<?=$vencimento2?>"/>
			<a href="calendario_periodo,<?=format_date_in($vencimento1) . ',' . format_date_in($vencimento2)?>,funcionario_detalhe&id=<?=$funcionario_id?>&modo=comissao" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
		</li>
        <li>
            <select style="width:210px" id="sel_funcionario_comissao_desconto" name="sel_funcionario_comissao_desconto">
                <option value="1" <?= ($_SESSION['sel_funcionario_comissao_desconto'] == "1") ? "selected" : "" ?>>considerar desconto em venda</option>
                <option value="0" <?= ($_SESSION['sel_funcionario_comissao_desconto'] == "0") ? "selected" : "" ?>>desconsiderar desconto em venda</option>
        	</select>
      	</li>
        
   		<li>
      		<label for="sel_funcionario_comissao_status">Comiss&atilde;o</label>
            <select id="sel_funcionario_comissao_status" name="sel_funcionario_comissao_status" style="width:92px;">
                <option value="todos" 					<?= ($_SESSION['sel_funcionario_comissao_status'] == "todos") 					? "selected" : "" ?>>todas</option>
                <option value="em aberto" 				<?= ($_SESSION['sel_funcionario_comissao_status'] == "em aberto")				? "selected" : "" ?>>em aberto</option>
				<option value="em aberto disponivel" 	<?= ($_SESSION['sel_funcionario_comissao_status'] == "em aberto disponivel") 	? "selected" : "" ?>>em aberto dispon&iacute;vel</option>
				<option value="em aberto indisponivel"	<?= ($_SESSION['sel_funcionario_comissao_status'] == "em aberto indisponivel")	? "selected" : "" ?>>em aberto indispon&iacute;vel</option>
                <option value="pago" 					<?= ($_SESSION['sel_funcionario_comissao_status'] == "pago") 					? "selected" : "" ?>>pagas</option>
        	</select>
      	</li>
        
        <li style="float:right">
        	<button style="margin-top: 0" type="submit" name="btn_exibir" title="Exibir">Exibir</button>
        </li>
        <li style="float:right">
       		<button style="margin-top: 0" type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
        </li>
    </ul>
    
  </fieldset>
</form>

<?

	if($_SESSION['txt_funcionario_comissao_pedido'] > 0){
		$filtro	= ' WHERE fldPedidoId = '.$_SESSION['txt_funcionario_comissao_pedido'];
	}
	
	$filtro_status .= " HAVING (fldTotalServico + fldTotalItem) > 0 ";	
	if(($_SESSION['sel_funcionario_comissao_status']) != ""){
		if($_SESSION['sel_funcionario_comissao_desconto'] > 0){
			
			$porcentagem 		= " ((100 / (fldTotalServico + fldTotalItem + fldValor_Terceiros)) * fldDescontoReais)"; 		//porcentagem de desconto caso dado em reais
			$desconto_comissao 	= " ($porcentagem / 100) * fldComissaoTotal"; 													//desconto calculado, resultado em reais
			
			$comissaoTotal 		= " fldComissaoTotal - $desconto_comissao";														//valor da comissao, ja com desconto referente a venda, se for dado em dinheiro
			$desconto	 		= " (fldDesconto / 100)"; 
			$comissaoTotal 		= "	REPLACE(FORMAT($comissaoTotal - ($desconto * FORMAT($comissaoTotal,2)),2), ',' ,'')";		//aqui calcula e ja subtrai o desconto, caso seja dado em porcentagem na venda;

			//CALCULAR COMISSAO LIBERADA
			$comissaoLiberada 	= " (100 / fldTotalParcelas) * fldParcelaValorBaixa";											//calcula a porcentagem do que ja foi pago
			$comissaoLiberada 	= " ($comissaoLiberada / 100) * $comissaoTotal";												//calcula o que esta liberado
		}else{
			$comissaoTotal 		= " fldComissaoTotal";
			$comissaoLiberada 	= " (100 / (fldTotalServico + fldTotalItem + fldValor_Terceiros)) * fldParcelaValorBaixa";	
			$comissaoLiberada 	= " ($comissaoLiberada / 100) * fldComissaoTotal";
		}
		
		switch($_SESSION['sel_funcionario_comissao_status']){
			case "em aberto":
				$filtro_status .= " AND(
								(fldComissaoPago < $comissaoTotal)
								or
								(fldComissaoPago is Null)
								or
								(fldComissaoPago = 0)
							)";
			break;
			
			case "em aberto disponivel":
				$filtro_status .= " AND (
								(REPLACE(FORMAT(($comissaoLiberada) ,2), ',','') > fldComissaoPago
								)or(
									($comissaoLiberada) > 0
									and
									fldComissaoPago is Null)
								)";
			break;
		
			case "em aberto indisponivel":
				$filtro_status .= " AND((
									(fldComissaoPago < ($comissaoTotal))
								AND
									((($comissaoLiberada) <= fldComissaoPago))
								)or (($comissaoLiberada) is Null) or (($comissaoLiberada) = 0)
							)";
			break;
			
			case "pago":
				$filtro_status .= " AND fldComissaoPago >= ($comissaoTotal)";
			break;

			case "todos":
				$filtro_status .= "";
			break;
		}
	}
	
	
	if(format_date_in($_SESSION['txt_funcionario_comissao_data_inicial']) != "" || format_date_in($_SESSION['txt_funcionario_comissao_data_final']) != ""){
		
		if(($_SESSION['sel_funcionario_comissao_periodo_status']) != "emitido"){
		
			if($_SESSION['txt_funcionario_comissao_data_inicial'] != "" && $_SESSION['txt_funcionario_comissao_data_final'] != ""){
				$_SESSION['funcionario_filtro_data_status'] .= " AND tblpedido_status_historico.fldData BETWEEN '".format_date_in($_SESSION['txt_funcionario_comissao_data_inicial'])."' AND '".format_date_in($_SESSION['txt_funcionario_comissao_data_final'])."'" ;
			}
			
			elseif($_SESSION['txt_funcionario_comissao_data_inicial'] != "" && $_SESSION['txt_funcionario_comissao_data_final'] == ""){
				$_SESSION['funcionario_filtro_data_status'] .= " AND tblpedido_status_historico.fldData >= '".format_date_in($_SESSION['txt_funcionario_comissao_data_inicial'])."'";
			}
			
			elseif($_SESSION['txt_funcionario_comissao_data_final'] != "" && $_SESSION['txt_funcionario_comissao_data_inicial'] == ""){
				$_SESSION['funcionario_filtro_data_status'] .= " AND tblpedido_status_historico.fldData <= '".format_date_in($_SESSION['txt_funcionario_comissao_data_final'])."'";
			}
			
			$where .= (!isset($where)) ? " WHERE" : " AND";
			$where .= " fldData_Historico != 'null'";
		
			$_SESSION['funcionario_filtro_data_status'] .= " AND tblpedido_status_historico.fldId  IN (SELECT MAX(fldId) FROM tblpedido_status_historico WHERE fldStatus_Id = '".$_SESSION['sel_funcionario_comissao_periodo_status']."' AND fldPedido_Id = fldPedidoId) "; //ULTIMO RESULTADO
		
		}else{
			if($_SESSION['txt_funcionario_comissao_data_inicial'] != "" && $_SESSION['txt_funcionario_comissao_data_final'] != ""){
				$_SESSION['funcionario_filtro_data_pedido'] .= " AND fldPedidoData BETWEEN '".format_date_in($_SESSION['txt_funcionario_comissao_data_inicial'])."' AND '".format_date_in($_SESSION['txt_funcionario_comissao_data_final'])."'";
			}elseif($_SESSION['txt_funcionario_comissao_data_inicial'] != "" && $_SESSION['txt_funcionario_comissao_data_final'] == ""){
				$_SESSION['funcionario_filtro_data_pedido'] .= " AND fldPedidoData >= '".format_date_in($_SESSION['txt_funcionario_comissao_data_inicial'])."'";
			}elseif($_SESSION['txt_funcionario_comissao_data_final'] != "" && $_SESSION['txt_funcionario_comissao_data_inicial'] == ""){
				$_SESSION['funcionario_filtro_data_pedido'] .= " AND fldPedidoData <= '".format_date_in($_SESSION['txt_funcionario_comissao_data_final'])."'";
			}
		}		
	}
	
	if($_SESSION['sel_funcionario_comissao_pedido_status'] != 'status'){
		$status_id = $_SESSION['sel_funcionario_comissao_pedido_status'];
		$where .= (!isset($where)) ? " WHERE" : " AND";
		$where .= " fldPedido_Status = '$status_id'";
	}
		
	//transferir para a sessão
	$_SESSION['filtro_funcionario_comissao'] = $where.' '.$filtro.' '.$filtro_status;

?>