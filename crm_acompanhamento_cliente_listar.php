<?
	require("crm_acompanhamento_cliente_filtro.php");
	
	//a��es em grupo
	if(isset($_POST['btn_action'])){
		require("crm_acompanhamento_cliente_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}	
	
/**************************** ORDER BY *******************************************/
	$filtroOrder = 'DataAgendamento';
	$class 		  = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_acompanhamento']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'ticket'		:  $filtroOrder = "fldId";   				break;
			case 'prioridade'	:  $filtroOrder = "fldPrioridade_Id";		break;
			case 'data'			:  $filtroOrder = "DataAgendamento";		break;
			case 'cliente'		:  $filtroOrder = "ClienteNome"; 			break;
			case 'funcionario'	:  $filtroOrder = "FuncionarioNome";		break;
			case 'tipo'			:  $filtroOrder = "fldTipo_Nome";			break;
			case 'situacao'		:  $filtroOrder = "fldSituacao_Nome";		break;
			
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_acompanhamento'] = (!$_SESSION['order_acompanhamento'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_acompanhamento'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=crm_acompanhamento_cliente$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_acompanhamento']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINA��O *******************************************/

	$sSQL = "SELECT tblcrm_acompanhamento_cliente_chamado.*,
			 tblcrm_acompanhamento_cliente_prioridade.fldPrioridade_Nome, tblcrm_acompanhamento_cliente_prioridade.fldPrioridade_Cor,
			 tblcrm_acompanhamento_cliente_situacao.fldSituacao_Nome,
			 tblcrm_acompanhamento_cliente_tipo.fldTipo_Nome,
			 tblcrm_acompanhamento_agenda.fldData AS DataAgendamento,
			 tblcrm_acompanhamento_agenda.fldHora AS HoraAgendamento,
			 tblcliente.fldNome AS ClienteNome,
			 tblcliente.fldNomeFantasia AS ClienteNomeFantasia,
			 tblfuncionario.fldNome AS FuncionarioNome
			 FROM tblcrm_acompanhamento_cliente_chamado
			 
			 INNER JOIN tblcrm_acompanhamento_cliente_prioridade ON tblcrm_acompanhamento_cliente_prioridade.fldId = tblcrm_acompanhamento_cliente_chamado.fldPrioridade_Id
			 INNER JOIN tblcrm_acompanhamento_cliente_situacao ON tblcrm_acompanhamento_cliente_situacao.fldId = tblcrm_acompanhamento_cliente_chamado.fldSituacao_Id
			 INNER JOIN tblcliente ON tblcliente.fldId = tblcrm_acompanhamento_cliente_chamado.fldCliente_Id
			 
			 LEFT JOIN tblcrm_acompanhamento_agenda ON tblcrm_acompanhamento_agenda.fldChamado_Id = tblcrm_acompanhamento_cliente_chamado.fldId AND tblcrm_acompanhamento_agenda.fldId = (SELECT fldId FROM tblcrm_acompanhamento_agenda WHERE fldChamado_Id = tblcrm_acompanhamento_cliente_chamado.fldId AND fldExcluido = 0 ORDER BY fldData DESC, fldHora DESC LIMIT 1)
			 LEFT JOIN tblcrm_acompanhamento_cliente_tipo ON tblcrm_acompanhamento_cliente_tipo.fldId = tblcrm_acompanhamento_agenda.fldTipo_Id AND tblcrm_acompanhamento_cliente_tipo.fldExcluido = 0
			 LEFT JOIN tblfuncionario ON tblfuncionario.fldId = tblcrm_acompanhamento_agenda.fldFuncionario_Id
			 
			 WHERE tblcrm_acompanhamento_cliente_chamado.fldExcluido = 0
			 ". $_SESSION['filtro_acompanhamento'] ."
			 ORDER BY tblcrm_acompanhamento_cliente_chamado.fldSituacao_Id DESC, " . $_SESSION['order_acompanhamento'] . ", tblcrm_acompanhamento_agenda.fldHora ASC";
	//echo $sSQL;
	
/**************************** PAGINA��O *******************************************/
	$_SESSION['acompanhamento_relatorio'] = $sSQL;
	
	//defini��o dos limites
	$rowsTotal  = mysql_num_rows(mysql_query($sSQL));
	$limite 	= 50;
	$n_paginas 	= 7;

	$total_paginas = ceil($rowsTotal / $limite);
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL 	 		 .= " LIMIT " . $inicio . "," . $limite;
	$rsAcompanhamento = mysql_query($sSQL);
	echo mysql_error();
	$pagina   		  = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
	#####################################################################################
?>
    <form class="table_form" id="frm_acompanhamento_cliente" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
					<li class="order" style="width: 20px;">
                        <a <?= ($filtroOrder == 'fldPrioridade_Id') ? print "class='$class'" : '' ?> style="width:20px; background: none; padding: 0;" href="<?=$raiz?>prioridade">
                            <span style="display: block; margin: 4px auto 0 auto; width: 10px; height: 10px; border-radius: 100%; background: #999;" title="Prioridade"></span>
                        </a>
					</li>
					<li class="order">
						<a <?= ($filtroOrder == 'DataAgendamento') 	? "class='$class'" 	: '' ?> style="width:80px" href="<?=$raiz?>data">Agendamento</a>
					</li>
                    <li class="order">
                    	<a <?= ($filtroOrder == 'ClienteNome') 		? "class='$class'" 	: '' ?> style="width:180px;" href="<?=$raiz?>cliente">Cliente</a>
                    </li>
					<li class="order">
						<a <?= ($filtroOrder == 'fldTipo_Nome') 	? "class='$class'" 	: '' ?> style="width:65px" href="<?=$raiz?>tipo">Tipo</a>
					</li>
					<li class="order">
						<a <?= ($filtroOrder == 'fldSituacao_Nome') ? "class='$class'" 	: '' ?> style="width:50px" href="<?=$raiz?>situacao">Situa&ccedil;&atildeo</a>
					</li>
					<li class="order">
						<a <?= ($filtroOrder == 'FuncionarioNome') 	? "class='$class'" 	: '' ?> style="width:135px;" href="<?=$raiz?>funcionario">Funcion&aacute;rio</a>
					</li>
					<li style="width:210px;">Descri&ccedil;&atilde;o</li>
					<li class="order">
                    	<a <?= ($filtroOrder == 'fldId') 			? "class='$class'" 		: '' ?> style="width:35px; text-align:left;" href="<?=$raiz?>ticket">Ticket</a>
                    </li>
					<li style="width:20px;"></li>
                    <li style="width:15px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de acompanhamentos/agendamentos com clientes">
                	<tbody>
<?
						$linha = "row";
						$n 	   = 0;
						$rows  = mysql_num_rows($rsAcompanhamento);
						
						while($rowAcompanhamento = mysql_fetch_array($rsAcompanhamento)) {
							$id_array[$n] = $rowAcompanhamento['fldId'];
							$n++;
							
							//alterar a cor da linha levando em conta a data de agendamento
							if((!is_null($rowAcompanhamento['DataAgendamento']) && $rowAcompanhamento['DataAgendamento'] <= date('Y-m-d'))) {
								$rowStyle = ' style="background: #ffff95;"';
							}
							else{
								$rowStyle = null;
							}
?>
							<tr class="<?= $linha; ?>"<?=$rowStyle;?>>
								<td style="width: 20px;">
									<span style="cursor: help; display: block; margin: 0 auto; width: 10px; height: 10px; border-radius: 100%; background: <?=$rowAcompanhamento['fldPrioridade_Cor'];?>" title="Prioridade: <?=$rowAcompanhamento['fldPrioridade_Nome'];?>"></span>
								</td>
								<td	style="width:95px; text-align:center;">
									<?=format_date_out3($rowAcompanhamento['DataAgendamento']);?>
									<?=(!empty($rowAcompanhamento['HoraAgendamento'])) ? ' - ' . date('H:i', strtotime($rowAcompanhamento['HoraAgendamento'])) : '';?>
								</td>
								<td style="width:195px; text-align:left;"><?=$rowAcompanhamento['ClienteNome'];?></td>
								<td	style="width:80px; text-align:center;"><?=$rowAcompanhamento['fldTipo_Nome'];?></td>
								<td	style="width:65px; text-align:center;"><?=$rowAcompanhamento['fldSituacao_Nome'];?></td>
								<td style="width:150px; text-align:left;"><?=$rowAcompanhamento['FuncionarioNome'];?></td>
								<td style="width:210px; text-align:left;" title="<?=htmlentities($rowAcompanhamento['fldDescricao'], ENT_QUOTES, 'UTF-8')?>"><?=limitarTexto($rowAcompanhamento['fldDescricao'], 100);?></td>
								<td style="width:50px; text-align: center;"><?=str_pad($rowAcompanhamento['fldId'], 6, '0', STR_PAD_LEFT);?></td>
                  	       		<td style="width:20px;"><a class="edit" href="index.php?p=crm_acompanhamento_cliente_detalhe&id=<?=$rowAcompanhamento['fldId']?>" title="Editar" style="margin-top: 2px;"></a></td>
								<td style="width:15px"><input type="checkbox" name="chk_chamado_<?=$rowAcompanhamento['fldId']?>" id="chk_chamado_<?=$rowAcompanhamento['fldId']?>" title="selecionar o registro posicionado" /></td>
                    	    </tr>
<?							$linha = ($linha == "row" )?"dif-row":"row";
						}
?>			 		</tbody>
				</table>
            </div>
            
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="index.php?p=crm_acompanhamento_cliente_novo" title="novo">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" value="excluir" title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=crm_acompanhamento_cliente";
				include("paginacao.php");
?>		
            </div>   
            <div class="table_registro">
                <span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>   
        </div>
       
	</form>