<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Estoque</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
        
	</head>
	<body>
   		<div id="no-print">
            <ul style="width:800px; display:inline">
                <li><a class="print" href="#" onClick="window.print()">imprimir</a></li>
            </ul>
        </div>

<?		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados		= mysql_fetch_array(mysql_query("SELECT * FROM tblempresa_info"));
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rowUsuario		= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		$compra = (isset($_POST['chk_valor_compra'])) ? true : false;

		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Compras</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border-top: 1px solid">
				<td style="width:75px;margin-right:10px; text-align:right; font-weight:bold">C&oacute;d Venda</td>
				<td style="width:590px; font-weight:bold">Produto</td>
				<td style="width:91px; font-weight:bold">Valor</td>
				<td style="width:34px; font-weight:bold">Qtd</td>
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
		##########################################################################################################################################################################################
			
			$sSQL 			= $_SESSION['compra_item_consulta'];
							  
			$rsEstoque  	= mysql_query($sSQL);
			echo mysql_error();

			$rowsEstoque 	= mysql_num_rows($rsEstoque);
		
			$n	 			= 1; #DEFINE O NUMERO DA DO BLOCO
			$countRegistro  = 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE DE 20
			$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
			$limite 		= 45;
			
			$pgTotal 		= ceil($rowsEstoque / $limite);
			$p = 1;

			$total_compra 	= 0;
			$total_itens	= 0;
			while($rowEstoque = mysql_fetch_array($rsEstoque)){
				echo mysql_error();
				$pagina[$n] .='
				<tr>
                	<td style="width:75px; text-align:right;">'.str_pad($rowEstoque['CompraId'], 6, "0", STR_PAD_LEFT).'</td>
					<td style="width:540px;">'.substr($rowEstoque['CompraItem'],0,100).'</td>
					<td style="width:70px; text-align:right;">'.format_number_out($rowEstoque['CompraValor']).'</td>
					<td style="width:75px; text-align:right;">'.format_number_out($rowEstoque['CompraQtd']).'</td>
				</tr>';
				
				
				#SE CHEGAR A 20 LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
				if($countRegistro == $limite){
					$countRegistro = 1;
					$n ++;
				}elseif($rowsEstoque == $x && $countRegistro < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU 20 LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
					while($countRegistro <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countRegistro++;}
				}else{
					$countRegistro ++;
				}
				$x ++;
			}
			
		#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
			print $tabelaCabecalho1;
			print '<td style="width: 200px"><p class="pag">'.$p.' de '.$pgTotal.'</p></td>';
			print $tabelaCabecalho2;
			print $tabelaCabecalho3;
			echo  $pagina[$x]; ?>
						</table>
					</td >
				</tr>
			</table>
<?			$x ++;
			$p ++;
		}
?>

	</body>
</html>