<div id="voltar">
    <p><a href="index.php?p=<?=$_GET['documento']?>">n&iacute;vel acima</a></p>
</div>	

<h2>Importar Venda(s) para a Nota Fiscal Eletr&ocirc;nica</h2>
<div id="principal">
<?	$usuario_id 				= $_SESSION['usuario_id'];
	$verificar_limite_credito 	= $_SESSION["sistema_venda_alerta_limite_credito"];
	$venda_decimal 				= fnc_sistema('venda_casas_decimais');
	$qtde_decimal 				= fnc_sistema('quantidade_casas_decimais');
	$tipo_ambiente				= fnc_sistema('nfe_ambiente_tipo');

	//importando vendas
	$id_query_array = array();
	
	if(isset($_POST['hid_array']) || isset($_GET['operacao'])) {
		#DEFINE NATUREZA DA OPERACAO DE ACORDO COM ORIGEM DA TELA
		$rsPerfil  = mysql_query('SELECT * FROM tblnfe_perfil');
		$rowPerfil = mysql_fetch_array($rsPerfil);
		$nfe_natureza_operacao_id 	= (isset($_GET['operacao'])) ? $_GET['operacao'] : $rowPerfil['fldNatureza_Operacao_Id'];

		$rsNatureza_Operacao 		= mysql_query("SELECT * FROM tblnfe_natureza_operacao WHERE fldId = $nfe_natureza_operacao_id");
		$rowNatureza_Operacao 		= mysql_fetch_assoc($rsNatureza_Operacao);
		$nfe_natureza_operacao		= $rowNatureza_Operacao['fldNatureza_Operacao'];
		$nfe_parcelamento_exibir 	= $rowNatureza_Operacao['fldParcelas'];
		
		#DEFININDO TIPO DE DOCUMENTO (ENTRADA-SAIDA)		
		if($_GET['documento'] == 'compra'){
			switch($nfe_natureza_operacao_id){
				case 5: 
					$nfe_operacao_tipo = '0';
				break;
				case 2:
				case 6:
				case 7:
					$nfe_operacao_tipo = '1';
				break;
			}
		}else{
			switch($nfe_natureza_operacao_id){
				case 1: 
				case 3: 
				case 4: 
				case 5: 
					$nfe_operacao_tipo = '1';
				break;
				case 2:
				case 6:
				case 7:
					$nfe_operacao_tipo = '0';
				break;
				
			}
		}#end if compra ou venda
		
		
		###############################################################################################################################################
		#PRA SABER SE HE IMPORTA��O COMUM OU DEVOLUCAO ETC
		$nfe_importar = (isset($_POST['hid_array'])) ? 'true' : '';
		#CASO SEJA IMPORTACAO DE VENDAS
		$id_array = unserialize(urldecode($_POST["hid_array"]));
		$n = 0;
		if(isset($_POST['hid_array'])){
			while($id_array[$n]){
				if($_POST['chk_pedido_importar_'.$id_array[$n]]){
					$id_query_array[$n] = $id_array[$n];
				}
				$n++;
			}
			//ordenando a matriz
			asort($id_query_array);
		}elseif(isset($_GET['id'])){
			$id_query_array[0] = $_GET['id'];
		}#end if post array

		
		if($_GET['documento'] == 'compra'){
			$documento_tipo = 2;
			$rsSQL = mysql_query("SELECT * FROM tblcompra WHERE fldId IN (" . join(",", $id_query_array) . ") AND fldExcluido = 0");
			echo mysql_error();
		}else{
				
			$documento_tipo = 1;
			$rsSQL = mysql_query("SELECT * FROM tblpedido WHERE fldId IN (" . join(",", $id_query_array) . ") AND fldExcluido = 0 ");
			echo mysql_error();
			if($_SESSION["sistema_tipo"] == "otica"){
				$rowOtica = mysql_fetch_array(mysql_query("SELECT * FROM tblpedido_otica WHERE fldPedido_Id IN (" . join(",", $id_query_array) . ")"));
			}
		}
		
		if(!mysql_num_rows($rsSQL)){
?>			<div class="alert">
				<p class="erro">Houve um erro ao encontrar essa venda, ela pode ter sido exclu&iacute;da!<p>
       	 	</div>
<?			die;
		}else{
			$rowSQL = mysql_fetch_array($rsSQL);
		}		
	}
	#############################################################################################################################################################################
	
	//a��es ao gravar venda
	if($_SERVER['REQUEST_METHOD'] == 'POST' and !isset($_POST['btn_importar_vendas'])) {
		
		//antes de seguir em frente com o processamento das vendas que ser�o convertidas em NFe, � feito o tratamento de n�meros de NFe ausentes, caso exista!
		if(isset($_POST['hid_controle_nfe_numero_perdido_enviar'])) {
			
			if(!is_null($_POST['rad_nnfe_n_perdido'])) { $numeroAusenteNFe = $_POST['rad_nnfe_n_perdido']; }
			
			$sql  = "INSERT INTO tblpedido_nfe_numero_inutilizado(fldNFe_Numero, fldMotivo, fldObservacao, fldData, fldHora, fldUsuario_Id) VALUES";
			$data = date('Y-m-d');
			$hora = date('H:i:s');
			$user = $_SESSION['usuario_id'];
			
			$exec_sql  = false;
			$limite    = $_POST['hid_controle_qtd_numeros'];
			for($x=1;$x<=$limite;$x++) {
				
				if(isset($_POST['sel_nnfe_nao_utilizar_'.$x]) && $_POST['sel_nnfe_nao_utilizar_'.$x] > 0) {
					
					$numero = $_POST['hid_nfe_numero_'.$x];
					$motivo = $_POST['sel_nnfe_nao_utilizar_'.$x];
					$observacao 	= $_POST['txt_nnfe_observacao_'.$x];
					$sql 	.= "('{$numero}', '{$motivo}', '{$observacao}', '{$data}', '{$hora}', '{$user}'),";
					$exec_sql = true;
				}
			}
			if($exec_sql == true) {
				//retirar a �ltima v�rgula
				$sql = substr($sql, 0, -1);
				if(!mysql_query($sql)) { echo mysql_error(); exit(); }
			}
			unset($sql, $data, $hora, $user, $exec_sql, $limite, $x, $numero, $motivo, $observacao);
		}
		#######################################################################################################################################################################
		$documento_tipo		= $_POST['hid_documento_tipo'];
		$destinatario_id	= $_POST['hid_cliente_id'];
		$nfe_importar 		= $_POST['hid_nfe_importar'];
		
		$veiculo_id 		= $_POST['sel_veiculo']; //automotivo
		$veiculo_km			= $_POST['txt_veiculo_km']; //automotivo
		$dependente_id 		= ($_POST['hid_dependente_id'] > 0 ) ? $_POST['hid_dependente_id'] : "NULL" ;
		$conta_bancaria_id	= $_POST['sel_conta_bancaria']; //financeira
		$desconto 			= format_number_in($_POST['txt_pedido_desconto']);
		$desconto_reais 	= format_number_in($_POST['txt_pedido_desconto_reais']);
		$observacao 		= mysql_escape_string($_POST['txt_pedido_obs']);
		
		$data_emissao 		= format_date_in($_POST['txt_pedido_data']);
		$marcador 			= $_POST['sel_marcador'];
		$cliente_pedido		= $_POST['txt_cliente_pedido'];
		
		$valor_terceiros	= format_number_in($_POST['txt_pedido_terceiros']);
		$valor_servicos		= $_POST['txt_pedido_servicos'];
		$faturado			= ($_POST['chk_faturada'] == 'faturada') ? 1 : 0;  //0 PARA  NAO FATURADO 1 PARA FATURADO
		$parcela_exibir 	= $_POST['hid_parcela_exibir'];
		
		#FUNCAO PARA INSERIR  REGISTRO NA TABELA CORRETA - VENDA OU COMPRA
		$documento_id = fnc_nfe_documento_novo($documento_tipo, $destinatario_id, $desconto, $desconto_reais, $observacao, $data_emissao, $marcador, $valor_terceiros, $faturado,
								$veiculo_id, $veiculo_km, $dependente_id, $conta_bancaria_id, $cliente_pedido);
		
		if($documento_id) {
			
			if($_SESSION["sistema_tipo"]=="otica"){   
				$responsavel = $_POST['sel_cliente_responsavel'];
				mysql_query("INSERT INTO tblpedido_otica (fldPedido_Id, fldResponsavel) VALUES ($documento_id, $responsavel)");
			}
			
			##CASO TENHA VALOR DE SERVICO, DEVE ARMAZENAR TB, APENAS VALOR
			if($valor_servicos > 0){
				$valor_servicos = format_number_in($valor_servicos);
				$insertServico	= "INSERT INTO tblpedido_funcionario_servico (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldFuncao_Tipo)
									VALUES ('$documento_id', '', '', '', '$valor_servicos', '', '2')";
				mysql_query($insertServico);
			}
			// DADOS NFE *************************************************************************************************************************************************************************************************************************************************************************************************
			//nfe
			$regime								= fnc_nfe_regime();
			$nfe_serie							= fnc_sistema('nfe_serie');
			$nfe_numero							= (isset($numeroAusenteNFe)) ? $numeroAusenteNFe : fnc_sistema('nfe_numero_nota');
			$nfe_operacao_natureza_id			= $_POST['hid_pedido_natureza_operacao_id'];
			if($nfe_operacao_natureza_id == '2'){
				$campo_devolucao				= 'fldDocumento_Devolucao_Id,';
				$campo_devolucao_valor			= $_POST['hid_id_vendas_origens'].',';
			}
			$nfe_operacao_tipo					= $_POST['sel_pedido_operacao_tipo'];
			$nfe_saida_data						= format_date_in($_POST['txt_pedido_saida_data']);
			$nfe_saida_hora						= $_POST['txt_pedido_saida_hora'];
			$nfe_pagamento_forma_codigo			= $_POST['sel_pedido_nfe_pagamento_forma'];
				
			$nfe_total_produtos					= format_number_in($_POST['txt_pedido_nfe_total_produtos']);
			$nfe_total_bc_icms					= format_number_in($_POST['txt_pedido_nfe_total_bc_icms']);
			$nfe_total_icms						= format_number_in($_POST['txt_pedido_nfe_total_icms']);
			$nfe_total_bc_icms_substituicao		= format_number_in($_POST['txt_pedido_nfe_total_bc_icms_substituicao']);
			$nfe_total_icms_substituicao		= format_number_in($_POST['txt_pedido_nfe_total_icms_substituicao']);
			$nfe_total_frete					= format_number_in($_POST['txt_pedido_nfe_total_frete']);
			$nfe_total_seguro					= format_number_in($_POST['txt_pedido_nfe_total_seguro']);
			$nfe_total_desconto					= format_number_in($_POST['txt_pedido_nfe_total_desconto']);
			$nfe_total_valor_ipi				= format_number_in($_POST['txt_pedido_nfe_total_ipi']);
				
			$nfe_total_valor_ii					= format_number_in($_POST['txt_pedido_nfe_total_ii']);
			$nfe_total_valor_pis				= format_number_in($_POST['txt_pedido_nfe_total_pis']);
			$nfe_total_valor_cofins				= format_number_in($_POST['txt_pedido_nfe_total_cofins']);
			$nfe_out_despesas_acessorias		= format_number_in($_POST['txt_pedido_nfe_outras_despesas_acessorias']);
			$nfe_total_valor_nota				= format_number_in($_POST['txt_pedido_nfe_total_nfe']);
			$nfe_total_tributo					= format_number_in($_POST['txt_pedido_nfe_total_tributos']);
			
			$nfe_total_bc_iss					= format_number_in($_POST['txt_pedido_nfe_total_bc_iss']);
			$nfe_total_iss						= format_number_in($_POST['txt_pedido_nfe_total_iss']);
			$nfe_total_pis_servicos				= format_number_in($_POST['txt_pedido_nfe_total_pis_servicos']);
			$nfe_total_cofins_servicos			= format_number_in($_POST['txt_pedido_nfe_total_cofins_servicos']);
			$nfe_total_icms_nao_incidencia		= format_number_in($_POST['txt_pedido_nfe_total_servicos_nao_incidencia_icms']);
			
			$nfe_total_retido_pis				= format_number_in($_POST['txt_pedido_nfe_total_retido_pis']);
			$nfe_total_retido_cofins			= format_number_in($_POST['txt_pedido_nfe_total_retido_cofins']);
			$nfe_total_retido_csll				= format_number_in($_POST['txt_pedido_nfe_total_retido_csll']);
			$nfe_total_bc_irrf					= format_number_in($_POST['txt_pedido_nfe_total_bc_irrf']);
			$nfe_total_retido_irrf				= format_number_in($_POST['txt_pedido_nfe_total_retido_irrf']);
			$nfe_total_bc_retencao_prev_social	= format_number_in($_POST['txt_pedido_nfe_bc_retencao_prev_social']);
			$nfe_total_retencao_prev_social		= format_number_in($_POST['txt_pedido_nfe_total_retencao_prev_social']);
			
			$nfe_entrega_endereco				= $_POST['txt_pedido_nfe_entrega_endereco'];
			$nfe_entrega_numero					= $_POST['txt_pedido_nfe_entrega_numero'];
			$nfe_entrega_complemento			= $_POST['txt_pedido_nfe_entrega_complemento'];
			$nfe_entrega_bairro					= $_POST['txt_pedido_nfe_entrega_bairro'];
			$nfe_entrega_cep					= $_POST['txt_pedido_nfe_entrega_cep'];
			$nfe_entrega_municipio_codigo		= $_POST['txt_municipio_codigo_pedido_nfe'];
			$nfe_entrega_cpf_cnpj 				= $_POST['txt_pedido_nfe_entrega_cpf_cnpj'];
			$nfe_entrega_diferente_destinatario = $_POST['chk_local_entrega_diferente'];
			
			$nfe_transporte_modalidade			= $_POST['sel_pedido_nfe_transporte_modalidade'];
			$nfe_transportador					= $_POST['sel_pedido_nfe_transportador'];
			$nfe_transporte_icms_isento			= ($_POST['chk_nfe_transporte_icms_isento'] == true)? '1' : '0';
			
			$nfe_transporte_veiculo_placa		= $_POST['txt_pedido_nfe_transporte_veiculo_placa'];
			$nfe_transporte_veiculo_uf			= $_POST['sel_pedido_nfe_transporte_veiculo_uf'];
			$nfe_transporte_veiculo_rntc		= $_POST['txt_pedido_nfe_transporte_veiculo_rntc'];
			$nfe_transporte_balsa				= $_POST['txt_pedido_nfe_transporte_balsa_identificacao'];
			$nfe_transporte_vagao				= $_POST['txt_pedido_nfe_transporte_vagao_identificacao'];
			
			$nfe_retencao_bc					= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_bc']);
			$nfe_retencao_aliquota				= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_aliquota']);
			$nfe_retencao_valor_servico			= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_valor_servico']);
			$nfe_retencao_municipio_codigo		= $_POST['sel_pedido_nfe_transporte_retencao_uf'].$_POST['sel_pedido_nfe_transporte_retencao_municipio'];
			$nfe_retencao_cfop					= $_POST['sel_pedido_nfe_transporte_retencao_cfop'];
			$nfe_retencao_icms_retido			= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_icms_retido']);
			$nfe_info_fisco						= $_POST['txt_pedido_nfe_fisco'];
			$nfe_info_contribuinte				= $_POST['txt_pedido_nfe_contribuite'];
			
			$sqlInsert_pedido_fiscal = "INSERT INTO tblpedido_fiscal (
				fldpedido_id,
				fldOperacao_Tipo,
				fldregime_tributario_codigo,
				fldserie,
				fldambiente,
				fldnumero,
				fldDocumento_Tipo,
				$campo_devolucao
				fldsaida_data,
				fldsaida_hora,
				fldnatureza_operacao_id,
				fldpagamento_forma_codigo,
				fldprodutos_servicos_total,
				fldicms_base_calculo,
				fldicms_total,
				fldicmsst_base_calculo,
				fldicmsst_total,
				fldfrete_total,
				fldseguro_total,
				
				fldipi_total,
				fldii_total,
				fldpis_total,
				fldcofins_total,
				flddespesas_acessorias_total,
				flddesconto_total,
				fldnfe_total,
				fldnfe_total_tributos,
				
				fldiss_base_calculo,
				fldiss_total,
				fldpis_servicos_total,
				fldcofins_servicos_total,
				fldicms_nao_incidencia_total,	
				
				fldpis_retido_total,
				fldcofins_retido_total,
				fldcsll_retido_total,
				fldirrf_base_calculo,
				fldirrf_retido_total,
				fldretencao_prev_social_base_calculo,
				fldretencao_prev_social_total,
				
				fldentrega_endereco,
				fldentrega_numero,
				fldentrega_complemento,
				fldentrega_bairro,
				fldentrega_cep,
				fldentrega_municipio_codigo,
				fldentrega_cpf_cnpj,
				fldentrega_diferente_destinatario,
				
				fldtransporte_modalidade_codigo,
				fldtransporte_transportador_id,
				fldtransporte_icms_isento,
				
				fldtransporte_retencao_icms_base_calculo,
				fldtransporte_retencao_icms_aliquota,
				fldtransporte_retencao_icms_servico_valor,
				fldtransporte_retencao_icms_municipio_codigo,
				fldtransporte_retencao_icms_cfop,
				fldtransporte_retencao_icms_retencao_valor,
				
				fldtransporte_veiculo_placa,
				fldtransporte_veiculo_uf,
				fldtransporte_veiculo_rntc,
				fldtransporte_balsa_identificacao,
				fldtransporte_vagao_identificacao,
				
				fldinfo_adicionais_fisco,
				fldinfo_adicionais_contribuinte
			)
			VALUES(
				'$documento_id',
				'$nfe_operacao_tipo',
				'$regime',
				'$nfe_serie',
				'$tipo_ambiente',
				'$nfe_numero',
				'$documento_tipo',
				
				$campo_devolucao_valor
				
				'$nfe_saida_data',
				'$nfe_saida_hora',
				'$nfe_operacao_natureza_id',
			
				'$nfe_pagamento_forma_codigo',
				
				'$nfe_total_produtos',
				'$nfe_total_bc_icms',
				'$nfe_total_icms',
				'$nfe_total_bc_icms_substituicao',
				'$nfe_total_icms_substituicao',
				'$nfe_total_frete',
				'$nfe_total_seguro',
				
				'$nfe_total_valor_ipi',
				'$nfe_total_valor_ii',
				'$nfe_total_valor_pis',
				'$nfe_total_valor_cofins',
				'$nfe_out_despesas_acessorias',
				'$nfe_total_desconto',
				'$nfe_total_valor_nota',
				'$nfe_total_tributo',
				
				'$nfe_total_bc_iss',
				'$nfe_total_iss',
				'$nfe_total_pis_servicos',
				'$nfe_total_cofins_servicos',
				'$nfe_total_icms_nao_incidencia',
				
				'$nfe_total_retido_pis',
				'$nfe_total_retido_cofins',
				'$nfe_total_retido_csll',
				'$nfe_total_bc_irrf',
				'$nfe_total_retido_irrf',
				'$nfe_total_bc_retencao_prev_social',
				'$nfe_total_retencao_prev_social',	
				
				'$nfe_entrega_endereco',
				'$nfe_entrega_numero',
				'$nfe_entrega_complemento',
				'$nfe_entrega_bairro',
				'$nfe_entrega_cep',
				'$nfe_entrega_municipio_codigo',
				'$nfe_entrega_cpf_cnpj',
				'$nfe_entrega_diferente_destinatario',
				
				'$nfe_transporte_modalidade',
				'$nfe_transportador',
				'$nfe_transporte_icms_isento',
				
				'$nfe_retencao_bc',
				'$nfe_retencao_aliquota',
				'$nfe_retencao_valor_servico',
				'$nfe_retencao_municipio_codigo',
				'$nfe_retencao_cfop',
				'$nfe_retencao_icms_retido',
				
				'$nfe_transporte_veiculo_placa',
				'$nfe_transporte_veiculo_uf',
				'$nfe_transporte_veiculo_rntc',
				'$nfe_transporte_balsa',
				'$nfe_transporte_vagao',
				
				'$nfe_info_fisco',
				'$nfe_info_contribuinte'
			)";
			
			#ATUALIZA NUMERO DA PROX NOTA
			if(mysql_query($sqlInsert_pedido_fiscal)){
				if(!isset($numeroAusenteNFe)){
					$nfe_numero += 1;
					fnc_sistema_update('nfe_numero_nota', $nfe_numero);
				}
			}else{
				die(mysql_error());
			}
			
			
			//INSERINDO REBOQUES ***********************************************************************************************************************************************************************************************************************************************************************************
			$n = 1;
			$limite = $_POST["hid_reboque_controle"];
			while($n <= $limite){
				if($_POST['hid_reboque_controle'] > 0){
					
					$reboque_placa 	= $_POST['txt_pedido_nfe_transporte_reboque_placa_'.$n];
					$reboque_uf	 	= $_POST['hid_pedido_nfe_transporte_reboque_uf_'.$n];
					$reboque_rntc 	= $_POST['txt_pedido_nfe_transporte_reboque_rntc_'.$n];
					
					mysql_query("INSERT INTO tblpedido_fiscal_transporte_reboque
					(fldPedido_Id, fldPlaca, fldUF, fldRNTC) VALUES(
					'$documento_id', '$reboque_placa',
					'$reboque_uf','$reboque_rntc'
					)");
				}
				$n++;
			}
				
			//INSERINDO VOLUMES *************************************************************************************************************************************************************************************************************************************************************************************************
			$n = 1;
			$limite = $_POST["hid_volume_controle"];
			while($n <= $limite){
				if($_POST['hid_volume_controle'] > 0){
					
					$volume_qtd 		= format_number_in($_POST['txt_pedido_nfe_transporte_volume_qtd_'.$n]);
					$volume_especie 	= $_POST['txt_pedido_nfe_transporte_volume_especie_'.$n];
					$volume_marca 		= $_POST['txt_pedido_nfe_transporte_volume_marca_'.$n];
					$volume_nummero 	= $_POST['txt_pedido_nfe_transporte_volume_numero_'.$n];
					$volume_peso_liq 	= format_number_in($_POST['txt_pedido_nfe_transporte_volume_peso_liquido_'.$n]);
					$volume_peso_bruto 	= format_number_in($_POST['txt_pedido_nfe_transporte_volume_peso_bruto_'.$n]);
					$volume_lacres 		= $_POST['hid_pedido_nfe_transporte_volume_lacre_'.$n];
					
					mysql_query("INSERT INTO tblpedido_fiscal_transporte_volume
					(fldPedido_Id, fldQuantidade, fldEspecie, fldMarca, fldNumero, fldPeso_Liquido, fldPeso_Bruto)
					VALUES(
					'$documento_id',		'$volume_qtd',
					'$volume_especie',	'$volume_marca ',
					'$volume_nummero',	'$volume_peso_liq',
					'$volume_peso_bruto'
					)") or die (mysql_error());
					
					$rsLastId 		= mysql_query("SELECT last_insert_id() as lastId");
					$LastId 		= mysql_fetch_array($rsLastId);
					$volume_id 		= $LastId['lastId'];
					
					$volume_lacres = substr($volume_lacres, 0 ,strlen($volume_lacres) - 1);
					$volume_lacres = explode(',',$volume_lacres);
					foreach($volume_lacres as $lacre){
						
						mysql_query("INSERT INTO tblpedido_fiscal_transporte_volume_lacre
						(fldVolume_Id, fldLacre) VALUES(
						'$volume_id', '$lacre'
						)") or die (mysql_error());
					}
				}
				$n++;
			}
		
			#INSER��O DE ITENS COM ID DA NOTA NOVA, NO MOMENTO NAO VERIFICA O QUE FOI INSERIDO NOVO EM ESTOQUE
			$n 		= 1;
			$limite = $_POST["hid_controle"];
			while($n <= $limite) {
				
				$produto_id 			= $_POST['hid_item_produto_id_'.$n];
				$item_quantidade 		= format_number_in($_POST['txt_item_quantidade_'.$n]);
				$item_valor				= format_number_in($_POST['txt_item_valor_'.$n]);
				$item_desconto 			= $_POST['txt_item_desconto_'.$n];
				$item_nome 				= mysql_escape_string($_POST['txt_item_nome_'.$n]);
				$item_tabela_preco 		= ($_POST['hid_item_tabela_preco_'.$n] != '') ? $_POST['hid_item_tabela_preco_'.$n] : 'null' ;
				$item_fardo	 			= $_POST['hid_item_fardo_'.$n];
				$item_estoque_id		= $_POST['hid_item_estoque_id_'.$n];
				$item_entregue			= ($_POST['chk_entregue_'.$n] == true)? '1' : '0';
				$item_entregue_data		= $_POST['hid_pedido_item_entregue_data_'.$n];
				
				if(isset($produto_id)) {
					$rsDadosProduto		= mysql_query("SELECT fldEstoque_Controle, fldTipo_Id, fldValorCompra FROM tblproduto WHERE fldId = $produto_id ");
					$rowDadosProduto 	= mysql_fetch_array($rsDadosProduto);
					$valor_compra 		= $rowDadosProduto['fldValorCompra'];
					$estoque_controle	= $rowDadosProduto['fldEstoque_Controle'];
					$tipo_id 			= $rowDadosProduto['fldTipo_Id'];
					
					
					#FUNCAO PARA INSERIR ITEM NA TABELA CORRETA - VENDA OU COMPRA
					$item_id = fnc_nfe_inserir_item($documento_tipo, $documento_id, $produto_id, $item_quantidade, $item_nome, $item_valor, $item_desconto, $item_estoque_id,
										$valor_compra, $item_fardo, $item_tabela_preco,  $tipo_id, $item_entregue, $item_entregue_data);
										
					
					if(item_id) {
						
						//dados fiscais do item
						$NCM 		 							= $_POST['txt_nfe_pedido_item_principal_ncm_'.$n];
						$EXTIPI 	 							= $_POST['txt_nfe_pedido_item_principal_ex_tipi_'.$n];
						$CFOP 		 							= $_POST['txt_nfe_pedido_item_principal_cfop_'.$n];
						$UnidadeComercial 						= $_POST['txt_nfe_pedido_item_principal_unidade_comercial_'.$n];
						
						$QtdComercial 							= format_number_in($_POST['txt_item_quantidade_'.$n]); //ser� mesma que quantidade do item
						$ValorUnitComercial 					= format_number_in($_POST['txt_item_valor_'.$n]);//ser� mesma que valor do item
						$desconto			 					= format_number_in($_POST['txt_nfe_pedido_item_principal_desconto_'.$n]);//ser� mesma que valor do item
						$UnidadeTributavel 						= format_number_in($_POST['txt_nfe_pedido_item_principal_unidade_tributavel_'.$n]);
						$QtdTributavel 							= format_number_in($_POST['txt_nfe_pedido_item_principal_quantidade_tributavel_'.$n]);
						$ValorUnitTributavel 					= format_number_in($_POST['txt_nfe_pedido_item_principal_valor_unidade_tributavel_'.$n]);
						$TotalSeguro 							= format_number_in($_POST['txt_nfe_pedido_item_principal_total_seguro_'.$n]);
						
						$sub_total								= $item_valor * $item_quantidade;
						$total_desconto							= ($item_desconto / 100) * $sub_total;
						$total_item								+= $sub_total - $total_desconto;
						
						$Frete 									= format_number_in($_POST['txt_nfe_pedido_item_principal_frete_'.$n]);
						$EAN 									= format_number_in($_POST['txt_nfe_pedido_item_principal_ean_'.$n]);
						$EANTributavel 							= format_number_in($_POST['txt_nfe_pedido_item_principal_ean_tributavel_'.$n]);
						$OutrasDespesasAcessorias		 		= format_number_in($_POST['txt_nfe_pedido_item_principal_outras_despesas_acessorias_'.$n]);
						$ValorTotalBruto 						= format_number_in($_POST['txt_nfe_pedido_item_principal_valor_total_bruto_'.$n]);
						$PedidoCompra 							= format_number_in($_POST['txt_nfe_pedido_item_principal_pedido_compra_'.$n]);
						$NumeroItemPedidoCompra	 				= format_number_in($_POST['txt_nfe_pedido_item_principal_numero_item_pedido_compra_'.$n]);
						$ValorTotalBrutoChk 					= ($_POST['chk_nfe_pedido_item_principal_valor_total_bruto_'.$n] == true)? '1' : '0';
						
						$InformacoesAdicionais					= $_POST['txt_nfe_item_pedido_informacoes_adicionais_'.$n];
						$radImposto								= $_POST['hid_nfe_tributo_rad_'.$n];
						
						$ICMSSituacaoTributaria					= $_POST['hid_nfe_pedido_item_tributos_icms_situacao_tributaria_'.$n];
						$ICMSOrigem 							= $_POST['hid_nfe_pedido_item_tributos_icms_origem_'.$n];
						$ICMSAliquotaAplicavelCalculoCredito	= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pCredSN_'.$n]);
						$ICMSValorCreditoAproveitado			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_vCredICMSSN_'.$n]);
						
						$ICMSModalidadeDeterminacaoBC 			= format_number_in($_POST['hid_nfe_pedido_item_tributos_icms_modBC_'.$n]);
						$ICMSValorBC				 			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_vBC_'.$n]);
						$ICMSPorcentReducaoBC	 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pRedBC_'.$n]);
						$ICMSAliquota 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pICMS_'.$n]);
						$ICMSValor	 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_vICMS_'.$n]);
						$ICMSPorcentBCOperacaoPropria 			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pBCOp_'.$n]);
						$ICMSMotivoDesoneracao 					= $_POST['hid_nfe_pedido_item_tributos_icms_motDesICMS_'.$n];
						$ICMSSTModalidadeDetermicaoBC	 		= $_POST['hid_nfe_pedido_item_tributos_icms_st_modBCST_'.$n];
						$ICMSSTPorcentReducaoBC 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_pRedBCST_'.$n]);
						$ICMSSTPorcentMargemValorAdicional 		= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_pMVAST_'.$n]);
						$ICMSSTValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vBCST_'.$n]);
						$ICMSSTAliquota 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_pICMSST_'.$n]);
						$ICMSSTValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vICMSST_'.$n]);
							
						$ICMSSTUFDevido 						= $_POST['hid_nfe_pedido_item_tributos_icms_st_UFST_'.$n];
						
						$ICMSSTValorBCRetidoAnterior			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_'.$n]);
						$ICMSSTValorRetidoAnterior 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_'.$n]);
						$ICMSSTValorBCUFDestino					= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_'.$n]);
						$ICMSSTValorUFDestino					= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_'.$n]);
						
						$COFINSSituacaoTributaria	 			= $_POST['hid_nfe_pedido_item_tributos_cofins_situacao_tributaria_'.$n];
						$COFINSTipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_cofins_calculo_tipo_'.$n];
						$COFINSValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_'.$n]);
						$COFINSAliquotaPercentual 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_'.$n]);
						$COFINSAliquotaReais 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_aliquota_reais_'.$n]);
						$COFINSQtdVendida 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_'.$n]);
						$COFINSValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_valor_'.$n]);
						
						$COFINSSTTipoCalculo 					= $_POST['hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo_'.$n];
						$COFINSSTValorBC 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_'.$n]);
						$COFINSSTAliquotaPercentual 			= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_'.$n]);
						$COFINSSTAliquotaReais 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_'.$n]);
						$COFINSSTQtdVendida 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_'.$n]);
						$COFINSSTValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_valor_'.$n]);
						
						$IIValorBC 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_base_calculo_'.$n]);
						$IIValorDespesasAduaneiras 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_'.$n]);
						$IIValorIOF 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_iof_'.$n]);
						$IIValor 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_'.$n]);
						
						$IPISituacaoTributaria					= $_POST['hid_nfe_pedido_item_tributos_ipi_situacao_tributaria_'.$n];
						$IPIClasseEnquadramento 				= $_POST['txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_'.$n];
						$IPICodigoEnquadramentoLegal			= $_POST['txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_'.$n];
						$IPICNPJProdutor						= $_POST['txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_'.$n];
						$IPICodigoSeloControle 					= $_POST['txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_'.$n];
						$IPIQtdSeloControle 					= $_POST['txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_'.$n];
						$IPITipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_ipi_calculo_tipo_'.$n];
						$IPIValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_'.$n]);
						$IPIAliquota 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_aliquota_'.$n]);
						$IPIQtdTotalUnidadePadrao				= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_'.$n]);
						$IPIValorUnidade 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_unidade_valor_'.$n]);
						$IPIValor 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_valor_'.$n]);
						
						$ISSQNTributacao 						= $_POST['hid_nfe_pedido_item_tributos_issqn_tributacao_'.$n];
						$ISSQNValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_'.$n]);
						$ISSQNAliquota 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_issqn_aliquota_'.$n]);
						$ISSQNListaServico 						= $_POST['hid_nfe_pedido_item_tributos_issqn_servico_lista_'.$n];
						$ISSQNUF 								= $_POST['hid_nfe_pedido_item_tributos_issqn_uf_'.$n];
						$ISSQNMunicipioOcorrencia 				= $_POST['hid_nfe_pedido_item_tributos_issqn_municipio_'.$n];
						$ISSQNValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_issqn_valor_issqn_'.$n]);
								
						$PISSituacaoTributaria	 				= $_POST['hid_nfe_pedido_item_tributos_pis_situacao_tributaria_'.$n];
						$PISTipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_pis_calculo_tipo_'.$n];
						$PISValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_valor_base_calculo_'.$n]);
						$PISAliquotaPercentual 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_aliquota_percentual_'.$n]);
						$PISAliquotaReais 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_aliquota_reais_'.$n]);
						$PISQtdVendida 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_quantidade_vendida_'.$n]);
						$PISValor 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_valor_'.$n]);
						$PISSTTipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_pis_st_calculo_tipo_'.$n];
						$PISSTValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_'.$n]);
						$PISSTAliquotaPercentual 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_'.$n]);
						$PISSTAliquotaReais 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_'.$n]);
						$PISSTQtdVendida 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_'.$n]);
						$PISSTValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_valor_'.$n]);
						
						$Tipo_Especifico 						= $_POST['txt_nfe_item_produto_especifico_id_'.$n];
						$Combustivel_Cod_ANP 					= $_POST['txt_nfe_item_produto_especifico_combustivel_anp_'.$n];
						$Combustivel_Cod_IF 					= $_POST['txt_nfe_item_produto_especifico_combustivel_if_'.$n];
						$Combustivel_UF 						= $_POST['txt_nfe_item_produto_especifico_combustivel_uf_'.$n];
						$Combustivel_Qtd_Faturada 				= $_POST['txt_nfe_item_produto_especifico_combustivel_qtd_faturada_'.$n];
						$Combustivel_Cide_bCalculo 				= $_POST['txt_nfe_item_produto_especifico_combustivel_cide_bc_'.$n];
						$Combustivel_Cide_Aliquota 				= $_POST['txt_nfe_item_produto_especifico_combustivel_cide_aliquota_'.$n];
						$Combustivel_Cide_Valor 				= $_POST['txt_nfe_item_produto_especifico_combustivel_cide_valor_'.$n];
						
						$Total_Tributo							= $_POST['txt_nfe_item_produto_especifico_total_tributos_'.$n];
						
						
						$sqlInsertItemFiscal = "INSERT INTO tblpedido_item_fiscal (
							fldItem_Id,
							fldncm,
							fldex_tipi,
							fldcfop,
							fldunidade_comercial,
							fldquantidade_comercial,
							fldvalor_unitario_comercial,
							fldunidade_tributavel,
							fldquantidade_tributavel,
							fldvalor_unitario_tributavel,
							fldtotal_seguro,
							fldoutras_despesas_acessorias,
							fldfrete,
							flddesconto,
							fldean,
							fldean_unidade_tributavel,
							fldvalor_total_bruto,
							fldpedido_compra,
							fldnumero_item_pedido_compra,
							fldvalor_bruto_compoe_total,
							fldinformacoes_adicionais,
							fldrad_imposto,
							
							fldgenero,
							fldicms_origem,
							fldicms_situacao_tributaria,
							fldicms_base_calculo_modalidade,
							fldicms_calculo_credito_aliquota_aplicavel,
							fldicms_credito_aproveitado,
							
							fldicms_base_calculo_percentual_reducao,
							fldicms_aliquota,
							fldicms_base_calculo,
							fldicms_valor,
							fldicms_base_calculo_operacao_propria,
							fldicms_desoneracao_motivo,
							fldicmsst_base_calculo_modalidade,
							fldicmsst_base_calculo_percentual_reducao,
							fldicmsst_mva,
							fldicmsst_aliquota,
							fldicmsst_valor,
							fldicmsst_base_calculo,
							fldicmsst_base_calculo_retido_uf_remetente,
							fldicmsst_valor_retido_uf_remetente,
							fldicmsst_base_calculo_uf_destino,
							fldicmsst_valor_uf_destino,
							fldicmsst_uf,
							
							fldpis_situacao_tributaria,
							fldpis_tipo_calculo,
							fldpis_base_calculo,
							fldpis_aliquota_percentual,
							fldpis_aliquota_reais,
							fldpis_quantidade_vendida,
							fldpis_valor,
							fldpisst_tipo_calculo,
							fldpisst_base_calculo,
							fldpisst_aliquota_percentual,
							fldpisst_aliquota_reais,
							fldpisst_quantidade_vendida,
							fldpisst_valor,
							
							fldipi_situacao_tributaria,
							fldipi_enquadramento_classe,
							fldipi_enquadramento_codigo,
							fldipi_produtor_cnpj,
							fldipi_calculo_tipo,
							fldipi_aliquota,
							fldipi_unidade_quantidade_total,
							fldipi_unidade_valor,
							fldipi_selo_controle_codigo,
							fldipi_selo_controle_quantidade,
							fldipi_base_calculo,
							fldipi_valor,
							
							fldcofins_situacao_tributaria,
							fldcofins_tipo_calculo,
							fldcofins_base_calculo,
							fldcofins_aliquota_percentual,
							fldcofins_aliquota_reais,
							fldcofins_quantidade_vendida,
							fldcofins_valor,
							fldcofinsst_tipo_calculo,
							fldcofinsst_base_calculo,
							fldcofinsst_aliquota_percentual,
							fldcofinsst_aliquota_reais,
							fldcofinsst_quantidade_vendida,
							fldcofinsst_valor,
							
							fldii_base_calculo,
							fldii_despesas_aduaneiras_valor,
							fldii_iof_valor,
							fldii_valor,
							
							fldissqn_tributacao,
							fldissqn_base_calculo,
							fldissqn_aliquota,
							fldissqn_lista_servico,
							fldissqn_uf,
							fldissqn_municipio_ocorrencia,
							fldissqn_valor,
							
							fldTipo_Especifico,
							fldCombustivel_Codigo_ANP,
							fldCombustivel_Codigo_CodIF,
							fldCombustivel_UF,
							fldCombustivel_Qtd_Faturada,
							fldCombustivel_Cide_bCalculo,
							fldCombustivel_Cide_Aliquota,
							fldCombustivel_Cide_Valor,
							
							fldTotal_Tributo
						)
						VALUES(
							'$item_id',
							'$NCM',
							'$EXTIPI',
							'$CFOP',
							'$UnidadeComercial',
							'$QtdComercial',
							'$ValorUnitComercial',
							'$UnidadeTributavel',
							'$QtdTributavel',
							'$ValorUnitTributavel',
							'$TotalSeguro',
							'$OutrasDespesasAcessorias',
							'$Frete',
							'$desconto',
							'$EAN',
							'$EANTributavel',
							'$ValorTotalBruto',
							'$PedidoCompra',
							'$NumeroItemPedidoCompra',
							'$ValorTotalBrutoChk',
							'$InformacoesAdicionais',
							'$radImposto',
							
							'$genero',
							'$ICMSOrigem',
							'$ICMSSituacaoTributaria',
							'$ICMSModalidadeDeterminacaoBC',
							'$ICMSAliquotaAplicavelCalculoCredito',
							'$ICMSValorCreditoAproveitado',							
							'$ICMSPorcentReducaoBC',
							'$ICMSAliquota',
							'$ICMSValorBC',
							'$ICMSValor',
							'$ICMSPorcentBCOperacaoPropria',
							'$ICMSMotivoDesoneracao',
							
							'$ICMSSTModalidadeDetermicaoBC',
							'$ICMSSTPorcentReducaoBC',
							'$ICMSSTPorcentMargemValorAdicional',
							'$ICMSSTAliquota',
							'$ICMSSTValor',
							'$ICMSSTValorBC',
							'$ICMSSTValorBCRetidoAnterior',
							'$ICMSSTValorRetidoAnterior',
							'$ICMSSTValorBCUFDestino',
							'$ICMSSTValorUFDestino',
							'$ICMSSTUFDevido',
							'$PISSituacaoTributaria',
							'$PISTipoCalculo',
							'$PISValorBC',
							'$PISAliquotaPercentual',
							'$PISAliquotaReais',
							'$PISQtdVendida',
							'$PISValor',
							'$PISSTTipoCalculo',
							'$PISSTValorBC',
							'$PISSTAliquotaPercentual',
							'$PISSTAliquotaReais',
							'$PISSTQtdVendida',
							'$PISSTValor',
							'$IPISituacaoTributaria',
							'$IPIClasseEnquadramento',
							'$IPICodigoEnquadramentoLegal',
							'$IPICNPJProdutor',
							'$IPITipoCalculo',
							'$IPIAliquota',
							'$IPIQtdTotalUnidadePadrao',
							'$IPIValorUnidade',
							'$IPICodigoSeloControle',
							'$IPIQtdSeloControle',
							'$IPIValorBC',
							'$IPIValor',
							'$COFINSSituacaoTributaria',
							'$COFINSTipoCalculo',
							'$COFINSValorBC',
							'$COFINSAliquotaPercentual',
							'$COFINSAliquotaReais',
							'$COFINSQtdVendida',
							'$COFINSValor',
							'$COFINSSTTipoCalculo',
							'$COFINSSTValorBC',
							'$COFINSSTAliquotaPercentual',
							'$COFINSSTAliquotaReais',
							'$COFINSSTQtdVendida',
							'$COFINSSTValor',
							'$IIValorBC',
							'$IIValorDespesasAduaneiras',
							'$IIValorIOF',
							'$IIValor',
							'$ISSQNTributacao',
							'$ISSQNValorBC',
							'$ISSQNAliquota',
							'$ISSQNListaServico',
							'$ISSQNUF',
							'$ISSQNMunicipioOcorrencia',
							'$ISSQNValor',
							
							'$Tipo_Especifico',
							'$Combustivel_Cod_ANP',
							'$Combustivel_Cod_IF',
							'$Combustivel_UF',
							'$Combustivel_Qtd_Faturada',
							'$Combustivel_Cide_bCalculo',
							'$Combustivel_Cide_Aliquota',
							'$Combustivel_Cide_Valor',
							
							'$Total_Tributo'
						)";
						
						mysql_query($sqlInsertItemFiscal);
					}else{
						echo mysql_error();
						die();
					}
				}
				$n++;
			}
		}
		//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		##CASO NAO SEJA DEVOLUCAO, FAZ PARCELAS NORMALMENTE
		if($parcela_exibir == '1'){
			$y = 1;
			$limite = $_POST["hid_controle_parcela"];
			
			while($y <= $limite) {
				if(isset($_POST['txt_parcela_numero_'.$y])) {
					$parcela_numero 			 = $_POST['txt_parcela_numero_'.$y];
					$parcela_vencimento 		 = format_date_in($_POST['txt_parcela_data_'.$y]);
					$parcela_valor 				 = format_number_in($_POST['txt_parcela_valor_'.$y]);
					$parcela_valor_pago 		 = format_number_in($_POST['txt_parcela_valor_pago_'.$y]);
					$parcela_valor_pago_desconto = format_number_in($_POST['txt_parcela_valor_pago_desconto_'.$y]);
					$pagamento_tipo 			 = $_POST['sel_pagamento_tipo_'.$y];
					$parcela_obs				 = $_POST['txt_pedido_obs'];
					
					$tbl_parcela = ($documento_tipo == 2 )? 'tblcompra_parcela' : 'tblpedido_parcela';
					mysql_query("INSERT INTO $tbl_parcela 
					(fldPedido_Id, fldParcela, fldVencimento, fldValor, fldPagamento_Id, fldObservacao, fldStatus)
					VALUES(
					'$documento_id',
					'$parcela_numero',
					'$parcela_vencimento',
					'$parcela_valor',
					'$pagamento_tipo',
					'$parcela_obs',
					'1')");
					
					//descontar o valor do desconto para inserir na baixa
					$parcela_valor = $parcela_valor - $parcela_valor_pago_desconto;
					
					$LastId = mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID"));
					if($y == 1) {
						$LastIdPrimeiraParcela = $LastId['lastID'];
						if(isset($_POST['txt_parcela_valor_pago_1'])) {
							$valorParcelaPaga  = format_number_in($_POST['txt_parcela_valor_pago_1']);
						}
					}
					$data = date("Y-m-d");
					
					//ACOES DE PAGAMENTO CONFORME TIPO 
					$rowPagamentoTipo 	= mysql_fetch_array(mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldId = $pagamento_tipo"));
					$sigla 				= $rowPagamentoTipo['fldSigla'];
					echo mysql_error();
						
					$documento_parcela 	= ($documento_tipo == 2 )? 'compra_parcela' : 'pedido_parcela';
					$acao 		 		= fnc_sistema($documento_parcela.'_acao_'.$sigla);
					//verifica se � a vista, se for substitui o $acao
					if($data == $parcela_vencimento){
						$acao = fnc_sistema($documento_parcela."_acao_AV");
					}
						
					if($acao == 2 || ($y == 1 && $parcela_valor_pago > 0)) {
						
						$insertBaixa = mysql_query("INSERT INTO ".$tbl_parcela."_baixa
							(fldParcela_Id, fldDataCadastro, fldValor, fldDesconto, fldDataRecebido)
							VALUES(
							'".$LastId['lastID']."',
							'$data',
							'$parcela_valor',
							'$parcela_valor_pago_desconto',
							'$data'
						)");
						
						if($insertBaixa){// se nao for orcamento
							//lan�ar no caixa
							$conta 		 = fnc_sistema($documento_parcela."_conta_$sigla");
							$LastIdBaixa = mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID"));
							//$descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta
							//para n�o inserir novamente no caixa algo que j� foi pago!
							if($parcela_valor_pago <= 0) {
								if($documento_tipo == 2 ){ #compra
									fnc_financeiro_conta_fluxo_lancar('', 0, $parcela_valor, '', $pagamento_tipo, $LastIdBaixa['lastID'], 4, '', $conta);
								}else{ #venda
									fnc_financeiro_conta_fluxo_lancar('', $parcela_valor, 0, '', $pagamento_tipo, $LastIdBaixa['lastID'], 3, '', $conta);
								}
							}
						}
					}
				}
				$y++;
			}
			
			//inserir o id da parcela nas parcelas de origem, no caso das parcelas pagas
			if(isset($_POST['hid-parcelas-ids'])) {
				mysql_query("UPDATE $tbl_parcela SET fldParcela_Destino_Id = $LastIdPrimeiraParcela WHERE fldId IN (" . $_POST['hid-parcelas-ids'] . ")");
			}
			
			//inserir o id da nova venda nas vendas que originaram
			if(isset($_POST['hid_id_vendas_origens']) && $nfe_importar == true ) {
				mysql_query("UPDATE tblpedido SET fldPedido_Destino_Nfe_Id = $documento_id WHERE fldId IN (" . $_POST['hid_id_vendas_origens'] . ")");
			}
			
			//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			//cheques
			if(!empty($_SESSION['ref_timestamp'])){ //CASO A SESSAO ESTIVER EM BRANCO, PRA NAO ATUALIZAR TODOS OS CHEQUES 
				$timestamp = $_SESSION['ref_timestamp'];
				if($documento_tipo == 2 ){ #compra
					$destino_movimento_id = '4'; //pgamento de parcela
					$destino_id 		  = $documento_id;
					fnc_cheque_update('', $destino_id, '', '', $destino_movimento_id, $timestamp);
				}else{ #venda
					$origem_movimento_id = '3'; //recebimento de parcela
					$origem_id 			 = $documento_id;
					fnc_cheque_update($origem_id, '', '', $origem_movimento_id, '', $timestamp); }
					//fnc_cheque_update($origem_id, $destino_id, $entidade, $origem_movimento_id, $destino_movimento_id, $timestamp, $timestamp_update = NULL)
			}
			
			echo mysql_error();
			unset($_SESSION['ref_timestamp']);
		}#endif !parcela exibir
		else{ #CASO SEJA UMA NFE DEVOLUCAO, ESTORNA AS PARCELAS OU GERA UM CREDITO 
			if(isset($_POST['hid-parcelas-ids'])) {
				$parcela_paga_acao = $_POST['rad_parcela_paga'];
				if($parcela_paga_acao == 'estorno'){
					
					$rsParcelaEstorno 		 = mysql_query("SELECT fldId FROM ".$tbl_parcela."_baixa WHERE fldParcela_Id IN (".$_POST['hid-parcelas-ids'].") AND fldExcluido = 0");
					while($rowParcelaEstorno = mysql_fetch_assoc($rsParcelaEstorno)){
						$rsFluxo = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo WHERE fldReferencia_Id =".$rowParcelaEstorno['fldId']." AND fldMovimento_Tipo = 3");
						if(mysql_num_rows($rsFluxo) > 0){
							$rowFluxo = mysql_fetch_assoc($rsFluxo);
							echo mysql_error();
							
							$conta 			= $rowFluxo['fldConta_Id'];
							$pagamento_id 	= $rowFluxo['fldPagamento_Tipo_Id'];
							$descricao 		= "Estorno ID ".$rowFluxo['fldId']." ( ".format_date_out($rowFluxo['fldData'])." ".$rowFluxo['fldDescricao'].")";
							if($documento_tipo == 2 ){ #compra
								$baixa_valor 	= $rowFluxo['fldDebito'];
								//$descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta
								fnc_financeiro_conta_fluxo_lancar($descricao, $baixa_valor, 0, '', $pagamento_id, $rowFluxo['fldId'], 5, '', $conta);
							}else{
								$baixa_valor 	= $rowFluxo['fldCredito'];
								//$descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta
								fnc_financeiro_conta_fluxo_lancar($descricao, 0, $baixa_valor, '', $pagamento_id, $rowFluxo['fldId'], 5, '', $conta);
							}
						}
						mysql_query("UPDATE ".$tbl_parcela."_baixa SET fldExcluido = 1 WHERE fldId =".$rowParcelaEstorno['fldId']);

					echo mysql_error();
					}
				}elseif($parcela_paga_acao == 'credito'){
					if($documento_tipo == 2 ){ #compra
					
						$rsParcelasPagas  	= mysql_query("	SELECT fldCompra_Id, SUM(tblcompra_parcela_baixa.fldValor) as fldValor 
															FROM tblcompra_parcela INNER JOIN tblcompra_parcela_baixa ON tblcompra_parcela.fldId = tblcompra_parcela_baixa.fldParcela_Id
															WHERE tblcompra_parcela.fldId IN (". $_POST['hid-parcelas-ids'] .") AND tblcompra_parcela_baixa.fldExcluido = '0'
															GROUP BY fldCompra_Id");
						#WHILE POIS PODE SER MAIS DE UM ID DE VENDA/COMPRA
						while($rowParcelasPagas 	= mysql_fetch_assoc($rsParcelasPagas)){
							$documento_id  			= $rowParcelasPagas['fldCompra_Id'];
							$valor					= $rowParcelasPagas['fldValor'];
							mysql_query("INSERT INTO tblcompra_parcela (fldCompra_Id, fldValor, fldCredito) VALUES ('$documento_id', '$valor', '1')");
							echo mysql_error();
						}
					}else{
						$rsParcelasPagas  	= mysql_query("	SELECT fldPedido_Id, SUM(tblpedido_parcela_baixa.fldValor) as fldValor 
															FROM tblpedido_parcela INNER JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
															WHERE tblpedido_parcela.fldId IN (". $_POST['hid-parcelas-ids'] .") AND tblpedido_parcela_baixa.fldExcluido = '0'
															GROUP BY fldPedido_Id");
						while($rowParcelasPagas 	= mysql_fetch_assoc($rsParcelasPagas)){
							$documento_id  			= $rowParcelasPagas['fldPedido_Id'];
							$valor					= $rowParcelasPagas['fldValor'];
							mysql_query("INSERT INTO tblcompra_parcela (fldCompra_Id, fldValor, fldCredito) VALUES ('$documento_id', '$valor', '1')");
							echo mysql_error();
						}
					}
				}
			}
		}
		
		$documento_tipo = ($documento_tipo == '1') ? 'pedido' : 'compra';
		if(!mysql_error()) {
			header("location:index.php?p={$documento_tipo}&mensagem=ok");
		}else {
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
				<a class="voltar" href="index.php?p=<?=$_GET['documento']?>">voltar</a>
			</div>
<?			echo mysql_error();
		}
	}//if post
	else{
		
		$remote_ip = gethostbyname($REMOTE_ADDR);
		$_SESSION['ref_timestamp'] = $remote_ip.date("YmdHis");
	
        $rowUsuario = mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId = $usuario_id"));

?>		<div class="alert">
			<p class="alert">ATEN&Ccedil;&Atilde;O. Conferir dados fiscais dos itens importados!</p>
		</div>		
<?
		//PEGO ALGUMAS INFORMACOES DE PERFIL DA NOTA, SE HOUVER
		$rsPerfil  = mysql_query("SELECT * FROM tblnfe_perfil");
		$rowPerfil = mysql_fetch_array($rsPerfil);
		
		$serie 				= fnc_sistema('nfe_serie');
		$dataSaida			= date('Y-m-d');
		$saidaHora	 		= date('H:i:s');
		$data_pedido 		= date('Y-m-d');
		$perfil_natureza_operacao = $rowPerfil['fldNatureza_Operacao_Id'];
		$modFrete			= $rowPerfil['fldFrete_Id'];
		$transportador 		= $rowPerfil['fldTransportador_Id'];
		$infoContribuinte 	= $rowPerfil['fldInfo_Contribuinte'];
		$infoFisco		  	= $rowPerfil['fldInfo_Fisco'];
		$nfePagamento		= $rowPerfil['fldPagamento_Id'];
	
?>
        <div class="form">
            <form class="frm_detalhe gravar_nfe" style="width:890px" id="frm_pedido_nfe_novo" action="" method="post">
            	<input type="hidden" name="hid_documento_tipo" id="hid_documento_tipo" value="<?=$documento_tipo?>" />
                <input type="hidden" name="hid_nfe_importar" id="hid_nfe_importar" value="<?=$nfe_importar?>" />
                <ul>
                    <li>
                        <label for="txt_codigo">Id</label>
                        <input type="text" style="width:50px; text-align:right" id="txt_codigo" name="txt_codigo" readonly="readonly" value="<?=$rowSQL['fldId']?>"/>
                    </li>
                    <li>
                        <label for="txt_usuario">Usu&aacute;rio</label>
                        <input type="text" style="width:200px" id="txt_usuario" name="txt_usuario" value="<?=$rowUsuario['fldUsuario']?>" readonly="readonly"/>
                    </li>
                    <li>
                        <label for="sel_marcador">Marcador</label>
                        <select style="width:180px;" id="sel_marcador" name="sel_marcador" >
	                        <option value="0">Selecionar</option>
<?							$rsMarcador = mysql_query("SELECT * FROM tblpedido_marcador WHERE fldExcluido = 0 ORDER BY fldMarcador");
							while($rowMarcador = mysql_fetch_array($rsMarcador)){                            
?>                      	    <option <?=($rowSQL['fldMarcador_Id'] == $rowMarcador["fldId"]) ? 'selected="selected"' : '' ?> value="<?=$rowMarcador['fldId']?>"><?=$rowMarcador['fldMarcador']?></option>
<?							}
?>                 	 </select>
            		</li>
                    <ul>
                        <li>
                            <label for="txt_serie">S&eacute;rie</label>
                            <input type="text" style="width:50px;text-align:right" id="txt_serie" name="txt_serie" value="<?=$serie?>"/>
                        </li>
                        <li>
                            <label for="txt_pedido_numero">NFe</label>
                            <input type="text" style="width:60px;text-align:right" id="txt_pedido_numero" name="txt_pedido_numero" readonly="readonly" value="novo" />
                        </li>
                        <li>
                            <label for="txt_pedido_data">Data Emiss&atilde;o</label>
                            <input type="text" style="width:100px; text-align: center;" class="calendario-mask" id="txt_pedido_data" name="txt_pedido_data" value="<?=format_date_out($data_pedido)?>" />
                            <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                        </li>
                        <li>
                            <label for="txt_pedido_hora">Hora Emiss&atilde;o</label>
                            <input type="text" style="width:80px; text-align: right" id="txt_pedido_hora" name="txt_pedido_hora" value="<?=$saidaHora?>" readonly="readonly" />
                        </li>
                        <li>
                            <label for="txt_pedido_saida_data">Data Sa&iacute;da</label>
                            <input type="text" style="width:80px; text-align: center;" class="calendario-mask" id="txt_pedido_saida_data" name="txt_pedido_saida_data" value="<?=format_date_out($dataSaida)?>" />
                            <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                        </li>
                        <li>
                            <label for="txt_pedido_saida_hora">Hora Sa&iacute;da</label>
                            <input type="text" style="width:80px;text-align:right" id="txt_pedido_saida_hora" name="txt_pedido_saida_hora" value="<?=$saidaHora?>"/>
                        </li>
                    	<li>
                        	<label for="txt_pedido_natureza_operacao">Natureza da Opera&ccedil;&atilde;o</label>
                            <input type="text" 	 name="txt_pedido_natureza_operacao" 	id="txt_pedido_natureza_operacao" 		value="<?=$nfe_natureza_operacao?>" readonly="readonly" style="width:200px" class="disabled" />
							<input type="hidden" name="hid_pedido_natureza_operacao_id"	id="hid_pedido_natureza_operacao_id" 	value="<?=$nfe_natureza_operacao_id?>" />
							<input type="hidden" name="hid_parcela_exibir" 				id="hid_parcela_exibir" 				value="<?=$nfe_parcelamento_exibir?>" />
                   		</li>
                        <li>
                            <label for="sel_pedido_operacao_tipo">Tipo de documento</label>
                            <select class="sel_pedido_operacao_tipo" style="width:120px;" id="sel_pedido_operacao_tipo" name="sel_pedido_operacao_tipo" >
                                <option <?=($nfe_operacao_tipo == 0 )? "selected ='selected'" : '' ?> value="0">Entrada</option>
                                <option <?=($nfe_operacao_tipo == 1 )? "selected ='selected'" : '' ?> value="1">Sa&iacute;da</option>
                            </select>
                        </li>
                    </ul>
                </ul>
                <ul>
<?					
					if($documento_tipo == 2 ){ #compra
						$rowDestinatario = mysql_fetch_array(mysql_query("SELECT * FROM tblfornecedor WHERE fldId = ".$rowSQL['fldFornecedor_Id']));
						$destinatario_codigo = $rowDestinatario['fldId'];
						$destinatario_nome 	 = $rowDestinatario['fldRazaoSocial'];
					}else{ #venda
						$rowDestinatario = mysql_fetch_array(mysql_query("SELECT * FROM tblcliente WHERE fldId = ".$rowSQL['fldCliente_Id']));
						$destinatario_codigo = $rowDestinatario['fldCodigo'];
						$destinatario_nome 	 = $rowDestinatario['fldNome'];
					}
								
?>             		<li>
                        <label for="txt_cliente_codigo">Destinat&aacute;rio</label>
                        <input type="text" style="width:70px; text-align:right;" id="txt_cliente_codigo" name="txt_cliente_codigo" value="<?=$destinatario_codigo?>" readonly="readonly" />
                        <!-- COMO HE IMPORTACAO, NAO VEJO MOTIVO PARA ALTERAR O DESTINAT�RIO, CLIENTE/FORNECEDOR -->
                        <!-- <a href="cliente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a> -->
                    </li>
                    <li style="margin-right:500px">
                        <label for="txt_cliente_nome">&nbsp;</label>
                        <input type="text" style=" width:315px" id="txt_cliente_nome" name="txt_cliente_nome" readonly="readonly" value="<?=$destinatario_nome?>" disabled="disabled" />
                        <input type="hidden" id="hid_cliente_id" name="hid_cliente_id" value="<?=$rowDestinatario['fldId']?>" />
                    </li>
<?					if($_SESSION["exibir_dependente"] > 0 && $nfe_importar == 'true'){
?>                  	<li>
                            <label for="txt_dependente_codigo">Dependente</label>
                            <input type="text" style="width:70px; text-align:right" id="txt_dependente_codigo" name="txt_dependente_codigo" value="" />
                            <a href="dependente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                        </li>
                     
                        <li>
                            <label for="txt_dependente_nome">&nbsp;</label>
                            <input type="text"	 id="txt_dependente_nome" 	name="txt_dependente_nome" 	value="" style=" width:315px" readonly="readonly" />
                            <input type="hidden" id="hid_dependente_id" 	name="hid_dependente_id"	value="" />
                        </li>
<?                  }  
?>                     
					<li>
                 	   <label for="txt_cliente_pedido">N&ordm; Servi&ccedil;o</label>
                       <input type="text" style=" width:70px; text-align:right" name="txt_cliente_pedido" id="txt_cliente_pedido" value="<?=$rowSQL['fldCliente_Pedido']?>" readonly="readonly"  />
                    </li>
                    
<?					if($_SESSION["sistema_tipo"]=="automotivo" && count($id_query_array) == 1 && $nfe_importar == 'true'){
						$exibir = fnc_sistema('venda_exibir_veiculo');
						$rowVeiculo = mysql_fetch_array(mysql_query("SELECT fldId, fldPlaca, fldVeiculo FROM tblcliente_veiculo WHERE fldId = ".$rowSQL['fldVeiculo_Id']));
?>						<li>
                            <label for="sel_veiculo">Ve&iacute;culo</label>
                            <select class="sel_veiculo" style="width:200px" id="sel_veiculo" name="sel_veiculo" >
                            	<option selected="selected" value="<?=$rowVeiculo['fldId']?>"><?= ($exibir == 'placa') ? $rowVeiculo['fldPlaca'] : $rowVeiculo['fldVeiculo']." [".$rowVeiculo['fldPlaca']."]"?></option>
                            </select>
						</li>
                        <a class="modal" style="display:none" id="modal_veiculo" href="cliente_veiculo_cadastro_venda" rel="680-200" title=""></a>
                        <li style="width: 200px">
                            <label for="txt_veiculo_km">KM</label>
                            <input type="text" style="width:135px" id="txt_veiculo_km" name="txt_veiculo_km" value="<?=$rowSQL['fldVeiculo_Km']?>"/>
                        </li>
<?					}
					if($_SESSION["sistema_tipo"]=="otica" && $nfe_importar == 'true'){   
?>						
                        <li>
                           	<label for="sel_cliente_responsavel">Respons&aacute;vel</label>
                           	<select style="width:200px" id="sel_cliente_responsavel" name="sel_cliente_responsavel" >
                           		<option value="1" <?= ($rowOtica['fldResponsavel'] == '1')? "selected = 'selected'" : '' ?>></option>
                           		<option value="2" <?= ($rowOtica['fldResponsavel'] == '2')? "selected = 'selected'" : '' ?>></option>
                           	</select>
                        </li>
						<li>
						   	<label for="txt_pedido_entrega">Entrega</label>
                    		<input type="text" style=" width:70px" name="txt_pedido_entrega" id="txt_pedido_entrega" class="calendario-mask" value="<?=date('d/m/Y')?>" />
						</li>
<?					}
?>
	              	<li>
                 	   <label for="txt_pedido_obs">Observa&ccedil;&atilde;o</label>
                       <textarea style=" <?=($_SESSION["sistema_tipo"]=="otica")? 'width:480px;' : 'width:765px;' ?> height:30px" id="txt_pedido_obs" name="txt_pedido_obs"></textarea>
                    </li>
                </ul>
                
                <!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
               	
              	<div id="pedido_lista" style="width:960px">
                    <ul id="pedido_lista_cabecalho" style="width: 11110px">
<?						if($nfe_importar != 'true'){ 	
?>							<li style="width:20px">&nbsp;</li>
<?						}									?>	
                        <li style="width:20px">&nbsp;</li>
                        <li style="width:20px" class="entregue" title="entregue"></li>
                        <li style="width:45px">Origem</li>
                        <li style="width:80px">C&oacute;digo</li>
                        <li style="width:320px">Produto</li>
                        <li style="width:50px">Fardo</li>
                        <li style="width:55px">Tabela</li>
                        <li style="width:80px">Valor Un.</li>
                        <li style="width:50px">Qtde</li>
                        <li style="width:70px">Estoque</li>
                        <li style="width:30px">U.M.</li>
                        <li style="width:50px">Desc(%)</li>
                        <li style="width:70px">Subtotal</li>
                        
                        <li style="width:70px">NCM</li>
                        <li style="width:70px">EX TIPI</li>
                        <li style="width:70px">CFOP</li>
                        <li style="width:90px">Uni. Comercial</li>
						<li style="width:70px">Uni. Tribut.</li>
                        <li style="width:70px">Qtd. Tribut.</li>
                        <li style="width:110px">Valor Unit. Tribut.</li>
                        <li style="width:70px">Tot. Seguro</li>
                        <li style="width:85px">Desconto</li>
                        <li style="width:70px">Frete</li>
                        <li style="width:70px">EAN</li>
                        <li style="width:70px">EAN Tribut.</li>
                        <li style="width:120px">Out. Desp. Acess&oacute;rias</li>
                        <li style="width:90px">Val. Tot. Bruto</li>
                        <li style="width:70px">Ped. Compra</li>
                        <li style="width:110px">N&ordm; Item Ped. Compra</li>
                        <li style="width:110px">Val. Tot. Bruto</li>
                        
                        <li style="width:120px">ICMS Sit. Tribut.</li>
                        <li style="width:70px">Origem</li>
                        <li style="width:150px">Aliq. Aplic&aacute;vel Calc. Cr&eacute;d.</li>
                        <li style="width:150px">Cr&eacute;d. pode ser aproveitado</li>
                        <li style="width:150px">Modalid. Determ. BC ICMS</li>
                        <li style="width:80px">BC ICMS</li>
                        <li style="width:120px">% Red. BC ICMS</li>
                        
                        <li style="width:70px">Al&iacute;q. ICMS</li>
                        <li style="width:80px">ICMS</li>
                        <li style="width:150px">% BC Opera&ccedil;. Pr&oacute;pria</li>
                        <li style="width:150px">Motivo desonera&ccedil;&atilde;o ICMS</li>
                        <li style="width:180px">Modalid. Determ. BC ICMS ST</li>
                        
                        <li style="width:120px">% Redu&ccedil;. BC ICMS ST</li>
                        <li style="width:150px">% Marg. Val. Adic. ICMS ST</li>
                        <li style="width:100px">BC do ICMS ST</li>
                        <li style="width:100px">Al&iacute;q. ICMS ST</li>
                        <li style="width:80px">ICMS ST</li>
                        <li style="width:120px">UF Devido ICMS ST</li>
                        <li style="width:150px">BC ICMS retido ant.</li>
                        <li style="width:150px">ICMS retido ant.</li>
                        <li style="width:150px">BC BC UF dest.</li>
                        <li style="width:150px">BC ICMS UF dest.</li>
                        
                        <li style="width:120px">COFINS - Sit. Tribut.</li>
                        <li style="width:110px">COFINS - Tipo C&aacute;lc.</li>
                        <li style="width:100px">COFINS - Val. BC</li>
                        <li style="width:130px">COFINS - Al&iacute;q. Percent.</li>
                        <li style="width:130px">COFINS - Al&iacute;q. Reais</li>
                        <li style="width:120px">COFINS - Qtd. Vendida</li>
                        <li style="width:100px">COFINS Val.</li>
                        
                        <li style="width:120px">COFINS ST - Tipo C&aacute;lc.</li>
                        <li style="width:120px">COFINS ST - Val. BC</li>
                        <li style="width:140px">COFINS ST - Al&iacute;q. Percent.</li>
                        <li style="width:120px">COFINS ST - Al&iacute;q. Reais</li>
                        <li style="width:140px">COFINS ST - Qtd. Vendida</li>
                        <li style="width:120px">COFINS ST Val.</li>
                        
                        <li style="width:120px">II - Val. BC</li>
                        <li style="width:150px">II - Val. Desp. Aduaneiras</li>
                        <li style="width:70px">II - Val. IOF</li>
                        <li style="width:70px">II Val.</li>
                        
                        <li style="width:100px">IPI - Sit. Tribut.</li>
                        <li style="width:140px">IPI - Class. Enquadramento</li>
                        <li style="width:170px">IPI - C&oacute;d. Enquadramento Leg.</li>
                        <li style="width:120px">IPI - CNPJ Produtor</li>
                        <li style="width:140px">IPI - C&oacute;d. Selo Controle</li>
                        <li style="width:140px">IPI - Qtd. Selo Controle</li>
                        <li style="width:120px">IPI - Tipo C&aacute;lc.</li>
                        <li style="width:70px">IPI - Val. BC.</li>
                        <li style="width:120px">IPI - Al&iacute;quota</li>
                        <li style="width:150px">IPI - Qtd. Tot. Unid. Padr&atilde;o</li>
                        <li style="width:120px">IPI - Val. Unid.</li>
                        <li style="width:70px">IPI Val.</li>
                        
                        <li style="width:120px">ISSQN - Tributa&ccedil;&atilde;o</li>
                        <li style="width:120px">ISSQN - Val. BC</li>
                        <li style="width:120px">ISSQN - Al&iacute;quota</li>
                        <li style="width:140px">ISSQN - List. Servi&ccedil;o</li>
                        <li style="width:70px">ISSQN - UF</li>
                        <li style="width:140px">ISSQN - Mun. Ocorr&ecirc;ncia</li>
                        <li style="width:80px">ISSQN Val.</li>
                        
                        <li style="width:100px">PIS - Sit. Tribut.</li>
                        <li style="width:100px">PIS - Tipo Calc.</li>
                        <li style="width:100px">PIS - Val. BS Calc.</li>
                        <li style="width:110px">PIS - Al&iacute;q. Percent.</li>
                        <li style="width:100px">PIS - Al&iacute;q. Reais.</li>
                        <li style="width:100px">PIS - Qtd. Vendida</li>
                        <li style="width:60px">PIS Val.</li>
                        
                        <li style="width:120px">PIS ST - Tipo Calc.</li>
                        <li style="width:100px">PIS ST - Val. BC</li>
                        <li style="width:120px">PIS ST - Al&iacute;q. Percent.</li>
                        <li style="width:120px">PIS ST - Al&iacute;q. Reais.</li>
                        <li style="width:120px">PIS ST - Qtd. Vendida</li>
                        <li style="width:70px">PIS ST Val.</li>
                        <li style="width:200px">Info. Adicionais</li>
                        <li style="width:122px">Prod. Especifico</li>
                    </ul>
                    
<?
					if($documento_tipo == 2 ){
						$tbl_documento	= 'tblcompra';
						$item_where 	= 'tblcompra_item.fldCompra_Id';
					}else{
						$tbl_documento  = 'tblpedido';
						$item_where 	= 'tblpedido_item.fldPedido_Id';
					}
					
					$rsItem = mysql_query("SELECT {$tbl_documento}_item.*, {$tbl_documento}_item.fldProduto_Id as fldProdutoId, tblproduto_fiscal.*
									  FROM {$tbl_documento}_item LEFT JOIN tblproduto_fiscal ON {$tbl_documento}_item.fldProduto_Id = tblproduto_fiscal.fldProduto_Id
									  WHERE $item_where IN (" . join(',', $id_query_array) . ") AND fldExcluido = '0'");
									  
					echo mysql_error();
					$rows = mysql_num_rows($rsItem);
					$i = 1;
					$valor_total_tributos = 0;
					while($rowItem = mysql_fetch_array($rsItem)){
						echo mysql_error();	
						$rsProduto = mysql_query("SELECT tblproduto.*, 
										 tblproduto_unidade_medida.fldSigla
										 FROM tblproduto LEFT JOIN tblproduto_unidade_medida ON tblproduto.fldUN_Medida_Id = tblproduto_unidade_medida.fldId
										 WHERE tblproduto.fldId = " . $rowItem['fldProdutoId']);
						$rowProduto = mysql_fetch_array($rsProduto);
						
						echo mysql_error();
						$rowEstoque = mysql_fetch_array(mysql_query("SELECT tblproduto_estoque.fldId, tblproduto_estoque.fldNome
													FROM tblproduto_estoque INNER JOIN {$tbl_documento}_item ON tblproduto_estoque.fldId = {$tbl_documento}_item.fldEstoque_Id
													WHERE tblproduto_estoque.fldId = ".$rowItem['fldEstoque_Id']."
						"));
						echo mysql_error();
						$item_id 				= $rowItem['fldId'];
						
						$valor_item 			= $rowItem['fldValor'];
						$qtd 					= $rowItem['fldQuantidade'];
						$total_item 			= $valor_item * $qtd;
						
						$desconto 				= $rowItem['fldDesconto'];
						$totalDesconto 			= ($total_item * $desconto)/100;
						$totalValor 			= $total_item - $totalDesconto;
						
						$produto_id 			= $rowItem['fldProdutoId'];
						
						$total_nota_desconto 	+= $totalDesconto;
						$total_pedido_item 		+= $total_item;
						
						//NFE ####################################################################################
						$cfop_id				= $rowItem['fldcfop_id'];
						if($cfop_id > 0){ $rowCFOP = (mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_cfop WHERE fldId = $cfop_id"))); }
						echo mysql_error();

						$ncm 					= $rowItem['fldncm']; 
						
						$ex_tipi 				= $rowItem['fldex_tipi']; 
						
						$cfop					= (isset($cfop_id)) ? $rowCFOP['fldCFOP'] : '';	
						$unid_comercial 		= $rowItem['fldunidade_comercial'];
						$unid_tributavel 		= $rowItem['fldunidade_tributavel'];
						
						$qtde_tributavel 		= $qtd;
						$valor_unit_tributavel 	= $valor_item;
						$itemTotalBruto 		= $totalValor;
						$itemChkBruto 			= '1';
						
						$total_seguro 			= '';
						$desconto				= $totalDesconto;
						$frete 					= '';
						$ean 					= $rowItem['fldean'];
						$ean_unid_tributavel 	= $rowItem['fldean_unidade_tributavel'];
						$outras_despesas		= '';
						$pedido_compra 			= '';
						$numero_pedido_compra 	= '';
						$produto_especifico 	= '';
						
						$icms_sit_tributaria 	= $rowItem['fldicms_CST'];
						$rad_imposto			= 'icms';
						
						$icms_origem		 	= $rowItem['fldicms_orig'];
						$icms_aliq_aplicavel	= $rowItem['fldicms_pCredSN'];
						
						//uso o ncm, origem e a sit pra fazer as buscas na tblibpt, e vou somando...
						$st_trib 	= $icms_sit_tributaria;
						$origem 	= $icms_origem;
						if($st_trib == '16' or $st_trib == '17' or $st_trib == '21' or $st_trib == '22' or $st_trib == '23' or $st_trib == '24' or $st_trib == '25'){
							if($origem != '' and $origem != null){
								if($origem == "0" or $origem == "3" or $origem == "4" or $origem == "5"){ $campo = "fldAliqNac"; }
								else{ $campo == "fldAliqImp"; }
										
								$rsNCM 							= mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_ibpt WHERE fldCodigo = $ncm"));
								$valor_trib 					= $rsNCM[$campo];
								$valor_total_trib_individual 	= ($valor_item * ($valor_trib / 100)) * $qtd;
								
								$valor_total_tributos += $valor_total_trib_individual;
							}
						}
						
						if($icms_aliq_aplicavel > 0){
							$icms_cred_aproveitado	= ($itemTotalBruto * $icms_aliq_aplicavel) / 100;
						}
						
						$icms_bc_modalidade		= $rowItem['fldicms_modBC'];
						$icms_base_calculo		= '';
						$icms_bc_perc_reducao	= $rowItem['fldicms_pRedBC'];
						$icms_aliquota			= $rowItem['fldicms_pICMS'];
						
						$icms_valor				= '';
						$icms_bc_op_propria		= $rowItem['fldicms_pBCOp'];
						$icms_desoneracao_motiv = $rowItem['fldicms_motDesICMS'];
						
						$icmsst_bc_modalidade	= $rowItem['fldicms_modBCST'];
						$icmsst_bc_perc_reducao	= $rowItem['fldicms_pRedBCST'];
						$icmsst_mva				= $rowItem['fldicms_pMVAST'];
						$icmsst_base_calculo	= '';
						$icmsst_aliquota		= $rowItem['fldicms_pICMSST'];
						$icmsst_valor			= '';
						$icmsst_uf				= $rowItem['fldicms_UFST'];
						$icmsst_bc_ret_uf_rem	= '';
						$icmsst_val_ret_uf_rem	= '';
						$icmsst_bc_uf_destino	= '';
						$icmsst_valor_uf_destino= '';
					
						$cofins_sit_tributaria	= $rowItem['fldcofins_situacao_tributaria'];
						$cofins_tipo_calculo	= $rowItem['fldcofins_calculo_tipo'];
						$cofins_base_calculo	= '';
						$cofins_aliq_percent	= $rowItem['fldcofins_aliquota_porcentagem'];
						$cofins_aliq_reais		= $rowItem['fldcofins_aliquota_reais'];
						$cofins_qtde_vendida	= '';
						$cofins_valor			= '';
						
						$cofinsst_tipo_calculo	= $rowItem['fldcofinsst_calculo_tipo'];
						$cofinsst_base_calculo	= '';
						$cofinsst_aliq_percent	= $rowItem['fldcofinsst_aliquota_porcentagem'];
						$cofinsst_aliq_reais	= $rowItem['fldcofinsst_aliquota_reais'];
						$cofinsst_qtde_vendida	= '';
						$cofinsst_valor			= '';
						
						$ii_base_calculo		= '';
						$ii_desp_aduaneiras_val	= '';
						$ii_iof_valor			= '';
						$ii_valor				= '';
						
						$ipi_sit_tributaria		= $rowItem['fldipi_situacao_tributaria'];
						$ipi_enquadra_classe	= $rowItem['fldipi_enquadramento_classe'];
						$ipi_enquadra_codigo	= $rowItem['fldipi_enquadramento_codigo'];
						$ipi_produtor_cnpj		= $rowItem['fldipi_produtor_cnpj'];
						
						$ipi_selo_controle_cod	= '';
						$ipi_selo_controle_qtde	= '';
						
						$ipi_calculo_tipo		= $rowItem['fldipi_calculo_tipo'];
						$ipi_base_calculo		= '';
						$ipi_aliquota			= $rowItem['fldipi_aliquota'];
						$ipi_unid_qtde_total	= '';
						$ipi_unidade_valor		= $rowItem['fldipi_unidade_valor'];
						$ipi_valor				= '';
						
						$issqn_sit_tributaria	= '';
						$issqn_base_calculo		= '';
						$issqn_aliquota			= '';
						$issqn_lista_servico	= '';
						$issqn_uf				= '';
						$issqn_mun_ocorrendia	= '';
						$issqn_valor			= '';
						
						$pis_sit_tributaria		= $rowItem['fldpis_situacao_tributaria'];
						$pis_calculo_tipo		= $rowItem['fldpis_calculo_tipo'];
						$pis_base_calculo		= '';
						$pis_aliq_percentual	= $rowItem['fldpis_aliquota_porcentagem'];
						$pis_aliq_reais			= $rowItem['fldpis_aliquota_reais'];
						$pis_qtde_vendida		= '';
						$pis_valor				= '';
						
						$pisst_tipo_calculo		= $rowItem['fldpisst_calculo_tipo'];
						$pisst_base_calculo		= '';
						$pisst_aliq_percent		= $rowItem['fldpisst_aliquota_porcentagem'];
						$pisst_aliq_reais		= '';
						$pisst_qtde_vendida		= '';
						$pisst_valor			= '';
						$info_adicionais		= $rowItem['fldinformacoes_adicionais'];
						
						$prod_especifico_id 		= $rowItem['fldTipo_Especifico'];
						$combustivel_anp 			= $rowItem['fldCombustivel_Codigo_ANP'];
						$combustivel_if 			= $rowItem['fldCombustivel_Codigo_CodIF'];
						$combustivel_uf 			= $rowItem['fldCombustivel_UF'];
						$combustivel_qtd_faturada 	= $rowItem['fldCombustivel_Qtd_Faturada'];
						$combustivel_cide_bc 		= $rowItem['fldCombustivel_Cide_bCalculo'];
						$combustivel_cide_aliquota 	= $rowItem['fldCombustivel_Cide_Aliquota'];
						$combustivel_cide_valor 	= $rowItem['fldCombustivel_Cide_Valor'];
?>					
                        <ul id="pedido_lista_item" style="width: 11110px">
                        	<input type="hidden" id="hid_venda_decimal"		 name="hid_venda_decimal" 		value="<?=$venda_decimal?>" />
                            <input type="hidden" id="hid_quantidade_decimal" name="hid_quantidade_decimal" 	value="<?=$qtde_decimal?>" />
                            <? if($nfe_importar != 'true'){ ?>                           
                                    <li style="width:20px;">
                                        <a class="a_excluir" id="excluir_<?=$i?>" href="" title="Excluir item" ></a>
                                    </li>
                            <?	}						?>
                            <li style="width:20px">
                                <a class="edit edit_js modal" id="edit_<?=$i?>" href="pedido_nfe_item,<?=$produto_id.",".$i?>,1" name='edit_<?=$i?>' rel='980-500' title="Editar item"></a>
                            </li>
							<li style="width:20px;background:#FFF">
                                <input class="chk_entregue" style="width:20px" type="checkbox" name="chk_entregue_<?=$i?>" id="chk_pedido_<?=$i?>" title="item entregue" checked='checked' onclick="return false"  />
                                <input class="hid_pedido_item_id" type="hidden" id="hid_pedido_item_id_<?=$i?>" name="hid_pedido_item_id_<?=$i?>" value="<?=$rowItem['fldId']?>" />
								<input class="hid_pedido_item_entregue_data" type="hidden" id="hid_pedido_item_entregue_data_<?=$i?>" name="hid_pedido_item_entregue_data_<?=$i?>" value="<?=$rowItem['fldEntregueData']?>" />
                            </li>
							<li>
                            	<input type="text" class="txt_item_origem" id="txt_item_origem_<?=$i?>" name="txt_item_origem_<?=$i?>" style="width:45px;text-align:center;color:#06F" value="<?= str_pad($rowItem['fldPedido_Id'],4,"0", STR_PAD_LEFT)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_item_codigo"  style="width: 80px" id="txt_item_codigo_<?=$i?>" name="txt_item_codigo_<?=$i?>" value="<?=$rowProduto['fldCodigo']?>" readonly="readonly"/>
                                <input type="hidden" class="hid_item_produto_id"  id="hid_item_produto_id_<?=$i?>" name="hid_item_produto_id_<?=$i?>" value="<?=$rowProduto['fldId']?>" />
								<input class="hid_item_detalhe" type="hidden" id="hid_item_detalhe_<?=$i?>" name="hid_item_detalhe_<?=$i?>" value="1" />
                            </li>
                            <li>
                                <input type="text" class="txt_item_nome" id="txt_item_nome_<?=$i?>" name="txt_item_nome_<?=$i?>" style="width:320px;text-align:left" value="<?=$rowItem['fldDescricao']?>" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_item_fardo" 		type="text" 	id="txt_item_fardo_<?=$i?>" 		name="txt_item_fardo_<?=$i?>" 		value="<?=$rowFardo['fldCodigo']?>" style="width:50px" readonly="readonly" />
                                <input class="hid_item_fardo" 		type="hidden" 	id="hid_item_fardo_<?=$i?>" 		name="hid_item_fardo_<?=$i?>" 		value="<?=$rowFardo['fldId']?>" />
                            </li>
                            <li>
                            	<?	if($rowItem['fldTabela_Preco_Id'] != NULL){
											$rowTabela 	= mysql_fetch_array(mysql_query("SELECT * FROM tblproduto_tabela WHERE fldId = ".$rowItem['fldTabela_Preco_Id'])); 
											$sigla 		= $rowTabela['fldSigla'];
									}else{	$sigla 		= 'cadastro';			}
								?>
                                <input type="text" 	class="txt_item_tabela_sigla" style="width:55px" id="txt_item_tabela_sigla_<?=$i?>" name="txt_item_tabela_sigla_<?=$i?>" value="<?=$sigla?>" readonly="readonly" />
                                <input type="hidden" class="hid_item_tabela_preco" id="hid_item_tabela_preco_<?=$i?>" name="hid_item_tabela_preco_<?=$i?>" value="<?=$rowItem['fldTabela_Preco_Id']?>" />
                            </li>
                            <li>
                                <input type="text" class="txt_item_valor"  id="txt_item_valor_<?=$i?>" name="txt_item_valor_<?=$i?>" style="width:80px; text-align:right" value="<?=format_number_out($rowItem['fldValor'],$venda_decimal)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_item_quantidade" style="width:50px; text-align: right" id="txt_item_quantidade_<?=$i?>" name="txt_item_quantidade_<?=$i?>" value="<?=format_number_out($rowItem['fldQuantidade'],$qtde_decimal)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" style="width:70px" class="txt_item_estoque_nome" id="txt_item_estoque_nome_<?=$i?>" name="txt_item_estoque_nome_<?=$i?>" value="<?=$rowEstoque['fldNome']?>" readonly="readonly" />
                                <input type="hidden" class="hid_item_estoque_id" id="hid_item_estoque_id_<?=$i?>" name="hid_item_estoque_id_<?=$i?>" value="<?=$rowEstoque['fldId']?>" />
                            </li>
                            <li>
                                <input type="text" class="txt_item_UM" id="txt_item_UM_<?=$i?>" name="txt_item_UM_<?=$i?>" style="width:30px; text-align:center" value="<?=$rowProduto['fldSigla']?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_item_desconto" style="width:50px; text-align: right" id="txt_item_desconto_<?=$i?>" name="txt_item_desconto_<?=$i?>" value="<?=format_number_out($rowItem['fldDesconto'])?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_item_subtotal" style="width:70px; text-align: right" id="txt_item_subtotal_<?=$i?>" name="txt_item_subtotal_<?=$i?>" value="<?=format_number_out($totalValor)?>" readonly="readonly" />
                            </li>
                            
                            <!-- nfe -->
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_ncm" id="txt_nfe_pedido_item_principal_ncm_<?=$i?>" name="txt_nfe_pedido_item_principal_ncm_<?=$i?>" style="width: 70px" value="<?=$ncm?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_ex_tipi" id="txt_nfe_pedido_item_principal_ex_tipi_<?=$i?>" name="txt_nfe_pedido_item_principal_ex_tipi_<?=$i?>" style="width: 70px" value="<?=$ex_tipi?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_cfop" id="txt_nfe_pedido_item_principal_cfop_<?=$i?>" name="txt_nfe_pedido_item_principal_cfop_<?=$i?>" style="width: 70px" value="<?=$cfop?>" readonly="readonly" />

                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_unidade_comercial" id="txt_nfe_pedido_item_principal_unidade_comercial_<?=$i?>" name="txt_nfe_pedido_item_principal_unidade_comercial_<?=$i?>" style="width: 90px; text-align:center" value="<?=$unid_comercial?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_unidade_tributavel" id="txt_nfe_pedido_item_principal_unidade_tributavel_<?=$i?>" name="txt_nfe_pedido_item_principal_unidade_tributavel_<?=$i?>" style="width: 70px;text-align:center" value="<?=$unid_tributavel?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_quantidade_tributavel" id="" name="txt_nfe_pedido_item_principal_quantidade_tributavel_<?=$i?>" style="width: 70px;text-align:right" value="<?=format_number_out($qtde_tributavel,$qtde_decimal)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_valor_unidade_tributavel" id="txt_nfe_pedido_item_principal_valor_unidade_tributavel_<?=$i?>" name="txt_nfe_pedido_item_principal_valor_unidade_tributavel_<?=$i?>" style="width: 110px;text-align:right" value="<?=format_number_out($valor_unit_tributavel,$venda_decimal)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_total_seguro" id="txt_nfe_pedido_item_principal_total_seguro_<?=$i?>" name="txt_nfe_pedido_item_principal_total_seguro_<?=$i?>" style="width: 70px;text-align:right" value="<?=format_number_out($total_seguro)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_desconto" style="width:85px; text-align: right" id="txt_nfe_pedido_item_principal_desconto_<?=$i?>" name="txt_nfe_pedido_item_principal_desconto_<?=$i?>" value="<?=format_number_out($desconto)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_frete" id="txt_nfe_pedido_item_principal_frete_<?=$i?>" name="txt_nfe_pedido_item_principal_frete_<?=$i?>" style="width: 70px;text-align:right" value="<?=format_number_out($frete)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_ean" id="txt_nfe_pedido_item_principal_ean_<?=$i?>" name="txt_nfe_pedido_item_principal_ean_<?=$i?>" style="width: 70px" value="<?=$ean?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_ean_tributavel" id="txt_nfe_pedido_item_principal_ean_tributavel_<?=$i?>" name="txt_nfe_pedido_item_principal_ean_tributavel_<?=$i?>" style="width: 70px;text-align:right" value="<?=$ean_unid_tributavel?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_outras_despesas_acessorias" id="txt_nfe_pedido_item_principal_outras_despesas_acessorias_<?=$i?>" name="txt_nfe_pedido_item_principal_outras_despesas_acessorias_<?=$i?>" style="width: 120px;text-align:right" value="<?=format_number_out($outras_despesas)?>" readonly="readonly" />
                            </li>
                            <li>
								<!--//130201 Ivan - n�o pode considerar desconto, aqui � valor bruto, antes estava usando a vari�vel com desconto do item (itemTotalBruto)-->
                                <input type="text" class="txt_nfe_pedido_item_principal_valor_total_bruto" id="txt_nfe_pedido_item_principal_valor_total_bruto_<?=$i?>" name="txt_nfe_pedido_item_principal_valor_total_bruto_<?=$i?>" style="width: 90px;text-align:right" value="<?=format_number_out($total_item)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_pedido_compra" id="txt_nfe_pedido_item_principal_pedido_compra_<?=$i?>" name="txt_nfe_pedido_item_principal_pedido_compra_<?=$i?>" style="width: 70px;text-align:right" value="<?=$pedido_compra?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_principal_numero_item_pedido_compra" id="txt_nfe_pedido_item_principal_numero_item_pedido_compra_<?=$i?>" name="txt_nfe_pedido_item_principal_numero_item_pedido_compra_<?=$i?>" style="width: 110px;text-align:right" value="<?=$numero_pedido_compra?>" readonly="readonly" />
                            </li>
                            <!--
                            <li>
                                <input type="text" style="width:70px;text-align:left" class="txt_nfe_pedido_item_principal_produto_especifico" id="txt_nfe_pedido_item_principal_produto_especifico_<?=$i?>" name="txt_nfe_pedido_item_principal_produto_especifico_<?=$i?>"  value="<?=$produto_especifico?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_principal_produto_especifico"  id="hid_nfe_pedido_item_principal_produto_especifico" name="hid_nfe_pedido_item_principal_produto_especifico_<?=$i?>" value="<?=$rowItem['fldproduto_especifico']?>" />  
                            </li>
                            -->
                            <li>
                                <input type="checkbox" class="chk_nfe_pedido_item_principal_valor_total_bruto" id="chk_nfe_pedido_item_principal_valor_total_bruto_<?=$i?>" name="chk_nfe_pedido_item_principal_valor_total_bruto_<?=$i?>" style="width: 110px;text-align:left" <?= ($itemChkBruto == '1') ? "checked='checked'" : ''?> onclick="return false" />
                            </li>
                            <!--tributos-->
                            <li>
                            	<?	$rowSitTributaria = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_notafiscal_icms WHERE fldId = ".fncNullId($icms_sit_tributaria)))	?>
                                <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_situacao_tributaria" id="txt_nfe_pedido_item_tributos_icms_situacao_tributaria_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_situacao_tributaria_<?=$i?>" value="<?=$rowSitTributaria['fldSituacaoTributaria'].' - '.$rowSitTributaria['fldDescricao']?>" readonly="readonly" />
                      			<input type="hidden" class="hid_nfe_pedido_item_tributos_icms_situacao_tributaria" id="hid_nfe_pedido_item_tributos_icms_situacao_tributaria" name="hid_nfe_pedido_item_tributos_icms_situacao_tributaria_<?=$i?>" value="<?=$icms_sit_tributaria?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_tributo_rad" id="hid_nfe_tributo_rad_<?=$i?>" name="hid_nfe_tributo_rad_<?=$i?>" value="<?=$rad_imposto?>"  />
                            </li>
                            <li>
	                            <?	$rowOrigem = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_icms_campo_orig WHERE fldId = ".fncNullId($icms_origem)))	?>
                                <input type="text" style="width:70px;text-align:center" class="txt_nfe_pedido_item_tributos_icms_origem" id="txt_nfe_pedido_item_tributos_icms_origem_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_origem_<?=$i?>" value="<?=$rowOrigem['fldDescricao']?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_origem" id="hid_nfe_pedido_item_tributos_icms_origem_<?=$i?>" name="hid_nfe_pedido_item_tributos_icms_origem_<?=$i?>" value="<?=$icms_origem?>" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_pCredSN" id="txt_nfe_pedido_item_tributos_icms_pCredSN_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_pCredSN_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icms_aliq_aplicavel)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_vCredICMSSN" id="txt_nfe_pedido_item_tributos_icms_vCredICMSSN_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_vCredICMSSN_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icms_cred_aproveitado)?>" readonly="readonly" />
                            </li>
                            
                            <li>
                            	<?	$rowModBC = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_icms_campo_modbc WHERE fldId = ".fncNullId($icms_bc_modalidade)))	?>
                                <input type="text"  style="width:150px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_modBC" id="txt_nfe_pedido_item_tributos_icms_modBC_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_modBC_<?=$i?>" value="<?=$rowModBC['fldDescricao']?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_modBC" id="hid_nfe_pedido_item_tributos_icms_modBC" name="hid_nfe_pedido_item_tributos_icms_modBC_<?=$i?>" value="<?=$icms_bc_modalidade?>" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_vBC" id="txt_nfe_pedido_item_tributos_icms_vBC_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_vBC_<?=$i?>" style="width:80px;text-align:right" value="<?=format_number_out($icms_base_calculo)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_pRedBC" id="txt_nfe_pedido_item_tributos_icms_pRedBC_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_pRedBC_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($icms_bc_perc_reducao)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_pICMS" id="txt_nfe_pedido_item_tributos_icms_pICMS_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_pICMS_<?=$i?>" style="width:70px;text-align:right" value="<?=format_number_out($icms_aliquota)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_vICMS" id="txt_nfe_pedido_item_tributos_icms_vICMS_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_vICMS_<?=$i?>" style="width:80px;text-align:right" value="<?=format_number_out($icms_valor)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_pBCOp" id="txt_nfe_pedido_item_tributos_icms_pBCOp_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_pBCOp_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icms_bc_op_propria)?>" readonly="readonly" />
                            </li>
                            <li>
                            	<?	$rowDesMotivo = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_icms_campo_motdesicms WHERE fldId = ".fncNullId($icms_desoneracao_motiv)))	?>
                                <input type="text" style="width:150px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_motDesICMS" id="txt_nfe_pedido_item_tributos_icms_motDesICMS_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_motDesICMS_<?=$i?>" value="<?=$rowDesMotivo['fldDescricao']?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_motDesICMS"  id="hid_nfe_pedido_item_tributos_icms_motDesICMS_<?=$i?>" name="hid_nfe_pedido_item_tributos_icms_motDesICMS_<?=$i?>" value="<?=$icms_desoneracao_motiv?>" />
                            </li>
                            <li>
                            	<?	$rowModBCST = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_icms_campo_modbcst WHERE fldId = ".fncNullId($icmsst_bc_modalidade))); echo mysql_error()?>
                                <input type="text" style="width:180px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_st_modBCST" id="txt_nfe_pedido_item_tributos_icms_st_modBCST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_modBCST_<?=$i?>" value="<?=$rowModBCST['fldDescricao']?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_st_modBCST" id="hid_nfe_pedido_item_tributos_icms_st_modBCST_<?=$i?>" name="hid_nfe_pedido_item_tributos_icms_st_modBCST_<?=$i?>" value="<?=$icmsst_bc_modalidade?>" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pRedBCST" id="txt_nfe_pedido_item_tributos_icms_st_pRedBCST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_pRedBCST_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($icmsst_bc_perc_reducao)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pMVAST" id="txt_nfe_pedido_item_tributos_icms_st_pMVAST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_pMVAST_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icmsst_mva)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCST" id="txt_nfe_pedido_item_tributos_icms_st_vBCST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vBCST_<?=$i?>" style="width:100px;text-align:right" value="<?=format_number_out($icmsst_base_calculo)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pICMSST" id="txt_nfe_pedido_item_tributos_icms_st_pICMSST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_pICMSST_<?=$i?>" style="width:100px;text-align:right" value="<?=format_number_out($icmsst_aliquota)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSST" id="txt_nfe_pedido_item_tributos_icms_st_vICMSST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vICMSST_<?=$i?>" style="width:80px;text-align:right" value="<?=format_number_out($icmsst_valor)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_st_UFST" id="txt_nfe_pedido_item_tributos_icms_st_UFST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_UFST_<?=$i?>" value="<?=$icmsst_uf?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_st_UFST" id="hid_nfe_pedido_item_tributos_icms_st_UFST_<?=$i?>" name="hid_nfe_pedido_item_tributos_icms_st_UFST_<?=$i?>" value="<?=$rowItem['fldicmsst_uf']?>" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet" id="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icmsst_bc_ret_uf_rem)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet" id="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icmsst_val_ret_uf_rem)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest" id="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icmsst_bc_uf_destino)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest" id="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icmsst_valor_uf_destino)?>" readonly="readonly" />
                            </li>
                            <!--tributos cofins-->
                            <li>
                            	<?	$rowSitTribCofins = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_cofins WHERE fldId = ".fncNullId($cofins_sit_tributaria)))	?>
                                <input type="text" style="width:120px;text-align:left"  class="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria" id="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria_<?=$i?>" value="<?=$rowSitTribCofins['fldCOFINS'].' - '.$rowSitTribCofins['fldDescricao']?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria" id="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria" name="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria_<?=$i?>" value="<?=$cofins_sit_tributaria?>" />
                            </li>
                            <li>
	                            <?	$calculoTipo = ($cofins_tipo_calculo == '1') ? 'percentual' : ''; $calculoTipo = ($cofins_tipo_calculo == '2') ? 'percentual' : ''; //FIZ AS DUAS VERIFICACOES PQ PODE TER 0 OU NULL TB ?>
                                <input type="text" style="width:110px;text-align:center" class="txt_nfe_pedido_item_tributos_cofins_calculo_tipo" id="txt_nfe_pedido_item_tributos_cofins_calculo_tipo_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_calculo_tipo_<?=$i?>" value="<?=$calculoTipo?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_calculo_tipo" id="hid_nfe_pedido_item_tributos_cofins_calculo_tipo" name="hid_nfe_pedido_item_tributos_cofins_calculo_tipo_<?=$i?>" value="<?=$cofins_tipo_calculo?>" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo" id="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_<?=$i?>" style="width:100px;text-align:right" value="<?=format_number_out($cofins_base_calculo)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual" id="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_<?=$i?>" style="width:130px;text-align:right" value="<?=format_number_out($cofins_aliq_percent)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_cofins_aliquota_reais" id="txt_nfe_pedido_item_tributos_cofins_aliquota_reais_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_aliquota_reais_<?=$i?>" style="width:130px;text-align:right" value="<?=format_number_out($cofins_aliq_reais)?>" readonly="readonly" />
                            </li>
                            <li>	
                                <input type="text" class="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida" id="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($cofins_qtde_vendida)?>" readonly="readonly" /> 
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_cofins_valor" id="txt_nfe_pedido_item_tributos_cofins_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_valor_<?=$i?>" style="width:100px;text-align:right" value="<?=format_number_out($cofins_valor)?>" readonly="readonly" />
                            </li>
                            <li>
                            	<?	$calculoTipo = ($cofinsst_tipo_calculo == '1') ? 'percentual' : ''; $calculoTipo = ($cofinsst_tipo_calculo == '2') ? 'percentual' : ''; //FIZ AS DUAS VERIFICACOES PQ PODE TER 0 OU NULL TB ?>
                                <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo" id="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo_<?=$i?>" value="<?=$calculoTipo?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo" id="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo" name="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo_<?=$i?>" value="<?=$cofinsst_tipo_calculo?>" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo" id="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($cofinsst_base_calculo)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual" id="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_<?=$i?>" style="width:140px;text-align:" value="<?=format_number_out($cofinsst_aliq_percent)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais" id="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($cofinsst_aliq_reais)?>" readonly="readonly" />
                            </li>
                            <li>	
                                <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida" id="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_<?=$i?>" style="width:140px;text-align:right" value="<?=format_number_out($cofinsst_qtde_vendida)?>" readonly="readonly" /> 
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_valor" id="txt_nfe_pedido_item_tributos_cofins_st_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_valor_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($cofinsst_valor)?>" readonly="readonly" />
                            </li>
                            <!--tributos ii-->
                            <li>
                                <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_base_calculo" id="txt_nfe_pedido_item_tributos_ii_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_ii_valor_base_calculo_<?=$i?>" value="<?=format_number_out($ii_base_calculo)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" style="width:150px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras" id="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_<?=$i?>" name="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_<?=$i?>" value="<?=format_number_out($ii_desp_aduaneiras_val)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_iof" id="txt_nfe_pedido_item_tributos_ii_valor_iof_<?=$i?>" name="txt_nfe_pedido_item_tributos_ii_valor_iof_<?=$i?>" value="<?=format_number_out($ii_iof_valor)?>" readonly="readonly" />
                            </li>
                            <li>	
                                <input type="text" class="txt_nfe_pedido_item_tributos_ii_valor" id="txt_nfe_pedido_item_tributos_ii_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_ii_valor_<?=$i?>" style="width:70px;text-align:right" value="<?=format_number_out($ii_valor)?>" readonly="readonly" /> 
                            </li>
                            <!--tributos ipi-->
                            <li>
                            	<?	$rowSitTribIPI = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_ipi WHERE fldId = ".fncNullId($ipi_sit_tributaria)))	?>
                                <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria" id="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria_<?=$i?>" value="<?=$rowSitTribIPI['fldSituacaoTributaria'].' - '.$rowSitTribIPI['fldDescricao']?>" />
								<input type="hidden" class="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria" id="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria" name="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria_<?=$i?>" value="<?=$ipi_sit_tributaria?>" />
                            </li>
                            <li>	
                                <input type="text" class="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe" id="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_<?=$i?>" style="width:140px" value="<?=$ipi_enquadra_classe?>" readonly="readonly" /> 
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo" id="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_<?=$i?>" style="width:170px" value="<?=$ipi_enquadra_codigo?>" readonly="readonly"/>
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj" id="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_<?=$i?>" style="width:120px;text-align:right" value="<?=$ipi_produtor_cnpj?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo" id="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_<?=$i?>" style="width:140px;text-align:right" value="<?=$ipi_selo_controle_cod?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade" id="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_<?=$i?>" style="width:140px;text-align:right" value="<?=format_number_out($ipi_selo_controle_qtde)?>" readonly="readonly" />
                            </li>
                            <li>
                                <?	$calculoTipo = ($ipi_calculo_tipo == '1') ? 'percentual' : ''; $calculoTipo = ($ipi_calculo_tipo == '2') ? 'percentual' : ''; //FIZ AS DUAS VERIFICACOES PQ PODE TER 0 OU NULL TB ?>
                                <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_ipi_calculo_tipo" id="txt_nfe_pedido_item_tributos_ipi_calculo_tipo_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_calculo_tipo_<?=$i?>" value="<?=$calculoTipo?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_ipi_calculo_tipo" id="hid_nfe_pedido_item_tributos_ipi_calculo_tipo" name="hid_nfe_pedido_item_tributos_ipi_calculo_tipo_<?=$i?>" value="<?=$ipi_calculo_tipo?>" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo" id="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_<?=$i?>" style="width:70px;text-align:right" value="<?=format_number_out($ipi_base_calculo)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_ipi_aliquota" id="txt_nfe_pedido_item_tributos_ipi_aliquota_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_aliquota_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($ipi_aliquota)?>" readonly="readonly"/>
                            </li>
                            <li>
                                <input type="text" class="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total" id="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_<?=$i?>" value="<?=format_number_out($ipi_unid_qtde_total)?>" style="width:150px;text-align:right"  readonly="readonly"/>
                            </li>
                            <li>
                                <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_ipi_unidade_valor" id="txt_nfe_pedido_item_tributos_ipi_unidade_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_unidade_valor_<?=$i?>" value="<?=format_number_out($ipi_unidade_valor)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_ipi_valor" id="txt_nfe_pedido_item_tributos_ipi_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_valor_<?=$i?>" value="<?=format_number_out($ipi_valor)?>" readonly="readonly" />
                            </li>
                            <!--tributos issqn-->
                            <li>
                           		<?	$rowSitTribISSQN = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_tributacao_issqn WHERE fldId = ".fncNullId($issqn_sit_tributaria))) ?>
                                <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_tributacao" id="txt_nfe_pedido_item_tributos_issqn_tributacao_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_tributacao_<?=$i?>" value="<?=$rowSitTribISSQN['fldSituacaoTributaria'].' - '.$rowSitTribISSQN['fldDescricao']?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_tributacao"  id="hid_nfe_pedido_item_tributos_issqn_tributacao" name="hid_nfe_pedido_item_tributos_issqn_tributacao_<?=$i?>" value="<?=$issqn_sit_tributaria?>" />
                            </li>
                            <li>
                                <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo" id="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_<?=$i?>" value="<?=format_number_out($issqn_base_calculo)?>" readonly="readonly" />
                            </li>
                            
                            <li>
                                <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_aliquota" id="txt_nfe_pedido_item_tributos_issqn_aliquota_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_aliquota_<?=$i?>" value="<?=format_number_out($issqn_aliquota)?>" readonly="readonly" />
                            </li>
                            <li>
                            	<?	$rowListaServSSQN = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_issqn_servico WHERE fldId = ".fncNullId($issqn_lista_servico))) ?>
                                <input type="text" style="width:140px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_servico_lista" id="txt_nfe_pedido_item_tributos_issqn_servico_lista_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_servico_lista_<?=$i?>" value="<?=$rowListaServSSQN['fldDescricao']?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_servico_lista" id="hid_nfe_pedido_item_tributos_issqn_servico_lista" name="hid_nfe_pedido_item_tributos_issqn_servico_lista_<?=$i?>" value="<?=$issqn_lista_servico?>" />
                            </li>
                            <li>
                            	<?	$rowUF = mysql_fetch_array(mysql_query("SELECT * FROM tblibge_uf WHERE fldCodigo = ".fncNullId($issqn_uf))) ?>
                                <input type="text"  style="width:70px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_uf" id="txt_nfe_pedido_item_tributos_issqn_uf_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_uf_<?=$i?>" value="<?=$rowUF['fldSigla']?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_uf" id="hid_nfe_pedido_item_tributos_issqn_uf_<?=$i?>" name="hid_nfe_pedido_item_tributos_issqn_uf_<?=$i?>" value="<?=$issqn_uf?>" />
                            </li>
                            <li>
                            	<?	$rowMunicipio = mysql_fetch_array(mysql_query("SELECT * FROM tblibge_municipio WHERE fldCodigo = ".fncNullId($issqn_mun_ocorrendia))) ?>
                                <input type="text" style="width:140px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_municipio" id="txt_nfe_pedido_item_tributos_issqn_municipio_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_municipio_<?=$i?>" value="<?=$rowMunicipio['fldNome']?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_municipio" id="hid_nfe_pedido_item_tributos_issqn_municipio" name="hid_nfe_pedido_item_tributos_issqn_municipio_<?=$i?>" value="<?=$issqn_mun_ocorrendia?>" />
                            </li>
                            <li>
                                <input type="text" style="width:80px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_valor" id="txt_nfe_pedido_item_tributos_issqn_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_valor_<?=$i?>" value="<?=$issqn_valor?>" readonly="readonly" />
                            </li>
                            <!--tributos pis-->
                            <li>
                            	<?	$rowSitTribPIS = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_pis WHERE fldId = ".fncNullId($pis_sit_tributaria))) ?>
                                <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_pis_situacao_tributaria" id="txt_nfe_pedido_item_tributos_pis_situacao_tributaria_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_situacao_tributaria_<?=$i?>" value="<?=$rowSitTribPIS['fldSituacaoTributaria'].' - '.$rowSitTribPIS['fldDescricao']?>"  readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_situacao_tributaria" id="hid_nfe_pedido_item_tributos_pis_situacao_tributaria" name="hid_nfe_pedido_item_tributos_pis_situacao_tributaria_<?=$i?>" value="<?=$pis_sit_tributaria?>" readonly="readonly" />
                            </li>
                            <li>
                            	<?	$calculoTipo = ($pis_calculo_tipo == '1') ? 'percentual' : ''; $calculoTipo = ($pis_calculo_tipo == '2') ? 'percentual' : ''; //FIZ AS DUAS VERIFICACOES PQ PODE TER 0 OU NULL TB ?>
                                <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_pis_calculo_tipo" id="txt_nfe_pedido_item_tributos_pis_calculo_tipo_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_calculo_tipo_<?=$i?>" value="<?=$calculoTipo?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_calculo_tipo" id="hid_nfe_pedido_item_tributos_pis_calculo_tipo" name="hid_nfe_pedido_item_tributos_pis_calculo_tipo_<?=$i?>" value="<?=$pis_calculo_tipo?>" />         
                            </li>
                            <li>
                                <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_valor_base_calculo" id="txt_nfe_pedido_item_tributos_pis_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_valor_base_calculo_<?=$i?>" value="<?=format_number_out($pis_base_calculo)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" style="width:110px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_aliquota_percentual" id="txt_nfe_pedido_item_tributos_pis_aliquota_percentual_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_aliquota_percentual_<?=$i?>" value="<?=format_number_out($pis_aliq_percentual)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_aliquota_reais" id="txt_nfe_pedido_item_tributos_pis_aliquota_reais_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_aliquota_reais_<?=$i?>" value="<?=format_number_out($pis_aliq_reais)?>" readonly="readonly" />
                            </li>
                            <li>	
                                <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_quantidade_vendida" id="txt_nfe_pedido_item_tributos_pis_quantidade_vendida_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_quantidade_vendida_<?=$i?>" value="<?=format_number_out($pis_qtde_vendida)?>" readonly="readonly" /> 
                            </li>
                            <li>
                                <input type="text" style="width:60px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_valor" id="txt_nfe_pedido_item_tributos_pis_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_valor_<?=$i?>" value="<?=format_number_out($pis_valor)?>" readonly="readonly" />
                            </li>
                            <li>
                                <?	$calculoTipo = ($pisst_tipo_calculo == '1') ? 'percentual' : ''; $calculoTipo = ($pisst_tipo_calculo == '2') ? 'percentual' : ''; //FIZ AS DUAS VERIFICACOES PQ PODE TER 0 OU NULL TB ?>
                                <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo" id="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo_<?=$i?>" value="<?=$calculoTipo?>" readonly="readonly" />
                                <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo" id="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo" name="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo_<?=$i?>" value="<?=$pisst_tipo_calculo?>" />  
                            </li>
                            <li>
                                <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo" id="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_<?=$i?>" value="<?=format_number_out($pisst_base_calculo)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual" id="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_<?=$i?>" value="<?=format_number_out($pisst_aliq_percent)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais" id="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_<?=$i?>" value="<?=format_number_out($pisst_aliq_reais)?>" readonly="readonly" />
                            </li>
                            <li>	
                                <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida" id="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_<?=$i?>" value="<?=format_number_out($pisst_qtde_vendida)?>" readonly="readonly" /> 
                            </li>
                            <li>
                                <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_valor" id="txt_nfe_pedido_item_tributos_pis_st_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_valor_<?=$i?>" value="<?=format_number_out($pisst_valor)?>" readonly="readonly" />
                            </li>
                            <li>
                                <input type="text" style="width:200px;text-align:left" class="txt_nfe_item_pedido_informacoes_adicionais" id="txt_nfe_item_pedido_informacoes_adicionais_<?=$i?>" name="txt_nfe_item_pedido_informacoes_adicionais_<?=$i?>" value="<?=$info_adicionais?>" readonly="readonly" />
                            </li>
                            <!--- PRODUTO ESPECIFICO -->
                            <li>
                                <input type="text" 	 style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico" 							id="txt_nfe_item_produto_especifico_<?=$i?>" 							name="txt_nfe_item_produto_especifico_<?=$i?>" 							value="<?=$prod_especifico;?>" 			readonly="readonly" />
                                <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_id" 						id="txt_nfe_item_produto_especifico_id_<?=$i?>" 						name="txt_nfe_item_produto_especifico_id_<?=$i?>" 						value="<?=$prod_especifico_id?>"		readonly="readonly" />
                                <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_anp"			id="txt_nfe_item_produto_especifico_combustivel_anp_<?=$i?>" 			name="txt_nfe_item_produto_especifico_combustivel_anp_<?=$i?>" 			value="<?=$combustivel_anp?>" 			readonly="readonly" />
                                <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_if" 			id="txt_nfe_item_produto_especifico_combustivel_if_<?=$i?>" 			name="txt_nfe_item_produto_especifico_combustivel_if_<?=$i?>" 			value="<?=$combustivel_if?>" 			readonly="readonly" />
                                <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_uf" 			id="txt_nfe_item_produto_especifico_combustivel_uf_<?=$i?>" 			name="txt_nfe_item_produto_especifico_combustivel_uf_<?=$i?>" 			value="<?=$combustivel_uf?>" 			readonly="readonly" />
                                <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_qtd_faturada" 	id="txt_nfe_item_produto_especifico_combustivel_qtd_faturada_<?=$i?>" 	name="txt_nfe_item_produto_especifico_combustivel_qtd_faturada_<?=$i?>" value="<?=$combustivel_qtd_faturada?>" 	readonly="readonly" />
                                <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_bc" 		id="txt_nfe_item_produto_especifico_combustivel_cide_bc_<?=$i?>" 		name="txt_nfe_item_produto_especifico_combustivel_cide_bc_<?=$i?>" 		value="<?=$combustivel_cide_bc?>" 		readonly="readonly" />
                                <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_aliquota" 	id="txt_nfe_item_produto_especifico_combustivel_cide_aliquota_<?=$i?>" 	name="txt_nfe_item_produto_especifico_combustivel_cide_aliquota_<?=$i?>" value="<?=$combustivel_cide_aliquota?>"readonly="readonly" />
                                <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_valor" 	id="txt_nfe_item_produto_especifico_combustivel_cide_valor_<?=$i?>" 	name="txt_nfe_item_produto_especifico_combustivel_cide_valor_<?=$i?>" 	value="<?=$combustivel_cide_valor?>" 	readonly="readonly" />
                                <!-- TRIBUTO -->
	                            <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_total_tributos" 			id="txt_nfe_item_produto_especifico_total_tributos_<?=$i?>" 			name="txt_nfe_item_produto_especifico_total_tributos_<?=$i?>" 			value="<?=$valor_total_trib_individual?>" readonly="readonly" />
                            </li>
                            <!--- PRODUTO ESPECIFICO -->
						</ul> 
<?                     	$subTotalPedido 	+= $totalValor;
						$totalNotaItem		+= $itemTotalBruto;
						$totalNotaDesconto	+= number_format($desconto,2,'.','');
						$i += 1;
					}
?>					
           		</div>
                <div>
                    <input type="hidden" name="hid_controle" id="hid_controle" value="<?=$rows?>" />
                </div>
<?
				#BUSCANDO DADOS DE CADA VENDA IMPORTADA PARA CALCULO DE TOTAIS
				if($documento_tipo == 2 ){				
					$rsTotal = mysql_query("SELECT tblcompra_item.fldDesconto, FORMAT(SUM((tblcompra_item.fldValor - ((tblcompra_item.fldDesconto / 100) * tblcompra_item.fldValor)) * tblcompra_item.fldQuantidade),2) AS fldTotalItens
										FROM tblcompra INNER JOIN tblcompra_item ON tblcompra_item.fldCompra_Id = tblcompra.fldId 
										 WHERE tblcompra.fldId IN (" . join(",", $id_query_array) . ") GROUP BY fldCompra_Id");
										 echo mysql_error();
				}else{		
					$rsTotal = mysql_query("SELECT tblpedido.* ,
										(SELECT FORMAT(SUM((tblpedido_item.fldValor - ((tblpedido_item.fldDesconto / 100) * tblpedido_item.fldValor)) * tblpedido_item.fldQuantidade),2) FROM tblpedido_item 
											WHERE tblpedido_item.fldPedido_Id = tblpedido.fldId GROUP BY fldPedido_Id) AS fldTotalItens,
											
										(SELECT IFNULL(SUM(tblpedido_funcionario_servico.fldValor),0) FROM tblpedido_funcionario_servico WHERE fldFuncao_Tipo = 2 AND fldPedido_Id = tblpedido.fldId)
										 AS fldTotalServicos
										FROM tblpedido WHERE tblpedido.fldId IN (" . join(",", $id_query_array) . ")");
				}
				while($rowSQL = mysql_fetch_array($rsTotal)){
					
					$venda_total = $rowSQL['fldTotalItens'] + $rowSQL['fldTotalServicos'] + $rowSQL['fldValor_Terceiros'];
					if($rowSQL['fldDesconto'] > 0){
						$desconto_venda	= ($rowSQL['fldDesconto'] /100) * $venda_total;
					}else{
						$desconto_venda	= $rowSQL['fldDescontoReais'];
					}
					
					$total_pedido_descontos	+= $desconto_venda;
					$total_pedido_servicos 	+= $rowSQL['fldTotalServicos'];
					$total_pedido_terceiros += $rowSQL['fldValor_Terceiros'];
				}
				//$totalNota = $totalNotaItem - $total_nota_desconto;
				$totalNota 		= $totalNotaItem; //removido por causa de problemas com desconto no item (fazendo o desconto 2 vezes)
				$pedidoSubTotal = $totalNota + $total_pedido_servicos + $total_pedido_terceiros;
				$pedidoTotal 	= $pedidoSubTotal - $total_pedido_descontos;
				
?>				
				<div id="dados_nfe">
                    <ul id="pedido_nfe_abas" class="menu_modo" style="width:470px; float:left">
                        <li><a href="totais">Totais</a></li>
                        <li><a href="entrega">Entrega</a></li>
                        <li><a href="transporte">Transporte</a></li>
                        <li><a href="informacoes">Inf. Adic.</a></li>
                    </ul>
					
                    <div id="pedido_nfe_totais">
                        <ul id="pedido_nfe_totais_abas" class="menu_modo" style="width:470px; float:left">
                            <li><a href="icms">ICMS</a></li>
                            <li><a href="issqn">ISSQN</a></li>
                            <li><a href="retencao">Reten&ccedil;&atilde;o de Tributos</a></li>
                        </ul>
                        
                        <div id="totais_icms">
                            <ul class="dados_nfe">
                                <li class="dados" style="margin-bottom:20px; margin-top:10px; width: 470px">
                                    <label for="sel_pedido_nfe_pagamento_forma">Forma Pagamento</label>
                                    <select style="width:305px; float: right" id="sel_pedido_nfe_pagamento_forma" name="sel_pedido_nfe_pagamento_forma" class="sel_pedido_nfe_pagamento_forma" title="Forma Pagamento">
										<option <?= ($nfePagamento == 0)? "selected='selected'":'' ?> value="0">Pagamento &agrave; vista</option>
										<option <?= ($nfePagamento == 1)? "selected='selected'":'' ?> value="1">Pagamento &agrave; prazo</option>
										<option <?= ($nfePagamento == 2)? "selected='selected'":'' ?> value="2">Outros</option>
                    				</select> 
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_produtos">Total dos Produtos</label>
                                    <input type="text" id="txt_pedido_nfe_total_produtos" name="txt_pedido_nfe_total_produtos" value="<?=format_number_out($total_pedido_item)?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_icms">BC do ICMS</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_icms" name="txt_pedido_nfe_total_bc_icms" value="" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_icms">Total ICMS</label>
                                    <input type="text" id="txt_pedido_nfe_total_icms" name="txt_pedido_nfe_total_icms" value="" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_icms_substituicao">BC ICMS Substitui&ccedil;&atilde;o</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_icms_substituicao" name="txt_pedido_nfe_total_bc_icms_substituicao" value="" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_icms_substituicao">Total ICMS Substitui&ccedil;&atilde;o</label>
                                    <input type="text" id="txt_pedido_nfe_total_icms_substituicao" name="txt_pedido_nfe_total_icms_substituicao" value="" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_frete">Total Frete</label>
                                    <input type="text" id="txt_pedido_nfe_total_frete" name="txt_pedido_nfe_total_frete" value="" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_seguro">Total Seguro</label>
                                    <input type="text" id="txt_pedido_nfe_total_seguro" name="txt_pedido_nfe_total_seguro" value="" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_desconto">Total Desconto</label>
                                    <input type="text" id="txt_pedido_nfe_total_desconto" name="txt_pedido_nfe_total_desconto" value="<?=format_number_out($total_nota_desconto)?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_ii">Total II</label>
                                    <input type="text" id="txt_pedido_nfe_total_ii" name="txt_pedido_nfe_total_ii" value="" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_ipi">Total IPI</label>
                                    <input type="text" id="txt_pedido_nfe_total_ipi" name="txt_pedido_nfe_total_ipi" value="" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_pis">Total PIS</label>
                                    <input type="text" id="txt_pedido_nfe_total_pis" name="txt_pedido_nfe_total_pis" value="" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_cofins">Total Cofins</label>
                                    <input type="text" id="txt_pedido_nfe_total_cofins" name="txt_pedido_nfe_total_cofins" value="" readonly="readonly" />
                                </li>
                                
                                <li class="dados">
                                    <label for="txt_pedido_nfe_outras_despesas_acessorias">Out. Despesas Acess&oacute;rias</label>
                                    <input type="text" id="txt_pedido_nfe_outras_despesas_acessorias" name="txt_pedido_nfe_outras_despesas_acessorias" value="" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_nfe">Total da Nota</label>
                                    <input type="text" id="txt_pedido_nfe_total_nfe" name="txt_pedido_nfe_total_nfe" value="<?=format_number_out($totalNota)?>" readonly="readonly" />
                                </li>
                            </ul>
                            
                            <hr />
                            
                            <ul style="margin-top:20px">
                            	<li class="dados">
                                    <label for="txt_pedido_nfe_total_tributos">Total dos Tributos</label>
                                    <input type="text" id="txt_pedido_nfe_total_tributos" name="txt_pedido_nfe_total_tributos" value="<?=format_number_out($valor_total_tributos);?>" readonly="readonly" />
                                    <img src="image/layout/loading.gif" style="float:left; margin:2px 0 0 10px; display:none" id="total_tributos_loading" />
                                </li>
                            </ul>
						</div>
                        
                        <div id="totais_issqn">
                            <ul class="dados_nfe">
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_iss">Base de C&aacute;lculo do ISS</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_iss" name="txt_pedido_nfe_total_bc_iss" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_iss">Total do ISS</label>
                                    <input type="text" id="txt_pedido_nfe_total_iss" name="txt_pedido_nfe_total_iss" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_pis_servicos">PIS sobre servi&ccedil;os</label>
                                    <input type="text" id="txt_pedido_nfe_total_pis_servicos" name="txt_pedido_nfe_total_pis_servicos" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_cofins_servicos">COFINS sobre servi&ccedil;os</label>
                                    <input type="text" id="txt_pedido_nfe_total_cofins_servicos" name="txt_pedido_nfe_total_cofins_servicos" value="0,00" readonly="readonly" />
                                </li>
                                <li style="width: 470px" class="dados">
                                    <label for="txt_pedido_nfe_total_servicos_nao_incidencia_icms">Total dos servi&ccedil;os sob n&atilde;o incid&ecirc;ncia ou n&atilde;o tributados pelo ICMS</label>
                                    <input type="text" id="txt_pedido_nfe_total_servicos_nao_incidencia_icms" name="txt_pedido_nfe_total_servicos_nao_incidencia_icms" value="0,00" readonly="readonly" />
                                </li>
                            </ul>
						</div>     
                        <div id="totais_retencao">
                            <ul class="dados_nfe">
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_pis">Valor retido de PIS</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_pis" name="txt_pedido_nfe_total_retido_pis" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_cofins">Valor retido de COFINS</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_cofins" name="txt_pedido_nfe_total_retido_cofins" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_csll">Valor retido de CSLL</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_csll" name="txt_pedido_nfe_total_retido_csll" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_irrf">Base de C&aacute;lculo do IRRF</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_irrf" name="txt_pedido_nfe_total_bc_irrf" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_irrf">Valor Retido do IRRF</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_irrf" name="txt_pedido_nfe_total_retido_irrf" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_bc_retencao_prev_social">BC Reten&ccedil;&atilde;o da Prev. Social</label>
                                    <input type="text" id="txt_pedido_nfe_bc_retencao_prev_social" name="txt_pedido_nfe_bc_retencao_prev_social" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retencao_prev_social">Reten&ccedil;&atilde;o da Prev. Social</label>
                                    <input type="text" id="txt_pedido_nfe_total_retencao_prev_social" name="txt_pedido_nfe_total_retencao_prev_social" value="0,00" readonly="readonly" />
                                </li>
                            </ul>
						</div>                   
                                                    
                    </div>
                    <div id="pedido_nfe_entrega">
<?						
 						if($documento_tipo == 2 ){
							$id_fornecedor		= $rowDestinatario['fldId'];
							$rsEntregaPadrao 	= mysql_query("SELECT * FROM tblfornecedor WHERE fldId = $id_fornecedor");
						}else{
							$id_cliente			= $rowDestinatario['fldId'];
							$rsEntregaPadrao 	= mysql_query("SELECT * FROM tblcliente_endereco_entrega WHERE fldCliente_Id = $id_cliente AND fldPadrao = 1");
						}
							$rowsEntregaPadrao 	= mysql_num_rows($rsEntregaPadrao);
							$rowEntregaPadrao 	= mysql_fetch_assoc($rsEntregaPadrao);
?>                    	
                    	<ul class="dados_nfe" style="margin:10px 0 0 3px">
							<li>
                    			<label for="txt_pedido_nfe_entrega_cpf_cnpj">CPF/CNPJ</label>
                    			<input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_cpf_cnpj" readonly="readonly" name="txt_pedido_nfe_entrega_cpf_cnpj" value="<?=$rowEntregaPadrao['fldCPF_CNPJ'];?>" />
                    		</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_endereco">Endereco</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_endereco" readonly="readonly" name="txt_pedido_nfe_entrega_endereco" value="<?=$rowEntregaPadrao['fldEndereco'];?>" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_numero">N&uacute;mero</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_numero" readonly="readonly" name="txt_pedido_nfe_entrega_numero" value="<?=$rowEntregaPadrao['fldNumero'];?>" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_complemento">Complemento</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_complemento" readonly="readonly" name="txt_pedido_nfe_entrega_complemento" value="<?=$rowEntregaPadrao['fldComplemento'];?>" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_bairro">Bairro</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_bairro" readonly="readonly" name="txt_pedido_nfe_entrega_bairro" value="<?=$rowEntregaPadrao['fldBairro'];?>" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_cep">Cep</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_cep" readonly="readonly" name="txt_pedido_nfe_entrega_cep" value="<?=$rowEntregaPadrao['fldCep'];?>" />
							</li>
                            <!-- aqui teve que ficar com esses ids no codigo do municipio por causa da busca, qe ja esta sendo parametrizada com esse id -->
                            <li>
                                <label for="txt_municipio_codigo_pedido_nfe">C&oacute;d. Munic&iacute;pio</label>
                                <input type="hidden" id="hid_pedido_nfe_municipio_entrega" name="hid_pedido_nfe_municipio_entrega" value="" />
                                <input type="text" style=" width:185px; text-align:left"  id="txt_municipio_codigo_pedido_nfe" readonly="readonly" name="txt_municipio_codigo_pedido_nfe" value="<?=$rowEntregaPadrao['fldMunicipio_Codigo'];?>" />
                                <a style="float:left;" href="municipio_busca,pedido_nfe" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                            </li>
                            <li>
                                <label for="txt_pedido_nfe_municipio">&nbsp;</label>
                                <input type="text" style=" width:220px; text-align:left" id="txt_pedido_nfe_municipio" name="txt_pedido_nfe_municipio" readonly="readonly" value="<?=fnc_ibge_municipio($rowEntregaPadrao['fldMunicipio_Codigo']);?>" />
                            </li>
                            <li style="width:463px;">
                            	<a style="margin:10px 0px 10px 4px; float:right" id="pedido_nfe_entrega_busca" class="btn_novo modal" href="pedido_nfe_entrega_busca,<?=$id_cliente?>" rel="900-350">buscar</a>
                            	<a style="margin:10px 0; float:right" class="btn_general" id="btn_entrega_limpar" href="">limpar</a>
                            </li>
							<li style="float:right; width:300px;">
								<span style="float:right; width:275px">
									<input type="checkbox" style="margin:0px 3px 0 0; width:22px;" <?= ($rowsEntregaPadrao > 0) ? 'checked="checked"' : ''?> value="1" id="chk_local_entrega_diferente" name="chk_local_entrega_diferente">
									<label for="chk_local_entrega_diferente" style="margin-top:3px">Local de entrega diferente do destinat&aacute;rio</label>
								</span>
                    		</li>
						</ul>
                    </div>
                    <div id="pedido_nfe_transporte">
                    	<ul>
                 			<li>
                                <label for="sel_pedido_nfe_transporte_modalidade">Modalidade do frete</label>
                                <select id="sel_pedido_nfe_transporte_modalidade" name="sel_pedido_nfe_transporte_modalidade" class="sel_pedido_nfe_transporte_modalidade" title="Modalidade do frete">
<?									$rsTransporte = mysql_query("SELECT * FROM tblnfe_pedido_transporte");
                                    while($rowTransporte = mysql_fetch_array($rsTransporte)){
?>										<option <?=($modFrete == $rowTransporte['fldId'])? "selected = 'selected'":''?> value="<?= $rowTransporte['fldCodigo'] ?>"><?= $rowTransporte['fldCodigo'] ?> - <?= $rowTransporte['fldDescricao'] ?></option>
<?									}
?>                    			</select> 
                            </li>
                            <li>
                                <label for="sel_pedido_nfe_transportador">Transportador</label>
                                <select style="width: 250px" id="sel_pedido_nfe_transportador" name="sel_pedido_nfe_transportador" class="sel_pedido_nfe_transportador" title="Transportador">
                                	<option value="0">Selecionar</option>
<?									$rsTransportador = mysql_query("SELECT * FROM tbltransportador");
                                    while($rowTransportador = mysql_fetch_array($rsTransportador)){
?>										<option <?=($transportador == $rowTransportador['fldId'])? "selected = 'selected'":'' ?> value="<?= $rowTransportador['fldId'] ?>"><?= $rowTransportador['fldNome'] ?></option>
<?									}
?>                    			</select> 
                            </li>  
                            <li style="width:20px; margin-bottom:0">
                            	<input style="width:20px" type="checkbox" name="chk_nfe_transporte_icms_isento" id="chk_nfe_transporte_icms_isento" value="true" />
                            </li>
                            <li style="margin-bottom:0">
                            	<label for="chk_nfe_transporte_icms_isento">Isento do ICMS</label>
                            </li>
						</ul>
                                
                        <ul id="pedido_nfe_transporte_abas" class="menu_modo" style="width:470px;float:left">
                            <li style="margin-left:0;margin-right:0"><a href="volumes">Volumes</a></li>
                            <li style="margin-left:0;margin-right:0"><a href="retencao">Reten&ccedil;&atilde;o ICMS</a></li>
                            <li style="margin-left:0;margin-right:0"><a href="veiculo">Ve&iacute;culo/Reboque/Balsa/Vag&atilde;o</a></li>
                        </ul>
                        
                        <div id="pedido_nfe_transporte_volumes" style="float:left">
                        	<input class="hid_volume_controle" type="hidden" name="hid_volume_controle" id="hid_volume_controle" value="0" />
                            <div class="dados_listar">
                                <ul class="dados_cabecalho">
                                    <li style="width:40px">qtd</li>
                                    <li style="width:80px">esp&eacute;cie</li>
                                    <li style="width:90px">marca</li>
                                    <li style="width:40px">num.</li>
                                    <li style="width:80px">peso liq.</li>
                                    <li style="width:80px; border:0">peso bruto</li>
                                    <li style="width:75px; border:0">&nbsp;</li>
                                </ul>
                                
                                <div class="dados_listar_dados" id="dados_listar_volume"> 
                                    <div id="hidden"> 
                                        <ul class="volume_listar">
                                            <li style="width:40px"><input type="text" style="width:60px" class="txt_pedido_nfe_transporte_volume_qtd" 			name="txt_pedido_nfe_transporte_volume_qtd" 		value="" readonly="readonly" /></li>
                                            <li style="width:80px"><input type="text" style="width:80px" class="txt_pedido_nfe_transporte_volume_especie" 		name="txt_pedido_nfe_transporte_volume_especie" 	value="" readonly="readonly" /></li>
                                            <li style="width:90px"><input type="text" style="width:90px" class="txt_pedido_nfe_transporte_volume_marca" 		name="txt_pedido_nfe_transporte_volume_marca" 		value="" readonly="readonly" /></li>
                                            <li style="width:40px"><input type="text" style="width:60px" class="txt_pedido_nfe_transporte_volume_numero" 		name="txt_pedido_nfe_transporte_volume_numero" 		value="" readonly="readonly" /></li>
                                            <li style="width:80px"><input type="text" style="width:80px" class="txt_pedido_nfe_transporte_volume_peso_liquido" 	name="txt_pedido_nfe_transporte_volume_peso_liquido"value="" readonly="readonly" /></li>
                                            <li style="width:80px"><input type="text" style="width:80px" class="txt_pedido_nfe_transporte_volume_peso_bruto" 	name="txt_pedido_nfe_transporte_volume_peso_bruto" 	value="" readonly="readonly" /></li>
                                            <li style="width:22px"><a style="width:22px;height:22px;background-position:5px" name="" class="edit edit_volume modal" href="" title="editar" rel="620-450"></a></li>
                                            <li style="width:20px"><a style="height:22px" class="a_excluir" id="excluir_0" href="excluir_volume" title="Excluir item"></a></li>
                                        </ul>
                                    </div>
								</div>
                            </div>
                            <a style="float:right; margin: 10px" class="btn_novo modal" href="pedido_nfe_transporte_volume" rel="620-450">incluir</a>
                        </div>
                        
                        <div style="float: left" id="pedido_nfe_transporte_retencao">
                        	<ul class="dados_nfe">
                                <li style="width: 146px">
                                    <label for="txt_pedido_nfe_transporte_retencao_bc">Base de C&aacute;lculo</label>
                                    <input type="text" style="width:146px" id="txt_pedido_nfe_transporte_retencao_bc" name="txt_pedido_nfe_transporte_retencao_bc" value="" />
                                </li>
                                <li style="width: 146px">
                                    <label for="txt_pedido_nfe_transporte_retencao_aliquota">Al&iacute;quota</label>
                                    <input type="text" style="width:146px" id="txt_pedido_nfe_transporte_retencao_aliquota" name="txt_pedido_nfe_transporte_retencao_aliquota" value="" />
                                </li>
                                <li style="width: 146px">
                                    <label for="txt_pedido_nfe_transporte_retencao_valor_servico">Valor do Servi&ccedil;o</label>
                                    <input type="text" style="width:146px" id="txt_pedido_nfe_transporte_retencao_valor_servico" name="txt_pedido_nfe_transporte_retencao_valor_servico" value="" />
                                </li>
                               	<li>
                                    <label for="sel_pedido_nfe_transporte_retencao_uf">UF</label>
                                    <select class="sel_uf" name="sel_pedido_nfe_transporte_retencao_uf" id="sel_pedido_nfe_transporte_retencao_uf" style="width: 225px">
                                    	<option value="0">Selecionar</option>
<?										$rsUF = mysql_query("SELECT * FROM tblibge_uf ORDER BY fldSigla");
                                    	while($rowUF = mysql_fetch_array($rsUF)){
                                   	  		echo '<option value="'.$rowUF['fldCodigo'].'">'.$rowUF['fldSigla'].'</option>';
                                    	}
?>									</select>
                                </li>
                                <li>
                                    <label for="sel_pedido_nfe_transporte_retencao_municipio">Munic&iacute;pio</label>
                                    <select class="sel_municipio" name="sel_pedido_nfe_transporte_retencao_municipio" style="width: 225px"></select>
                                </li>
                                <li style="width: 320px">
                                    <label for="sel_pedido_nfe_transporte_retencao_cfop">CFOP</label>
                                    <select name="sel_pedido_nfe_transporte_retencao_cfop" style="width: 320px">
                                    	<option value="0"></option>
<?										$rsCFOP = mysql_query('SELECT * FROM tblnfe_cfop WHERE fldModo = 2');
										while($rowCFOP = mysql_fetch_array($rsCFOP)){
?>											<option value="<?=$rowCFOP['fldId']?>"><?=$rowCFOP['fldCFOP']?> - <?=$rowCFOP['fldDescricao']?></option>
<?                              		}
?>									</select>
                                </li>
                                <li style="width: 130px">
                                    <label for="txt_pedido_nfe_transporte_retencao_icms_retido">ICMS Retido</label>
                                    <input type="text" style="width:130px" id="txt_pedido_nfe_transporte_retencao_icms_retido" name="txt_pedido_nfe_transporte_retencao_icms_retido" value="" />
                                </li>
                            </ul>
                        </div>
                        
                    	<div id="pedido_nfe_transporte_veiculo" style="float:left">
                        	<ul style="border-bottom: 1px solid;">
                            	<li style="width: 20px; margin-bottom:0">
	                                <input type="radio" style="width:20px" name="rad_transporte" value="veiculo" checked="checked" />
                                </li>
                                <li style="width:120px;margin-bottom:0">
                                	<label for="rad_veiculo">Veiculo/Reboque</label>
                                </li>
                            	<li style="width: 20px; margin-bottom:0">
	                                <input type="radio" style="width:20px" name="rad_transporte" value="balsa" />
                                </li>
                                <li style="width:120px;margin-bottom:0">
                                	<label for="rad_veiculo">Balsa</label>
                                </li>
                            	<li style="width: 20px; margin-bottom:0">
	                                <input type="radio" style="width:20px" value="vagao" name="rad_transporte" />
                                </li>
                                <li style="width:120px;margin-bottom:0">
                                	<label for="rad_veiculo">Vag&atilde;o</label>
                                </li>
                            </ul>
                            
                            <div id="rad_veiculo" style="float:left">
                                <ul class="dados_nfe" style="margin-bottom:5px">
                                    <li style="width: 146px">
                                        <label for="txt_pedido_nfe_transporte_veiculo_placa">Placa</label>
                                        <input type="text" style="width:146px; text-align:left" id="txt_pedido_nfe_transporte_veiculo_placa" name="txt_pedido_nfe_transporte_veiculo_placa" value="" />
                                    </li>
                                    <li style="width: 100px">
                                        <label for="sel_pedido_nfe_transporte_veiculo_uf">UF</label>
                                        <select name="sel_pedido_nfe_transporte_veiculo_uf" id="sel_pedido_nfe_transporte_veiculo_uf" style="width: 100px">
                                            <option value="0">Selecionar</option>
<?											$rsUF = mysql_query("SELECT * FROM tblibge_uf ORDER BY fldSigla");
                                    		while($rowUF = mysql_fetch_array($rsUF)){
                                   	  			echo '<option value="'.$rowUF['fldCodigo'].'">'.$rowUF['fldSigla'].'</option>';
                                    		}
?>                                  	</select>
                                    </li>
                                    <li style="width: 146px">
                                        <label for="txt_pedido_nfe_transporte_veiculo_rntc">RNTC</label>
                                        <input type="text" style="width:146px; text-align:left" id="txt_pedido_nfe_transporte_veiculo_rntc" name="txt_pedido_nfe_transporte_veiculo_rntc" value="" />
                                    </li>
                                </ul>
                                <input class="hid_reboque_controle" type="hidden" name="hid_reboque_controle" id="hid_reboque_controle" value="0" />
                                <div class="dados_listar" style="height:80px">
                                    <ul class="dados_cabecalho">
                                        <li style="width:150px">Placa</li>
                                        <li style="width:80px">UF</li>
                                        <li style="width:247px">RNTC</li>
                                    </ul>
                                   
                                    <div class="dados_listar_dados" style="height:80px" id="dados_listar_reboque"> 
                                    	<div id="hidden">     
                                            <ul class="reboque_listar">
                                                <li style="width:150px"><input type="text" style="width:150px" class="txt_pedido_nfe_transporte_reboque_placa"					name="txt_pedido_nfe_transporte_reboque_placa" 	value="" readonly="readonly" /></li>
                                                <li style="width:80px"><input  type="text" style="width:80px; text-align:center" class="txt_pedido_nfe_transporte_reboque_uf" 	name="txt_pedido_nfe_transporte_reboque_uf" 	value="" readonly="readonly" /></li>
                                                <li><input  type="hidden" class="hid_pedido_nfe_transporte_reboque_uf" 	name="hid_pedido_nfe_transporte_reboque_uf" value="" 	readonly="readonly" /></li>
                                                <li style="width:180px"><input type="text" style="width:180px" class="txt_pedido_nfe_transporte_reboque_rntc" 					name="txt_pedido_nfe_transporte_reboque_rntc" 	value="" readonly="readonly" /></li>
                                                <li style="width:22px"><a style="width:22px;height:22px;background-position:5px" name="" class="edit edit_reboque modal" href="" title="editar" rel="470-150"></a></li>
                                                <li style="width:22px"><a style="height:22px" class="a_excluir excluir_reboque" id="excluir_0" href="" title="Excluir item"></a></li>
                                    		</ul>
                                    	</div>    
                                	</div>
                            	</div>
                            	<a style="float:right; margin: 10px" class="btn_novo" id="btn_reboque_novo" href="pedido_nfe_transporte_reboque" rel="470-150">incluir</a>
							</div>
                            <div id="rad_balsa" style="float:left">
                            	<ul class="dados_nfe">
                                    <li style="width: 420px">
                                        <label for="txt_pedido_nfe_transporte_balsa_identificacao">Identifica&ccedil;&atilde;o da Balsa</label>
                                        <input type="text" style="width:420px" id="txt_pedido_nfe_transporte_balsa_identificacao" name="txt_pedido_nfe_transporte_balsa_identificacao" />
                                    </li>
                                </ul>
                            </div>   
                            <div id="rad_vagao" style="float:left">
                            	<ul class="dados_nfe">
                                    <li style="width: 420px">
                                        <label for="txt_pedido_nfe_transporte_vagao_identificacao">Identifica&ccedil;&atilde;o do Vag&atilde;o</label>
                                        <input type="text" style="width:420px" id="txt_pedido_nfe_transporte_vagao_identificacao" name="txt_pedido_nfe_transporte_vagao_identificacao" />
                                    </li>
                                </ul>
                            </div>                                
                        </div>
                        
                    </div>
                    <div id="pedido_nfe_informacoes">
                    	<ul class="dados_nfe" style="margin-left:20px">
                        	<li style="width: 420px">
                                <label for="txt_pedido_nfe_fisco">Fisco</label>
                                <textarea style=" width:420px; height:100px" id="txt_pedido_nfe_fisco" name="txt_pedido_nfe_fisco"><?=$infoFisco?></textarea>
							</li>
                        	<li style="width: 420px">
                                <label for="txt_pedido_nfe_contribuite">Contribuinte</label>
                                <textarea style=" width:420px; height:100px" id="txt_pedido_nfe_contribuite" name="txt_pedido_nfe_contribuite"><?=$infoContribuinte?></textarea>
							</li>
						</ul>
                    </div>
                </div>  
                <div id="pedido_pagamento" style="margin-left:35px; width:445px">
                    <div class="pagamento_bottom">
                        <ul style="float:right; margin-right: 10px">
                            <li style="visibility: hidden;">
                                <label class="pedido" for="txt_pedido_produtos">Total Nota</label>
                            	<input type="text" id="txt_pedido_produtos" name="txt_pedido_produtos" value="<?=format_number_out($totalNota)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_servicos">Servi&ccedil;os</label>
                            	<input type="text" id="txt_pedido_servicos" name="txt_pedido_servicos" value="<?=format_number_out($total_pedido_servicos)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_terceiros">Terceiros</label>
                            	<input style="background:#FEF9BA; height:25px; font-size:18px" type="text" id="txt_pedido_terceiros" name="txt_pedido_terceiros" value="<?=format_number_out($total_pedido_terceiros)?>" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_subtotal">Subtotal</label>
                            	<input style="background:#D7F9C6; height:25px; font-size:18px" type="text" id="txt_pedido_subtotal" name="txt_pedido_subtotal" value="<?=format_number_out($pedidoSubTotal)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label for="txt_pedido_desconto">Desconto (%)</label>
                                <input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_desconto" name="txt_pedido_desconto" value="0,00"  />
                            </li>
                            <li>
                                <label for="txt_pedido_desconto_reais">Desconto (R$)</label>
                                <input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_desconto_reais" name="txt_pedido_desconto_reais" value="<?=format_number_out($total_pedido_descontos)?>" />
                            </li>
							<li>
								<label for="txt_pedido_nfe_total_desconto">Desconto em itens</label>
								<input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_nfe_total_desconto" name="txt_pedido_nfe_total_desconto" value="<?=format_number_out($total_nota_desconto)?>" readonly="readonly" />
							</li>
                            <li>
                                <label class="total" for="txt_pedido_total">Total</label>
                                <input type="text" class="total" id="txt_pedido_total" name="txt_pedido_total" value="<?=format_number_out($pedidoTotal)?>" readonly="readonly" />
                            </li>
                            
<?							if($nfe_parcelamento_exibir == '1'){                            
?>								<li>
									<a style="margin:0; float:right" class="modal btn_cheque" name="btn_cheque" id="btn_cheque" title="exibir cheques" href="financeiro_cheque_listar" rel="780-410">cheques</a>
								</li>
<?							}
?>								
						</ul>
                    </div>
<?					//if($nfe_devolucao != 'true'){
						require_once('componentes/parcelamento/nfe_vendas_importadas.php');
					//}
?>
				</div>
<?
				//verifica��o de n�mero de NFe pulado
				require_once('componentes/nfe/tabela_nnfe_faltando.php');
?>				
                <div style="float:right">
					<input type="hidden" name="hid_id_vendas_origens" id="hid_id_vendas_origens" value="<?= implode(",", $id_query_array); ?>" />
                    <input type="submit" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
                </div>
            </form>
        </div>
<?	
	}
?>  </div>
	
    <script type="text/javascript">
	
        $('div#dados_nfe div').hide();
        $('div#pedido_nfe_totais').show();
        $('div#pedido_nfe_totais div#totais_icms').show();
        
        $('ul#pedido_nfe_abas a[href=totais]').addClass('ativo');
        $('ul#pedido_nfe_totais_abas a[href=icms]').addClass('ativo');
		$('div#pedido_nfe_totais input:first').focus();
		
        $('ul#pedido_nfe_abas a').click(function(event){
            event.preventDefault();
            //marcar aba ativa
            $('ul#pedido_nfe_abas a').removeClass('ativo');
            $(this).addClass('ativo');
            
            //ocultar todas as abas
            $('div#dados_nfe div').hide();
			
            //exibir a selecionada
            var aba = $(this).attr('href');
			
			if(aba == 'transporte'){
				$('div#pedido_nfe_transporte div#pedido_nfe_transporte_volumes').show();
				$('div#pedido_nfe_transporte div#pedido_nfe_transporte_volumes div').show();
				$('div.dados_listar_dados div#hidden').hide(); // pra esconder div hidden
			}
			else if(aba == 'totais'){
				$('div#pedido_nfe_totais div#totais_icms').show();
           		$('ul#pedido_nfe_totais_abas a').removeClass('ativo');
            	$('ul#pedido_nfe_totais_abas  a[href=icms]').addClass('ativo');
			}
			
            $('div#pedido_nfe_'+aba).show();
			$('div#pedido_nfe_'+aba+' input:first, div#pedido_nfe_'+aba+' textarea:first').focus();
        });
		
		//#############################################################################################################################################
		$('ul#pedido_nfe_totais_abas a').click(function(event){
            event.preventDefault();
            //marcar aba ativa
            $('ul#pedido_nfe_totais_abas a').removeClass('ativo');
            $(this).addClass('ativo');
            
            //ocultar todas as abas
            $('div#pedido_nfe_totais div').hide();
			
            //exibir a selecionada
            var aba = $(this).attr('href');
			
            $('div#totais_'+aba).show();
			$('div#totais_'+aba+' input:first, div#totais_'+aba+' textarea:first').focus();
        });
		
		//#############################################################################################################################################
		$('div#pedido_nfe_transporte div').hide();
        $('ul#pedido_nfe_transporte_abas a[href=volumes]').addClass('ativo');
		
		$('ul#pedido_nfe_transporte_abas a').click(function(event){
            event.preventDefault();
			
            //marcar aba ativa
            $('ul#pedido_nfe_transporte_abas a').removeClass('ativo');
            $(this).addClass('ativo');
           
            //ocultar todas as abas
            $('div#pedido_nfe_transporte div').hide();
			
            //exibir a selecionada
            var aba = $(this).attr('href');
            $('div#pedido_nfe_transporte_'+aba).show();
            $('div#pedido_nfe_transporte_'+aba+' div').show();
			$('div.dados_listar_dados div#hidden').hide(); // pra esconder div hidden
			$('div#pedido_nfe_transporte_'+aba+' input:first, div#pedido_nfe_transporte_'+aba+' textarea:first').focus();
       
	   });
		
    </script>