
<h2>Editar Fornecedor</h2>

<div id="principal">

<?	
	
	echo mysql_error();
	if(isset($_POST["btn_enviar"])){

		$NomeFantasia = mysql_escape_string($_POST['txt_nome_fantasia']);
		$RazaoSocial =  mysql_escape_string($_POST['txt_razao_social']);
		$Data = date("Y-m-d");
		
		if($_POST['txt_nascimento_abertura'] != ''){
			$Abertura = format_date_in($_POST['txt_nascimento_abertura']);
		}else{
			$Abertura = '';
		}

		$IE 			= $_POST['txt_rg_ie'];
		$Fone1 			= $_POST['txt_telefone1'];
		$Fone2 			= $_POST['txt_telefone2'];
		$Fax 			= $_POST['txt_fax'];
		$Email 			= $_POST['txt_email'];
		$Site 			= $_POST['txt_website'];
		$Endereco 		= mysql_escape_string($_POST['txt_endereco']);
		$Numero 		= $_POST['txt_numero'];
		$Complemento 	= $_POST['txt_complemento'];
		$Bairro 		= mysql_escape_string($_POST['txt_bairro']);
		$Cep 			= $_POST['txt_cep'];
		$Municipio_Cod 	= $_POST['txt_municipio_codigo'];
		$Tipo 			= $_POST['sel_tipo'];
		$Observacao		= $_POST['txt_observacao'];
		
		$CPF_CNPJ = formatCPFCNPJTipo_in($_POST['txt_cpf_cnpj'], $Tipo);
		
		$sqlInsert = "update tblfornecedor set
		fldNomeFantasia 		= '$NomeFantasia', 
		fldRazaoSocial 			= '$RazaoSocial',
		fldTipo 				= '$Tipo', 
		fldCadastroData 		= '$Data', 
		fldNascimento_Abertura 	= '$Abertura',
		fldCPF_CNPJ 			= '$CPF_CNPJ', 
		fldIE 					= '$IE', 
		fldTelefone1 			= '$Fone1',
		fldTelefone2 			= '$Fone2',
		fldFax 					= '$Fax', 
		fldEmail 				= '$Email', 
		fldWebsite 				= '$Site', 
		fldEndereco 			= '$Endereco', 
		fldNumero 				= '$Numero',
		fldComplemento 			= '$Complemento',
		fldBairro 				= '$Bairro',
		fldCEP 					= '$Cep',
		fldObservacao 			= '$Observacao',
		fldMunicipio_Codigo 	= '$Municipio_Cod'
		where fldId 			= $fornecedor_id";
			
		echo mysql_error();
		
		if (mysql_query($sqlInsert)){
			header("location:index.php?p=fornecedor&mensagem=ok");
		}
		else {
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
				<a class="voltar" href="index.php?p=fornecedor">voltar</a>
			</div>
<?			echo mysql_error();
		}
	}
	else{
		
		$tela_id = 2;
		
		if($rowFornecedor['fldCPF_CNPJ'] != ''){
			if($rowFornecedor['fldTipo'] == 1){
				$CPF_CNPJ = format_cpf_out($rowFornecedor['fldCPF_CNPJ']);
			}elseif($rowFornecedor['fldTipo'] == 2){
				$CPF_CNPJ = format_cnpj_out($rowFornecedor['fldCPF_CNPJ']);
			}
		}else{
			$CPF_CNPJ = '';
		}
		
?>		<div class="form">
            <form id="frm_fornecedor" class="frm_detalhe" style="width:890px" action="" method="post">
                <ul>
                    <li class="form">
                        <label for="txt_codigo">Codigo</label>
                        <input type="text" style="width: 60px" id="txt_codigo" name="txt_codigo" value="<?=$rowFornecedor['fldId']?>" disabled="disabled" />
                    </li>
                    <li class="form">
                        <label for="txt_cadastro">Cadastrado em</label>
                        <input type="text" style="width: 90px; text-align:center" id="txt_cadastro" name="txt_cadastro" value="<?=format_date_out($rowFornecedor['fldCadastroData'])?>" readonly="readonly" />
                    </li>
                    <li class="form">
                        <label for="txt_nome_fantasia">Nome Fantasia</label>
                        <input <?=formPermissao("txt_nome_fantasia",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:265px;" id="txt_nome_fantasia" name="txt_nome_fantasia" value="<?=$rowFornecedor['fldNomeFantasia']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_razao_social">Raz&atilde;o Social</label>
                        <input <?= formPermissao("txt_razao_social",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:265px;" id="txt_razao_social" name="txt_razao_social" value="<?=$rowFornecedor['fldRazaoSocial']?>" />
                    </li>
                    <li class="form">
                        <label for="sel_tipo">Tipo</label>
                        <select style="width:110px" id="sel_tipo" name="sel_tipo" >
                            <option <?=($rowFornecedor['fldTipo'] == 1) ? 'selected="selected"' : '' ?> value="1">F&iacute;sica</option>
                            <option <?=($rowFornecedor['fldTipo'] == 2) ? 'selected="selected"' : '' ?> value="2">Jur&iacute;dica</option>
                        </select>
                    </li>
                    <li class="form">
                        <label for="txt_cpf_cnpj">CPF/CNPJ</label>
                        <input <?=formPermissao("txt_cpf_cnpj",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:160px;" id="txt_cpf_cnpj" name="txt_cpf_cnpj" value="<?=$CPF_CNPJ?>" />
                        <div class="clear"></div>
                    </li>
                    <li class="form">
                        <label for="txt_rg_ie">Inscri&ccedil;&atilde;o Estadual</label>
                        <input <?= formPermissao("txt_rg_ie",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:160px;" id="txt_rg_ie" name="txt_rg_ie" value="<?=$rowFornecedor['fldIE']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_nascimento_abertura">Nascimento/Abertura</label>
                        <input <?= formPermissao("txt_nascimento_abertura",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:130px;" id="txt_nascimento_abertura" name="txt_nascimento_abertura" value="<?=format_date_out($rowFornecedor['fldNascimento_Abertura'])?>" />
                    </li>
                    <li class="form">
                        <label for="txt_telefone1">Telefone (ddd)</label>
                        <input <?= formPermissao("txt_telefone1",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:110px;" id="txt_telefone1" name="txt_telefone1" value="<?=$rowFornecedor['fldTelefone1']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_telefone2">Telefone 2 (ddd)</label>
                        <input type="text" style=" width:110px;" id="txt_telefone2" name="txt_telefone2" value="<?=$rowFornecedor['fldTelefone2']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_fax">Fax</label>
                        <input type="text" style=" width:105px;" id="txt_fax" name="txt_fax" value="<?=$rowFornecedor['fldFax']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_email">E-mail</label>
                        <input <?= formPermissao("txt_email",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:160px" id="txt_email" name="txt_email" value="<?=$rowFornecedor['fldEmail']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_website">Website</label>
                        <input type="text" style=" width:160px;" id="txt_website" name="txt_website" value="<?=$rowFornecedor['fldWebsite']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_endereco">Endere&ccedil;o</label>
                        <input <?= formPermissao("txt_endereco",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:280px;" id="txt_endereco" name="txt_endereco" value="<?=$rowFornecedor['fldEndereco']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_numero">N&uacute;mero</label>
                        <input <?= formPermissao("txt_numero",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:60px;" id="txt_numero" name="txt_numero" value="<?=$rowFornecedor['fldNumero']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_complemento">Complemento</label>
                        <input type="text" style=" width:130px;" id="txt_complemento" name="txt_complemento" value="<?=$rowFornecedor['fldComplemento']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_bairro">Bairro</label>
                        <input <?= formPermissao("txt_bairro",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:100px;" id="txt_bairro" name="txt_bairro" value="<?=$rowFornecedor['fldBairro']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_cep">CEP</label>
                        <input <?= formPermissao("txt_cep",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:120px;" id="txt_cep" name="txt_cep" value="<?=$rowFornecedor['fldCEP']?>" />
                    </li>
                    <li class="form">
                        <label for="txt_municipio_codigo">C&oacute;d. Munic&iacute;pio</label>
                        <input <?= formPermissao("txt_municipio_codigo",$tela_id) ? print "class='obrigatorio'" : "" ?> type="text" style=" width:60px;" id="txt_municipio_codigo" name="txt_municipio_codigo" value="<?=$rowFornecedor['fldMunicipio_Codigo']?>" />
                        <a href="municipio_busca" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
                    
<?					$Mcodigo = substr($rowFornecedor['fldMunicipio_Codigo'],2,5);
                    $UFCodigo = substr($rowFornecedor['fldMunicipio_Codigo'],0,2);
                    
                    $rsMunicipio = mysql_query("select * from tblibge_municipio where fldCodigo = '" . $Mcodigo . "' and fldUF_Codigo =".$UFCodigo);
                    $rowMunicipio = mysql_fetch_array($rsMunicipio);
                                
                    $rsUF = mysql_query("select * from tblibge_uf where fldCodigo = '" . $UFCodigo . "'");
                    $rowUF = mysql_fetch_array($rsUF);
?>
                    <li class="form">
                        <label for="txt_municipio">Munic&iacute;pio</label>
                        <input type="text" style=" width:210px; background:#EAEAEA;" id="txt_municipio" name="txt_municipio" readonly="readonly" value="<?=$rowMunicipio['fldNome']?>" />
                    </li>
                    <li class="form">
                        <label id="disabled" for="txt_uf">UF</label>
                        <input type="text" style=" width:50px; background:#EAEAEA;" id="txt_uf" name="txt_uf" readonly="readonly" value="<?=$rowUF['fldSigla']?>" />
                    </li>
					
					<li class="form">
						<label for="txt_observacao">Observa&ccedil;&atilde;o</label>
						<textarea name="txt_observacao" style="width: 860px; height:100px;"><?=$rowFornecedor['fldObservacao'];?></textarea>
					</li>
			
					<div style="float:right; margin:-25px 15px 0 0">
					
						<li style="margin-top:0;">
							<input type="submit" class="btn_enviar" style="margin-top:14px" name="btn_enviar" id="btn_enviar" value="salvar" title="Salvar" />
						</li>
						<li style="margin-top:0;">
							<a style="margin-top:14px" href="index.php?p=funcionario" class="btn_cancel">cancelar</a>
						</li>
					
					</div>
					
                </ul>
            </form>
       </div>
<?	}
?>        
</div>

<script>
	
	$("#txt_cpf_cnpj").change(function(){
		//BLOQUEIA CADASTRO DE CPF/CNPJ DUPLICADO
		var cpf = $('input#txt_cpf_cnpj').val();
		exp = /\.|-/g;
		cpf = cpf.toString().replace(exp, "");
		
		$.post('filtro_ajax.php',{checar_cpfcnpj: cpf, tabela: 'tblfornecedor'},function(data){
			if(cpf != ''){
				if(data == 'erro'){
					$('input#txt_cpf_cnpj').attr('value', '');
					$('input#txt_cpf_cnpj').focus();
					alert('CPF ja cadastrado!');
				}
			}
		})
	})

</script>