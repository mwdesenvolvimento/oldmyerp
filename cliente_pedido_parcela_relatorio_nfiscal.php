<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
        <title>myERP - Relat&oacute;rio de Parcelas</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css"></link>
 
	</head>
	<body>
        
<?	
		$clienteId = $_GET['cliente_id'];
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		require_once("inc/fnc_imprimir.php");
		require_once("inc/fnc_identificacao.php");
		
		
		$impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
		if(!$impressao_local){$impressao_local = fnc_estacao_impressora('todos');}
		#CAMINHO DA IMPRESSORA
		$texto 		= $impressao_local." \r\n";
		
		$rsEmpresa 	= mysql_query("select * from tblempresa_info");
		$rowEmpresa = mysql_fetch_array($rsEmpresa);
		$CPF_CNPJ 	= formatCPFCNPJTipo_out($rowEmpresa['fldCPF_CNPJ'], $rowEmpresa['fldTipo']);
		$sqlCliente = "SELECT * FROM tblcliente WHERE fldId = ".$_GET['cliente_id'];
		$rsCliente	= mysql_query($sqlCliente);
		$rowCliente	= mysql_fetch_array($rsCliente);
		
		$cliente_id = $rowCliente['clienteId'];
		$data 		= date("Y-m-d");
		$hora 		= date("H:i:s");

		$usuario_sessao = $_SESSION['usuario_id'];
		$remote_name 	= gethostbyaddr(gethostbyname($REMOTE_ADDR));
		$identificacao 	= fnc_identificacao($remote_name);
		$identificacao 	= acentoRemover($identificacao);
		
		$texto .= format_margem_print(acentoRemover($rowEmpresa['fldNome_Fantasia']), 40, 'centro')." \r \r\n";
		$texto .="Fone Fax: ". $rowEmpresa['fldTelefone1']." \r\n";
		$texto .="Data: ".format_date_out(date("Y-m-d"))." Hora: ".date("H:i:s")." \r\n";
		$texto .="Estacao de trabalho: ".$identificacao." \r\n";
		$texto .="Cliente: ".acentoRemover($rowCliente['fldNome']);
		$texto .="\r\n\r\n";
		
		$sqlParcela = $_SESSION['parcela_relatorio'] ;
		$rsParcela 	= mysql_query($sqlParcela);
		
		while($rowParcela 	= mysql_fetch_array($rsParcela)){	
			$sql			= mysql_query("SELECT COUNT(*) as totalParcelas FROM tblpedido_parcela WHERE fldPedido_Id = ".$rowParcela['fldPedido_Id']);
			//$rowsParcela	= mysql_fetch_array($sql);
			
			$ultimaData = $rowParcela['fldVencimento'];
			$parcela_num = str_pad($rowParcela['fldParcela'], 2, "0", STR_PAD_LEFT).'/'.str_pad($rowsParcela['totalParcelas'], 2, "0", STR_PAD_LEFT);
			$texto .= "---------------------------------------- \r\n";
			$texto .= format_margem_print('venda', 17, 'direita');
			$texto .= format_margem_print('parc', 7, 'direita');
			$texto .= format_margem_print('vencto', 8, 'direita');
			$texto .= format_margem_print('valor', 8, 'direita')." \r\n";

			$texto .= format_margem_print(str_pad($rowParcela['fldPedido_Id'],5,'0', STR_PAD_LEFT), 17, 'direita');
			$texto .= format_margem_print($parcela_num, 7, 'direita');
			$texto .= format_margem_print(format_date_out4($rowParcela['fldVencimento']), 8, 'direita');
			$texto .= format_margem_print(format_number_out($rowParcela['fldValor']), 8, 'direita')." \r\n";
						
			$sqlBaixa = "SELECT * FROM tblpedido_parcela_baixa WHERE fldParcela_Id = ".$rowParcela['fldId']." AND fldExcluido = 0 ORDER BY fldId";
			$rsBaixa 			= mysql_query($sqlBaixa);
			$totalBaixas 		= mysql_num_rows($rsBaixa);
			echo mysql_error();
			
			$valorDevedor 		= $rowParcela['fldValor'];
			$devedorAnterior 	= 0;
			$parcelaAnterior 	= 0;
			$baixaAnterior 		= 0;
			$valorOld 			= 0;
			
			$totalParcela += $rowParcela['fldValor'];
			
			#EXIBE AS BAIXAS SE HOUVER E VALOR DO JUROS 
			//if($totalBaixas > 0 ){
				
				$texto .= format_margem_print('baixa', 5, 'direita');
				$texto .= format_margem_print('juros', 6, 'direita');
				$texto .= format_margem_print('multa', 6, 'direita');
				$texto .= format_margem_print('desc', 7, 'direita');
				$texto .= format_margem_print('pago', 8, 'direita');
				$texto .= format_margem_print('devedor', 8, 'direita')."\r\n";
				
				while($rowBaixa = mysql_fetch_array($rsBaixa)){
					
					$ultimaData = $rowBaixa['fldDataRecebido'];
					//confere se ainda esta descrevendo a mesma parcela que anterior, pra pegar o devedor anterior
					if($rowParcela['fldPedido_Id'].$rowParcela['fldParcela'] != $baixaAnterior){
						$devedorAnterior 	= 0;
						$baixaAnterior 		= 0;
					}
					
					#PEGA OS VALORES DE BAIXAS ANTERIORES
					$sql		= mysql_query("SELECT SUM(fldValor) as fldValor, SUM(fldDesconto) as fldDesconto, SUM(fldMulta) as fldMulta, SUM(fldJuros) as fldJuros, fldId FROM tblpedido_parcela_baixa 
										WHERE fldParcela_Id = ".$rowBaixa['fldParcela_Id']." AND fldExcluido = 0 AND fldId < ".$rowBaixa['fldId']." ORDER BY fldId");
					$old 		= mysql_fetch_array($sql);
					
					#SOMA VALOR PAGO ANTERIORMENTE ATE ESSA BAIXA | TOTAL PAGO ANTERIOR + ATUAL
					$valorOld 	= $old['fldValor'] + $old['fldJuros'] + $old['fldMulta'];
					$valorPago 	= $rowBaixa['fldValor'] + $rowBaixa['fldJuros' ] + $rowBaixa['fldMulta'];
					$totalPago 	= $valorOld + $valorPago;
					$valorMulta = $rowBaixa['fldMulta'];
					
					if($devedorAnterior > 0){
						$valorJuros 	= $rowBaixa['fldDevedor'] - (($devedorAnterior + $old['fldDesconto']) - ($rowBaixa['fldValor'] + $rowBaixa['fldMulta'] + $rowBaixa['fldDesconto']));
						$valorReceber 	= $devedorAnterior + $valorJuros + $rowBaixa['fldMulta'];
					}else{
						$valorJuros 	= ($rowBaixa['fldDevedor'] + $rowBaixa['fldValor'] + $rowBaixa['fldDesconto']) - $rowParcela['fldValor'];
						$valorReceber 	= number_format((($valorMulta + $valorJuros + $rowParcela['fldValor']) - $valorOld),2,'.','');
					}
					
					$valorJuros 	= ($rowBaixa['fldJuros'] > 0) ? $rowBaixa['fldJuros'] :'0.00';	
					$valorDevedor 	= (($valorReceber + $valorJuros) - $valorPago) - $rowBaixa['fldDesconto'];
					$valorDesconto 	= $rowBaixa['fldDesconto'];
					
					//tipo de pagamento
					$rowPagamentoTipo = mysql_fetch_array(mysql_query("SELECT tblfinanceiro_conta_fluxo.fldPagamento_Tipo_Id, tblpagamento_tipo.* 
								FROM tblfinanceiro_conta_fluxo 
								LEFT JOIN tblpagamento_tipo ON tblfinanceiro_conta_fluxo.fldPagamento_Tipo_Id = tblpagamento_tipo.fldId
								WHERE tblfinanceiro_conta_fluxo.fldReferencia_Id = ".$rowBaixa['fldId']));
					#VERIFICA SE CHEGOU AO LIMITE DE LINHAS PRA TROCAR DE FOLHA
					$data_baixa = format_date_out4($rowBaixa['fldDataRecebido']);
					$texto .= format_margem_print($data_baixa, 5, 'direita');
					$texto .= format_margem_print(format_number_out($valorJuros), 6, 'direita');
					$texto .= format_margem_print(format_number_out($valorMulta), 6, 'direita');
					$texto .= format_margem_print(format_number_out($valorDesconto), 7, 'direita');
					$texto .= format_margem_print(format_number_out($valorPago), 8, 'direita');
					$texto .= format_margem_print(format_number_out($valorDevedor), 8, 'direita')."\r\n";

					$devedorAnterior 	= $rowBaixa['fldDevedor'];
					$baixaAnterior 		= $rowBaixa['fldValor'];
					$baixaAnterior 		= $rowParcela['fldPedido_Id'].$rowParcela['fldParcela'];
					
					$totalJuros 	+= $valorJuros ;
					$totalMulta 	+= $valorMulta;
					$totalDesconto 	+= $valorDesconto;
					$totalBaixa		+= $valorPago;
					
				}// end while
			//}//end if
			
			#DEIXO UMA LINHA DE REGISTRO PARA EXIBIR O DEVEDOR, CASO AJA JUROS, JA FAZ O CALCULO PARA VALOR NO DIA ################################
			if($valorDevedor > 0 && fnc_sistema('pedido_juros') > 0){
				$juros 			= fnc_sistema('pedido_juros');
				$multa	 		= fnc_sistema('pedido_multa');
				$jurosTipo 		= fnc_sistema('pedido_juros_tipo');
				
				$dias 			= intervalo_data($ultimaData, date("Y-m-d"));
				$juros_ad 		= number_format((($juros / 30) * $dias), 2, '.', ''); 				// pega o juros ao mes, e divide pelo numero de dias que esta atrasado
				$valorJuros 	= number_format((($juros_ad * $valorDevedor) / 100), 2, '.', ''); 	// valor do juros calculado com o ultimo valor da parcela, o valor total antes da ultima baixa
				
				$valorJuros 	= ($valorJuros > 0) ? $valorJuros :'0.00'; //caso nao tenha vencido aina, o juros fica negativo, dando diferença no calculo do devedor
				$valorMulta 	= ($valorMulta) ? 0 : $multa;	//se ja tiver cobrado multa na baixa, nao cobrar de novo, se nao houver baxa ainda, cobrar multa
				$valorDevedor 	= $valorJuros + $valorDevedor + $valorMulta;
				
				$texto .= format_margem_print('----', 5, 'direita');
				$texto .= format_margem_print(format_number_out($valorJuros), 6, 'direita');
				$texto .= format_margem_print(format_number_out($valorMulta), 6, 'direita');
				$texto .= format_margem_print('------', 7, 'direita');
				$texto .= format_margem_print('------', 8, 'direita');
				$texto .= format_margem_print(format_number_out($valorDevedor), 8, 'direita')."\r\n";
				
				$totalJuros 	+= $valorJuros;
				$totalMulta 	+= $valorMulta;
			}//end if
			
			#######################################################################################################################################
			$totalDevedor 	+= $valorDevedor;
			
			
		}//end while
		$totalReceber	= ($totalParcela - $totalDesconto) + $totalMulta + $totalJuros;
		/*** gravando no txt ********************************************************/
		$texto .= " \r\n---------------------------------------- \r\n\n";
		$texto 	.= "                 Parcelas";
		$texto	.= format_margem_print(format_number_out($totalParcela),15, 'direita')."\r\n";
		$texto 	.= "                    Multa";
		$texto	.= format_margem_print(format_number_out($totalMulta),15, 'direita')."\r\n";
		$texto 	.= "                    Juros";
		$texto	.= format_margem_print(format_number_out($totalJuros),15, 'direita')."\r\n";
		$texto 	.= "                 Desconto";
		$texto	.= format_margem_print(format_number_out($totalDesconto),15, 'direita')."\r\n";
		$texto 	.= "                    Total";
		$texto	.= format_margem_print(format_number_out($totalReceber),15, 'direita')."\r\n";
		$texto 	.= "                    Baixa";
		$texto	.= format_margem_print(format_number_out($totalBaixa),15, 'direita')."\r\n";
		$texto 	.= "                  Devedor";
		$texto	.= format_margem_print(format_number_out($totalDevedor),15, 'direita')."\r\n";
		$texto .="\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n";

		/*****************************************************************************************************************************************************************************************/
		$timestamp  = date("Ymd_His");
		$local_file = "impressao///inbox///imprimir_recibo_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		$fp			= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
		$salva 		= fwrite($fp, $texto);
		fclose($fp);
?>
		<script type="text/javascript">
			var local = '<?= $local_file ?>';
			window.location=local;
			//window.close();
        </script>


	</body>
</html>
