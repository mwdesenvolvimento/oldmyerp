<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Fardos</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
        
	</head>
	<body>
<? 
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados = mysql_fetch_array(mysql_query("SELECT * FROM tblempresa_info"));
		
		$CPF_CNPJDados = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		
		$rowUsuario = mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		

		$rsFardo 	= mysql_query("SELECT tblproduto_fardo.*,
								  	SUM(tblpedido_item.fldQuantidade * (tblpedido_item.fldExcluido * -1 + 1)) AS fldSaida
									FROM tblproduto_fardo 
									LEFT JOIN tblpedido_item ON tblproduto_fardo.fldId = tblpedido_item.fldFardo_Id
									LEFT JOIN tblproduto ON tblproduto_fardo.fldProduto_Id = tblproduto.fldId
									WHERE tblproduto_fardo.fldExcluido = '0' 
									GROUP BY tblproduto_fardo.fldId
									ORDER BY tblproduto_fardo.fldId DESC 
									".$_SESSION['filtro_produto']);
			
		$rowsFardo 	= mysql_num_rows($rsFardo);
		echo mysql_error();
					echo $rowsFardo;				
		$limite = 42;
		$n = 1;
		
		$pgTotal = $rowsFardo / $limite;
		$p = 1;
		
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Fardos</h1></td>
                <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                            <td style="width: 200px;">Local do estoque: '.$localEstoque['fldNome'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="table_relatorio" summary="Relat&oacute;rio">
                        <tr style="border:none">
                            <td style="width:200px; text-align:right">C&oacute;digo</td>	
							<td style="width:200px; text-align:center">Descri&ccedil;&atilde;o</td>
							<td style="width:118px; text-align:right">Quantidade</td>
							<td style="width:116px; text-align:right">Sa&iacute;das</td>
							<td style="width:116px; text-align:right">Saldo</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
?>

   		<table class="relatorio_print" style="page-break-before:avoid">
<?			print $tabelaCabecalho;
			$tabelaCabecalho 	= '<table class="relatorio_print">'.$tabelaCabecalho;
			$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
			while($rowFardo = mysql_fetch_array($rsFardo)){
				echo mysql_error();
				$x+= 1;
				$saldo = $rowFardo['fldQuantidade'] - $rowFardo['fldSaida'];
				
				$quantidadeTotal += $rowFardo['fldQuantidade'];
				$saidaTotal 	 += $rowFardo['fldSaida'];
				$saldoTotal 	 += $saldo;
?>                    
				<tr>
                	<td style="width:200px; text-align:right"><?=$rowFardo['fldCodigo']?></td>
                    <td style="width:200px"><?=substr($rowFardo['fldNome'],0, 50)?></td>
                    <td style="width:118px;text-align:right"><?=format_number_out($rowFardo['fldQuantidade'], $quantidadeDecimal)?></td>
                    <td style="width:116px;text-align:right;color:#C00"><?=format_number_out($rowFardo['fldSaida'], $quantidadeDecimal)?></td>
                    <td style="width:116px;text-align:right"><?=format_number_out($saldo)?></td>
				</tr>
<?				if(($n == $limite) or ($x == $rowsFardo)){
?>							</table>    
                            </td>
                        </tr>
                    </table>
<?					$n = 1;
					if($x < $rowsFardo){
						$p += 1;
						print $tabelaCabecalho;
					}
				}else{
					$n += 1;
				}
			}
?>
            <table class="table_relatorio_rodape" summary="Relat&oacute;rio">
                <tr>
                    <td style="width:220px">&nbsp;</td>
                    <td style="width:100px">Quantidade Total</td>
                    <td style="width:60px; text-align:right"><?=format_number_out($quantidadeTotal)?></td>
                    <td style="width:10px; border-left:1px solid">&nbsp;</td>
                    <td style="width:100px">Total Sa&iacute;das</td>
                    <td style="width:60px; text-align:right"><?=format_number_out($saidaTotal)?></td>
                    <td style="width:10px; border-left:1px solid">&nbsp;</td>
                    <td style="width:100px">Total Saldo</td>
                    <td style="width:60px; text-align:right"><?=format_number_out($saldoTotal)?></td>
                </tr>
            </table> 
	</body>
</html>	
