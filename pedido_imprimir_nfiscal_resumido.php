<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
      	<title>myERP - Imprimir venda</title>
        
	</head>
	<body>

<?php	ob_start();
		session_start();
		
		require_once("inc/con_db.php");
		require_once("inc/fnc_ibge.php");
		require_once("inc/fnc_general.php");
		require_once("inc/fnc_imprimir.php");
		require_once("inc/fnc_identificacao.php");
		$usuario_sessao = $_SESSION['usuario_id'];
		
		$local_id = $_POST['hid_impressao_local_id'];
		$impressao_local = fnc_estacao_impressora($local_id);
		
		$texto 		= $impressao_local." \r\n";
	
		$pedido_id  = $_GET['id'];
		$raiz 		= $_GET['raiz'];
		$data 		= date("Y-m-d");
		
		$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
		
		
		$rsPedido  			= mysql_query("SELECT tblpedido.*, SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) as fldTotalItem,
								(SELECT SUM(fldValor) FROM tblpedido_funcionario_servico WHERE fldFuncao_Tipo = 2 AND fldPedido_Id = tblpedido.fldId) as fldTotalServico,
								tblpedido.fldVeiculo_Id, tblpedido.fldEndereco AS fldEnderecoPedido, tblpedido.fldReferencia, tblpedido.fldPedidoData, tblpedido.fldServico, tblpedido.fldDependente_Id, tblpedido.fldDesconto, tblpedido.fldValor_Terceiros, tblpedido.fldId as fldPedidoId, tblpedido.fldDescontoReais, tblcliente.*, tblcliente.fldId AS clienteID, tblcliente_dados_adicionais.fldResponsavel, tblpedido_otica.fldResponsavel as fldResponsavel_check
								FROM tblpedido 
								LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
								LEFT JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								LEFT JOIN tblcliente_dados_adicionais on tblcliente.fldId = tblcliente_dados_adicionais.fldCliente_Id
								LEFT JOIN tblpedido_otica ON tblpedido.fldId = tblpedido_otica.fldPedido_Id
								WHERE tblpedido.fldId = '$pedido_id' GROUP BY tblpedido_item.fldPedido_Id");
		$rowPedido 			= mysql_fetch_assoc($rsPedido);
		/*----------------------------------------------------------------------------------------------*/

		$texto .="Data: ".format_date_out(date("Y-m-d"))." Hora: ".date("H:i:s")." \r\n\r\n";
		$texto .="Num. venda: ".str_pad($rowPedido['fldPedidoId'], 6, "0", STR_PAD_LEFT)."     Comanda: ".$rowPedido['fldComanda_Numero']." \r\n";
		
		$texto .="\r\n\r\nProduto             Quantidade\r\n";
		$texto .="----------------------------------------\r\n";
						
		$rsItem = mysql_query("SELECT tblpedido_item.*, tblproduto.fldCodigo 
							  FROM tblpedido_item INNER JOIN tblproduto ON tblpedido_item.fldProduto_Id = tblproduto.fldId
							  WHERE tblpedido_item.fldExcluido = '0' AND tblpedido_item.fldPedido_Id = '".$pedido_id."'");
		while($rowItem = mysql_fetch_array($rsItem)){
			$quantidade = format_number_out($rowItem['fldQuantidade']);
			$descricao 	= $rowItem['fldDescricao']." x$quantidade";
		
			$x = 0; //quebrando linha a cada 29 caracteres
			while(strlen(substr($descricao, $x * 30, 30)) > 0){
				$texto .= substr(acentoRemover($descricao),$x * 30, 30) . "\r\n";
				$x +=1;
			}
		}
	
		#########################################################################################################################################################
		
		$timestamp  = date("Ymd_His");
		//$local_file = "impressao\inbox\imprimir_pedido_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		$local_file = "impressao///inbox///imprimir_pedido_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		$fp 		= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
		$salva		= fwrite($fp, $texto);
		fclose($fp);
?>    
	<script type="text/javascript">
		window.close();
	</script>
</html>	
