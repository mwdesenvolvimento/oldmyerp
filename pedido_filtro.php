<?php

	$statusFiltro = fnc_sistema('pedido_filtro'); 
	
	// pra pegar os valores padrao do bd
	$_SESSION['sel_pedido_status_pagamento']	= (isset($_SESSION['sel_pedido_status_pagamento'])) ? $_SESSION['sel_pedido_status_pagamento'] 	: $statusFiltro;
	$_SESSION['sel_pedido_periodo_status'] 		= (isset($_SESSION['sel_pedido_periodo_status']))	? $_SESSION['sel_pedido_periodo_status'] 	: 'emitido';
	
	$_SESSION['chk_nfe_venda'] 					= (isset($_SESSION['chk_nfe_venda']))	? $_SESSION['chk_nfe_venda'] : true ;
	$_SESSION['chk_nfe_saida'] 					= (isset($_SESSION['chk_nfe_saida']))	? $_SESSION['chk_nfe_saida'] : true ;
	
	//recebendo a data do calendário de período
	if(isset($_POST['txt_calendario_data_inicial']) && isset($_POST['txt_calendario_data_final'])){
	    $_SESSION['txt_data1'] = (isset($_POST['txt_calendario_data_inicial'])) ? $_POST['txt_calendario_data_inicial'] : '';
	    $_SESSION['txt_data2'] = (isset($_POST['txt_calendario_data_final']))	? $_POST['txt_calendario_data_final'] 	: '';
	}
	
	$_SESSION['txt_data1'] 	= (isset($_SESSION['txt_data1']))	? $_SESSION['txt_data1'] : date('d') . '/' . date('m/Y', mktime(0, 0, 0, date('m') - 3));
	$_SESSION['txt_data2'] 	= (isset($_SESSION['txt_data2']))	? $_SESSION['txt_data2'] : date('d/m/Y');
	
	
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_pedido_ref_venda'] 			= "";
		$_SESSION['txt_pedido_cod_venda'] 			= "";
		$_SESSION['sel_pedido_status'] 				= "status";
		$_SESSION['sel_pedido_periodo_status'] 		= "emitido";
		$_SESSION['sel_pedido_status_pagamento'] 	= $statusFiltro;
		$_SESSION['txt_pedido_cliente'] 			= "";
		$_SESSION['sel_pedido_funcionario'] 		= "";
		$_SESSION['sel_pedido_marcador'] 			= "";
//		$_SESSION['sel_pedido_convertido'] 			= "1";
		$_SESSION['chk_nfe_venda']					= true;
		$_SESSION['chk_nfe_saida']					= true;
		$_SESSION['chk_nfe_entrada']				= false;
		$_SESSION['chk_nfe_importada']				= false;
		$_SESSION['txt_pedido_veiculo_placa'] 		= "";
		$_SESSION['txt_data1'] 						= date('d') . '/' . date('m/Y', mktime(0, 0, 0, date('m') - 3));
		$_SESSION['txt_data2'] 						= date('d/m/Y');
		$_SESSION['chk_cliente'] 					= false;
		$_SESSION['sel_venda_fatura'] 				= "";
	}elseif(isset($_POST['btn_exibir'])){
		$_SESSION['txt_pedido_ref_venda'] 			= (isset($_POST['txt_pedido_ref_venda']) 		? $_POST['txt_pedido_ref_venda'] 		: $_SESSION['txt_pedido_ref_venda']);
		$_SESSION['txt_pedido_cod_venda'] 			= (isset($_POST['txt_pedido_cod_venda']) 		? $_POST['txt_pedido_cod_venda'] 		: $_SESSION['txt_pedido_cod_venda']);
		$_SESSION['sel_pedido_status'] 				= (isset($_POST['sel_pedido_status']) 			? $_POST['sel_pedido_status'] 			: $_SESSION['sel_pedido_status']);
		$_SESSION['sel_pedido_periodo_status'] 		= (isset($_POST['sel_pedido_periodo_status']) 	? $_POST['sel_pedido_periodo_status'] 	: $_SESSION['sel_pedido_periodo_status']);
		$_SESSION['sel_pedido_status_pagamento']	= (isset($_POST['sel_pedido_status_pagamento']) ? $_POST['sel_pedido_status_pagamento'] : $_SESSION['sel_pedido_status_pagamento']);
		$_SESSION['txt_pedido_cliente'] 			= (isset($_POST['txt_pedido_cliente']) 			? $_POST['txt_pedido_cliente'] 			: $_SESSION['txt_pedido_cliente']);
		$_SESSION['sel_pedido_funcionario'] 		= (isset($_POST['sel_pedido_funcionario']) 		? $_POST['sel_pedido_funcionario'] 		: $_SESSION['sel_pedido_funcionario']);
		$_SESSION['sel_pedido_marcador'] 			= (isset($_POST['sel_pedido_marcador']) 		? $_POST['sel_pedido_marcador'] 		: $_SESSION['sel_pedido_marcador']);
		//$_SESSION['sel_pedido_convertido'] 			= (isset($_POST['sel_pedido_convertido']) 		? $_POST['sel_pedido_convertido'] 		: $_SESSION['sel_pedido_convertido']);
		$_SESSION['txt_pedido_veiculo_placa'] 		= (isset($_POST['txt_pedido_veiculo_placa']) 	? $_POST['txt_pedido_veiculo_placa'] 	: $_SESSION['txt_pedido_veiculo_placa']);
		$_SESSION['txt_data1'] 						= (isset($_POST['txt_data1'])					? $_POST['txt_data1']					: $_SESSION['txt_data1']);
		$_SESSION['txt_data2'] 						= (isset($_POST['txt_data2'])					? $_POST['txt_data2']					: $_SESSION['txt_data2']);
		$_SESSION['sel_venda_fatura'] 				= (isset($_POST['sel_venda_fatura'])			? $_POST['sel_venda_fatura']			: $_SESSION['sel_venda_fatura']);

		$_SESSION['chk_cliente'] 					= (isset($_POST['chk_cliente']))				?  true									: false;
		$_SESSION['chk_nfe_venda'] 					= (isset($_POST['chk_nfe_venda']))				?  true									: false;
		$_SESSION['chk_nfe_saida'] 					= (isset($_POST['chk_nfe_saida']))				?  true									: false;
		$_SESSION['chk_nfe_entrada']				= (isset($_POST['chk_nfe_entrada']))			?  true									: false;
		$_SESSION['chk_nfe_importada']				= (isset($_POST['chk_nfe_importada']))			?  true									: false;
		
	}
	

//	echo 'saida2='.$_POST['chk_nfe_saida'];
	
?>
<form id="frm-filtro" action="index.php?p=<?=$_GET['p']?>" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
    	<li class="large_height">
<?			$codigo_pedido = ($_SESSION['txt_pedido_cod_venda'] ? $_SESSION['txt_pedido_cod_venda'] : "c&oacute;digo");
			($_SESSION['txt_pedido_cod_venda'] == "código") ? $_SESSION['txt_pedido_cod_venda'] = '' : '';
?>      	<input style="width:60px" type="text" name="txt_pedido_cod_venda" id="txt_pedido_cod_venda" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo_pedido?>"/>
		</li>
        <li class="large_height" >
			<input type="checkbox" name="chk_cliente" id="chk_cliente" <?=($_SESSION['chk_cliente'] == true ? 'checked ="checked"' : '')?>/>
<?			$cliente = ($_SESSION['txt_pedido_cliente'] 	? $_SESSION['txt_pedido_cliente'] : "cliente");
			($_SESSION['txt_pedido_cliente'] == "cliente") 	? $_SESSION['txt_pedido_cliente'] = '' : '';
?>      	<input style="width: 200px" type="text" name="txt_pedido_cliente" id="txt_pedido_cliente" onfocus="limpar (this,'cliente');" onblur="mostrar (this, 'cliente');" value="<?=$cliente?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
        <li class="large_height">
            <select style="width:180px" id="sel_pedido_funcionario" name="sel_pedido_funcionario" >
                <option value="">funcion&aacute;rio</option>
<?				$rsFuncionario = mysql_query("select * from tblfuncionario ORDER BY fldNome ASC");
				while($rowFuncionario= mysql_fetch_array($rsFuncionario)){
?>					<option <?=($_SESSION['sel_pedido_funcionario'] == $rowFuncionario['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowFuncionario['fldId'] ?>"><?= $rowFuncionario['fldNome'] ?></option>
<?				}
?>			</select>
		</li>
        <li class="large_height"><!--Lucas -->
      		<label for="sel_pedido_periodo_status">Per&iacute;odo: </label>
            <select id="sel_pedido_periodo_status" name="sel_pedido_periodo_status" style="width: 80px" >
            	<option <?=($_SESSION['sel_pedido_periodo_status'] == 'emitido') ? 'selected="selected"' : '' ?> value="emitido">Emiss&atilde;o</option>
<?				$rsStatus = mysql_query("SELECT * FROM tblpedido_status");
				while($rowStatus = mysql_fetch_array($rsStatus)){
?>              	<option <?=($_SESSION['sel_pedido_periodo_status'] == $rowStatus['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowStatus['fldId']?>"><?=$rowStatus['fldStatus']?></option>
<?				}
?>			</select>
		</li>
        <li class="large_height">
<?			$data1 = ($_SESSION['txt_data1'] ? $_SESSION['txt_data1'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 70px" type="text" name="txt_data1" id="txt_data1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
		
		<li class="large_height">
<?			$data2 = ($_SESSION['txt_data2'] ? $_SESSION['txt_data2'] : "");
?>     		<input title="Data final" style="text-align:center;width: 70px" type="text" name="txt_data2" id="txt_data2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,pedido" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
        <li class="large_height">
<?			$referencia_pedido = ($_SESSION['txt_pedido_ref_venda'] ? $_SESSION['txt_pedido_ref_venda'] : "referencia");
			($_SESSION['txt_pedido_ref_venda'] == "referencia") ? $_SESSION['txt_pedido_ref_venda'] = '' : '';
?>      	<input style="width: 70px" type="text" name="txt_pedido_ref_venda" id="txt_pedido_ref_venda" onfocus="limpar (this,'referencia');" onblur="mostrar (this, 'referencia');" value="<?=$referencia_pedido?>"/>
		</li>
        <li>
            <select id="sel_pedido_status" name="sel_pedido_status" style="width:85px" >
            	<option <?=($_SESSION['sel_pedido_status'] == 'status') ? 'selected="selected"' : '' ?> value="status">status</option>
<?				$rsStatus = mysql_query("SELECT * FROM tblpedido_status");
				while($rowStatus = mysql_fetch_array($rsStatus)){
?>              	<option <?=($_SESSION['sel_pedido_status'] == $rowStatus['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowStatus['fldId']?>"><?=$rowStatus['fldStatus']?></option>
<?				}
?>			</select>
		</li>
        <li>
            <select style="width:205px" id="sel_pedido_marcador" name="sel_pedido_marcador" >
                <option value="">marcador</option>
<?				$rsMarcador = mysql_query("SELECT * FROM tblpedido_marcador WHERE fldExcluido = 0 ORDER BY fldMarcador ASC");
				while($rowMarcador = mysql_fetch_array($rsMarcador)){
?>					<option <?=($_SESSION['sel_pedido_marcador'] == $rowMarcador['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowMarcador['fldId'] ?>"><?= $rowMarcador['fldMarcador'] ?></option>
<?				}
?>			</select>
		</li>
        <li>
            <select id="sel_pedido_status_pagamento" name="sel_pedido_status_pagamento" style="width:120px" >
                <option <?=($_SESSION['sel_pedido_status_pagamento'] == "aberto") 	? 'selected="selected"' : '' ?> value="aberto"	>em aberto	 </option>
                <option <?=($_SESSION['sel_pedido_status_pagamento'] == "total") 	? 'selected="selected"' : '' ?> value="total"	>pago total	 </option>
                <option <?=($_SESSION['sel_pedido_status_pagamento'] == "parcial") 	? 'selected="selected"' : '' ?> value="parcial"	>pago parcial</option>
                <option <?=($_SESSION['sel_pedido_status_pagamento'] == "vencidos") ? 'selected="selected"' : '' ?> value="vencidos">vencidos	 </option>
                <option <?=($_SESSION['sel_pedido_status_pagamento'] == "todos") 	? 'selected="selected"' : '' ?> value="todos">	 pagamento	 </option>
			</select>
		</li>
        <li>
            <select id="sel_venda_fatura" name="sel_venda_fatura" style="width:105px" >
                <option value="">fatura</option>
                <option <?=($_SESSION['sel_venda_fatura'] == "1") ? 'selected="selected"' : '' ?> value="1">faturado</option>
                <option <?=($_SESSION['sel_venda_fatura'] == "0") ? 'selected="selected"' : '' ?> value="0">não faturado</option>
			</select>
		</li>
       	<li>
<?			if($_SESSION['sistema_tipo'] == 'automotivo'){
				$veiculo_placa = ($_SESSION['txt_pedido_veiculo_placa'] ? $_SESSION['txt_pedido_veiculo_placa'] : "placa");
				($_SESSION['txt_pedido_veiculo_placa'] == "placa") 		? $_SESSION['txt_pedido_veiculo_placa'] = '' : '';
?>      		<input style="width:75px" type="text" name="txt_pedido_veiculo_placa" id="txt_pedido_veiculo_placa" onfocus="limpar (this,'placa');" onblur="mostrar (this, 'placa');" value="<?=$veiculo_placa?>"/>
<?			}
?>		</li>
<?		($_SESSION["sistema_nfe"] > 0) ? print "<li ><a href='' id='filtro_nfe_exibir' style='width:50px;display:block' class='desc'><small style='margin-left:15px;text-decoration:underline'>mais</small></a></li>" : '' ; ?>
        <li style="float:right">
	        <button type="submit" name="btn_exibir" style="margin:0" title="Exibir">Exibir</button>
        </li>
        <li style="float:right">
        	<button type="submit" name="btn_limpar" style="margin:0" title="Limpar Filtro">Limpar filtro</button>
        </li>
        
<?		if($_SESSION["sistema_nfe"] > 0){                    
?>
            <fieldset id="nfe_filtro" style=" background:none;width:635px;margin-left:10px;display:none"><legend class="none" >NFe</legend>
               
                <li>
                    <input type="checkbox" style="width:25px;float:left" name="chk_nfe_venda" id="chk_nfe_venda" <?=($_SESSION['chk_nfe_venda'] == true ? 'checked ="checked"' : (!isset($_SESSION['chk_nfe_venda'])) ? 'checked ="checked"'  : '')?> />
               		<span style="float:left">Vendas</span>
               	</li>
                <li>
                    <input type="checkbox" style="width:25px;float:left" name="chk_nfe_saida" id="chk_nfe_saida" <?=($_SESSION['chk_nfe_saida'] == true ?  'checked ="checked"' : '')?> />
                    <span style="float:left">NFe sa&iacute;da</span>
                </li>
                <li>
                    <input type="checkbox" style="width:25px;float:left" name="chk_nfe_entrada" id="chk_nfe_entrada" <?=($_SESSION['chk_nfe_entrada'] == true ? 'checked ="checked"' : '')?> />
                    <span style="float:left">NFe entrada</span>
                </li>
                <li>
                    <input type="checkbox" style="width:25px;float:left" name="chk_nfe_importada" id="chk_nfe_importada" <?=($_SESSION['chk_nfe_importada'] == true ? 'checked ="checked"' : '')?> />
                    <span style="float:left">Vendas importadas</span>
                </li>
			</fieldset>
<?		}
?>      
    </ul>
  </fieldset>
</form>

<? 
	
	if(($_SESSION['txt_pedido_cod_venda']) != ""){ $filtro .= " AND tblpedido.fldId = '".$_SESSION['txt_pedido_cod_venda']."'"; }
	
	if(($_SESSION['txt_pedido_ref_venda']) != ""){
		
		$filtro .= " AND fldReferencia = '".$_SESSION['txt_pedido_ref_venda']."'";
	}
	
	if(($_SESSION['sel_pedido_status']) != ""){
		if(($_SESSION['sel_pedido_status']) != "status"){
			$filtro .= " AND tblpedido.fldStatus = '".$_SESSION['sel_pedido_status']."'";
		}
	}
	
	if(($_SESSION['sel_venda_fatura']) != ""){
		$filtro .= " AND tblpedido.fldFaturado = '".$_SESSION['sel_venda_fatura']."'";
	}
	
	
	
	#AQUI FOI ALTERADO, ANTES USAVA HAVING, AGORA ESTOU USANDO JUNTO AO WHERE, POIS ESTA PEGANDO OS CAMPOS JA CALCULADOS DA VIEW, COMO UM CAMPO COMUM
	if(($_SESSION['sel_pedido_status_pagamento']) != ""){
		//$filtro_excluido .= " and (tblpedido_parcela.fldExcluido is null or tblpedido_parcela.fldExcluido = 0)";
		switch($_SESSION['sel_pedido_status_pagamento']){
			case "aberto":
				#$filtro_having .= " HAVING (fldValorBaixa = 0 or fldValorBaixa is NULL)";
				$filtro .= " AND 
							((SELECT SUM((tblpedido_parcela_baixa.fldValor - tblpedido_parcela_baixa.fldJuros) * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) 
							FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId 
							WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0) = NULL OR
							(SELECT SUM((tblpedido_parcela_baixa.fldValor - tblpedido_parcela_baixa.fldJuros) * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) 
							FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId 
							WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0) = 0)";
			break;
			
			case "total":
				#$filtro_having .= " HAVING (fldValorBaixa + fldBaixaDesconto) >= fldTotalParcelas";
				$filtro .= " AND ((SELECT SUM((tblpedido_parcela_baixa.fldValor - tblpedido_parcela_baixa.fldJuros) * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) 
							FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId 
							WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0) + (SELECT SUM( tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) 
							FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId 
							WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0)) = (SELECT SUM( tblpedido_parcela.fldValor	* (tblpedido_parcela.fldExcluido * -1 + 1)) 
							FROM tblpedido_parcela WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0)";

			break;

			case "parcial":
				#$filtro_having .= " HAVING fldTotalParcelas > (fldValorBaixa + fldBaixaDesconto) AND fldValorBaixa > 0";
				$filtro .= " AND (SELECT SUM( tblpedido_parcela.fldValor * (tblpedido_parcela.fldExcluido * -1 + 1)) FROM tblpedido_parcela 
							WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0) > ((SELECT SUM((tblpedido_parcela_baixa.fldValor - tblpedido_parcela_baixa.fldJuros) * 
							(tblpedido_parcela_baixa.fldExcluido * -1 + 1)) FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON 
							tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId WHERE fldPedido_Id = tblpedido.fldId AND 
							fldCredito = 0) + (SELECT SUM( tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) 
							FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = 
							tblpedido_parcela.fldId WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0)) AND (SELECT SUM((tblpedido_parcela_baixa.fldValor - 
							tblpedido_parcela_baixa.fldJuros) * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) FROM tblpedido_parcela_baixa 
							RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId WHERE fldPedido_Id = tblpedido.fldId AND 
							fldCredito = 0) > 0";
			break;

			case "vencidos":
				#$filtro_having .= " HAVING ((fldTotalParcelas > (fldValorBaixa + fldBaixaDesconto)) or fldValorBaixa is Null) AND fldVencimento < '".date("Y-m-d")."'";
				$filtro .= " AND (((SELECT SUM( tblpedido_parcela.fldValor * (tblpedido_parcela.fldExcluido * -1 + 1)) 
							FROM tblpedido_parcela WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0) > ((SELECT SUM((tblpedido_parcela_baixa.fldValor - tblpedido_parcela_baixa.fldJuros) * 
							(tblpedido_parcela_baixa.fldExcluido * -1 + 1)) FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = 
							tblpedido_parcela.fldId WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0) + (SELECT SUM( tblpedido_parcela_baixa.fldDesconto * 
							(tblpedido_parcela_baixa.fldExcluido * -1 + 1)) FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = 
							tblpedido_parcela.fldId WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0))) or (SELECT SUM((tblpedido_parcela_baixa.fldValor - tblpedido_parcela_baixa.fldJuros) * 
							(tblpedido_parcela_baixa.fldExcluido * -1 + 1)) FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = 
							tblpedido_parcela.fldId WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0) is NULL) AND (SELECT MAX(fldVencimento) FROM tblpedido_parcela 
							WHERE tblpedido_parcela.fldPedido_Id = tblpedido.fldId AND fldCredito = 0) < '".date("Y-m-d")."'";
			break;

			case "todos":
			break;
		}
	}
	
				
	if(($_SESSION['sel_pedido_funcionario']) != ""){
		#verifica se ja esta usando o HAVING do filtro anterior
		if($_SESSION['sel_pedido_status_pagamento'] != 'todos'){
			$filtro .= " AND ((SELECT tblfuncionario.fldId FROM tblfuncionario INNER JOIN tblpedido_funcionario_servico ON tblfuncionario.fldId = tblpedido_funcionario_servico.fldFuncionario_Id
							 AND tblpedido_funcionario_servico.fldFuncao_Tipo = 1 WHERE tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId GROUP BY 
							 tblpedido_funcionario_servico.fldFuncionario_Id, tblpedido_funcionario_servico.fldPedido_Id LIMIT 1) = ".$_SESSION['sel_pedido_funcionario']." 
						OR (SELECT tblfuncionario.fldId FROM tblfuncionario INNER JOIN tblpedido_funcionario_servico ON tblfuncionario.fldId = tblpedido_funcionario_servico.fldFuncionario_Id
						AND tblpedido_funcionario_servico.fldFuncao_Tipo = 2 WHERE tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId GROUP BY tblpedido_funcionario_servico.fldFuncionario_Id,
						tblpedido_funcionario_servico.fldPedido_Id LIMIT 1) = ".$_SESSION['sel_pedido_funcionario'].")";
		} else {
			$filtro .= " AND (SELECT tblfuncionario.fldId FROM tblfuncionario INNER JOIN tblpedido_funcionario_servico ON tblfuncionario.fldId = tblpedido_funcionario_servico.fldFuncionario_Id
						 AND tblpedido_funcionario_servico.fldFuncao_Tipo = 1 WHERE tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId GROUP BY 
						 tblpedido_funcionario_servico.fldFuncionario_Id, tblpedido_funcionario_servico.fldPedido_Id LIMIT 1) = ".$_SESSION['sel_pedido_funcionario'];
		}
	}
	
	if(format_date_in($_SESSION['txt_data1']) != "" || format_date_in($_SESSION['txt_data2']) != ""){
		if(($_SESSION['sel_pedido_periodo_status']) != "emitido"){
				if($_SESSION['txt_data1'] != "" && $_SESSION['txt_data2'] != ""){
					$filtro_data .= " AND tblpedido_status_historico.fldData BETWEEN '".format_date_in($_SESSION['txt_data1'])."' AND '".format_date_in($_SESSION['txt_data2'])."'" ;
				}elseif($_SESSION['txt_data1'] != "" && $_SESSION['txt_data2'] == ""){
					$filtro_data .= " AND tblpedido_status_historico.fldData >= '".format_date_in($_SESSION['txt_data1'])."'";
				}elseif($_SESSION['txt_data2'] != "" && $_SESSION['txt_data1'] == ""){
					$filtro_data .= " AND tblpedido_status_historico.fldData <= '".format_date_in($_SESSION['txt_data2'])."'";
				}
				$filtro_data .= " AND tblpedido_status_historico.fldId  IN (SELECT MAX(fldId) FROM tblpedido_status_historico WHERE fldStatus_Id = '".$_SESSION['sel_pedido_periodo_status']."' AND fldPedido_Id = tblpedido.fldId) "; //ULTIMO RESULTADO
		}else{
				if($_SESSION['txt_data1'] != "" && $_SESSION['txt_data2'] != ""){
					$filtro_data .= " AND fldPedidoData BETWEEN '".format_date_in($_SESSION['txt_data1'])."' AND '".format_date_in($_SESSION['txt_data2'])."'";
				}elseif($_SESSION['txt_data1'] != "" && $_SESSION['txt_data2'] == ""){
					$filtro_data .= " AND fldPedidoData >= '".format_date_in($_SESSION['txt_data1'])."'";
				}elseif($_SESSION['txt_data2'] != "" && $_SESSION['txt_data1'] == ""){
					$filtro_data .= " AND fldPedidoData <= '".format_date_in($_SESSION['txt_data2'])."'";
				}
		}		
	}
	
	if(($_SESSION['txt_pedido_cliente']) != ""){
		$cliente = addslashes($_SESSION['txt_pedido_cliente']); // no caso de aspas, pra nao dar erro na consulta
		
		if($_SESSION['chk_cliente'] == true){
			$filtro .= " AND (tblcliente.fldNome like '%$cliente%' OR tblcliente.fldNomeFantasia like '%$cliente%' OR tblNome2.fldNome LIKE '%$cliente%' OR tblNome2.fldNomeFantasia LIKE '%$cliente%')";
		}else{
			$filtro .= " AND (tblcliente.fldNome like '$cliente%'  OR tblcliente.fldNomeFantasia like '$cliente%'  OR tblNome2.fldNome LIKE '$cliente%'  OR tblNome2.fldNomeFantasia LIKE '$cliente%')";
		}
	}
	
	if(($_SESSION['txt_pedido_veiculo_placa']) != ""){
		$placa 		= $_SESSION['txt_pedido_veiculo_placa'];
		
		$filtro 	.= " AND fldPlaca = '$placa'";
	}
	
	if(($_SESSION['sel_pedido_marcador']) != ""){
		$filtro .= " AND fldMarcador_Id = '".$_SESSION['sel_pedido_marcador']."'";
	}
	/*
	switch($_SESSION['sel_pedido_convertido']){
		
		case '1':
			$filtro .= " AND (fldPedido_Destino_Nfe_Id IS NULL OR fldPedido_Destino_Nfe_Id <= 0)";
		break;
		case '2':
			$filtro .= " AND (fldPedido_Destino_Nfe_Id > 0)";
		break;
		case '3':
			$filtro .= "";
		break;
	}
	*/
	
	#FILTRO NFe
	if($_SESSION["sistema_nfe"] > 0){ 
			
		#unset($filtro_having);
		if(($_SESSION['chk_nfe_importada'] == true || $_SESSION['chk_nfe_venda'] == true) && $_SESSION['chk_nfe_entrada'] == false &&  $_SESSION['chk_nfe_saida'] == false){
			$filtro_nfe = " AND tblpedido.fldTipo_Id != 3";
		}elseif($_SESSION['chk_nfe_venda'] == false && $_SESSION['chk_nfe_importada'] == false){
			$filtro_nfe = " AND tblpedido.fldTipo_Id = 3";
		}else{
			$filtro_nfe = '';
		}
		
		
		if($_SESSION['chk_nfe_entrada'] == false && $_SESSION['chk_nfe_saida'] == true){
			$filtro_having .= " HAVING tblpedido_fiscal.fldOperacao_Tipo = 1";
		}elseif($_SESSION['chk_nfe_entrada'] == true && $_SESSION['chk_nfe_saida'] == false){
			$filtro_having .= " HAVING tblpedido_fiscal.fldOperacao_Tipo = 0";
		}elseif($_SESSION['chk_nfe_entrada'] == true || $_SESSION['chk_nfe_saida'] == true){
			$filtro_having .= " HAVING tblpedido_fiscal.fldOperacao_Tipo < 2";
		}
		
		
		if($_SESSION['chk_nfe_importada'] == true && $_SESSION['chk_nfe_venda'] == false){
			$filtro 	.= " AND (fldPedido_Destino_Nfe_Id > 0 OR tblpedido.fldTipo_Id = 3)";
			if($_SESSION['chk_nfe_entrada'] == false || $_SESSION['chk_nfe_saida'] == false){
				$filtro_having 	.= (isset($filtro_having)) ? " OR " : " HAVING ";
				$filtro_having	.= " fldPedido_Destino_Nfe_Id > 0";
			}
			
		}elseif($_SESSION['chk_nfe_importada'] == false && $_SESSION['chk_nfe_venda'] == true){
			$filtro 	.= " AND (fldPedido_Destino_Nfe_Id IS NULL OR fldPedido_Destino_Nfe_Id <= 0)";
			if($_SESSION['chk_nfe_entrada'] == true || $_SESSION['chk_nfe_saida'] == true){
				$filtro_having 	.= (isset($filtro_having)) ? " OR " : " HAVING ";
				$filtro_having	.= "(tblpedido.fldTipo_Id !=3 AND (fldPedido_Destino_Nfe_Id IS NULL OR fldPedido_Destino_Nfe_Id <= 0))";
			}
		}elseif($_SESSION['chk_nfe_importada'] == true && $_SESSION['chk_nfe_venda'] == true){
			if($_SESSION['chk_nfe_entrada'] == false || $_SESSION['chk_nfe_saida'] == false ){
				$filtro_having 	.= (isset($filtro_having)) ? " OR " : " HAVING ";
				$filtro_having	.= " tblpedido.fldTipo_Id != 3";
			}
		}
		
		$filtro .= $filtro_nfe;
	}else{
		$filtro .= " AND fldTipo_Id !=3";
	}
	//transferir para a sessão
	$_SESSION['filtro_pedido'] = $filtro.$filtro_data." GROUP BY  tblpedido.fldId " . $filtro_status . " ".$filtro_having;
?>


    <script>

		$('#filtro_nfe_exibir').click(function(event){
    		event.preventDefault();
		 	$("#nfe_filtro").slideToggle(35);
		});
    </script>