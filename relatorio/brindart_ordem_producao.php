<?	

	
	############################################## REQUIRES ######################################################
	require("inc/relatorio.php");
	require("inc/fnc_general.php");
	
	$pdf = new PDF('P', 'mm', 'A4');
	//pode ser P (portrait) ou L (landscape)
	
	$pdf->AliasNbPages();
	//isso faz com que funcione o total de paginas
	
	$pdf->SetMargins(6,15,6);
	//defino as margens do documento...
	
	$pdf->AddPage();

	
	
	
        $pdf->SetFont('Arial', 'B',10);
	$pdf->Cell(160,7,"ORDEM DE PRODUÇÃO",'L R B T','','C');
        $pdf->Image('logo.jpg',172,12,27,22);
        $pdf->SetFont('Arial','B',9);
        
        $pdf->Ln();
            $pdf->Cell(10,6,"N.OP:",'L  B');
            $pdf->SetFont('Arial', '',9);
            $pdf->Cell(150,6,"<N. OP>",' R B'); //numero da op 
            
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(35,6,"DATA ABERTURA:",'L B');
            $pdf->SetFont('Arial', '',9);
            $pdf->Cell(125,6,"<DATA ABERTURA>",' R B');//data abertura
            
        $pdf->Ln();
        $pdf->Cell(130,6,"",'L R  B');
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(27,6,"PRIORIDADE:",'T B');
            $pdf->SetFont('Arial', '',9);
            $pdf->Cell(41,6,"<PRIORIDADE>",'R T B');//prioridade
            
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(35,6,"DATA CONCLUSÃO:",'L B');
            $pdf->SetFont('Arial', '',9);
            $pdf->Cell(95,6,"<DATA CONCLUSÃO>",'R B'); // data conclusão
            
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(27,6,"SITUAÇÃO:",'T B');
            $pdf->SetFont('Arial', '',9);
            $pdf->Cell(41,6,"<SITUAÇÃO>",'R T B'); //situação
        
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(16,9,"CLIENTE:",'L');
            $pdf->SetFont('Arial', '',9);
            $pdf->Cell(152,9,"<NOME CLIENTE>");  //nome cliente
        
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(30,9,"PEDIDO CLIENTE:",'L R',"",'C');
        
        $pdf->Ln(9);
        $pdf->Cell(16,9,"CIDADE:",'L B');
            $pdf->SetFont('Arial','',9);
            $pdf->Cell(152,9,"<CIDADE>"); //nome cidade
        
        $pdf->Cell(30,9,"<PEDIDO>",'L R B',"",'C'); //pedido cliente
        
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(198,7,"ITENS PRODUZIDOS",1,"",'C');
        
    ##################################### Itens Produzidos #############################
        $pdf->Ln();
        $pdf->Cell(16,6,"PEDIDO",'L R B',"",'C');
        $pdf->Cell(16,6,"LOTE",'L R B',"",'C');
        $pdf->Cell(21,6,"QTDE",'L R B',"",'C');
        $pdf->Cell(13,6,"UN",'L R B',"",'C');
        $pdf->Cell(20,6,"COD ITEM",'L R B',"",'C');
        $pdf->Cell(90,6,"ITEM",'L R B',"",'C');
        $pdf->Cell(11,6,"LAR",'L R B',"",'C');
        $pdf->Cell(11,6,"ALT",'L R B',"",'C');
        
    ##################################################################################
        
        $pdf->Ln();
            $pdf->SetFont('Arial', '',8);
            $pdf->Cell(16,6,"<PEDIDO>",'L R B',"",'C');
            $pdf->Cell(16,6,"<LOTE>",'L R B',"",'C');
            $pdf->Cell(21,6,"<QTDE>",'L R B',"",'C');
            $pdf->Cell(13,6,"<UN>",'L R B',"",'C');
            $pdf->Cell(20,6,"COD. ITEM>",'L R B',"",'C');
            $pdf->Cell(90,6,"<ITEM>",'L R B',"",'C');
            $pdf->Cell(11,6,"<LAR>",'L R B',"",'C');
            $pdf->Cell(11,6,"<ALT>",'L R B',"",'C'); 
        
        $pdf->Ln();
        $pdf->Cell(198,5,"",1);
         
    ##################################### Calculodo Papel ####################################
        
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(198,7,"CÁLCULO DO PAPEL",1,'','C');
            $pdf->SetFont('Arial', 'B',8);
      
     
            $pdf->Ln();
            $pdf->Cell(13,6,"ITEM",'L R B','','C');
            $pdf->Cell(75,6,"MATERIAL",'L R B','','C');
            $pdf->Cell(23,6,"LARGURA(cm)",'L R B','','C');
            $pdf->Cell(18,6,"COMP.(m)",'L R B','','C');
            $pdf->Cell(18,6,"% ACRESC.",'L R B','','C');
            $pdf->Cell(24,6,"COMP.FINAL(m)",'L R B','C');
            $pdf->Cell(27,6,"QUANTIDADE M^2",'L R B','C');
    #####################################################################################
           
           
           
            $pdf->Ln();
                $pdf->SetFont('Arial', '',8);
                $pdf->Cell(13,6,"<ITEM>",'L R B','','C');
                $pdf->Cell(75,6,"<MATERIAL>",'L R B','','C');
                $pdf->Cell(23,6,"<LARGURA>",'L R B','','C');
                $pdf->Cell(18,6,"<COMP.>",'L R B','','C');
                $pdf->Cell(18,6,"<ACRESC.>",'L R B','','C');
                $pdf->Cell(24,6,"<COMP.FINAL>",'L R B','C');
                $pdf->Cell(27,6,"<QUANTIDADE>",'L R B','C');
                
    ######################################## Tintas e Cromias #############################
        $pdf->Ln();
            $pdf->SetFont('Arial','B',9);
            $pdf->Cell(198,6,"TINTAS E CROMIAS",1,'','C');
                $pdf->Ln();
                $pdf->SetFont('Arial','B',8);
                $pdf->Cell(198,6,"TINTAS",1,'','C');
                    $pdf->SetFont('Arial', 'B',8);
                    $pdf->Ln();
                    $pdf->Cell(99,6,"TINTAS",1,'','C');
                    $pdf->Cell(99,6,"CROMIAS",1,'','C');
                        $pdf->SetFont('Arial', '',8);
                        
    #######################################################################################
                        
                        $pdf->Ln();
                        $pdf->Cell(99,8,"<TINTAS>",1);
                        $pdf->Cell(99,8,"<CROMIAS>",1);
            
        $pdf->Ln();
        $pdf->Cell(198,5,"",1);
            
            
            
    ######################################## Relatório de Produção ##############################       
            
            $pdf->Ln();
            $pdf->SetFont('Arial','B',9);
            $pdf->Cell(198,6,"RELATÓRIO DE PRODUÇÃO",1,'','C');
            $pdf->Ln();
                  $pdf->SetFont('Arial', 'B',8);
                  $pdf->Cell(32,6,"",1);
                  $pdf->Cell(69,6,"PRODUÇÃO",'R B','','C');
                  $pdf->Cell(50,6,"ALMOXARIFADO",'R B','','C');
                  $pdf->Cell(47,6,"REBOBINAMENTO",'R B','','C');
                  
                  
                   
   
                    $pdf->Ln();
                    $pdf->SetFont('Arial', 'B',9);
                    $pdf->Cell(11,6,"",'L R','','C');
                    $pdf->Cell(21,6,"",'R','','C');
                    $pdf->Cell(16,6,"Acerto",'L R','','C');
                    $pdf->Cell(17,6,"Papel",'L R','','C');
                    $pdf->Cell(16,6,"Retorno",'L R','','C');
                    $pdf->Cell(20,6,"Resistência",'L R','','C');
                    $pdf->Cell(18,6,"Papel",'L R','','C');
                    $pdf->Cell(16,6,"Acerto",'L R','','C');
                    $pdf->Cell(16,6,"Cod.",'L R','','C');
                    $pdf->Cell(15,6,"Papel",'L R','','C');
                    $pdf->Cell(17,6,"Qtde Etiq.",'L R','','C');
                    $pdf->Cell(15,6,"",'L R','','C');
                        $pdf->Ln();
                        $pdf->SetFont('Arial', 'B',9);
                        $pdf->Cell(11,6,"Item",'L R B','','C');
                        $pdf->Cell(21,6,"Máquina",'R B','','C');
                        $pdf->Cell(16,6,"Máquina",'L R B','','C');
                        $pdf->Cell(17,6,"Produzido",'L R B','','C');
                        $pdf->Cell(16,6,"Papel",'L R B','','C');
                        $pdf->Cell(20,6,"Verniz",'L R B','','C');
                        $pdf->Cell(18,6,"Entregue",'L R B','','C');
                        $pdf->Cell(16,6,"Papel",'L R B','','C');
                        $pdf->Cell(16,6,"Clichê",'L R B','','C');
                        $pdf->Cell(15,6,"Perdido",'L R B','','C');
                        $pdf->Cell(17,6,"Perdida",'L R B','','C');
                        $pdf->Cell(15,6,"Total",'L R B','','C');
    ###################################################################################

                            $pdf->Ln();
                            $pdf->SetFont('Arial', '',8);
                            $pdf->Cell(11,6,"<ITEM>",'L R B','','C');
                            $pdf->Cell(21,6,"<MAQUINA>",'R B','','C');
                            $pdf->Cell(16,6,"<CANETA>",'L R B','','C');
                            $pdf->Cell(17,6,"<CANETA>",'L R B','','C');
                            $pdf->Cell(16,6,"<CANETA>",'L R B','','C');
                            $pdf->Cell(20,6,"<VERNIZ>",'L R B','','C');
                            $pdf->Cell(18,6,"<P.ENTRE.>",'L R B','','C');
                            $pdf->Cell(16,6,"<PAPEL>",'L R B','','C');
                            $pdf->Cell(16,6,"<CLICHÊ>",'L R B','','C');
                            $pdf->Cell(15,6,"<PEDIDO>",'L R B','','C');
                            $pdf->Cell(17,6,"<PERDIDA>",'L R B','','C');
                            $pdf->Cell(15,6,"<TOTAL>",'L R B','','C');
                        
                      
    ########################## Esses campos serão todos preenchidos manualmente #############                
        $pdf->Ln();
        $pdf->Cell(198,3,"",'T B');

        $pdf->Ln();
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(33,6,"Setup",'L R','','C');
        $pdf->Cell(70,6,"PRODUZIDO POR:",'L R');
        $pdf->Cell(47,6,"DATA",'L R');
        $pdf->Cell(48,6,"REBOBINADO POR:",'L R');
        
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B',8);
        $pdf->Cell(33,3,"",'L R');
        $pdf->Cell(32,3,"",'L');
        $pdf->Cell(38,3,"",' R');
        $pdf->Cell(47,3,"",'L R');
        $pdf->Cell(21,3,"",'L');
        $pdf->Cell(27,3,"",'R');
        
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B',8);
        $pdf->Cell(33,6,"Início:",'L R');
        $pdf->Cell(32,6,"DATA:",'L');
        $pdf->Cell(38,6,"DATA:",' R');
        $pdf->Cell(47,6,"VISTO RETIRADA MATERIAL:",'L R');
        $pdf->Cell(21,6,"DATA:",'L');
        $pdf->Cell(27,6,"DATA:",'R');
        
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(33,6,"Término:",'L R B');
        $pdf->Cell(32,6,"Início:",'L B');
        $pdf->Cell(38,6,"Término:",'R B');
        $pdf->Cell(47,6,"",'L R B');
        $pdf->Cell(20,6,"Início:",'L B');
        $pdf->Cell(28,6,"Término:",'R B');
        
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B',9);
        $pdf->Cell(33,6,"",'L R');
        $pdf->Cell(70,6,"CONFERIDO POR:",'L R');
        $pdf->Cell(47,6,"",'L R');
        $pdf->Cell(48,6,"CONFERIDO POR:",'L R');
        $pdf->Ln();
        $pdf->Cell(33,6,"",'L R');
        $pdf->Cell(70,6,"",'L R');
        $pdf->Cell(47,6,"",'L R');
        $pdf->Cell(48,6,"",'L R');
        $pdf->Ln();
        $pdf->Cell(33,6,"",'L R B');
        $pdf->Cell(70,6,"DATA:",'L R B');
        $pdf->Cell(47,6,"",'L R B');
        $pdf->Cell(48,6,"DATA:",'L R B');
        
        $pdf->Ln();
        $pdf->Cell(150,6,"OBSERVAÇÕES:",'L R');
        $pdf->Cell(48,6,"LOTE BRINDART:",'L R');
        $pdf->Ln();
        $pdf->Cell(150,13,"<OBSERVAÇÕES>",'L R B');
        $pdf->Cell(48,13,"",'L R B');
        
        $pdf->Ln();
        $pdf->Cell(198,6,"EXPEDIÇÃO",1);
        $pdf->Ln();
        $pdf->Cell(21,6,"CAIXA",1,'','C');
        $pdf->Cell(21,6,"ROLOS",1,'','C');
        $pdf->Cell(36,6,"ETIQUETAS P/ ROLO",1,'','C');
        $pdf->Cell(21,6,"CAIXA",1,'','C');
        $pdf->Cell(21,6,"ROLOS",1,'','C');
        $pdf->Cell(36,6,"ETIQUETAS P/ ROLO",1,'','C');
        $pdf->Cell(42,6,"DATA",1);
        $pdf->Ln();
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(36,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(36,5,"",1,'','C');
        $pdf->Cell(42,5,"",1);
        $pdf->Ln();
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(36,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(36,5,"",1,'','C');
        $pdf->Cell(42,5,"ASSINATURA",1);
        $pdf->Ln();
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(36,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(36,5,"",1,'','C');
        $pdf->Cell(42,5,"",'L R');
        $pdf->Ln();
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(36,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(21,5,"",1,'','C');
        $pdf->Cell(36,5,"",1,'','C');
        $pdf->Cell(42,5,"",'L R B');
        
        
        
        
        
        
        
        
                    
                
            
            
            
            
        
            
        
        
       
        
        
	
     $pdf->Output();   

?>