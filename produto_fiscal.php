<?

	$id		= $_GET['id'];
    if(isset($_POST["btn_enviar"])){
	
		$ean							= $_POST['txt_ean'];
		$ean_unidade_tributavel			= $_POST['txt_ean_unidade_tributavel'];
		$ex_tipi						= $_POST["txt_ex_tipi"];
		$genero							= $_POST["txt_genero"];
		$ncm							= $_POST["txt_ncm"];
		$cfop_dentro					= $_POST["sel_cfop_dentro"];
		$cfop_fora						= $_POST["sel_cfop_fora"];
		$unidade_comercial				= $_POST["txt_unidade_comercial"];
		$unidade_tributavel 	   		= $_POST["txt_unidade_tributavel"];
		
		//icms
		$icms_regime 	   				= $_POST["sel_icms_regime"];
		$icms_CST						= $_POST["sel_icms_situacao_tributaria"];
		$icms_orig						= $_POST['sel_icms_origem'];
		$icms_CSOSN						= $_POST['txt_icms_CSOSN'];
		$icms_modBC						= $_POST['sel_icms_modBC'];
		$icms_pRedBC					= format_number_in($_POST['txt_icms_pRedBC']);
		$icms_pICMS						= format_number_in($_POST['txt_icms_pICMS']);
		$icms_pBCOp						= format_number_in($_POST['txt_icms_pBCOp']);
		$icms_motDesICMS				= $_POST['sel_icms_motDesICMS'];
		$icms_pCredSN					= format_number_in($_POST['txt_icms_pCredSN']);
		
		$icms_modBCST					= $_POST['sel_icms_st_modBCST'];
		$icms_pRedBCST					= format_number_in($_POST['txt_icms_st_pRedBCST']);
		$icms_UFST						= $_POST['sel_icms_st_UFST'];
		
		//ipi
		$ipi_situacao_tributaria		= $_POST['sel_ipi_situacao_tributaria'];
		$ipi_enquadramento_classe		= $_POST['txt_ipi_enquadramento_classe'];
		$ipi_enquadramento_codigo		= $_POST['txt_ipi_enquadramento_codigo'];
		$ipi_produtor_cnpj				= $_POST['txt_ipi_produtor_cnpj'];
		$ipi_calculo_tipo				= $_POST['sel_ipi_calculo_tipo'];
		$ipi_aliquota					= format_number_in($_POST['txt_ipi_aliquota']);
		$ipi_unidade_valor				= format_number_in($_POST['txt_ipi_unidade_valor']);
		
		//pis
		$pis_situacao_tributaria		= $_POST['sel_pis_situacao_tributaria'];
		$pis_calculo_tipo				= $_POST['sel_pis_calculo_tipo'];
		$pis_aliquota_porcentagem		= format_number_in($_POST['txt_pis_aliquota_percentual']);
		$pis_aliquota_reais				= format_number_in($_POST['txt_pis_aliquota_reais']);
		
		//pisST
		$pisST_calculo_tipo				= $_POST['sel_pis_st_calculo_tipo'];
		$pisST_aliquota_porcentagem		= format_number_in($_POST['txt_pis_st_aliquota_percentual']);
		$pisST_aliquota_reais			= format_number_in($_POST['txt_pis_st_aliquota_reais']);
		
		//cofins
		$cofins_situacao_tributaria		= $_POST['sel_cofins_situacao_tributaria'];
		$cofins_calculo_tipo			= $_POST['sel_cofins_calculo_tipo'];
		$cofins_aliquota_porcentagem	= format_number_in($_POST['txt_cofins_aliquota_percentual']);
		$cofins_aliquota_reais			= format_number_in($_POST['txt_cofins_aliquota_reais']);
		
		//cofinsST
		$cofinsST_calculo_tipo			= $_POST['sel_cofins_st_calculo_tipo'];
		$cofinsST_aliquota_porcentagem	= format_number_in($_POST['txt_cofins_st_aliquota_percentual']);
		$cofinsST_aliquota_reais		= format_number_in($_POST['txt_cofins_st_aliquota_reais']);

		$Tipo_Especifico 				= $_POST['sel_prod_especifico_tipo'];
		$Combustivel_Cod_ANP 			= $_POST['sel_produto_especifico_combustivel_codanp'];
		$Combustivel_Cod_IF 			= $_POST['txt_produto_especifico_combustivel_codif'];
		$Combustivel_UF 				= $_POST['sel_produto_especifico_combustivel_uf'];
		$Combustivel_Qtd_Faturada 		= $_POST['txt_produto_especifico_combustivel_qtd_faturada'];
		$Combustivel_Cide_bCalculo 		= $_POST['txt_produto_especifico_combustivel_cide_bc'];
		$Combustivel_Cide_Aliquota 		= $_POST['txt_produto_especifico_combustivel_cide_aliquota'];
		$Combustivel_Cide_Valor 		= $_POST['txt_produto_especifico_combustivel_cide_valor'];

		//echo "ipi_enquadramento_classe:$ipi_enquadramento_classe";

		$sql = "SELECT COUNT(*) AS fldCount FROM tblproduto_fiscal WHERE fldProduto_Id = $id";
		$rsProduto_Fiscal = mysql_query($sql);
		$rowProduto_Fiscal = mysql_fetch_array($rsProduto_Fiscal);
		
		if(!$rowProduto_Fiscal['fldCount']){
			$sql = "INSERT INTO tblproduto_fiscal (
						fldProduto_Id,
						fldean,
						fldean_unidade_tributavel,
						fldex_tipi,
						fldgenero,
						fldncm,
						fldcfop_id,
						fldcfop_fora_id,
						fldunidade_comercial,
						fldunidade_tributavel,
											
						fldicms_regime,
						fldicms_orig,
						fldicms_CST,
		
						fldicms_CSOSN,
						fldicms_modBC,
						fldicms_pRedBC,
						fldicms_pICMS,
						fldicms_modBCST,
						fldicms_pRedBCST,
						fldicms_pBCOp,
						fldicms_UFST,
						fldicms_motDesICMS,
						fldicms_pCredSN,
						
						fldipi_situacao_tributaria,
						fldipi_enquadramento_classe,
						fldipi_enquadramento_codigo,
						fldipi_produtor_cnpj,
						fldipi_calculo_tipo,
						fldipi_aliquota,
						fldipi_unidade_valor,
						
						fldpis_situacao_tributaria,
						fldpis_calculo_tipo,
						fldpis_aliquota_porcentagem,
						fldpis_aliquota_reais,
												
						fldpis_st_calculo_tipo,
						fldpis_st_aliquota_reais,
						fldpis_st_aliquota_porcentagem,
												
						fldcofins_situacao_tributaria,
						fldcofins_calculo_tipo,
						fldcofins_aliquota_reais,
						fldcofins_aliquota_porcentagem,
												
						fldcofins_st_calculo_tipo,
						fldcofins_st_aliquota_reais,
						fldcofins_st_aliquota_porcentagem,

						fldTipo_Especifico,
						fldCombustivel_Codigo_ANP,
						fldCombustivel_Codigo_CodIF,
						fldCombustivel_UF,
						fldCombustivel_Qtd_Faturada,
						fldCombustivel_Cide_bCalculo,
						fldCombustivel_Cide_Aliquota,
						fldCombustivel_Cide_Valor
						
			)
			values(
						'$id',
						'$ean',
						'$ean_unidade_tributavel',
						'$ex_tipi',
						'$genero',
						'$ncm',
						'$cfop_dentro',
						'$cfop_fora',
						'$unidade_comercial',
						'$unidade_tributavel',
						
						'$icms_regime',
						'$icms_orig',
						'$icms_CST',
						
						'$icms_CSOSN',
						'$icms_modBC',
						'$icms_pRedBC',
						'$icms_pICMS',
						'$icms_modBCST',
						'$icms_pRedBCST',
						'$icms_pBCOp',
						'$icms_UFST',
						'$icms_motDesICMS',
						'$icms_pCredSN',
						
						'$ipi_situacao_tributaria',
						'$ipi_enquadramento_classe',
						'$ipi_enquadramento_codigo',
						'$ipi_produtor_cnpj',
						'$ipi_calculo_tipo',
						'$ipi_aliquota',
						'$ipi_unidade_valor', 
						
						'$pis_situacao_tributaria',
						'$pis_calculo_tipo',
						'$pis_aliquota_porcentagem',
						'$pis_aliquota_reais', 
						
						'$pisST_calculo_tipo',
						'$pisST_aliquota_porcentagem',
						'$pisST_aliquota_reais',
						
						'$cofins_situacao_tributaria',
						'$cofins_calculo_tipo',
						'$cofins_aliquota_porcentagem',
						'$cofins_aliquota_reais', 
						
						'$cofinsST_calculo_tipo',
						'$cofinsST_aliquota_porcentagem',
						'$cofinsST_aliquota_reais',

						'$Tipo_Especifico',
						'$Combustivel_Cod_ANP',
						'$Combustivel_Cod_IF',
						'$Combustivel_UF',
						'$Combustivel_Qtd_Faturada',
						'$Combustivel_Cide_bCalculo',
						'$Combustivel_Cide_Aliquota',
						'$Combustivel_Cide_Valor'
						
			)";
			
			
		
		}
		else{
			$sql = "UPDATE tblproduto_fiscal SET
						fldProduto_Id					= '$id',
						fldean							= '$ean',
						fldean_unidade_tributavel		= '$ean_unidade_tributavel',
						fldex_tipi						= '$ex_tipi',
						fldgenero						= '$genero',
						fldncm							= '$ncm',
						fldcfop_id						= '$cfop_dentro',
						fldcfop_fora_id					= '$cfop_fora',
						fldunidade_comercial			= '$unidade_comercial',
						fldunidade_tributavel			= '$unidade_tributavel',
						
						fldicms_regime					= '$icms_regime',
						fldicms_orig					= '$icms_orig',
						fldicms_CST						= '$icms_CST',
						
						fldicms_CSOSN					= '$icms_CSOSN',
						fldicms_modBC					= '$icms_modBC',
						fldicms_pRedBC					= '$icms_pRedBC',
						fldicms_pICMS					= '$icms_pICMS',
						fldicms_modBCST					= '$icms_modBCST',
						fldicms_pRedBCST				= '$icms_pRedBCST',
						fldicms_pBCOp					= '$icms_pBCOp',
						fldicms_UFST					= '$icms_UFST',
						fldicms_motDesICMS				= '$icms_motDesICMS',
						fldicms_pCredSN					= '$icms_pCredSN',
						
						fldipi_situacao_tributaria		= '$ipi_situacao_tributaria',
						fldipi_enquadramento_classe		= '$ipi_enquadramento_classe',
						fldipi_enquadramento_codigo		= '$ipi_enquadramento_codigo',
						fldipi_produtor_cnpj			= '$ipi_produtor_cnpj',
						fldipi_calculo_tipo				= '$ipi_calculo_tipo',
						fldipi_aliquota					= '$ipi_aliquota',
						fldipi_unidade_valor			= '$ipi_unidade_valor',
						
						fldpis_situacao_tributaria		= '$pis_situacao_tributaria',
						fldpis_calculo_tipo				= '$pis_calculo_tipo',		
						fldpis_aliquota_reais			= '$pis_aliquota_reais',
						fldpis_aliquota_porcentagem		= '$pis_aliquota_porcentagem',
												
						fldpis_st_calculo_tipo			= '$pisST_calculo_tipo',
						fldpis_st_aliquota_reais		= '$pisST_aliquota_porcentagem',
						fldpis_st_aliquota_porcentagem	= '$pisST_aliquota_reais',
												
						fldcofins_situacao_tributaria	= '$cofins_situacao_tributaria',
						fldcofins_calculo_tipo			= '$cofins_calculo_tipo',
						fldcofins_aliquota_reais		= '$cofins_aliquota_porcentagem',
						fldcofins_aliquota_porcentagem	= '$cofins_aliquota_reais',
												
						fldcofins_st_calculo_tipo		= '$cofinsST_calculo_tipo',
						fldcofins_st_aliquota_reais		= '$cofinsST_aliquota_porcentagem',
						fldcofins_st_aliquota_porcentagem	= '$cofinsST_aliquota_reais',

						fldTipo_Especifico 				= '$Tipo_Especifico',
						fldCombustivel_Codigo_ANP 		= '$Combustivel_Cod_ANP',
						fldCombustivel_Codigo_CodIF 	= '$Combustivel_Cod_IF',
						fldCombustivel_UF 				= '$Combustivel_UF',
						fldCombustivel_Qtd_Faturada 		= '$Combustivel_Qtd_Faturada',
						fldCombustivel_Cide_bCalculo 	= '$Combustivel_Cide_bCalculo',
						fldCombustivel_Cide_Aliquota	= '$Combustivel_Cide_Aliquota',
						fldCombustivel_Cide_Valor		= '$Combustivel_Cide_Valor'
						
			WHERE fldProduto_Id = $id";
			
		}
		if(mysql_query($sql)){
			
			#INSERINDO ALIQUOTA E MVA POR ESTADO
			$controle_icms_pMVAST	= $_POST['hid_controle_icms_st_pMVAST'];
			for($n=1; $n <= $controle_icms_pMVAST; $n++){
				$uf_icms		= $_POST['hid_uf_icms_st_pMVAST_'.$n];
				$icms_pMVAST	= format_number_in($_POST['txt_icms_st_pMVAST_'.$n]);
				$icms_pICMSST	= format_number_in($_POST['txt_icms_st_pICMSST_'.$n]);
				#VERIFICA SE EXISTE NO BD
				$rsEstado = mysql_query('SELECT * FROM tblproduto_fiscal_estado WHERE fldProduto_Id ='.$id.' AND fldUF_Codigo = '.$uf_icms);
				if(mysql_num_rows($rsEstado)){
					mysql_query('UPDATE tblproduto_fiscal_estado SET
								fldicmsst_mva = "'.$icms_pMVAST.'", fldicmsst_aliquota ="'.$icms_pICMSST.'"
								WHERE fldProduto_Id = "'.$id.'" AND fldUF_Codigo  ="'.$uf_icms.'"');
				
				}else{
					mysql_query('INSERT INTO tblproduto_fiscal_estado	(fldicmsst_mva, fldicmsst_aliquota, fldProduto_Id, fldUF_Codigo)
								VALUES ("'.$icms_pMVAST.'","'.$icms_pICMSST.'","'.$id.'","'.$uf_icms.'")');
				
				}
			
				echo mysql_error();
			}unset($n);
?>
			<div class="alert" style="margin-top: 6px">
                <p class="ok">Dados alterados com sucesso!<p>
            </div>
<?			
		}else{
			echo mysql_error();
		}
		
    };
	
	//carregar dados
	$sql = "select * from tblproduto_fiscal where fldProduto_Id = $id";
	$rsProduto_Fiscal = mysql_query($sql);
	
	if(mysql_num_rows($rsProduto_Fiscal)){
		
		$rowProduto_Fiscal 						= mysql_fetch_array($rsProduto_Fiscal);
		
		$ean									= $rowProduto_Fiscal['fldean'];
		$ean_unidade_tributavel					= $rowProduto_Fiscal['fldean_unidade_tributavel'];
		$ex_tipi								= $rowProduto_Fiscal["fldex_tipi"];
		$genero									= $rowProduto_Fiscal["fldgenero"];
		$ncm									= $rowProduto_Fiscal["fldncm"];
		$cfop_dentro_id							= $rowProduto_Fiscal["fldcfop_id"];
		$cfop_fora_id							= $rowProduto_Fiscal["fldcfop_fora_id"];
		$unidade_comercial						= $rowProduto_Fiscal["fldunidade_comercial"];
		$unidade_tributavel 	   				= $rowProduto_Fiscal["fldunidade_tributavel"];
		
		//icms
		$icms_regime 	   						= $rowProduto_Fiscal['fldicms_regime'];
		$icms_CST								= $rowProduto_Fiscal['fldicms_CST'];
		$icms_orig								= $rowProduto_Fiscal['fldicms_orig'];
		
		$icms_CSOSN								= $rowProduto_Fiscal['fldicms_CSOSN'];
		$icms_modBC								= $rowProduto_Fiscal['fldicms_modBC'];
		$icms_pRedBC							= format_number_out($rowProduto_Fiscal['fldicms_pRedBC']);
		$icms_pICMS								= format_number_out($rowProduto_Fiscal['fldicms_pICMS']);
		$icms_pBCOp								= format_number_out($rowProduto_Fiscal['fldicms_pBCOp']);
		$icms_modBCST							= $rowProduto_Fiscal['fldicms_modBCST'];
		$icms_pRedBCST              			= format_number_out($rowProduto_Fiscal['fldicms_pRedBCST']);
		$icms_pMVAST							= format_number_out($rowProduto_Fiscal['fldicms_pMVAST']);
		$icms_pICMSST							= format_number_out($rowProduto_Fiscal['fldicms_pICMSST']);
		$icms_motDesICMS						= $rowProduto_Fiscal['fldicms_motDesICMS'];
		$icms_pCredSN							= format_number_out($rowProduto_Fiscal['fldicms_pCredSN']);
		
		//ipi
		$ipi_situacao_tributaria				= $rowProduto_Fiscal['fldipi_situacao_tributaria'];
		$ipi_enquadramento_classe				= $rowProduto_Fiscal['fldipi_enquadramento_classe'];
		$ipi_enquadramento_codigo				= $rowProduto_Fiscal['fldipi_enquadramento_codigo'];
		$ipi_produtor_cnpj						= $rowProduto_Fiscal['fldipi_produtor_cnpj'];
		$ipi_calculo_tipo						= $rowProduto_Fiscal['fldipi_calculo_tipo'];
		$ipi_aliquota							= format_number_out($rowProduto_Fiscal['fldipi_aliquota']);
		$ipi_unidade_valor						= format_number_out($rowProduto_Fiscal['fldipi_unidade_valor']);
		
		//pis
		$pis_situacao_tributaria				= $rowProduto_Fiscal['fldpis_situacao_tributaria'];
		$pis_calculo_tipo						= $rowProduto_Fiscal['fldpis_calculo_tipo'];
		$pis_aliquota_porcentagem				= format_number_out($rowProduto_Fiscal['fldpis_aliquota_porcentagem']);
		$pis_aliquota_reais						= format_number_out($rowProduto_Fiscal['fldpis_aliquota_reais']);
		
		//pisST
		$pisST_calculo_tipo						= $rowProduto_Fiscal['fldpis_st_calculo_tipo'];
		$pisST_aliquota_porcentagem				= format_number_out($rowProduto_Fiscal['fldpis_st_aliquota_porcentagem']);
		$pisST_aliquota_reais					= format_number_out($rowProduto_Fiscal['fldpis_st_aliquota_reais']);
		
		//cofins
		$cofins_situacao_tributaria				= $rowProduto_Fiscal['fldcofins_situacao_tributaria'];
		$cofins_calculo_tipo					= $rowProduto_Fiscal['fldcofins_calculo_tipo'];
		$cofins_aliquota_porcentagem			= format_number_out($rowProduto_Fiscal['fldcofins_aliquota_porcentagem']);
		$cofins_aliquota_reais					= format_number_out($rowProduto_Fiscal['fldcofins_aliquota_reais']);
		
		//cofinsST
		$cofinsST_calculo_tipo					= $rowProduto_Fiscal['fldcofins_st_calculo_tipo'];
		$cofinsST_aliquota_porcentagem			= format_number_out($rowProduto_Fiscal['fldcofins_st_aliquota_porcentagem']);
		$cofinsST_aliquota_reais				= format_number_out($rowProduto_Fiscal['fldcofins_st_aliquota_reais']);
				
	}else{
	
		$sqlUnidade = "SELECT fldNome FROM tblproduto_unidade_medida WHERE fldId = ".$rowProduto['fldUN_Medida_Id'];
		$rsUnidade	= mysql_query($sqlUnidade);
		$rowUnidade = mysql_fetch_array($rsUnidade);
		
		$unidade_comercial	= substr($rowUnidade['fldNome'],0,6);
		$unidade_tributavel = substr($rowUnidade['fldNome'],0,6);
	}
	
	
	//AQUI EU BUSCO O ESTADO QUE A EMPRESA DO SISTEMA ESTA, PARA TORNAR O CADASTRO MAIS FACIL
	$rowEmpresaUF = mysql_fetch_array(mysql_query('SELECT fldMunicipio_Codigo FROM tblempresa_info'));
	$codigo_uf = substr($rowEmpresaUF['fldMunicipio_Codigo'] ,0,2);
?>
	<div class="form">
		<form class="frm_detalhe frm_nfe" style="width:890px;text-align:right" id="frm_produto_fiscal" action="" method="post">
			<fieldset id="fset_geral">
            	<legend>Produto</legend>
				<ul style="width:940px">
					<li>	
						<label for='txt_ean'>EAN</label>
						<input type="text" style="width:120px;text-align:right" id="txt_ean" name="txt_ean" value="<?=$ean?>"/> 
					</li>
					<li>
						<label for='txt_ean_unidade_tributavel'>EAN UN. Tributavel</label>
						<input type="text" style="width: 120px;text-align:right" id="txt_ean_unidade_tributavel" name="txt_ean_unidade_tributavel" value="<?=$ean_unidade_tributavel?>"/>
					</li>
					<li>
						<label for='txt_ex_tipi'>EX TIPI</label>
						<input type="text" style="width:80px;text-align:right" id="txt_ex_tipi" name="txt_ex_tipi" value="<?=$ex_tipi?>" maxlength="3" />
					</li>
					<li>
						<label for='txt_genero'>Genero</label>
						<input type="text" style="width:80px;text-align:right" id="txt_genero" name="txt_genero" value="<?=$genero?>"/>
					</li>
					<li>
						<label for='txt_ncm'>NCM</label>
						<input type="text" style="width:120px;text-align:right" id="txt_ncm" name="txt_ncm" value="<?=$ncm?>"/>
					</li>
						<li>
							<label for="sel_cfop_dentro">CFOP (dentro estado)</label>
							<select name="sel_cfop_dentro" id="sel_cfop_dentro" style="width:150px">
                            	<option value=""></option>
<?								$rsCFOP  = mysql_query('SELECT * FROM tblnfe_cfop');
								while($rowCFOP = mysql_fetch_array($rsCFOP)){
?>									<option <?=($rowCFOP['fldId'] == $cfop_dentro_id) ? 'selected="selected"' : ''?> value='<?=$rowCFOP['fldId']?>'><?=$rowCFOP['fldCFOP']?></option>
<?								}
?>							</select>
						</li>
						<li>
							<label for="sel_cfop_fora">CFOP (fora estado)</label>
							<select name="sel_cfop_fora" id="sel_cfop_fora" style="width:150px">
                            	<option value=""></option>
<?								$rsCFOP  = mysql_query('SELECT * FROM tblnfe_cfop');
								while($rowCFOP = mysql_fetch_array($rsCFOP)){
?>									<option <?=($rowCFOP['fldId'] == $cfop_fora_id) ? 'selected="selected"' : ''?> value='<?=$rowCFOP['fldId']?>'><?=$rowCFOP['fldCFOP']?></option>
<?								}
?>							</select>
						</li>
					<li>
						<label for='txt_unidade_comercial'>Unid. Comercial</label>
						<input type="text" style="width:95px;text-align:right" id="txt_unidade_comercial" name="txt_unidade_comercial" value="<?=$unidade_comercial?>"/>
					</li>
					<li>
						<label for='txt_unidade_tributavel'>Unid. Tribut&aacute;vel</label>
						<input type="text" style="width:95px;text-align:right" id="txt_unidade_tributavel" name="txt_unidade_tributavel" value="<?=$unidade_tributavel?>"/>
					</li>
				</ul>
			</fieldset>
			<fieldset id="fset_icms">
				<legend>ICMS</legend>
				<fieldset style="margin:10px;">
					<div id="controle"></div>
					<ul style="width:100%;">
						<li>
							<label for="sel_icms_situacao_tributaria">Situa&ccedil;&atilde;o Tribut&aacute;ria</label>
							<select name="sel_icms_situacao_tributaria" id="sel_icms_situacao_tributaria" style="width:700px">
<?								require("produto_fiscal_icms_select_situacao_tributaria.php");
?>							</select>
						</li>
						<li>
							<label for="sel_icms_origem">Origem</label>
							<select name="sel_icms_origem" id="sel_icms_origem" style="width:150px">
	                            <option value=""></option>
<?								$sql = "select * from tblnfe_icms_campo_orig order by fldId";
								$rsOrigem = mysql_query($sql);
								while($rowOrigem = mysql_fetch_array($rsOrigem)){
?>									<option <? $icms_orig ==  $rowOrigem['fldId'] ? print 'selected="selected"' : '' ?> value="<?=$rowOrigem['fldId']?>"><?=substr($rowOrigem['fldDescricao'],0,70)?></option>
<?								}
?>							</select>
						</li>
					</ul>
				</fieldset>
				<fieldset style="margin:10px;">
					<div style="width:914px">
						<ul style="width:100%;">
                            <li>
                                <label for="txt_icms_pCredSN">Al&iacute;quota aplic&aacute;vel de c&aacute;lculo do cr&eacute;dito</label>
                                <input type="text" style="width:250px;text-align:right" id="txt_icms_pCredSN" name="txt_icms_pCredSN" value="<?=$icms_pCredSN?>" />
                            </li>
                            
						</ul>
					</div>
				    <div style="width:100%">
						<!--ICMS-->
						<fieldset style="min-height:250px;width:431px;display:inline;margin:10px;float:left">
							<legend>ICMS</legend>
								<ul>
									<li style="width:100%">
    								    <label for="sel_icms_modBC">Modalid. de determ. da BC ICMS</label>
									    <select id="sel_icms_modBC" name="sel_icms_modBC">
                                        	<option value=""></option>
<?									      	$rsModalidade = mysql_query("SELECT * FROM tblnfe_icms_campo_modbc");
											while($rowModalidade = mysql_fetch_array($rsModalidade)){
?>											    <option <? $icms_modBC==$rowModalidade['fldCodigo'] ? print 'selected="selected"' : '' ?> value="<?=$rowModalidade['fldCodigo']?>"><?=$rowModalidade['fldDescricao']?></option>
<? 											}
?>									    </select>
								    </li>
									
								    <li style="width:100%">
									    <label for="txt_icms_pRedBC">% Redu&ccedil;&atilde;o da BC ICMS</label>
									    <input type="text" style="width:200px;text-align:right" id="txt_icms_pRedBC" name="txt_icms_pRedBC" value="<?=$icms_pRedBC?>" />
								    </li>
								    
									<li style="width:100%">
									    <label for="txt_icms_pICMS">Al&iacute;quota do ICMS</label>
									    <input type="text" style="width:200px;text-align:right" id="txt_icms_pICMS" name="txt_icms_pICMS" value="<?=$icms_pICMS?>" />
								    </li>
									
								    <li style="width:100%">
									    <label for="txt_icms_pBCOp">% BC da opera&ccedil;&atilde;o pr&oacute;pria</label>
									    <input type="text" style="width:200px;text-align:right" id="txt_icms_pBCOp" name="txt_icms_pBCOp" value="<?=$icms_pBCOp?>" />
								    </li>
								    
								    <li style="width:100%">
									    <label for="sel_icms_motDesICMS">Motivo da desonera&ccedil;&atilde;o do ICMS</label>
										<select id="sel_icms_motDesICMS" name="sel_icms_motDesICMS">
                                        	<option value=""></option>
<?											$rsDesoneracao = mysql_query("SELECT * FROM tblnfe_icms_campo_motdesicms");
												while($rowDesoneracao = mysql_fetch_array($rsDesoneracao)){
?>													<option <? $icms_motDesICMS==$rowDesoneracao['fldId'] ? print 'selected="selected"' : '' ?> value="<?=$rowDesoneracao['fldId']?>"><?=$rowDesoneracao['fldDescricao']?></option>
<? 												}
?>										</select>
								    </li>
						      </ul>
						</fieldset>		
						<!--ICMS ST-->
						<fieldset style="min-height:280px;width:430px;display:inline;margin:10px">
						    <legend>ICMS ST</legend>
							    <ul>
								    <li style="width:100%">
										<label for="sel_icms_st_modBCST">Modalid. de determ. da BC do ICMS ST</label>
										<select id="sel_icms_st_modBCST" name="sel_icms_st_modBCST">
                                        	<option value=""></option>
<?											$rsModalidDeterm = mysql_query("SELECT * FROM tblnfe_icms_campo_modbcst");
												while($rowModalidDeterm = mysql_fetch_array($rsModalidDeterm)){
?>											  		<option <? $icms_modBCST==$rowModalidDeterm['fldId'] ? print 'selected="selected"' : '' ?> value="<?=$rowModalidDeterm['fldId']?>"><?=$rowModalidDeterm['fldDescricao']?></option>
<? 											  	}
?>										</select>
									</li>
									
									<li style="width:100%">
										<label for="txt_icms_st_pRedBCST">% Redu&ccedil;&atilde;o da BC ICMS ST</label>
										<input type="text" style="width:200px;text-align:right" id="txt_icms_st_pRedBCST" name="txt_icms_st_pRedBCST" value="<?=$icms_pRedBCST?>" />
									</li>
									
									<li style="width:100%">
										<label for="txt_icms_st_pMVAST"> % margem de valor adic. do ICMS ST</label>
                                        <ul>
<?											$rsEstado = mysql_query('SELECT tblibge_uf.*, tblproduto_fiscal_estado.fldicmsst_mva 
                                                                     FROM tblibge_uf LEFT JOIN tblproduto_fiscal_estado 
                                                                     ON tblibge_uf.fldCodigo = tblproduto_fiscal_estado.fldUF_Codigo AND tblproduto_fiscal_estado.fldProduto_Id ='.$produto_id.'
                                                                     ORDER BY FIELD(tblibge_uf.fldCodigo, "'.$codigo_uf.'") DESC, fldSigla');
                                            while($rowEstado = mysql_fetch_array($rsEstado)){
												$n++;	
?>                                     		
                                                <li style="width:95px">
                                                    <input type="text" style="width:30px" id="txt_icms_st_pMVAST" name="txt_icms_st_pMVAST"  disabled="disabled"value="<?=$rowEstado['fldSigla']?>" />
                                                    <input type="text" style="width:50px;text-align:right" id="txt_icms_st_pMVAST_<?=$n?>" name="txt_icms_st_pMVAST_<?=$n?>" value="<?=format_number_out($rowEstado['fldicmsst_mva'])?>" />	
                                                    <input type="hidden" id="hid_uf_icms_st_pMVAST_<?=$n?>" name="hid_uf_icms_st_pMVAST_<?=$n?>" value="<?=$rowEstado['fldCodigo']?>" />	
                                                </li>
<?												if($rowEstado['fldCodigo'] == $codigo_uf){											
													echo '<li style="margin-left:180px;text-align:right"><small style=" display:inline-table; margin-right:6px">exibir todos os estados</small><a href="#" id="exibir_mva_estado" class="mostrar_tudo"></a></li>';
													echo '<br \><div id="mva_icmsst_estado" style="display:none;width:98%; height:220px; border:1px solid #999;margin-top:15px">';
												}
											}
?>                                      	</div>
											<input type="hidden" id="hid_controle_icms_st_pMVAST" name="hid_controle_icms_st_pMVAST" value="<?=$n?>" />	
										</ul>
	
									</li>
									
									<li style="width:100%">
										<label for="txt_icms_st_pICMSST">Al&iacute;quota do ICMS ST</label>
                                        <ul>
<?											unset($n);
											$rsEstado = mysql_query('SELECT tblibge_uf.*, tblproduto_fiscal_estado.fldicmsst_aliquota 
                                                                     FROM tblibge_uf LEFT JOIN tblproduto_fiscal_estado 
                                                                     ON tblibge_uf.fldCodigo = tblproduto_fiscal_estado.fldUF_Codigo AND tblproduto_fiscal_estado.fldProduto_Id ='.$produto_id.'
                                                                     ORDER BY FIELD(tblibge_uf.fldCodigo, "'.$codigo_uf.'") DESC, fldSigla');
																	 echo mysql_error();
                                            while($rowEstado = mysql_fetch_array($rsEstado)){
												$n++;	
?>                                     		
                                                <li style="width:95px">
                                                    <input type="text" style="width:30px" id="txt_icms_st_pICMSST" name="txt_icms_st_pICMSST"  disabled="disabled"value="<?=$rowEstado['fldSigla']?>" />
                                                    <input type="text" style="width:50px;text-align:right" id="txt_icms_st_pICMSST_<?=$n?>" name="txt_icms_st_pICMSST_<?=$n?>" value="<?=format_number_out($rowEstado['fldicmsst_aliquota'])?>" />	
                                                </li>
<?												if($rowEstado['fldCodigo'] == $codigo_uf){											
													echo '<li style="margin-left:180px;text-align:right"><small style=" display:inline-table; margin-right:6px">exibir todos os estados</small><a href="#" id="exibir_aliquota_estado" class="mostrar_tudo"></a></li>';
													echo '<br \><div id="aliquota_icmsst_estado" style="display:none;width:98%; height:220px; border:1px solid #999;margin-top:15px">';
												}
											}
?>                                      	</div>
										</ul>
                                    </li>
									
									<li style="width:100%">
									    <label for="sel_icms_st_UFST">UF para a qual &eacute; devido o ICMS ST</label>
									    <select id="sel_icms_st_UFST" name="sel_icms_st_UFST">
                                        	<option value=""></option>
<?									   	 	$sql = "select * from tblnfe_icms_campo_ufst fldId";
											$rsAuxiliar = mysql_query($sql);
											$rowAuxiliar = mysql_fetch_array($rsAuxiliar);
											$rowDados = mysql_fetch_array(mysql_query("
												SELECT * FROM tblproduto_fiscal WHERE fldProduto_Id = ".$_GET['id']."
											"));
											
											while($rowAuxiliar = mysql_fetch_array($rsAuxiliar)){
?>									          <option <?=($rowAuxiliar['fldDescricao'] == $rowDados['fldicms_UFST'])? print "selected='selected'" : ""; ?> title="<?=$rowAuxiliar['fldId']?>" value="<?=$rowAuxiliar['fldDescricao']?>"><?=substr($rowAuxiliar['fldDescricao'],0,70)?></option>
<?											}
?>										</select>
									</li>
							     </ul>
						</fieldset>
					</div>
				</fieldset>
			</fieldset>		
<?
		//DESABILITAR OU NAO CAMPOS DE CADASTRO DE ACORDO COM MODALIDADE
		
		//IPI
			if($ipi_situacao_tributaria == 1 || $ipi_situacao_tributaria == 7 || $ipi_situacao_tributaria == 8 || $ipi_situacao_tributaria == 14){
				$ipiCalculoTipo = 'enable';
			}
			
			//PIS
			if($pis_situacao_tributaria == 3){
				
				$disabledPISAliqReais = "enable";
				$disabledPISQtdVendida = "enable";
				
			}elseif($pis_situacao_tributaria >= 9){
				
				$pisCalculoTipo = 'enable';
				if($pis_calculo_tipo == 1){
					
					$disabledPISAliqPorcentagem = "enable";
					$disabledPISBC = "enable";
					
				}elseif($pis_calculo_tipo == 2){
					
					$disabledPISAliqReais = "enable";
					$disabledPISQtdVendida = "enable";
					
				}
			}elseif($pis_situacao_tributaria < 9){
				if($pis_situacao_tributaria >= 3){
					
					$disabledPISAliqReais = "enable";
					$disabledPISQtdVendida = "enable";
					
				}elseif($pis_situacao_tributaria != 3){
					
					$disabledPISAliqPorcentagem = "enable";
					$disabledPISBC = "enable";
				}
			}
			
			//COFINS
			if($cofins_situacao_tributaria == 3){
				
				$disabledCOFINSAliqReais = "enable";
				$disabledCOFINSQtdVendida = "enable";
				
			}elseif($cofins_situacao_tributaria >= 9){
				
				$COFINSCalculoTipo = 'enable';
				
				if($cofins_calculo_tipo == 1){
					
					$disabledCOFINSAliqPorcentagem = "enable";
					$disabledCOFINSBC = "enable";
					
				}elseif($cofins_calculo_tipo == 2){
					
					$disabledCOFINSAliqReais = "enable";
					$disabledCOFINSQtdVendida = "enable";
				}
			}elseif($cofins_situacao_tributaria < 9){
				if($cofins_situacao_tributaria >= 3){
					
					$disabledCOFINSAliqReais = "enable";
					$disabledCOFINSQtdVendida = "enable";
					
				}elseif($cofins_situacao_tributaria != 3){
					
					$disabledCOFINSAliqPorcentagem = "enable";
					$disabledCOFINSBC = "enable";
				}
			}
			
			
        if(fnc_nfe_regime() > '1'){
?>
            <fieldset id="fset_IPI">
                <legend>IPI</legend>
                <ul style="width:940px">
                    <li>
                        <label for="sel_ipi_situacao_tributaria">Situa&ccedil;&atilde;o Tribut&aacute;ria</label>
                        <select id="sel_ipi_situacao_tributaria" name="sel_ipi_situacao_tributaria" style="width:400px">
                        	<option value=""></option>
<?							$sql = "select * from tblnfe_ipi";
                            $rsIPI_Situacao = mysql_query($sql);
                            while($rowIPI_Situacao = mysql_fetch_array($rsIPI_Situacao)){
?>								<option <?=$ipi_situacao_tributaria==$rowIPI_Situacao["fldId"] ? print 'selected="selected"' : '' ?> value="<?=$rowIPI_Situacao["fldId"]?>" title="<?=$rowIPI_Situacao["fldDescricao"]?>"><?=$rowIPI_Situacao["fldSituacaoTributaria"] . ' - ' . substr($rowIPI_Situacao["fldDescricao"],0,130)?></option>
<?							}
?>							</select>
                    </li>
                    <li>	
                        <label for="txt_ipi_enquadramento_classe">Classe de Enquadramento (cigarros e bebidas)</label>
                        <input type="text" style="width:280px" id="txt_ipi_enquadramento_classe" name="txt_ipi_enquadramento_classe" value="<?=$ipi_enquadramento_classe?>"/> 
                    </li>
                    <li>
                        <label for="txt_ipi_enquadramento_codigo">C&oacute;digo de Enquadramento Legal</label>
                        <input type="text" style="width:200px" id="txt_ipi_enquadramento_codigo" name="txt_ipi_enquadramento_codigo" value="<?=$ipi_enquadramento_codigo?>"/>
                    </li>
                    <li>
                        <label for="txt_ipi_produtor_cnpj">CNPJ do Produtor</label>
                        <input type="text" style="width:200px" id="txt_ipi_produtor_cnpj" name="txt_ipi_produtor_cnpj" value="<?=$ipi_produtor_cnpj?>" maxlength="17" />
                    </li>
                    <li id="li_calculo_ipi_js">
                        <label for="sel_ipi_calculo_tipo">Tipo de C&aacute;lculo</label>
                        <select name="sel_ipi_calculo_tipo" id="sel_ipi_calculo_tipo" <?=($ipiCalculoTipo != 'enable' )? "disabled='disabled'" : ''?> style="width:186px">
                        	<option value=""></option>
                            <option <?=$ipi_calculo_tipo==1 ? print 'selected="selected"' : '' ?> value="1">Percentual</option>
                            <option <?=$ipi_calculo_tipo==2 ? print 'selected="selected"' : '' ?> value="2">Valor</option>
                        </select>
                    </li>
                    <li>
                        <label for="txt_ipi_aliquota">Al&iacute;quota</label>
                        <input type="text" style="width:132px;text-align:right;<?=($ipi_calculo_tipo != 1)? "background:#F1F1F1" : ''?>" id="txt_ipi_aliquota" name="txt_ipi_aliquota" <?=($ipi_calculo_tipo != 1)? "disabled='disabled'" : "value='$ipi_aliquota'" ?> />
                    </li>
                    <li>
                        <label for="txt_ipi_unidade_valor">Valor por Unidade</label>
                        <input type="text" style="width:132px;text-align:right;<?=($ipi_calculo_tipo < 2)? "background:#F1F1F1" : ''?>" id="txt_ipi_unidade_valor" name="txt_ipi_unidade_valor"<?=($ipi_calculo_tipo < 2)? "disabled='disabled'" : "value='$ipi_unidade_valor'" ?> />
                    </li>
                </ul>
            </fieldset>
<?		}
?>
            <fieldset id="fset_PSI">
                <legend>PIS</legend>
                <ul>
                    <li>
                        <label for="sel_pis_situacao_tributaria">Situa&ccedil;&atilde;o Tribut&aacute;ria</label>
                        <select id="sel_pis_situacao_tributaria" name="sel_pis_situacao_tributaria" style="width:400px">
                            <option value=""></option>
<?							$sql = "SELECT * FROM tblnfe_pis";
                            $rsPIS_Situacao = mysql_query($sql);
                            while($rowPIS_Situacao = mysql_fetch_array($rsPIS_Situacao)){
?>								<option <?=$pis_situacao_tributaria==$rowPIS_Situacao["fldId"] ? print 'selected="selected"' : '' ?> value="<?=$rowPIS_Situacao["fldId"]?>" title="<?=$rowPIS_Situacao["fldDescricao"]?>"><?=$rowPIS_Situacao["fldSituacaoTributaria"] . ' - ' . substr($rowPIS_Situacao["fldDescricao"],0,130)?></option>
<?							}
?>						</select>
                    </li>
                    <li id="li_calculo_pis_js">
                        <label for="sel_pis_calculo_tipo">Tipo de C&aacute;lculo</label>
                        <select name="sel_pis_calculo_tipo" id="sel_pis_calculo_tipo" style="width:170px;" <?=($pisCalculoTipo != 'enable' )? "disabled='disabled'" : ''?> >
                            <option value=""></option>
                            <option <?=$pis_calculo_tipo== 1 ? print 'selected="selected"' : '' ?> value="1">Percentual</option>
                            <option <?=$pis_calculo_tipo== 2 ? print 'selected="selected"' : '' ?> value="2">Valor</option>
                        </select>
                    </li>
                    <li>
                        <label for="txt_pis_aliquota_percentual">Al&iacute;quota (percentual)</label>
                        <input type="text" style="width:150px;text-align:right;<?=($disabledPISAliqPorcentagem != "enable")? "background:#F1F1F1" : ''?>" id="txt_pis_aliquota_percentual" name="txt_pis_aliquota_percentual"<?=($disabledPISAliqPorcentagem != "enable" )? "disabled='disabled'" : "value='$pis_aliquota_porcentagem'" ?>  />
                    </li>
                    <li>
                        <label for="txt_pis_aliquota_reais">Al&iacute;quota (reais)</label>
                        <input type="text" style="width:150px;text-align:right;<?=($disabledPISAliqReais != "enable")? "background:#F1F1F1" : ''?>" id="txt_pis_aliquota_reais" name="txt_pis_aliquota_reais" <?=($disabledPISAliqReais != "enable" )? "disabled='disabled'" : "value='$pis_aliquota_reais'" ?> />
                    </li>
                </ul>
				<fieldset id="fset_PISST" style="width: 920px; margin-bottom:5px">
                    <legend>PIS ST</legend>
                    <ul>
                        <li id="li_calculo_pisST_js">
                            <label for="sel_pis_st_calculo_tipo">Tipo de C&aacute;lculo</label>
                            <select name="sel_pis_st_calculo_tipo" id="sel_pis_st_calculo_tipo" style="width:170px;">
                                <option value=""></option>
                                <option <? $pisST_calculo_tipo== 1 ? print 'selected="selected"' : '' ?> value="1">Percentual</option>
                                <option <? $pisST_calculo_tipo== 2 ? print 'selected="selected"' : '' ?> value="2">Valor</option>
                            </select>
                        </li>
                        <li>
                            <label for="txt_pis_st_aliquota_percentual">Al&iacute;quota (percentual)</label>
                            <input type="text" style="width:150px;text-align:right;<?=($pisST_calculo_tipo != 1)? "background:#F1F1F1" : ''?>" id="txt_pis_st_aliquota_percentual" name="txt_pis_st_aliquota_percentual" <?=($pisST_calculo_tipo  != 1)? "disabled='disabled'" : "value='$pisST_aliquota_porcentagem'" ?>/>
                        </li>
                        <li>
                            <label for="txt_pis_st_aliquota_reais">Al&iacute;quota (reais)</label>
                            <input type="text" style="width:150px;text-align:right;<?=($pisST_calculo_tipo < 2)? "background:#F1F1F1" : ''?>" id="_pis_st_aliquota_reais" name="_pis_st_aliquota_reais" <?=($pisST_calculo_tipo < 2 )? "disabled='disabled'" : "value='$pisST_aliquota_reais'"?> />
                        </li>
                    </ul>
                </fieldset>
            </fieldset>	
            
            <fieldset id="fset_COFINS">
            	<legend>COFINS</legend>
                <ul>
                    <li>
                        <label for="sel_cofins_situacao_tributaria">Situa&ccedil;&atilde;o Tribut&aacute;ria</label>
                        <select id="sel_cofins_situacao_tributaria" name="sel_cofins_situacao_tributaria" style="width:400px">
                            <option value=""></option>
<?							$sql = "SELECT * FROM tblnfe_cofins";
                            $rsCofins_Situacao = mysql_query($sql);
                            while($rowCofins_Situacao = mysql_fetch_array($rsCofins_Situacao)){
?>								<option <?=$cofins_situacao_tributaria==$rowCofins_Situacao["fldId"] ? print 'selected="selected"' : '' ?> value="<?=$rowCofins_Situacao["fldId"]?>" title="<?=$rowCofins_Situacao["fldDescricao"]?>"><?=$rowCofins_Situacao['fldCOFINS'] . $rowCofins_Situacao["fldSituacaoTributaria"] . ' - ' . substr($rowCofins_Situacao["fldDescricao"],0,130)?></option>
<?							}
?>						</select>
                    </li>
                    <li id="li_calculo_cofins_js">
                        <label for="sel_cofins_calculo_tipo">Tipo de C&aacute;lculo</label>
                        <select name="sel_cofins_calculo_tipo" id="sel_cofins_calculo_tipo" style="width:170px;" <?=($COFINSCalculoTipo != 'enable' )? "disabled='disabled'" : ''?> >
                            <option value=""></option>
                            <option <?=$cofins_calculo_tipo== 1 ? print 'selected="selected"' : '' ?> value="1">Percentual</option>
                            <option <?=$cofins_calculo_tipo== 2 ? print 'selected="selected"' : '' ?> value="2">Valor</option>
                        </select>
                    </li>
                    <li>
                        <label for="txt_cofins_aliquota_percentual">Al&iacute;quota (percentual)</label>
                        <input type="text" style="width:150px;text-align:right;<?=($disabledCOFINSAliqPorcentagem != "enable")? "background:#F1F1F1" : ''?>" id="txt_cofins_aliquota_percentual" name="txt_cofins_aliquota_percentual"  <?=($disabledCOFINSAliqPorcentagem != "enable" )? "disabled='disabled'" : "value='$cofins_aliquota_porcentagem'" ?> />
                    </li>
                    <li>
                        <label for="txt_cofins_aliquota_reais">Al&iacute;quota (reais)</label>
                        <input type="text" style="width:150px;text-align:right;<?=($disabledCOFINSAliqReais != "enable")? "background:#F1F1F1" : ''?>" id="txt_cofins_aliquota_reais" name="txt_cofins_aliquota_reais"  <?=($disabledCOFINSAliqReais != "enable" )? "disabled='disabled'" : "value='$cofins_aliquota_reais'" ?> />
                    </li>
                </ul>
                <fieldset style="width:920px;margin:10px">
                    <legend>COFINS ST</legend>
                    <ul>
                        <li id="li_calculo_cofinsST_js">
                            <label for="sel_cofins_st_calculo_tipo">Tipo de C&aacute;lculo</label>
                            <select name="sel_cofins_st_calculo_tipo" id="sel_cofins_st_calculo_tipo" style="width:170px;">
                                <option value=""></option>
                                <option <?=$cofinsST_calculo_tipo== 1 ? print 'selected="selected"' : '' ?> value="1">Percentual</option>
                                <option <?=$cofinsST_calculo_tipo== 2 ? print 'selected="selected"' : '' ?> value="2">Valor</option>
                            </select>
                        </li>
                        <li>
                            <label for="txt_cofins_st_aliquota_percentual">Al&iacute;quota (percentual)</label>
                            <input type="text" style="width:150px;text-align:right;<?=($cofinsST_calculo_tipo != 1)? "background:#F1F1F1" : ''?>" id="txt_cofins_st_aliquota_percentual" name="txt_cofins_st_aliquota_percentual" <?=($cofinsST_calculo_tipo != 1)? "disabled='disabled'" : "value='$cofinsST_aliquota_porcentagem'" ?> />
                        </li>
                        <li>
                            <label for="txt_cofins_st_aliquota_reais">Al&iacute;quota (reais)</label>
                            <input type="text" style="width:150px;text-align:right;<?=($cofinsST_calculo_tipo < 2)? "background:#F1F1F1" : ''?>" id="txt_cofins_st_aliquota_reais" name="txt_cofins_st_aliquota_reais" <?=($cofinsST_calculo_tipo < 2 )? "disabled='disabled'" : "value='$cofinsST_aliquota_reais'" ?>  />
                        </li>
                    </ul>
                </fieldset>
            </fieldset>

            <fieldset id="fset_produto_especifico">
            	<legend>Produto Específico</legend>

            	<ul style="margin-left:8px">
            		<li>
            			<label for="sel_prod_especifico_tipo">Tipo de produto</label>
            			<select name="sel_prod_especifico_tipo" id="sel_prod_especifico_tipo" style="width:170px;">
            				<option></option>
            				<?php
            					$sqlTipo_Fiscal = mysql_query("SELECT * FROM tblproduto_fiscal_tipo_especifico");
            					while($rowTipo_Fiscal = mysql_fetch_assoc($sqlTipo_Fiscal)){ ?>
		            			<option id="<?=$rowTipo_Fiscal['fldId'];?>" value="<?=$rowTipo_Fiscal['fldId'];?>" <?=($rowProduto_Fiscal['fldTipo_Especifico'] == $rowTipo_Fiscal['fldId']) ? "selected='selected'" : '';?> ><?=$rowTipo_Fiscal['fldTipo'];?></option>
		            		<? }
            				?>
            			</select>
            		</li>
            	</ul>

            	<div id="div_prod_especifico_1" class="div_prod_especifico" style="clear:both; display:none">
	            	<fieldset id="fset_prod_especifico_combustivel" style="width:920px;">
	            		<legend>Combustivel</legend>

	            		<ul>
	            			<li>
	            				<label for="sel_produto_especifico_combustivel_codanp">Código ANP</label>
			            		<select name="sel_produto_especifico_combustivel_codanp" id="sel_produto_especifico_combustivel_codanp" style="width:170px">
			            			<option></option>
			            			<?php
										$sqlANP = mysql_query("SELECT * FROM tblproduto_fiscal_anp");
										while($rowANP = mysql_fetch_assoc($sqlANP)){ ?>
										<option id="<?=$rowANP['fldId'];?>" value="<?=$rowANP['fldId'];?>" <?=($rowProduto_Fiscal['fldCombustivel_Codigo_ANP'] == $rowANP['fldId']) ? "selected='selected'" : '';?> ><?=$rowANP['fldCodigo'];?></option>
			            			<? }
			            			?>
			            		</select>
	            			</li>
	            			<li>
	            				<label for="txt_produto_especifico_combustivel_codif">CODIF</label>
	            				<input name="txt_produto_especifico_combustivel_codif" id="txt_produto_especifico_combustivel_codif" type="text" style="width:170px" value="<?=$rowProduto_Fiscal['fldCombustivel_Codigo_CodIF']?>" />
	            			</li>
	            			<li>
	            				<label for="sel_produto_especifico_combustivel_uf">UF</label>
			            		<select style="width:60px" id="sel_produto_especifico_combustivel_uf" name="sel_produto_especifico_combustivel_uf">
			            			<option></option>
			            			<?php
										$sqlUF = mysql_query("SELECT * FROM tblibge_uf");
										while($rowUF = mysql_fetch_assoc($sqlUF)){ ?> 
										<option id="<?=$rowUF['fldCodigo'];?>" value="<?=$rowUF['fldCodigo'];?>" <?=($rowProduto_Fiscal['fldCombustivel_UF'] == $rowUF['fldCodigo']) ? "selected='selected'" : '';?> ><?=$rowUF['fldSigla'];?></option>
			            			<? }
			            			?>
			            		</select>
	            			</li>
	            			<li>
	            				<label for="txt_produto_especifico_combustivel_qtd_faturada">Qtd. faturada em temperatura ambiente</label>
	            				<input type="text" style="width:250px" name="txt_produto_especifico_combustivel_qtd_faturada" id="txt_produto_especifico_combustivel_qtd_faturada" value="<?=$rowProduto_Fiscal['fldCombustivel_Qtd_Faturada']?>"/>
	            			</li>
	            		</ul>
	            	</fieldset>

	            	<fieldset style="width:920px; margin-bottom:10px">
	            		<legend>CIDE</legend>

	            		<ul>
	            			<li>
	            				<label for="txt_produto_especifico_combustivel_cide_bc">Base de cálculo</label>
	            				<input type="text" style="width:170px" name="txt_produto_especifico_combustivel_cide_bc" id="txt_produto_especifico_combustivel_cide_bc" value="<?=$rowProduto_Fiscal['fldCombustivel_Cide_bCalculo'];?>" />
	            			</li>
	            			<li>
	            				<label for="txt_produto_especifico_combustivel_cide_aliquota">Alíquota</label>
	            				<input type="text" style="width:170px" name="txt_produto_especifico_combustivel_cide_aliquota" id="txt_produto_especifico_combustivel_cide_aliquota" value="<?=$rowProduto_Fiscal['fldCombustivel_Cide_Aliquota'];?>" />
	            			</li>
	            			<li>
	            				<label for="txt_produto_especifico_combustivel_cide_valor">Valor</label>
	            				<input type="text" style="width:170px" name="txt_produto_especifico_combustivel_cide_valor" id="txt_produto_especifico_combustivel_cide_valor" value="<?=$rowProduto_Fiscal['fldCombustivel_Cide_Valor'];?>" readonly='readonly'/>
	            			</li>
	            		</ul>

	            	</fieldset>
	            </div> <!-- DIV COMBUSTIVEL -->

            </fieldset>

            <li>
                <input type="submit" style="margin-top:14px; float:right" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="salvar" title="Salvar" />
            </li>
        </form>
    </div>

    <script>

    	$(document).ready(function(){
    		var value = $('#sel_prod_especifico_tipo').find('option:selected').attr('id');
    		$('.div_prod_especifico').css('display','none');
    		$('#div_prod_especifico_'+value).css('display','block');
    	})

    	$('#sel_prod_especifico_tipo').change(function(){
    		var value = $(this).find('option:selected').attr('id');
    		$('.div_prod_especifico').css('display','none');
    		$('#div_prod_especifico_'+value).css('display','block');
    	})

		$('#exibir_aliquota_estado').click(function(event){
    		event.preventDefault();
		 	$("div#aliquota_icmsst_estado").slideToggle("slow");
		});
		$('#exibir_mva_estado').click(function(event){
    		event.preventDefault();
		 	$("div#mva_icmsst_estado").slideToggle("slow");
		});
		
		$("[name^='txt_icms_st_pMVAST_'], [name^='txt_icms_st_pICMSST']").change(function(event){
    		event.preventDefault();
		 	$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		});
    </script>