<?	
	require('financeiro_cheque_filtro.php');
	
	//a��es em grupo
	if(isset($_POST['hid_action'])){
		require("financeiro_cheque_action.php");
	}
	
	if(isset($_GET['id'])){
		if($_GET['cheque'] == 'erro'){//se veio duplicado
?>			<div class="alert">
				<p class="erro">O cheque <?=$_GET['id']?> j&aacute; foi cadastrado!</p>
			</div>
<?		}elseif($_GET['edit']){
?>			<div class="alert">
				<p class="ok">Cheque <?=$_GET['id']?> alterado com sucesso!<p>
     	  </div>
<?		}else{
?>			<div class="alert">
				<p class="ok">Cheque <?=$_GET['id']?> cadastrado com sucesso!<p>
     	  </div>
<?		}
	}
	
/**************************** ORDER BY *******************************************/
	$filtroOrder = 'fldDataCadastro ';
	$class 		  = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_cheque']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'cpf'			:  $filtroOrder = "fldCPF_CNPJ";   		break;
			case 'nome'			:  $filtroOrder = "fldNome";   			break;
			case 'vencimento'	:  $filtroOrder = "fldVencimento";   	break;
			case 'valor'		:  $filtroOrder = "fldValor"; 			break;
			case 'data'			:  $filtroOrder = "fldDataCadastro";	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_cheque'] = (!$_SESSION['order_cheque'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_cheque'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=financeiro&modo=cheque$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_cheque']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class

/**************************** PAGINA��O *******************************************/
	$sSQL =  "SELECT tblcheque.*, tbltipo_origem.fldTipo as fldOrigem_Tipo, tbltipo_destino.fldTipo as fldDestino_Tipo
			 FROM tblcheque
			 LEFT JOIN tblfinanceiro_conta_fluxo_tipo tbltipo_origem ON tblcheque.fldOrigem_Movimento_Id = tbltipo_origem.fldId
			 LEFT JOIN tblfinanceiro_conta_fluxo_tipo tbltipo_destino ON tblcheque.fldDestino_Movimento_Id = tbltipo_destino.fldId
			  ".$_SESSION['filtro_cheque']." ORDER BY ".$_SESSION['order_cheque'];

	$rsTotal = mysql_query($sSQL);	
	echo mysql_error();
	$rowsTotal = mysql_num_rows($rsTotal);
	
	//defini��o dos limites
	$limite = 150;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " limit " . $inicio . "," . $limite;
	$rsCheque = mysql_query($sSQL);
	$pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : "1";
	
#########################################################################################
?>
	<form class="table_form" id="frm_cheques" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho" style="width:auto;">
                <ul class="table_cabecalho">
                
                	<li class="order" style="width:70px; text-align:center">
                    	<a <?= ($filtroOrder == 'fldDataCadastro') ? print "class='$class'" : '' ?> style="width:55px" href="<?=$raiz?>data">Cadastro</a>
                    </li>
                    <li style="width:40px;text-align:right">Banco</li>
                    <li style="width:40px;text-align:right">Ag.</li>
                    <li style="width:80px;text-align:right">Conta</li>
                    <li style="width:60px;text-align:right">N&uacute;mero</li>
                    <li class="order" style="width:70px;text-align:right">
                    	<a <?= ($filtroOrder == 'fldValor') 	? "class='$class'" : '' ?> style="width:55px" href="<?=$raiz?>valor">Valor</a>
                    </li>
                    <li class="order" style="width:90px; text-align:center">
                    	<a <?= ($filtroOrder == 'fldVencimento')? "class='$class'" : '' ?> style="width:75px" href="<?=$raiz?>vencimento">Vencimento</a>
                    </li>
                    <li style="width:100px">Entidade</li>
                    <li class="order" style="width:125px">
                    	<a <?= ($filtroOrder == 'fldNome') 		? "class='$class'" : '' ?> style="width:100px" href="<?=$raiz?>nome">Correntista</a>
                    </li>
                    <li class="order" style="width:80px;text-align:center">
                    	<a <?= ($filtroOrder == 'fldCPF_CNPJ') 	? "class='$class'" : '' ?> style="width:65px" href="<?=$raiz?>cpf">CPF/CNPJ</a>
                    </li>
                    <li style="width:50px;text-align:right">Origem</li>
                    <li style="width:50px;text-align:right">Destino</li>
                    <li style="width:15px">&nbsp;</li>
                    <li style="width:15px"><input style="width:15px" type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
     		<div id="table_container">       
            	<table id="table_general" class="table_general" summary="Lista de cheques">
                	<tbody>
<?						
						$n = 0;	
						$id_array 	= array();                        
                        $linha 		= "row";
						$rows 		= mysql_num_rows($rsCheque);
						while($rowCheque = mysql_fetch_array($rsCheque)){
							
							unset($origem);
							unset($destino);
							unset($entidade);
							//ORIGEM
							if($rowCheque['fldOrigem_Id'] >0){
								$origem = $rowCheque['fldOrigem_Tipo'].' '.str_pad($rowCheque['fldOrigem_Id'], 5, '0', STR_PAD_LEFT);
							
								switch($rowCheque['fldOrigem_Movimento_Id']){
									case '3': //venda
										$rsEntidade  = mysql_query("SELECT tblcliente.fldNome FROM tblpedido 
																	INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId	WHERE tblpedido.fldId IN (".$rowCheque['fldOrigem_Id'].")");
										$rowEntidade = mysql_fetch_array($rsEntidade);
										echo mysql_error();
										$entidade	 = $rowEntidade['fldNome'];
									break;
								}
							}
							//DESTINO
							if($rowCheque['fldDestino_Id'] >0){
								$destino = $rowCheque['fldDestino_Tipo'].' '.str_pad($rowCheque['fldDestino_Id'], 5, '0', STR_PAD_LEFT);
								
								switch($rowCheque['fldDestino_Movimento_Id']){
									case '4': //compra
										$rsEntidade  = mysql_query("SELECT tblfornecedor.fldNomeFantasia FROM tblcompra 
																	INNER JOIN tblfornecedor ON tblcompra.fldFornecedor_Id = tblfornecedor.fldId
																	WHERE tblcompra.fldId IN (".$rowCheque['fldDestino_Id'].")");
										$rowEntidade = mysql_fetch_array($rsEntidade);
										$entidade 	 = $rowEntidade['fldNomeFantasia'];
									break;
									case '6': // conta programada
									
										$rsConta = mysql_query("SELECT tblfinanceiro_conta_pagar_programada_baixa.fldId, tblfinanceiro_conta_pagar_programada.fldNome FROM 
																		tblfinanceiro_conta_pagar_programada_baixa INNER JOIN tblfinanceiro_conta_pagar_programada
																		ON tblfinanceiro_conta_pagar_programada_baixa.fldContaProgramada_Id = tblfinanceiro_conta_pagar_programada.fldId
																		WHERE tblfinanceiro_conta_pagar_programada_baixa.fldExcluido = 0 AND tblfinanceiro_conta_pagar_programada_baixa.fldId =".$rowCheque['fldDestino_Id']);
										$rowConta = mysql_fetch_array($rsConta);
										$destino = $rowCheque['fldDestino_Tipo'].' '.$rowConta['fldNome'];
									break;
								}
							}
							
							$entidade 		= ($rowCheque['fldEntidade']) ? $rowCheque['fldEntidade'] : $entidade;
							$id_array[$n]	= $rowCheque["fldId"];
							$totalSaldo  	+= $rowCheque['fldValor'];
							$n += 1;
							
?>							<tr class="<?= $linha; ?>">
                                <td style="width:70px; text-align: center"><?=format_date_out3($rowCheque['fldDataCadastro'])?></td>
                                <td style="width:40px;text-align:right"><?=$rowCheque['fldBanco']?></td>
                                <td style="width:40px;text-align:right"><?=$rowCheque['fldAgencia']?></td>
                                <td style="width:80px;text-align:right"><?=$rowCheque['fldConta']?></td>
                                <td style="width:60px;text-align:right"><?=$rowCheque['fldNumero']?></td>
                                <td style="width:70px;text-align:right"><?=format_number_out($rowCheque['fldValor'])?></td>
                                <td style="width:90px;text-align:center"><?=format_date_out3($rowCheque['fldVencimento'])?></td>
                                <td style="width:100px" title="<?=substr($entidade,0,12)?>"><?=substr($entidade,0,12)?></td>
                                <td style="width:125px" title="<?=substr($rowCheque['fldNome'],0,12)?>"><?=substr($rowCheque['fldNome'],0,12)?></td>
                                <td style="width:80px;text-align:right"><?=$rowCheque['fldCPF_CNPJ']?></td>
                                <td style="width:50px"><?= ($origem != '') 	? "<a class='detalhe' title='$origem'></a>" 	: '' ?></td>
                                <td style="width:50px"><?= ($destino != '') ? "<a class='detalhe' title='$destino'></a>" 	: '' ?></td>
                                <td style="width:15px"><a class="edit modal" href="financeiro_cheque,<?=$rowCheque['fldId']?>" name="<?=$rowCheque['fldId']?>" title="editar" rel="780-150"></a></td>
                                <td style="width:15px"><input style="width:15px" type="checkbox" name="chk_cheque_<?=$rowCheque['fldId']?>" id="chk_cheque_<?=$rowCheque['fldId']?>" title="selecionar o registro posicionado" /></td>
                            </tr>
<?							$linha = ($linha == "row" ?	$linha = "dif-row": $linha = "row");
						}
?>					</tbody>
				</table>
			</div>
   
            <input type="hidden" name="hid_array"  id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
         
            <div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo modal" href="financeiro_cheque" rel="780-150">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" value="excluir" title="Excluir cheque(s) selecionado(s)" onclick="return confirm('Deseja excluir os cheques selecionados?')" /></li>
                </ul>
            </div> 
            <div class="saldo" style=" width: 210px;margin-left:194px">
                <span style="margin-left: 25px">Total cheques</span>
                <span style="margin-left: 10px" class="<?= fnc_status_saldo($totalSaldo)?>"><?= format_number_out($totalSaldo)?></span>
            </div>
            
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=financeiro&modo=cheque";
				include("paginacao.php")
?>          </div>   
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>  
		</div>
    </form>
