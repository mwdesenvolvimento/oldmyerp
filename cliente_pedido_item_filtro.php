<?php

	//recebendo a data do calend�rio de per�odo
	if(isset($_POST['txt_calendario_data_inicial']) && isset($_POST['txt_calendario_data_final'])){
	    $_SESSION['txt_data_calendario1'] = (isset($_POST['txt_calendario_data_inicial'])) 	? $_POST['txt_calendario_data_inicial'] : ''; 
	    $_SESSION['txt_data_calendario2'] = (isset($_POST['txt_calendario_data_final'])) 	? $_POST['txt_calendario_data_final'] 	: ''; 
	}
	
	//memorizar os filtros para exibi��o nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_cliente_pedido_item_cod_produto'] = "";
		$_SESSION['txt_cliente_pedido_item_cod_venda'] 	= "";
		$_SESSION['txt_cliente_pedido_item_produto'] 	= "";
		$_SESSION['sel_cliente_pedido_item_pago'] 		= "0"; 
		$_SESSION['txt_data_calendario1'] 				= "";
		$_SESSION['txt_data_calendario2'] 				= "";
		$_POST['chk_produto'] 							=  false;
	}
	else{
		$_SESSION['txt_cliente_pedido_item_cod_produto'] = (isset($_POST['txt_cliente_pedido_item_cod_produto']) ? $_POST['txt_cliente_pedido_item_cod_produto'] : $_SESSION['txt_cliente_pedido_item_cod_produto']);
		$_SESSION['txt_cliente_pedido_item_cod_venda'] 	 = (isset($_POST['txt_cliente_pedido_item_cod_venda']) 	 ? $_POST['txt_cliente_pedido_item_cod_venda'] 	 : $_SESSION['txt_cliente_pedido_item_cod_venda']);
		$_SESSION['txt_cliente_pedido_item_produto'] 	 = (isset($_POST['txt_cliente_pedido_item_produto']) 	 ? $_POST['txt_cliente_pedido_item_produto'] 	 : $_SESSION['txt_cliente_pedido_item_produto']);
		$_SESSION['sel_cliente_pedido_item_pago'] 		 = (isset($_POST['sel_cliente_pedido_item_pago']) 		 ? $_POST['sel_cliente_pedido_item_pago'] 		 : $_SESSION['sel_cliente_pedido_item_pago']);
		$_SESSION['txt_data_calendario1']				 = (isset($_POST['txt_data_calendario1'])				 ? $_POST['txt_data_calendario1']				 : $_SESSION['txt_data_calendario1']);
		$_SESSION['txt_data_calendario2']				 = (isset($_POST['txt_data_calendario2'])				 ? $_POST['txt_data_calendario2']				 : $_SESSION['txt_data_calendario2']);
	}

?>
<form id="frm-filtro" action="index.php?p=cliente_detalhe&id=<?=trim($_GET['id']);?>&modo=pedido&aba=item" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
    	<li>
<?			$codigo_produto = ($_SESSION['txt_cliente_pedido_item_cod_produto'] ? $_SESSION['txt_cliente_pedido_item_cod_produto'] : "c&oacute;digo");
?>      	<input style="width: 80px" type="text" name="txt_cliente_pedido_item_cod_produto" id="txt_cliente_pedido_item_cod_produto" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo_produto?>"/>
		</li>
	
		<li>
<?			$codigo_venda = ($_SESSION['txt_cliente_pedido_item_cod_venda'] ? $_SESSION['txt_cliente_pedido_item_cod_venda'] : "venda");
?>      	<input style="width: 80px" type="text" name="txt_cliente_pedido_item_cod_venda" id="txt_cliente_pedido_item_cod_venda" onfocus="limpar (this,'venda');" onblur="mostrar (this, 'venda');" value="<?=$codigo_venda?>"/>
		</li>
        
		<li style="height:40px">
        	<input type="checkbox" name="chk_produto" id="chk_produto" <?=($_POST['chk_produto'] == true) ? print 'checked ="checked"' : ''?> />
<?			$produto = ($_SESSION['txt_cliente_pedido_item_produto']) ? $_SESSION['txt_cliente_pedido_item_produto'] : "produto";
			($_SESSION['txt_cliente_pedido_item_produto'] == "produto") ? $_SESSION['txt_cliente_pedido_item_produto'] = '' : '';
?>     		<input style="width: 220px" type="text" name="txt_cliente_pedido_item_produto" id="txt_cliente_pedido_item_produto" onfocus="limpar (this,'produto');" onblur="mostrar (this, 'produto');" value="<?=$produto?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>   
        <li>
            <select id="sel_cliente_pedido_item_pago" name="sel_cliente_pedido_item_pago" style="width: 120px" >
                <option <?=($_SESSION['sel_cliente_pedido_item_pago'] == "0") ? 'selected="selected"' : '' ?> value="0">todos </option>
                <option <?=($_SESSION['sel_cliente_pedido_item_pago'] == "1") ? 'selected="selected"' : '' ?> value="1">pago </option>
                <option <?=($_SESSION['sel_cliente_pedido_item_pago'] == "2") ? 'selected="selected"' : '' ?> value="2">n&atilde;o pago</option>
			</select>
		</li>
        <li>
      		<label for="txt_data_calendario1">Per&iacute;odo: </label>
<?			$data1 = ($_SESSION['txt_data_calendario1'] ? $_SESSION['txt_data_calendario1'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 70px" type="text" name="txt_data_calendario1" id="txt_data_calendario1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
		<li>
<?			$data2 = ($_SESSION['txt_data_calendario2'] ? $_SESSION['txt_data_calendario2'] : "");
?>     		<input title="Data final" style="text-align:center;width: 70px" type="text" name="txt_data_calendario2" id="txt_data_calendario2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="pedido_calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,pedido, <?=$_GET['id'];?>,pedido&aba=item" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
    </ul>
    
    <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
    <button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
  </fieldset>
</form>

<?
	if(format_date_in($_SESSION['txt_data_calendario1']) != ""){

		if(format_date_in($_SESSION['txt_data_calendario2']) != ""){
			$filtro .= " and tblpedido.fldPedidoData between '".format_date_in($_SESSION['txt_data_calendario1'])."' and '".format_date_in($_SESSION['txt_data_calendario2'])."'";
		}else{
			$filtro .= " and tblpedido.fldPedidoData = '".format_date_in($_SESSION['txt_data_calendario1'])."'";
		}
	}
	
	if(is_numeric($_SESSION['txt_cliente_pedido_item_cod_venda'])){
		$filtro .= " and tblpedido.fldId = '".$_SESSION['txt_cliente_pedido_item_cod_venda']."'";
	}
	
	if(is_numeric($_SESSION['txt_cliente_pedido_item_cod_produto'])){
		$filtro .= " and tblproduto.fldCodigo = '".$_SESSION['txt_cliente_pedido_item_cod_produto']."'";
	}
	
	if($_SESSION['txt_cliente_pedido_item_produto']){
		if($_POST['chk_produto'] == true){
			$filtro .= " AND tblproduto.fldNome like '%$produto%'";
		}else{
			$filtro .= " AND tblproduto.fldNome like '$produto%'";
		}
	}
	
	if(($_SESSION['sel_cliente_pedido_item_pago']) != "0"){
		switch($_SESSION['sel_cliente_pedido_item_pago']){
			case "0":
				$filtro .= "";
			break;
			
			case "1":
				$filtro .= " AND tblpedido_item_pago.fldQuantidade > 0 ";
			break;

			case "2":
				$filtro_having = " HAVING (tblpedido_item.fldQuantidade > fldQuantidadePaga OR fldQuantidadePaga IS NULL)";
			break;
		}
	}	
	
	//transferir para a sess�o
	$_SESSION['filtro_cliente_pedido_item'] = $filtro;

?>