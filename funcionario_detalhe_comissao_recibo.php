
<script type="text/javascript">
    window.print();
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
        <title>Impress&atilde;o de recibo</title>
		<link rel="stylesheet" type="text/css" href="style/impressao/style_pedido_imprimir_recibo_A4.css" />
    	<link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
    
	</head>
	<body>
    
<? 
	ob_start();
	session_start();
	
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	$rsDados 	= mysql_query("SELECT * FROM tblempresa_info");
	$rowDados 	= mysql_fetch_array($rsDados);
	$filtro_id	= explode(' ', $_GET["id"]);
	$filtro_id 	= array_filter($filtro_id); //RETIRANDO ARRAY NULL
	echo mysql_error();
	$data = date("Y-m-d");
	$hora = date("H:i:s");
	
?>
    <div id="no-print">
        <a class="print" href="#" onClick="window.print()">imprimir</a>
    </div>
<?	
	$funcionario_id = $_GET['funcionario'];
	foreach($filtro_id as $filtro){
		$filtro 			= explode(',', $filtro);
		$pedido_id 			= $filtro[0];
		$funcao				= $filtro[1];
		
		
		if($_GET['tipo'] == 'total'){
			$parametro = "(SELECT SUM(fldValor) FROM tblfuncionario_conta_fluxo WHERE fldReferencia_Id = tblpedido_funcionario_servico.fldPedido_Id
							AND fldEstorno = '0' AND fldId NOT IN (SELECT fldEstorno FROM tblfuncionario_conta_fluxo WHERE fldFuncionario_Id = $funcionario_id AND fldEstorno >0))";
		}elseif($_GET['tipo'] == 'baixa'){
			$parametro = "(SELECT fldValor FROM tblfuncionario_conta_fluxo WHERE fldReferencia_Id = tblpedido_funcionario_servico.fldPedido_Id AND fldValor > 0
							AND fldEstorno = '0' AND fldId NOT IN (SELECT fldEstorno FROM tblfuncionario_conta_fluxo WHERE fldFuncionario_Id = $funcionario_id AND fldEstorno >0)
							ORDER BY fldId DESC LIMIT 1)";
		}
		
		$sSQL = "SELECT 
				tblpedido.fldDesconto,
				tblpedido.fldDescontoReais,  
				tblpedido.fldValor_Terceiros, 
				tblpedido_funcionario_servico.fldComissao, 
				tblfuncionario.fldId as fldFuncionario_Id,
				tblfuncionario.fldNome,	
				tblfuncionario.fldCPF_CNPJ,
				tblpedido.fldReferencia,
				tblpedido_funcionario_servico.fldPedido_Id,
				tblpedido_funcionario_servico.fldFuncao_Tipo,
				REPLACE(FORMAT(SUM(((tblpedido_funcionario_servico.fldComissao / 100) * tblpedido_funcionario_servico.fldValor) + tblpedido_funcionario_servico.fldComissao_Reais),2), ',' ,'') as fldComissaoTotal,
				IFNULL((SELECT REPLACE(FORMAT(SUM(tblpedido_funcionario_servico.fldValor),2), ',' ,'') FROM tblpedido_funcionario_servico
						WHERE fldFuncionario_Id = $funcionario_id AND fldFuncao_Tipo = 2 AND fldPedido_Id = tblpedido.fldId ) ,0) AS fldTotalServico,
				IFNULL((SELECT REPLACE(FORMAT(SUM((fldValor - ((fldDesconto / 100) * fldValor)) * fldQuantidade),2), ',' ,'') FROM tblpedido_item 
					WHERE fldExcluido = 0 AND fldPedido_Id = tblpedido.fldId GROUP BY fldPedido_Id) ,0) as fldTotalItem,
				(SELECT	MAX(fldData_Cadastro) FROM tblfuncionario_conta_fluxo WHERE fldReferencia_Id = tblpedido_funcionario_servico.fldPedido_Id) as fldDataPago,
				
				$parametro as fldBaixaValor
		  
			FROM tblpedido_funcionario_servico 
				
				INNER JOIN tblpedido 		ON tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId
				INNER JOIN tblfuncionario 	ON tblpedido_funcionario_servico.fldFuncionario_Id = tblfuncionario.fldId
				
				WHERE tblpedido_funcionario_servico.fldPedido_Id = $pedido_id
				AND tblpedido_funcionario_servico.fldFuncao_Tipo = $funcao
				AND tblfuncionario.fldId = $funcionario_id
			GROUP BY tblpedido_funcionario_servico.fldPedido_Id";
			
		$rsBaixa	= mysql_query($sSQL);
		$rowBaixa 	= mysql_fetch_array($rsBaixa);
		
		$data_pago 	= ($rowBaixa['fldDataPago'])?format_date_out($rowBaixa['fldDataPago']) : '--------';
		echo mysql_error();
		
		#################################################################################################################################################################
		if($_SESSION['sel_funcionario_comissao_desconto'] > 0){
		
			$venda_total 		= $rowBaixa['fldTotalItem'] + $rowBaixa['fldTotalServico'] + $rowBaixa['fldValor_Terceiros'];	
			
			$porcentagem 		= ((100 / ($venda_total)) * $rowBaixa['fldDescontoReais']); 	
			$desconto_comissao 	= ($porcentagem / 100) * $rowBaixa['fldComissaoTotal']; 		
			
			$comissaoTotal 		= $rowBaixa['fldComissaoTotal'] - $desconto_comissao;														
			$desconto	 		= ($rowBaixa['fldDesconto'] / 100); 
			$comissaoTotal 		= ($comissaoTotal - ($desconto * $comissaoTotal));
			
		}else{
			$comissaoTotal 		= $rowBaixa['fldComissaoTotal'];
		}
		#################################################################################################################################################################
		
		$comissao 	= ($rowBaixa['fldTotalValor'] * $rowBaixa['fldComissao']) / 100;
		$funcao		= ($rowBaixa['fldFuncao_Tipo'] == 1) ? 'venda' : 'servi&ccedil;o';
?>	
        <div class="recibo_print">
        	<h1>Recibo de pagamento de funcion&aacute;rio</h1>
            <p>
            	<span><?=$rowDados['fldNome_Fantasia']?></span>&nbsp;<span><?=$rowDados['fldCPF_CNPJ']?></span>
            </p>
            <p class="data">
            	<span><?=format_date_out($data)?></span>&nbsp;<span><?=format_time_short($hora)?></span>
            </p>
            
            <p style="width:450px">Funcion&aacute;rio: <span><?=str_pad($rowBaixa['fldFuncionario_Id'], 6, "0", STR_PAD_LEFT)?>&nbsp;<?=$rowBaixa['fldNome']?></span></p>
            <p style="width:450px; height:20px"><span><?=format_cpf_out($rowBaixa['fldCPF_CNPJ'])?></span></p>
            
            <div class="dados">
            	<ul class="cabecalho">
                	<li>Pago em</li>
                	<li>Venda</li>
                	<li>Ref.</li>
                	<li>Comiss&atilde;o</li>
                	<li>Tipo</li>
                </ul>
                <ul class="dados">
                	<li><?=$data_pago?></li>
                	<li><?=str_pad($rowBaixa['fldPedido_Id'], 6, "0", STR_PAD_LEFT)?></li>
                	<li><?=str_pad($rowBaixa['fldReferencia'], 6, "0", STR_PAD_LEFT)?></li>
                	<li><?=format_number_out($comissaoTotal)?></li>
                	<li><?=$funcao?></li>
                </ul>
            </div>
            <div class="valor">
            	<p>Pago R$<span><?=format_number_out($rowBaixa['fldBaixaValor'])?></span></p>
			</div>
        </div>
<?	}
?>
    </body>
</html>