<?
    require("inc/constants.php");
	date_default_timezone_set("Brazil/East");
	//definições do Garbage Collection - GC
	ini_set('session.gc_maxlifetime', 60 * 60 * 12); //12hrs
	ini_set('session.gc_probability', 0);
	ini_set('session.gc_divisor', 100);
	
	ob_start();
	session_start();
	
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	require("inc/fnc_obrigatorio_permissao.php");
	require("inc/fnc_status.php");
	require("inc/fnc_produto_preco.php");
	require("inc/fnc_produto.php");
	require("inc/fnc_log.php");
	require_once("inc/fnc_financeiro.php");
	require_once("inc/fnc_estoque.php");
	require_once("inc/fnc_estoque_enderecamento.php");
	require_once('inc/fnc_nfe.php');
	require_once('inc/fnc_ibge.php');
	require_once('inc/fnc_identificacao.php');
	
	/* Verificando nome de usuário e senha
	 * Se o usuário possui origem do website, este bloco de código rertonará um objeto json
	 * Caso contrário, apenas redireciona
	 */
	if(isset($_POST["txt_usuario"]) && isset($_POST["txt_senha"])){
		
		$usuario 	= $_POST["txt_usuario"];
		$senha 		= $_POST["txt_senha"];
		// echo 'usuario'.$usuario;
		$remote_ip	 = getenv('REMOTE_ADDR');
		$remote_name = gethostbyaddr(gethostbyname($remote_ip));
		$_SESSION['remote_name'] 	= $remote_name;
		$_SESSION['estacao_nome'] 	= fnc_identificacao($remote_name);
		
		$rsUsuario = mysql_query("SELECT * FROM tblusuario WHERE fldUsuario = '$usuario' AND fldSenha = '$senha'");
		$rowUsuario = mysql_fetch_array($rsUsuario);
		//echo mysql_error();
		
		if(mysql_num_rows($rsUsuario)){
			
			$_SESSION['logged'] 	= "xuxublz";
			$_SESSION['usuario_id'] = $rowUsuario["fldId"];
			
			$data = date('Y-m-d');
			$hora = strftime("%H:%M:%S!");
			
			mysql_query("INSERT INTO tblusuario_acesso (fldUsuario_Id, fldData, fldHora) values 
			('" . $_SESSION['usuario_id'] . "','$data','$hora')");
			
			#carregar na sessão variáveis de sistema
			#vamos transferir as personalizações e perfis de usuário para c�,
			#para nao acessar o BD a todo momento sem necessidade

			###########################################################################
			#  TIPO DO SISTEMA
			#
			#  AUTOMOTIVO
			#  FINANCEIRA
			#  OTICA
			#  FARMACIA
			#  MWDESENVOLVIMENTO
			#
			###########################################################################
			$_SESSION["sistema_tipo"] 					= fnc_sistema('sistema_tipo');
			$_SESSION["sistema_rota_controle"] 			= fnc_sistema('modulo_rota_controle');
			$_SESSION["sistema_nfe"] 					= fnc_sistema('modulo_nfe');
			$_SESSION["sistema_bina"]					= fnc_sistema('modulo_bina');
			$_SESSION["sistema_historico_servicos"]  	= fnc_sistema('modulo_historico_servicos');
			$_SESSION["sistema_crm_acompanhamento"]  	= fnc_sistema('modulo_crm_acompanhamento_cliente');
			$_SESSION["exibir_dependente"]				= fnc_sistema('exibir_dependente');
			$_SESSION["sistema_pedido_comissao"]		= fnc_sistema('pedido_comissao');
			$_SESSION["sistema_pedido_item_repetido"]	= fnc_sistema('pedido_item_repetido_alerta');
			$_SESSION["sistema_pedido_multa_tipo"]		= fnc_sistema('pedido_multa_tipo');
			$_SESSION["sistema_pedido_juros_tipo"]		= fnc_sistema('pedido_juros_tipo');
			$_SESSION["sistema_cliente_id"]				= fnc_sistema('id_cliente');
			
			##VENDA		##############################
			$_SESSION["sistema_venda_alerta_limite_credito"] = fnc_sistema('venda_alerta_limite_credito');
			
			##RECIBO	##############################
			$_SESSION["sistema_recibo_exibir_multa"] 	= fnc_sistema('recibo_exibir_multa');
			$_SESSION["sistema_recibo_exibir_juros"] 	= fnc_sistema('recibo_exibir_juros');
			$_SESSION["sistema_recibo_exibir_devedor"]	= fnc_sistema('recibo_exibir_devedor');
			$_SESSION["sistema_recibo_exibir_assinatura_funcionario"] = fnc_sistema('recibo_exibir_assinatura_funcionario');
			
			##OP #####################################
			$_SESSION["ordem_producao"]	= fnc_sistema('ordem_producao');
			
			
			
			if(isset($_POST['origem']) and $_POST['origem'] == 'website') {
				header('Content-type: application/json; charset=utf-8');
				echo json_encode(array('onlineLogon' => 'autorizado'));
				exit();
			}else {
				header("Location: index.php");
			}
		}else{
			
			if(isset($_POST['origem']) and $_POST['origem'] == 'website') {
				header('Content-type: application/json; charset=utf-8');
				echo json_encode(array('onlineLogon' => 'usuarioNaoEncontrado'));
				exit();
			}else {
				header("location:index.php?error=true");
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
                
        <meta name="description" content="my ERP" />
        <meta name="author" content="Mindware" />
        <title>myERP</title>
		
        <link rel="shortcut icon" href="/favicon.ico" />
        
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_index.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_home.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_table.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_form.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_general.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_login.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_pedido.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_pedido_nfe.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_status.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_financeiro.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_calendario.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_busca.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_cotacao.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_cliente.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_cheque.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_pedido_comanda.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_bina.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_op.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_nfe.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_load.css" />
        
        
		<link rel="stylesheet" type="text/css" media="screen" href="style/style_produto.css" />
		<!--
	    <script type="text/javascript" src="js/jquery-1.4.4.js"></script>
        -->
        <script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.7.1.custom.min.js"></script>
		<script type="text/javascript" src="js/general.js" charset="ISO-8859-1"></script>
		<script type="text/javascript" src="js/jquery.maskedinput-1.2.2.js"></script>
        <script type="text/javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript" src="js/jquery.click-calendario-min.js"></script>
		<script type="text/javascript" src="js/init-click-calendario.js"></script>
        <script type="text/javascript" src="js/init-validate.js"></script>
		<script type="text/javascript" src="js/janela-modal.js"></script>
		<script type="text/javascript" src="js/comissao.js" charset="ISO-8859-1"></script>
		<script type="text/javascript" src="js/pedido_nfe_calculo.js"></script>
        <script type="text/javascript" src="js/myerp.js" charset="UTF-8"></script>
        <script type="text/javascript" src="js/financeiro.js"></script>
        <script type="text/javascript" src="js/fnc_bina.js"></script>
        <script type="text/javascript" src="js/fnc_cliente.js"></script>
		<script type="text/javascript" src="js/parcelamento.js"></script>
		<script type="text/javascript" src="js/crm.js"></script>
        <script type="text/javascript" src="js/pedido.js" charset="ISO-8859-1"></script>
		<script type="text/javascript" src="js/ordem_producao.js" charset="ISO-8859-1"></script>
		<script type="text/javascript" src="js/usuario_permissao.js"></script>
    	<!---->
        
        
<?		
		if(isset($_GET['p'])){
			switch($_GET['p']){
				case "pedido":
					if($_SESSION["sistema_nfe"] > 0){
?>						<script type="text/javascript" src="js/fnc_nfe_consulta.js"	charset="ISO-8859-1"></script>
<?					}
				case "pedido_novo":
				case "pedido_detalhe":
					if($_GET['modo'] == "rapido_novo" || $_GET['modo'] == "rapido_detalhe"){
?>						<script type="text/javascript" src="js/pedido_rapido.js" charset="ISO-8859-1"></script>
<?					}
?>        			<script type="text/javascript" src="js/pedido_otica.js" charset="ISO-8859-1"></script>
<?				break;
			
				case 'pedido_nfe_novo':
				case 'pedido_nfe_detalhe':
				case 'pedido_nfe_importar':
?>					<script type="text/javascript" src="js/pedido_nfe.js" 		charset="ISO-8859-1"></script>
					<script type="text/javascript" src="js/produto_fiscal.js" 	charset="ISO-8859-1"></script>
                    <script type="text/javascript" src="js/pedido_otica.js" 	charset="ISO-8859-1"></script>
<?				break;

				case 'configuracao':
?>        			<script type="text/javascript" src="js/configuracao.js" 	charset="ISO-8859-1"></script>
<?				break;

				case "cliente_detalhe":
?>					<link rel="stylesheet" type="text/css" media="screen" href="style/style_arquivos_listar.css" />
	   				<link rel="stylesheet" type="text/css" media="screen" href="style/jquery.lightbox-0.5.css" 	 />
                    <script type="text/javascript" src="js/jquery.lightbox-0.5.pack.js"></script>
                    <script type="text/javascript" src="js/cliente_parcela.js"></script>
					<script type="text/javascript" src="js/fnc_parcela_credito.js"></script>
					<script type="text/javascript">
                   		$(function() {
                        	$('a[rel*=lightbox]').lightBox();
						});
                    </script>
<?				break;

				case "estoque":
				case "produto":
				case "produto_novo":
				case "produto_detalhe":
					require_once("inc/fnc_produto_codigo.php");
?>					<script type="text/javascript" src="js/produto.js" charset="ISO-8859-1"></script>
					<script type="text/javascript" src="js/estoque.js" charset="UTF-8"></script>
					<script type="text/javascript" src="js/produto_fiscal.js"></script>
<?				break;
				
				case "compra":
					if($_SESSION["sistema_nfe"] > 0){
?>						<script type="text/javascript" src="js/fnc_nfe_consulta.js"	charset="ISO-8859-1"></script>
<?					}
				case "compra_novo":
				case "compra_detalhe":
				case "compra_cotacao_novo":
				case "compra_cotacao_detalhe":				
?>					<script type="text/javascript" src="js/compra.js"></script>
<?				break;
				
				case "fornecedor_detalhe":				
?>					<script type="text/javascript" src="js/fornecedor_parcela.js"></script>
					<script type="text/javascript" src="js/fnc_parcela_credito.js"></script>
<?				break;
			}
		}
?>

</head>
<body style="overflow-y: scroll">

    <script language="JavaScript">
        document.write('<div id="loader" ><p>carregando...</p></div>');
        window.onload=function() {
            document.getElementById("loader").style.display="none";
        }
    </script>
<?


	/*	PRIMEIRO DE TUDO VAI COMPARAR A DATA COM O ULTIMO ACESSO!!  */
	$rsAcesso = mysql_query("SELECT fldData FROM tblusuario_acesso ORDER BY fldData DESC LIMIT 1");
	if(mysql_num_rows($rsAcesso)){
		$rowAcesso		= mysql_fetch_array($rsAcesso);
		$ultimoAcesso 	= $rowAcesso['fldData'];
		if(date("Y-m-d") < $ultimoAcesso){ ?>
			<script type="text/javascript">
                $(document).ready(function(){
                    anchor = $('.modal');
                    winModal(anchor);	
                });
            </script>
            <a class="modal" href="sistema_data" rel="450-350">&nbsp;</a>
            
<?			mysql_query("INSERT INTO tblsistema_log (fldTipo_Id) VALUES (1)");
			echo mysql_error();
		}
	}
	
	#ENTAO VAI COMPARAR A TRAVA!!
	# - verifico se nao existe o arquivo txt com o id do cliente, que é gerado no momento da baixa, com a nova data de destrava.
	# - pego a data que esta criptografada pela função do PHP base64_encode, decodifico e comparo se for menor que data atual, ou seja se já passou
	# - se data de bloqueio ja passou, abre o modal para digitar a nova senha e nao exibe o restante do sistema
	
	if(!isset($_SESSION["logged"])){
		if(fnc_sistema('sistema_trava') == '1'){
			$clienteId = fnc_sistema('id_cliente');
			$txt = "http://www.mwdesenvolvimento.com.br/myerp/mw_clientes/$clienteId.txt";
			
			if(url_exists($txt)){
				$fp = fopen($txt, "r");
				$dataFTP = fread($fp, 20);
				
				$dataBloqueio = fnc_sistema('data_bloqueio');
				$dataBloqueio = base64_decode($dataBloqueio);
				
				//checando se o valor que está no banco de dados possui uma "assinatura" de data, que nesse caso seria aaaa-mm-dd
				//$validarDataBloqueio = explode('-', $dataBloqueio);
				//if(is_array($validarDataBloqueio) and count($validarDataBloqueio) == 3) {
					//checando se o valor do arquivo de texto no servidor contém uma data válida
					$validarData = explode('-', $dataFTP);
					if(checkdate($validarData[1], $validarData[2], $validarData[0])) {
						
						if($dataBloqueio < $dataFTP) {
							$data_update = base64_encode($dataFTP);
							mysql_query("UPDATE tblsistema SET fldValor = '$data_update' WHERE fldParametro = 'data_bloqueio'") or die(mysql_error());
						}
						
					}
				//}
			}
			
			//data de bloqueio = vencimento + 5
			$dataBloqueio 	= fnc_sistema('data_bloqueio');
			$dataBloqueio 	= base64_decode($dataBloqueio);
			$dataAtual 		= date("Y-m-d");
			
			if($dataAtual > $dataBloqueio){
?>
		<script type="text/javascript">
                $(document).ready(function(){
                    anchor = $('.modal');
                    winModal(anchor);	
                });
                </script>
                <a class="modal" href="sistema_trava" rel="450-326">&nbsp;</a>
<?				die();
			}
		}//se tiver trava
	}
	if(isset($_GET["logoff"])){
		$_SESSION["logged"] = "";
		session_destroy();
	}
	
	if(!isset($_SESSION["logged"]) or $_SESSION["logged"] != "xuxublz"){		
?>		<div id="login">
            <form id="frm_login" name="frm_login" action="index.php" method="post">
                <label for="txt_usuario"><span> usu&aacute;rio</span>
                    <input type="text" id="txt_usuario" name="txt_usuario" />
                </label>
                <label for="txt_senha"><span> senha </span>
                    <input type="password" id="txt_senha" name="txt_senha" />
                </label>
                <input type="hidden" id="logar" name="logar" value="true" />
                <input type="submit" name="btn_login" id="btn_login" value="ok" />
<?              if(isset($_GET['error']) && !empty($_GET['error'])){
?>					<span class="alert">* usu&aacute;rio ou senha inv&aacute;lido</span>
<?				}
?>			</form>
		</div>
<?	}
	else{
			
		$rowUsuario = mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId =".$_SESSION['usuario_id']));
		$rsLogin 	= mysql_query("SELECT * FROM tblusuario_acesso WHERE fldUsuario_Id = '" . $_SESSION['usuario_id'] . "' ORDER BY fldId desc limit 1,1");
		$rowLogin 	= mysql_fetch_array($rsLogin);
		$rows 		= mysql_num_rows($rsLogin);
		
		$usuario_sessao = $_SESSION['usuario_id'];
		
?>		<div id="header">
            <div id="header_menu">
                <img src="image/layout/logo_myerp.jpg" alt="Logo Myerp" />
                <div id="identificacao">
                    <p><?=$_SESSION['estacao_nome']?></p>
                </div>
                
                <ul class="menu_small">
<?					$rsTela = mysql_query('SELECT * FROM tblsistema_tela WHERE fldAntecessor_Id = 0 AND fldAtivo = "1" ORDER BY fldOrdem LIMIT 15');
					while($rowTela = mysql_fetch_array($rsTela)){	
?>	
						<li>
							<a <?= (checkUsuario($usuario_sessao, $rowTela['fldId'])) ? "class='".$rowTela['fldClass']."' href='index.php?p=".$rowTela['fldArquivo']."'" : "class='".$rowTela['fldClass']."_disable'" ?> title="<?=$rowTela['fldTela']?>" ></a>
						</li>
<?					}
?>				</ul>
                <ul id="menu_general">
                    <li><a class="link_home" href="index.php">in&iacute;cio</a></li>
                    <li><a class="link_close" href="index.php?logoff=true">sair</a></li>
                </ul>
            </div>
        </div>
         
        <div id="container">
 
<?       	
			if(isset($_GET['p'])){
				require($_GET['p'].".php");
			}else{
				require("home.php");
			}
			/*
			if(isset($_GET['p'])){
				$arquivo  = $_GET['p'];
				$modo 	  = (isset($_GET['modo'])) ? $_GET['modo'] : '';
				$arquivo .= (!empty($modo)) ? '_'.$modo : '';
				if(fnc_check_permissao($usuario_sessao, $_GET['p'])){
					require($_GET['p'].".php");
				}else{
					echo '<div class="permissao_alert">Usu&aacute;rio sem permiss&atilde;o para acessar este m&oacute;dulo</div>';
				}
			}else{
				require("home.php");
			}
			
			*/
?>		</div>
<?	}
	
	if($_SESSION["sistema_bina"] > 0 && $_SESSION["logged"] == 'xuxublz'){
?>	
        <div id="bina_alert">
        </div>
        
        <script type="text/javascript">
			setInterval(fnc_bina, 1000);      
            setInterval(fnc_bina_alert, 500);
        </script>
<?	}
?>

	<form id="sistema_variaveis">
<?		$rsSistema = mysql_query("select * from tblsistema order by fldParametro");
		while($rowSistema = mysql_fetch_array($rsSistema)){
?>
			<input type="hidden" name="sys_<?=$rowSistema['fldParametro']?>" id="sys_<?=$rowSistema['fldParametro']?>" value="<?=$rowSistema['fldValor']?>">
<?
		}
?>
	</form>
</div>


    <script type="text/javascript">
        usuario_permissao_novo();
        usuario_permissao_editar();			
    </script>
    
    
    <div class="nfe_consulta_sefaz" >
    	<div>
            <p>NF-e em transmiss&atilde;o.</p>
            <p>Aguardando retorno da SEFAZ.</p>
		</div>
    </div>	
</html>

