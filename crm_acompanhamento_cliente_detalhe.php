<?
	//verificando se a requis�o � ajax - Utilizado quando � inserido/atualizado algum agendamento durante a edi��o do chamado
	//� reenviado as informa��es do formul�rio relacionado ao chamado
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		require_once('inc/con_db.php');
		require_once('inc/fnc_general.php');
	}
	
?>

<div id="voltar">
    <p><a href="index.php?p=crm_acompanhamento_cliente">n&iacute;vel acima</a></p>
</div>	

<h2>Editar Chamado</h2>
<div id="principal">
<?	
	//A��es
	//Excluir um agendamento
	if(isset($_GET['excluir'])) {
		
		$sql = "UPDATE tblcrm_acompanhamento_agenda SET fldExcluido = 1 WHERE fldId = " . mysql_real_escape_string($_GET['excluir']);
		if(mysql_query($sql)) {
			
			$sql = "SELECT fldId FROM tblcrm_acompanhamento_agenda WHERE fldChamado_Id = " . mysql_real_escape_string($_GET['id']) . " AND fldExcluido = 0";
			
			if(mysql_num_rows(mysql_query($sql)) < 1) {
				
				//caso n�o haja mais nenhum agendamento para este chamado, altera a situa��o do mesmo para "Em espera" (id = 1)
				$sql = "UPDATE tblcrm_acompanhamento_cliente_chamado SET fldSituacao_Id = 1 WHERE fldId = " . mysql_real_escape_string($_GET['id']);
				mysql_query($sql);
				
			}
			
		}
		
	}
	
	if($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		//Atualizar os dados do chamado
		if(isset($_POST['hid_chamado_id'])) {
			
			$chamadoId 					   = $_POST['hid_chamado_id'];
			$clienteId 					   = $_POST['hid_cliente_id'];
			$dataChamado 				   = format_date_in($_POST['txt_chamado_data']);
			$horaChamado 				   = $_POST['txt_chamado_hora'];
			$usuarioId  				   = $_POST['hid_usuario_id'];
			$descricaoChamado 			   = nl2br(mysql_real_escape_string($_POST['txt_chamado_descricao']));
			$situacaoChamado 			   = $_POST['sel_chamado_situacao'];
			$prioridadeChamado 			   = $_POST['sel_chamado_prioridade'];
			
			$sql  = "UPDATE tblcrm_acompanhamento_cliente_chamado SET
					 fldCliente_Id = '{$clienteId}',
					 fldSituacao_Id = '{$situacaoChamado}',
					 fldPrioridade_Id = '{$prioridadeChamado}',
					 fldUsuario_Id = '{$usuarioId}',
					 fldDescricao = '{$descricaoChamado}',
					 fldData = '{$dataChamado}',
					 fldHora = '{$horaChamado}'
					 WHERE fldId = {$chamadoId}";
			
			mysql_query($sql);
			$redirecionarPara = 'crm_acompanhamento_cliente&mensagem=ok';
			
		}
		
		//Atualizar os dados do agendamento relativo ao chamado
		if(isset($_POST['hid_agendamento_id'])) {
			
			$chamadoId 					   = $_POST['hid_chamado_referencia_id'];
			$agendamentoId				   = $_POST['hid_agendamento_id'];
			$usuarioId					   = $_POST['hid_usuario_id'];
			$dataChamadoAgendamento 	   = (empty($_POST['txt_chamado_agendamento_data'])) ? 'NULL' : "'" . format_date_in($_POST['txt_chamado_agendamento_data']) . "'";
			$horaChamadoAgendamento 	   = (empty($_POST['txt_chamado_agendamento_hora'])) ? "NULL" : "'" . $_POST['txt_chamado_agendamento_hora'] . "'";
			$tipoChamadoAgendamento 	   = ($_POST['sel_chamado_agendamento_tipo'] < 1) ? 0 : $_POST['sel_chamado_agendamento_tipo'];
			$funcionarioChamadoAgendamento = ($_POST['sel_chamado_agendamento_funcionario'] < 1) ? 0 : $_POST['sel_chamado_agendamento_funcionario'];
			$descricaoChamadoAgendamento   = nl2br(mysql_real_escape_string($_POST['txt_chamado_agendamento_desc']));
			
			$sql  = "UPDATE tblcrm_acompanhamento_agenda SET
					 fldTipo_Id = '{$tipoChamadoAgendamento}',
					 fldFuncionario_Id = '{$funcionarioChamadoAgendamento}',
					 fldDescricao = '{$descricaoChamadoAgendamento}',
					 fldData = {$dataChamadoAgendamento},
					 fldHora = {$horaChamadoAgendamento},
					 fldUsuario_id = '{$usuarioId}'
					 WHERE fldId = {$agendamentoId}";
			
			mysql_query($sql);
			$redirecionarPara = 'crm_acompanhamento_cliente_detalhe&id=' . $chamadoId;
			
		}
		
		//Insere um novo agendamento para o chamado
		if(isset($_POST['hid_agendamento_novo'])) {
			
			$chamadoId 					   = $_POST['hid_chamado_referencia_id'];
			$dataChamadoAgendamento 	   = (empty($_POST['txt_chamado_agendamento_data'])) ? null : format_date_in($_POST['txt_chamado_agendamento_data']);
			$horaChamadoAgendamento 	   = (empty($_POST['txt_chamado_agendamento_hora'])) ? "NULL" : "'" . $_POST['txt_chamado_agendamento_hora'] . "'";
			$tipoChamadoAgendamento 	   = ($_POST['sel_chamado_agendamento_tipo'] < 1) ? 0 : $_POST['sel_chamado_agendamento_tipo'];
			$funcionarioChamadoAgendamento = ($_POST['sel_chamado_agendamento_funcionario'] < 1) ? 0 : $_POST['sel_chamado_agendamento_funcionario'];
			$descricaoChamadoAgendamento   = nl2br(mysql_real_escape_string($_POST['txt_chamado_agendamento_desc']));
			$usuarioId					   = $_POST['hid_usuario_id'];
			
			$sql = "INSERT INTO tblcrm_acompanhamento_agenda (fldChamado_Id, fldTipo_Id, fldFuncionario_Id, fldDescricao, fldData, fldHora, fldUsuario_Id)
					VALUES(
					'{$chamadoId}',
					'{$tipoChamadoAgendamento}',
					'{$funcionarioChamadoAgendamento}',
					'{$descricaoChamadoAgendamento}',
					'{$dataChamadoAgendamento}',
					{$horaChamadoAgendamento},
					'{$usuarioId}'
					)";
			
			mysql_query($sql);
			$redirecionarPara = 'crm_acompanhamento_cliente_detalhe&id=' . $chamadoId;
			
		}
		
		if(!mysql_error()){
			header("location:index.php?p=$redirecionarPara");
		}
		else {
?>			<div class="alert">
				<p class="erro">
					N&atilde;o foi poss&iacute;vel gravar os dados!
					<a class="voltar link" href="index.php?p=crm_acompanhamento_cliente_detalhe&amp;id=<?=$chamadoId?>">Voltar</a>
				</p>
			</div>
<?
			die(mysql_error());
		}
	}
	else {
		//exibir os dados do chamado na tela
		if(isset($_GET['id'])) {
			
			$sql = "SELECT tblcrm_acompanhamento_cliente_chamado.*,
					tblcliente.fldNome, tblusuario.fldUsuario
					FROM tblcrm_acompanhamento_cliente_chamado
					
					INNER JOIN tblcliente ON tblcliente.fldId = tblcrm_acompanhamento_cliente_chamado.fldCliente_Id
					INNER JOIN tblusuario ON tblusuario.fldId = tblcrm_acompanhamento_cliente_chamado.fldUsuario_Id
					
					WHERE tblcrm_acompanhamento_cliente_chamado.fldId = " . mysql_real_escape_string($_GET['id']);
			$rowDadosAcompanhamento = mysql_fetch_array(mysql_query($sql));
			echo mysql_error();
			
		}
?>		
        <div class="form">
            <form class="frm_detalhe" style="width:890px" id="frm_acompanhamento_chamado_detalhe" action="" method="post">
                <ul>
                    <li>
                        <label for="txt_chamado_id">Ticket #</label>
                        <input type="text" style="width:70px; text-align:right" id="txt_chamado_id" name="txt_chamado_id" disabled="disabled" value="<?=str_pad($rowDadosAcompanhamento['fldId'], 6, '0', STR_PAD_LEFT);?>" />
						<input type="hidden" id="hid_chamado_id" name="hid_chamado_id" value="<?=$rowDadosAcompanhamento['fldId'];?>" />
                    </li>
					
					<li>
                        <label for="txt_cliente_codigo">Cliente</label>
                        <input type="text" style="width:70px; text-align:right" id="txt_cliente_codigo" name="txt_cliente_codigo" value="<?=$rowDadosAcompanhamento['fldCliente_Id'];?>" />
                        <a href="cliente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
                    
					<li>
                        <label for="txt_cliente_nome">&nbsp;</label>
                        <input type="text" 		id="txt_cliente_nome" 	name="txt_cliente_nome" value="<?=$rowDadosAcompanhamento['fldNome'];?>" style=" width:315px" readonly="readonly" />
                        <input type="hidden" 	id="hid_cliente_id" 	name="hid_cliente_id"	value="<?=$rowDadosAcompanhamento['fldCliente_Id'];?>" />
                    </li>
					
					<li>
                        <label for="txt_chamado_data">Data</label>
                        <input type="text" style="width: 80px; text-align: center" class="calendario-mask" id="txt_chamado_data" name="txt_chamado_data" value="<?=format_date_out($rowDadosAcompanhamento['fldData']);?>" />
                        <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                    </li>
                    
					<li>
                        <label for="txt_chamado_hora">Hora</label>
                        <input type="text" style="width: 80px; text-align: center" id="txt_chamado_hora" name="txt_chamado_hora" value="<?=$rowDadosAcompanhamento['fldHora'];?>" />
                    </li>
					
					<li>
                        <label for="txt_usuario">Autor</label>
                        <input type="text" style="width:146px" id="txt_usuario" name="txt_usuario" value="<?=$rowDadosAcompanhamento['fldUsuario'];?>" readonly="readonly" />
						<input type="hidden" id="hid_usuario_id" name="hid_usuario_id" value="<?=$rowDadosAcompanhamento['fldUsuario_Id'];?>" />
                    </li>
				</ul>
				
				<ul>
					<li>
						<label for="txt_chamado_descricao">Descri&ccedil;&atilde;o</label>
						<textarea style="width: 870px; height: 100px; padding: 5px;" id="txt_chamado_descricao" name="txt_chamado_descricao"><?= strip_tags($rowDadosAcompanhamento['fldDescricao']); ?></textarea>
					</li>
				</ul>
<?
				//dados de agendamento
				$sql = "SELECT tblcrm_acompanhamento_agenda.*, tblcrm_acompanhamento_cliente_tipo.fldTipo_Nome, tblfuncionario.fldNome, tblusuario.fldUsuario FROM tblcrm_acompanhamento_agenda
						LEFT JOIN tblcrm_acompanhamento_cliente_tipo ON tblcrm_acompanhamento_cliente_tipo.fldId = tblcrm_acompanhamento_agenda.fldTipo_Id AND tblcrm_acompanhamento_cliente_tipo.fldExcluido = 0
						INNER JOIN tblfuncionario ON tblfuncionario.fldId = tblcrm_acompanhamento_agenda.fldFuncionario_Id
						INNER JOIN tblusuario ON tblusuario.fldId = tblcrm_acompanhamento_agenda.fldUsuario_Id
						WHERE tblcrm_acompanhamento_agenda.fldExcluido = 0 AND tblcrm_acompanhamento_agenda.fldChamado_Id = " . mysql_real_escape_string($_GET['id']) .
						" ORDER BY fldData DESC, fldHora DESC";
						
				$rsAgenda = mysql_query($sql);
				echo mysql_error();
?>				
				<div id="table" style="width: 880px;">
					<div id="table_cabecalho" style="width: 880px;">
						<ul class="table_cabecalho" style="width: 880px;">
							<li style="width: 110px; text-align: center;">Agendamento</li>
							<li style="width: 120px; text-align: center;">Tipo</li>
							<li style="width: 292px; text-align: center;">Descri&ccedil;&atilde;o</li>
							<li style="width: 167px; text-align: center;">Funcion&aacute;rio</li>
							<li style="width: 118px; text-align: center;">Usu&aacute;rio</li>
							<li></li>
							<li></li>
						</ul>
					</div>
					
					<div id="table_container" style="width: 880px; height: 100px;">       
						<table id="table_general" class="table_general" summary="Lista de agendamentos com clientes">
							<tbody>
<?							$linha = "row";
							while($rowAgenda = mysql_fetch_array($rsAgenda)) {
?>								<tr class="<?= $linha; ?>" style="width: 100%;">
									<td style="width: 112px; text-align: center;">
										<?=format_date_out($rowAgenda['fldData']);?>
										<?=(!empty($rowAgenda['fldHora'])) ? ' - ' . date('H:i', strtotime($rowAgenda['fldHora'])) : '';?>
									</td>
									<td style="width: 122px; text-indent: 3px;"><?=$rowAgenda['fldTipo_Nome']?></td>
									<td style="width: 300px; text-indent: 3px;" title="<?=htmlentities($rowAgenda['fldDescricao'])?>"><?=limitarTexto(($rowAgenda['fldDescricao']), 100)?></td>
									<td style="width: 170px; text-indent: 3px;"><?=$rowAgenda['fldNome']?></td>
									<td style="width: 120px; text-align: center;"><?=$rowAgenda['fldUsuario']?></td>
									<td style="width: 15px;"><a class="edit modal" rel="730-300" href="crm_acompanhamento_agenda,<?=$_GET['id'];?>,<?=$rowAgenda['fldId']?>" title="Visualizar/Editar" style="margin-top: 2px;"></a></td>
									<td style="width: 15px;"><a class="delete" title="Excluir" href="?p=crm_acompanhamento_cliente_detalhe&id=<?=$_GET['id'];?>&excluir=<?=$rowAgenda['fldId']?>" onclick="return confirm('Deseja excluir o agendamento?')"></a></td>
								</tr>
<?  				        	$linha = ($linha == "row") ? "dif-row" : "row";
							}
?>			 				</tbody>
						</table>
					</div>
				</div>
				
				<a style="float: right; margin: 5px 3px 0 0;" class="modal btn_novo" rel="730-300" href="crm_acompanhamento_agenda,<?=$_GET['id'];?>">novo agendamento</a>
				
				<ul style="width: 100%; margin: 10px 0 0 0;">
					<li>
						<label for="sel_chamado_situacao">Situa&ccedil;&atilde;o do chamado</label>
						<select id="sel_chamado_situacao" name="sel_chamado_situacao">
<?						$rsChamadoSituacao = mysql_query("SELECT * FROM tblcrm_acompanhamento_cliente_situacao");							
						while($rowChamadoSituacao = mysql_fetch_array($rsChamadoSituacao)) {
?>
							<option value="<?=$rowChamadoSituacao['fldId']?>"<?= ($rowChamadoSituacao['fldId'] == $rowDadosAcompanhamento['fldSituacao_Id']) ? ' selected="selected"' : ''; ?>><?=$rowChamadoSituacao['fldSituacao_Nome']?></option>
<?						}
?>						</select>
					</li>
					
					<li>
						<label for="sel_chamado_prioridade">Prioridade do chamado</label>
						<select id="sel_chamado_prioridade" name="sel_chamado_prioridade">
<?						$rsChamadoPrioridade = mysql_query("SELECT * FROM tblcrm_acompanhamento_cliente_prioridade");							
						while($rowChamadoPrioridade = mysql_fetch_array($rsChamadoPrioridade)) {
?>
							<option value="<?=$rowChamadoPrioridade['fldId']?>"<?= ($rowChamadoPrioridade['fldId'] == $rowDadosAcompanhamento['fldPrioridade_Id']) ? ' selected="selected"' : ''; ?>><?=$rowChamadoPrioridade['fldPrioridade_Nome']?></option>
<?						}
?>						</select>
					</li>
				</ul>
				
                <div style="float:right">
                    <input type="submit" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
                </div>
            </form>
		</div>
<?	}
?>        
</div>

<script type="text/javascript">
	//Ao excluir algum agendamento, enviar os dados do Chamado junto, pois se usu�rio estiver editando n�o haver� perda.
	$('#frm_acompanhamento_chamado_detalhe a.delete').click(function() {
		$.post('crm_acompanhamento_cliente_detalhe.php', $('#frm_acompanhamento_chamado_detalhe').serialize());
	});
</script>