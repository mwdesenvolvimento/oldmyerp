<?php
	$filtro = '';

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_transportador_id'] = "";
		$_SESSION['txt_transportador_nome'] = "";
		$_SESSION['txt_transportador_nome_fantasia'] = "";
		$_SESSION['txt_transportador_cnpj'] = "";
		$_POST['chk_transportador'] =  false;
		$_POST['chk_transportador_filtro'] =  false;
	}
	else{
		$_SESSION['txt_transportador_id'] = (isset($_POST['txt_transportador_id']) ? $_POST['txt_transportador_id'] : $_SESSION['txt_transportador_id']);
		$_SESSION['txt_transportador_nome'] = (isset($_POST['txt_transportador_nome']) ? $_POST['txt_transportador_nome'] : $_SESSION['txt_transportador_nome']);
		$_SESSION['txt_transportador_nome_fantasia'] = (isset($_POST['txt_transportador_nome_fantasia']) ? $_POST['txt_transportador_nome_fantasia'] : $_SESSION['txt_transportador_nome_fantasia']);
		$_SESSION['txt_transportador_cnpj'] = (isset($_POST['txt_transportador_cnpj']) ? $_POST['txt_transportador_cnpj'] : $_SESSION['txt_transportador_cnpj']);
	}

?>
<form id="frm-filtro" action="index.php?p=transportador&modo=cadastro" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
    	<li>
<?			$codigo = ($_SESSION['txt_transportador_id'] ? $_SESSION['txt_transportador_id'] : "c&oacute;digo");
			($_SESSION['txt_transportador_id'] == "código") ? $_SESSION['txt_transportador_id'] = '' : '';
?>      	<input style="width: 100px" type="text" name="txt_transportador_id" id="txt_transportador_id" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo?>"/>
		</li>
        <li>
        	<input type="checkbox" name="chk_transportador_filtro" id="chk_transportador_filtro" <?=($_POST['chk_transportador_filtro'] == true ? print 'checked ="checked"' : '')?>/>
<?			$nome = ($_SESSION['txt_transportador_nome'] ? $_SESSION['txt_transportador_nome'] : "nome");
			($_SESSION['txt_transportador_nome'] == "nome") ? $_SESSION['txt_transportador_nome'] = '' : '';
?>     		<input style="width: 165px" type="text" name="txt_transportador_nome" id="txt_transportador_nome" onfocus="limpar (this,'nome');" onblur="mostrar (this, 'nome');" value="<?=$nome?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
    	<li>
        	<input type="checkbox" name="chk_transportador" id="chk_transportador" <?=($_POST['chk_transportador'] == true ? print 'checked ="checked"' : '')?>/>
<?			$nomeF = ($_SESSION['txt_transportador_nome_fantasia'] ? $_SESSION['txt_transportador_nome_fantasia'] : "nome fantasia");
			($_SESSION['txt_transportador_nome_fantasia'] == "nome fantasia") ? $_SESSION['txt_transportador_nome_fantasia'] = '' : '';
?>     		<input style="width: 165px" type="text" name="txt_transportador_nome_fantasia" id="txt_transportador_nome_fantasia" onfocus="limpar (this,'nome fantasia');" onblur="mostrar (this, 'nome fantasia');" value="<?=$nomeF?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
        <li>
<?			$cnpj = ($_SESSION['txt_transportador_cnpj'] ? $_SESSION['txt_transportador_cnpj'] : "CPF/CNPJ");
			($_SESSION['txt_transportador_cnpj'] == "CPF/CNPJ") ? $_SESSION['txt_transportador_cnpj'] = '' : '';
?>     		<input style="width: 120px" type="text" name="txt_transportador_cnpj" id="txt_transportador_cnpj" onfocus="limpar (this,'CPF/CNPJ');" onblur="mostrar (this, 'CPF/CNPJ');" value="<?=$cnpj?>"/>
			<small style="margin-left: 40px"> *apenas n&uacute;meros</small>
		</li>
        <li>
        	<button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
        </li>
        <li>
	        <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
        </li>
    </ul>
    
  </fieldset>
</form>

<?
	/** inicializando a string sql para realizar a consulta ($filtro)
	 * assim sempre exitirá a clásula where, mesmo sendo irrelevante neste ponto
	 */
	
	$filtro = 'WHERE fldId > 0 ';

	if(($_SESSION['txt_transportador_id']) != ""){
		
		$filtro .= "and fldId = '".$_SESSION['txt_transportador_id']."'";
	}

	if(($_SESSION['txt_transportador_nome_fantasia']) != ""){
		$transportador = addslashes($_SESSION['txt_transportador_nome_fantasia']); // no caso de aspas, pra nao dar erro na consulta
		if($_POST['chk_transportador'] == true){
			$filtro .= "and fldNomeFantasia like '%".$transportador."%'";
		}else{
			$filtro .= "and fldNomeFantasia like '".$transportador."%'";
		}
	}
	
	if(($_SESSION['txt_transportador_nome']) != ""){
		$transportador = addslashes($_SESSION['txt_transportador_nome']); // no caso de aspas, pra nao dar erro na consulta
		if($_POST['chk_transportador_filtro'] == true){
			$filtro .= "and fldNome like '%".$transportador."%'";
		}else{
			$filtro .= "and fldNome like '".$transportador."%'";
		}
	}
	
	if(($_SESSION['txt_transportador_cnpj']) != ""){
		
		$filtro .= "and fldCPF_CNPJ = '".$_SESSION['txt_transportador_cnpj']."'";
	}
			

	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_transportador'] = $filtro;
	}
	elseif(isset($_POST['btn_limpar'])){
		$_SESSION['filtro_transportador'] = "";
	}

?>
