<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
      	<title>myERP - Imprimir venda</title>
        
	</head>
	<body>
<?php
		if (!isset($_SESSION['logado'])){
			session_start();
		}
	
		ob_start();
		session_start();
		
		require_once("inc/con_db.php");
		require_once("inc/fnc_general.php");
		require_once("inc/fnc_imprimir.php");
		require_once("inc/fnc_identificacao.php");
		require_once("inc/fnc_ibge.php");
	
		$impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
		if(!$impressao_local){
			$impressao_local = fnc_estacao_impressora('todos');
		}
		
		$texto 				= $impressao_local."\r\n";
		$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
		$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
		$usuario_sessao 	= $_SESSION['usuario_id'];
		$pedido_id  		= $_GET['id'];
		$raiz 				= $_GET['raiz'];
		$data 				= date("Y-m-d");
		
		$rsPedido  			= mysql_query("SELECT SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) as fldTotalItem,
								(SELECT SUM(fldValor) FROM tblpedido_funcionario_servico WHERE fldFuncao_Tipo = 2 AND fldPedido_Id = tblpedido.fldId) as fldTotalServico,
								tblpedido.fldServico, tblpedido.fldVeiculo_Id, tblpedido.fldDesconto, tblpedido.fldValor_Terceiros, tblpedido.fldPedidoData, tblpedido.fldVeiculo_Km,
								tblpedido.fldId as fldPedidoId, tblpedido.fldDescontoReais, tblcliente.*
								FROM tblpedido 
								LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
								INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								WHERE tblpedido.fldId = $pedido_id GROUP BY tblpedido_item.fldPedido_Id");
		$rowPedido 			= mysql_fetch_array($rsPedido);
		
		echo mysql_error();
		$rsEmpresa  	= mysql_query("SELECT * FROM tblempresa_info");
		$rowEmpresa 	= mysql_fetch_array($rsEmpresa);
		$pedido_id 		= $rowPedido['fldPedidoId'];
	
		# CABECALHO TXT ###############################################################################################################################################
		$cabecalho 		.= "<b>";
		$nomeEmpresa 	 = acentoRemover(strtoupper($rowEmpresa['fldNome_Fantasia']));
		$cabecalho 		.= format_margem_print('---- ****  '.$nomeEmpresa.'  **** ----',80,'centro')."\r\n";
		$cabecalho 		.= "</b>";
		$cabecalhoLinha += 1;
		
		$enderecoEmpresa = substr(acentoRemover($rowEmpresa['fldEndereco']),0,41);
		$cabecalho 		.= format_margem_print(strtoupper($enderecoEmpresa).' '.$rowEmpresa['fldNumero'],46,'esquerda');
		$cabecalho 		.= format_margem_print($rowEmpresa['fldTelefone1'],17,'esquerda');
		$cabecalho 		.= format_margem_print($rowEmpresa['fldTelefone2'],17,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		$cabecalho 		.= format_margem_print("VENDA:    <b>".str_pad($rowPedido['fldPedidoId'], 5, "0", STR_PAD_LEFT)."</b>",30,'esquerda');
		$cabecalho 		.= format_margem_print("DATA: ".format_date_out(date("Y-m-d")),25,'esquerda');
		$cabecalho 		.= format_margem_print("<b>DATA DA VENDA: ".format_date_out($rowPedido['fldPedidoData'])."</b>",25,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		$nome			 = substr($rowPedido['fldNome'],0,63);
		$cabecalho		.= format_margem_print("CLIENTE:  <b>".str_pad($rowPedido['fldCodigo'], 6, "0", STR_PAD_LEFT).' '.acentoRemover(strtoupper($nome))."</b>",80,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		$endereco 		 = substr(acentoRemover($rowPedido['fldEndereco']),0,40);
		$bairro 		 = substr(acentoRemover($rowPedido['fldBairro']),0,20);
		$CPFCNPJ 		 = formatCPFCNPJTipo_out($rowPedido['fldCPF_CNPJ'], $rowPedido['fldTipo']);
		$cabecalho		.= format_margem_print("CPF/CNPJ: ".$CPFCNPJ,30,'esquerda');
		$cabecalho 		.= format_margem_print(strtoupper($endereco).' '.$rowPedido['fldNumero'],50,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		$municipio 		 = fnc_ibge_municipio($rowPedido['fldMunicipio_Codigo']);
		$cabecalho 		.= format_margem_print("RG/IE: ".$rowPedido['fldRG_IE'],30,'esquerda');
		$cabecalho 		.= format_margem_print(strtoupper($bairro).' ',25,'esquerda');
		$cabecalho 		.= format_margem_print(strtoupper($municipio),25,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
			
		$cabecalho 		.= format_margem_print("TELEFONE: ".$rowPedido['fldTelefone1'],30,'esquerda');
		$cabecalho 		.= format_margem_print($rowPedido['fldTelefone1'],25,'esquerda');
		$cabecalho 		.= format_margem_print($rowPedido['fldCEP'],20,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		if($_SESSION['sistema_tipo'] == 'automotivo'){
			if(isset($rowPedido['fldVeiculo_Id'])){
				$rsVeiculo 	= mysql_query("SELECT fldVeiculo, fldPlaca FROM tblcliente_veiculo WHERE fldId = ".$rowPedido['fldVeiculo_Id']);
				$rowVeiculo = mysql_fetch_array($rsVeiculo);
				
				$cabecalho 		.= format_margem_print("PLACA: ".$rowVeiculo['fldPlaca']." ",30,'esquerda');
				$cabecalho 		.= format_margem_print("VEICULO: ".$rowVeiculo['fldVeiculo'],25,'esquerda');
				$cabecalho 		.= format_margem_print("KM: ".$rowPedido['fldVeiculo_Km'],25,'esquerda')."\r\n";
				$cabecalhoLinha += 1;
			}
		}
		
		#JA FACO A CONSULTA DOS ITENS AQUI, PRA SABER SE DEVO CHAMAR O ARQUIVO DE PRODUTOS
		$rsItem 		= mysql_query("SELECT tblpedido_item.*, tblproduto.fldCodigo 
						  FROM tblpedido_item INNER JOIN tblproduto ON tblpedido_item.fldProduto_Id = tblproduto.fldId
						  WHERE tblpedido_item.fldPedido_Id = ".$rowPedido['fldPedidoId']);
		$totalItens 	= mysql_num_rows($rsItem);
		if($totalItens > 0) {
			$texto .= $cabecalho;
			require("pedido_imprimir_matricial_servico_via1.php");
			$texto .= "\r\n\r\n\r\n";
		}
		$texto .= $cabecalho;
		require("pedido_imprimir_matricial_servico_via2.php");
		
	 	############################################################################################################################################################
		$timestamp  = date("Ymd_His");
		$local_file = "impressao///inbox///imprimir_pedido_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		
		$fp 		= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
		$salva		= fwrite($fp, $texto);
		$texto 		= fread($fp, filesize($local_file));
		
		//transformamos as quebras de linha em etiquetas <br>
		$texto = nl2br($texto);
		print $texto;

		unset($total_item);
		if(fnc_sistema("pedido_imprimir_np") > 0 || $_GET['NP'] > 0){
			require('pedido_imprimir_np_matricial.php');
		}
		fclose($fp);
?> 
	<script type="text/javascript">
		//window.location="<?=$local_file?>";
		window.close();
	</script>

</html>	
