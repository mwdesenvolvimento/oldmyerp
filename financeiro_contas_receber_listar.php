<?
	require("financeiro_contas_receber_filtro.php");
	require("financeiro_contas_receber_action.php");
	
	if(isset($_POST["btn_imprimir"])){	
?>		<script language="javascript">
			window.open("financeiro_contas_receber_relatorio.php");
		</script>
<?	}

	if(isset($_GET['msg']) && $_GET['msg'] == 'carteira_atualizada' && $_SERVER['REQUEST_METHOD'] != "POST"){
?>
        <div class="alert">
            <p class="ok">Registro gravado com sucesso</p>
        </div>
<?
	}else if(isset($_GET['msg']) && $_GET['msg'] == 'erro' && $_SERVER['REQUEST_METHOD'] != "POST"){
?>
        <div class="alert">
            <p class="alert">Erro ao atualizar os registros</p>
        </div>	
<?	
	}

/**************************** ORDER BY *******************************************/
	$filtroOrder 	= 'tblpedido_parcela.fldVencimento';
	$class 		  	= 'asc';
	$order_sessao 	= explode(" ", $_SESSION['order_contas_receber']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "tblcliente.fldCodigo";   							break;
			case 'cliente'		:  $filtroOrder = "tblcliente.fldNome"; 								break;
			case 'funcionario'	:  $filtroOrder = "tblpedido_funcionario_servico.fldFuncionario_Id";	break;
			case 'venda'		:  $filtroOrder = "tblpedido.fldId";									break;
			case 'parcela'		:  $filtroOrder = "tblpedido_parcela.fldParcela";						break;
			case 'vencimento'	:  $filtroOrder = "tblpedido_parcela.fldVencimento";					break;
			case 'venda'		:  $filtroOrder = "tblpedido.fldId";									break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_contas_receber'] = (!$_SESSION['order_contas_receber'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_contas_receber'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=financeiro_contas_receber$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_contas_receber']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class


/**************************** BUSCA NO BANCO DE DADOS ***********************************/
				
	$sSQL = "SELECT tblcliente.fldId as ClienteId,
					tblcliente.fldCodigo as ClienteCodigo, 
					tblcliente.fldNome as ClienteNome, 
					tblpedido_funcionario_servico.fldFuncionario_Id, 
					tblpagamento_tipo.fldTipo as TipoPagamento, 
					tblpedido.fldObservacao as fldPedidoObs,
					tblpedido.fldTipo_Id,
					COUNT(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) AS QtdBaixas,
					SUM((tblpedido_parcela_baixa.fldValor + tblpedido_parcela_baixa.fldDesconto) * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) AS ValorBaixa, 
					tblpedido_parcela.*
					
					FROM tblpedido_parcela
					
					INNER JOIN tblpedido ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId
					INNER JOIN tblcliente ON tblcliente.fldId = tblpedido.fldCliente_Id
					LEFT JOIN tblpedido_funcionario_servico ON tblpedido.fldId 	 = tblpedido_funcionario_servico.fldPedido_Id 
					INNER JOIN tblpagamento_tipo ON tblpagamento_tipo.fldId = tblpedido_parcela.fldPagamento_Id
					LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
					LEFT JOIN tblpedido_fiscal ON tblpedido_parcela.fldPedido_Id = tblpedido_fiscal.fldpedido_id
					WHERE tblpedido_parcela.fldStatus = '1' 
					AND tblpedido_parcela.fldExcluido = '0' 
					" . $_SESSION['filtro_contas_receber'] ."
					AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0) 
					GROUP BY tblpedido_parcela.fldId HAVING ValorBaixa < tblpedido_parcela.fldValor OR QtdBaixas = 0 ORDER BY " . $_SESSION['order_contas_receber'];
	
	$rsPedido = mysql_query($sSQL);
	
	echo mysql_error();
	$_SESSION['contas_receber_relatorio'] = $sSQL;
	
	//vars
	$totalReceber 	= 0.00;
	$data_atual 	= date("Y-m-d");
#########################################################################################
?>
    <form class="table_form" action="" method="post" name="form" style="clear: both;">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
					
                    <li class="order" style="width:75px;">
                    	<a <?= ($filtroOrder == 'tblpedido_parcela.fldVencimento') 					? "class='$class'" : '' ?> style="width:75px" href="<?=$raiz?>vencimento">Vencimento</a>
                    </li>                
                	<li class="order" style="width:50px">
                    	<a <?= ($filtroOrder == 'tblcliente.fldCodigo') 							? "class='$class'" : '' ?> href="<?=$raiz?>codigo">C&oacute;d.</a>
                    </li>
                    <li class="order" style="width:150px;">
                    	<a <?= ($filtroOrder == 'tblcliente.fldNome') 								? "class='$class'" : '' ?> href="<?=$raiz?>cliente">Cliente</a>
                    </li>
                    <li class="order" style="width:70px">
                    	<a <?= ($filtroOrder == 'tblpedido_funcionario_servico.fldFuncionario_Id') 	? "class='$class'" : '' ?>  style="width:80px;" href="<?=$raiz?>funcionario">Funcion&aacute;rio</a>
                    </li>
					<li style="width:115px;">Observa&ccedil;&atilde;o</li>
                    <li class="order" style="width:50px;">
                    	<a <?= ($filtroOrder == 'tblpedido.fldId') 									? "class='$class'" : '' ?> style="width:50px" href="<?=$raiz?>venda">Venda</a>
                    </li>                    
                    <li class="order" style="width:60px;">
                    	<a <?= ($filtroOrder == 'tblpedido_parcela.fldParcela') 					? "class='$class'" : '' ?> style="width:60px" href="<?=$raiz?>">Parcela</a>
                    </li>
                    <li class="order" style="width:60px;">
                    	<a <?= ($filtroOrder == 'tblpedido_parcela.fldValor') 						? "class='$class'" : '' ?> style="width:45px" href="<?=$raiz?>parcela">Valor</a>
                    </li>
                   
					<li style="width:70px; text-align:center;">Carteira</li>
					<li style="width:90px; text-align:center;">Pagamento</li>
					<li style="margin-right:22px;"></li>
					<li style="width:15px; margin:2px 0 0 0;"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de contas a receber">
                <tbody>
<?

					$id_array = array();
					$n 		= 0;
					
					$linha = "row";
					while($rowPedido = mysql_fetch_array($rsPedido)){
						
						//acumulando o valor e subtraindo o valor j� abatido do total
						$totalReceber 	+= $rowPedido['fldValor'] - $rowPedido['ValorBaixa'];
						$totalParcela  	= $rowPedido['fldValor'] - $rowPedido['ValorBaixa'];
						$funcionarioId 	= ($rowPedido['fldFuncionario_Id'])?$rowPedido['fldFuncionario_Id']:0;
						
						//linkar para o detalhe da venda ou NFe
						$paginaLink 	= redirecionarPagina($rowPedido['fldTipo_Id'], 'venda');
						
						//VERIFICA O TOTAL DE PARCELAS E CARTEIRA
						$rsParcela	 	= mysql_query("SELECT * FROM tblpedido_parcela WHERE fldPedido_Id = '" .$rowPedido['fldPedido_Id']. "'");
						$rowParcela 	= mysql_num_rows($rsParcela);
						$rsParcela2 	= mysql_query("SELECT * FROM tblpedido_parcela WHERE fldPedido_Id = '" .$rowPedido['fldPedido_Id']. "' AND fldParcela = '".$rowPedido['fldParcela']."'");
						$rowParcela2 	= mysql_fetch_array($rsParcela2);
						
						$carteira_Id 	= $rowParcela2['fldCarteira_Id'];
						
						$rsCarteira 	= mysql_query("SELECT * FROM tblpedido_parcela_carteira WHERE fldId = $carteira_Id");
						$rowCarteira	= mysql_fetch_array($rsCarteira);
						echo mysql_error();
						$parcelaTotal 	= str_pad($rowParcela, 2, "0", STR_PAD_LEFT);
						//VERIFICA O TOTAL DE PARCELAS E CARTEIRA
						
						$id_array[$n] = $rowParcela2["fldId"];
						$n += 1;
						
?>						<tr class="<?= $linha; ?>">
							<td style="width:90px; text-align:center;">
								<span title="<?=fnc_status_parcela($rowPedido['fldId'], "tblpedido_parcela")?>" class="parcela_<?=fnc_status_parcela($rowPedido['fldId'], "tblpedido_parcela")?>"><?=format_date_out($rowPedido['fldVencimento'])?></span>
                            </td>
							<td style="width:50px; text-align:center;"><a href="index.php?p=cliente_detalhe&id=<?=$rowPedido['ClienteCodigo']?>"><?=str_pad($rowPedido['ClienteCodigo'], 6, "0", STR_PAD_LEFT)?></a></td>
							<td style="width:148px; padding-left:5px;" <? ($rowPedido['fldVencimento'] < $data_atual)? print "class='vencido'" : '' ?>><?=$rowPedido['ClienteNome']; ?></td>
							<td style="width:95px; text-align:center;">
								<?	if($funcionarioId != '0') {	?>
								<a href="<?='?p=funcionario_detalhe&id='.$funcionarioId?>" title="Visualizar/Editar Funcion&aacute;rio"><?=str_pad($funcionarioId, 4, "0", STR_PAD_LEFT)?></a>
								<?php } ?>
                            </td>
							<td style="width:115px; padding-left:2px;"><?=substr($rowPedido['fldPedidoObs'], 0, 25);?></td>
							<td style="width:65px; text-align:right; padding-right:10px">
								<a href="<?='?p='.$paginaLink.'&id='.$rowPedido['fldPedido_Id']?>" title="Visualizar/Editar Pedido" style="border-bottom:1px dashed #ccc; display:block; text-decoration:none;">
									<?
										//verifico se � NFE, se for, exibo o n�mero da NFE, sen�o, o c�digo da venda mesmo!
                                    	$sFiscal 	= mysql_query("select * from tblpedido_fiscal where fldpedido_id = {$rowPedido['fldPedido_Id']}");
										$total 		= mysql_num_rows($sFiscal);
										if($total > 0){
											$sFiscal 	= mysql_fetch_assoc($sFiscal);
											$numero 	= str_pad($sFiscal['fldnumero'], 6, "0", STR_PAD_LEFT);
											echo "<span class='nfe' title='nfe'>$numero</span>"; 
										}else{
											echo "<span class='orcamento' title='venda comum'>".str_pad($rowPedido['fldPedido_Id'], 6, "0", STR_PAD_LEFT)."</span>";
										}
									?>
								</a>
							</td>
							<td style="width:78px; text-align:center;"><?=str_pad($rowPedido['fldParcela'], 2, "0", STR_PAD_LEFT)?>/<?=$parcelaTotal;?></td>
							<td style="width:55px; text-align:right;"><?=format_number_out($totalParcela)?></td>
							<td style="width:70px; text-align:center;"><?php if($carteira_Id != 0) { echo $rowCarteira['fldDesc']; } else { echo "Padr&atilde;o"; } ?></td>
							<td style="width:90px; text-align:center;"><?=$rowPedido['TipoPagamento'];?></td>
							<td style="width:20px;">
                            	<a href="?p=cliente_detalhe&amp;id=<?=$rowPedido['ClienteId']?>&amp;modo=pedido_parcela_editar&amp;filtro=<?=$rowPedido['fldId']?>" class="dar-baixa" title="Dar baixa">Dar baixa</a>
                            </td>
							<td>
								<input type="checkbox" name="chk_parcela_<?php echo $rowParcela2['fldId']; ?>" class="<?= fnc_status_parcela($rowParcela2['fldId'], "tblpedido_parcela")?>" id="chk_parcela_<?php echo $rowParcela2['fldId']; ?>" title="selecionar o registro posicionado" />
							</td>
						</tr>
<?
						$linha = ($linha == "row" ? "dif-row" : "row");
				   }
				   
?>		 		</tbody>
				</table>
            </div>
			
			<input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
			
			<div id="table_action" style="margin-top:3px">
             	<ul id="action_button">
					<li><input type="submit" name="btn_action" id="btn_baixa" 	value="dar baixa" 	title="Dar baixa em selecionados" class="btn_general" /></li>
					<li><input type="submit" name="btn_action" id="btn_carteira" 	value="mover p/ carteira" 	title="Mover para carteira" class="btn_general" /></li>
                    <li><button id="btn_print" name="btn_imprimir" value="imprimir" title="Imprimir relat&oacute;rio" ></button></li>
					<li style="display:none;"><a href="financeiro_contas_receber_mover_carteira,<?=$filtro_id?>" id="mover_carteira_verificado" class="modal" rel="320-135">Link para modal (mover para carteira)</a>
                </ul>
            </div>
			<div class="saldo" style=" width: 210px; float: right;">
				<p style="padding: 5px 5px 5px 10px;">Total &agrave; receber <span style="margin-left: 20px" class="credito"><?=format_number_out($totalReceber)?></span></p>
			</div>
              
        
        </div>
       
	</form>


	
<!-- DIV ESCONDIDA PARA MOVER A CARTEIRA (MODAL) -->
	
<div id="move_carteira" style="position:absolute; left:0; top:0; height:auto; display:none; z-index:9999; background:#FFF; width: 300px; margin:0 auto; margin-top:-160px; padding:15px;">

	<?php

		$queryCarteira = "SELECT * FROM tblpedido_parcela_carteira WHERE fldStatus = 1 ORDER BY fldDesc ASC";
		
		$getCarteira = mysql_query($queryCarteira);
		
		$chkCarteira = mysql_num_rows($getCarteira);
		
		if ($chkCarteira == 0) { echo "<p style='text-align:center; font-size:14px; margin:60px 0;'>Nenhuma carteira criada no momento</p>";}
	
		else
		{
	
		?>
			
			<div style="margin:0 auto;">				
			
				<h1 style="font-size:13px; margin:3px 0; text-align:left; font-weight:bold; color:#FF0000;">Mover selecionado para:</h1>
			
				<form class="frm_detalhe" id="formMoveCarteira" name="frmMoveCart" method="post" action="">
				
					<input type="hidden" name="txt_ids" id="txt_ids" value="<?php echo $filtro_id; ?>">
				
					<select name="sel_mover_carteira" id="sel_mover_carteira" style="width:300px; height:30px; padding:3px;">
				
						<option selected="selected" value="0"></option>
				
						<option value="0">Padr&atilde;o</option>
				
						<?php
				
							while($rowCarteira = mysql_fetch_array($getCarteira)){
				
								$id = $rowCarteira['fldId'];
								$desc = $rowCarteira['fldDesc'];
								$filtro = $_POST['hid_filtro'];
				
						?>
		
						<option value="<?php echo $id; ?>"><?php echo $desc; ?></option>
				
						<?php
						
							}
						
						?>
		
					</select>
				
					<input type="submit" style="margin-top:8px; float:right;" class="btn_enviar" name="btn_action" id="btn_action" value="mover" title="mover" />
					
					<span class="close" style="font-size:12px; text-decoration:underline; color:blue; cursor:pointer; float:right; margin:15px 0 0 0">cancelar</a>
				
				</form>
			
				<span style="display:block; clear:both"></span>
			
			</div>
	
		<?php
		
		}
	
	?>
</div>

<div id="mask" style="position:absolute; left:0; top:0; z-index:9000; background:#000; display:none"></div>