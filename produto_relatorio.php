<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Produtos</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css"></link>
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_calendario.css"></link>
        
        <script type="text/javascript" src="js/general.js"></script>
        <script type="text/javascript" src="js/jquery-1.4.4.js"></script>
        <script type="text/javascript" src="js/jquery.click-calendario-min.js"></script>
        <script type="text/javascript" src="js/init-click-calendario.js"></script>
		<script type="text/javascript" src="js/produto.js"></script>
        <script type="text/javascript" src="js/jquery.maskedinput-1.2.2.js"></script>
        <script type="text/javascript" src="js/init-validate.js"></script>
	</head>
	<body>
<?php
	
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		ob_start();
		session_start();
		
		$tipo_relatorio	= $_POST['sel_produto_tipo_relatorio'];
		$ordem			= $_POST['sel_produto_ordem'];
		$produtos		= $_POST['sel_produto_exibir'];
		$data_inicio	= format_date_in($_POST['txt_data1']);
		$data_final		= format_date_in($_POST['txt_data2']);
		$cliente		= $_POST['sel_cliente_exibir'];
		$detalhes		= $_POST['sel_produto_detalhes'];
		
		if($detalhes == 'detalhado'){
			$tipo_relatorio	= $tipo_relatorio.'_detalhado';
		}
		
		$exibir_valor		= ($_POST['chk_produto_exibir_valor'] == true)		? '1' : '0';
		$exibir_fornecedor	= ($_POST['chk_produto_exibir_fornecedor'] == true)	? '1' : '0';
		$exibir_unidade		= ($_POST['chk_produto_exibir_unidade'] == true)	? '1' : '0';
	
		$rsDados = mysql_query("select * from tblempresa_info");
		$rowDados = mysql_fetch_array($rsDados);
		
		$CPF_CNPJDados = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rsUsuario  = mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']);
		$rowUsuario = mysql_fetch_array($rsUsuario);
		
		
		if($_POST['sel_produto_ordem'] == 'fornecedor'){
			#FACO UM REPLACE PRA INSERIR TB A TABELA DE FORNECEDORES PRA ORDENAR PELO NOME
			$select =  ", tblfornecedor.fldNomeFantasia FROM tblproduto LEFT JOIN tblfornecedor ON tblproduto.fldFornecedor_Id = tblfornecedor.fldId";
			$sql	= str_replace(" FROM tblproduto",$select,$_SESSION['produto_relatorio_sql']);
			$sql	= str_replace(" ORDER BY " . $_SESSION['order_produto'], ' ORDER BY tblfornecedor.fldNomeFantasia', $sql);
		}else{
			$sql	= $_SESSION['produto_relatorio_sql'];
		}
?>

        <div id="no-print">
            <a class="print" href="#" onClick="window.print()">imprimir</a>
        </div>
<?		
		$rsRelatorio 	= mysql_query( $sql); 
		echo mysql_error();
		
		if($tipo_relatorio == 'tabela_preco'){
			header("Location: produto_relatorio_tabela_preco.php?tabela=".$_POST['sel_tabela_preco']."&fornecedor=".$_POST['sel_fornecedor']);
		} else {		
			include('produto_relatorio_'.$tipo_relatorio.'.php');
		}
?>
		
	</body>
</html>
