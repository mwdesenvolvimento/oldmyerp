<?
		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$rota_cabecalho = ($_SESSION["sistema_rota_controle"] > 0) ? 'Rota' : '' ;
		//$pgTotal 		= $rowsItem / $limite;
		$p = 1;
		
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Consignados</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border-top: 1px solid">
				<td style="width:50px;margin-right:10px;text-align:right">C&oacute;d</td>
				<td style="width:60px">Data</td>
				<td style="width:195px">Funcionario 1</td>
				<td style="width:195px">Funcionario 2</td>
				<td style="width:70px; text-align:right">A vista</td>
				<td style="width:70px; text-align:right">A prazo</td>
				<td style="width:70px; text-align:right">Recebido</td>
				<td style="width:75px; text-align:right">Retornado</td>
				<td style="width:8px"></td>
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
		##########################################################################################################################################################################################
		
			$sSQL 			= $_SESSION['relatorio_pedido_consignado'];

			$rsConsignado	= mysql_query($sSQL);
			$rowsConsignado	= mysql_num_rows($rsConsignado);
		
			$n	 			= 1; #DEFINE O NUMERO DA DO BLOCO
			$countRegistro  = 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE DE 20
			$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
			$limite 		= 45;
			
			$pgTotal 		= ceil($rowsConsignado / $limite);
			$p = 1;

			while($rowConsignado = mysql_fetch_array($rsConsignado)){
				
				$consignado_id = $rowConsignado['fldId'];
				$rsConsignadoItem = mysql_query("SELECT SUM(fldQuantidade_Aprazo * fldValor) as fldTotal_Aprazo, SUM(fldQuantidade_Avista * fldValor) as fldTotal_Avista FROM tblpedido_consignado_item WHERE fldConsignado_Id = $consignado_id");            
				$rowConsignadoItem = mysql_fetch_array($rsConsignadoItem);
				
				$rsConsignadoRecebido = mysql_query("SELECT SUM(fldQuantidade * fldValor) as fldTotal_Recebido FROM tblpedido_consignado_item_recebido WHERE fldConsignado_Id = $consignado_id");            
				$rowConsignadoRecebido = mysql_fetch_array($rsConsignadoRecebido);
				
				$total_avista		= $rowConsignadoItem['fldTotal_Avista'];
				$total_aprazo	 	= $rowConsignadoItem['fldTotal_Aprazo'];
				$total_recebido	 	= $rowConsignadoRecebido['fldTotal_Recebido'];
				$total_retorno	 	= $rowConsignado['fldValor_Retorno'];
				
				$rodape_avista 		+= $total_avista;			
				$rodape_aprazo 		+= $total_aprazo;			
				$rodape_recebido 	+= $total_recebido;			
				$rodape_retorno 	+= $total_retorno;			
				
				$pagina[$n] .='
				<tr>
                	<td style="width:50px;text-align:right">'.str_pad($rowConsignado['fldId'], 6, "0", STR_PAD_LEFT).'</td>
					<td style="width:60px;margin:0;">'.format_date_out3($rowConsignado['fldData']).'</td>
					<td style="width:195px;margin:0">'.substr($rowConsignado['fldFuncionario1'],0, 25).'</td>
					<td style="width:195px;margin:0">'.substr($rowConsignado['fldFuncionario2'],0, 25).'</td>
					<td style="width:70px;margin:0;text-align:right">'.format_number_out($total_avista).'</td>
					<td style="width:70px;margin:0;text-align:right">'.format_number_out($total_aprazo).'</td>
					<td style="width:70px;margin:0;text-align:right">'.format_number_out($total_recebido).'</td>
					<td style="width:75px;margin:0;text-align:right">'.format_number_out($total_retorno).'</td>
					<td style="width:5px;margin:0"></td>
				</tr>';
				
				
				#SE CHEGAR A 20 LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
				if($countRegistro == $limite){
					$countRegistro = 1;
					$n ++;
				}elseif($rowsConsignado == $x && $countRegistro < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU 20 LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
					while($countRegistro <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countRegistro++;}
				}else{
					$countRegistro ++;
				}
				$x ++;
			}
			
		#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
				
                print '<td style="width: 200px"><p class="pag">'.$p.' de '.$pgTotal.'</p></td>';
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				echo  $pagina[$x];
?>
						</table>
					</td >
				</tr>
			</table>			
<?			$x ++;
			$p ++;
		}
?>        
		<table class="table_relatorio_rodape" summary="Relat&oacute;rio">
            <tr>
                <td style="width:10px">&nbsp;</td>
                <td style="width:100px">Total a vista</td>
                <td style="width:60px; text-align:right;"><?=format_number_out($rodape_avista)?></td>
				<td style="width:10px; border-left:1px solid">&nbsp;</td>
                <td style="width:100px">Total a prazo</td>
                <td style="width:60px; text-align:right"><?=format_number_out($rodape_aprazo)?></td>
                <td style="width:10px; border-left:1px solid">&nbsp;</td>
                <td style="width:100px">Total recebido</td>
                <td style="width:60px; text-align:right"><?=format_number_out($rodape_recebido)?></td>
                <td style="width:10px; border-left:1px solid">&nbsp;</td>
                <td style="width:100px">Total retornado</td>
                <td style="width:60px; text-align:right"><?=format_number_out($rodape_retorno)?></td>
            </tr>
        </table>      
	