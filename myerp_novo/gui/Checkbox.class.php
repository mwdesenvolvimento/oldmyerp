<?php


/**
 * Description of Select
 *
 * @author Ivan
 */
class Checkbox {
    private $html;
    
    /**
     * Monta um checkbox inteiro e define o checked
     * @param string $nome
     * @param boolean $checked
     */
    public function __construct($nome, $checked = 0) {
    
    $checkbox = '<input type="checkbox" value="1" ';
    $checkbox .= "id=\"$nome\" ";
    $checkbox .= "name=\"$nome\" ";
    
    if($checked){
        $checkbox .= 'checked ';
    }
    
    $checkbox .= '>';
        
        $this->html = $checkbox;
    }
    
    
    public function __toString() {
        return $this->html;
    }
}
