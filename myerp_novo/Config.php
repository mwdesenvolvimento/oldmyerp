 <?php
    /*
     * Configuração PHP
     */
    ini_set('display_errors', 1);
    ini_set('log_errors', 1);
    ini_set('error_log', dirname(__FILE__) . 'error_log.txt');
    ini_set('mssql.datetimeconvert', 0);
    error_reporting(E_ALL);
    session_start();
    
    /*
     * Carregar classes dinamicamente
     */
    function __autoload($classe){
        $diretorios = array(
            './',
            './libs/',
            './libs/cliente/',
            './libs/funcionario/',
            './libs/ordemDeProducao/',
            './libs/ordemDeProducao/historico/',
            './libs/pedido/',
            './libs/produto/',
            './libs/outros/',
            './libs/transportador/',
            './utils/',
            './gui/',
            './controller/',
            './controller/ordemDeProducao/',
            './controller/ordemDeProducao/historico/',
            './controller/home/',
            './controller/produto/'
        );
        
        foreach($diretorios as $dir){
            if(file_exists($dir . $classe . '.class.php')){
                require_once $dir . $classe . '.class.php';
            }
        }
    }
    
    
    /*
     * Constantes para conexão com o banco
     */
    define('DB_HOST', '192.168.1.10');
    define('DB_PORT', '3306');
    define('DB_NAME', 'myerp');
    define('DB_USER_NAME', 'myerp');
    define('DB_PASSWORD', 'senha753951');
    
    /*
     * Browser
     */
    define('USER_AGENT_NAME', UserAgent::getNome());
    
?>