<?php

/**
 * Trait para formatar campos da TRecord
 *
 * @author Ivan
 */
trait TRecordFormatacao {
    private $formatarParaFormulario = false;
    
    public function formatar() {
        foreach ($this->fields as $campo => $propriedades) {
            $tipo = $propriedades['tipo'];
            if (isset($propriedades['value'])) {
                $value = $propriedades['value'];
                
                if(method_exists($this, $tipo.'FormatarDoBd')){
                    $this->fields[$campo]['value'] = call_user_func(array($this, $tipo.'FormatarDoBd'), $value);
                }
            }
        }
    }

    public function formatarParaBd() {
        foreach ($this->fields as $campo => $propriedades) {
            $tipo = $propriedades['tipo'];
            if (isset($propriedades['value'])) {
                $value = $propriedades['value'];
                
                if(method_exists($this, $tipo.'FormatarParaBd')){
                    $this->fields[$campo]['value'] = call_user_func(array($this, $tipo.'FormatarParaBd'), $value);
                }
            }
        }
    }
    
    /*
     * método isFormulario
     * prepara formatação para formulário
     * tratando input['date'] do Chrome e outros
     */
    public function setFormatarParaFormulario($bol) {
        $this->formatarParaFormulario = $bol;
    }
    
    /*
     * método dateFormatarDoBd
     * formata a data para apresentar ao usuário
     * Chrome possui datePicker que entende o formato como 'yyyy-mm-dd' e formata automaticamente
     * Outros navegadores não possuem, então é formatado aqui
     */
    private function dateFormatarDoBd($data) {
        //if($this->formatarParaFormulario and USER_AGENT_NAME == 'Chrome'){ return $data; }
        if($data == '' || $data == null) return "";
        $data = explode('-', $data);
        $data = $data[2]."/".$data[1]."/".$data[0];
        if($data == '00/00/0000') return "";
        return $data;
    }

    private function dateFormatarParaBd($data){
        if($data == '' || $data == null) return "";
        $data = explode('/', $data);
        $data = $data[2]."-".$data[1]."-".$data[0];
        return $data;
    }
    
    private function moneyFormatarDoBd($valor){
        return number_format($valor, 2, ',', '.');
    }

    private function moneyFormatarParaBd($valor){
    	return str_replace(",", ".", str_replace(".", "", $valor));
    }
    
    private function decimalFormatarDoBd($valor){
        return number_format($valor, 2, ',', '.');
    }

    private function decimalFormatarParaBd($valor){
    	return str_replace(",", ".", str_replace(".", "", $valor));
    }

}
