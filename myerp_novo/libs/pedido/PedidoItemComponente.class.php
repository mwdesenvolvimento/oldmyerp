<?php

/**
 * Description of PedidoItem
 *
 * @author Ivan
 */
class PedidoItemComponente extends TRecord {
    
    protected $dataSource   = 'vwPedidoItemComponente';

    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'itemId'				=> array('tipo' => 'integer'	),
        'pedidoId'				=> array('tipo' => 'integer'	),
        'produtoId'				=> array('tipo' => 'integer'	),
        'produtoNome'				=> array('tipo' => 'string'	),
        'componenteId'				=> array('tipo' => 'integer'	),
        'componenteNome'			=> array('tipo' => 'string'	),
        'componenteQuantidade'			=> array('tipo' => 'double'	),
        'excluido'				=> array('tipo' => 'integer'	),
    );
}
