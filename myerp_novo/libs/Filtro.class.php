<?php

/**
 * @author Ivan
 */

class Filtro {
    
    use Fields;
    
    //Nome da sessão em que será armazenado os valores dos campos
    public $sessionName;
    
    //Nome do botão que enviará o formulário do filtro
    protected $submitButtonName;

    //Nome do botão que fará com que todos os filtros sejam inativados
    protected $clearButtonName;
    
    public function __construct(TRecord $object) {
        $this->fields = $object->getFields();
        $this->validarFiltro();
    }
    
    /**
     * Salva o tipo de filtro a ser realizado no campo
     * @param string @field 
     * @param string @tipo [BETWEEN, IN, NOT IN, LIKE, NOT LIKE, MAIOR, MENOR, MAIOR IGUAL, MENOR IGUAL, ABSOLUTE] */
    public function setTipo($field, $tipo){
        if(isset($this->fields[$field])){
            $this->fields[$field]['tipoFiltro'] = $tipo;
        } else {
            echo 'campo não encontrado';
        }
    }
    
    //valida a session quando receber o botão submit
    private function validarSubmit(){
        //sempre que for filtrado, destroi a sessão para então refaze-la
        if(isset($_SESSION[$this->sessionName])){ unset($_SESSION[$this->sessionName]); }

        
        foreach($this->fields as $field => $value){
            if(isset($_POST[$field]) && $_POST[$field] != ''){
                if(is_array($_POST[$field])){
                    //limpo todo index em branco
                    $checkArray = array_filter($_POST[$field]);
                    if(empty($checkArray)){ continue; }
                }
                
                $_SESSION[$this->sessionName][$field] = $_POST[$field];
            }
        }
    }
    
    /**
     * Esta função valida e alimenta a session de acordo com o $_POST enviado
     * do formulário do filtro */
    protected function validarFiltro(){
        //valido a session se estiver recebendo um submit
        if(isset($_POST[$this->submitButtonName])){ 
            $this->validarSubmit(); 
        }
        
        //se estiver limpando o filtro
        if(isset($_POST[$this->clearButtonName])){
            if(isset($_SESSION[$this->sessionName])){ 
                unset($_SESSION[$this->sessionName]);
            }
        }
        
        //valida os fields.
        foreach($this->fields as $field => $value){
            if(isset($_SESSION[$this->sessionName][$field])){
                $this->$field = $_SESSION[$this->sessionName][$field];
            }
        }
    }
    
    /**
     * Monto e penduro no index (sql) uma instrução simples para cada field alimentado
     * @param   string    $field
     * @param   array     $value
     * @return  string
     */
    public function formatSql($field, $value){
        //variavel que será retornada;
        $sql = '';
        
        //algum valor deve ser setado anteriormente
        if(isset($value['value'])){
            //caso nenhum tipo tenha sido setado, uso absolute como padrão
            if(!isset($value['tipoFiltro'])){ $value['tipoFiltro'] = '='; }
            
            // @see Filtro::setTipo();
            switch($value['tipoFiltro']){
                /* escreve uma instrucao para filtrar um periodo, 
                 * o value obrigatoriamente precisa ser uma array */
                case 'BETWEEN':
                    if(is_array($value['value'])){
                        $dataInicial    = (isset($value['value'][0]) && $value['value'][0] != '') ? $value['value'][0] : '0001-01-01';
                        $dataFinal      = (isset($value['value'][1]) && $value['value'][1] != '') ? $value['value'][1] : '9999-12-31';
                        $sql            = "($field BETWEEN '$dataInicial' AND '$dataFinal')";
                    }
                break;
                
                /* escreve uma instrucao para filtrar multiplos valores.
                 * o value deve ser uma array. */
                case 'IN':
                case 'NOT IN':
                    if(is_array($value['value'])){
                        $strValue   = implode(',', $value['value']);
                        $sql        = "($field {$value['tipoFiltro']} ('{$strValue}'))";
                    }
                break;
                
                /* instrucao simples
                 * value deve ser string */
                case '>':
                case '>=':
                case '<':
                case '<=':
                case '=':
                case 'LIKE':
                case 'NOT LIKE':
                    if(is_string($value['value'])){
                        $sql    = "($field {$value['tipoFiltro']} '{$value['value']}')";
                    }
                break;
            }
            //retorna a sql já preparada
            return $sql;
        }
    }

    /**
     * Monta a instrucao sql do WHERE de acordo com o que está setado
     * nos fields
     * @return string */
    public function getInstrucaoSql(){
        $instrucoes = array();        
        foreach($this->fields as $field => $value){
            $instrucaoSql = $this->formatSql($field, $value);
            $instrucoes[] = $instrucaoSql;
        }
        
        //limpo os registros em braco da array de instrucoes;
        $instrucoes = array_filter($instrucoes);
        $instrucoes = implode(' and ', $instrucoes);
        return $instrucoes;
    }

}
