<?php
/**
 * Description of Validar
 * Trait para validar dados dentro das classes base de tabelas do BD
 *
 * @author Ivan
 */


trait Validacao{
    //protected $fields; //propriedade da classe TRecord
    protected $erros;
    
    /*
     * método validar
     * valida a coleção fields
     * return erros[]
     */
    public function validar(){
        foreach ($this->fields as $campo => $propriedades) {
            $tipo = $propriedades['tipo'];
            if(isset($propriedades['value'])){
                $value = $propriedades['value'];
                call_user_func(array($this,$tipo), $campo, $value);
            }
        }
        return $this->erros;
    }
    
    
    private function date($campo, $value){
        $dia = 0;
        $mes = 0;
        $ano = 0;
        //testa o formato da data e se contém apenas números
        if(preg_match("/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/", $value)){
            $date = explode('/', $value);
            $dia = $date[0];
            $mes = $date[1];
            $ano = $date[2];
        }
        /* se precisar do formato de banco:
        elseif(preg_match("/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/", $value)){
            $date = explode('-', $value);
            $dia = $date[2];
            $mes = $date[1];
            $ano = $date[0];
        }
        */
        else{
            $this->erros[] = "Campo $campo inválido. Informado: $value";
            return false;
        }
        //testa a validade da data em si
        if(checkdate($mes, $dia, $ano)){
            return true;
        }
        else{
            $this->erros[] = "Campo $campo inválido. Informado: $value";
            return false;
        }
    }
    
    private function doublebkp($campo, $valor){
        $valorString = trim(strtr($valor,array(','=>'.', '.'=>'')));
        $valorDouble = (double) $valorString;
        //testa se o valor não se perdeu após o casting, garantindo a validade
        if($valorString == (string) $valorDouble){
            return true;
        }
        else{
            $this->erros[] = "Campo $campo inválido. Informado: $valor. Esperado: número double";
        }
    }
    
    private function double($campo, $valor){
        $valorString = trim(strtr($valor,array(','=>'.', '.'=>'')));
        $valorDouble = (double) $valorString;
        //testa se o valor não se perdeu após o casting, garantindo a validade
        if($valorString == (string) $valorDouble){
            return true;
        }
        else{
            $this->erros[] = "Campo $campo inválido. Informado: $valor. Esperado: número double";
        }
    }
    
    
    private function integer($campo, $valor, $min_range = 50, $max_range = 999) {
        $valorString = trim(strtr($valor,array(','=>'.', '.'=>'')));
        $valorInteger = (integer) $valorString;
        
        $arrayOptions = null;
        $intOptions = null;
        $mensagem_extra = '';
        
        if($min_range){
            $arrayOptions['min_range'] = $min_range;
            $mensagem_extra .= " - maior que $min_range";
        }
        
        if($max_range){
            $arrayOptions['max_range'] = $max_range;
            $mensagem_extra .= " - menor que $max_range";
        }
        
        if($arrayOptions){
            $intOptions = array("options" => $arrayOptions);
        }

        if(!filter_var($valorInteger, FILTER_VALIDATE_INT, $intOptions)){
            $this->erros[] = "Campo $campo inválido. Informado: $valor. Esperado: número inteiro$mensagem_extra";
            return false;
        }

        if($valorString == (string) $valorInteger){
            return true;
        }
        else{
            $this->erros[] = "Campo $campo inválido. Informado: $valor. Esperado: número inteiro";
            return false;
        }
    }
    
    private function email($campo, $valor){
        if(filter_var($valor,FILTER_VALIDATE_EMAIL)){
            return true;
        }
        else{
            $this->erros[] = "Campo $campo inválido. Informado: $valor. Esperado: e-mail";
        }
    }
    
}
    
    
    
