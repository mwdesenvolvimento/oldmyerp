<?php

/**
 * Manipula coleções de objetos
 *
 * @author Ivan
 */
final class Repository {
    
    private $classe;
    private $dataShow; // nome da classe manipulada pelo repositório
    private $fields;
    private $join;
    private $where;
    private $limit;
    private $filtro = null;
    
    function __construct($classe) {
        $this->classe       = get_class($classe);
        $this->dataShow   = $classe->getDataShow();
    }
    
    public function setFields($fieldList){
        $this->fields = $fieldList;
    }

    public function setJoin($arg){
        $this->join = $arg;
    }
    
    public function setWhere($where){
        $this->where = $where;
    }
    
    public function setLimit($value){
        $this->limit = $value;
    }
    
    public function setFiltro(Filtro $filtro){
        $this->filtro = $filtro;
    }

    function listar(){
        $this->fields   = ($this->fields    == '')  ? $this->dataShow.'.*'  : $this->fields;
        $this->join     = ($this->join      == '')  ? ''                    : $this->join;
        $this->where    = ($this->where     == '')  ? '1'                   : $this->where;
        $this->limit    = ($this->limit     == '')  ? ''                    : 'limit ' . $this->limit;
        
        $db     = Conexao::getConexao();
        $sql    = "select {$this->fields} from {$this->dataShow} {$this->join} where {$this->where} " . $this->limit;
        
        if($this->filtro && $this->filtro->getInstrucaoSql() != ''){
            //conecta o where ao filtro.
            $sql .= " and ";
            //pega a instrucao do filtro.
            $sql .= $this->filtro->getInstrucaoSql();
        }
               
        $sth = $db->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_CLASS, $this->classe, null);
    }

}