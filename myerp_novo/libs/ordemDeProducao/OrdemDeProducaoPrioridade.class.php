<?php

/**
 * Description of OrdemDeProducaoStatus
 *
 * @author Ivan
 */
class OrdemDeProducaoPrioridade extends TRecord {
    
    protected $dataSource = 'tblOrdemDeProducaoPrioridade';
    protected $dataShow = 'tblOrdemDeProducaoPrioridade';
    
    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'nome'                                  => array('tipo' => 'string'	),
    );
}
