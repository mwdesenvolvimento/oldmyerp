<?php

/**
 * Description of OrdemDeProducaoItem
 *
 * @author Ivan */
class OrdemDeProducaoItem extends TRecord {

    protected $dataSource   = 'tblOrdemDeProducaoItem';
    protected $dataShow     = 'vwOrdemDeProducaoItem';

    protected $fields       = array(
        'id'					        => array('tipo' => 'integer'	),
        'ordemDeProducaoId'			    => array('tipo' => 'integer'	),
        'pedidoId'				        => array('tipo' => 'integer'	),
        'itemId'                        => array('tipo' => 'integer'    ),
        'produtoId'				        => array('tipo' => 'integer'	),
        'clienteProdutoCodigo'			=> array('tipo' => 'integer'	),
        'produtoCodigo'				    => array('tipo' => 'string'	    ),
        'produtoNome'				    => array('tipo' => 'string'	    ),
        'produtoLote'            		=> array('tipo' => 'string'	    ),
        'operador'                      => array('tipo' => 'string'     ),
        'produtoUnidadeMedidaId'  		=> array('tipo' => 'integer'	),
        'produtoUnidadeMedidaNome'  	=> array('tipo' => 'string'	    ),
        'produtoQuantidade'			    => array('tipo' => 'decimal'	),
        'produtoVerniz'				    => array('tipo' => 'integer'	),
        'produtoVernizQualidade'		=> array('tipo' => 'integer'	),
        'produtoClicheCodigo'			=> array('tipo' => 'string'	    ),
        'produtoFacaNumero'			    => array('tipo' => 'string'	    ),
        'produtoLargura'			    => array('tipo' => 'decimal'	),
        'produtoAltura'                 => array('tipo' => 'decimal'	),
        'produtoPapelId'			    => array('tipo' => 'integer'	 ,	'estrangeiroClass' => 'Produto',		'estrangeiroCampo' => 'nome'	),
        'produtoPapelNome'			    => array('tipo' => 'string'	    ),
        'produtoPapelLargura'			=> array('tipo' => 'decimal'	),
        'produtoPapelComprimento'		=> array('tipo' => 'decimal'	),
        'produtoPapelAcrescimo'			=> array('tipo' => 'decimal'	),
        'produtoPapelComprimentoFinal'  => array('tipo' => 'decimal'	),
        'produtoPapelQuantidadeM2'		=> array('tipo' => 'decimal'	),
        'produtoMaquinaId'			    => array('tipo' => 'integer'	 ,	'estrangeiroClass' => 'Maquina',		'estrangeiroCampo' => 'nome'	),
        'produtoMaquinaAcerto'			=> array('tipo' => 'decimal'	),
        'produtoPapelProduzido'			=> array('tipo' => 'decimal'	),
        'produtoPapelRetorno'			=> array('tipo' => 'decimal'	),
        'produtoPapelAcerto'			=> array('tipo' => 'decimal'	),
        'produtoPapelEntregue'			=> array('tipo' => 'string'	    ),
        'produtoPapelPerdido'           => array('tipo' => 'decimal'	),
        'produtoQuantidadePerdida'      => array('tipo' => 'decimal'	),
        'produtoTotalEtiquetas'			=> array('tipo' => 'decimal'	),
        'excluido'				        => array('tipo' => 'integer'	),
    );

    public function getComponentes(){
        $componente     = new Repository(new OrdemDeProducaoItemComponente());
        $componente->setWhere("itemId = '{$this->id}'");
        $componentes    = $componente->listar();
        return $componentes;
    }
}
