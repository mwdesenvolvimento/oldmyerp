<?php

/**
 * OrdemDeProducao usa uma view simples para ser usada ao inserir e atualizar registros
 *
 * @author Ivan
 */
class OrdemDeProducaoHistorico extends TRecord {
    
    protected $dataSource   = 'tblOrdemDeProducaoHistorico';
    protected $dataShow     = 'vwOrdemDeProducaoHistorico';
    
    protected $fields = array(
        'id'					        => array('tipo' => 'integer' ),
        'ordemDeProducaoId'             => array('tipo' => 'integer' ),
        'ordemDeProducaoStatus'			=> array('tipo' => 'string'	 ),
        'ordemDeProducaoPrioridade'		=> array('tipo' => 'string'	 ),
        'ordemDeProducaoObservacao'		=> array('tipo' => 'string'	 ),
        'funcionarioNome'			    => array('tipo' => 'string'	 ),
        'funcionarioId'				    => array('tipo' => 'integer' ),
        'cadastroData'				    => array('tipo' => 'date'	 ),
        'usuarioNome'				    => array('tipo' => 'string'	 ),
        'usuarioId'				        => array('tipo' => 'integer' ),
        'descricao'				        => array('tipo' => 'string'  ),
        'excluido'				        => array('tipo' => 'integer' ),
    );
    
}
