<?php

/**
 * Description of OrdemDeProducaoItem
 *
 * @author Ivan
 */
class OrdemDeProducaoItemComponente extends TRecord {
    protected $dataSource   = 'tblOrdemDeProducaoItemComponente';
    protected $dataShow     = 'vwOrdemDeProducaoItemComponente';
    protected $fields       = array(
        'id'					=> array('tipo' => 'integer'	),
        'itemId'				=> array('tipo' => 'integer'	),
        'componenteId'				=> array('tipo' => 'integer'	),
        'componenteNome'			=> array('tipo' => 'string'	),
        'excluido'				=> array('tipo' => 'integer'	),
    );
}
