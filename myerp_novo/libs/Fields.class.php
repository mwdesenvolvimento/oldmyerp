<?php
/**
 * Description of Validar
 * Trait para usar o padrão "fields"
 *
 * @author Ivan
 */


trait Fields {
    /**
     * Array com os campos da classe em uso
     * @var type array
     */
    protected $fields;
    

    /**
     * Executado ao requerir uma propriedade.
     * Verifica se a propriedade existe antes de retorna-la
     * @param string $property
     * @return string
     */
    public function __get($property) {
        if (array_key_exists($property, $this->fields)) {
            if(isset($this->fields[$property]['value'])){
                return $this->fields[$property]['value'];
            }else{
                return null;
            }
        }else{
            return "< $property > não encontrado na classe " . get_class($this);
        }
    }

    /**
     * Executado ao atribuir um valor a uma propriedade
     * Verifica se existe metodo set especifico para a propriedade
     * @param string $property
     * @param string $value
     * @return boolean */
    public function __set($property, $value){
        $metodo = 'set' . ucfirst($property);
        //verifica se existe método específico set<$propriedade> antes de executar o genérico
        if (method_exists($this, $metodo)) {
            call_user_func(array($this, $metodo), $value);
        } else {
            //verifica se existe o campo na coleção fields
            if (array_key_exists($property, $this->fields)) {
                $this->fields[$property]['value'] = $value;
            }
        }

        return true;
    }
    
    /**
     * Retorna a array fields
     * @return string
     */
    public function getFields(){
        return $this->fields;
    }
}
    
    
    
