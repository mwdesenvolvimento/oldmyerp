<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrdemDeProducaoStatus
 *
 * @author Ivan
 */
class Maquina extends TRecord {
    
    protected $dataSource = 'tblMaquina';
    
    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'nome'                                  => array('tipo' => 'string'	),
        'observacao'                            => array('tipo' => 'string'	),
        'excluido'                              => array('tipo' => 'integer'	),
    );
}
