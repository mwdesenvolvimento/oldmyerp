<?php

/**
 *
 * @author Ivan
 */

class ProdutoEspecificacao extends TRecord {

    /**
     * é uma view temporaria para uso na especificacao
     */
    protected $dataSource   = 'tblProdutoEspecificacao';
    
    protected $fields = array(
        'produtoId'				=> array('tipo' => 'integer'	),
        'maquinaId'				=> array('tipo' => 'integer'	),
        'papelId'				=> array('tipo' => 'integer'	),
        'largura'				=> array('tipo' => 'decimal'	),
        'altura'				=> array('tipo' => 'decimal'	),
        'portaClicheNumero'			=> array('tipo' => 'string'	),
        'papelLargura'				=> array('tipo' => 'decimal'	),
        'papelMaquinaQtd'			=> array('tipo' => 'decimal'	),
        'regua'					=> array('tipo' => 'decimal'	),
        'papelCalculoTamanho'			=> array('tipo' => 'decimal'	),
        'maquinaVelocidade'			=> array('tipo' => 'decimal'	),
        'facaNumero'				=> array('tipo' => 'string'	),
        'carreiraNumero'			=> array('tipo' => 'decimal'	),
        'clicheRepeticao'			=> array('tipo' => 'decimal'	),
        'clicheCodigo'				=> array('tipo' => 'string'	),
        'papelAcerto'				=> array('tipo' => 'decimal'	),
        'producaoTempo'				=> array('tipo' => 'decimal'	),
        'roloId'				=> array('tipo' => 'integer'	),
        'verniz'				=> array('tipo' => 'integer'	),
        'vernizQualidade'			=> array('tipo' => 'integer'	),
        'etiquetaRoloQtd'			=> array('tipo' => 'decimal'	),
        'etiquetaCortada'			=> array('tipo' => 'integer'	),
        'acertoTempo'				=> array('tipo' => 'string'	),
        'rebobinamentoTipoId'			=> array('tipo' => 'integer'	),
        'rebobinamentoMaquinaId'		=> array('tipo' => 'integer'	),
        'rebobinamentoPapelLargura'		=> array('tipo' => 'decimal'	),
        'rebobinamentoCarreiraNumero'		    => array('tipo' => 'decimal'	),
        'rebobinamentoMaquinaVelocidade'	    => array('tipo' => 'decimal'	),
        'rebobinamentoFacaNumero'		        => array('tipo' => 'string'	),
        'rebobinamentoMaquinaAcerto'		    => array('tipo' => 'decimal'	),
        'rebobinamentoTubete'			        => array('tipo' => 'string'	),
        'rebobinamentoRoloDiametro'		        => array('tipo' => 'string'	),
        'rebobinamentoRoloMetroQuantidade'	    => array('tipo' => 'decimal'	),
        'rebobinamentoRoloPlastificado'         => array('tipo' => 'integer'	),
        'rebobinamentoEtiquetaMetroQuantidade'	=> array('tipo' => 'decimal'	),
        'rebobinamentoIr'			            => array('tipo' => 'string'	),
        'rebobinamentoCa'			            => array('tipo' => 'string'	),
        'rebobinamentoTempo'			        => array('tipo' => 'decimal'	),
        'rebobinamentoData'			            => array('tipo' => 'date'	),
        'rebobinamentoObservacao'		        => array('tipo' => 'string'	),
        'embalagemRoloDiametro'			=> array('tipo' => 'string'	),
        'embalagemRoloAltura'			=> array('tipo' => 'string'	),
        'embalagemRoloPlastificado'		=> array('tipo' => 'integer'	),
        'embalagemCaixaId'			=> array('tipo' => 'integer'	),
        'embalagemTransportadorId'		=> array('tipo' => 'integer'	),
        'embalagemQuantidadeExata'		=> array('tipo' => 'string'	),
        'embalagemEnchimentoId'			=> array('tipo' => 'integer'	),
        'embalagemData'				=> array('tipo' => 'date'	),
        'embalagemObservacao'			=> array('tipo' => 'string'	),
    );

}