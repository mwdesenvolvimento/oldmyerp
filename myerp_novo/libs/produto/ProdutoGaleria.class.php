<?php

/**
 *
 * @author Ivan
 */

class ProdutoGaleria extends TRecord {

    /**
     * é uma view temporaria para uso na especificacao
     */
    protected $dataSource   = 'tblProdutoGaleria';
    
    protected $fields = array(
        'id'        => array('tipo' => 'integer'    ),
        'produtoId'	=> array('tipo' => 'integer'	),
        'codigo'	=> array('tipo' => 'string'	),
        'ordem'		=> array('tipo' => 'integer'	),
        'capa'      => array('tipo' => 'decimal'	),
        'excluido'	=> array('tipo' => 'decimal'	),
    );

}