<?php

/**
 *
 * @author Ivan
 */

class ProdutoComponente extends TRecord {

    /**
     * é uma view temporaria para uso na especificacao
     */
    protected $dataSource   = 'tblproduto_componente';
    protected $dataShow     = 'vwProdutoComponente';
    
    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'produtoId'				=> array('tipo' => 'integer'	),
        'produtoNome'				=> array('tipo' => 'string'	),
        'componenteId'				=> array('tipo' => 'integer'	),
        'componenteNome'			=> array('tipo' => 'string'	),
        'componenteQuantidade'			=> array('tipo' => 'decimal'	),
        'componenteRelacionamento'		=> array('tipo' => 'integer'	),
        'componenteQuantidadeProporcional'	=> array('tipo' => 'decimal'	),
    );

}