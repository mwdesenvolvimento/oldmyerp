<?php
/**
 * Classes e objetos relacionados com os controladores
 * da aplicação
 * @package	com.imasters.pro.mvc.controller
 */

/**
 * O gerenciador de Controllers permite que descubramos qual o
 * Controller que consegue manipular determinada requisição do
 * usuário.
 * @author	João Batista Neto */
class ControllerManager {
	/**
	 * Instância única do ControllerManager para evitar que, em
	 * outros pontos da aplicação, tenhamos instâncias redundantes.
	 * @var	ControllerManager */
	private static $instance;

	/**
	 * Lista de Controllers anexados ao gerenciador.
	 * @var	array
	 */
	private $controllers;
        
        /**
         * Id do controller "PAI" (o que está em uso)
         * @var type int */
        private $controllerCurrentParentId;
        
        /**
         * Id das "crianças" do controller que está em uso.
         * @var type array */
        private $controllerCurrentChildId;

	private function __construct(){
            $this->controllers                  = array();
            $this->controllerCurrentParentId    = null;
            $this->controllerCurrentChildId     = array(); }

	/**
	 * Adiciona um Controller ao ControllerManager.
	 * @param	Controller $controller
	 * @return	boolean TRUE se o Controller tiver sido anexado ao
	 * 			gerenciador.
	 */
	public function addController(Controller $controller) {
            $exists = false;
            
            // Verificando duplicatas
            foreach ($this->controllers as $c){
                if ($c == $controller) {
                    $exists = true;
                    break;
                }
            }

            if (!$exists){
                $this->controllers[] = $controller;
                
                /**
                 * Adiciono os ID's dos "filhos" do controller atual
                 * em uma array, assim posso controla-las mais tarde
                 */
                if($this->controllerCurrentParentId != null){
                    $controllerLastIndex                = array_keys($this->controllers);
                    $controllerLastIndex                = end($controllerLastIndex);
                    $this->controllerCurrentChildId[]   = $controllerLastIndex;
                }
                
                return true;
            }
            
            return false;
	}

	/**
	 * Recupera a instância do ControllerManager.
	 * @return	ControllerManager
	 */
	public static function getInstance() {
            if(self::$instance == null){ self::$instance = new ControllerManager(); }
            return self::$instance;
	}

	/**
	 * Identifica o Controller adequado para manipular a requisição do
	 * usuário e delega a responsabilidade de manipulação caso encontrado.
	 * @throws		RuntimeException Caso nenhum Controller saiba como
	 * manipular a requisição do usuário. */
	public function handle() {
            
            /**
             * Se estiver manipulando algum controller, faço o handle
             * nos "filhos". Caso contrário, manipula os controllers
             * "pais".
             */
            if($this->controllerCurrentParentId != null){
                foreach($this->controllerCurrentChildId as $childId){
                    $controllersArray[] = $this->controllers[$childId];
                }
            } 
            else {
                $controllersArray = $this->controllers;
            }

            $found = false;

            foreach ($controllersArray as $offset => $controller) {
                if ($controller->canHandle()){
                    $this->controllerCurrentParentId = $offset;
                    $controller->handle();
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                require('view/outros/controllerNotFound.php');
            }
	}
        
        /**
	 * Remove um Controller do ControllerManager.
	 * @param	Controller $controller
	 * @return	boolean TRUE se o Controller tiver sido desanexado do
	 * 			gerenciador.
	 */
	public function removeController(Controller $controller) {
            foreach ($this->controllers as $offset => $c) {
                if ($c == $controller) {
                    unset($this->controllers[$offset]);
                    return true;
                }
            }

            return false;
	}
}

?>