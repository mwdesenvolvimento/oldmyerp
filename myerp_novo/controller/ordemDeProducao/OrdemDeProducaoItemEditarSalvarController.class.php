<?php

    chdir('../..');
    include('Config.php');

    $item = new OrdemDeProducaoItem();
    $item->id                           = filter_input(INPUT_POST, 'id');
    $item->produtoNome                  = filter_input(INPUT_POST, 'produtoNome');
    $item->produtoLote                  = filter_input(INPUT_POST, 'produtoLote');
    $item->operador                     = filter_input(INPUT_POST, 'operador');
    $item->produtoQuantidade            = filter_input(INPUT_POST, 'produtoQuantidade');
    $item->produtoVerniz                = filter_input(INPUT_POST, 'produtoVerniz');
    $item->produtoVernizQualidade       = filter_input(INPUT_POST, 'produtoVernizQualidade');
    $item->produtoClicheCodigo          = filter_input(INPUT_POST, 'produtoClicheCodigo');
    $item->produtoPapelId               = filter_input(INPUT_POST, 'produtoPapelId');
    $item->produtoPapelLargura          = filter_input(INPUT_POST, 'produtoPapelLargura');
    $item->produtoPapelComprimento      = filter_input(INPUT_POST, 'produtoPapelComprimento');
    $item->produtoPapelAcrescimo        = filter_input(INPUT_POST, 'produtoPapelAcrescimo');
    $item->produtoPapelComprimentoFinal = filter_input(INPUT_POST, 'produtoPapelComprimentoFinal');
    $item->produtoPapelQuantidadeM2     = filter_input(INPUT_POST, 'produtoPapelQuantidadeM2');
    $item->produtoMaquinaId             = filter_input(INPUT_POST, 'produtoMaquinaId');
    $item->produtoMaquinaAcerto         = filter_input(INPUT_POST, 'produtoMaquinaAcerto');
    $item->produtoPapelProduzido        = filter_input(INPUT_POST, 'produtoPapelProduzido');
    $item->produtoPapelRetorno          = filter_input(INPUT_POST, 'produtoPapelRetorno');
    $item->produtoPapelAcerto           = filter_input(INPUT_POST, 'produtoPapelAcerto');
    $item->produtoPapelEntregue         = filter_input(INPUT_POST, 'produtoPapelEntregue');
    $item->produtoPapelPerdido          = filter_input(INPUT_POST, 'produtoPapelPerdido');
    $item->produtoQuantidadePerdida     = filter_input(INPUT_POST, 'produtoQuantidadePerdida');
    $item->produtoTotalEtiquetas        = filter_input(INPUT_POST, 'produtoTotalEtiquetas');
        
    if($item->update()){
        echo "Alterado com sucesso";
    } else {
        echo "Erro ao gravar as informações no banco de dados <ItemEditarSalvar>";
    }