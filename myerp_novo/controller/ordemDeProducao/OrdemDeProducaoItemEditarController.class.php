<?php

class OrdemDeProducaoItemEditarController extends OrdemDeProducaoController {
    
	/**
	 * @return	boolean
	 * @see		Controller::canHandle()
	 */
	public function canHandle(){
            $submodulo = filter_input(INPUT_GET, 's');
            return (parent::canHandle()) && $submodulo == 'itemEditar';
	}

	/**
	 * @see		Controller::handle()
	 */
	public function handle(){
            $itemId         = filter_input(INPUT_GET, 'id');            
            $item           = new OrdemDeProducaoItem();
            $item->load($itemId);
            $item->setFormatarParaFormulario(true);
            $item->formatar();
            $maquinaId      = new SelectOptions($item, 'produtoMaquinaId');
            $papelId        = new SelectOptions($item, 'produtoPapelId', true, "tipo_id = 6");
            $componentes    = $item->getComponentes();
            require("./view/ordemDeProducao/ordemDeProducaoItemEditar.php");
        }
}

?>