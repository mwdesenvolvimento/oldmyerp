<?php

class OrdemDeProducaoListarFiltroController extends Filtro {
    
    public function __construct(){
        parent::__construct(new OrdemDeProducao());
        $this->carregarTipos();
    }
    
    public      $sessionName      = "ordemDeProducaoListarFiltro";
    protected   $submitButtonName = "btnFiltrar";
    protected   $clearButtonName  = "btnLimparFiltro";
    
    public function carregarTipos(){
        $this->setTipo('cadastroTimeStamp',     'BETWEEN');
        $this->setTipo('previsaoData',          'BETWEEN');
        $this->setTipo('conclusaoData',         'BETWEEN');
    }
    
    public function getEstrangeiroLista($estrangeiroChave, $estrangeiroCampo = 'nome'){
        $ordemDeProducao = new OrdemDeProducao();
        return $ordemDeProducao->getEstrangeiroLista($estrangeiroChave, $estrangeiroCampo);
    }
    
}

?>