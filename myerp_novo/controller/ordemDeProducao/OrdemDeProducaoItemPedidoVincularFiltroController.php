<?php

    chdir('../..');
    include('Config.php');

    $form           = filter_input_array(INPUT_POST);
    $pedidoId       = $form['pedidoId'];
    $clienteNome    = $form['clienteNome'];
    $produtoNome    = $form['produtoNome'];
    
    $where          = array();

    if($pedidoId){
        array_push($where, "pedidoData = '$pedidoId'");
    }

    if($clienteNome){
        array_push($where, "clienteNome LIKE '%$clienteNome%'");
    }

    if($produtoNome){
        array_push($where, "itemDescricao = '$produtoNome'");
    }

    $where = implode(" AND ", $where);
    
    if($where){
        
        $pedidoRepository   = new Repository(new Pedido);
        $pedidoRepository   ->setJoin("LEFT JOIN vwOrdemDeProducaoItem ON vwPedidoShow.itemId = vwOrdemDeProducaoItem.itemId");
        $pedidoRepository   ->setWhere("vwOrdemDeProducaoItem.id is null and $where");
        $pedidoLista        = $pedidoRepository->listar(); 
        foreach($pedidoLista as $registro){ 
            $registro->formatar(); ?>
            <tr>
                <td class="text-center"><?=$registro->pedidoData?></td>
                <td class="text-center"><?=$registro->id?></td>
                <td><?=$registro->clienteNome?></td>
                <td><?=$registro->itemDescricao?></td>
                <td><?=$registro->itemQuantidade?></td>
                <td><input type="checkbox" name="ItemId[]" value="<?=$registro->itemId;?>"></td>
            </tr>
    <?  }
        
    }