<?php

    chdir('../..');
    include('Config.php');
    
    $ordemDeProducao = new OrdemDeProducao();

    //print_r($_POST);

    $ordemDeProducao->aberturaData      = filter_input(INPUT_POST, 'aberturaData');
    $ordemDeProducao->previsaoData      = filter_input(INPUT_POST, 'previsaoData');
    $ordemDeProducao->conclusaoData     = filter_input(INPUT_POST, 'conclusaoData');
    $ordemDeProducao->statusId          = filter_input(INPUT_POST, 'statusId');
    $ordemDeProducao->prioridadeId      = filter_input(INPUT_POST, 'prioridadeId');
    $ordemDeProducao->impressaoLiberada = filter_input(INPUT_POST, 'impressaoLiberada');
    $ordemDeProducao->observacao        = filter_input(INPUT_POST, 'observacao');

    if(filter_input(INPUT_POST, 'id')){
        $ordemDeProducao->id = filter_input(INPUT_POST, 'id');
        if($ordemDeProducao->update()){ echo $ordemDeProducao->id; }
    }
    else{
        if($ordemDeProducao->insert()){
            echo $ordemDeProducao->getLastInsertId();
        } else {
            echo "Erro ao gravar as informações no banco de dados <RegistroSalvar>";
        }
    }

