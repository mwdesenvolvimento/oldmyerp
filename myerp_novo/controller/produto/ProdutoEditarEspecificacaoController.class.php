<?php

class ProdutoEditarEspecificacaoController extends ProdutoEditarController {
    
	/**
	 * @return	boolean
	 * @see		Controller::canHandle()
	 */
	public function canHandle() {
            $variacao   = filter_input(INPUT_GET, 'v');
            return (parent::canHandle()) && $variacao == 'especificacao';
	}

	/**
	 * @see		Controller::handle()
	 */
	public function handle() {
            $produtoId  = filter_input(INPUT_GET, 'produtoId');
            $produto    = new Produto();
            if(!$produto->load($produtoId)){ echo 'produto não encontrado'; }
            $produto->setFormatarParaFormulario(true);
            $produto->formatar();
            
            //O QUARTO PARAMETRO É PROVISORIO /09/04/15/
            $especificacaoMaquinaId     = new SelectOptions($produto, 'maquinaId');
            $especificacaoPapelId       = new SelectOptions($produto, 'papelId', true, "tipo_id = 6");
            $especificacaoRoloId        = new SelectOptions($produto, 'roloId');
            $rebobinamentoTipoId        = new SelectOptions($produto, 'rebobinamentoTipoId');
            $rebobinamentoMaquinaId     = new SelectOptions($produto, 'rebobinamentoMaquinaId');
            #$embalagemCaixaId           = new SelectOptions($produto, 'embalagemCaixaId', "fldTipo_Id = 6");
            #$embalagemEnchimentoId      = new SelectOptions($produto, 'embalagemEnchimentoId', "fldTipo_Id = 6");
            $embalagemTransportadorId   = new SelectOptions($produto, 'embalagemTransportadorId');
            require('./view/produto/produtoEditarEspecificacao.php');
	}
}

?>