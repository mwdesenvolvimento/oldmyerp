<?php

    chdir('../..');
    include('Config.php');
    
    $especificacao = new ProdutoEspecificacao();
    $especificacao->produtoId = filter_input(INPUT_POST, 'produtoId');

    /* verifico se existe na tabela ProdutoEspecificacao o produto,
     * caso não tenha, crio, com valores nulos */
    $produtoEspecificacao = new ProdutoEspecificacao();
    if(!$produtoEspecificacao->load($especificacao->produtoId)){
    $produtoEspecificacao->produtoId = $especificacao->produtoId;
    $produtoEspecificacao->insert(); }
        
    //continuo preparando os dados para o update
    $especificacao->maquinaId                               = filter_input(INPUT_POST, 'especificacaoMaquinaId');
    $especificacao->papelId                                 = filter_input(INPUT_POST, 'especificacaoPapelId');
    $especificacao->largura                                 = filter_input(INPUT_POST, 'especificacaoLargura');
    $especificacao->altura                                  = filter_input(INPUT_POST, 'especificacaoAltura');
    $especificacao->portaClicheNumero                       = filter_input(INPUT_POST, 'especificacaoPortaClicheNumero');
    $especificacao->papelLargura                            = filter_input(INPUT_POST, 'especificacaoPapelLargura');
    $especificacao->papelMaquinaQtd                         = filter_input(INPUT_POST, 'especificacaoPapelMaquinaQtd');
    $especificacao->regua                                   = filter_input(INPUT_POST, 'especificacaoRegua');
    $especificacao->papelCalculoTamanho                     = filter_input(INPUT_POST, 'especificacaoPapelCalculoTamanho');
    $especificacao->maquinaVelocidade                       = filter_input(INPUT_POST, 'especificacaoMaquinaVelocidade');
    $especificacao->facaNumero                              = filter_input(INPUT_POST, 'especificacaoFacaNumero');
    $especificacao->carreiraNumero                          = filter_input(INPUT_POST, 'especificacaoCarreiraNumero');
    $especificacao->clicheRepeticao                         = filter_input(INPUT_POST, 'especificacaoClicheRepeticao');
    $especificacao->clicheCodigo                            = filter_input(INPUT_POST, 'especificacaoClicheCodigo');
    $especificacao->maquinaAcerto                           = filter_input(INPUT_POST, 'especificacaoMaquinaAcerto');
    $especificacao->producaoTempo                           = filter_input(INPUT_POST, 'especificacaoProducaoTempo');
    $especificacao->roloId                                  = filter_input(INPUT_POST, 'especificacaoRoloId');
    $especificacao->verniz                                  = filter_input(INPUT_POST, 'especificacaoVerniz');
    $especificacao->vernizQualidade                         = filter_input(INPUT_POST, 'especificacaoVernizQualidade');
    $especificacao->etiquetaRoloQtd                         = filter_input(INPUT_POST, 'especificacaoEtiquetaRoloQtd');
    $especificacao->etiquetaCortada                         = filter_input(INPUT_POST, 'especificacaoEtiquetaCortada');
    $especificacao->acertoTempo                             = filter_input(INPUT_POST, 'especificacaoAcertoTempo');
    $especificacao->operador                                = filter_input(INPUT_POST, 'especificacaoOperador');
    $especificacao->rebobinamentoTipoId                     = filter_input(INPUT_POST, 'rebobinamentoTipoId');
    $especificacao->rebobinamentoMaquinaId                  = filter_input(INPUT_POST, 'rebobinamentoMaquinaId');
    $especificacao->rebobinamentoPapelLargura               = filter_input(INPUT_POST, 'rebobinamentoPapelLargura');
    $especificacao->rebobinamentoCarreiraNumero             = filter_input(INPUT_POST, 'rebobinamentoCarreiraNumero');
    $especificacao->rebobinamentoMaquinaVelocidade          = filter_input(INPUT_POST, 'rebobinamentoMaquinaVelocidade');
    $especificacao->rebobinamentoFacaNumero                 = filter_input(INPUT_POST, 'rebobinamentoFacaNumero');
    $especificacao->rebobinamentoMaquinaAcerto              = filter_input(INPUT_POST, 'rebobinamentoMaquinaAcerto');
    $especificacao->rebobinamentoTubete                     = filter_input(INPUT_POST, 'rebobinamentoTubete');
    $especificacao->rebobinamentoRoloDiametro               = filter_input(INPUT_POST, 'rebobinamentoRoloDiametro');
    $especificacao->rebobinamentoRoloMetroQuantidade        = filter_input(INPUT_POST, 'rebobinamentoRoloMetroQuantidade');
    $especificacao->rebobinamentoRoloPlastificado           = filter_input(INPUT_POST, 'rebobinamentoRoloPlastificado');
    $especificacao->rebobinamentoEtiquetaMetroQuantidade    = filter_input(INPUT_POST, 'rebobinamentoEtiquetaMetroQuantidade');
    $especificacao->rebobinamentoIr                         = filter_input(INPUT_POST, 'rebobinamentoIr');
    $especificacao->rebobinamentoCa                         = filter_input(INPUT_POST, 'rebobinamentoCa');
    $especificacao->rebobinamentoTempo                      = filter_input(INPUT_POST, 'rebobinamentoTempo');
    $especificacao->rebobinamentoData                       = filter_input(INPUT_POST, 'rebobinamentoData');
    $especificacao->rebobinamentoObservacao                 = filter_input(INPUT_POST, 'rebobinamentoObservacao');
    $especificacao->embalagemRoloDiametro                   = filter_input(INPUT_POST, 'embalagemRoloDiametro');
    $especificacao->embalagemRoloAltura                     = filter_input(INPUT_POST, 'embalagemRoloAltura');
    $especificacao->embalagemRoloPlastificado               = filter_input(INPUT_POST, 'embalagemRoloPlastificado');
    $especificacao->embalagemCaixaId                        = filter_input(INPUT_POST, 'embalagemCaixaId');
    $especificacao->embalagemTransportadorId                = filter_input(INPUT_POST, 'embalagemTransportadorId');
    $especificacao->embalagemQuantidadeExata                = filter_input(INPUT_POST, 'embalagemQuantidadeExata');
    $especificacao->embalagemEnchimentoId                   = filter_input(INPUT_POST, 'embalagemEnchimentoId');
    $especificacao->embalagemData                           = filter_input(INPUT_POST, 'embalagemData');
    $especificacao->embalagemObservacao                     = filter_input(INPUT_POST, 'embalagemObservacao');

    if($especificacao->update("produtoId = '{$especificacao->produtoId}'")){
        echo "Alterado com sucesso";
    } else {
        echo "Erro ao gravar as informações no banco de dados <ProdutoEspecificacao>";
    }