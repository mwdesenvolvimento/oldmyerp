<div class="modal fade modalVazio">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header small clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body"></div>
        </div>
    </div>
</div>