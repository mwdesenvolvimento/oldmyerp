<?
    /*
     * RECEBO SEMPRE $ordemDeProducaoLista;
     */
?>

<script>
    $(document).ready(function(){
        $('#ordemDeProducaoLista').clickable('index.php?m=ordemDeProducao&s=editar&id=#');
    });
</script>

<div class="container-fluid" style="padding-bottom:40px; margin-top:10px">
        <div class="row">
            <!--<div class="col-lg-3 hidden-md hidden-sm">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        Painel
                    </div>
                    <div class="panel-body hidden-md hidden-sm" style="background: #fcfcfc">
                    </div>
                </div>
            </div>-->
            
            <? require('./view/ordemDeProducao/ordemDeProducaoListarFiltro.php'); ?>
            
            <form id="frmOrdemDeProducao" method="POST">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Lista: Ordem de Produção
                        </div>

                        <div class="table-responsive">
                            <table id="ordemDeProducaoLista" class="table table-bordered table-striped table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Entrega</th>
                                        <th>Abertura</th>
                                        <th>Conclusão</th>
                                        <th>Status</th>
                                        <th>Prioridade</th>
                                        <th>Pedido Id</th>
                                        <th>Produto Id</th>
                                        <th>Descrição</th>
                                        <th>Quantidade</th>
                                        <th>Cliente</th>
                                        <th>Máquina</th>
                                        <th style="width:10px"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                <? foreach($ordemDeProducaoLista as $registro){ 
                                    $registro->formatar(); ?>
                                    <tr id="<?=$registro->id;?>">
                                        <td><?=$registro->id?></td>
                                        <td><?=$registro->previsaoData?></td>
                                        <td><?=$registro->aberturaData?></td>
                                        <td><?=$registro->conclusaoData?></td>
                                        <td><?=$registro->statusNome?></td>
                                        <td><?=$registro->prioridadeNome?></td>
                                        <td><?=$registro->itemPedidoId?></td>
                                        <td><?=$registro->itemProdutoId?></td>
                                        <td><?=$registro->itemDescricao?></td>
                                        <td><?=$registro->itemQuantidade?></td>
                                        <td><?=$registro->itemPedidoCliente?></td>
                                        <td><?=$registro->itemMaquina?></td>
                                        <td><input name="OpId[]" type="checkbox" value="<?=$registro->id?>"></td>
                                    </tr>
                                    <? } ?>
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->

                        <!-- AINDA NAO VAMOS TER PAGINAÇÃO
                        <div class="panel-footer text-center" style="cursor: pointer;">
                            <span class="glyphicon glyphicon-refresh"></span>
                            Carregar mais resultados
                        </div>-->
                    </div>
                </div>
            </form>
        </div>
        
        <nav id="footer" class="navbar navbar-default navbar-fixed-bottom">
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-danger" id="btnExcluirSelecionados">
                        <span class="glyphicon glyphicon-remove-sign"></span>
                        Remover selecionado
                    </button>
                </div>

                <div class="btn-group">
                    <a href="index.php?m=ordemDeProducao&s=novo" role="button" class="btn btn-success">
                        <span class="glyphicon glyphicon-plus-sign"></span>
                        Nova Ordem de Produção
                    </a>
                </div>
            </div>
        </nav>
    </div>

    <script>
        <!-- Excluir os itens selecionados -->
        $("#btnExcluirSelecionados").click(function(){
            $.post("controller/ordemDeProducao/OrdemDeProducaoExcluirController.php", $("#frmOrdemDeProducao").serialize())
            .done(function(){
                alert("Ordens de produção excluidas com sucesso");
                location.href = "index.php?m=ordemDeProducao";
            });
        });
    </script>