<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:10px">
    <ul class="nav nav-pills pull-left">
        <li>
            <a href="../myerp_antigo/index.php"  style="background:orange !important; color:white">
                <span class="glyphicon glyphicon-arrow-left"></span>
                Voltar para o sistema antigo
            </a>
        </li>
    </ul>
</div>

<div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom:10px">
    <hr />
</div>

<div class="container-fluid" style="padding-bottom:40px; margin-top:10px">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">Histórico de produção: Inserir registro</h4>
                </div>

                <div class="panel-body">
                    <form id="frmOrdemDeProducaoHistorico" method="post" action="index.php?m=ordemDeProducaoHistorico&modo=inserir">
                        <div class="page-header col-lg-12 col-md-12 col-sm-12">
                            <h5 class="pull-left">Informações da Ordem de Produção</h5>
                        </div>

                        <div class="form-group col-lg-1 col-md-2 col-sm-2">
                            <label for="opId" class="small">Id</label>
                            <input value="<?=$ordemDeProducao->id?>" type="text" class="form-control input-sm" id="opId" name="opId" readonly>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label class="small">Status</label>
                            <input value="<?=$ordemDeProducao->statusNome?>" type="text" class="form-control input-sm" disabled>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label class="small">Prioridade</label>
                            <input value="<?=$ordemDeProducao->prioridadeNome?>" type="text" class="form-control input-sm" disabled>
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label class="small">Observacao</label>
                            <textarea disabled class="form-control"><?=$ordemDeProducao->observacao?></textarea>
                        </div>

                        <div class="page-header col-lg-12 col-md-12 col-sm-12">
                            <h5 class="pull-left">Informações do Funcionário</h5>
                        </div>

                        <div class="form-group col-lg-1 col-md-2 col-sm-2">
                            <label for="funcionarioId" class="small">Id</label>
                            <input value="<?=$funcionario->id?>" type="text" class="form-control input-sm" id="funcionarioId" name="funcionarioId" readonly>
                        </div>

                        <div class="form-group col-lg-11 col-md-10 col-sm-10">
                            <label class="small">Nome</label>
                            <input value="<?=$funcionario->nome?>" type="text" class="form-control input-sm" disabled>
                        </div>

                        <div class="page-header col-lg-12 col-md-12 col-sm-12">
                            <h5 class="pull-left">Histórico</h5>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="historicoData" class="small">Data atual</label>
                            <input value="<?=$dataAtual;?>" type="text" class="form-control input-sm" id="historicoData" name="historicoData" readonly>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="historicoStatus" class="small">Status</label>
                            <select class="form-control input-sm" name="historicoStatus" id="historicoStatus">
                                <option>Em andamento</option>
                                <option>Em pausa</option>
                                <option>Finalizado</option>
                            </select>
                            <a href="#" class="pull-right">ver histórico</a>
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label for="historicoDescricao" class="small">Descrição</label>
                            <textarea class="form-control" rows="5" name="historicoDescricao" id="historicoDescricao"></textarea>
                        </div>

                        <div class="pull-right col-lg-12 col-md-12 col-sm-12">
                            <div class="btn-group pull-right">
                                <a class="btn btn-danger" href="index.php?m=ordemDeProducaoHistorico">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    Cancelar
                                </a>
                                
                                <button type="submit" class="btn btn-success" id="btnOrdemDeProducaoHistoricoSalvar">
                                    <span class="glyphicon glyphicon-ok"></span>
                                    Salvar informações
                                </button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    <!-- Enviar o form para salvar por Ajax -->
    $( "#btnOrdemDeProducaoHistoricoSalvar" ).click(function() {
        $.post( "controller/ordemDeProducao/historico/OrdemDeProducaoHistoricoSalvarController.php", $("#frmOrdemDeProducaoHistorico").serialize())
        .done(function() {
            alert("Registro salvo com sucesso");
        });
    });
    
</script>