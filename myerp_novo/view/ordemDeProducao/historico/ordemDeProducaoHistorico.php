<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:10px">
    <ul class="nav nav-pills pull-left">
        <li>
            <a href="../myerp_antigo/index.php"  style="background:orange !important; color:white">
                <span class="glyphicon glyphicon-arrow-left"></span>
                Voltar para o sistema antigo
            </a>
        </li>
    </ul>
</div>

<div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom:10px">
    <hr />
</div>

<div class="container-fluid" style="padding-bottom:40px; margin-top:10px">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">Histórico de produção: Inserir registro</h4>
                </div>

                <div class="panel-body">
                    
                    <form id="frmOrdemDeProducaoItemEditar" method="post" action="index.php?m=ordemDeProducaoHistorico&modo=inserir">
                        <div class="form-group pull-left">
                            <label for="opId" class="small">ID da Ordem de Produção</label>
                            <div style="display: block;">
                                <input style="width: 100px; float:left" value="" type="text" class="form-control text-center" id="opId" name="opId">
                                <a href="#">
                                    <img src="./view/ordemDeProducao/historico/img/search.png" style="float:left; margin:1px 0 0 3px">
                                </a>
                            </div>
                        </div>

                        <div class="form-group pull-left" style="margin-left:30px; width:calc(100% - 176px);">
                            <label for="funcionarioId" class="small">Cód. do Funcionário</label>
                            <div style="display: block;">
                                <input style="width:60px; float:left" value="" type="text" class="form-control text-center" id="funcionarioId" name="funcionarioId">
                                <a href="#"><img src="./view/ordemDeProducao/historico/img/search.png" style="float:left; margin:1px 0 0 3px"></a>
                                <input style="width:calc(100% - 110px); float:right" value="" type="text" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-success" id="btnOrdemDeProducaoItemSalvar">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    Prosseguir
                                </button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>