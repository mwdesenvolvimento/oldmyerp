    <div class="container-fluid" style="padding-bottom:40px; margin-top:10px">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">Ver ordem de produção</h4>
                    </div>

                    <div class="panel-body">
                        <form id="frmOrdemDeProducaoRegistro" method="post">
                            <div class="form-group col-lg-1 col-md-1 col-sm-1">
                                <label for="id" class="small">Id</label>
                                <input type="text" class="form-control input-sm" id="id" name="id" value="<?=$ordemDeProducao->id?>" readonly>
                            </div>
                             
                            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                                <label for="aberturaData" class="small">Data de abertura</label>
                                <input type="text" class="form-control input-sm" id="aberturaData" name="aberturaData" value="<?=$ordemDeProducao->aberturaData?>">
                            </div>

                            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                                <label for="prioridadeId" class="small">Prioridade</label>
                                <select class="form-control input-sm" id="prioridadeId" name="prioridadeId">
                                    <?=$selOptPrioridade?>
                                </select>
                            </div>
                             
                            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                                <label for="previsaoData" class="small">Previsão de entrega</label>
                                <input type="text" class="form-control input-sm" id="previsaoData" name="previsaoData" value="<?=$ordemDeProducao->previsaoData?>">
                            </div>

                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="statusId" class="small">Status</label>
                                <select class="form-control input-sm" id="statusId" name="statusId">
                                    <?=$selOptStatus?>
                                </select>
                            </div>

                            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                                <label for="conclusaoData" class="small">Data de conclusão</label>
                                <input type="text" class="form-control input-sm" id="conclusaoData" name="conclusaoData" value="<?=$ordemDeProducao->conclusaoData?>">
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                <label for="observacao" class="small">Observação</label>
                                <textarea class="form-control input-sm" rows="7" style="resize: none" id="observacao" name="observacao"><?=$ordemDeProducao->observacao?></textarea>
                            </div>

                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <div class="checkbox">
                                    <label for="impressaoLiberada">
                                        <?=$chkImpressaoLiberada?>
                                        Liberado para impressão
                                    </label>
                                </div>
                            </div>
                        </form>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 clearfix">
                            <hr />
                        </div>

                        <script>
                            $(document).ready(function(){
                                $('#ordemDeProducaoItens').clickable('index.php?m=ordemDeProducao&s=itemEditar&id=#', 'modal');
                            });
                        </script>

                        <form id="frmOrdemDeProducaoItem" method="POST">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Itens
                                    </div>

                                    <div class="table-responsive">
                                        <table id="ordemDeProducaoItens" class="table table-bordered table-striped table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width:100px">ID do Pedido</th>
                                                    <th>Produto</th>
                                                    <th style="width:130px" class="text-center">Quantidade</th>
                                                    <th style="width:10px"></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            <?  foreach($ordemDeProducaoItens as $item){ 
                                                $item->formatar(); ?>
                                                <tr id="<?=$item->id;?>">
                                                    <td><?=$item->pedidoId;?></td>
                                                    <td><?=$item->produtoNome;?></td>
                                                    <td class="text-center"><?=$item->produtoQuantidade;?></td>
                                                    <td><input name="ItemId[]" type="checkbox" value="<?=$item->id;?>"></td>
                                                </tr>
                                            <?  } ?>
                                            </tbody>
                                        </table>
                                    </div><!-- /.table-responsive -->
                                </div>
                            </div>
                        </form>

                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger btn-sm" id="btnItemExcluirSelecionados">
                                        <span class="glyphicon glyphicon-remove"></span>
                                        Excluir selecionados
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <nav id="footer" class="navbar navbar-default navbar-fixed-bottom">
            <div class="pull-right">
                
                <div class="btn-group">
                    <button type="button" class="btn btn-default" id="btnOrdemDeProducaoPedidoVincular">
                        <span class="glyphicon glyphicon-import"></span>
                        Vincular itens de pedidos
                    </button>
                </div>
                
                <div class="btn-group">
                    <a rel="button" class="btn btn-info" id="btnOrdemDeProducaoRelatorio" href="./report/ordemDeProducaoRegistroReport.php?id=<?=$ordemDeProducao->id?>" target="_blank">
                        <span class="glyphicon glyphicon-print"></span>
                        Imprimir relatório
                    </a>
                </div>

                <div class="btn-group">
                    <button type="button" class="btn btn-success" id="btnOrdemDeProducaoSalvar">
                        <span class="glyphicon glyphicon-ok"></span>
                        Salvar
                    </button>
                </div>
            </div>
        </nav>
    </div>

    <script>
        <!--Exibir itens de pedidos para vincular na Ordem de Produção-->
        $( "#btnOrdemDeProducaoPedidoVincular").click(function(){
            loadModal('index.php?m=ordemDeProducao&s=ItemPedidoVincular');
        });
        
        
        <!-- Enviar o form para salvar por Ajax -->
        $( "#btnOrdemDeProducaoSalvar" ).click(function() {
            $.post( "controller/ordemDeProducao/OrdemDeProducaoRegistroSalvarController.php", $("#frmOrdemDeProducaoRegistro").serialize())
            .done(function() {
                alert("Registro salvo com sucesso");
            });
        });
        
        <!-- Excluir os itens selecionados -->
        $("#btnItemExcluirSelecionados").click(function(){
            $.post("controller/ordemDeProducao/OrdemDeProducaoRegistroSalvarController.php", $("#frmOrdemDeProducaoRegistro").serialize())
            .done(function(data) {
                //excluir itens da OP
                $.post("controller/ordemDeProducao/OrdemDeProducaoItemExcluirController.php", $("#frmOrdemDeProducaoItem").serialize())
                .done(function(data){
                    alert("Itens excluidos com sucesso");
                    location.href = "index.php?m=ordemDeProducao&s=editar&id=<?=$ordemDeProducao->id?>";
                });
            });
        });
    </script>
    