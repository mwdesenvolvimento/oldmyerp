<?
    $filtro = $ordemDeProducaoListarFiltro;
?>

    <!--<div class="col-lg-12 col-md-12 col-sm-12" style="margin-bottom:15px">
        <div class="btn-group pull-left" style="width:calc(100% - 160px)">
            <input type="text" placeholder="Pesquisa rápida..." class="form-control input-sm">
        </div>

        <div class="btn-group pull-right" style="width:149px">
            <button type="button" class="btn btn-warning btn-sm" data-toggle="collapse" 
                    href="#filtroAvancado" aria-expanded="false" aria-controls="collapseExample">
                <span class="glyphicon glyphicon-cog"></span>
                pesquisa avançada
            </button>
        </div>
    </div>-->

    <div class="col-lg-12 col-md-12 col-sm-12" id="filtroAvancado">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-search"></span>
                Filtro avançado
            </div>
            
            <form id="frmOrdemDeProducaoListarFiltro" method="post">
                <div class="panel-body">
                    <div class="form-group col-lg-1 col-md-2 col-sm-2">
                        <label for="id" class="small">Id</label>
                        <input value="<?=$filtro->id?>" type="text" class="form-control input-sm" id="id" name="id">
                    </div>

                    <div class="form-group col-lg-3 col-md-5 col-sm-5">
                        <label for="prioridadeId" class="small">Prioridade</label>
                        <select class="form-control input-sm" name="prioridadeId" id="prioridadeId">
                            <?=$selOptPrioridade;?>
                        </select>
                    </div>

                    <div class="form-group col-lg-3 col-md-5 col-sm-5">
                        <label for="cadastroTimeStamp" class="small">Data de abertura</label>
                        <div class="form-inline">
                            <input value="<?=$filtro->cadastroTimeStamp[0]?>" type="text" style="width:49%" class="form-control input-sm" id="aberturaData" name="cadastroTimeStamp[]">
                            <input value="<?=$filtro->cadastroTimeStamp[1]?>" type="text" style="width:49%" class="form-control input-sm pull-right" name="cadastroTimeStamp[]">
                        </div>
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-4">
                        <label for="previsaoData" class="small">Data de previsão de conclusão</label>
                        <div class="form-inline">
                            <input value="<?=$filtro->previsaoData[0]?>" type="text" style="width:49%" class="form-control input-sm" id="previsaoData" name="previsaoData[]">
                            <input value="<?=$filtro->previsaoData[1]?>" type="text" style="width:49%" class="form-control input-sm pull-right" name="previsaoData[]">
                        </div>
                    </div>

                    <div class="form-group col-lg-2 col-md-3 col-sm-3">
                        <label for="impressaoLiberada" class="small">Liberado para impressão</label>
                        <select class="form-control input-sm" id="impressaoLiberada" name="impressaoLiberada">
                            <option></option>
                            <option value="1">Sim</option>
                            <option value="0">Não</option>
                        </select>
                    </div>


                    <div class="form-group col-lg-4 col-md-5 col-sm-5">
                        <label for="statusId" class="small">Status</label>
                        <select class="form-control input-sm" name="statusId" id="statusId">
                            <?=$selOptStatus;?>
                        </select>
                    </div>

                    <div class="form-group col-lg-3 col-md-4 col-sm-4">
                        <label for="conclusaoData" class="small">Data de conclusão</label>
                        <div class="form-inline">
                            <input value="<?=$filtro->conclusaoData[0]?>" type="text" style="width:49%" class="form-control input-sm" id="conclusaoData" name="conclusaoData[]">
                            <input value="<?=$filtro->conclusaoData[1]?>" type="text" style="width:49%" class="form-control input-sm pull-right" name="conclusaoData[]">
                        </div>
                    </div>
                </div>

                <div class="panel-footer clearfix">
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="submit" class="btn btn-danger" id="btnLimparFiltro" name="btnLimparFiltro">
                                <span class="glyphicon glyphicon-remove-sign"></span>
                                Limpar filtro
                            </button>
                        </div>
                        
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success" id="btnFiltrar" name="btnFiltrar">
                                <span class="glyphicon glyphicon-ok-sign"></span>
                                Filtrar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>