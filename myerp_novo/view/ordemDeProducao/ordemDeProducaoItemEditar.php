<div class="modal-title">
    Editando item: <?=$item->produtoNome;?>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <form id="frmOrdemDeProducaoItemEditar" method="post">
            
            <input type="hidden" name="id" id="id" value="<?=$item->id?>">
            
            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                <label for="pedidoId" class="small">ID do pedido</label>
                <input value="<?=$item->pedidoId?>" type="text" class="form-control input-sm" id="pedidoId" name="pedidoId" disabled>
            </div>
            
            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                <label for="produtoCodigo" class="small">Código do produto</label>
                <input value="<?=$item->produtoCodigo?>" type="text" class="form-control input-sm" id="produtoCodigo" name="produtoCodigo" disabled>
            </div>
            
            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                <label for="produtoNome" class="small">Nome</label>
                <input value="<?=$item->produtoNome?>" type="text" class="form-control input-sm" id="produtoNome" name="produtoNome">
            </div>
            
            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                <label for="produtoQuantidade" class="small">Quantidade</label>
                <input value="<?=$item->produtoQuantidade?>" type="text" class="form-control input-sm" id="produtoQuantidade" name="produtoQuantidade">
            </div>
            
            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                <label for="produtoVerniz" class="small">Verniz</label>
                <select id="produtoVerniz" name="produtoVerniz" class="form-control">
                    <option value=""></option>
                    <option <?=($item->produtoVerniz == '0') ? 'selected': ''?> value="0">Não</option>
                    <option <?=($item->produtoVerniz == '1') ? 'selected': ''?> value="1">Sim</option>
                </select>
            </div>

            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                <label for="produtoVernizQualidade" class="small">Qualidade do verniz</label>
                <select id="produtoVernizQualidade" name="produtoVernizQualidade" class="form-control">
                    <option value=""></option>
                    <option <?=($item->produtoVernizQualidade == '1') ? 'selected': ''?> value="1">Alta</option>
                    <option <?=($item->produtoVernizQualidade == '0') ? 'selected': ''?> value="0">Baixa</option>
                </select>
            </div>
            
            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                <label for="produtoClicheCodigo" class="small">Código do clichê</label>
                <input value="<?=$item->produtoClicheCodigo?>" type="text" class="form-control input-sm" id="produtoClicheCodigo" name="produtoClicheCodigo">
            </div>
            
            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                <label for="produtoLote" class="small">Lote</label>
                <input value="<?=$item->produtoLote?>" type="text" class="form-control input-sm" id="produtoLote" name="produtoLote">
            </div>

            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                <label for="operador" class="small">Operador</label>
                <input value="<?=$item->operador?>" type="text" class="form-control input-sm" id="operador" name="operador">
            </div>
            
            <div class="page-header col-lg-12 col-md-12 col-sm-12">
                <h5>Papel <small>cálculos das quantidades de papel</small></h5>
            </div>

            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                <label for="produtoPapelId" class="small">Papel</label>
                <select class="form-control input-sm" id="produtoPapelId" name="produtoPapelId">
                    <?=$papelId?>
                </select>
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoPapelLargura" class="small">Largura do papel (cm)</label>
                <input value="<?=$item->produtoPapelLargura?>" type="text" class="form-control input-sm" id="produtoPapelLargura" name="produtoPapelLargura">
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoPapelComprimento" class="small">Comprimento do papel (m)</label>
                <input value="<?=$item->produtoPapelComprimento?>" type="text" class="form-control input-sm" id="produtoPapelComprimento" name="produtoPapelComprimento">
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoPapelAcrescimo" class="small">Acréscimo de papel (%)</label>
                <input value="<?=$item->produtoPapelAcrescimo?>" type="text" class="form-control input-sm" id="produtoPapelAcrescimo" name="produtoPapelAcrescimo">
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoPapelComprimentoFinal" class="small">Comprimento Final (m)</label>
                <input value="<?=$item->produtoPapelComprimentoFinal?>" type="text" class="form-control input-sm" id="produtoPapelComprimentoFinal" name="produtoPapelComprimentoFinal">
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoPapelQuantidadeM2" class="small">Quantidade (m²)</label>
                <input value="<?=$item->produtoPapelQuantidadeM2?>" type="text" class="form-control input-sm" id="produtoPapelQuantidadeM2" name="produtoPapelQuantidadeM2">
            </div>
            
            <div class="page-header col-lg-12 col-md-12 col-sm-12">
                <h5>Produção <small>relatório de produção</small></h5>
            </div>    
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoMaquinaId" class="small">Maquina</label>
                <select class="form-control input-sm" id="produtoMaquinaId" name="produtoMaquinaId">
                    <?=$maquinaId?>
                </select>
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoMaquinaAcerto" class="small">Acerto</label>
                <input value="<?=$item->produtoMaquinaAcerto?>" type="text" class="form-control input-sm" id="produtoMaquinaAcerto" name="produtoMaquinaAcerto">
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoPapelProduzido" class="small">Papel produzido</label>
                <input value="<?=$item->produtoPapelProduzido?>" type="text" class="form-control input-sm" id="produtoPapelProduzido" name="produtoPapelProduzido">
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoPapelRetorno" class="small">Retorno de papel</label>
                <input value="<?=$item->produtoPapelRetorno?>" type="text" class="form-control input-sm" id="produtoPapelRetorno" name="produtoPapelRetorno">
            </div>
            
            <div class="page-header col-lg-12 col-md-12 col-sm-12">
                <h5>Almoxarifado <small>relatório de produção</small></h5>
            </div> 
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoPapelAcerto" class="small">Acerto de papel</label>
                <input value="<?=$item->produtoPapelAcerto?>" type="text" class="form-control input-sm" id="produtoPapelAcerto" name="produtoPapelAcerto">
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoPapelEntregue" class="small">Papel entregue</label>
                <input value="<?=$item->produtoPapelEntregue?>" type="text" class="form-control input-sm" id="produtoPapelEntregue" name="produtoPapelEntregue">
            </div>
            
            <div class="page-header col-lg-12 col-md-12 col-sm-12">
                <h5>Rebobinamento <small>relatório de produção</small></h5>
            </div> 
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoPapelPerdido" class="small">Papel perdido</label>
                <input value="<?=$item->produtoPapelPerdido?>" type="text" class="form-control input-sm" id="produtoPapelPerdido" name="produtoPapelPerdido">
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoQuantidadePerdida" class="small">Quantidade perdida</label>
                <input value="<?=$item->produtoQuantidadePerdida?>" type="text" class="form-control input-sm" id="produtoQuantidadePerdida" name="produtoQuantidadePerdida">
            </div>
            
            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                <label for="produtoTotalEtiquetas" class="small">Total de etiquetas</label>
                <input value="<?=$item->produtoTotalEtiquetas?>" type="text" class="form-control input-sm" id="produtoTotalEtiquetas" name="produtoTotalEtiquetas">
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12">
                <hr>
            </div>            
            
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Componentes</div>

                <?  if($componentes){ ?>
                    <div class="table-responsive">
                        <table id="ordemDeProducaoItens" class="table table-hover table-striped">
                            <tbody>
                            <?  foreach($componentes as $componente){ ?>
                                <tr>
                                    <td><?=$componente->componenteNome?></td>
                                </tr>
                            <?  } ?>
                            </tbody>
                        </table>
                    </div>
                <?  } else { ?>
                    <div class="panel-body text-center">
                        Nenhum componente encontrado.
                    </div>
                <?  } ?>

                </div>
            </div> <!-- end div componentes -->
            
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger" id="btnOrdemDeProducaoItemCancelar" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove-sign"></span>
                            Cancelar
                        </button>
                    </div>

                    <div class="btn-group">
                        <button type="button" class="btn btn-success" id="btnOrdemDeProducaoItemSalvar">
                            <span class="glyphicon glyphicon-ok"></span>
                            Salvar
                        </button>
                    </div>
                </div>
            </div> 
        </form>
    </div>
</div>

<script>
    $(document).ready(function(){
        <!-- Enviar o form para salvar por Ajax -->
        $( "#btnOrdemDeProducaoItemSalvar" ).click(function() {
            $.post( "controller/ordemDeProducao/OrdemDeProducaoItemEditarSalvarController.class.php", $( "#frmOrdemDeProducaoItemEditar" ).serialize())
                .done(function(data) {
                alert(data);
            });
        }); 

        $('#produtoPapelAcrescimo, #produtoPapelComprimento').change(function(){
            var papel_comprimento   = $('#produtoPapelComprimento').val();
            papel_comprimento       = papel_comprimento.replace('.', '');
            papel_comprimento       = papel_comprimento.replace(',', '.');
            papel_comprimento       = parseFloat(papel_comprimento).toFixed(6);

            var acrescimo           = $('#produtoPapelAcrescimo').val();
            acrescimo               = acrescimo.replace('.', '');
            acrescimo               = acrescimo.replace(',', '.');
            acrescimo               = parseFloat(acrescimo).toFixed(6);
            acrescimo               = (papel_comprimento * (acrescimo / 100));

            var comp_final          = (parseFloat(papel_comprimento) + parseFloat(acrescimo)).toFixed(2);
            var comprimento_final   = comp_final.replace('.', ',');
            $('#produtoPapelComprimentoFinal').val(comprimento_final);
        });
    });
</script>