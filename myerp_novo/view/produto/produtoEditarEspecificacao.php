    <div class="container-fluid" style="padding-bottom:40px; margin-top:10px">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title"><?=$produto->nome?> - Especificações</h4>
                    </div>

                    <div class="panel-body">
                        <form id="frmProdutoEspecificacao" method="post">
                            
                            <input id="produtoId" name="produtoId" class="form-control" type="hidden" value="<?=$produto->id?>">

                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="especificacaoMaquinaId" class="small">Maquina</label>
                                <select id="especificacaoMaquinaId" name="especificacaoMaquinaId" class="form-control">
                                    <?=$especificacaoMaquinaId;?>
                                </select>
                            </div>
                            
                            <div class="form-group col-lg-3 col-md-4 col-sm-4">
                                <label for="especificacaoPapelId" class="small">Papel</label>
                                <select id="especificacaoPapelId" name="especificacaoPapelId" class="form-control">
                                    <?=$especificacaoPapelId;?>
                                </select>
                            </div>

                            <div class="form-group col-lg-3 col-md-4 col-sm-4">
                                <label for="especificacaoLargura" class="small pull-left" style="width:100%; margin-top:3px">
                                    Largura x Altura (cm)
                                </label>
                                <div class="pull-left" style="width:49%; margin-right:2%;">
                                    <input value="<?=$produto->largura?>" type="text" id="especificacaoLargura" name="especificacaoLargura" class="form-control">
                                </div>
                                <div class="pull-right" style="width:49%;">
                                    <input value="<?=$produto->altura?>" type="text" id="especificacaoAltura" name="especificacaoAltura" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoPortaClicheNumero" class="small">Nº do porta clichê</label>
                                <input value="<?=$produto->portaClicheNumero?>" type="text" id="especificacaoPortaClicheNumero" name="especificacaoPortaClicheNumero" class="form-control">
                            </div>

                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoPapelLargura" class="small">Largura do papel (cm)</label>
                                <input value="<?=$produto->papelLargura?>" type="text" id="especificacaoPapelLargura" name="especificacaoPapelLargura" class="form-control">
                            </div>

                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoPapelMaquinaQtd" class="small">Papel na máquina (cm)</label>
                                <input value="<?=$produto->papelMaquinaQtd?>" type="text" id="especificacaoPapelMaquinaQtd" name="especificacaoPapelMaquinaQtd" class="form-control">
                            </div>

                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoRegua" class="small">Régua (cm)</label>
                                <input value="<?=$produto->regua?>" type="text" id="especificacaoRegua" name="especificacaoRegua" class="form-control">
                            </div>

                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoPapelCalculoTamanho" class="small">Tam. p/ cálculo de papel</label>
                                <input value="<?=$produto->papelCalculoTamanho?>" type="text" id="especificacaoPapelCalculoTamanho" name="especificacaoPapelCalculoTamanho" class="form-control">
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoMaquinaVelocidade" class="small">Velocidade da máquina</label>
                                <input value="<?=$produto->maquinaVelocidade?>" type="text" id="especificacaoMaquinaVelocidade" name="especificacaoMaquinaVelocidade" class="form-control">
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoFacaNumero" class="small">Faca número</label>
                                <input value="<?=$produto->facaNumero?>" type="text" id="especificacaoFacaNumero" name="especificacaoFacaNumero" class="form-control">
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoCarreiraNumero" class="small">Número de carreiras</label>
                                <input value="<?=$produto->carreiraNumero?>" type="text" id="especificacaoCarreiraNumero" name="especificacaoCarreiraNumero" class="form-control">
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoClicheRepeticao" class="small" title="Número de repetições do clichê">Repetições do clichê</label>
                                <input value="<?=$produto->clicheRepeticao?>" type="text" id="especificacaoClicheRepeticao" name="especificacaoClicheRepeticao" class="form-control">
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoClicheCodigo" class="small">Código do clichê</label>
                                <input value="<?=$produto->clicheCodigo?>" type="text" id="especificacaoClicheCodigo" name="especificacaoClicheCodigo" class="form-control">
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoPapelAcerto" class="small">Acerto Papel (mt)</label>
                                <input value="<?=$produto->papelAcerto?>" type="text" id="especificacaoPapelAcerto" name="especificacaoPapelAcerto" class="form-control">
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoProducaoTempo" class="small">Tempo de produção (minutos)</label>
                                <input value="<?=$produto->producaoTempo?>" type="text" id="especificacaoProducaoTempo" name="especificacaoProducaoTempo" class="form-control">
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoRoloId" class="small">Rolo</label>
                                <select id="especificacaoRoloId" name="especificacaoRoloId" class="form-control">
                                    <?=$especificacaoRoloId;?>
                                </select>
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoVerniz" class="small">Verniz</label>
                                <select id="especificacaoVerniz" name="especificacaoVerniz" class="form-control">
                                    <option value=""></option>
                                    <option <?=($produto->verniz == '0') ? 'selected': ''?> value="0">Não</option>
                                    <option <?=($produto->verniz == '1') ? 'selected': ''?> value="1">Sim</option>
                                </select>
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoVernizQualidade" class="small">Qualidade do verniz</label>
                                <select id="especificacaoVernizQualidade" name="especificacaoVernizQualidade" class="form-control">
                                    <option value=""></option>
                                    <option <?=($produto->vernizQualidade == '1') ? 'selected': ''?> value="1">Alta</option>
                                    <option <?=($produto->vernizQualidade == '0') ? 'selected': ''?> value="0">Baixa</option>
                                </select>
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoEtiquetaRoloQtd" class="small">Etiquetas por rolo</label>
                                <input value="<?=$produto->etiquetaRoloQtd?>" type="text" id="especificacaoEtiquetaRoloQtd" name="especificacaoEtiquetaRoloQtd" class="form-control">
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoEtiquetaCortada" class="small">Etiqueta cortada</label>
                                <select id="especificacaoEtiquetaCortada" name="especificacaoEtiquetaCortada" class="form-control">
                                    <option value=""></option>
                                    <option <?=($produto->etiquetaCortada == '1') ? 'selected': ''?> value="1">Sim</option>
                                    <option <?=($produto->etiquetaCortada == '0') ? 'selected': ''?> value="0">Não</option>
                                </select>
                            </div>
                            
                            <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                <label for="especificacaoAcertoTempo" class="small">Tempo de acerto</label>
                                <input value="<?=$produto->acertoTempo?>" type="text" id="especificacaoAcertoTempo" name="especificacaoAcertoTempo" class="form-control">
                            </div>
							
							<hr class="col-lg-12 col-md-12 col-sm-12">
                            
                            <div class="col-lg-6 col-md-6 col-sm-6" style="border-right:2px solid #D7E8FA">
                                <div class="row">
                                    <div class="page-header col-lg-12 col-md-12 col-sm-12">
                                        <h5 class="pull-left">Rebobinamento</h5>
                                        
                                        <div class="pull-right" style="margin-top:3px">
                                            <div class="btn-group">
                                                <a class="btn btn-info btn-sm" href="./report/produtoRebobinamentoReport.php?produtoId=<?=$produto->id?>" target="_blank" style="text-transform: none !important;">
                                                    <span class="glyphicon glyphicon-print"></span>
                                                    Imprimir relatório de rebobinamento
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <script>
                                        $(document).ready(function(){
                                            $('#rebobinamentoTipoId').change(function(){
                                                //1 - normal
                                                //2 - rolinho
                                                var tipo = $(this).val();
                                                switch(tipo){
                                                    case '1':
                                                        //esconde campos do rolinho
                                                        $('#rebobinamentoRoloPlastificado')     .closest('div').hide();
                                                        $('#rebobinamentoRoloMetroQuantidade')  .closest('div').hide();
                                                        
                                                        //exibe campos do normal
                                                        $('#rebobinamentoMaquinaVelocidade')    .closest('div').show();
                                                        $('#rebobinamentoFacaNumero')           .closest('div').show();
                                                        $('#rebobinamentoMaquinaAcerto')        .closest('div').show();
                                                        $('#rebobinamentoIr')                   .closest('div').show();
                                                        $('#rebobinamentoCa')                   .closest('div').show();
                                                        break;
                                                        
                                                    case '2':
                                                        //exibe campos do rolinho
                                                        $('#rebobinamentoRoloPlastificado')     .closest('div').show();
                                                        $('#rebobinamentoRoloMetroQuantidade')  .closest('div').show();
                                                        
                                                        //esconde campos do normal
                                                        $('#rebobinamentoMaquinaVelocidade')    .closest('div').hide();
                                                        $('#rebobinamentoFacaNumero')           .closest('div').hide();
                                                        $('#rebobinamentoMaquinaAcerto')        .closest('div').hide();
                                                        $('#rebobinamentoIr')                   .closest('div').hide();
                                                        $('#rebobinamentoCa')                   .closest('div').hide();
                                                        break;
                                                }
                                            });
                                            
                                            //força a execução da função change() a primeira vez que a pagina é carregada.
                                            $('#rebobinamentoTipoId').change();
                                        });
                                    </script>
                                    
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label for="rebobinamentoTipoId" class="small">Tipo de rebobinamento</label>
                                        <select id="rebobinamentoTipoId" name="rebobinamentoTipoId" class="form-control">
                                            <?=$rebobinamentoTipoId;?>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label for="rebobinamentoMaquinaId" class="small">Máquina</label>
                                        <select id="rebobinamentoMaquinaId" name="rebobinamentoMaquinaId" class="form-control">
                                            <?=$rebobinamentoMaquinaId;?>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoPapelLargura" class="small">Largura do papel (cm)</label>
                                        <input value="<?=$produto->rebobinamentoPapelLargura?>" type="text" id="rebobinamentoPapelLargura" name="rebobinamentoPapelLargura" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoCarreiraNumero" class="small">Número de carreiras</label>
                                        <input value="<?=$produto->rebobinamentoCarreiraNumero?>" type="text" id="rebobinamentoCarreiraNumero" name="rebobinamentoCarreiraNumero" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoMaquinaVelocidade" class="small">Velocidade da máquina</label>
                                        <input value="<?=$produto->rebobinamentoMaquinaVelocidade?>" type="text" id="rebobinamentoMaquinaVelocidade" name="rebobinamentoMaquinaVelocidade" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoFacaNumero" class="small">Faca número</label>
                                        <input value="<?=$produto->rebobinamentoFacaNumero?>" type="text" id="rebobinamentoFacaNumero" name="rebobinamentoFacaNumero" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoMaquinaAcerto" class="small">Acerto máquina</label>
                                        <input value="<?=$produto->rebobinamentoMaquinaAcerto?>" type="text" id="rebobinamentoMaquinaAcerto" name="rebobinamentoMaquinaAcerto" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoTubete" class="small">Tubete</label>
                                        <input value="<?=$produto->rebobinamentoTubete?>" type="text" id="rebobinamentoTubete" name="rebobinamentoTubete" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoRoloDiametro" class="small">Diamêtro do rolo</label>
                                        <input value="<?=$produto->rebobinamentoRoloDiametro?>" type="text" id="rebobinamentoRoloDiametro" name="rebobinamentoRoloDiametro" class="form-control">
                                    </div>

                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoRoloMetroQuantidade" class="small">Quantidade de metros por rolo</label>
                                        <input value="<?=$produto->rebobinamentoRoloMetroQuantidade?>" type="text" id="rebobinamentoRoloMetroQuantidade" name="rebobinamentoRoloMetroQuantidade" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoEtiquetaMetroQuantidade" class="small">Qtd de etiqueta por metro</label>
                                        <input value="<?=$produto->rebobinamentoEtiquetaMetroQuantidade?>" type="text" id="rebobinamentoEtiquetaMetroQuantidade" name="rebobinamentoEtiquetaMetroQuantidade" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoRoloPlastificado" class="small">Rolo plastificado</label>
                                        <select id="rebobinamentoRoloPlastificado" name="rebobinamentoRoloPlastificado" class="form-control">
                                            <option value=""></option>
                                            <option <?=($produto->rebobinamentoRoloPlastificado == '0') ? 'selected': ''?> value="0">Não</option>
                                            <option <?=($produto->rebobinamentoRoloPlastificado == '1') ? 'selected': ''?> value="1">Sim</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoIr" class="small">IR</label>
                                        <input value="<?=$produto->rebobinamentoIr?>" name="rebobinamentoIr" id="rebobinamentoIr" type="text" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoCa" class="small">CA</label>
                                        <input value="<?=$produto->rebobinamentoCa?>" name="rebobinamentoCa" id="rebobinamentoCa" type="text" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoTempo" class="small" title="Tempo de rebobinamento de um rolo">Tempo rebobin. de um rolo</label>
                                        <input value="<?=$produto->rebobinamentoTempo?>" type="text" id="rebobinamentoTempo" name="rebobinamentoTempo" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="rebobinamentoData" class="small">Data</label>
                                        <input value="<?=$produto->rebobinamentoData?>" type="text" id="rebobinamentoData" name="rebobinamentoData" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <label for="rebobinamentoObservacao" class="small">Observações</label>
                                        <textarea id="rebobinamentoObservacao" name="rebobinamentoObservacao" class="form-control"><?=$produto->rebobinamentoObservacao?></textarea>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="row">
                                    <div class="page-header col-lg-12 col-md-12 col-sm-12">
                                        <h5 class="pull-left">Embalagem</h5>
                                        
                                        <div class="pull-right" style="margin-top:3px">
                                            <div class="btn-group">
                                                <a class="btn btn-info btn-sm" href="./report/produtoEmbalagemReport.php?produtoId=<?=$produto->id?>" target="_blank" style="text-transform: none !important;">
                                                    <span class="glyphicon glyphicon-print"></span>
                                                    Imprimir relatório de embalagem
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="embalagemRoloDiametro" class="small">Diâmetro do rolo (mm)</label>
                                        <input value="<?=$produto->embalagemRoloDiametro?>" id="embalagemRoloDiametro" name="embalagemRoloDiametro" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="embalagemRoloAltura" class="small">Altura do rolo (mm)</label>
                                        <input value="<?=$produto->embalagemRoloAltura?>" id="embalagemRoloAltura" name="embalagemRoloAltura" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="embalagemRoloPlastificado" class="small">Rolo plastificado</label>
                                        <select id="embalagemRoloPlastificado" name="embalagemRoloPlastificado" class="form-control">
                                            <option value=""></option>
                                            <option <?=($produto->embalagemRoloPlastificado == '0') ? 'selected': ''?> value="0">Não</option>
                                            <option <?=($produto->embalagemRoloPlastificado == '1') ? 'selected': ''?> value="1">Sim</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="embalagemCaixaId" class="small">Caixa</label>
                                        <select id="embalagemCaixaId" name="embalagemCaixaId" class="form-control">
                                            <?=$embalagemCaixaId;?>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="embalagemTransportadorId" class="small">Transportador</label>
                                        <select id="embalagemTransportadorId" name="embalagemTransportadorId" class="form-control">
                                            <option value=""></option>
                                            <option <?=($produto->embalagemTransportadorId == '0') ? 'selected': ''?> value="0">Não</option>
                                            <option <?=($produto->embalagemTransportadorId == '1') ? 'selected': ''?> value="1">Sim</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="embalagemQuantidadeExata" class="small">Quantidade exata</label>
                                        <select id="embalagemQuantidadeExata" name="embalagemQuantidadeExata" class="form-control">
                                            <option value=""></option>
                                            <option <?=($produto->embalagemQuantidadeExata == '0') ? 'selected': ''?> value="0">Não</option>
                                            <option <?=($produto->embalagemQuantidadeExata == '1') ? 'selected': ''?> value="1">Sim</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="embalagemEnchimentoId" class="small">Enchimento da caixa</label>
                                        <select id="embalagemEnchimentoId" name="embalagemEnchimentoId" class="form-control">
                                            <option value=""></option>
                                            <option <?=($produto->embalagemEnchimentoId == '0') ? 'selected': ''?> value="0">Não</option>
                                            <option <?=($produto->embalagemEnchimentoId == '1') ? 'selected': ''?> value="1">Sim</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                        <label for="embalagemData" class="small">Data</label>
                                        <input value="<?=$produto->embalagemData?>" type="text" id="embalagemData" name="embalagemData" class="form-control">
                                    </div>
                                    
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <label for="embalagemObservacao" class="small">Observações</label>
                                        <textarea id="embalagemObservacao" name="embalagemObservacao" class="form-control"><?=$produto->embalagemObservacao?></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <nav id="footer" class="navbar navbar-default navbar-fixed-bottom">
            <div class="pull-right">
                <div class="btn-group">
                    <a class="btn btn-info" id="btnProdutoEspecificacaoRelatorioFichaTecnica" href="./report/produtoFichaTecnicaReport.php?produtoId=<?=$produto->id?>" target="_blank">
                        <span class="glyphicon glyphicon-print"></span>
                        Imprimir ficha técnica
                    </a>
                </div>
                
                <div class="btn-group">
                    <button type="button" class="btn btn-success" id="btnProdutoEspecificacaoSalvar">
                        <span class="glyphicon glyphicon-ok"></span>
                        Salvar alterações
                    </button>
                </div>
            </div>
        </nav>
    </div>

    <script>
        $( "#btnProdutoEspecificacaoSalvar" ).click(function() {
            $.post("controller/produto/ProdutoEditarEspecificacaoSalvarController.php", $("#frmProdutoEspecificacao").serialize())
                .done(function(data) {
                alert(data);
            });
        });
    </script>
    