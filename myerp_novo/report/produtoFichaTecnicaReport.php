<?php

    chdir('../');
    require_once('libs/fpdf/fpdf.php');
    require_once('Config.php');
   
############################################################################################

    $pdf = new FPDF('P', 'mm', 'A4');
    //pode ser P (portrait) ou L (landscape)

    $pdf->AliasNbPages();
    //isso faz com que funcione o total de paginas

    $pdf->SetMargins('13', 6, '');
    //defino as margens do documento...

    $pdf->AddPage();

    $produto                                = new Produto();
    $produto                                ->load(filter_input(INPUT_GET, 'produtoId'));
    $produto                                ->formatar();
    $produtoId                              = $produto->produtoId;
    $maquinaId                              = $produto->maquinaId;
    $papelId                                = $produto->papelId;
    $largura                                = $produto->largura;
    $altura                                 = $produto->altura;
    $portaClicheNumero                      = $produto->portaClicheNumero;
    $papelLargura                           = $produto->papelLargura;
    $papelMaquinaQtd                        = $produto->papelMaquinaQtd;
    $regua                                  = $produto->regua;
    $papelCalculoTamanho                    = $produto->papelCalculoTamanho;
    $maquinaVelocidade                      = $produto->maquinaVelocidade;
    $facaNumero                             = $produto->facaNumero;
    $carreiraNumero                         = $produto->carreiraNumero;
    $clicheRepeticao                        = $produto->clicheRepeticao;
    $clicheCodigo                           = $produto->clicheCodigo;
    $papelAcerto                            = $produto->papelAcerto;
    $producaoTempo                          = $produto->producaoTempo;
    $roloId                                 = $produto->roloId;
    $verniz                                 = $produto->verniz;
    $vernizQualidade                        = $produto->vernizQualidade;
    $etiquetaRoloQtd                        = $produto->etiquetaRoloQtd;
    $etiquetaCortada                        = $produto->etiquetaCortada;
    $acertoTempo                            = $produto->acertoTempo;
    $operador                               = ''; #$produto->operador;
    $rebobinamentoTipoId                    = $produto->rebobinamentoTipoId;
    $rebobinamentoMaquinaId                 = $produto->rebobinamentoMaquinaId;
    $rebobinamentoPapelLargura              = $produto->rebobinamentoPapelLargura;
    $rebobinamentoCarreiraNumero            = $produto->rebobinamentoCarreiraNumero;
    $rebobinamentoMaqunaVelocidade          = $produto->rebobinamentoMaquinaVeloicidade;
    $reboninamentoFacaNumero                = $produto->rebobinamentoFacaNumero;
    $rebobinamentoMaquinaAcerto             = $produto->rebobinamentoMaquinaAcerto;
    $rebobinamentoTubete                    = $produto->rebobinamentoTubete;
    $rebobinamenroRoloDiametro              = $produto->rebobinamentoRoloDiametro;
    $reboninamentoEtiquetaMetroQuantidade   = $produto->reboninamentoEtiquetaMetroQuantidade;
    $rebobinamentoIr                        = $produto->rebobinamentoIr;
    $rebobinamentoCa                        = $produto->rebobinamentoCa;
    $rebobinamentoTempo                     = $produto->rebobinamentoTempo;
    $rebobinamentoData                      = $produto->rebobinamentoData;
    $rebobinamentoObservacao                = $produto->rebobinamentoObservacao;
    $embalagemRoloDiametro                  = $produto->embalagemRoloDiametro;
    $embalagemRoloAltura                    = $produto->embalagemRoloAltura;
    $embalagemRoloPlastificado              = $produto->embalagemRoloPlastificado;
    $embalagemCaixaId                       = $produto->embalagemCaixaId;
    $embalagemTransportadorId               = $produto->embalagemTransportadorId;
    $embalagemQuantidadeExata               = $produto->embalagemQuantidadeExata;
    $embalagemEnchimentoId                  = $produto->embalagemEmchimentoId;
    $embalagemData                          = $produto->embalagemData;
    $embalagemObservacao                    = $produto->embalagemObservacao;

    $pdf->SetFont('Arial','',15);
    $pdf->SetY(9);
    $pdf->Cell(185, 10, "Ficha Técnica", 'B');
    $pdf->Ln(11);
    
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(185, 9, "Amostra de Etiqueta", 'L R T');
    $pdf->Ln();
    $pdf->Cell(185, 90, "", 'L R B');
    $pdf->Ln(94);
    
    $pdf->Cell(185,5,"Sentido do Rolo");
    $pdf->Image("report/img/sentido_rolo.png", 12, 130, 187, 30);
    $pdf->Ln(41);
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(20, 6,"Cliente:",'L T');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(165, 6,"$produto->clienteNome",'T R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(20, 6, "Descrição", 'L B');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(95, 6, $produto->nome, 'B');
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(28, 6, "Código (cliente)", 'B');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(42, 6, $produto->clienteProdutoCodigo, 'B R');
    $pdf->Ln(10);
    
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(30, 6, "Nº do Porta Cliche", 'L  T');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(50, 6, "$portaClicheNumero", 'R T', '', 'R');
    $pdf->Ln();

    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(30,6,"Largura da Etiqueta(cm)",'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(50,6,"$largura",'R','','R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(30, 6, "Altura da Etiqueta(cm)", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(50, 6, $altura, 'R', '', 'R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(30,6,"Largura do Papel(cm)",'L ');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(50,6,"$papelLargura",'R','','R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(30,6,"Papel na Maquina(cm)",'L ');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(50,6,"$papelMaquinaQtd",'R','','R');
    $pdf->Ln();

    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(30, 6,"Régua",'L ');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(50, 6,"$regua",'R','','R');
    $pdf->SetFont('Arial','B',9);
    $pdf->Ln();

    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(50,6,"Tamanho p/ Cálculo de Papel",'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(30,6,"$papelCalculoTamanho",'R','','R');
    $pdf->Ln();

    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(50, 6, "Velocidade Máquina",'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(30, 6, "$maquinaVelocidade",'R','','R');
    $pdf->Ln();
    
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(50, 6, "Faca Nº",'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(30, 6, "$facaNumero",'R','','R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(50, 6, "Nº Carreiras",'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(30, 6, "$carreiraNumero",'R','','R');
    $pdf->Ln();

    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(50, 6, "Nº Repetições de Clichê",'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(30, 6, "$clicheRepeticao",'R','','R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(30, 6,"Código do Clichê",'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(50, 6,"$clicheCodigo",'R','','R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(30,6,"Acerto de Papel(mt)",'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(50,6,"$papelAcerto",'R','','R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(30,6,"Tempo de Produção(min)",'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(50,6,"$producaoTempo",'R','','R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(50,6,"Qdte. de Etiquetas por Rolo",'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(30,6,"$etiquetaRoloQtd",'R','','R');
    $pdf->Ln();

    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(50,6,"Rolo",'L B');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(30,6,"$produto->rolo",'R B','','R');
    
    # LADO DIREITO
    $pdf->setY(180);
    $pdf->setX(94);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(15,6, "Papel:", 'L T');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(89,6, "$produto->papel", 'T R');
    $pdf->Ln();
    
    $pdf->setX(94);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(104,6, "Tintas", 'L R');
    $pdf->Ln();
    
    $componentes = $produto->getComponentes();
    if($componentes){
        $pdf->setX(94);
        $pdf->SetFont('Arial', '', 9);
        foreach($componentes as $componente){ $pdf->Cell(104,6, $componente->componenteNome, 'L R'); }
        $pdf->Ln();
    }
    
    $pdf->setX(94);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(15, 6, "Verniz:", 'L B');
    $pdf->SetFont('Arial', '', 9);
    $verniz = ($verniz == 1) ? "Sim" : "Não";
    $pdf->Cell(32, 6, $verniz, 'B');
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(25,6, "Resistência:", 'B');
    $pdf->SetFont('Arial', '', 9);
    $vernizQualidade = ($vernizQualidade == 1) ? "Alta" : "Baixa";
    $pdf->Cell(32,6, $vernizQualidade, 'R B');
    $pdf->Ln(9);
    
    $pdf->setX(94);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(104, 6, "Observação:", 'L T R');
    $pdf->Ln();
    
    $pdf->setX(94);
    $pdf->SetFont('Arial', '', 9);
    $pdf->MultiCell(104, 6, "$rebobinamentoObservacao", 'L R');
    
    $pdf->setX(94);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(35, 6, "Data de cadastro:", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(69, 6, "$rebobinamentoData", 'R', '', 'C');
    $pdf->Ln();    
    
    $pdf->setX(94);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(35, 6, "Máquina:", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(69, 6, "$produto->maquina", 'R', '', 'C');
    $pdf->Ln();
    
    $pdf->setX(94);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(35, 6, "Tempo de Acerto:", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(69, 6, "$acertoTempo", 'R', '', 'C');
    $pdf->Ln(); 
    
    $pdf->setX(94);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(35, 6, "Operador:", 'L B');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(69, 6, "$operador", 'R B', '', 'C');
    $pdf->Ln(); 

    $pdf->Output();

?>


