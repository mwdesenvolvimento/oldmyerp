<?php	

    chdir('../');
    require_once('libs/fpdf/fpdf.php');
    require_once('Config.php');

    $pdf = new FPDF('P', 'mm', 'A4');
    //pode ser P (portrait) ou L (landscape)

    $pdf->AliasNbPages();
    //isso faz com que funcione o total de paginas

    $pdf->SetMargins('13',6,'');
    //defino as margens do documento...

    $pdf->AddPage();

    $produto                                = new Produto();
    $produto                                ->load(filter_input(INPUT_GET, 'produtoId'));
    $produto                                ->formatar();
    $produtoId                              = $produto->produtoId;
    $maquinaId                              = $produto->maquinaId;
    $papelId                                = $produto->papelId;
    $largura                                = $produto->largura;
    $altura                                 = $produto->altura;
    $portaClicheNumero                      = $produto->portaClicheNumero;
    $papelLargura                           = $produto->papelLargura;
    $papelMaquinaQtd                        = $produto->papelMaquinaQtd;
    $regua                                  = $produto->regua;
    $papelCalculoTamanho                    = $produto->papelCalculoTamanho;
    $maquinaVelocidade                      = $produto->maquinaVelocidade;
    $facaNumero                             = $produto->facaNumero;
    $carreiraNumero                         = $produto->carreiraNumero;
    $clicheRepeticao                        = $produto->clicheRepeticao;
    $clicheCodigo                           = $produto->clicheCodigo;
    $papelAcerto                            = $produto->papelAcerto;
    $producaoTempo                          = $produto->producaoTempo;
    $roloId                                 = $produto->roloId;
    $verniz                                 = $produto->verniz;
    $vernizQualidade                        = $produto->vernizQualidade;
    $etiquetaRoloQtd                        = $produto->etiquetaRoloQtd;
    $etiquetaCortada                        = $produto->etiquetaCortada;
    $acertoTempo                            = $produto->acertoTempo;
    $operador                               = $produto->operador;
    $rebobinamentoTipoId                    = $produto->rebobinamentoTipoId;
    $rebobinamentoMaquinaId                 = $produto->rebobinamentoMaquinaId;
    $rebobinamentoPapelLargura              = $produto->rebobinamentoPapelLargura;
    $rebobinamentoCarreiraNumero            = $produto->rebobinamentoCarreiraNumero;
    $rebobinamentoMaquinaVelocidade         = $produto->rebobinamentoMaquinaVelocidade;
    $rebobinamentoFacaNumero                = $produto->rebobinamentoFacaNumero;
    $rebobinamentoMaquinaAcerto             = $produto->rebobinamentoMaquinaAcerto;
    $rebobinamentoTubete                    = $produto->rebobinamentoTubete;
    $rebobinamentoRoloMetroQuantidade       = $produto->rebobinamentoRoloMetroQuantidade;
    $rebobinamentoRoloDiametro              = $produto->rebobinamentoRoloDiametro;
    $rebobinamentoEtiquetaMetroQuantidade   = $produto->rebobinamentoEtiquetaMetroQuantidade;
    $rebobinamentoIr                        = $produto->rebobinamentoIr;
    $rebobinamentoCa                        = $produto->rebobinamentoCa;
    $rebobinamentoTempo                     = $produto->rebobinamentoTempo;
    $rebobinamentoData                      = $produto->rebobinamentoData;
    $rebobinamentoObservacao                = $produto->rebobinamentoObservacao;
    $embalagemRoloDiametro                  = $produto->embalagemRoloDiametro;
    $embalagemRoloAltura                    = $produto->embalagemRoloAltura;
    $embalagemRoloPlastificado              = $produto->embalagemRoloPlastificado;
    $embalagemCaixaId                       = $produto->embalagemCaixaId;
    $embalagemTransportadorId               = $produto->embalagemTransportadorId;
    $embalagemQuantidadeExata               = $produto->embalagemQuantidadeExata;
    $embalagemEnchimentoId                  = $produto->embalagemEnchimentoId;
    $embalagemData                          = $produto->embalagemData;
    $embalagemObservacao                    = $produto->embalagemObservacao;

    $pdf->SetFont('Arial', '', 15);
    $pdf->SetY(9);
    $pdf->Cell(185, 10, "Ficha Técnica de Rebobinamento", 'B');
    #$pdf->Image('image\layout\logo_empresa.jpg',172,5,27,22);
    $pdf->Ln(11);
    
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(185,9,"Amostra de Etiqueta",'L R T');
    $pdf->Ln();
    $pdf->Cell(185,90,"",'L R B');
    $pdf->Ln(94);
    
    $pdf->Cell(185, 5, "Sentido do Rolo");
    $pdf->Image("report/img/sentido_rolo.png", 12, 130, 187, 30);
    $pdf->Ln(45);
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(20, 6, "Cliente", 'L T');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(165, 6, $produto->clienteNome, 'T R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(20, 6, "Descrição", 'L B');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(95, 6, $produto->nome, 'B');
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(28, 6, "Código (cliente)", 'B');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(42, 6, $produto->clienteProdutoCodigo, 'B R');
    $pdf->Ln(10);
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(35, 6, "Tipo de rebobinamento", 'L T');
    $pdf->SetFont('Arial', '', 9);
    $tipo_rebobinamento = ($rebobinamentoTipoId == '1') ? 'Normal' : 'Rolinho';
    $pdf->Cell(49, 6, $tipo_rebobinamento, 'T R', '', 'R');
    $pdf->Ln();

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(35, 6, "Largura da Etiqueta(cm)", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(49, 6, $largura, 'R', '', 'R');
    $pdf->Ln();

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(35, 6, "Altura da Etiqueta(cm)", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(49, 6, $altura, 'R', '', 'R');
    $pdf->Ln();

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(35, 6, "Tamanho p/ Cálculo de Papel", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(49, 6, $papelCalculoTamanho, 'R', '', 'R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(50, 6, "Largura Papel", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(34, 6, $rebobinamentoPapelLargura, 'R', '', 'R');
    $pdf->Ln();

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(41, 6, "Nº Carreiras", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(43, 6, $rebobinamentoCarreiraNumero, 'R', '', 'R');
    $pdf->Ln();

    if($rebobinamentoTipoId == 2){
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(41, 6, "Qtde. Metros Por Rolo", 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(43, 6, $rebobinamentoRoloMetroQuantidade, 'R', '', 'R');
        $pdf->Ln();
    }
    
    if($rebobinamentoTipoId == 1){
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(41, 6, "Velocidade da Máquina", 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(43, 6, $rebobinamentoMaquinaVelocidade, 'R', '', 'R');
        $pdf->Ln();
        
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(41, 6, "Faca", 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(43, 6, $rebobinamentoFacaNumero, 'R', '', 'R');
        $pdf->Ln();
        
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(41, 6, "IR", 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(43, 6, $rebobinamentoIr, 'R', '', 'R');
        $pdf->Ln();
        
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(41, 6, "CA", 'L');
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(43, 6, $rebobinamentoCa, 'R', '', 'R');
        $pdf->Ln();
    }

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(41, 6, "Qtde. de Etiquetas por Metro", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(43, 6, $rebobinamentoEtiquetaMetroQuantidade, 'R', '', 'R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(41, 6, "Tubete", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(43, 6, $rebobinamentoTubete, 'R', '', 'R');
    $pdf->Ln();

    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(41, 6, "Qtde. Etiquetas por Rolo", 'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(43, 6, $etiquetaRoloQtd, 'R', '', 'R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(41, 6, "Diâmetro do Rolo", 'L B');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(43, 6, $rebobinamentoRoloDiametro, 'R B', '', 'R');
    $pdf->Ln();
    
    # LADO DIREITO
    $pdf->setY(184);
    $pdf->setX(98);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(60, 6, "Tempo de rebobinamento de um rolo", 'L T');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(40, 6, $produto->rebobinamentoTempo, 'T R', '', 'C');
    $pdf->Ln();
    
    $pdf->setX(98);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(100, 6, "Observação", 'L R');
    $pdf->Ln();
    
    $pdf->setX(98);
    $pdf->SetFont('Arial', '', 9);
    $pdf->MultiCell(100, 6, $rebobinamentoObservacao, 'L R');
    
    $pdf->setX(98);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(40, 6, "Data", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(60, 6, $rebobinamentoData, 'R', '', 'C');
    $pdf->Ln();
    
    $pdf->setX(98);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(40, 6, "Maquina", 'L B');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(60, 6, $produto->maquina, 'R B', '', 'C');
    $pdf->Ln();

    $pdf->Output();

?>