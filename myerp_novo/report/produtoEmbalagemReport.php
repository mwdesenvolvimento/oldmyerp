<?php	

    chdir('../');
    require_once('libs/fpdf/fpdf.php');
    require_once('Config.php');

    $pdf = new FPDF('P', 'mm', 'A4');
    //pode ser P (portrait) ou L (landscape)

    $pdf->AliasNbPages();
    //isso faz com que funcione o total de paginas

    $pdf->SetMargins('13',6,'');
    //defino as margens do documento...

    $pdf->AddPage();

    $produto                                = new Produto();
    $produto                                ->load(filter_input(INPUT_GET, 'produtoId'));
    $produto                                ->formatar();
    $produtoId                              = $produto->produtoId;
    $maquinaId                              = $produto->maquinaId;
    $papelId                                = $produto->papelId;
    $largura                                = $produto->largura;
    $altura                                 = $produto->altura;
    $portaClicheNumero                      = $produto->portaClicheNumero;
    $papelLargura                           = $produto->papelLargura;
    $papelMaquinaQtd                        = $produto->papelMaquinaQtd;
    $regua                                  = $produto->regua;
    $papelCalculoTamanho                    = $produto->papelCalculoTamanho;
    $maquinaVelocidade                      = $produto->maquinaVelocidade;
    $facaNumero                             = $produto->facaNumero;
    $carreiraNumero                         = $produto->carreiraNumero;
    $clicheRepeticao                        = $produto->clicheRepeticao;
    $clicheCodigo                           = $produto->clicheCodigo;
    $maquinaAcerto                          = $produto->maquinaAcerto;
    $producaoTempo                          = $produto->producaoTempo;
    $roloId                                 = $produto->roloId;
    $verniz                                 = $produto->verniz;
    $vernizQualidade                        = $produto->vernizQualidade;
    $etiquetaRoloQtd                        = $produto->etiquetaRoloQtd;
    $etiquetaCortada                        = $produto->etiquetaCortada;
    $acertoTempo                            = $produto->acertoTempo;
    $operador                               = $produto->operador;
    $rebobinamentoTipoId                    = $produto->rebobinamentoTipoId;
    $rebobinamentoMaquinaId                 = $produto->rebobinamentoMaquinaId;
    $rebobinamentoPapelLargura              = $produto->rebobinamentoPapelLargura;
    $rebobinamentoCarreiraNumero            = $produto->rebobinamentoCarreiraNumero;
    $rebobinamentoMaquinaVelocidade         = $produto->rebobinamentoMaquinaVelocidade;
    $rebobinamentoFacaNumero                = $produto->rebobinamentoFacaNumero;
    $rebobinamentoMaquinaAcerto             = $produto->rebobinamentoMaquinaAcerto;
    $rebobinamentoTubete                    = $produto->rebobinamentoTubete;
    $rebobinamentoRoloMetroQuantidade       = $produto->rebobinamentoRoloMetroQuantidade;
    $rebobinamentoRoloDiametro              = $produto->rebobinamentoRoloDiametro;
    $rebobinamentoEtiquetaMetroQuantidade   = $produto->rebobinamentoEtiquetaMetroQuantidade;
    $rebobinamentoIr                        = $produto->rebobinamentoIr;
    $rebobinamentoCa                        = $produto->rebobinamentoCa;
    $rebobinamentoTempo                     = $produto->rebobinamentoTempo;
    $rebobinamentoData                      = $produto->rebobinamentoData;
    $rebobinamentoObservacao                = $produto->rebobinamentoObservacao;
    $embalagemRoloDiametro                  = $produto->embalagemRoloDiametro;
    $embalagemRoloAltura                    = $produto->embalagemRoloAltura;
    $embalagemRoloPlastificado              = $produto->embalagemRoloPlastificado;
    $embalagemCaixaId                       = $produto->embalagemCaixaId;
    $embalagemTransportadorId               = $produto->embalagemTransportadorId;
    $embalagemQuantidadeExata               = $produto->embalagemQuantidadeExata;
    $embalagemEnchimentoId                  = $produto->embalagemEnchimentoId;
    $embalagemData                          = $produto->embalagemData;
    $embalagemObservacao                    = $produto->embalagemObservacao;


    $pdf->SetFont('Arial', '', 15);
    $pdf->SetY(9);
    $pdf->Cell(185, 10, "Ficha Técnica de Embalagem", 'B');
    #$pdf->Image('..\image\layout\logo_empresa.jpg',172,5,27,22);
    $pdf->Ln(11);
    
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(185,9,"Amostra de Etiqueta",'L R T');
    $pdf->Ln();
    $pdf->Cell(185, 90,"",'L R B');
    $pdf->Ln(94);
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(20, 8, "Cliente", 'L T');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(165, 8, $produto->clienteNome, 'T R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(20, 8, "Descrição", 'L B');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(95, 8, $produto->nome, 'B');
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(28, 8, "Código (cliente)", 'B');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(42, 8, $produto->clienteProdutoCodigo, 'B R');
    $pdf->Ln(10);

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(35, 8, "Largura da Etiqueta (cm)", 'LT');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(49, 8, $largura, 'RT', '', 'R');
    $pdf->Ln();

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(35, 8, "Altura da Etiqueta (cm)", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(49, 8, $altura, 'R', '', 'R');
    $pdf->Cell(1, 8, "", '');
    $pdf->Ln();

    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(41, 8, "Qtde. Etiquetas por Rolo", 'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(43, 8, $etiquetaRoloQtd, 'R', '', 'R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(41, 8, "Diâmetro do Rolo", 'L ');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(43, 8, $embalagemRoloDiametro, 'R ', '', 'R');
    $pdf->Ln();
    

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(41, 8, "Altura do Rolo (cm)", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(43, 8, $embalagemRoloAltura, 'R', '', 'R');
    $pdf->Ln();

   
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(45, 8, "Rolo Plastificado", 'L');
    $pdf->SetFont('Arial', '', 9);
    $roloPlastificado = ($embalagemRoloPlastificado == '1') ? "Sim" : "Não";
    $pdf->Cell(39, 8, $roloPlastificado, 'R', '', 'R');
    $pdf->Ln();
    
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(41, 8, "Caixa", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(43, 8, $embalagemCaixaId, 'R', '', 'R');
    $pdf->Ln();

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(45, 8, "Transportadora", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(39, 8, ($embalagemTransportadorId == '1') ? "Sim" : "Não", 'R', '', 'R');
    $pdf->Ln();

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(45, 8, "Quantidade Exata", 'L');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(39, 8, ($embalagemQuantidadeExata == '1') ? "Sim" : "Não", 'R', '', 'R');
    $pdf->Ln();

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(45, 8, "Enchimento da Caixa", 'L B');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(39, 8, $embalagemEnchimentoId, 'B R', '', 'R');
    $pdf->Ln();
    
    
    
    
    # LADO DIREITO
    $pdf->setY(141);

    $pdf->setX(98);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(100, 8, "Observação", 'L R T');
    $pdf->Ln();
    
    $pdf->setX(98);
    $pdf->SetFont('Arial', '', 9);
    $pdf->MultiCell(100, 8, $embalagemObservacao, 'L R');
    
   
    
    $pdf->setX(98);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(40, 8, "Data", 'L B');
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(60, 8, $embalagemData , 'R B', '', 'C');
    $pdf->Ln();

    $pdf->Output();

?>