<?php

    chdir('../');
    require_once('libs/fpdf/fpdf.php');
    require_once('Config.php');
    
    ############################################## REQUIRES ######################################################
    $pdf = new FPDF('P', 'mm', 'A4');
    //pode ser P (portrait) ou L (landscape)

    $pdf->AliasNbPages();
    //isso faz com que funcione o total de paginas

    $pdf->SetMargins(6, 6, 6);
    //defino as margens do documento...

    $ordemDeProducao    = new OrdemDeProducao();
    $ordemDeProducao    ->load(filter_input(INPUT_GET, 'id'));
    $ordemDeProducao    ->formatar();
    $id                 = $ordemDeProducao->id;
    $aberturaData       = $ordemDeProducao->aberturaData;
    $cadastroTimeStamp  = $ordemDeProducao->cadastroTimeStamp;
    $previsaoData       = $ordemDeProducao->previsaoData;
    $conclusaoData      = $ordemDeProducao->conclusaoData;
    $statusId           = $ordemDeProducao->statusNome;
    $prioridadeId       = $ordemDeProducao->prioridadeNome;
    $impressaoLiberada  = $ordemDeProducao->impressaoLiberada;
    $observacao         = $ordemDeProducao->observacao;
    $lote               = $ordemDeProducao->lote;
    $usuarioId          = $ordemDeProducao->usuarioId;
    $itens              = $ordemDeProducao->getItens();

    $pdf->AddPage();

    $pdf->SetFont('Arial', 'B',10);
    $pdf->Cell(160, 7, "ORDEM DE PRODUÇÃO", 'L R B T', '', 'C');
    $pdf->Image('../image/layout/logo_empresa.jpg', 172, 7, 27, 22);

    $pdf->Ln();
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(20, 6, "ID DA OP:", 'L  B');
    $pdf->SetFont('Arial', '',9);
    $pdf->Cell(140, 6, "$id", 'R B'); //numero da op 

    $pdf->Ln();
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(35, 6, "LOTE BRINDART:", 'L  B');
    $pdf->SetFont('Arial', '',9);
    $pdf->Cell(125, 6, "$id", 'R B'); //numero da op 

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(35, 6, "DATA ABERTURA:", 'L B');
    $pdf->SetFont('Arial', '',9);
    $pdf->Cell(125, 6, "$aberturaData", 'R B');//data abertura

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(37, 6,"",'T B L');
    $pdf->SetFont('Arial', '',9);
    $pdf->Cell(93, 6,"",'R T B'); //situação

    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(27, 6, "PRIORIDADE:", 'T B');
    $pdf->SetFont('Arial', '',9);
    $pdf->Cell(41, 6, "$prioridadeId", 'R T B');//prioridade

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(35,6,"DATA CONCLUSÃO:",'L B');
    $pdf->SetFont('Arial', '',9);
    $pdf->Cell(95,6,"$conclusaoData",'R B'); // data conclusão

    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(27,6,"STATUS:",'T B');
    $pdf->SetFont('Arial', '',9);
    $pdf->Cell(41,6,"$statusId",'R T B'); //situação

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(16, 9, "CLIENTE:", 'L');
    $pdf->SetFont('Arial', '',9);
    $pdf->Cell(182, 9, "$ordemDeProducao->itemPedidoCliente", 'R');  //nome cliente

    
    $cliente = new Cliente();
    $cliente->load($ordemDeProducao->itemPedidoClienteId);

    $pdf->Ln(9);
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(21, 9, "ENDEREÇO:", 'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(177, 9, "$cliente->endereco, $cliente->numero - $cliente->bairro", 'R'); //nome cidade

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(198,7,"ITENS PRODUZIDOS",1,"",'C');

    ##################################### Itens Produzidos #############################
    
    $pdf->Ln();
    $pdf->Cell(14, 6, "PEDIDO", 'L R B', "", 'C');
    $pdf->Cell(18, 6, "LOTE", 'L R B', "", 'C');
    $pdf->Cell(22, 6, "QTDE", 'L R B', "", 'C');
    $pdf->Cell(8, 6, "UN", 'L R B', "", 'C');
    $pdf->Cell(20, 6, "COD ITEM", 'L R B', "", 'C');
    $pdf->Cell(84, 6, "ITEM", 'L R B', "", 'C');
    $pdf->Cell(16, 6, "LAR", 'L R B', "", 'C');
    $pdf->Cell(16, 6, "ALT", 'L R B', "", 'C');

    ##################################################################################
    
    $pdf->SetFont('Arial', '',8);
    
    foreach($itens as $item){
        $item->formatar();
        $pdf->Ln();
        $pdf->Cell(14, 6, $item->pedidoId, 'L R B', "", 'C');
        $pdf->Cell(18, 6, $item->produtoLote, 'L R B', "", 'C');
        $pdf->Cell(22, 6, $item->produtoQuantidade, 'L R B', "", 'C');
        $pdf->Cell(8, 6, $item->produtoUnidadeMedidaNome, 'L R B',"", 'C');
        $pdf->Cell(20, 6, $item->clienteProdutoCodigo, 'L R B', "", 'C');
        $pdf->Cell(84, 6, $item->produtoNome, 'L R B', "", '');
        $pdf->Cell(16, 6, $item->produtoLargura, 'L R B', "", 'C');
        $pdf->Cell(16, 6, $item->produtoAltura, 'L R B', "", 'C'); 
    }

    $pdf->Ln();
    $pdf->Cell(198,5,"",1);

    ##################################### Calculodo Papel ####################################

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(198,7,"CÁLCULO DO PAPEL",1,'','C');
    $pdf->SetFont('Arial', 'B',8);
    $pdf->Ln();
    $pdf->Cell(13,6,"ITEM",'L R B','','C');
    $pdf->Cell(75,6,"MATERIAL",'L R B','','C');
    $pdf->Cell(23,6,"LARGURA(cm)",'L R B','','C');
    $pdf->Cell(18,6,"COMP.(m)",'L R B','','C');
    $pdf->Cell(18,6,"% ACRESC.",'L R B','','C');
    $pdf->Cell(24,6,"COMP.FINAL(m)",'L R B','C');
    $pdf->Cell(27,6,"QUANTIDADE M²",'L R B','C');

    #####################################################################################
    
    $pdf->SetFont('Arial', '',8);
    foreach($itens as $item){
        $pdf->Ln();
        $pdf->Cell(13, 6, $item->produtoCodigo, 'L R B', '', 'C');
        $pdf->Cell(75, 6, $item->produtoPapelNome, 'L R B', '');
        $pdf->Cell(23, 6, $item->produtoPapelLargura, 'L R B', '', 'C');
        $pdf->Cell(18, 6, $item->produtoPapelComprimento, 'L R B', '', 'C');
        $pdf->Cell(18, 6, $item->produtoPapelAcrescimo, 'L R B', '', 'C');
        $pdf->Cell(24, 6, $item->produtoPapelComprimentoFinal, 'L R B', '', 'C');
        $pdf->Cell(27, 6, $item->produtoPapelQuantidadeM2, 'L R B', '', 'C');
    }

    ######################################## Tintas e Cromias #############################

    $pdf->Ln();
    $pdf->Cell(198,5,"",1);
    
    $pdf->Ln();
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(198,6,"TINTAS",1,'','C');

    #######################################################################################

    $pdf->SetFont('Arial','',9);
    
    foreach($itens as $item){
        $componentes = $item->getComponentes();
        foreach($componentes as $componente){
            $pdf->Ln();
            $pdf->Cell(198, 8, "$componente->componenteNome", 1);
        }
    }
    
    $pdf->Ln();
    $pdf->Cell(198,5,"",1);

    ######################################## Relatório de Produção ##############################       

    $pdf->Ln();
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(198,6,"RELATÓRIO DE PRODUÇÃO",1,'','C');
    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',8);
    $pdf->Cell(32,6,"",1);
    $pdf->Cell(69,6,"PRODUÇÃO",'R B','','C');
    $pdf->Cell(50,6,"ALMOXARIFADO",'R B','','C');
    $pdf->Cell(47,6,"REBOBINAMENTO",'R B','','C');

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(11,6,"",'L R','','C');
    $pdf->Cell(21,6,"",'R','','C');
    $pdf->Cell(16,6,"Acerto",'L R','','C');
    $pdf->Cell(17,6,"Papel",'L R','','C');
    $pdf->Cell(16,6,"Retorno",'L R','','C');
    $pdf->Cell(20,6,"Resistência",'L R','','C');
    $pdf->Cell(18,6,"Papel",'L R','','C');
    $pdf->Cell(16,6,"Acerto",'L R','','C');
    $pdf->Cell(16,6,"Cod.",'L R','','C');
    $pdf->Cell(15,6,"Papel",'L R','','C');
    $pdf->Cell(17,6,"Qtde Etiq.",'L R','','C');
    $pdf->Cell(15,6,"",'L R','','C');
    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(11,6,"Item",'L R B','','C');
    $pdf->Cell(21,6,"Máquina",'R B','','C');
    $pdf->Cell(16,6,"Máquina",'L R B','','C');
    $pdf->Cell(17,6,"Produzido",'L R B','','C');
    $pdf->Cell(16,6,"Papel",'L R B','','C');
    $pdf->Cell(20,6,"Verniz",'L R B','','C');
    $pdf->Cell(18,6,"Entregue",'L R B','','C');
    $pdf->Cell(16,6,"Papel",'L R B','','C');
    $pdf->Cell(16,6,"Clichê",'L R B','','C');
    $pdf->Cell(15,6,"Perdido",'L R B','','C');
    $pdf->Cell(17,6,"Perdida",'L R B','','C');
    $pdf->Cell(15,6,"Total",'L R B','','C');

    ###################################################################################

    foreach($itens as $item){
        $pdf->Ln();
        $pdf->SetFont('Arial', '',8);
        $pdf->Cell(11, 6, "$item->produtoCodigo", 'L R B','','C');
        $pdf->Cell(21, 6, "$item->produtoMaquinaId", 'R B', '', 'C');
        $pdf->Cell(16, 6, ($item->produtoMaquinaAcerto > 0) ? $item->produtoMaquinaAcerto : '', 'L R B', '', 'C');
        $pdf->Cell(17, 6, ($item->produtoPapelProduzido > 0) ? $item->produtoPapelProduzido : '', 'L R B', '', 'C');
        $pdf->Cell(16, 6, ($item->produtoPapelRetorno > 0) ? $item->produtoPapelRetorno : '', 'L R B', '', 'C');
        $pdf->Cell(20, 6, ($item->produtoVernizQualidade == '1') ? "Alta" : "Baixa", 'L R B', '', 'C');
        $pdf->Cell(18, 6, "$item->produtoPapelComprimentoFinal", 'L R B', '', 'C'); //produtoPapelComprimentoFinal é o papel entregue...
        $pdf->Cell(16, 6, "$item->produtoPapelAcerto", 'L R B', '', 'C');
        $pdf->Cell(16, 6, "$item->produtoClicheCodigo", 'L R B', '', 'C');
        $pdf->Cell(15, 6, ($item->produtoPapelPerdido > 0) ? $item->produtoPapelPerdido : '', 'L R B', '', 'C');
        $pdf->Cell(17, 6, ($item->produtoQuantidadePerdida > 0) ? $item->produtoQuantidadePerdida : '', 'L R B', '', 'C');
        $pdf->Cell(15, 6, ($item->produtoTotalEtiquetas > 0) ? $item->produtoTotalEtiquetas : '', 'L R B', '', 'C');
    }

    ########################## Esses campos serão todos preenchidos manualmente #############                

    $pdf->Ln();
    $pdf->Cell(198,3,"",'T B');
    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(33,6,"Setup",'L R','','C');
    $pdf->Cell(70,6,"PRODUZIDO POR:",'L R');
    $pdf->Cell(47,6,"DATA",'L R');
    $pdf->Cell(48,6,"REBOBINADO POR:",'L R');

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',8);
    $pdf->Cell(33,3,"",'L R');
    $pdf->Cell(32,3,"",'L');
    $pdf->Cell(38,3,"",' R');
    $pdf->Cell(47,3,"",'L R');
    $pdf->Cell(21,3,"",'L');
    $pdf->Cell(27,3,"",'R');

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',8);
    $pdf->Cell(33,6,"Início:",'L R');
    $pdf->Cell(32,6,"DATA:",'L');
    $pdf->Cell(38,6,"DATA:",' R');
    $pdf->Cell(47,6,"VISTO RETIRADA MATERIAL:",'L R');
    $pdf->Cell(21,6,"DATA:",'L');
    $pdf->Cell(27,6,"DATA:",'R');

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(33,6,"Término:",'L R B');
    $pdf->Cell(32,6,"Início:",'L B');
    $pdf->Cell(38,6,"Término:",'R B');
    $pdf->Cell(47,6,"",'L R B');
    $pdf->Cell(20,6,"Início:",'L B');
    $pdf->Cell(28,6,"Término:",'R B');

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(33,6,"",'L R');
    $pdf->Cell(70,6,"CONFERIDO POR:",'L R');
    $pdf->Cell(47,6,"",'L R');
    $pdf->Cell(48,6,"CONFERIDO POR:",'L R');
    $pdf->Ln();
    $pdf->Cell(33,6,"",'L R');
    $pdf->Cell(70,6,"",'L R');
    $pdf->Cell(47,6,"",'L R');
    $pdf->Cell(48,6,"",'L R');
    $pdf->Ln();
    $pdf->Cell(33,6,"",'L R B');
    $pdf->Cell(70,6,"DATA:",'L R B');
    $pdf->Cell(47,6,"",'L R B');
    $pdf->Cell(48,6,"DATA:",'L R B');

    $pdf->Ln();
    $pdf->Cell(198, 6, "OBSERVAÇÕES:", 'L R');
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 9);
    $pdf->MultiCell(198, 4, $observacao, 'L R B');

    $pdf->Ln();
    $pdf->Cell(198,6,"EXPEDIÇÃO",1);
    $pdf->Ln();
    $pdf->Cell(21,6,"CAIXA",1,'','C');
    $pdf->Cell(21,6,"ROLOS",1,'','C');
    $pdf->Cell(36,6,"ETIQUETAS P/ ROLO",1,'','C');
    $pdf->Cell(21,6,"CAIXA",1,'','C');
    $pdf->Cell(21,6,"ROLOS",1,'','C');
    $pdf->Cell(36,6,"ETIQUETAS P/ ROLO",1,'','C');
    $pdf->Cell(42,6,"DATA",1);
    $pdf->Ln();
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(36,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(36,5,"",1,'','C');
    $pdf->Cell(42,5,"",1);
    $pdf->Ln();
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(36,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(36,5,"",1,'','C');
    $pdf->Cell(42,5,"ASSINATURA",1);
    $pdf->Ln();
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(36,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(36,5,"",1,'','C');
    $pdf->Cell(42,5,"",'L R');
    $pdf->Ln();
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(36,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(21,5,"",1,'','C');
    $pdf->Cell(36,5,"",1,'','C');
    $pdf->Cell(42,5,"",'L R B');

    $pdf->AddPage();

    $pdf->Ln();
    $pdf->SetFont('Arial', 'B',9);
    $pdf->Cell(198,7,"IMAGENS",1,"",'C');

    $pdf->Ln();
    $pdf->Cell(20,6,"COD ITEM",'L R B',"",'C');
    $pdf->Cell(114,6,"ITEM",'L R B',"",'C');
    $pdf->Cell(64,6,"IMAGEM",'L R B',"",'C');

    foreach($itens as $item){
        $pdf->Ln();
        $pdf->Cell(20, 6, $item->clienteProdutoCodigo, 'T L R B', "", 'C');
        $pdf->Cell(114, 6, "($item->produtoCodigo) $item->produtoNome", 'T L R B', "", '');
        $produtoGaleria = new Repository(new ProdutoGaleria);
        $produtoGaleria->setWhere("produtoId = '{$item->produtoId}' and capa = '1' and excluido = '0'");
        $produtoGaleria = $produtoGaleria->listar();
        if(isset($produtoGaleria[0])){
            $img = "../image/produtos/{$produtoGaleria[0]->codigo}.jpg";
            $pdf->Cell(64, 60, $pdf->Image($img, $pdf->GetX(), $pdf->GetY(), 64, 60), 'T L R B', "", '');
        }
    }

    $pdf->Output();

?>