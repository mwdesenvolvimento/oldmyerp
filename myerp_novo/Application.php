<?php

//carrego as classes e etc.
require_once './Config.php';

class Application {
        
        //@var	Application
	private static $instance;

	// @var	ControllerManager
	private $controllerManager;

	private function __construct() {
            $this->controllerManager = ControllerManager::getInstance();

            //TODO: criar um processo automatizado para carregar todos os controllers
            $this->controllerManager->addController(new HomeController());
            $this->controllerManager->addController(new OrdemDeProducaoController());
            $this->controllerManager->addController(new ProdutoController());
            $this->controllerManager->addController(new OrdemDeProducaoHistoricoController());
	}

	/**
	 * Delega a manipulação das requisições feitas à
	 * aplicação ao controlador responsável.
	 */
	public function handle() {
		try {
                    $this->controllerManager->handle();
		} catch ( Exception $e ) {
		    echo $e;
		}
	}

	/**
	 * Recupera a instância da aplicação.
	 * @return	Application */
	public static function getInstance(){
            if(self::$instance == null){ self::$instance = new Application(); }
            return self::$instance;
	}

	/**
	 * Inicializa a aplicação.
	 * @see		Application::handle()
	 */
	public static function start() {
		self::getInstance()->handle();
	}
}
    
    
?>