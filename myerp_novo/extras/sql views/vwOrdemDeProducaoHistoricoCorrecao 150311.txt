SELECT

tblOrdemDeProducaoHistoricoCorrecao.id as id,
historicoId,
vwOrdemDeProducaoHistorico.ordemDeProducaoId as ordemDeProducaoId,
vwOrdemDeProducaoHistorico.ordemDeProducaoStatus as ordemDeProducaoStatus,
vwOrdemDeProducaoHistorico.ordemDeProducaoPrioridade as ordemDeProducaoPrioridade,
vwOrdemDeProducaoHistorico.ordemDeProducaoObservacao as ordemDeProducaoObservacao,
historicoCadastroData,
historicoFuncionarioId,
tblfuncionario.fldNome as historicoFuncionarioNome,
historicoDescricao,
tblOrdemDeProducaoHistoricoCorrecao.usuarioId as usuarioId,
tblusuario.fldUsuario as usuarioNome,
correcaoData,
tblOrdemDeProducaoHistoricoCorrecao.excluido

from tblOrdemDeProducaoHistoricoCorrecao

left join vwOrdemDeProducaoHistorico on tblOrdemDeProducaoHistoricoCorrecao.historicoId = vwOrdemDeProducaoHistorico.id
left join tblfuncionario on tblOrdemDeProducaoHistoricoCorrecao.historicoFuncionarioId = tblfuncionario.fldId
left join tblusuario on tblOrdemDeProducaoHistoricoCorrecao.usuarioId = tblusuario.fldId