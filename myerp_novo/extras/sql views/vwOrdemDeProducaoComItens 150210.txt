select 

tblOrdemDeProducao.id,
tblOrdemDeProducao.aberturaData,
tblOrdemDeProducao.cadastroTimeStamp,
tblOrdemDeProducao.previsaoData,
tblOrdemDeProducao.conclusaoData,
tblOrdemDeProducao.statusId,
tblOrdemDeProducaoStatus.nome as statusNome,
tblOrdemDeProducao.prioridadeId,
tblOrdemDeProducaoPrioridade.nome as prioridadeNome,
tblOrdemDeProducao.impressaoLiberada,
tblOrdemDeProducao.observacao,
tblOrdemDeProducao.lote,
tblOrdemDeProducao.usuarioId,
tblOrdemDeProducao.excluido,
tblpedido_item.fldPedido_Id as itemPedidoId,
tblOrdemDeProducaoItem.produtoId as itemProdutoId,
tblOrdemDeProducaoItem.produtoNome as itemDescricao,
tblOrdemDeProducaoItem.produtoQuantidade as itemQuantidade,
tblcliente.fldNome as itemPedidoCliente,
tblpedido.fldCliente_Id as itemPedidoClienteId,
tblmaquina.nome as itemMaquina

from tblOrdemDeProducao

left join tblOrdemDeProducaoItem 		on tblOrdemDeProducao.id 					= tblOrdemDeProducaoItem.ordemDeProducaoId
left join tblpedido_item 				on tblOrdemDeProducaoItem.pedidoId 			= tblpedido_item.fldId
left join tblpedido 					on tblpedido_item.fldPedido_Id 				= tblpedido.fldId
left join tblcliente 					on tblpedido.fldCliente_Id 					= tblcliente.fldId
left join tblmaquina 					on tblOrdemDeProducaoItem.ProdutoMaquinaId 	= tblmaquina.id
left join tblOrdemDeProducaoStatus	 	on tblOrdemDeProducao.statusId 				= tblOrdemDeProducaoStatus.id
left join tblOrdemDeProducaoPrioridade 	on tblOrdemDeProducao.prioridadeId 			= tblOrdemDeProducaoPrioridade.id