<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de NFe</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />

	</head>
	<body>
    
    <div id="no-print">
        <a class="print" href="#" onClick="window.print()">imprimir</a>
    </div>
    
<?	ob_start();
	session_start();
    
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	$rsDados 	= mysql_query("SELECT * FROM tblempresa_info");
	$rowDados 	= mysql_fetch_array($rsDados);
	
	$rsUsuario 	= mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']);
	$rowUsuario = mysql_fetch_array($rsUsuario);
	
	$rsPedido 	= mysql_query($_SESSION['pedido_nfe_relatorio']);
	
	while($rowPedido = mysql_fetch_array($rsPedido)){	
		$pedido_id = $rowPedido['fldPedidoId'];
		
		//calculando o total do relatorio
		$rsItem = mysql_query("SELECT * FROM tblpedido_item WHERE fldPedido_Id = $pedido_id");
		$subTotalPedido = 0;
		while($rowItem 	= mysql_fetch_array($rsItem)){$totalCompra	+= $valor_compra;}
		$subTotalPedido		= $rowPedido['fldnfe_total'];
		$descPedido 		= $rowPedido['fldDesconto'];
		$descPedidoReais 	= $rowPedido['fldDescontoReais'];
		$subTotalPedido		= $subTotalPedido 	+ $rowPedido['fldComissao'] + $rowPedido['fldValor_Terceiros'] + $rowPedido['fldValorServico'];
		$descontoPedido 	= ($subTotalPedido 	* $descPedido) / 100;
		$totalPedido 		= $subTotalPedido 	- $descontoPedido;
		$totalPedido 		= $totalPedido 		- $descPedidoReais;
		$totalRelatorio 	+= $totalPedido;
	}
	
	unset($subTotalPedido);
	
	/*----------------------------------------------------------------------------------------*/	
	$rsPedido 		= mysql_query($_SESSION['pedido_nfe_relatorio']);
	$totalRegistro 	= mysql_num_rows($rsPedido);
	echo mysql_error();
	$limite = 28;
	$n 		= 1;
	
	$pgTotal = $totalRegistro / $limite;
	$p = 1;
	
	$CPF_CNPJDados = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
	//define se vai aparecer funcionario ou descricao da venda
	$descricao = (isset($_GET['desc'])) ? $descricao = 'Descri&ccedil;&atilde;o' : 'Funcion&aacute;rio';
	
	$tabelaCabecalho ='
				<tr style="border-bottom: 2px solid">
                    <td style="width: 950px"><h1>Relat&oacute;rio de NFe</h1></td>
                    <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 930px" name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
                            <tr>
                                <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                                <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                                <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                                <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                            </tr>
                        </table>	
                    </td>
                    <td>        
                        <table class="dados_impressao">
                            <tr>
                                <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                                <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                                <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="total">
                	<td style="width: 950px">&nbsp;</td>
                	<td>Total selecionado: R$ '.format_number_out($totalRelatorio).'</td>
                    <td style="width:10px;">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio_paisagem" class="table_relatorio_paisagem" summary="Relat&oacute;rio">
                            <tr style="border:none; margin: 0;">
                                <td style="width:60px; margin-left:5px">C&oacute;d.</td>
                                <td style="width:80px">Data venda</td>
                                <td style="width:50px">NFe</td>
                                <td style="width:190px">Cliente</td>
                                <td style="width:200px">'.$descricao.'</td>
                                <td style="width:70px; text-align:right">Total</td>
                                <td style="width:50px; text-align:right">Desc.<br>(%)</td>
                                <td style="width:50px; text-align:right">Desc.<br>(R$)</td>
								<td style="width:55px; text-align:right">Desc.<br>(Itens R$)</td>
                                <td style="width:75px; text-align:right">A pagar</td>
                                <td style="width:50px; text-align:right">Lucro</td>
								<td style="width:45px; text-align:right">Frete</td>
                                <td style="width:50px; text-align:right">Pago</td>
                                <td style="width:50px; text-align:right">Devedor</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio_paisagem" class="table_relatorio_paisagem" summary="Relat&oacute;rio">';
?>	
	<table class="relatorio_print_paisagem" style="page-break-before:avoid">
<?      print $tabelaCabecalho;
		$tabelaCabecalho = '<table class="relatorio_print_paisagem">'.$tabelaCabecalho;        
		while($rowPedido = mysql_fetch_array($rsPedido)){
			$x+= 1;
			$pedido_id 		= $rowPedido['fldPedidoId'];
					
			$rsStatus 		= mysql_query("SELECT * FROM tblpedido_status WHERE fldId =". $rowPedido['fldStatus']);
			$rowStatus		= mysql_fetch_array($rsStatus);
			
			unset($subTotalPedido);
			unset($totalPedido);
			unset($totalCompra);
			unset($totalLucro);
			unset($totalItem);
			unset($descontoItemTotal);
			
			$rsItem 			= mysql_query("SELECT tblpedido_item.*, SUM((tblpedido_item.fldValor * tblpedido_item.fldQuantidade)) AS fldTotalItem,
											  SUM(tblpedido_item_fiscal.flddesconto) AS fldDescontoItemTotal FROM tblpedido_item
											  INNER JOIN tblpedido_item_fiscal ON tblpedido_item_fiscal.fldItem_Id = tblpedido_item.fldId
											  WHERE tblpedido_item.fldPedido_Id = $pedido_id");
			$descricao 			= '';
			
			while($rowItem 		= mysql_fetch_array($rsItem)) {
				$descricao 		   .= $rowItem['fldDescricao'].", ";
				$totalItem 		   += $rowItem['fldTotalItem'];
				$descontoItemTotal += $rowItem['fldDescontoItemTotal'];
			}
			
			$totalNota			= $rowPedido['fldnfe_total'];
			//total pedido
			$descPedido 		= $rowPedido['fldDesconto'];
			$descPedidoReais 	= $rowPedido['fldDescontoReais'];
			$subTotalPedido		= $totalNota + $rowPedido['fldComissao'] + $rowPedido['fldValor_Terceiros'] + $rowPedido['fldValorServico'];
			$descontoPedido 	= ($subTotalPedido 	* $descPedido) / 100;
			$totalPedido 		= $subTotalPedido 	- $descontoPedido;
			$totalPedido 	 	= $totalPedido 		- $descPedidoReais;
			$totalLucro 		+= ($totalPedido - $totalCompra) - $rowPedido['fldComissao'];
			
			$descTotal          += $descPedido + $descPedidoReais + $descontoItemTotal;
			
			$sSQL = "SELECT tblpedido_parcela.fldId, SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor
					FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
					WHERE tblpedido_parcela.fldPedido_Id = $pedido_id  GROUP BY tblpedido_parcela.fldPedido_Id";
					
			$rsBaixa  = mysql_query($sSQL);
			$rowBaixa = mysql_fetch_array($rsBaixa);
			echo mysql_error();
			
			$valorBaixa 	= $rowBaixa['fldBaixaValor'];
			$valorDevedor 	= $totalPedido - $rowBaixa['fldBaixaValor'];
			
			$totalTerceiros	+= $rowPedido['fldValor_Terceiros'];
			$totalItens 	+= $totalItem;
			$totalServicos 	+= $rowPedido['fldValorServico'];
			$totalPedidos 	+= $subTotalPedido;
			$totalLucros 	+= $totalLucro;
			$totalPagar 	+= $totalPedido;
			$totalBaixa 	+= $valorBaixa;
			$totalDevedor 	+= $valorDevedor;
			
			$descricao = (isset($_GET['desc'])) ? substr($descricao, 0, 36) : substr($rowPedido['fldFuncionarioNome'], 0, 30);
			
			//acumulando o frete total
			$frete = mysql_fetch_array(mysql_query("SELECT fldfrete_total FROM tblpedido_fiscal WHERE fldpedido_id = $pedido_id"));
			$totalFrete += $frete['fldfrete_total'];
?>
			<tr>
				<td style="width:60px; margin-left:5px"><?=str_pad($rowPedido['fldPedidoId'], 6, "0", STR_PAD_LEFT)?></td>
				<td style="width:80px"><?=format_date_out($rowPedido['fldPedidoData'])?></td>
				<td style="width:50px"><strong><?=str_pad($rowPedido['fldnumero'], 5, "0", STR_PAD_LEFT)?></strong></td>
				<td style="width:190px"><?=substr($rowPedido['fldClienteNome'],0, 28)?></td>
				<td style="width:200px"><?=$descricao?></td>
				<td style="width:70px; text-align:right"><?=format_number_out($subTotalPedido + $descPedido + $descPedidoReais + $descontoItemTotal)?></td>
				<td style="width:50px; text-align:right"><?=format_number_out($rowPedido['fldDesconto'])?></td>
				<td style="width:50px; text-align:right"><?=format_number_out($rowPedido['fldDescontoReais'])?></td>
				<td style="width:55px; text-align:right"><?=format_number_out($descontoItemTotal)?></td>
				<td style="width:75px; text-align:right"><?=format_number_out($totalPedido)?></td>
				<td style="width:50px; text-align:right"><?=format_number_out($totalLucro)?></td>
				<td style="width:45px; text-align:right"><?=format_number_out($frete['fldfrete_total'])?></td>
				<td style="width:50px; text-align:right"><?=format_number_out($valorBaixa)?></td>
				<td style="width:50px; text-align:right"><?=format_number_out($valorDevedor)?></td>
			</tr>
<?			if(($n == $limite) or ($x == $totalRegistro)){ ?>
							</table>
                        </td>
                    </tr>
                </table>
<?				$n = 1;
				if($x < $totalRegistro){
					$p += 1;
					print str_replace('p&aacute;g. 1 de ', "p&aacute;g. $p de ", $tabelaCabecalho);
				}
			}else{
				$n += 1;
			}
		} ?>
        <table style="width:1152px" name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
            <tr>
                <td style="width:50px">Produtos</td>
                <td style="width:50px; text-align:right"><?=format_number_out($totalItens)?></td>
                <td style="border-right:1px solid">&nbsp;</td> 
               
                <td style="width:40px">Servi&ccedil;os</td>
                <td style="width:40px; text-align:right"><?=format_number_out($totalServicos)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
                                
                <td style="width:40px">Terceiros</td>
                <td style="width:50px; text-align:right"><?=format_number_out($totalTerceiros)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
				
				<td style="width:20px">Desc.</td>
                <td style="width:40px; text-align:right"><?=format_number_out($descTotal)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
                
                <td style="width:30px">Total</td>
                <td style="width:50px; text-align:right"><?=format_number_out($totalPedidos)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
                
                <td style="width:45px">A Pagar</td>
                <td style="width:60px; text-align:right"><?=format_number_out($totalPagar)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
                
                <td style="width:30px">Lucro</td>
                <td style="width:50px; text-align:right"><?=format_number_out($totalLucros)?></td>
                <td style="width:2px; border-left:1px solid">&nbsp;</td>
				
				<td style="width:20px">Frete</td>
                <td style="width:40px; text-align:right"><?=format_number_out($totalFrete)?></td>
                <td style="width:2px; border-left:1px solid">&nbsp;</td>
                
                <td style="width:30px">Pago</td>
                <td style="width:50px; text-align:right"><?=format_number_out($totalBaixa)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
                
                <td style="width:40px">Devedor</td>
                <td style="width:50px; text-align:right"><?=format_number_out($totalDevedor)?></td>
            </tr>                   
        </table>
		
	</body>
</html>