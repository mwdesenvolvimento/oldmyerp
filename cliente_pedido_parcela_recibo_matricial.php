<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
      	<title>myERP - Imprimir venda</title>
        
	</head>
	<body>
       
<?php 
	
	ob_start();
	session_start();

	require_once("inc/con_db.php");
	require_once("inc/fnc_general.php");
	require_once("inc/fnc_imprimir.php");
	require_once("inc/fnc_identificacao.php");
	require_once("inc/fnc_ibge.php");

	$filtro 	= $_GET['filtro'];
	$id_parcela = $_GET['id'];
	
	$rsEmpresa 	= mysql_query("select * from tblempresa_info");
	$rowEmpresa = mysql_fetch_array($rsEmpresa);
	
	if(isset($filtro)){
		$sql = "SELECT tblpedido_parcela_baixa.*, tblpedido_parcela_baixa.fldValor as fldValorBaixa, tblpedido_parcela_baixa.fldDevedor as fldValorDevedor,
				tblpedido_parcela_baixa.fldJuros as fldValorJuros, tblpedido_parcela_baixa.fldMulta as fldValorMulta, tblpedido_parcela_baixa.fldDesconto as fldValorDesconto,
				tblpedido_parcela.fldObservacao, tblpedido_parcela.fldValor as fldValorParcela, tblpedido_parcela.*, tblpagamento_tipo.fldSigla as fldPagamento_Tipo
				FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa
			   	ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
				LEFT JOIN tblpagamento_tipo ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
			  	WHERE tblpedido_parcela_baixa.fldId IN ($filtro) ORDER BY tblpedido_parcela.fldVencimento";

	}elseif(isset($id_parcela)){
		
		$sql = "SELECT tblpedido.*, tblpedido_parcela.fldValor as fldValorParcela, tblpedido_parcela.fldObservacao, tblpedido_parcela.fldId as fldParcela_Id, tblpedido_parcela.*,
				tblpagamento_tipo.fldSigla as fldPagamento_Tipo
				FROM tblpedido_parcela INNER JOIN tblpedido
			   	ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId
				LEFT JOIN tblpagamento_tipo ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
				WHERE tblpedido_parcela.fldId IN ($id_parcela) ORDER BY tblpedido_parcela.fldVencimento";
	}
	$rsBaixa = mysql_query($sql);
	$totalRegistroParcelas = mysql_num_rows($rsBaixa);
	echo mysql_error();
	
	$rsCliente 	= mysql_query("SELECT * FROM tblcliente WHERE fldId =".$_GET['cliente']);
	$rowCliente = mysql_fetch_array($rsCliente);
	
	echo mysql_error();
	$data = date("Y-m-d");
	$hora = date("H:i:s");
	
	//26-11-2012
	$impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
	if(!$impressao_local){
		$impressao_local = fnc_estacao_impressora('todos');
	}
	
	# CABECALHO TXT ###############################################################################################################################################
		$cabecalho 	.= $impressao_local . "\r\n <b>";
		$nomeEmpresa = acentoRemover(strtoupper($rowEmpresa['fldNome_Fantasia']));
		$cabecalho 	.= format_margem_print('---- ****  '.$nomeEmpresa.'  **** ----',80,'centro')."\r\n";
		$cabecalho 	.= "</b>";
		$cabecalhoLinha += 1;
		
		$enderecoEmpresa = substr($rowEmpresa['fldEndereco'],0,41);
		$cabecalho 	.= format_margem_print(strtoupper($enderecoEmpresa).', '.$rowEmpresa['fldNumero'],47,'esquerda');
		$cabecalho 	.= format_margem_print($rowEmpresa['fldTelefone1'],17,'esquerda');
		$cabecalho 	.= format_margem_print($rowEmpresa['fldTelefone2'],16,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		$cabecalho .= format_margem_print("DATA: ".format_date_out(date("Y-m-d")),50,'esquerda');
		$cabecalho .= format_margem_print("HORA: ".date("H:i:s"),30,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		$nome = substr($rowCliente['fldNome'],0,32);
		$cabecalho .= format_margem_print("CLIENTE: <b>".str_pad($rowCliente['fldCodigo'], 6, "0", STR_PAD_LEFT).' '.acentoRemover(strtoupper($nome))."</b>",50,'esquerda');
		$cabecalho .= format_margem_print("TELEFONE: ".$rowCliente['fldTelefone1'],30,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		$cabecalho .= format_margem_print("END.: ".substr($rowCliente['fldEndereco'].' '.$rowCliente['fldNumero'],42),50,'esquerda');
		$cabecalho .= format_margem_print("BAIRRO: ".$rowCliente['fldBairro'],30,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
			
		$cabecalho 	.="--------------------------------------------------------------------------------\r\n";
		$cabecalho 	.= format_margem_print("COMPROVANTE DE PAGAMENTO",80,'centro')."\r\n";
		$cabecalho 	.="--------------------------------------------------------------------------------\r\n";
		$cabecalhoLinha += 3;
		
		$cabecalho .= "<b>";
		$cabecalho .= format_margem_print('VENDA',6,'esquerda').'|';
		$cabecalho .= format_margem_print('PARC',4,'esquerda').'|';
		$cabecalho .= format_margem_print('VENC',8,'centro').'|';
		$cabecalho .= format_margem_print(' VALOR',7,'centro').'|';
		$cabecalho .= format_margem_print(' MULTA',6,'centro').'|';
		$cabecalho .= format_margem_print(' JUROS',6,'centro').'|';
		$cabecalho .= format_margem_print('DESC',6,'centro').'|';
		$cabecalho .= format_margem_print('PAGO',7,'centro').'|';
		$cabecalho .= format_margem_print(' PAGO EM',7,'direita').'|';
		$cabecalho .= format_margem_print('FORMA',5,'esquerda').'|';
		$cabecalho .= format_margem_print('DEVEDOR',8,'direita')."</b>\r\n";
		$cabecalho .="--------------------------------------------------------------------------------\r\n";
		$cabecalhoLinha +=2;
		
		############################################################################################################################################################
		$limite = 31; #LIMITE DE LINHAS POR FOLHA
		$linhasRestantes = $limite - $cabecalhoLinha;
		$texto 			.= $cabecalho;
		############################################################################################################################################################

		while($rowBaixa = mysql_fetch_array($rsBaixa)){
			echo mysql_error();
			
			if(isset($id_parcela)){
				$rsBaixaValor = mysql_query("SELECT SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa,
												SUM(tblpedido_parcela_baixa.fldMulta 	* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaMulta,
												SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaDesconto,
												SUM(tblpedido_parcela_baixa.fldJuros 	* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldJurosTotal, 
												(SELECT tblpedido_parcela_baixa.fldDataRecebido FROM tblpedido_parcela_baixa WHERE fldParcela_Id = tblpedido_parcela.fldId AND fldExcluido = 0) AS fldDataRecebimento,
												tblpedido_parcela_baixa.*, tblpedido_parcela.fldValor as fldValorParcela, tblpedido_parcela.* 
												FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
												WHERE tblpedido_parcela.fldId =".$rowBaixa['fldParcela_Id']." GROUP BY tblpedido_parcela_baixa.fldParcela_Id");
				$rowBaixaValor = mysql_fetch_array($rsBaixaValor);
				echo mysql_error();
				$baixaValor 	= $rowBaixaValor['fldValorBaixa'] + $rowBaixaValor['fldJurosTotal'] + $rowBaixaValor['fldBaixaMulta'];
				$dataRecebido 	= $rowBaixaValor['fldDataRecebimento'];
				$juros 			= $rowBaixaValor['fldJurosTotal'];
				$desconto 		= $rowBaixaValor['fldBaixaDesconto'];
				$multa 			= $rowBaixaValor['fldBaixaMulta'];
				$devedor 		= ($rowBaixaValor['fldValorParcela'] + $rowBaixaValor['fldBaixaMulta'] + $juros) - ($baixaValor + $rowBaixaValor['fldBaixaDesconto']);
			}else{
				$baixaValor 	= $rowBaixa['fldValorBaixa'] + $rowBaixa['fldValorJuros'] + $rowBaixa['fldBaixaMulta'];
				$dataRecebido 	= $rowBaixa['fldDataRecebimento'];
				$juros 			= $rowBaixa['fldValorJuros'];
				$desconto 		= $rowBaixa['fldValorDesconto'];
				$multa 			= $rowBaixa['fldValorMulta'];
				$devedor 		= $rowBaixa['fldValorDevedor'];
			}
			
			$texto .= format_margem_print(str_pad($rowBaixa['fldPedido_Id'], 6, "0", STR_PAD_LEFT),6,'direita').'|';
			$texto .= format_margem_print(str_pad($rowBaixa['fldParcela'], 2, "0", STR_PAD_LEFT),4,'centro').'|';
			$texto .= format_margem_print(format_date_out3($rowBaixa['fldVencimento']),8,'centro').'|';
			$texto .= format_margem_print(format_number_out($rowBaixa['fldValorParcela']),6,'direita').'|';
			$texto .= format_margem_print(format_number_out($multa),6,'direita').'|';
			$texto .= format_margem_print(format_number_out($juros),6,'direita').'|';
			$texto .= format_margem_print(format_number_out($desconto),6,'direita').'|';
			$texto .= format_margem_print(format_number_out($baixaValor),6,'direita').'|';
			$texto .= format_margem_print(format_date_out3($dataRecebido),9,'direita').'|';
			$texto .= format_margem_print($rowBaixa['fldPagamento_Tipo'],5,'direita').'|';
			$texto .= format_margem_print(format_number_out($devedor),7,'direita')."\r\n";
			$linha +=1;
			
			$registroParcela +=1;
			
			if($registroParcela == $totalRegistroParcelas){

				if(fnc_sistema('pedido_exibir_devedor') > 0 ){

					$sSQL = "SELECT tblpedido_parcela.*,
						SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa,
						SUM(tblpedido_parcela_baixa.fldJuros * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldJuros,
						SUM(tblpedido_parcela_baixa.fldMulta * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldMulta,
						SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldDesconto,
						tblpedido.fldId as fldPedidoId
						FROM 
						(tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id)
						INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
						WHERE tblpedido.fldCliente_Id = ".$_GET['cliente']."
						AND tblpedido_parcela.fldStatus = '1' 
						AND tblpedido_parcela.fldExcluido = '0'
						AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0) GROUP BY tblpedido_parcela.fldId";
					$rsParcela = mysql_query($sSQL);
					$rowParcelasPendencia = mysql_num_rows($rsParcela);

					if($rowParcelasPendencia > 0){

						$registroParcelaDevedor = 0;

						$texto 	.="\r\n\r\n".format_margem_print("********** OUTRAS PENDENCIAS ***********",80,'centro')."\r\n";
						$texto	.= "\r\n".format_margem_print("Venda",11,'direita');
						$texto	.= format_margem_print("Venc",12,'direita');
						$texto	.= format_margem_print("Valor",13,'direita');
						$texto	.= format_margem_print("Encargo",14,'direita');
						$texto	.= format_margem_print("Pago",14,'direita');
						$texto	.= format_margem_print("Devedor",14,'direita')."\r\n";
						$texto 	.="--------------------------------------------------------------------------------\r\n";
						$linha += 7;

						while($rowParcela = mysql_fetch_assoc($rsParcela)){

							$valorBaixa = $rowParcela['fldValorBaixa'] + $rowParcela['fldJuros'] + $rowParcela['fldMulta'];
							
							$rsDevedor = mysql_query("SELECT fldDevedor FROM tblpedido_parcela_baixa WHERE fldParcela_Id = ".$rowParcela['fldId']." AND fldExcluido = 0 ORDER BY fldId desc LIMIT 1 ");
							$rowDevedor = mysql_fetch_array($rsDevedor);
							if(mysql_num_rows($rsDevedor)){
								$devedor = $rowDevedor['fldDevedor'];
							}else{
								$devedor = $rowParcela['fldValor'];
							}
							
							$pedido_id 		= format_number_out($rowParcela['fldPedidoId']);  
							$valor	 		= format_number_out($rowParcela['fldValor']); 
							$valorEncargo	= format_number_out($rowParcela['fldJuros'] + $rowParcela['fldMulta']); 
							
							$totalDevedor 	+= $devedor;
							$pedido_id 		= str_pad($rowParcela['fldPedidoId'], 5, "0", STR_PAD_LEFT);
							
							if($devedor > 0){
								/*** gravando no txt ********************************************************/
								$texto.= format_margem_print($pedido_id,11,'direita');
								$texto.= format_margem_print(format_date_out4($rowParcela['fldVencimento']),12,'direita');
								$texto.= format_margem_print($valor,13, 'direita');
								$texto.= format_margem_print($valorEncargo,14, 'direita');
								$texto.= format_margem_print(format_number_out($valorBaixa),14, 'direita');
								$texto.= format_margem_print(format_number_out($devedor),14, 'direita')."\r\n";
								$linha+=1;
							}

							$registroParcelaDevedor += 1;

							if($registroParcelaDevedor == $rowParcelasPendencia){ //se acabou todas as parcelas

								#completa com lnhas em branco
								while($linha <= $linhasRestantes){
									$texto .= "\r\n";
									$linha  += 1;
								}

							}
							elseif($linhas == $linhasRestantes){
								$texto .= "\r\n\r\n";
								$texto .= $cabecalho;
								$linha  = 0;
							}
						}
					}
					else
					{
						while($linha <= $linhasRestantes){
							$texto .= "\r\n";
							$linha  += 1;
						}
					}

				}else{
					while($linha <= $linhasRestantes){
						$texto .= "\r\n";
						$linha  += 1;
					}
				}
			}
			elseif($linhasRestantes == $linha){
				$texto .= "\r\n\r\n";	
				$texto .= $cabecalho;
				$linha  = 0;
			}
			
		}
		$timestamp  = date("Ymd_His");
		
		$local_file = "impressao///inbox///imprimir_recibo_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		$fp 		= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
		$salva		= fwrite($fp, $texto);
		$texto 		= fread($fp, filesize($local_file));
		
		//transformamos as quebras de linha em etiquetas <br>
		$texto = nl2br($texto);
		print $texto;

		unset($total_item);
		if($_GET['np'] > 0){
			require('pedido_imprimir_np_nfiscal.php');
		}
		fclose($fp);
?> 
    
		<script type="text/javascript">
            window.location="<?=$local_file?>";
            var raiz = '<?= $raiz ?>';
            
            if(raiz){
                opener.location.href="index.php?p=pedido&modo="+raiz;
            }
            window.close();
            
        </script> 
	</body>
</html>        
