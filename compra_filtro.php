<?php
	
	$rsStatusPag 	= mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'compra_filtro'");
	$rowStatusPag 	= mysql_fetch_array($rsStatusPag);
	$statusFiltro 	= $rowStatusPag['fldValor'];
	
	// pra pegar os valores padrao do bd
	$_SESSION['sel_compra_status_pagamento'] = (isset($_SESSION['sel_compra_status_pagamento'])) ? $_SESSION['sel_compra_status_pagamento'] : $statusFiltro;

	$_SESSION['chk_compra_nfe_compra']		 = (isset($_SESSION['chk_compra_nfe_compra']))	? $_SESSION['chk_compra_nfe_compra'] : true ;
	
	//recebendo a data do calendário de período
	if(isset($_POST['txt_calendario_data_inicial']) && isset($_POST['txt_calendario_data_final'])){
	    $_SESSION['txt_data1'] = (isset($_POST['txt_calendario_data_inicial'])) ? $_POST['txt_calendario_data_inicial'] : '';
	    $_SESSION['txt_data2'] = (isset($_POST['txt_calendario_data_final']))	? $_POST['txt_calendario_data_final'] 	: '';
	}
	
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_compra_cod'] 				= "";
		$_SESSION['sel_compra_status']				= "todos";
		$_SESSION['sel_compra_status_pagamento']	= $rowStatusPag['fldValor'];
		$_SESSION['sel_compra_fornecedor'] 			= "";
		$_SESSION['txt_compra_num_nota'] 			= "";
		$_SESSION['sel_compra_funcionario'] 		= "";
		$_SESSION['txt_data1']						= "";
		$_SESSION['txt_data2'] 						= "";
		$_SESSION['sel_compra_fatura'] 				= "";
		$_SESSION['sel_compra_numero_nota']			= "";
		$_SESSION['sel_compra_marcador']			= "";
		$_SESSION['chk_compra_nfe_compra']			= true;
	}elseif(isset($_POST['btn_exibir'])){
		
		$_SESSION['txt_compra_cod'] 				= (isset($_POST['txt_compra_cod']) 				? $_POST['txt_compra_cod'] 				: $_SESSION['txt_compra_cod']);
		$_SESSION['sel_compra_status']				= (isset($_POST['sel_compra_status']) 			? $_POST['sel_compra_status'] 			: $_SESSION['sel_compra_status']);
		$_SESSION['sel_compra_status_pagamento']	= (isset($_POST['sel_compra_status_pagamento']) ? $_POST['sel_compra_status_pagamento'] : $_SESSION['sel_compra_status_pagamento']);
		$_SESSION['sel_compra_fornecedor'] 			= (isset($_POST['sel_compra_fornecedor']) 		? $_POST['sel_compra_fornecedor'] 		: $_SESSION['sel_compra_fornecedor']);
		$_SESSION['txt_compra_num_nota'] 			= (isset($_POST['txt_compra_num_nota']) 		? $_POST['txt_compra_num_nota'] 		: $_SESSION['txt_compra_num_nota']);
		$_SESSION['sel_compra_funcionario'] 		= (isset($_POST['sel_compra_funcionario']) 		? $_POST['sel_compra_funcionario'] 		: $_SESSION['sel_compra_funcionario']);
		$_SESSION['txt_data1'] 						= (isset($_POST['txt_data1'])					? $_POST['txt_data1']					: $_SESSION['txt_data1']);
		$_SESSION['txt_data2'] 						= (isset($_POST['txt_data2'])					? $_POST['txt_data2']					: $_SESSION['txt_data2']);
		$_SESSION['sel_compra_fatura'] 				= (isset($_POST['sel_compra_fatura'])			? $_POST['sel_compra_fatura']			: $_SESSION['sel_compra_fatura']);
		$_SESSION['sel_compra_numero_nota'] 		= (isset($_POST['sel_compra_numero_nota'])		? $_POST['sel_compra_numero_nota']		: $_SESSION['sel_compra_numero_nota']);
		$_SESSION['sel_compra_marcador']	 		= (isset($_POST['sel_compra_marcador'])			? $_POST['sel_compra_marcador']			: $_SESSION['sel_compra_marcador']);
		
		
		$_SESSION['chk_compra_nfe_compra'] 			= (isset($_POST['chk_compra_nfe_compra']))		?  true									: false;
		$_SESSION['chk_compra_nfe_saida'] 			= (isset($_POST['chk_compra_nfe_saida']))		?  true									: false;
		$_SESSION['chk_compra_nfe_entrada']			= (isset($_POST['chk_compra_nfe_entrada']))		?  true									: false;
	}
	
?>
<form id="frm-filtro" action="index.php?p=compra" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
    	<li style="height:30px">
<?			$codigo_compra = ($_SESSION['txt_compra_cod'] 	? $_SESSION['txt_compra_cod'] : "c&oacute;digo");
			($_SESSION['txt_compra_cod'] == "código") 		? $_SESSION['txt_compra_cod'] = '' : '';
?>      	<input style="width:75px" type="text" name="txt_compra_cod" id="txt_compra_cod" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo_compra?>"/>
		</li>
    	<li style="height:30px">
            <select id="sel_compra_status" name="sel_compra_status" style="width: 145px" >
            	<option <?=($_SESSION['sel_compra_status'] == 'todos') 	? 'selected="selected"' : '' ?> value="todos">status</option>
            	<option <?=($_SESSION['sel_compra_status'] == '2') 		? 'selected="selected"' : '' ?> value="2">Em andamento</option>
            	<option <?=($_SESSION['sel_compra_status'] == '4') 		? 'selected="selected"' : '' ?> value="4">Entregue</option>
			</select>
		</li>
    	<li style="height:30px">
            <select id="sel_compra_status_pagamento" name="sel_compra_status_pagamento" style="width: 110px" >
                <option <?=($_SESSION['sel_compra_status_pagamento'] == "aberto") 	? 'selected="selected"'	: '' ?> value="aberto">em aberto	</option>
                <option <?=($_SESSION['sel_compra_status_pagamento'] == "total") 	? 'selected="selected"' : '' ?> value="total">pago total	</option>
                <option <?=($_SESSION['sel_compra_status_pagamento'] == "parcial") 	? 'selected="selected"' : '' ?> value="parcial">pago parcial</option>
                <option <?=($_SESSION['sel_compra_status_pagamento'] == "vencidos") ? 'selected="selected"' : '' ?> value="vencidos">vencidos	</option>
                <option <?=($_SESSION['sel_compra_status_pagamento'] == "todos") 	? 'selected="selected"' : '' ?> value="todos">todos			</option>
			</select>
		</li>
    	<li style="height:30px">
            <select style="width: 180px" id="sel_compra_fornecedor" name="sel_compra_fornecedor" >
                <option value="">Fornecedor</option>
<?				$rsFornecedor = mysql_query("select fldId, fldNomeFantasia from tblfornecedor ORDER BY fldNomeFantasia");
				while($rowFornecedor = mysql_fetch_array($rsFornecedor)){
?>					<option <?=($_SESSION['sel_compra_fornecedor'] == $rowFornecedor['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowFornecedor['fldId'] ?>"><?= $rowFornecedor['fldNomeFantasia'] ?></option>
<?				}
?>			</select>
		</li>
    	<li style="height:30px">
            <select style="width:120px" id="sel_compra_usuario" name="sel_compra_usuario" >
                <option value="">Usu&aacute;rio</option>
<?				$rsUsuario = mysql_query("select * from tblusuario ORDER BY fldUsuario ASC");
				while($rowUsuario= mysql_fetch_array($rsUsuario)){
?>					<option <?=($_SESSION['sel_compra_usuario'] == $rowUsuario['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowUsuario['fldId'] ?>"><?= $rowUsuario['fldUsuario'] ?></option>
<?				}
?>			</select>
		</li>
    	<li style="height:30px">
      		<label for="txt_data1">Per&iacute;odo: </label>
<?			$data1 = ($_SESSION['txt_data1'] ? $_SESSION['txt_data1'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 65px" type="text" name="txt_data1" id="txt_data1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
    	<li style="height:30px">
<?			$data2 = ($_SESSION['txt_data2'] ? $_SESSION['txt_data2'] : "");
?>     		<input title="Data final" style="text-align:center;width: 65px" type="text" name="txt_data2" id="txt_data2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,compra" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
    	<li style="height:30px">
            <select id="sel_compra_fatura" name="sel_compra_fatura" style="width: 80px" >
                <option value="">fatura</option>
                <option <?=($_SESSION['sel_compra_fatura'] == "1") ? 'selected="selected"' : '' ?> value="1">faturado</option>
                <option <?=($_SESSION['sel_compra_fatura'] == "0") ? 'selected="selected"' : '' ?> value="0">não faturado</option>
			</select>
		</li>
    	<li style="height:30px">
            <select id="sel_compra_numero_nota" name="sel_compra_numero_nota" style="width:145px" >
                <option value="">numera&ccedil;&atilde;o de nota</option>
                <option <?=($_SESSION['sel_compra_numero_nota'] == "0") ? 'selected="selected"' : '' ?> value="0">sem numera&ccedil;&atilde;o</option>
                <option <?=($_SESSION['sel_compra_numero_nota'] == "1") ? 'selected="selected"' : '' ?> value="1">com  numera&ccedil;&atilde;o</option>
			</select>
		</li>
    	<li style="height:30px">
<?			$compra_num_nota = ($_SESSION['txt_compra_num_nota'] ? $_SESSION['txt_compra_num_nota'] : "n&ordm; da nota");
			($_SESSION['txt_compra_num_nota'] == "nº da nota") ? $_SESSION['txt_compra_num_nota'] = '' : '';
?>      	<input style="width:80px" type="text" name="txt_compra_num_nota" id="txt_compra_num_nota" onfocus="limpar (this,'n&ordm; da nota');" onblur="mostrar (this, 'n&ordm; da nota');" value="<?=$compra_num_nota?>" />
        </li>
		<li style="height:30px">
			<select style="width: 140px" id="sel_compra_marcador" name="sel_compra_marcador" >
                <option value="">Marcador</option>
<?				$rsMarcador = mysql_query("select fldId, fldMarcador FROM tblfinanceiro_conta_fluxo_marcador ORDER BY fldMarcador");
				while($rowMarcador = mysql_fetch_array($rsMarcador)){
?>					<option <?=($_SESSION['sel_compra_marcador'] == $rowMarcador['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowMarcador['fldId'] ?>"><?= $rowMarcador['fldMarcador'] ?></option>
<?				}
?>			</select>
        </li>
		
        <?		($_SESSION["sistema_nfe"] > 0) ? print "<li ><a href='' id='filtro_nfe_exibir' style='width:50px;display:block' class='desc'><small style='margin-left:15px;text-decoration:underline'>mais</small></a></li>" : '' ; ?>
        <li style="float:right">
	        <button type="submit" name="btn_exibir" style="margin:0" title="Exibir">Exibir</button>
        </li>
        <li style="float:right">
        	<button type="submit" name="btn_limpar" style="margin:0" title="Limpar Filtro">Limpar filtro</button>
        </li>
        
<?		if($_SESSION["sistema_nfe"] > 0){ 
?>
            <fieldset id="nfe_filtro" style=" background:none;width:475px;margin-left:10px;display:none"><legend class="none" >NFe</legend>
                <li>
					<input type="checkbox" style="width:25px;float:left" name="chk_compra_nfe_compra" id="chk_compra_nfe_compra" <?=($_SESSION['chk_compra_nfe_compra'] == true ? 'checked ="checked"' : (!isset($_SESSION['chk_compra_nfe_compra'])) ? 'checked ="checked"'  : '')?> />
               		<span style="float:left">Compras</span>
               	</li>
                <li>
                    <input type="checkbox" style="width:25px;float:left" name="chk_compra_nfe_saida" id="chk_compra_nfe_saida" <?=($_SESSION['chk_compra_nfe_saida'] == true ?  'checked ="checked"' : '')?> />
                    <span style="float:left">NFe sa&iacute;da</span>
                </li>
                <li>
                    <input type="checkbox" style="width:25px;float:left" name="chk_compra_nfe_entrada" id="chk_compra_nfe_entrada" <?=($_SESSION['chk_compra_nfe_entrada'] == true ? 'checked ="checked"' : '')?> />
                    <span style="float:left">NFe entrada</span>
                </li>
			</fieldset>
<?		}
?>      

		
    </ul>
  </fieldset>
</form>

<?
		
	if(format_date_in($_SESSION['txt_data1']) != "" || format_date_in($_SESSION['txt_data2']) != ""){
		if($_SESSION['txt_data1'] != "" && $_SESSION['txt_data2'] != ""){
			$filtro .= " AND tblcompra.fldCompraData between '".format_date_in($_SESSION['txt_data1'])."' AND '".format_date_in($_SESSION['txt_data2'])."'";
		}elseif($_SESSION['txt_data1'] != "" && $_SESSION['txt_data2'] == ""){
			$filtro .= " AND tblcompra.fldCompraData >= '".format_date_in($_SESSION['txt_data1'])."'";
		}elseif($_SESSION['txt_data2'] != "" && $_SESSION['txt_data1'] == ""){
			$filtro .= " AND tblcompra.fldCompraData <= '".format_date_in($_SESSION['txt_data2'])."'";
		}
	}
	
	if(($_SESSION['sel_compra_status']) != ""){
		if(($_SESSION['sel_compra_status']) != "todos"){
			$filtro .= " and tblcompra.fldStatus = '".$_SESSION['sel_compra_status']."'";
		}
	}
	
	if(($_SESSION['sel_compra_status_pagamento']) != ""){
		//$filtro_excluido .= " and (tblcompra_parcela.fldExcluido is null or tblcompra_parcela.fldExcluido = 0)";
		switch($_SESSION['sel_compra_status_pagamento']){
			case "aberto":
				$filtro_status = " HAVING (fldValorBaixa = 0 or fldValorBaixa is null)";
			break;
			
			case "total":
				$filtro_status = " HAVING tblcompra_parcela.fldValor <= fldValorBaixa";
			break;

			case "parcial":
				$filtro_status = " HAVING tblcompra_parcela.fldValor > fldValorBaixa";
			break;

			case "vencidos":
				$filtro_status = " HAVING tblcompra_parcela.fldValor > fldValorBaixa AND tblcompra_parcela.fldVencimento < '".date("Y-m-d")."'";
			break;

			case "todos":
				$filtro_status = "";
			break;
		}
	}
	
	
	if(($_SESSION['txt_compra_cod']) != ""){
		
		$filtro .= "AND tblcompra.fldId = '".$_SESSION['txt_compra_cod']."'";
	}
	
	if(($_SESSION['sel_compra_fornecedor']) != ""){
		
		$filtro .= "AND tblfornecedor.fldId = '".$_SESSION['sel_compra_fornecedor']."'";
	}
	
	if(($_SESSION['sel_compra_fatura']) != ""){
		
		$filtro .= "AND tblcompra.fldFaturado = '".$_SESSION['sel_compra_fatura']."'";
	}
	
	if(($_SESSION['txt_compra_num_nota']) != ""){
		
		$filtro .= "AND tblcompra.fldNota = '".$_SESSION['txt_compra_num_nota']."'";
	}
	
	if(($_SESSION['sel_compra_usuario']) != ""){
		
		$filtro .= "AND tblcompra.fldUsuario_Id = '".$_SESSION['sel_compra_usuario']."'";
	}

	if($_SESSION['sel_compra_numero_nota'] != ""){
		if($_SESSION['sel_compra_numero_nota'] == 0 ){
			$filtro .= "AND tblcompra.fldNota = ''";
		}else{
			$filtro .= "AND tblcompra.fldNota != ''";
		}
	}
	
	if(($_SESSION['sel_compra_marcador']) != ""){
		
		$filtro .= "AND tblcompra.fldMarcador = '".$_SESSION['sel_compra_marcador']."'";
	}
	
	
	#FILTRO NFe
	if($_SESSION["sistema_nfe"] > 0){ 
			
		unset($filtro_having);
		if( $_SESSION['chk_compra_nfe_compra'] == true && $_SESSION['chk_compra_nfe_entrada'] == false &&  $_SESSION['chk_compra_nfe_saida'] == false){
			$filtro_nfe = " AND tblcompra.fldTipo_Id != 3";
		}elseif($_SESSION['chk_compra_nfe_compra'] == false){
			$filtro_nfe = " AND tblcompra.fldTipo_Id = 3";
		}else{
			$filtro_nfe = '';
		}
		
		#VERIFICA SE JA TEM ALGUM FILTRO HAVING
		if($filtro_status){
			$filtro_having = ' AND ';
		}else{
			$filtro_having = ' HAVING ';
		}
		
		if($_SESSION['chk_compra_nfe_entrada'] == false && $_SESSION['chk_compra_nfe_saida'] == true){
			$filtro_having .= " tblpedido_fiscal.fldOperacao_Tipo = 1";
		}elseif($_SESSION['chk_compra_nfe_entrada'] == true && $_SESSION['chk_compra_nfe_saida'] == false){
			$filtro_having .= " tblpedido_fiscal.fldOperacao_Tipo = 0";
		}elseif($_SESSION['chk_compra_nfe_entrada'] == true || $_SESSION['chk_compra_nfe_saida'] == true){
			$filtro_having .= " tblpedido_fiscal.fldOperacao_Tipo < 2";
		}
		$filtro .= $filtro_nfe;
	}else{
		$filtro .= " AND fldTipo_Id = 1 ";
	}
	

	$filtro = $filtro." GROUP BY tblcompra.fldId ".$filtro_status;
	
	//transferir para a sessão
	$_SESSION['filtro_compra'] = $filtro;


?>

    <script>

		$('#filtro_nfe_exibir').click(function(event){
    		event.preventDefault();
		 	$("#nfe_filtro").slideToggle(35);
		});
    </script>