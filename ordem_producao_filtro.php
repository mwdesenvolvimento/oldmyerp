<?php

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_op_cod'] 				= "";
		$_SESSION['sel_situacao_op'] 			= "";
		$_SESSION['sel_prioridade_op']		 	= "";
		$_SESSION['sel_maquina_op'] 			= "";
		$_SESSION['sel_impressao_op'] 			= "";
		$_SESSION['sel_periodo_op'] 			= "";
		$_SESSION['txt_data1_op'] 				= "";
		$_SESSION['txt_data2_op'] 				= "";
	}
	else{
		$_SESSION['txt_op_cod'] 		= (isset($_POST['txt_op_cod']) 			? $_POST['txt_op_cod'] 			: $_SESSION['txt_op_cod']);
		$_SESSION['sel_situacao_op'] 	= (isset($_POST['sel_situacao_op']) 	? $_POST['sel_situacao_op'] 	: $_SESSION['sel_situacao_op']);
		$_SESSION['sel_prioridade_op'] 	= (isset($_POST['sel_prioridade_op']) 	? $_POST['sel_prioridade_op'] 	: $_SESSION['sel_prioridade_op']);
		$_SESSION['sel_maquina_op'] 	= (isset($_POST['sel_maquina_op']) 		? $_POST['sel_maquina_op'] 		: $_SESSION['sel_maquina_op']);
		$_SESSION['sel_impressao_op']	= (isset($_POST['sel_impressao_op']) 	? $_POST['sel_impressao_op'] 	: $_SESSION['sel_impressao_op']);
		$_SESSION['sel_periodo_op'] 	= (isset($_POST['sel_periodo_op']) 		? $_POST['sel_periodo_op'] 		: $_SESSION['sel_periodo_op']);
		$_SESSION['txt_data1_op'] 		= (isset($_POST['txt_data1_op']) 		? $_POST['txt_data1_op'] 		: $_SESSION['txt_data1_op']);
		$_SESSION['txt_data2_op'] 		= (isset($_POST['txt_data2_op']) 		? $_POST['txt_data2_op'] 		: $_SESSION['txt_data2_op']);
	}

?>
<form id="frm-filtro" action="index.php?p=ordem_producao" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
		<li>
<?			$codigo = ($_SESSION['txt_op_cod']) ? $_SESSION['txt_op_cod'] : "cod. OP";
			($_SESSION['txt_op_cod'] == "cod. OP") ? $_SESSION['txt_op_cod'] = '' : '';
?>      	<input style="width: 62px" type="text" name="txt_op_cod" id="txt_op_cod" onfocus="limpar (this,'cod. OP');" onblur="mostrar (this, 'cod. OP');" value="<?=$codigo?>"/>
		</li>
		<li>
            <select style="width:120px" id="sel_situacao_op" name="sel_situacao_op" >
                <option value="">Situação</option>
				<option <?=($_SESSION['sel_situacao_op'] == '2') ? 'selected="selected"' : '' ?> value="2">Em andamento</option>
				<option <?=($_SESSION['sel_situacao_op'] == '3') ? 'selected="selected"' : '' ?> value="3">Finalizado</option>
			</select>
		</li>
		<li>
            <select style="width:100px" id="sel_prioridade_op" name="sel_prioridade_op" >
                <option value="">Prioridade</option>
<?				$rsPrioridade = mysql_query("SELECT * FROM tblop_prioridade ORDER BY fldId ASC");
				while($rowPrioridade= mysql_fetch_array($rsPrioridade)){
?>					<option <?=($_SESSION['sel_prioridade_op'] == $rowPrioridade['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowPrioridade['fldId']?>"><?=$rowPrioridade['fldPrioridade']?></option>
<?				}
?>			</select>
		</li>
		<li>
            <select style="width:120px" id="sel_maquina_op" name="sel_maquina_op" >
                <option value="">Máquina</option>
<?				$rsMaquina = mysql_query("SELECT * FROM tblop_maquina ORDER BY fldId ASC");
				while($rowMaquina = mysql_fetch_array($rsMaquina)){
?>					<option <?=($_SESSION['sel_maquina_op'] == $rowMaquina['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowMaquina['fldId']?>"><?=$rowMaquina['fldMaquina']?></option>
<?				}
?>				</select>
		</li>
		<li>
            <select style="width:120px" id="sel_impressao_op" name="sel_impressao_op" >
                <option value="">Impressão</option>
				<option value="1">Liberadas</option>
				<option value="0">Não Liberadas</option>
			</select>
		</li>
        <li>
      		<label for="sel_periodo_op">Per&iacute;odo: </label>
            <select id="sel_periodo_op" name="sel_periodo_op" style="width: 90px" >
            	<option <?=($_SESSION['sel_periodo_op'] == 'abertura') ? 'selected="selected"' : '' ?> value="abertura">Abertura</option>
				<option <?=($_SESSION['sel_periodo_op'] == 'previsao') ? 'selected="selected"' : '' ?> value="previsao">Previsão</option>
				<option <?=($_SESSION['sel_periodo_op'] == 'finalizado') ? 'selected="selected"' : '' ?> value="finalizado">Finalizado</option>
			</select>
		</li>
        <li>
<?			$data1 = ($_SESSION['txt_data1_op'] ? $_SESSION['txt_data1_op'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 70px" type="text" name="txt_data1_op" id="txt_data1_op" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
		<li>
<?			$data2 = ($_SESSION['txt_data2_op'] ? $_SESSION['txt_data2_op'] : "");
?>     		<input title="Data final" style="text-align:center;width: 70px" type="text" name="txt_data2_op" id="txt_data2_op" class="calendario-mask" value="<?=$data2?>"/>
			<a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,pedido" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
        <li style="float:right">
        	<button type="submit" name="btn_exibir" style="margin:0" title="Exibir">Exibir</button>
        	<button type="submit" name="btn_limpar" style="margin:0 3px" title="Limpar Filtro">Limpar filtro</button>
        </li>
    </ul>
	</fieldset>
</form>

<?
	$filtro = '';
	if($_SESSION['txt_op_cod'] != ""){
		$filtro .= " AND tblop.fldId = ".$_SESSION['txt_op_cod'];
	}
	
	if($_SESSION['sel_situacao_op'] != ""){
		$filtro .= " AND tblop.fldStatus = ".$_SESSION['sel_situacao_op'];
	}
	
	if($_SESSION['sel_prioridade_op'] != ""){
		$filtro .= " AND tblop.fldPrioridade = ".$_SESSION['sel_prioridade_op'];
	}
	
	if($_SESSION['sel_maquina_op'] != ""){
		$filtro .= " AND tblop.fldMaquina = ".$_SESSION['sel_maquina_op'];
	}
	
	if($_SESSION['sel_impressao_op'] != ""){
		$filtro .= " AND tblop.fldImpressao = ".$_SESSION['sel_impressao_op'];
	}
	
	if(format_date_in($_SESSION['txt_data1_op']) != "" || format_date_in($_SESSION['txt_data2_op']) != ""){
		if($_SESSION['sel_periodo_op'] == 'abertura'){$campoData = 'fldAbertura';}
		elseif($_SESSION['sel_periodo_op'] == 'previsao'){$campoData = 'fldPrevisao';}
		elseif($_SESSION['sel_periodo_op'] == 'finalizado'){$campoData = 'fldFinalizado';}
		
		if($_SESSION['txt_data1_op'] != "" && $_SESSION['txt_data2_op'] != ""){
			$filtro .= " AND tblop.".$campoData." BETWEEN '".format_date_in($_SESSION['txt_data1_op'])."' AND '".format_date_in($_SESSION['txt_data2_op'])."'";
		}elseif($_SESSION['txt_data1_op'] != "" && $_SESSION['txt_data2_op'] == ""){
			$filtro .= " AND tblop.".$campoData." >= '".format_date_in($_SESSION['txt_data1_op'])."'";
		}elseif($_SESSION['txt_data2_op'] != "" && $_SESSION['txt_data1_op'] == ""){
			$filtro .= " AND tblop.".$campoData." <= '".format_date_in($_SESSION['txt_data2_op'])."'";}
	}
		
	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_op'] = $filtro;
	}
	elseif(isset($_POST['btn_limpar'])){
		$_SESSION['filtro_op'] = "";
	}
?>