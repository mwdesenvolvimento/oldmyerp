Create View `vwfinanceiro_conta_fluxo` as
select
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldId` AS `fldId`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldData` AS `fldData`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldDataInserida` AS `fldDataInserida`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldHora` AS `fldHora`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldUsuario_Id` AS `fldUsuario_Id`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldDescricao` AS `fldDescricao`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldEntidade` AS `fldEntidade`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldCredito` AS `fldCredito`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldDebito` AS `fldDebito`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldPagamento_Tipo_Id` AS `fldPagamento_Tipo_Id`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldReferencia_Id` AS `fldReferencia_Id`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldConta_Origem` AS `fldConta_Origem`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldConta_Destino` AS `fldConta_Destino`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldConta_Id` AS `fldConta_Id`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldMovimento_Tipo` AS `fldMovimento_Tipo`,
    `myerp` . `tblfinanceiro_conta_fluxo` . `fldMarcador_Id` AS `fldMarcador_Id`,
    if(
        (
            `myerp` . `tblfinanceiro_conta_fluxo` . `fldEntidade` = ''
        ),
        (
            case
                `myerp` . `tblfinanceiro_conta_fluxo` . `fldMovimento_Tipo`
                when '1' then `myerp` . `tblfinanceiro_conta_fluxo` . `fldEntidade`
                when '2' then convert(
                    (
                        select
                            `myerp` . `tblfinanceiro_conta` . `fldNome` AS `fldNome`
                        from
                            `myerp` . `tblfinanceiro_conta`
                        where
                            (
                                `myerp` . `tblfinanceiro_conta` . `fldId` = if(
                                    (
                                        `myerp` . `tblfinanceiro_conta_fluxo` . `fldCredito` > 0
                                    ),
                                    `myerp` . `tblfinanceiro_conta_fluxo` . `fldConta_Origem`,
                                    `myerp` . `tblfinanceiro_conta_fluxo` . `fldConta_Destino`
                                )
                            ) limit 1
                    )
                        using utf8
                )
                when '3' then convert(
                    (
                        select
                            `myerp` . `tblcliente` . `fldNome` AS `fldNome`
                        from
                            (
                                (
                                    (
                                        `myerp` . `tblpedido_parcela_baixa` left join `myerp` . `tblpedido_parcela` on
                                        (
                                            (
                                                `myerp` . `tblpedido_parcela_baixa` . `fldParcela_Id` = `myerp` . `tblpedido_parcela` . `fldId`
                                            )
                                        )
                                    ) left join `myerp` . `tblpedido` on
                                    (
                                        (
                                            `myerp` . `tblpedido_parcela` . `fldPedido_Id` = `myerp` . `tblpedido` . `fldId`
                                        )
                                    )
                                ) left join `myerp` . `tblcliente` on
                                (
                                    (
                                        `myerp` . `tblpedido` . `fldCliente_Id` = `myerp` . `tblcliente` . `fldId`
                                    )
                                )
                            )
                        where
                            (
                                `myerp` . `tblpedido_parcela_baixa` . `fldId` = `myerp` . `tblfinanceiro_conta_fluxo` . `fldReferencia_Id`
                            ) limit 1
                    )
                        using utf8
                )
                when '4' then convert(
                    (
                        select
                            `myerp` . `tblfornecedor` . `fldNomeFantasia` AS `fldNomeFantasia`
                        from
                            (
                                (
                                    (
                                        `myerp` . `tblcompra_parcela_baixa` left join `myerp` . `tblcompra_parcela` on
                                        (
                                            (
                                                `myerp` . `tblcompra_parcela_baixa` . `fldParcela_Id` = `myerp` . `tblcompra_parcela` . `fldId`
                                            )
                                        )
                                    ) left join `myerp` . `tblcompra` on
                                    (
                                        (
                                            `myerp` . `tblcompra_parcela` . `fldCompra_Id` = `myerp` . `tblcompra` . `fldId`
                                        )
                                    )
                                ) left join `myerp` . `tblfornecedor` on
                                (
                                    (
                                        `myerp` . `tblcompra` . `fldFornecedor_Id` = `myerp` . `tblfornecedor` . `fldId`
                                    )
                                )
                            )
                        where
                            (
                                `myerp` . `tblcompra_parcela_baixa` . `fldId` = `myerp` . `tblfinanceiro_conta_fluxo` . `fldReferencia_Id`
                            ) limit 1
                    )
                        using utf8
                )
                when '6' then(
                    select
                        `myerp` . `tblfinanceiro_conta_pagar_programada` . `fldNome` AS `fldNome`
                    from
                        (
                            `myerp` . `tblfinanceiro_conta_pagar_programada_baixa` left join `myerp` . `tblfinanceiro_conta_pagar_programada` on
                            (
                                (
                                    `myerp` . `tblfinanceiro_conta_pagar_programada_baixa` . `fldContaProgramada_Id` = `myerp` . `tblfinanceiro_conta_pagar_programada` . `fldId`
                                )
                            )
                        )
                    where
                        (
                            `myerp` . `tblfinanceiro_conta_pagar_programada_baixa` . `fldId` = `myerp` . `tblfinanceiro_conta_fluxo` . `fldReferencia_Id`
                        ) limit 1
                )
                when '7' then convert(
                    (
                        select
                            `myerp` . `tblfuncionario` . `fldNome` AS `fldNome`
                        from
                            (
                                `myerp` . `tblfuncionario_conta_fluxo` left join `myerp` . `tblfuncionario` on
                                (
                                    (
                                        `myerp` . `tblfuncionario_conta_fluxo` . `fldFuncionario_Id` = `myerp` . `tblfuncionario` . `fldId`
                                    )
                                )
                            )
                        where
                            (
                                `myerp` . `tblfuncionario_conta_fluxo` . `fldId` = `myerp` . `tblfuncionario_conta_fluxo` . `fldReferencia_Id`
                            ) limit 1
                    )
                        using utf8
                )
                when '8' then convert(
                    (
                        select
                            `myerp` . `tblfuncionario` . `fldNome` AS `fldNome`
                        from
                            (
                                `myerp` . `tblfuncionario_conta_fluxo` left join `myerp` . `tblfuncionario` on
                                (
                                    (
                                        `myerp` . `tblfuncionario_conta_fluxo` . `fldFuncionario_Id` = `myerp` . `tblfuncionario` . `fldId`
                                    )
                                )
                            )
                        where
                            (
                                `myerp` . `tblfuncionario_conta_fluxo` . `fldId` = `myerp` . `tblfuncionario_conta_fluxo` . `fldReferencia_Id`
                            ) limit 1
                    )
                        using utf8
                )
                when '9' then convert(
                    (
                        select
                            `myerp` . `tblfuncionario` . `fldNome` AS `fldNome`
                        from
                            (
                                `myerp` . `tblfuncionario_conta_fluxo` left join `myerp` . `tblfuncionario` on
                                (
                                    (
                                        `myerp` . `tblfuncionario_conta_fluxo` . `fldFuncionario_Id` = `myerp` . `tblfuncionario` . `fldId`
                                    )
                                )
                            )
                        where
                            (
                                `myerp` . `tblfuncionario_conta_fluxo` . `fldId` = `myerp` . `tblfuncionario_conta_fluxo` . `fldReferencia_Id`
                            ) limit 1
                    )
                        using utf8
                )
            end
        ),
        `myerp` . `tblfinanceiro_conta_fluxo` . `fldEntidade`
    ) AS `Entidade`
from
    `myerp` . `tblfinanceiro_conta_fluxo`
