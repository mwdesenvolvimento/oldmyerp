<?
	require("inc/con_db.php");
	require("produto_lente_filtro.php");

	//a��es em grupo
	if(isset($_POST['hid_action'])){
		require("produto_lente_action.php");
	}

/**************************** ORDER BY *******************************************/
	$filtroOrder  = 'fldLente ';
	$class 		  = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_produto_lente']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "fldId";				break;
			case 'lente'		:  $filtroOrder = "fldLente"; 			break;
			case 'cadastro'		:  $filtroOrder = "fldCadastroData"; 	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_produto_lente'] = (!$_SESSION['order_produto_lente'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_produto_lente'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=produto&amp;modo=lente$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_produto_lente']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
		
/**************************** PAGINA��O *******************************************/
	$sSQL =  "SELECT * FROM tblproduto_otica_lente ". $_SESSION['filtro_produto_lente']."  ORDER BY " . $_SESSION['order_produto_lente'];
	
	$rsTotal 	= mysql_query($sSQL);	
	echo mysql_error();
	$rowsTotal 	= mysql_num_rows($rsTotal);
	
	//defini��o dos limites
	$limite 	= 150;
	$n_paginas 	= 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL 		.= " limit " . $inicio . "," . $limite;
	$rsLente 	= mysql_query($sSQL);
	$pagina 	= (isset($_GET['pagina'])) ? $_GET['pagina'] : "1";
	
#########################################################################################
?>
    <form class="table_form" id="frm_produto" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:80px">
                    	<a <?= ($filtroOrder == 'fldId') 			? "class='$class'" : '' ?> style="width:65px" href="<?=$raiz?>codigo">C&oacute;d.</a>
                    </li>
                    <li class="order" style="width:655px">
                    	<a <?= ($filtroOrder == 'fldLente') 		? "class='$class'" : '' ?> style="width:585px" href="<?=$raiz?>lente">Lente</a>
                    </li>
                    <li class="order" style="width:150px">
                    	<a <?= ($filtroOrder == 'fldCadastroData') 	? "class='$class'" : '' ?> style="width:135px" href="<?=$raiz?>cadastro">Cadastro</a>
                    </li>
                    <li style="width:20px">&nbsp;</li>
                    <li style="width:15px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de lentes">
                	<tbody>
<?					
						$id_array 	= array();
						$n			= 0;
						$linha 		= "row";
						$rows 		= mysql_num_rows($rsLente);
						while($rowLente 	= mysql_fetch_array($rsLente)){
							$id_array[$n] 	= $rowLente["fldId"];
							$n += 1;
								
?>							<tr class="<?= $linha; ?>">
                                <td class="cod"	style="width:80px;text-align:right;padding-right:20px"><?= str_pad($rowLente['fldId'], 4, "0", STR_PAD_LEFT)?></td>
                                <td style="width:640px"><?=$rowLente['fldLente']?></td>
                                <td style="width:150px"><?=format_date_out($rowLente['fldCadastroData'])?></td>
                                <td style="width:20px"><a class="edit modal " href="produto_lente,<?=$rowLente['fldId']?>" rel="520-150" title="editar"></a></td>
                                <td style="width:15px"><input type="checkbox" name="chk_lente_<?=$rowLente['fldId']?>" id="chk_lente_<?=$rowLente['fldId']?>" title="selecionar o registro posicionado" /></td>
    	                    </tr>
<?	                      $linha = ($linha == "row") ? "dif-row" : "row";
                  		}
?>		 			</tbody>
				</table>
            </div>
       	  
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="modal btn_novo" href="produto_lente"rel="520-150">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" value="excluir" title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                    
				</ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=produto&amp;modo=lente";
				include("paginacao.php")
?>          </div>  
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>      
        </div>
	</form>