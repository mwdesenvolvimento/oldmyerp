<?
		$sSQL = "SELECT 
					tblproduto.fldCodigo, 
					tblproduto.fldNome, 
					tblpedido_consignado.fldRota_Id, 
					tblendereco_rota.fldRota, 
					SUM(tblpedido_consignado_item.fldQuantidade_Avista + tblpedido_consignado_item.fldQuantidade_Aprazo) as fldTotalQuantidade,
					SUM((tblpedido_consignado_item.fldValor * tblpedido_consignado_item.fldQuantidade_Avista) 
						+ 
						(tblpedido_consignado_item.fldValor * tblpedido_consignado_item.fldQuantidade_Aprazo)) as fldTotalValor
					FROM tblpedido_consignado 
						INNER JOIN tblpedido_consignado_item ON tblpedido_consignado.fldId 	= tblpedido_consignado_item.fldConsignado_Id
						INNER JOIN tblproduto ON tblpedido_consignado_item.fldProduto_Id 	= tblproduto.fldId
						LEFT  JOIN tblendereco_rota ON tblendereco_rota.fldId 				= tblpedido_consignado.fldRota_Id
					WHERE tblpedido_consignado.fldExcluido = 0 AND fldFinalizado = '1'
					" . $_SESSION['filtro_pedido_consignado']."
				   
					GROUP BY tblpedido_consignado_item.fldProduto_Id
					ORDER BY tblpedido_consignado.fldRota_Id, tblproduto.fldNome";
			
        
        $rsConsignadoItem 	= mysql_query($sSQL);
		$rowsItem 	= mysql_num_rows($rsConsignadoItem);
		
		$n	 			= 1; #DEFINE O NUMERO DO BLOCO
		$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
		$limite 		= 42;
		
		$pgTotal 		= $rowsItem / $limite;
		$p = 1;

		$contadorCabecalho = '<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Consignados - Itens</h1></td>
                <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
            </tr>';
		$tabelaCabecalho =' 
            <tr style="margin:0">
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
			<tr class="total">
				<td style="width:580px">&nbsp;</td>
				<td>Movimento '.$_SESSION['txt_pedido_item_data_inicial'].' a '.$_SESSION['txt_pedido_item_data_final'].'</td>
				<td style="width: 6px">&nbsp;</td>
			</tr>
            <tr>
                <td>
                    <table class="table_relatorio" summary="Relat&oacute;rio">
                        <tr style="border:none; margin:8px 0 3px 0">
                            <td style="width:60px;text-align:right;margin-right:05px;font-weight:bold">Rota</td>
                            <td style="width:75px;text-align:right;margin-right:10px;font-weight:bold">Código</td>
                            <td style="width:420px;text-align:left;font-weight:bold">Produto</td>
							<td class="valor" style="width:103px;text-align:right;font-weight:bold">Qtde</td>
							<td class="valor" style="width:95px;text-align:right;font-weight:bold">Valor Total</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';

?>

    <div id="no-print">
        <a class="print" href="#" onClick="window.print()">imprimir</a>
    </div>

	<table class="relatorio_print" style="page-break-before:avoid">
<?      print $contadorCabecalho.$tabelaCabecalho;

	######################################################################################################################################################################################
	########### ITENS ####################################################################################################################################################################

		while($rowConsignadoItem = mysql_fetch_array($rsConsignadoItem)){
			echo mysql_error(); ?>
			
			<tr>
				<td style="width:60px;text-align:right;margin-right:5px;"><?=$rowConsignadoItem['fldRota']?></td>
				<td style="width:75px;text-align:right;margin-right:10px;"><?=$rowConsignadoItem['fldCodigo']?></td>
				<td style="width:460px;text-align:left; display:inline; overflow:hidden"><?=$rowConsignadoItem['fldNome']?></td>
				<td class="valor" style="width:65px;text-align:right;"><?=format_number_out($rowConsignadoItem['fldTotalQuantidade'])?></td>
				<td class="valor" style="width:95px;text-align:right;"><?=format_number_out($rowConsignadoItem['fldTotalValor'])?></td>
			</tr>
			
<?	#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
						if(($n == $limite) or ($x == $rowsItem)){
?>										</table>    
									</td>
								</tr>
							</table>		
<?        									
							if($x < $rowsItem){
								$p += 1;
								$contadorCabecalho = '<tr style="border-bottom: 2px solid">
						                <td style="width: 600px"><h1>Relat&oacute;rio de Consignados - Itens</h1></td>
						                <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
						            </tr>';
								$n = 1;
								print '<table class="relatorio_print">'.$contadorCabecalho.$tabelaCabecalho;
							}
						}
						else {$x+= 1; $n += 1;}
		}
?>               
		
