<?Php

		$compra = (isset($_POST['chk_valor_compra'])) ? true : false;

		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Estoque</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border-top: 1px solid">
				<td style="width:75px;margin-right:10px; text-align:right">C&oacute;d</td>';
			
				if($agrupar == 'validade'){
					$tabelaCabecalho2 .=
					'<td style="width:575px">Produto</td>
					<td style="width:60px; text-align:right">Validade</td>
					<td style="width:80px; text-align:center">Estoque</td>';
				}else{
					if($compra){
						$tabelaCabecalho2 .='
						<td style="width:445px">Produto</td>
						<td style="width:100px;text-align:center">Vl. Compra</td>
						<td style="width:100px;text-align:center">Compra Total</td>
						<td style="width:70px;text-align:center">Estoque</td>';
					} else {
						$tabelaCabecalho2 .='
						<td style="width:645px">Produto</td>
						<td style="width:70px;text-align:center">Estoque</td>';
					}
				}
				$tabelaCabecalho2 .= '
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
		##########################################################################################################################################################################################
			
			if($agrupar  == 'validade'){
				$group_by 	= ', tblproduto_estoque_movimento.fldValidade_Data';
				//$having 	= ($_SESSION['txt_estoque_quantidade'] != '') ? ' AND fldEstoqueQuantidade > 0': ' HAVING fldEstoqueQuantidade > 0' ;
				if(format_date_in($_POST['txt_periodo_inicio']) != "" || format_date_in($_POST['txt_periodo_final']) != ""){
					$data_inicial	= format_date_in($_POST['txt_periodo_inicio']);
					$data_final 	= format_date_in($_POST['txt_periodo_final']);
					if($data_inicial != "" && $data_final != ""){$having .= ' HAVING fldValidade_Data BETWEEN "'.$data_inicial.'" AND "'.$data_final.'"';}
					elseif($data_inicial != "" && $data_final == ""){$having .= ' HAVING fldValidade_Data >= "'.$data_inicial.'"';}
					elseif($data_inicial == "" && $data_final != ""){$having .= ' HAVING fldValidade_Data <= "'.$data_final.'"';}
				}
				
				$consulta = $_SESSION['relatorio_estoque'];
				
			} else {
				
				$data 	= format_date_in($_POST['txt_estoque_dia']);
				
				if($data != null and $data != ""){
					$find 		= "tblproduto_estoque_movimento.fldEstoque_Id = '1'";
					$replace 	= "tblproduto_estoque_movimento.fldEstoque_Id = '1' AND tblproduto_estoque_movimento.fldData <= '$data' ";
					$consulta 	= str_replace($find, $replace, $_SESSION['relatorio_estoque']);
				} else {
					$consulta = $_SESSION['relatorio_estoque'];
				}
			
			}
			
			
			
			/*
			else{
				$having = ($_SESSION['txt_estoque_quantidade'] != '') ? ' AND fldEstoqueQuantidade > 0': ' HAVING fldEstoqueQuantidade > 0';
			}
			$sSQL 			= $_SESSION['relatorio_estoque']. $group_by .$_SESSION['txt_estoque_quantidade'].$having
			*/
			
			
			
			$sSQL 			= $consulta. $group_by .$_SESSION['txt_estoque_quantidade'] . $having
							  ." ORDER BY ". $_SESSION['order_estoque']
							  .", tblproduto_estoque_movimento.fldData, tblproduto_estoque_movimento.fldHora, tblproduto_estoque_movimento.fldId";
							  
			$rsEstoque  	= mysql_query($sSQL);
			echo mysql_error();

			$rowsEstoque 	= mysql_num_rows($rsEstoque);
		
			$n	 			= 1; #DEFINE O NUMERO DA DO BLOCO
			$countRegistro  = 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE DE 20
			$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
			$limite 		= 45;
			
			$pgTotal 		= ceil($rowsEstoque / $limite);
			$p = 1;

			$total_compra 	= 0;
			$total_itens	= 0;
			while($rowEstoque = mysql_fetch_array($rsEstoque)){
				echo mysql_error();
				$estoque 		= $rowEstoque['fldEstoqueQuantidade'];
				$validade		= ($agrupar == 'validade') ? format_date_out3($rowEstoque['fldValidade_Data']) : '' ;
				$vCompra 		= $rowEstoque['fldValorCompra'];
				$vVenda 		= $rowEstoque['fldValorVenda'];
				$total_compra  += ($vCompra * $estoque);
				$total_itens   += $estoque;
				
				$pagina[$n] .='
				<tr>
                	<td style="width:75px; text-align:right">'.str_pad($rowEstoque['fldCodigo'], 6, "0", STR_PAD_LEFT).'</td>';
					if($agrupar == 'validade'){
						$pagina[$n] .='              
						<td style="width:545px">'.substr($rowEstoque['fldNome'],0,83).'</td>
						<td style="width:60px; text-align:right">'.$validade.'</td>
						<td style="width:80px; text-align:right">'.format_number_out($estoque).'</td>';
					}else{
						if($compra){
							$pagina[$n] .= '                          
							<td style="width:412px">'.substr($rowEstoque['fldNome'],0,100).'</td>
							<td style="width:100px; text-align:right">'.format_number_out($vCompra).'</td>
							<td style="width:100px; text-align:right">'.format_number_out($vCompra * $estoque).'</td>
							<td style="width:61px; text-align:right">'.format_number_out($estoque).'</td>';
						} else {
							$pagina[$n] .='                          
							<td style="width:625px">'.substr($rowEstoque['fldNome'],0,100).'</td>
							<td style="width:70px; text-align:right">'.format_number_out($estoque).'</td>';
						}
					}
					$pagina[$n] .='
				</tr>';
				
				
				#SE CHEGAR A 20 LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
				if($countRegistro == $limite){
					$countRegistro = 1;
					$n ++;
				}elseif($rowsEstoque == $x && $countRegistro < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU 20 LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
					while($countRegistro <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countRegistro++;}
				}else{
					$countRegistro ++;
				}
				$x ++;
			}
			
		#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
				
                print '<td style="width: 200px"><p class="pag">'.$p.' de '.$pgTotal.'</p></td>';
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				echo  $pagina[$x];
							
?>
						</table>
					</td >
				</tr>
			</table>
<?			$x ++;
			$p ++;
		}
?>

    <table name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
        <tr>
	        <td style="<?=($compra) ? 'width:400px' : 'width:610px'?>"></td>
            <td style="width:80px">Total de itens</td>
            <td style="width:80px; text-align:right"><?=format_number_out($total_itens)?></td>
            <? if($compra){ ?>
            <td style="width:3px; border-left:1px solid">&nbsp;</td>
            <td style="width:100px">Total de Compra</td>
            <td style="width:80px; text-align:right"><?=format_number_out($total_compra)?></td>
            <? } ?>
        </tr>
    </table>