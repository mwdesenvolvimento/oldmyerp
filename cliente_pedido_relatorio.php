<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Vendas</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />

	</head>
	<body>
    
    <div id="no-print">
        <a class="print" href="#" onClick="window.print()">imprimir</a>
    </div>
    
<?	ob_start();
	session_start();
    
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	$rsDados 	= mysql_query("SELECT * FROM tblempresa_info");
	$rowDados 	= mysql_fetch_array($rsDados);
	
	$rsUsuario 	= mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']);
	$rowUsuario = mysql_fetch_array($rsUsuario);
	
	$rsPedido 	= mysql_query($_SESSION['cliente_pedido_relatorio']);
	echo mysql_error();
	while($rowPedido = mysql_fetch_array($rsPedido)){	
		$pedido_id = $rowPedido['fldPedidoId'];
		
		//calculando o total do relatorio
		$rsItem = mysql_query("SELECT * FROM tblpedido_item WHERE fldPedido_Id = $pedido_id");
		$subTotalPedido = 0;
		while($rowItem 	= mysql_fetch_array($rsItem)){
			
			$valor_item 	= $rowItem['fldValor'];
			$qtd 			= $rowItem['fldQuantidade'];
			$total_item 	= $valor_item * $qtd;
			
			//subtotal
			$desconto 		= $rowItem['fldDesconto'];
			$totalDesconto 	= ($total_item * $desconto)/100;
			$totalValor 	= $total_item - $totalDesconto;
			$subTotalPedido += $totalValor;
			$totalCompra	+= $valor_compra;
		}
		
		$descPedido 		= $rowPedido['fldDesconto'];
		$descPedidoReais 	= $rowPedido['fldDescontoReais'];
		$subTotalPedido		= $subTotalPedido 	+ $rowPedido['fldComissao'] + $rowPedido['fldValor_Terceiros'] + $rowPedido['fldValorServico'];
		$descontoPedido 	= ($subTotalPedido 	* $descPedido) / 100;
		$totalPedido 		= $subTotalPedido 	- $descontoPedido;
		$totalPedido 		= $totalPedido 		- $descPedidoReais;
		$totalRelatorio 	+= $totalPedido;
		
		$cliente_nome		= $rowPedido['fldNomeCliente'];
		$clienteCPF			= $rowPedido['fldCPF_CNPJ'];
		$clienteCPF_tipo	= $rowPedido['fldCPF_Tipo'];
	}
	
	unset($subTotalPedido);	
	/*----------------------------------------------------------------------------------------*/	
	$rsPedido 		= mysql_query($_SESSION['cliente_pedido_relatorio']);
	$totalRegistro 	= mysql_num_rows($rsPedido);
	echo mysql_error();
	$limite = 28;
	$n 		= 1;
	
	if($_SESSION['sistema_tipo'] == 'automotivo'){
		$cabecalho_status  = '<td style="width:60px">Placa</td>';
		$cabecalho_status .= '<td style="width:50px">KM</td>';
	}else{
		$cabecalho_status = '<td style="width:110px">Status</td>';
	}
	
	$pgTotal = $totalRegistro / $limite;
	$p = 1;
	
	$CPF_CNPJDados	 = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
	$CPF_CNPJCliente = formatCPFCNPJTipo_out($clienteCPF, $clienteCPF_tipo);
	
	$nfe_cabecalho 	= ($_SESSION["sistema_nfe"] > 0) ? 'NFe' : '';
	$tabelaCabecalho ='
				<tr style="border-bottom: 2px solid">
                    <td style="width: 950px"><h1>Relat&oacute;rio de Vendas</h1></td>
                    <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 930px" name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
                            <tr>
                                <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                                <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                                <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                                <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
								
                                <td style="width: 320px;">Cliente: '.$cliente_nome.'</td>
                                <td style="width: 200px;">CPF/CNPJ: '.$CPF_CNPJCliente.'</td>
                            </tr>
                        </table>	
                    </td>
                    <td>        
                        <table class="dados_impressao">
                            <tr>
                                <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                                <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                                <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="total">
                	<td style="width: 950px">&nbsp;</td>
                	<td>Total selecionado: R$ '.format_number_out($totalRelatorio).'</td>
                    <td style="width:10px;">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio_paisagem" class="table_relatorio_paisagem" summary="Relat&oacute;rio">
                            <tr style="border:none">
                                <td style="width:50px; margin-left:5px;margin-right:10px">C&oacute;d.</td>
                                <td style="width:60px">Data venda</td>
                                <td style="width:50px; text-align:center">'.$nfe_cabecalho.'</td>
                                <td style="width:190px">Funcion&aacute;rio</td>
                                <td style="width:250px">Descri&ccedil;&atilde;o</td>
								'.$cabecalho_status.'
								<td style="width:70px; text-align:right">A pagar</td>
                                <td style="width:60px; text-align:right">Desc.(%)</td>
                                <td style="width:60px; text-align:right">Desc.(R$)</td>
                                <td style="width:60px; text-align:right">Total</td>
                                <td style="width:60px; text-align:right">Pago</td>
                                <td style="width:55px; text-align:right">Devedor</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio_paisagem" class="table_relatorio_paisagem" summary="Relat&oacute;rio">';
?>	
	<table class="relatorio_print_paisagem" style="page-break-before:avoid">
<?      print $tabelaCabecalho;
		$tabelaCabecalho = '<table class="relatorio_print_paisagem">'.$tabelaCabecalho;        
		while($rowPedido = mysql_fetch_array($rsPedido)){
			$x+= 1;
			$pedido_id 		= $rowPedido['fldPedidoId'];
					
			$rsStatus 		= mysql_query("SELECT * FROM tblpedido_status WHERE fldId =". $rowPedido['fldStatus']);
			$rowStatus		= mysql_fetch_array($rsStatus);
			
			if($_SESSION['sistema_tipo'] == 'automotivo'){
				$rowVeiculo = mysql_fetch_array(mysql_query("SELECT fldPlaca FROM tblcliente_veiculo WHERE fldId = ". $rowPedido['fldVeiculo_Id']));
				$campo_status  = '<td style="width:60px">'.$rowVeiculo['fldPlaca'].'</td>';
				$campo_status .= '<td style="width:50px">'.$rowPedido['fldVeiculo_Km'].'</td>';
			}else{
				$campo_status = '<td style="width:110px">'.$rowStatus['fldStatus'].'</td>';
			}
	
			unset($subTotalPedido);
			unset($totalPedido);
			unset($totalCompra);
			unset($totalLucro);
			unset($totalItem);
			
			$rsItem 			= mysql_query("SELECT * FROM tblpedido_item WHERE fldPedido_Id = $pedido_id");
			$descricao 			= '';
			while($rowItem 		= mysql_fetch_array($rsItem)){
			
				$valor_compra 	= $rowItem['fldValor_Compra'];
				$valor_item 	= $rowItem['fldValor'];
				$qtd 			= $rowItem['fldQuantidade'];
				$total_item 	= $valor_item * $qtd;
				$desconto 		= $rowItem['fldDesconto'];
				$totalDesconto 	= ($total_item * $desconto)/100;
				$totalValor		= $total_item - $totalDesconto;
				$totalItem		+= $totalValor;
				$descricao 		.= $rowItem['fldDescricao'].", ";
				
			}
			//total pedido
			$descPedido 		= $rowPedido['fldDesconto'];
			$descPedidoReais 	= $rowPedido['fldDescontoReais'];
			$subTotalPedido		= $totalItem + $rowPedido['fldComissao'] + $rowPedido['fldValor_Terceiros'] + $rowPedido['fldValorServico'];
			$descontoPedido 	= ($subTotalPedido 	* $descPedido) / 100;
			$totalPedido 		= $subTotalPedido 	- $descontoPedido;
			$totalPedido 	 	= $totalPedido 		- $descPedidoReais;
			
			$sSQL = "SELECT tblpedido_parcela.fldId, SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor
					FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
					WHERE tblpedido_parcela.fldPedido_Id = $pedido_id  GROUP BY tblpedido_parcela.fldPedido_Id";
					
			$rsBaixa  = mysql_query($sSQL);
			$rowBaixa = mysql_fetch_array($rsBaixa);
			echo mysql_error();
			
			$valorBaixa 	= $rowBaixa['fldBaixaValor'];
			$valorDevedor 	= $totalPedido - $rowBaixa['fldBaixaValor'];
			
			$totalTerceiros	+= $rowPedido['fldValor_Terceiros'];
			$totalItens 	+= $totalItem;
			$totalServicos 	+= $rowPedido['fldValorServico'];
			$totalPedidos 	+= $subTotalPedido;
			$totalLucros 	+= $totalLucro;
			$totalPagar 	+= $totalPedido;
			$totalBaixa 	+= $valorBaixa;
			$totalDevedor 	+= $valorDevedor;
			
			$nfe_numero 	= ($_SESSION["sistema_nfe"] > 0) ? str_pad($rowPedido['fldNumero'], 5, "0", STR_PAD_LEFT) : '';
?>
			<tr>
				<td style="width:50px;margin-left:5px;margin-right:10px"><?=str_pad($rowPedido['fldPedidoId'], 6, "0", STR_PAD_LEFT)?></td>
                <td style="width:60px">	<?=format_date_out($rowPedido['fldPedidoData'])?></td>
				<td style="width:50px;text-align:center; font-weight:bold"><?=$nfe_numero?></td>
				<td style="width:190px"><?=substr($rowPedido['fldFuncionarioNome'],0, 28)?></td>
				<td style="width:250px"><?=$descricao?></td>
				<?=$campo_status?>
				<td style="width:70px; text-align:right"><?=format_number_out($subTotalPedido )?></td>
				<td style="width:60px; text-align:right"><?=format_number_out($rowPedido['fldDesconto'])?></td>
				<td style="width:60px; text-align:right"><?=format_number_out($rowPedido['fldDescontoReais'])?></td>
				<td style="width:60px; text-align:right"><?=format_number_out($totalPedido)?></td>
				<td style="width:60px; text-align:right"><?=format_number_out($valorBaixa)?></td>
				<td style="width:55px; text-align:right"><?=format_number_out($valorDevedor)?></td>
			</tr>
<?		
			if(($n == $limite) or ($x == $totalRegistro)){
?>							</table>    
                        </td>
                    </tr>
                </table>
<?				$n = 1;
				if($x < $totalRegistro){
					$p += 1;
					print $tabelaCabecalho;
				}
			}else{
				$n += 1;
			}
		}
?>
        <table style="width:1152px" name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
            <tr>
                <td style="width:50px; margin-left:130px">Produtos</td>
                <td style="width:70px; text-align:right"><?=format_number_out($totalItens)?></td>
                <td style="border-right:1px solid">&nbsp;</td> 
               
                <td style="width:50px">Servi&ccedil;os</td>
                <td style="width:70px; text-align:right"><?=format_number_out($totalServicos)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
                                
                <td style="width:50px">Terceiros</td>
                <td style="width:70px; text-align:right"><?=format_number_out($totalTerceiros)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
                
                <td style="width:50px">Total</td>
                <td style="width:80px; text-align:right"><?=format_number_out($totalPedidos)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
                
                <td style="width:50px">A Pagar</td>
                <td style="width:60px; text-align:right"><?=format_number_out($totalPagar)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
                
                <td style="width:50px">Pago</td>
                <td style="width:60px; text-align:right"><?=format_number_out($totalBaixa)?></td>
                <td style="border-left:1px solid">&nbsp;</td>
                
                <td style="width:50px">Devedor</td>
                <td style="width:60px; text-align:right"><?=format_number_out($totalDevedor)?></td>
            </tr>                   
        </table>
		
	</body>
</html>