<div id="paginacao">
<?
			$pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : 1;
			if (!$pagina) {
				 $inicio = 0;
				 $pagina=1;
			}
			else {
				 $inicio = isset($_GET['inicio']);
			}

			//calcular total de p�ginas
			if ($pagina > $total_paginas){
				$pagina = 1;
			}
			
			//bot�o anterior
			if ($pagina != 1) {
?>				<span class="link"><a class="btn_paginacao" href="<?=$paginacao_destino?>&amp;pagina=<?=$pagina - 1?>"><</a></span>
<?			}


			//n�meros das p�ginas
			if ($total_paginas > 1){ 
				
				//limito o in�cio do �ndice
				$pagina_inicio = $pagina;
				if ($pagina_inicio > $total_paginas - $n_paginas) {
					$pagina_inicio = $total_paginas - $n_paginas;
				}
				if ($pagina_inicio < 1) {$pagina_inicio = 1;}
				//limito o fim do �ndice
				$pagina_limite = $pagina + $n_paginas;
				if ($pagina_limite > $total_paginas) {
					$pagina_limite = $total_paginas;
				}
				
				for ($i=$pagina_inicio;$i<=$pagina_limite;$i++){ 
					if ($pagina == $i){
						//se mostro o �ndice da p�gina atual, n�o coloco link  
?>						<span class="desativado"><a class="desativado"><?=$pagina?></a></span>
<?					}
					else{
						//se o �ndice n�o corresponde com a p�gina mostrada atualmente, coloco o link para ir a essa p�gina 
?>						<span class="link"><a class="btn_paginacao" href="<?=$paginacao_destino?>&amp;pagina=<?=$i?>"><?=$i?></a></span>
<?					}
				}
			} 
			//bot�o pr�xima
			if ($pagina != $total_paginas and $total_paginas != 0) {
?>				<span class="link"><a class="btn_paginacao" href="<?=$paginacao_destino?>&amp;pagina=<?=$pagina + 1?>">></a></span>
<?			}
?>
</div>