<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Compras</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />

	</head>
	<body>
    
        <div id="no-print">
            <a class="print" href="#" onClick="window.print()">imprimir</a>
        </div>
<?
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rsDados 	= mysql_query("select * from tblempresa_info");
		$rowDados 	= mysql_fetch_array($rsDados);
		$rsUsuario 	= mysql_query("select * from tblusuario where fldId=".$_SESSION['usuario_id']);
		$rowUsuario = mysql_fetch_array($rsUsuario);
		
		$CPF_CNPJDados   = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rsCompra 		 = mysql_query($_SESSION['compra_relatorio']);
		while($rowTotal = mysql_fetch_array($rsCompra)){
		
			$descontoCompra 	= ($rowTotal['fldTotalItem'] * $rowTotal['fldDesconto']) / 100;
			$totalCompra 		= $rowTotal['fldTotalItem'] 	- $descontoCompra;
			$totalCompra 		= $totalCompra 	- $rowTotal['fldDescontoReais'];
			$totalCompra		= (($totalCompra + $rowTotal['fldTransporte']) + $rowTotal['fldIPI_Valor']) + $rowTotal['fldST_Valor'];
	
			if($rowTotal['fldnatureza_operacao_id'] == '2'){
				$totalRelatorio 		-= $totalCompra;
			}elseif($rowTotal['fldParcelas '] == '0'){
				$totalRelatorio 		= $totalRelatorio;
			}else{
				$totalRelatorio 		+= $totalCompra;
			}	
			
		}	
		/*----------------------------------------------------------------------------------------*/	
		
		$rsCompra = mysql_query($_SESSION['compra_relatorio']);
		$limiteRegistro = mysql_num_rows($rsCompra);
		echo mysql_error();
		$limite = 19;
		$n = 1;
		
		$pgTotal = $limiteRegistro / $limite;
		$p = 1;
		
		
		$contadorCabecalho = '<tr style="border-bottom: 2px solid">
					<td style="width: 950px"><h1>Relat&oacute;rio de Compras</h1></td>
					<td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
					</tr>';
		$tabelaCabecalho ='
                <tr>
                    <td>
                        <table style="width: 930px" name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
                            <tr>
                                <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                                <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                                <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                                <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                            </tr>
                        </table>	
                    </td>
                    <td>        
                        <table class="dados_impressao">
                            <tr>
                                <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                                <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                                <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="total">
                	<td style="width: 950px">&nbsp;</td>
                	<td>Total selecionado: R$ '.format_number_out($totalRelatorio).'</td>
                    <td style="width:10px;">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio_paisagem" class="table_relatorio_paisagem" summary="Relat&oacute;rio">
                            <tr style="border:none">
                                <td style="width:60px; margin-left: 5px;">C&oacute;d.</td>
                                <td style="width:110px;">Data</td>
                                <td style="width:180px;">Ref. Nota</td>
                                <td style="width:250px">Fornecedor</td>
                                <td style="width:150px">Usu&aacute;rio</td>
                                <td style="width:170px">Status</td>
                                <td style="width:60px; text-align:right">Total</td>
                                <td style="width:60px; text-align:right">Pago</td>
                                <td style="width:60px; text-align:right">Devedor</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio_paisagem" class="table_relatorio_paisagem" summary="Relat&oacute;rio">
	';
		
?>
        <table class="relatorio_print_paisagem" style="page-break-before:avoid">
<?			print $contadorCabecalho.$tabelaCabecalho;
			while($rowCompra = mysql_fetch_array($rsCompra)){
				$x+= 1;
				$compra_id = $rowCompra['fldCompra_Id'];
				
				$rsStatus  = mysql_query("select * from tblpedido_status where fldId =". $rowCompra['fldStatus']);
				$rowStatus = mysql_fetch_array($rsStatus);
				mysql_error();
				
				$rsUsuario_compra  = mysql_query("select * from tblusuario where fldId = ". $rowCompra['fldUsuario_Id']);
				$rowUsuario_compra = mysql_fetch_array($rsUsuario_compra);
				
				/*
				$rsItem = mysql_query("select * from tblcompra_item where fldCompra_Id =".$compra_id);
				while($rowItem = mysql_fetch_array($rsItem)){
					
					$valor_item = $rowItem['fldValor'];
					$quantidade = $rowItem['fldQuantidade'];
					$total_item = $valor_item * $quantidade;
					
					$desconto 		= $rowItem['fldDesconto'];
					$totalDesconto 	= ($total_item * $desconto)/100;
					$totalValor 	= $total_item - $totalDesconto;
					
					$totalCompra += $totalValor;
				}
			
				$sSQL = "SELECT tblcompra_parcela.fldId, 
							SUM(tblcompra_parcela_baixa.fldValor * (tblcompra_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor
							FROM tblcompra_parcela LEFT JOIN tblcompra_parcela_baixa ON tblcompra_parcela.fldId = tblcompra_parcela_baixa.fldParcela_Id
							WHERE tblcompra_parcela.fldCompra_Id =".$compra_id;
				$rsBaixa = mysql_query($sSQL);
				
				echo mysql_error();
				$rowBaixa = mysql_fetch_array($rsBaixa);
				
				*/
							
				$descontoCompra 	= ($rowCompra['fldTotalItem'] * $rowCompra['fldDesconto']) / 100;
				$valorCompra 		= $rowCompra['fldTotalItem'] - $descontoCompra;
				$valorCompra 		= $valorCompra 	 - $rowCompra['fldDescontoReais'];
				$valorCompra		= (($valorCompra + $rowCompra['fldTransporte']) + $rowCompra['fldIPI_Valor']) + $rowCompra['fldST_Valor'];
				
				$valorBaixa			= $rowCompra['fldValorBaixa'];
				$valorDevedor 		= $valorCompra 	 - $valorBaixa;

				if($rowCompra['fldnatureza_operacao_id'] == '2'){
					
					$totalPagar 	-= $valorCompra;
					$totalBaixa 	-= $rowCompra['fldValorBaixa'];
					$totalDevedor 	-= $valorDevedor;
				}elseif($rowPedido['fldParcelas '] == '0'){
					
					$totalPagar 	= $totalPagar;
					$totalBaixa 	= $totalBaixa;
					$totalDevedor 	= $totalDevedor;
				}else{
					$totalPagar 	+= $valorCompra;
					$totalBaixa 	+= $rowCompra['fldValorBaixa'];
					$totalDevedor 	+= $valorDevedor;
				}
				
?>		
                <tr>
                    <td style="width:60px; margin-left: 5px;"><?= print str_pad($rowCompra['fldCompra_Id'], 6, "0", STR_PAD_LEFT)?></td>
                    <td style="width:110px"><?=format_date_out($rowCompra['fldCompraData'])?></td>
                    <td style="width:180px;"><?=$rowCompra['fldNota']?></td>
                    <td style="width:250px"><?=$rowCompra['fldNomeFantasia']?></td>
                    <td style="width:150px"><?=$rowUsuario_compra['fldUsuario']?></td>
                    <td style="width:170px"><?=$rowStatus['fldStatus']?></td>
                    
                    
<?					if($rowCompra['fldParcelas'] != '0'){                    
?>						 <td style="width:60px; text-align:right"><?=format_number_out($valorCompra)?></td>
                        <td style="width:60px; text-align:right"><?=format_number_out($valorBaixa)?></td>
                        <td style="width:60px; text-align:right"><?=format_number_out($valorDevedor)?></td>
<?					}elseif($rowCompra['fldnatureza_operacao_id'] == '2'){
						echo '<td style="width:60px; text-align:right"> -'.format_number_out($valorCompra).'</td>';
						echo '<td style="width:120px"></td>';
					}else{
						echo '<td style="width:180px"></td>';
					}
?>					
                </tr>
            
<?				if(($n == $limite) or ($x == $limiteRegistro)){
          			$n = 1;
?>								</table>    
                			</td>
                        </tr>
                    </table>
<?

					if($p < $pgTotal){
							$p += 1;
					}
					if($x < $limiteRegistro){
						$contadorCabecalho = '<tr style="border-bottom: 2px solid">
						<td style="width: 950px"><h1>Relat&oacute;rio de Vendas</h1></td>
						<td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
						</tr>';
						print '<table class="relatorio_print_paisagem">'.$contadorCabecalho.$tabelaCabecalho;
					}
				}else{
					$n += 1;
				}
			}
?>
            <table style="width:1150px" name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
                <tr>
                    <td style="width:650px">&nbsp;</td>
                    <td style="width:70px">Total</td>
                    <td style="width:60px; text-align:right"><?=format_number_out($totalPagar)?></td>
                    <td style="width:10px; border-left:1px solid">&nbsp;</td>
                    <td style="width:70px">Pago</td>
                    <td style="width:60px; text-align:right"><?=format_number_out($totalBaixa)?></td>
                    <td style="width:10px; border-left:1px solid">&nbsp;</td>
                    <td style="width:70px">Devedor</td>
                    <td style="width:60px; text-align:right"><?=format_number_out($totalDevedor)?></td>
                </tr>                   
            </table>
	</body>
</html>