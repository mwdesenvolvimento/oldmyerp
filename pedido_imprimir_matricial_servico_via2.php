<?	
	$limite = 31; #LIMITE DE LINHAS POR FOLHA
	
	$texto 				.= "*-------------<<< MAO DE OBRA >>>----------------------------------------------*\r\n";
	$rodapeLinha 		+= 1;
	
	$rodapeMensagem .= format_margem_print("<b>CIENTE _____________________________________</b>",80, 'esquerda')."\r\n\r\n";
	$rodapeLinha +=2;
			
	$rsMensagem 		= mysql_query("SELECT * FROM tblsistema_impressao_mensagem WHERE fldImpressao = 'venda' ORDER BY fldOrdem");
	while($rowMensagem 	= mysql_fetch_array($rsMensagem)){
		$mensagem 		= strtoupper(acentoRemover($rowMensagem['fldMensagem']));
		$x = 0; //quebrando linha a cada 76 caracteres
		while(strlen(substr($mensagem,$x * 76, 76)) > 0){
			$rodapeMensagem .= "<b>".format_margem_print(substr($mensagem,$x * 76, 76)."</b>",80, 'centro')."\r\n";
			$rodapeLinha +=1;
			$x +=1;
		}
	}
	$rodapeMensagem .= "\r\n\r\n";
	$rodapeLinha +=1;
	
	#CRIANDO RODAPE  !!!########################################################################################################################################
	$sSQL		= "SELECT group_concat(fldServico SEPARATOR ' * ') as fldServico FROM tblpedido_funcionario_servico WHERE fldPedido_Id = '$pedido_id'";
	$rsServico  = mysql_query($sSQL);
	$rowServico = mysql_fetch_array($rsServico);
	
	$servicoDescricao		= " * ".$rowPedido['fldServico'];
	$servicoDescricao 		.= $rowServico['fldServico'];
	$limiteLinhaServico 	= $limite - ($cabecalhoLinha + $rodapeLinha);
	$limiteColunaServico 	= 46;
	$servicoDescricao 		= preg_replace('/[\n|\r|\n\r|\r\n]{2,}/',' ', $servicoDescricao);
	$servicoDescricao		= str_pad($servicoDescricao, ($limite * 46), ' ', STR_PAD_RIGHT);
	
	for($x = 0; $x <= $limiteLinhaServico; $x++){
		$n+=1;
		$servico[$n] = substr(acentoRemover($servicoDescricao), $x * $limiteColunaServico, $limiteColunaServico);
	}
	
	#PARCELAS
	$sSQL = "SELECT SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor
			FROM tblpedido_parcela 
			LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
			WHERE tblpedido_parcela.fldPedido_Id = $pedido_id GROUP BY tblpedido_parcela.fldPedido_Id";
	$rsPedidoBaixa 	= mysql_query($sSQL);
	$rowPedidoBaixa	= mysql_fetch_array($rsPedidoBaixa);
	
	$total_pedido 			= $rowPedido['fldTotalItem'] + $rowPedido['fldTotalServico'] + $rowPedido['fldValor_Terceiros'];
	$desconto_pedido 		= $rowPedido['fldDesconto'];
	$desconto_reais_pedido 	= $rowPedido['fldDescontoReais'];
	$desconto 				= ($total_pedido * $desconto_pedido) / 100;
	$total_descontos 		= $desconto + $desconto_reais_pedido;
	$total_pedido_apagar 	= $total_pedido - $total_descontos;
	
	$rodape[1] 	= " |TOTAL DE PECAS  : R$ ".format_margem_print(format_number_out($rowPedido['fldTotalItem']), 10, 'direita')."|";#34 caracteres
	$rodape[2] 	= " |TOTAL MAO OBRA  : R$ ".format_margem_print(format_number_out($rowPedido['fldTotalServico']), 10, 'direita')."|"; 
	$rodape[3]	= " |TOTAL TERCEIROS : R$ ".format_margem_print(format_number_out($rowPedido['fldValor_Terceiros']), 10, 'direita')."|"; 
	$rodape[4]	= " |TOTAL DESCONTO  : R$ ".format_margem_print(format_number_out($total_descontos), 10, 'direita')."|"; 
	$rodape[5]	= "<b>"; 
	$rodape[5]	.= " |TOTAL GERAL     : R$ ".format_margem_print(format_number_out($total_pedido_apagar), 10, 'direita')."|"; 
	$rodape[5]	.= "</b>";
	$rodape[6]	= " |-------------------------------|";
	$rodape[7]	= "<b>"; 
	$rodape[7]	.= " |".format_margem_print(" *** P A R C E L A S ***", 30, 'centro')." |";
	$rodape[7]	.= "</b>";
	$rodape[8]	= " |                               |";
	
	# PARCELAS ###############################################################################################################################################
	$sSQL = "SELECT tblpedido_parcela.*, tblpagamento_tipo.fldSigla as fldForma_Pagamento, SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldTotalBaixa
						FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
						LEFT JOIN tblpagamento_tipo ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
						WHERE tblpedido_parcela.fldPedido_Id = $pedido_id GROUP BY tblpedido_parcela.fldId ORDER BY tblpedido_parcela.fldParcela";
	$rsParcela = mysql_query($sSQL);
	
	$n = 9;
	$totalParcelas = mysql_num_rows($rsParcela);
	while($rowParcela = mysql_fetch_array($rsParcela)){
		
		$parcela		= str_pad($rowParcela['fldParcela'], 2, "0", STR_PAD_LEFT);
		$totalParcelas 	= str_pad($totalParcelas, 2, "0", STR_PAD_LEFT);
		$vencimento		= format_date_out($rowParcela['fldVencimento']);
		$valor 	 		= format_number_out($rowParcela['fldValor']);
		$rodape[$n] 	= ' | '.$parcela."/".$totalParcelas." ".$vencimento." R$ ".format_margem_print($valor, 6, 'direita').' '.$rowParcela['fldForma_Pagamento'].' |';
		$n ++;
	}
	
	for($n = $n; $n <= $limiteLinhaServico; $n ++){ #pra garantir os caracteres, mesmo nao tendo parcelas suficiente
		$rodape[$n] = " |                               |";
	}
	
	for($x = 1; $x <= $limiteLinhaServico; $x++){
		$texto .= $servico[$x].$rodape[$x]."\r\n";
	}
	
	$texto	.= $rodapeMensagem;
	
	
?>
