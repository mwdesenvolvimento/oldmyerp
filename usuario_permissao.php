<?	
	if(isset($_POST['btn_action'])){
		require("usuario_permissao_action.php");
	}
	
?>
	<script type="text/javascript">

		$(document).ready(function(){
			/*---------------------------------------------------------------*/
			//bloqueia a seleção quando der 2 clicks
			if (typeof document.onselectstart!="undefined")
			document.onselectstart=function(){return false}
			else //FF
			document.getElementById('tela_ramificacao').style.MozUserSelect = "none";
			document.getElementById('table_cabecalho').style.MozUserSelect = "none";
			/*---------------------------------------------------------------*/
			
			var timerIn, iconCarregar, ultimoIdCarregado;
					
			$(".submenu").hide().end() //esconde todos os subcomponentes
			
			$('.linkCarregar').dblclick(function(event) {
				$('.submenu:first', this).toggle("fast"); //exibe os subcomponentes (componente do componente)
				$('.folder:first', this).toggleClass("openFolder"); //troca a classe de pastinha fechada para pastinha aberta
				
				tela_id = $(this).attr('id');
				if($('#tabela_telas').find('tr#'+tela_id).html() != null){
					$('#tabela_telas').find('tr').hide();
					$('#tabela_telas').find('tr#'+tela_id).show();
				}
				event.stopPropagation()
			});
			
			$('.titulo_tela').click(function(event){
				$("#tela_ramificacao li").find('span:first').css('background-color', 'transparent'); //limpa tudo, mostra os componentes do produto principal
			})
			
			$('#tela_ramificacao li').click(function(event){
				$("#tela_ramificacao li").find('span:first').css('background-color', 'transparent'); //limpa o background do selecionado anterior
				$(this).find('span:first').css('background-color', '#DDFFDD'); //adiciona o bg no atual
			})
			
			
			$('#mostrar_tudo').click( //mostra todos os subcomponentes
				function(event) { 
					$('.submenu').slideDown("fast");
					$('.folder', '#tela_ramificacao').addClass("openFolder");
					event.stopPropagation()
				}
			);
			
			$('#esconder_tudo').click( //esconde todos os subcomponentes
				function(event) { 
					$('.submenu').slideUp("fast");
					$('.openFolder', '#tela_ramificacao').attr("class", "folder");
					event.stopPropagation()
				}
			);			
		});
	</script>
<?
	function checkSubTela($id){
		$rsSubTela = mysql_query('SELECT * FROM tblsistema_tela WHERE fldAntecessor_Id = '.$id);
		$row = mysql_num_rows($rsSubTela);
		if($row){return true;} else {return false;}
		
	}
	function getSubTela($antecessor_id){
		$rsSubTela = mysql_query('SELECT * FROM tblsistema_tela WHERE fldAtivo= 1 AND fldAntecessor_Id = '.$antecessor_id.' ORDER BY fldTela');
		while($rowSubTela = mysql_fetch_array($rsSubTela)){
			$subtype_class	= (checkSubTela($rowSubTela['fldId']) && $rowSubTela['fldId'] != $antecessor_id) ? "folder" : "file";
			echo '<li id="'.$rowSubTela['fldId'].'" class="linkCarregar"><span class="'.$subtype_class.' clicked">'.$rowSubTela['fldTela'];
			
			if(checkSubTela($rowSubTela['fldId'])){
				echo '</span><ul class="submenu">';
				getSubTela($rowSubTela['fldId']);
				echo '</ul></li>';
			}else {
				echo '</span></li>';
			}
		}
	}
?>
	<h3>Permiss&otilde;es do sistema</h3>
    <form class="table_form" id="frm_usuario_permissao" method="post" style="margin-bottom:20px" action="">
        <div style="float:left;">
            <div id="table_cabecalho" style="width:400px; margin-top:10px; ">
                <ul class="table_cabecalho" style="width:400px; float:left;">
                    <li style="display:block; padding:5px; width:100%;">
                        <span style="color:#666; float:left; padding: 1px 0 0 0;" id="<?=$usuario_id?>" class="linkCarregar titulo_tela"><?=substr($rowProduto["fldNome"], 0, 42)?></span>
                        <span id="mostrar_tudo" title="Expandir tudo" style="margin-right:8px"></span>
                        <span id="esconder_tudo" title="Recolher tudo"></span>
                    </li>
                </ul>
            </div>
            
            <div id="table_container" style="width:390px; height:240px; border:1px solid #CCC; background:white; clear:both; padding:5px;">
                <ul id="tela_ramificacao">
                    <li id="0" class="linkCarregar"><span class="folder clicked">Tela inicial</span>
                        <ul class="submenu">
                            <?=getSubTela(0)?>
                        </ul>
                    </li>                            
                </ul>
            </div>
        </div>
            
        <div style="width:550px; float: right">
            <div id="table" style="margin-top:10px; width:540px;">
                <div id="table_cabecalho" style="width:550px;">
                    <ul class="table_cabecalho" style="width:550px">
                        <li style="width:500px;padding-left:5px">Tela</li>
                        <li style="width:20px"><input style="width:20px" type="checkbox" name="chk_todos" id="chk_todos" /></li>
                    </ul>
                </div>
            
                <div id="tabela_telas">
                    <div id="table_container" style="width:550px">
                        <table id="table_general" class="table_general" summary="Permiss&otilde;es do usu&aacute;rio">
                            <tbody>
                        	
<?								$id_array = array();
								$n = 0;
								
								$linha 	= "row";
								$rsTela = mysql_query('SELECT * FROM tblsistema_tela WHERE fldAtivo = "1" ORDER BY  fldAntecessor_Id, fldTela');
								echo mysql_error();
								while($rowTela = mysql_fetch_array($rsTela)){
									$id_array[$n] = $rowTela["fldId"];
									$n += 1;		
									$rsDiretiva = mysql_query('SELECT fldId FROM tbldiretiva_usuario WHERE fldUsuario_Id ='.$usuario_id.' AND fldTela_Id = '.$rowTela['fldId']);
									$rowCheck = mysql_num_rows($rsDiretiva);
									echo mysql_error();
									
?>									<tr class="<?= $linha; ?>" id="<?=$rowTela['fldAntecessor_Id']?>">
                                        <td style="width:500px;padding-left:5px"><?=$rowTela['fldTela']?></td>
                                        <td style="width:25px; text-align:center"><input type="checkbox" name="chk_diretiva_<?=$rowTela['fldId']?>" id="chk_diretiva_<?=$rowTela['fldId']?>" <?= ($rowCheck == true) ? 'checked="checked"' : '' ?> title="habilitar/desabilitar" /></td>
                                    </tr>
                                    
<?									$linha = ($linha == "row" ?	$linha = "dif-row": $linha = "row");
								}
?>								<input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            
							</tbody>
                    	</table>
                    </div>
                </div>
            </div>
            <div style="float:right; margin: 5px">
                <ul id="action_button">
                    <li><input type="submit" class="btn_enviar" name="btn_action" value="gravar" title="Gravar altera&ccedil;&otilde;es" /></li>
                </ul>
        	</div>
    	</div>
    </form>
    <script type="text/javascript">
    
        $('#tabela_telas').find('tr').hide();
        $('#tabela_telas').find('tr#0').show();
            
    </script>
