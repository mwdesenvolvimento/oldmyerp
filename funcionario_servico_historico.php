<div id="voltar">
    <p><a href="index.php">n&iacute;vel acima</a></p>
</div>	
<h2>Hist&oacute;rico de Servi&ccedil;os</h2>

		<form class="frm_detalhe" id="frm_funcionario_servico_historico" method="get" action="">
        	<ul style=" width:480px; margin:0 auto">
                <li style="margin-right:50px;float:left">
                	<label for="txt_funcionario_id">Funcion&aacute;rio</label>
                    <input style="width:96px; height: 50px; text-align:center; font-size:25px" type="text" id="txt_funcionario_id" name="txt_funcionario_id" />
                </li>
                <li style="margin-right:50px;float:left">
                	<label for="txt_venda_id">Venda</label>
                    <input style="width:96px; height: 50px; text-align:center; font-size:25px" type="text" id="txt_venda_id" name="txt_venda_id" />
                </li>
                <li style="float:left; margin-top:18px">
               		<button class="btn_comanda_gravar" id="btn_funcionario_servico_abrir" name="btn_funcionario_servico_abrir">OK</button>
                	<a href="funcionario_servico_historico_novo" class="modal" style="display:none" rel="870-490"></a>
                </li>
            </ul>
		</form>
    
	<script type="text/javascript">
    
        $("#btn_funcionario_servico_abrir").click(function(event) {
            event.preventDefault();
			
			funcionario_id  = $("#txt_funcionario_id").val();
     		venda_id		= $("#txt_venda_id").val();
	 		
			$.post('funcionario_servico_historico_buscar.php', {funcionario_id:funcionario_id, venda_id:venda_id}, function(theXML){
				$('dados',theXML).each(function(){
					var retorno = $(this).find("return").text();
					if(retorno == false){
						alert('Nada encontrado. Verifique se o funcionário digitado pertence a essa venda ou se a venda foi excluida');
					}else{
						if($("a.modal").attr({"href": 'funcionario_servico_historico_novo,'+funcionario_id+','+venda_id})){
							anchor = $('a.modal');
							winModal(anchor);
						}
					}
				});
			});
				
			
        });
        
        $("#txt_funcionario_id").focus();
        
    </script>
