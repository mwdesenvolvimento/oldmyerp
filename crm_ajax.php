<?php
    
    /**
     * Arquivo responsável por controlar requisições Ajax do módulo CRM
     */
    
    //carregando arquivos necessários para conexão com banco de dados e funções genéricas
    header('Content-type: application/json');
    session_start();
	require_once('inc/con_db.php');
	require_once('inc/fnc_general.php');
    
    //Acompanhamento de clientes - preencher o select com os tipos cadastrados
    if(isset($_GET['carregar_select_agendamento_tipo'])) {
        
        $rsChamadoTipoAtendimento = mysql_query("SELECT fldId, fldTipo_Nome FROM tblcrm_acompanhamento_cliente_tipo WHERE fldExcluido = 0 ORDER BY fldTipo_Nome");
		
		$x = 0;
		while($rowChamadoTipoAtendimento = mysql_fetch_array($rsChamadoTipoAtendimento)) {
			$chamadoTipoAtendimento[$x]['id'] 	= $rowChamadoTipoAtendimento['fldId'];
			$chamadoTipoAtendimento[$x]['tipo'] = $rowChamadoTipoAtendimento['fldTipo_Nome'];
			$x++;
		}
        
        if(isset($_GET['callback'])) echo $_GET['callback'] . '(' . json_encode($chamadoTipoAtendimento) . ')';
        else echo json_encode($chamadoTipoAtendimento);
        
    }
    
    //Acompanhamento de clientes - verificar se funcionário possui compromisso agendado para a data, hora e id do chamado passados como parâmetros
    if(isset($_GET['checar_funcionario_agenda'])) {
        
        //se o 'agendamento_id' for igual a fldId, significa que o registro está sendo atualizado
        $sql = "SELECT fldId FROM tblcrm_acompanhamento_agenda
                WHERE fldFuncionario_Id = " . $_GET['funcionario_id'] . " AND fldData = '" . format_date_in($_GET['data_agenda']) . "'
                AND fldHora = '" . $_GET['horario_agenda'] . "' AND fldId <> " . $_GET['agendamento_id'] . " AND fldExcluido = 0";
		$rsAgenda = mysql_query($sql);
        
		if(mysql_num_rows($rsAgenda) > 0) $agenda['preenchida'] = true;
        else                              $agenda['preenchida'] = false;
        
        if(isset($_GET['callback'])) echo $_GET['callback'] . '(' . json_encode($agenda) . ')';
        else echo json_encode($agenda);
        
    }
    
    //Acompanhamento de clientes - verificar se há ou não agendamentos ao tentar mudar a situação do chamado para "Em espera"
    if(isset($_GET['checar_situacao_chamado'])) {
        
        $sql = "SELECT fldId FROM tblcrm_acompanhamento_agenda WHERE fldChamado_Id = " . $_GET['chamado_id'] . " AND fldExcluido = 0";
		$rsAgenda = mysql_query($sql);
        
		if(mysql_num_rows($rsAgenda) > 0) $agenda['mudar_situacao'] = false;
        else                              $agenda['mudar_situacao'] = true;
        
        if(isset($_GET['callback'])) echo $_GET['callback'] . '(' . json_encode($agenda) . ')';
        else echo json_encode($agenda);
        
    }
    
?>