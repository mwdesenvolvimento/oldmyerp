<?

	$cfop_id				= $rowItem['fldcfop_id'];
    if(isset($cfop_id)){
        $rowCFOP = (mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_cfop WHERE fldId = $cfop_id")));
        $cfop					= $rowCFOP['fldCFOP'];	
    }
    
    $ncm 					= $rowItem['fldncm']; 
    $ex_tipi 				= $rowItem['fldex_tipi']; 
    
    $unid_comercial 		= $rowItem['fldunidade_comercial'];
    $unid_tributavel 		= $rowItem['fldunidade_tributavel'];
    
    $qtde_tributavel 		= $qtd;
    $valor_unit_tributavel 	= $valor_item;
    
    $itemTotalBruto 		= $totalValor;
    $itemChkBruto 			= '1';
    
    $total_seguro 			= '';
    $desconto				= $totalDesconto;
    $frete 					= '';
    $ean 					= $rowItem['fldean'];
    $ean_unid_tributavel 	= $rowItem['fldean_unidade_tributavel'];
    
    $outras_despesas		= '';
    
    $pedido_compra 			= '';
    $numero_pedido_compra 	= '';
    $produto_especifico 	= '';
    
    $icms_sit_tributaria 	= $rowItem['fldicms_CST'];
    $rad_imposto			= 'icms';
    
    $icms_origem		 	= $rowItem['fldicms_orig'];
    $icms_aliq_aplicavel	= $rowItem['fldicms_pCredSN'];
    
    $icms_bc_modalidade		= $rowItem['fldicms_modBC'];
    $icms_base_calculo		= '';
    $icms_bc_perc_reducao	= $rowItem['fldicms_pRedBC'];
    $icms_aliquota			= $rowItem['fldicms_pICMS'];
    
    $icms_bc_op_propria		= $rowItem['fldicms_pBCOp'];
    $icms_desoneracao_motiv = $rowItem['fldicms_motDesICMS'];
    
    $icmsst_bc_modalidade	= $rowItem['fldicms_modBCST'];
    $icmsst_bc_perc_reducao	= $rowItem['fldicms_pRedBCST'];
    $icmsst_mva				= $rowItem['fldicms_pMVAST'];
    $icmsst_aliquota		= $rowItem['fldicms_pICMSST'];
    $icmsst_uf				= $rowItem['fldicms_UFST'];
    $icmsst_bc_ret_uf_rem	= '';
    $icmsst_val_ret_uf_rem	= '';
    $icmsst_bc_uf_destino	= '';
    $icmsst_valor_uf_destino= '';

    $cofins_sit_tributaria	= $rowItem['fldcofins_situacao_tributaria'];
    $cofins_tipo_calculo	= $rowItem['fldcofins_calculo_tipo'];
    $cofins_base_calculo	= '';
    $cofins_aliq_percent	= $rowItem['fldcofins_aliquota_porcentagem'];
    $cofins_aliq_reais		= $rowItem['fldcofins_aliquota_reais'];
    $cofins_qtde_vendida	= '';
    
    $cofinsst_tipo_calculo	= $rowItem['fldcofinsst_calculo_tipo'];
    $cofinsst_base_calculo	= '';
    $cofinsst_aliq_percent	= $rowItem['fldcofinsst_aliquota_porcentagem'];
    $cofinsst_aliq_reais	= $rowItem['fldcofinsst_aliquota_reais'];
    $cofinsst_qtde_vendida	= '';
    
    $ii_base_calculo		= '';
    $ii_desp_aduaneiras_val	= '';
    $ii_iof_valor			= '';
    $ii_valor				= '';
    
    $ipi_sit_tributaria		= $rowItem['fldipi_situacao_tributaria'];
    $ipi_enquadra_classe	= $rowItem['fldipi_enquadramento_classe'];
    $ipi_enquadra_codigo	= $rowItem['fldipi_enquadramento_codigo'];
    $ipi_produtor_cnpj		= $rowItem['fldipi_produtor_cnpj'];
    
    $ipi_selo_controle_cod	= '';
    $ipi_selo_controle_qtde	= '';
    
    $ipi_calculo_tipo		= $rowItem['fldipi_calculo_tipo'];
    $ipi_base_calculo		= '';
    $ipi_aliquota			= $rowItem['fldipi_aliquota'];
    $ipi_unid_qtde_total	= '';
    $ipi_unidade_valor		= $rowItem['fldipi_unidade_valor'];
    $ipi_valor				= '';
    
    $issqn_sit_tributaria	= '';
    $issqn_base_calculo		= '';
    $issqn_aliquota			= '';
    $issqn_lista_servico	= '';
    $issqn_uf				= '';
    $issqn_mun_ocorrendia	= '';
    
    $pis_sit_tributaria		= $rowItem['fldpis_situacao_tributaria'];
    $pis_calculo_tipo		= $rowItem['fldpis_calculo_tipo'];
    $pis_base_calculo		= '';
    $pis_aliq_percentual	= $rowItem['fldpis_aliquota_porcentagem'];
    $pis_aliq_reais			= $rowItem['fldpis_aliquota_reais'];
    $pis_qtde_vendida		= '';
    
    $pisst_tipo_calculo		= $rowItem['fldpisst_calculo_tipo'];
    $pisst_base_calculo		= '';
    $pisst_aliq_percent		= $rowItem['fldpisst_aliquota_porcentagem'];
    $pisst_aliq_reais		= '';
    $pisst_qtde_vendida		= '';
    
	$info_adicionais		= $rowItem['fldinformacoes_adicionais'];




	//CALCULOS
	//calculo da BC do ICMS nao precisa, pois � na verdade o mesmo valor que o total bruto
	switch($icms_sit_tributaria){
		case  '2':
		case '12':
		case '13':
		case '14':
		case '21':
		case '22':
		case '23':
		case '25':
			$icms_base_calculo		= $itemTotalBruto;
			
			//calculo 00
			$icms_valor = $icms_base_calculo * ($icms_aliquota / 100);
	
			//calculo 030
			$icmsst_base_calculo = $itemTotalBruto * ((100 + $icmsst_mva)/100); 
			$valorAliquota = $icmsst_aliquota / 100;
			$valorICMSNormal = 100 * $valorAliquota;
			$icmsst_valor = ($icmsst_base_calculo * $valorAliquota) - $valorICMSNormal;
			
		break;
		case  '3':
		case  '10': //o 10 tb calcula o 020, mais pra baixo calcula o resto
			$icms_base_calculo		= $itemTotalBruto;
			//calculo 020
			$icms_valor = $icms_base_calculo * ($icms_aliquota / 100);
			
		break;
		case  '4':
			//calculo 1030			
			$icmsst_base_calculo = $itemTotalBruto * ((100 + $icmsst_mva)/100);
			
			$valorAliquota = $icmsst_aliquota / 100;
			$valorICMSNormal = 100 * $valorAliquota;
			$icmsst_valor = ($icmsst_base_calculo * $valorAliquota) - $valorICMSNormal;
		break;
		case  '10':
			$icms_base_calculo		= $itemTotalBruto;
			//calculo 070			
			//Calculo Base de ICMS Pr�prio com Redu��o:
			$baseReduzida = (($icms_base_calculo * (100 - $icms_bc_perc_reducao))/100 );
			//Calculo Base ICMS ST
			$icmsst_base_calculo = ($icms_base_calculo + nfe_ipi)*(100 + $icmsst_mva)/100;
			//Calculo Base Reduzida ICMS ST
			$baseReduzidaST = $baseICMSST * (100 - $icmsst_bc_perc_reducao)/100;
			//Valor ICMS Pr�prio
			$valorICMSSTProprio = ($baseReduzida * $icms_aliquota) /100;
			//Valor ICMS ST
			$icmsst_valor = (($baseReduzidaST * $icms_aliquota) /100) - $valorICMSSTProprio;
		break;
		case  '8':
			$icms_base_calculo		= $itemTotalBruto;
			//calculo 051			
			$aliquotaICMS	= $icms_aliquota/100;
			$icms_valor   	= $icms_base_calculo * aliquotaICMS;
		break;
	}
	
	
	//CALCULAR CREDITO ICMS QUE PODE SER APROVEITADO
	$icms_cred_aproveitado = ($itemTotalBruto * $icms_aliq_aplicavel) / 100;

	//CALCULO BASE DO IPI
	#o IPI n�o � calculado automatico pois precisa de dados como Despesas acesorias e seguro, que s� tem depois de abrir o modal

	//calculo PIS
	switch($pis_calculo_tipo){
		case  '1': #percentual
			$pis_valor = ($pis_aliq_percentual / 100) * $pis_base_calculo;
		break;
		case  '2': #valor
			$pis_valor = $pis_aliq_reais * $pis_qtde_vendida;
		break;
		
	}
	//calculo PIS ST
	switch($pisst_calculo_tipo){
		case  '1': #percentual
			$pisst_valor = ($pisst_aliq_percent / 100) * $pisst_base_calculo;
		break;
		case  '2': #valor
			$pisst_valor = $pisst_aliq_reais * $pisst_qtde_vendida;
		break;
	}
	
	//calculo COFINS
	switch($cofins_tipo_calculo){
		case  '1': #percentual
			$cofins_valor = ($cofins_aliq_reais / 100) * $cofins_base_calculo;
		break;
		case  '2': #valor
			$cofins_valor = $cofins_aliq_reais * $cofins_qtde_vendida;
		break;
	}
	//calculo COFINS ST
	switch($cofinsst_tipo_calculo){
		case  '1': #percentual
			$cofinsst_valor = ($cofins_aliq_reais / 100) * $cofinsst_base_calculo;
		break;
		case  '2': #valor
			$cofinsst_valor = $cofinsst_aliq_reais * $cofinsst_qtde_vendida;
		break;
	}
	
	//calculo ISSQN
	$issqn_valor = ($issqn_aliquota / 100) * $issqn_base_calculo;
	
	
















?>

