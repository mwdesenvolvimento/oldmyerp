<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html language="pt-br">
<head>
<title>myERP - Imprimir NP</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Robots" content="none" />

</head>
<body>

<?
	if (!isset($_SESSION['logado'])){
		session_start();
	}
	ob_start();
	session_start();
	
	//conectar ao db
	require_once("inc/con_db.php");
	require_once("inc/fnc_general.php");
	require_once("inc/fnc_imprimir.php");
	require_once("inc/fnc_identificacao.php");
	
	$impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
	if(!$impressao_local){
		$impressao_local = fnc_estacao_impressora('todos');
	}
	$texto 		= $impressao_local." \r\n";
	$pedido_id 	= $_GET['id'];
	$data 		= date("Y-m-d");
	
	$rsPedido  	= mysql_query("SELECT SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) as fldTotalItem,
						(SELECT SUM(fldValor) FROM tblpedido_funcionario_servico WHERE fldFuncao_Tipo = 2 AND fldPedido_Id = tblpedido.fldId) as fldTotalServico,
						tblpedido.fldCadastroData as fldPedidoData, tblpedido.fldServico, tblpedido.fldDesconto, tblpedido.fldValor_Terceiros, tblpedido.fldId as fldPedidoId, tblpedido.fldDescontoReais, tblcliente.*
						FROM tblpedido 
						LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
						INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
						WHERE tblpedido.fldId = $pedido_id GROUP BY tblpedido_item.fldPedido_Id");
	$rowPedido 	= mysql_fetch_array($rsPedido);
	
	if($rowPedido['fldMunicipio_Codigo']){
		
		$queryString = $rowPedido['fldMunicipio_Codigo'];
		$municipio =  substr($queryString,2,5);	
		$uf =  substr($queryString,0,2);
			
		$rsMunicipio = mysql_query("select * from tblibge_municipio where fldCodigo = $municipio and fldUF_codigo = $uf");
		$rowMunicipio = mysql_fetch_array($rsMunicipio);
	
		$rsUF = mysql_query("select * from tblibge_uf where fldCodigo = $uf");
		$rowUF = mysql_fetch_array($rsUF);
	}
	
	/*----------------------------------------------------------------------------------------------*/
	$rsEmpresa = mysql_query("SELECT * FROM tblempresa_info");
	$rowEmpresa = mysql_fetch_array($rsEmpresa);
	
	if($rowEmpresa['fldMunicipio_Codigo']){
		
		$queryString = $rowEmpresa['fldMunicipio_Codigo'];
		$municipio 	 =  substr($queryString,2,5);	
		$uf			 =  substr($queryString,0,2);
			
		$rsMunicipio_Empresa = mysql_query("select * from tblibge_municipio where fldCodigo = $municipio and fldUF_codigo = $uf");
		$rowMunicipio_Empresa = mysql_fetch_array($rsMunicipio_Empresa);
	
		$rsUF_Empresa = mysql_query("select * from tblibge_uf where fldCodigo = $uf");
		$rowUF_Empresa = mysql_fetch_array($rsUF_Empresa);
	}
	
	/*----------------------------------------------------------------------------------------------*/
	$empresa = strlen($rowEmpresa['fldNome_Fantasia']);
	$limite = 40;
	$espaco = $limite - $empresa;
	$espaco = $espaco / 2;
	$espaco =  number_format($espaco , 0, '.', '');
	
	while($espaco > 0){
		$margem .=" "; 
		$espaco -= 1;		
	}
	
	if($rowPedido['fldCPF_CNPJ'] != null){
		if($rowPedido['fldTipo'] == 1){
			$CPF_CNPJ = format_cpf_out($rowPedido['fldCPF_CNPJ']);
		}elseif($rowPedido['fldTipo'] == 2){
			$CPF_CNPJ = format_cnpj_out($rowPedido['fldCPF_CNPJ']);
		}
	}
	
	$rsParcela = mysql_query("SELECT fldVencimento FROM tblpedido_parcela WHERE fldPedido_Id = ".$rowPedido['fldPedidoId']." AND fldParcela = '1' AND fldStatus = 1 AND fldExcluido = 0 ");
	$rowParcela = mysql_fetch_array($rsParcela);
	
	$texto .= $margem . acentoRemover($rowEmpresa['fldNome_Fantasia'])." \r \r\n";
	$texto .="Fone Fax: ". $rowEmpresa['fldTelefone1']." \r\n\r\n";
	$texto .="Data da fatura: ".format_date_out($rowPedido['fldPedidoData'])." \r\n";
	$texto .="Vencimento: ".format_date_out($rowParcela['fldVencimento'])." \r\n";
	$texto .="Codigo: ".str_pad($rowPedido['fldPedidoId'], 6, "0", STR_PAD_LEFT)." \r\n";
	$texto .="Cliente: ".acentoRemover($rowPedido['fldNome'])." \r\n";
	$texto .="CPF/CNPJ: ".$CPF_CNPJ."\r\n";
	
	
	
	if($rowPedido['fldEndereco'] != NULL){
		$texto .= "End.: ".acentoRemover($rowPedido['fldEndereco']).", ".$rowPedido['fldNumero']."\r\n";
	}
	$texto .= $rowPedido['fldBairro']."\r\n";
	if($rowMunicipio['fldNome'] != NULL){
		$texto .= acentoRemover($rowMunicipio['fldNome'])." - ".$rowUF['fldSigla']."\r\n";
	}
	$texto .= "\r\n";
	
	$total_pedido 			= $rowPedido['fldTotalItem'] + $rowPedido['fldTotalServico'] + $rowPedido['fldValor_Terceiros'];;
	$desconto_pedido 		= $rowPedido['fldDesconto'];
	$desconto_reais_pedido 	= $rowPedido['fldDescontoReais'];
	
	$desconto 				= ($total_pedido * $desconto_pedido) / 100;
	$total_descontos 		= $desconto + $desconto_reais_pedido;
	
	$total_pedido_apagar 	= ($total_pedido - $total_descontos);
	$data = format_date_out($rowPedido['fldCadastroData']);
	
	//CNPJ DA EMPRESA
	if($rowEmpresa['fldCPF_CNPJ'] != NULL){
		if($rowEmpresa['fldTipo'] == 1){
			$CPF_CNPJ = format_cpf_out($rowEmpresa['fldCPF_CNPJ']);
		}elseif($rowEmpresa['fldTipo'] == 2){
			$CPF_CNPJ = format_cnpj_out($rowEmpresa['fldCPF_CNPJ']);
		}
	}
	
	$NP = "Ao ". formata_data_extenso($rowParcela['fldVencimento']) ." pagarei por esta unica via de NOTA PROMISSORIA a ".$rowEmpresa['fldRazao_Social']." CNPJ - $CPF_CNPJ ou a sua ordem, a quantia de R$ ".format_number_out($total_pedido_apagar)." (".utf8_encode(valorExtenso($total_pedido_apagar,1,"baixa")).") em moeda corrente deste pais, pagavel em ITAPIRA-SP.";

	$x = 0;
	while(strlen(substr($NP,$x * 40, 40)) > 0){
		$texto .= substr(acentoRemover($NP),$x * 40, 40) . "\r\n";
		$x +=1;
	}
	
	$texto .= "\r\n";
	
	$data = date("d/m/Y");
	$rodape = strlen("ITAPIRA, ".$data);
	$limite = 40;
	$espaco = $limite - $rodape;
	$espaco = $espaco / 2;
	$espaco =  number_format($espaco , 0, '.', '');
	$margem = '';
	
	while($espaco > 0){
		$margem .=" "; 
		$espaco -= 1;		
	}

	$texto .= $margem."ITAPIRA, ".format_date_out($rowPedido['fldPedidoData'])."\r\n\r\n\r\n     ------------------------------     \r\n";
	
	$cliente = strlen($rowPedido['fldNome']);
	$limite = 40;
	$espaco = $limite - $cliente;
	$espaco = $espaco / 2;
	$espaco =  number_format($espaco , 0, '.', '');
	
	$margem = '';
	while($espaco > 0){
		$margem .=" "; 
		$espaco -= 1;		
	}
	
	$texto .= $margem.acentoRemover($rowPedido['fldNome'])."\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n";
	
/*****************************************************************************************************************************************************************************************/
	
	$timestamp  = date("Ymd_His");
	$local_file = "impressao\inbox\imprimir_np_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
	$fp 		= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
	$salva 		= fwrite($fp, $texto);
	fclose($fp);
	
?>  
	<script type="text/javascript">
		var raiz = '<?= $raiz ?>';
		if(raiz){
			opener.location.href="index.php?p=pedido&modo="+raiz;
		}
		window.close();

	</script>
</body>
</html>