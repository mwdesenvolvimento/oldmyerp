<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
      	<title>myERP - Imprimir venda</title>
        
	</head>
	<body>
<?php
		if (!isset($_SESSION['logado'])){
			session_start();
		}
	
		ob_start();
		session_start();
		
		require_once("inc/con_db.php");
		require_once("inc/fnc_general.php");
		require_once("inc/fnc_imprimir.php");
		require_once("inc/fnc_identificacao.php");
		require_once("inc/fnc_ibge.php");
	
		$impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
		if(!$impressao_local){
			$impressao_local = fnc_estacao_impressora('todos');
		}
		
		$texto 				= $impressao_local."\r\n";
		$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
		$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
		$usuario_sessao 	= $_SESSION['usuario_id'];
		$pedido_id  		= $_GET['id'];
		$raiz 				= $_GET['raiz'];
		$data 				= date("Y-m-d");
		
		############################################################################################################################################################
		$rsPedido  	= mysql_query("SELECT tblpedido_status.fldStatus, tblpedido.fldVeiculo_Id, tblpedido.fldVeiculo_Km, tblpedido.fldObservacao as fldPedidoObservacao, tblpedido.fldId as fldPedidoId, tblcliente.*
								 FROM tblpedido 
								 INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								 INNER JOIN tblpedido_status ON tblpedido_status.fldId = tblpedido.fldStatus
								 WHERE tblpedido.fldId = $pedido_id");
		$rowPedido 	= mysql_fetch_array($rsPedido);
		
		
		$rsEmpresa  = mysql_query("SELECT * FROM tblempresa_info");
		$rowEmpresa = mysql_fetch_array($rsEmpresa);
		
		############################################################################################################################################################
		#EMPRESA
		$endereco 	= $rowEmpresa['fldEndereco'];
		$numero 	= $rowEmpresa['fldNumero'];
		$bairro 	= $rowEmpresa['fldBairro'];
		
		$site		= $rowEmpresa['fldWebsite'];
		$telefone2	= $rowEmpresa['fldTelefone2'];
		
		# CABECALHO TXT ###############################################################################################################################################
		$cabecalho 	.= '|';
		$cabecalho 	.= format_margem_print(acentoRemover(strtoupper($rowEmpresa['fldNome_Fantasia'])).' ',48,'esquerda');
		$cabecalho 	.= format_margem_print($rowEmpresa['fldTelefone1']." ",15,'esquerda');
		$cabecalho 	.= format_margem_print($telefone2,15,'esquerda');
		$cabecalho 	.= "|\r\n";
		$cabecalhoLinha += 1;
		
		$cabecalho 	.="|==============================================================================|\r\n";
		$cabecalhoLinha += 1;
		
		$cabecalho 	.= '|';
		$cabecalho 	.= format_margem_print("ORDEM DE SERVICO: ".str_pad($rowPedido['fldPedidoId'], 5, "0", STR_PAD_LEFT).' ',30,'esquerda');
		$cabecalho 	.= format_margem_print("DATA: ".format_date_out(date("Y-m-d")),15,'esquerda');
		$cabecalho 	.= format_margem_print("HORA: ".date("H:i:s"),15,'esquerda');
		$cabecalho 	.= format_margem_print($rowPedido['fldStatus'].' ',16,'direita');
		$cabecalho 	.= "|\r\n";
		$cabecalhoLinha += 1;
		
		$cabecalho 	.="|==============================================================================|\r\n";
		$cabecalhoLinha += 1;
		
		$nome 		 = substr($rowPedido['fldNome'],0, 42);
		$cabecalho 	.= "|";
		$cabecalho 	.= format_margem_print("CLIENTE: ".str_pad($rowPedido['fldCodigo'], 6, "0", STR_PAD_LEFT).' '.acentoRemover(strtoupper($nome)),58,'esquerda');
		$cabecalho 	.= format_margem_print("TEL.: ".$rowPedido['fldTelefone1'],20,'esquerda');
		$cabecalho 	.= "|\r\n";
		$cabecalhoLinha += 1;
		
		$municipio 	= fnc_ibge_municipio(strtoupper(acentoRemover($rowPedido['fldMunicipio_Codigo'])));
		$siglaUF	= fnc_ibge_uf_sigla($rowPedido['fldMunicipio_Codigo']);
		
		$endereco 	= substr(acentoRemover($rowPedido['fldEndereco']).' '.$rowPedido['fldNumero'].' '.strtoupper($municipio),0,80);
		$cabecalho 	.= "|";
		$cabecalho 	.= format_margem_print(strtoupper($endereco),78,'esquerda');
		$cabecalho 	.= "|\r\n";
		$cabecalhoLinha += 1;
		
		if($_SESSION['sistema_tipo'] == 'automotivo'){
			if(isset($rowPedido['fldVeiculo_Id'])){
				$rsVeiculo = mysql_query("SELECT fldVeiculo, fldPlaca FROM tblcliente_veiculo WHERE fldId = ".$rowPedido['fldVeiculo_Id']);
				$rowVeiculo = mysql_fetch_array($rsVeiculo);
				
				$cabecalho 	.= "|";
				$cabecalho 	.= format_margem_print("PLACA: ".$rowVeiculo['fldPlaca']." ",25,'esquerda');
				$cabecalho 	.= format_margem_print("VEICULO: ".$rowVeiculo['fldVeiculo'],33,'esquerda');
				$cabecalho 	.= format_margem_print("KM: ".$rowPedido['fldVeiculo_Km'],20,'esquerda');
				$cabecalho 	.= "|\r\n";
				$cabecalhoLinha += 1;
			}
		}
		
		$cabecalho 		.="|-----------------------------<SERVICOS A EFETUAR>-----------------------------|\r\n";
		$cabecalhoLinha += 1;
		
		$limite = 76;
		$x = 0;
		$observacao = acentoRemover($rowPedido['fldPedidoObservacao']);
		while(strlen(substr($observacao,$x * $limite, $limite)) > 0){
			$cabecalho 		.="| ".format_margem_print(substr($observacao,$x * $limite, $limite),76,'esquerda')." |\r\n";
			$cabecalhoLinha += 1;
			$x +=1;
		}	
		
		$cabecalho 		.="|                                                                              |\r\n";
		$cabecalho 		.="|------------------------------------------------------------------------------|\r\n";
		$cabecalhoLinha += 2;
		
		$mecanico 		.= 'Mecanico :                      '; //32
		$inicio 		.= 'Inicio :     :    '; //18
		$fim 			.= 'Fim :     :    '; //15
		
		$mecanicoLinha 	.= '           -------------------- '; //32
		$inicioLinha 	.= '         ---- ----'; //18
		$fimLinha 		.= '      ---- ----'; //15
		
		$x=1;
		while($x < 4){
			$cabecalho 	.= '|';
			$cabecalho 	.= format_margem_print($mecanico,38,'direita');
			$cabecalho 	.= format_margem_print($inicio,20,'esquerda');
			$cabecalho 	.= format_margem_print($fim,20,'esquerda');
			$cabecalho 	.= "|\r\n";
			$cabecalhoLinha += 1;
			
			$cabecalho 	.= '|';
			$cabecalho 	.= format_margem_print($mecanicoLinha,38,'direita');
			$cabecalho 	.= format_margem_print($inicioLinha,20,'esquerda');
			$cabecalho 	.= format_margem_print($fimLinha,20,'esquerda');
			$cabecalho 	.= "|\r\n";
			$cabecalhoLinha += 1;
			$x ++;
		}
		
		$cabecalho 	.="|------------------------------------------------------------------------------|\r\n";
		$cabecalhoLinha += 1;
	
		$cabecalho 	.= '|';
		$cabecalho 	.= format_margem_print('PRODUTO',11,'centro').'|';
		$cabecalho 	.= format_margem_print(' DESCRICAO',26,'centro').'|';
		$cabecalho 	.= format_margem_print('CUSTO',7,'centro').'|';
		$cabecalho 	.= format_margem_print(' PRECO',10,'centro').'|';
		$cabecalho 	.= format_margem_print('QTDE',8,'centro').'|';
		$cabecalho 	.= format_margem_print('VALOR',11,'centro')."|\r\n";
		$cabecalhoLinha += 1;
		
		
		############################################################################################################################################################
		$limite = 60; #LIMITE DE LINHAS POR FOLHA
		$linhasRestantes = $limite - $cabecalhoLinha;
		$texto .= $cabecalho;
		############################################################################################################################################################
		while($n < $linhasRestantes){
			
			$texto 	.= "|___________|__________________________|_______|__________|________|___________|\r\n";
			$texto 	.= "|           |                          |       |          |        |           |\r\n";
			$n += 2;
		}
		
	 	############################################################################################################################################################
		$timestamp  = date("Ymd_His");
		$local_file = "impressao///inbox///imprimir_os_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		//$local_file = "impressao\inbox\imprimir_pedido_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		$fp 		= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
		$salva		= fwrite($fp, $texto);
		
		$texto = fread($fp, filesize($local_file));
		//transformamos as quebras de linha em etiquetas <br>
		$texto = nl2br($texto);
		fclose($fp);
?> 

	
	<script type="text/javascript">
		var raiz = '<?= $raiz ?>';
		if(raiz){
			opener.location.href="index.php?p=pedido&modo="+raiz;
		}
		window.close();
	</script> 
</html>	
