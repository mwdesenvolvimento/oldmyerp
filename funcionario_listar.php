
<?
	require("inc/con_db.php");
	require("funcionario_filtro.php");
	
	//a��es em grupo
	if(isset($_POST['hid_action'])){
		require("funcionario_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}

/**************************** ORDER BY *******************************************/
	$filtroOrder 	= 'fldNome';
	$class 		 = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_funcionario']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'	:  $filtroOrder = "fldId";   break;
			case 'nome'		:  $filtroOrder = "fldNome"; break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_funcionario'] = (!$_SESSION['order_funcionario'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_funcionario'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=funcionario$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_funcionario']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINA��O *******************************************/
	$sSQL = "SELECT * FROM tblfuncionario ". $_SESSION['filtro_funcionario']." ORDER BY " . $_SESSION['order_funcionario'];
	
	$_SESSION['funcionario_relatorio'] = $sSQL;
	
	$rsTotal 	= mysql_query($sSQL);
	$rowsTotal 	= mysql_num_rows($rsTotal);
	
	//defini��o dos limites
	$limite 	= 50;
	$n_paginas 	= 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " limit " . $inicio . "," . $limite;
	$rsFuncionario = mysql_query($sSQL);
	
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#########################################################################################
?>
    <form class="table_form" id="frm_funcionario" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:70px;">
                    	<a <?= ($filtroOrder == 'fldId') 	? "class='$class'" : '' ?> style="width:55px" href="<?=$raiz?>codigo">C&oacute;digo</a>
                    </li>
                    <li class="order" style="width:300px;">
                    	<a <?= ($filtroOrder == 'fldNome') 	? "class='$class'" : '' ?> style="width:285px" href="<?=$raiz?>nome">Funcion&aacute;rio</a>
                    </li>
                    <li style="width:160px; text-align:center;">CPF</li>
                    <li style="width:120px; text-align:center;">Telefone</li>
                    <li style="width:120px; text-align:center;">Telefone (2)</li>
                    <li style="width:80px; text-align:center;">Clientes</li>
                    <li style="width:46px;"></li>
                    <li style="width:20px;"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de funcionarios">
                	<tbody>
<?					
					$id_array = array();
					$n = 0;
					
					$linha = "row";
					$rows = mysql_num_rows($rsFuncionario);
					while ($rowFuncionario = mysql_fetch_array($rsFuncionario)){
						
						$id_array[$n] = $rowFuncionario["fldId"];
						$n += 1;
							
						//formatar cpf_cnpj
						$CPF_CNPJ = formatCPFCNPJTipo_out($rowFuncionario['fldCPF_CNPJ'], $rowFuncionario['fldTipo']);
?>						<tr class="<?= $linha; ?>">
<?				 	 		$icon = ($rowFuncionario["fldDisabled"] ? "bg_disable" : "bg_enable");
							$title = ($rowFuncionario["fldDisabled"] ? "desabilitado" : "habilitado");
?>							<td style="width:0;"><img src="image/layout/<?=$icon?>.gif" alt="status" title="<?=$title?>" /></td>
							<td class="cod"	style="width:44px; text-align:center; padding-right:10px;"><?=str_pad($rowFuncionario['fldId'], 4, "0", STR_PAD_LEFT)?></td>
                            <td style="width:300px;"><?=$rowFuncionario['fldNome']?></td>
							<td style="width:160px; text-align:center;"><?=$CPF_CNPJ?></td>
							<td style="width:120px; text-align:center;"><?=$rowFuncionario['fldTelefone1']?></td>
							<td style="width:120px; text-align:center;"><?=$rowFuncionario['fldTelefone2']?></td>
<?							$sql = mysql_query("select * from tblcliente where fldFuncionario_Id = '" . $rowFuncionario['fldId'] . "'");
							$clientes = mysql_num_rows($sql);
?>							<td style="width:80px; text-align:center;"><?=$clientes?></td>
							<td style="width:27px;"></td>
                            <td style="width:0px;"><a class="edit" href="index.php?p=funcionario_detalhe&modo=cadastro&amp;id=<?=$rowFuncionario['fldId']?>" title="editar"></a></td>
                            <td style="width:0px"><input type="checkbox" name="chk_funcionario_<?=$rowFuncionario['fldId']?>" id="chk_funcionario_<?=$rowFuncionario['fldId']?>" title="selecionar o registro posicionado" /></td>
                        </tr>
<?                      $linha = ($linha == "row") ? "dif-row" : "row";
                   }
?>		 		</tbody>
				</table>
            </div>
       	  
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="index.php?p=funcionario_novo">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" value="excluir" title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                    <li><input type="submit" name="btn_action" id="btn_habilitar" value="habilitar" title="Habilitar registro(s) selecionado(s)" /></li>
                    <li><input type="submit" name="btn_action" id="btn_desabilitar" value="desabilitar" title="Desabilitar registro(s) selecionado(s)" /></li>
                	<li><button id="btn_print" name="btn_action" type="submit" value="imprimir" title="Imprimir relat&oacute;rio de registro(s) selecionado(s)" ></button></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=funcionario&modo=listar";
				include("paginacao.php")
?>		
            </div>      
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>       
        
        </div>
       
	</form>