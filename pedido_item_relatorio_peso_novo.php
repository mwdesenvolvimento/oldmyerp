<?	

	##############################################################################################################
	############################################## REQUIRES ######################################################
	require("inc/relatorio.php");
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	$pdf = new PDF('P', 'mm', 'A4');
	//pode ser P (portrait) ou L (landscape)
	
	$pdf->AliasNbPages();
	//isso faz com que funcione o total de paginas
	
	$pdf->SetMargins(17, 5);
	//defino as margens do documento...
	
	$pdf->AddPage();
	//comando para add a pagina
	
	##### PERIODO #####
	$d1 = $_GET['d1'];
	$d2 = $_GET['d2'];
	###################
	
	######################### TITULO DO RELATORIO #####################################################
	$pdf->SetTextColor(255, 255, 255);
	$pdf->SetFillColor(0, 0, 0);
	$pdf->SetFont('Arial', 'B', 14);
	$pdf->Cell(130, 8, "Relatório de Itens Vendidos", 1, 0, 'C', true);
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(60, 8, "Período selecionado: ".format_date_out($d1)." - ".format_date_out($d2), 1, 0, 'L', true);
	$pdf->SetTextColor(0, 0, 0); //ja reseto a cor da fonte para nao ter problema
	$pdf->Ln();
	###################################################################################################
	
	$sqlProdutos = mysql_query("SELECT 

									tblproduto.fldId,
									tblproduto.fldCodigo,
									tblproduto.fldNome,
									tblproduto.fldPeso,
									SUM(tblproduto.fldPeso * tblpedido_item.fldQuantidade) as fldPeso_Total,
									SUM(tblpedido_item.fldQuantidade) as fldQuantidade,
									tblcategoria.fldNome as fldCategoria,
									tblcategoria.fldId as fldCategoria_Id

								FROM tblpedido 
								
								INNER JOIN 	tblpedido_item 	ON tblpedido.fldId 				= tblpedido_item.fldPedido_Id
								INNER JOIN	tblproduto 		ON tblpedido_item.fldProduto_Id = tblproduto.fldId
								LEFT JOIN 	tblcategoria	ON tblproduto.fldCategoria_Id	= tblcategoria.fldId
								
								WHERE tblpedido.fldPedidoData BETWEEN '$d1' AND '$d2'
								AND tblpedido_item.fldExcluido = '0'
								AND tblpedido.fldExcluido = '0'
								GROUP BY tblproduto.fldId
								ORDER BY fldCategoria, fldPeso_Total ASC");

	//primeiro eu organizo todos os resultados em uma array, com suas categorias...
	$produtos = array();
	while($rowProdutos = mysql_fetch_assoc($sqlProdutos)){
		$produto 							= array();
		$produto['id']						= $rowProdutos['fldId'];
		$produto['codigo'] 					= str_pad($rowProdutos['fldCodigo'], 6, 0, STR_PAD_LEFT);
		$produto['nome'] 					= $rowProdutos['fldNome'];
		$produto['quantidade'] 				= format_number_out($rowProdutos['fldQuantidade']);
		$produto['peso'] 					= format_number_out($rowProdutos['fldPeso']);
		$produto['peso_total'] 				= format_number_out($rowProdutos['fldPeso_Total']);
		$produto['categoria_id'] 			= $rowProdutos['fldCategoria_Id'];
		$produto['categoria'] 				= $rowProdutos['fldCategoria'];
		$produtos[$rowProdutos['fldId']] 	= $produto;
	}
	
	foreach($produtos as $produto){
		//trato os componentes
		$produto_id 			= $produto['id'];
		$sqlComponente 			= mysql_query("SELECT 
													tblpedido_item_componente.* 
												FROM tblpedido_item_componente 
												LEFT JOIN tblpedido ON tblpedido_item_componente.fldPedido_Id = tblpedido.fldId
												WHERE tblpedido_item_componente.fldComponente_Id = '$produto_id'
												AND tblpedido.fldPedidoData BETWEEN '$d1' AND '$d2'
												AND tblpedido.fldExcluido = '0'");
		if(mysql_num_rows($sqlComponente) > 0){
			$sqlComponente 			= mysql_fetch_assoc($sqlComponente);
			$componente_id			= $sqlComponente['fldProduto_Id'];
			$quantidade				= $produtos[$produto_id]['quantidade'];
			$quantidade_componente	= $sqlComponente['fldQtd'];
			while($quantidade > $quantidade_componente){
				$quantidade 							-= $quantidade_componente;
				$produtos[$produto_id]['quantidade'] 	-= $quantidade_componente;
				$produtos[$produto_id]['peso_total']	-= $quantidade_componente * $produtos[$produto_id]['peso'];
			
				if(isset($produtos[$componente_id])){
					//insiro o produto
					$produtos[$componente_id]['quantidade'] += 1;
					$produtos[$componente_id]['peso_total']	+= $sqlProduto_Novo['fldPeso'];
				} else {
					//crio o produto
					$sqlProduto_Novo 				= mysql_query("SELECT tblproduto.*, tblcategoria.fldId as fldCategoria_Id, tblcategoria.fldNome as fldCategoria 
																	FROM tblproduto LEFT JOIN tblcategoria ON tblproduto.fldCategoria_Id = tblcategoria.fldId 
																	WHERE tblproduto.fldId = '$componente_id'") or die(mysql_error());
					$sqlProduto_Novo				= mysql_fetch_assoc($sqlProduto_Novo);
					$novo_produto 					= array();
					$novo_produto['id'] 			= $componente_id;
					$novo_produto['nome'] 			= $sqlProduto_Novo['fldNome'];
					$novo_produto['quantidade'] 	= 1;
					$novo_produto['peso'] 			= $sqlProduto_Novo['fldPeso'];
					$novo_produto['peso_total'] 	= $sqlProduto_Novo['fldPeso'];
					$novo_produto['categoria_id'] 	= $sqlProduto_Novo['fldCategoria_Id'];
					$novo_produto['categoria'] 		= $sqlProduto_Novo['fldCategoria'];
					$produtos[$componente_id] 		= $novo_produto;
				}
			}
		}
	}
	
	//faço as categorias
	$categorias = array();
	foreach($produtos as $produto){
		if(!isset($categorias['categoria_id'])){
			$categorias[$produto['categoria_id']]['categoria_id'] 				= $produto['categoria_id'];
			$categorias[$produto['categoria_id']]['categoria']					= $produto['categoria'];
			$categorias[$produto['categoria_id']]['peso_total']   			   += $produto['peso_total'];
			$categorias[$produto['categoria_id']]['produtos'][$produto['id']]	= $produto;
		} else {
			$categorias[$produto['categoria_id']]['categoria_id'] 				= $produto['categoria_id'];
			$categorias[$produto['categoria_id']]['categoria']					= $produto['categoria'];
			$categorias[$produto['categoria_id']]['peso_total']   			   += $produto['peso_total'];
			$categorias[$produto['categoria_id']]['produtos'][$produto['id']]	= $produto;
		}
	}
	
	unset($produtos); //removo a array de produtos pq agr ja estao em suas categorias
	
	//agora organizo os pesos das categorias
	$sort_categoria = array();
	foreach($categorias as $categoria){	$sort_categoria[$categoria['categoria_id']] = $categoria['peso_total']; }
	asort($sort_categoria);
	$sort_categoria = array_reverse($sort_categoria, true);
	foreach($categorias as $categoria){	$sort_categoria[$categoria['categoria_id']] = $categorias[$categoria['categoria_id']];	}
	$categorias = $sort_categoria;
	
	//agora organizo os pesos de cada produto dentro da categoria
	foreach($categorias as $categoria){
		$sort_produto = array();
		foreach($categoria['produtos'] as $produto){ $sort_produto[$produto['id']] = $produto['peso_total']; }
		asort($sort_produto);
		$sort_produto = array_reverse($sort_produto, true);
		foreach($categoria['produtos'] as $produto){ $sort_produto[$produto['id']]	= $categoria['produtos'][$produto['id']]; }
		$categorias[$categoria['categoria_id']]['produtos'] = $sort_produto; }
	##############################################################################################################
	########################################FIM DO PROCESSAMENTO DOS DADOS########################################
	
	###################################################################################################
	foreach($categorias as $categoria){
	$header 		= array('Cód', 'Produto', 'Qtd vendida', 'Peso total');
	$header_index 	= array('codigo', 'nome', 'quantidade', 'peso_total');
	$data 			= $categoria['produtos'];
	$width 			= array(20, 110, 30, 30);
	$align 			= array("C", "L", "R", "R");
	$pdf->SetFont('Arial', '', 12);
	$pdf->SetFillColor(108, 108, 108);
	$pdf->SetTextColor(255, 255, 255);
	$pdf->Cell(130, 8, "{$categoria['categoria']}", 1, 0, 'C', true);
	$pdf->Cell(60, 8, "Peso total: ".format_number_out($categoria['peso_total']), 1, 1, 'C', true);
	$pdf->Table($header, $data, $width, $align, $header_index); }
	###################################################################################################
	
	$pdf->Output();

?>