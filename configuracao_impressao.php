<?php
	require('configuracao_impressao_action.php');
	$sistemaImpressao = fnc_sistema('sistema_impressao');
?>		
        <!-- configurar impressao -->
    	<div class="form" style="margin-bottom:10px">
			<h3>Configura&ccedil;&otilde;es de Impress&atilde;o</h3>
            <form id="frm_impressao" class="frm_detalhe" style="width:980px; margin-bottom:10px" action="index.php?p=configuracao&modo=impressao" method="post">
                <ul>
                	<li style=" padding:8px; background:#D7D7D7"> 
                        <label for="sel_sistema_impressao">Modo de impress&atilde;o</label>
                        <select style="width:815px;" id="sel_sistema_impressao" name="sel_sistema_impressao" class="sel_sistema_impressao" >
<?							$rsImpressao = mysql_query("SELECT * FROM tblsistema_impressao ORDER BY fldModelo");
							while($rowImpressao = mysql_fetch_array($rsImpressao)){
?>                          	<option <?=($rowImpressao['fldId'] == $sistemaImpressao)? "selected='selected'" : '' ?> value="<?=$rowImpressao['fldId']?>" ><?=$rowImpressao['fldModelo']?> - <?=$rowImpressao['fldDescricao']?></option>
<?							}
?>                		</select>
                    </li>
                    <li style="float:right"><input style="margin: 15px 20px 0 0" type="submit" class="btn_enviar" name="btn_enviar" value="Gravar" ></li>
                    
        		</ul>
            </form>
		</div>
        
		<div class="form">   
            <!-- configurar local da impressora, no caso de impressao txt por driver -->
            <div>
            
            	<h3>Adicionar impressora</h3>
            
                <form id="frm_impressao_local" class="frm_detalhe" style="width:480px; float:left" action="index.php?p=configuracao&modo=impressao" method="post">
                    <ul>
                        <li style=" padding:8px; background:#D7D7D7"> 
                            <label for="sel_impressora_local">Local</label>
                            <select style="width:205px;" id="sel_impressora_local" name="sel_impressora_local" class="sel_impressora_local" >
                                <option value="" >&nbsp;</option>
<?								$rsEstacao = mysql_query("SELECT * FROM tblsistema_estacao WHERE fldId > 0 ORDER BY fldNome");
								while($rowEstacao = mysql_fetch_array($rsEstacao)){
?>                          		<option value="<?=$rowEstacao['fldLocal']?>" ><?=$rowEstacao['fldNome']?></option>
<?								}
?>                			</select>
                    	</li>
                        <li style=" padding:8px; background:#D7D7D7"> 
                            <label for="txt_impressora_nome">Impressora</label>
                            <input type="text" style=" width:205px" id="txt_impressora_nome" name="txt_impressora_nome" value="" />
                        </li>
                        <li><input type="checkbox" style="width: 20px" value="1" name="chk_zebra" id="chk_zebra"/> <span>impressora tipo Zebra</span></li>


                    </ul>
                    <input style="float:right; margin-right: 20px; margin-top:0" type="submit" class="btn_enviar" name="btn_gravar_impressora_local" value="Gravar" >
                </form>
                <div id="table" style="width:480px">
                    <div id="table_container" style="width:480px; height:90px">      
                        <table id="table_general" class="table_general" summary="Listagem">
<?					
							$linha = "row";
							$sSQL = mysql_query("SELECT * FROM tblsistema_impressao_local ORDER BY fldDescricao");
							while($rowImpressora = mysql_fetch_array($sSQL)){
?>								<tr class="<?= $linha; ?>">
                                    <td style="width:5px"></td>
                                    <td style="width:450px"><?=$rowImpressora['fldDescricao']?></td>
                                    <td><a class="delete" style="width:20px" title="Excluir" href="?p=configuracao&modo=impressao&delImpressora=<?=$rowImpressora['fldId']?>" onclick="return confirm('Confirmar excluir?')"></a></td>
                                </tr>
<?								$linha = ($linha == "row") ? "dif-row" : "row";
						   }
?>			 			</table>
					</div>
            	</div>
            </div>
		</div>
        
        <!-- configurar impressao de cada estacao -->
        <div>
            <div class="form">
                <h3>Configura&ccedil;&otilde;es de Impress&atilde;o por Esta&ccedil;&atilde;o</h3>
                <form id="frm_impressao_estacao" class="frm_detalhe" style="width:480px; float:left" action="index.php?p=configuracao&modo=impressao" method="post">
                    <ul>
                        <li style=" padding:8px; background:#D7D7D7"> 
                            <label for="sel_impressao_estacao">Esta&ccedil;&atilde;o de Trabalho</label>
                            <select style="width:205px" id="sel_impressao_estacao" name="sel_impressao_estacao" class="sel_impressao_estacao" >
                                <option value=""></option>
<?								$rsEstacao = mysql_query("SELECT * FROM tblsistema_estacao ORDER BY fldNome");
								while($rowEstacao = mysql_fetch_array($rsEstacao)){
?>                          		<option value="<?=$rowEstacao['fldId']?>" ><?=$rowEstacao['fldNome']?></option>
<?								}
?>                			</select>
                        </li>
                        <li style="padding:8px;background:#D7D7D7"> 
                            <label for="sel_impressao_impressora">Local da Impressora</label>
                            <select style="width:205px;" id="sel_impressao_impressora" name="sel_impressao_impressora" class="sel_impressao_impressora" >
                                <option value="" >&nbsp;</option>
<?								$rsImpressora = mysql_query("SELECT * FROM tblsistema_impressao_local ORDER BY fldDescricao");
								while($rowImpressora = mysql_fetch_array($rsImpressora)){
?>                          		<option value="<?=$rowImpressora['fldId']?>" ><?=$rowImpressora['fldDescricao']?></option>
<?								}
?>                			</select>
                    	</li>
                    </ul>
                    <input style="float:right;margin-right:20px;margin-top:0" type="submit" class="btn_enviar" name="btn_gravar_impressao_estacao" value="Gravar" >
                </form>
                <div id="table" style="width:480px">
                    <div id="table_container" style="width:480px;height:90px">      
                        <table id="table_general" class="table_general">
<?					
							$linha = "row";
							$sSQL = mysql_query("SELECT tblsistema_estacao_impressao.fldId, tblsistema_impressao_local.fldDescricao, tblsistema_estacao.fldNome 
												FROM tblsistema_estacao_impressao 
												INNER JOIN tblsistema_estacao ON tblsistema_estacao_impressao.fldEstacao_Id = tblsistema_estacao.fldId
												INNER JOIN tblsistema_impressao_local ON tblsistema_estacao_impressao.fldImpressora_Id = tblsistema_impressao_local.fldId");
							echo mysql_error();
							while($rowLocal = mysql_fetch_array($sSQL)){
?>								<tr class="<?= $linha; ?>">
                                    <td style="width:5px"></td>
                                    <td style="width:210px"><?=$rowLocal['fldNome']?></td>
                                    <td style="width:210px"><?=$rowLocal['fldDescricao']?></td>
                                    <td><a class="delete" style="width:20px" title="Excluir" href="?p=configuracao&modo=impressao&delLocal=<?=$rowLocal['fldId']?>" onclick="return confirm('Confirmar excluir?')"></a></td>
                                </tr>
<?                     			$linha = ($linha == "row") ? "dif-row" : "row";
						   }
?>			 			</table>
					</div>
           		</div>
			</div>
		</div>

        <div class="form">
                <h3>Mensagens complementares</h3>
                <form id="frm_impressao_mensagem" class="frm_detalhe" style="width:480px; float:left" action="index.php?p=configuracao&modo=impressao" method="post">
<?                  if(isset($_GET['editMensagem'])){
                        $sSQL = mysql_query("SELECT * FROM tblsistema_impressao_mensagem WHERE fldId = ".$_GET['editMensagem']);
                        $rowMensagem = mysql_fetch_array($sSQL);
                    }
?>                  <ul>
                        <li style=" padding:8px; background:#D7D7D7"> 
                            <label for="txa_impressao_mensagem">Mensagem</label>
                            <textarea style="width:350px; height:50px" id="txa_impressao_mensagem" name="txa_impressao_mensagem"><?=$rowMensagem['fldMensagem']?></textarea>
                            <input type="hidden" name="hid_impressao_mensagem_id" id="hid_impressao_mensagem_id" value="<?=$rowMensagem['fldId']?>" />
                        </li>
                        <li style="padding:8px;background:#D7D7D7"> 
                            <label for="txt_impressao_mensagem_ordem">Ordem</label>
                            <input style="width:50px" type="text" id="txt_impressao_mensagem_ordem" name="txt_impressao_mensagem_ordem" value="<?=$rowMensagem['fldOrdem']?>" />                            
                        </li>
                    </ul>
                    <input style="float:right;margin-right:20px;margin-top:0" type="submit" class="btn_enviar" name="btn_gravar_impressao_mensagem" value="Gravar" >
                </form>

                <div id="table" style="width:480px">
                    <div id="table_container" style="width:480px;height:90px">      
                        <table id="table_general" class="table_general">
<?                  
                            $linha = "row";
                            $sSQL = mysql_query("SELECT * FROM tblsistema_impressao_mensagem WHERE fldImpressao = 'venda' ORDER BY fldOrdem");
                            echo mysql_error();
                            while($rowMensagem = mysql_fetch_array($sSQL)){
?>                              <tr class="<?= $linha; ?>">
                                    <td style="width:5px"></td>
                                    <td style="width:390px"><?=$rowMensagem['fldMensagem']?></td>
                                    <td style="width:20px; text-align:right"><?=$rowMensagem['fldOrdem']?></td>
                                    <td style="width:20px"><a class="edit" href="?p=configuracao&modo=impressao&editMensagem=<?=$rowMensagem['fldId']?>" title="editar"></a></td>
                                    <td><a class="delete" style="width:20px" title="Excluir" href="?p=configuracao&modo=impressao&delMensagem=<?=$rowMensagem['fldId']?>" onclick="return confirm('Confirmar excluir?')"></a></td>
                                </tr>
<?                              $linha = ($linha == "row") ? "dif-row" : "row";
                           }
?>                      </table>
                    </div>
                </div>
            </div>