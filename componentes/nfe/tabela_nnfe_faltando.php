
<?php

	//Procedimento para procurar por números de NFes ausentes/pulados
	$limiteBuscaNFe = 100;
	
	//busca pelos números de NFes que estão sendo utilizados para poder comparar com números excluídos ou desfeitos. Essa comparação é feita para não aparecer na tela de reutilização números já utilizados
	/*$rsNumerosNFeUtilizados = mysql_query("SELECT fldnumero FROM tblpedido_fiscal
										   INNER JOIN tblpedido ON tblpedido_fiscal.fldpedido_id = tblpedido.fldId
										   WHERE tblpedido.fldExcluido = 0 AND tblpedido.fldTipo_Id = 3 ORDER BY fldnumero DESC LIMIT {$limiteBuscaNFe}"
										 );*/
	$rsNumerosNFeUtilizados = mysql_query("SELECT fldnumero FROM tblpedido_fiscal INNER JOIN tblpedido ON tblpedido_fiscal.fldpedido_id = tblpedido.fldId
											AND tblpedido.fldExcluido = 0 AND tblpedido_fiscal.fldDocumento_Tipo = 1
												UNION
											SELECT fldnumero FROM tblpedido_fiscal INNER JOIN tblcompra ON tblpedido_fiscal.fldpedido_id = tblcompra.fldId
											AND tblcompra.fldExcluido =0 AND tblpedido_fiscal.fldDocumento_Tipo = 2
											ORDER BY fldnumero DESC LIMIT {$limiteBuscaNFe}");
	
	$x = 0;
	while($rowNumerosNFe = mysql_fetch_array($rsNumerosNFeUtilizados)) {
		$nfeNumerosUtilizados[$x] = $rowNumerosNFe['fldnumero'];
		$x++;
	}
	
	//verificação para evitar erros quando não há NFes
	//$trechoSql = (!empty($nfeNumerosUtilizados)) ? "AND tblpedido_fiscal.fldnumero NOT IN (" . implode(',', $nfeNumerosUtilizados) . ")" : null;
	
	//todos os números de NFes desconsiderando números utilizados
	/*$rsNFe = mysql_query("SELECT tblpedido_fiscal.fldnumero FROM tblpedido_fiscal
						  INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_fiscal.fldpedido_id
						  WHERE tblpedido.fldTipo_Id = 3
						  
						  ORDER BY tblpedido_fiscal.fldnumero DESC LIMIT $limiteBuscaNFe"
						);*/
	$rsNFe = mysql_query("SELECT tblpedido_fiscal.fldnumero FROM tblpedido_fiscal ORDER BY tblpedido_fiscal.fldnumero DESC LIMIT $limiteBuscaNFe");						
	//echo $trechoSql;
	//para evitar erro quando não há nenhuma nota
	if(mysql_num_rows($rsNFe) > 0) {
		
		$x = 0;
		while($rowNFe = mysql_fetch_array($rsNFe)) {
			$nfeSequencia[$x] = $rowNFe['fldnumero'];
			$comecoBuscaNFe   = $rowNFe['fldnumero']; //será o último valor, pois a ordenação está decrescente
			$x++;
		}
		
		//pega o próximo número do sistema (-1 para o próximo número não aparecer na tela de reutilização)
		$fimBuscaNFe = (fnc_sistema('nfe_numero_nota') - 1);
		
		//buscar números de NFe que foram puladas/descartados que possuem uma justificativa
		$rsNFeInutilizada = mysql_query("SELECT fldNFe_Numero FROM tblpedido_nfe_numero_inutilizado WHERE fldNFe_Numero BETWEEN {$comecoBuscaNFe} AND {$fimBuscaNFe}");
		
		$x = 0;
		$nfeInutilizadas = array();
		while($rowNFeInutilizada = mysql_fetch_array($rsNFeInutilizada)) {
			$nfeInutilizadas[$x] = $rowNFeInutilizada['fldNFe_Numero'];
			$x++;
		}
		
		sort($nfeSequencia);
		sort($nfeInutilizadas);
		
		//preencher uma matriz com o intervalo a ser comparado com os números de NFes do banco de dados, dispensando os números que possuem justificativas
		$i = 0;
		for($x=$comecoBuscaNFe; $x<=$fimBuscaNFe; $x++) {
			if(!in_array($x, $nfeInutilizadas)) $intervaloNFe[$i] = $x;
			$i++;
		}
		
		//verificar se há algum intervalo, caso não, não há números para serem reaproveitados
		if(!empty($intervaloNFe) && !empty($nfeSequencia)) {
			//primeiro: compara as matrizes e retornando apenas os números que constam na primeira e não estão na segunda
			//segundo:  junta duas matrizes, o resultado do primeiro passo com o intervalo de números NFe
			//terceiro: remove os números válidos (utilizados em NFes)
			$numerosFaltando = array_unique(array_diff(array_merge($intervaloNFe, array_diff($intervaloNFe, $nfeSequencia)), $nfeNumerosUtilizados));
		}
		
	}
	
	//exibindo ou não a tabela com os números pulados
	if(isset($numerosFaltando) && !empty($numerosFaltando)) {
		
		//pegando os motivos de inutilização pré-cadastrados no banco de dados
		$rsMotivosInutilizacao = mysql_query("SELECT * FROM tblpedido_nfe_numero_inutilizado_motivo");
		$x = 0;
		
		while($rowMotivosInutilizacao = mysql_fetch_array($rsMotivosInutilizacao)) {
			$motivosInutilizacao[$x]['id'] 	 = $rowMotivosInutilizacao['fldId'];
			$motivosInutilizacao[$x]['desc'] = $rowMotivosInutilizacao['fldDescricao'];
			$x++;
		}
		
?>
		<div id="notas_puladas" style="clear: both;">
							
			<h3 id="alert">H&aacute; n&uacute;mero(s) ausente(s) na sequ&ecirc;ncia das NFes</h3>
			<p style="font-size: 14px; margin-bottom: 15px;">Escolha um n&uacute;mero para a NFe atual ou marque o motivo de n&atilde;o utiliza-lo para poder prosseguir.</p>
			
			<table>
				<thead>
					<tr>
						<th>N&ordm; ausente</th>
						<th>Utilizar <a href="#" id="limpar_radio">[limpar]</a></th>
						<th>N&atilde;o utilizar por:</th>
						<th>Observa&ccedil;&otilde;es</th>
					</tr>
				</thead>
				
				<tbody>
<?
					$x = 1;
					foreach($numerosFaltando as $numero) {
?>
					<tr>
						<td><?=$numero?></td>
						<td>
							<input type="radio" id="rad_nnfe_utilizar_n_perdido_<?=$x?>" name="rad_nnfe_n_perdido" value="<?=$numero?>" />
							<input type="hidden" id="hid_nfe_numero_<?=$x?>" name="hid_nfe_numero_<?=$x?>" value="<?=$numero?>" />
						</td>
						<td>
							<select id="sel_nnfe_nao_utilizar_<?=$x?>" name="sel_nnfe_nao_utilizar_<?=$x?>" class="sel_nnfe_nao_utilizar">
								<option value="0"></option>
<?
								foreach($motivosInutilizacao as $motivo) {
?>
									<option value="<?=$motivo['id']?>"><?=$motivo['desc']?></option>
<?
								}
?>
							</select>
						</td>
						<td><input type="text" id="txt_nnfe_observacao_<?=$x?>" name="txt_nnfe_observacao_<?=$x?>" value="" /></td>
					</tr>
<?
						$x++;
					}
?>
				</tbody>
			</table>
			
			<input type="hidden" id="hid_controle_nfe_numero_perdido_enviar" name="hid_controle_nfe_numero_perdido_enviar" value="false" />
			<input type="hidden" id="hid_controle_qtd_numeros" name="hid_controle_qtd_numeros" value="<?=$x - 1?>" />
			
		</div>
<?
	}
	
	unset($x, $i, $limiteBuscaNFe, $rsNFe, $rowNFe, $nfeSequencia, $comecoBuscaNFe, $fimBuscaNFe, $intervaloNFe, $numerosFaltando, $numero, $rsMotivosInutilizacao, $rowMotivosInutilizacao, $motivosInutilizacao, $motivo, $rsNFeInutilizada, $rowNFeInutilizada, $nfeInutilizadas, $rsNumerosNFeUtilizados, $rowNumerosNFe, $nfeNumerosUtilizados);
?>

<script type="text/javascript">

	$('.sel_nnfe_nao_utilizar').change(function(){
		$('.sel_nnfe_nao_utilizar').val($(this).val());
	});
	
	//comportamento para quando selecionar algum número
	$('input[name=rad_nnfe_n_perdido]').click(function() {
		$(this).parents('table').find('select, input:not([type=radio])').attr('disabled', false);
		
		$(this).parents('tr').find('select, input:not([type=radio])').attr('disabled', true);
		$(this).parents('tr').find('select').val(0);
		$(this).parents('tr').find('input:not([type=radio])').val(null);
	});
	
	//resetar radio buttons
	$('a#limpar_radio').click(function(e) {
		e.preventDefault();
		
		$('input[name=rad_nnfe_n_perdido]').attr('checked', false);
		$('#notas_puladas table').find('select, input:not([type=radio])').attr('disabled', false);
	});
	
	//Verificação se os dados do formulário devem ou não ser enviados
	$('form.gravar_nfe').submit(function() {
		var objControle  		= $('#hid_controle_nfe_numero_perdido_enviar');
		var radioChecked 		= false;
		var totalSelects 		= $('select.sel_nnfe_nao_utilizar').length;
		var numerosInutilizados = 0;
		
		//verificando se algum radio button foi selecionado
		$('input[name=rad_nnfe_n_perdido]').each(function() { if($(this).is(':checked')) { radioChecked = true } });
		
		//verificando quantos números - por alguma razão - foram inutilizados
		$('select.sel_nnfe_nao_utilizar').each(function(){ if($(this).val() > 0) { numerosInutilizados++ } });
		
		//se algum radio button estiver selecionado ou a quantidade de números inutilizados for igual a quantidade de números, marca o controle como verdadeiro e prossegue com o processamento da NFe
		if(radioChecked == true || numerosInutilizados == totalSelects) { objControle.val('true'); }
		
		//verificando se deve ou não prosseguir com o envio do formulário
		if(objControle.length && objControle.val() == 'false') {
			alert('Escolha antes um número para a NFe atual ou inutilize-o.');
			return false;
		}
		else {
			return true
		}
	});
	
</script>