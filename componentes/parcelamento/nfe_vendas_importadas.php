<?
	//procedimento referente ao dia pre-definido
	$dia = fnc_sistema('parcelamento_dia_predefinido');
	
	if($dia === 'd') $dataReferenciaParcelamento = date('d/m/Y');
	else $dataReferenciaParcelamento = date("$dia/m/Y", strtotime(date('Y-m-d', strtotime(date('Y-m-d'))) . ' +1 month'));
	
	
	
	if($documento_tipo == 2){
		$rsPagos = mysql_query("SELECT SUM(tblcompra_parcela_baixa.fldValor * (tblcompra_parcela_baixa.fldExcluido * -1 + 1)) AS ValorPago, SUM(tblcompra_parcela_baixa.fldDesconto * (tblcompra_parcela_baixa.fldExcluido * -1 + 1)) AS ValorPagoDesconto FROM tblcompra_parcela_baixa
								LEFT JOIN tblcompra_parcela ON tblcompra_parcela.fldId = tblcompra_parcela_baixa.fldParcela_Id
								WHERE tblcompra_parcela.fldCompra_Id IN (" . join(",", $id_query_array) . ") AND tblcompra_parcela.fldExcluido = 0");
		
		$rsIdParcelas = mysql_query("SELECT tblcompra_parcela_baixa.fldParcela_Id AS ID FROM tblcompra_parcela_baixa
									INNER JOIN tblcompra_parcela ON tblcompra_parcela.fldId = tblcompra_parcela_baixa.fldParcela_Id
									WHERE tblcompra_parcela.fldCompra_Id IN (" . join(",", $id_query_array) . ") AND tblcompra_parcela.fldExcluido = 0
									GROUP BY tblcompra_parcela.fldId");
	}else{
		$rsPagos = mysql_query("SELECT SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) AS ValorPago, SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) AS ValorPagoDesconto FROM tblpedido_parcela_baixa
								LEFT JOIN tblpedido_parcela ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
								WHERE tblpedido_parcela.fldPedido_Id IN (" . join(",", $id_query_array) . ") AND tblpedido_parcela.fldExcluido = 0");
								
		$rsIdParcelas = mysql_query("SELECT tblpedido_parcela_baixa.fldParcela_Id AS ID FROM tblpedido_parcela_baixa
									INNER JOIN tblpedido_parcela ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
									WHERE tblpedido_parcela.fldPedido_Id IN (" . join(",", $id_query_array) . ") AND tblpedido_parcela.fldExcluido = 0
									GROUP BY tblpedido_parcela.fldId");
	}
		
	$rowPago = mysql_fetch_array($rsPagos);
?>
<div class="pedido_parcela" id="editar_parcelas">
<?	if($nfe_parcelamento_exibir == '1'){ #VARIAVEL NO ARQUIVO DE GERAR NFE
?>		<div class="tabs">
            <ul class="menu_modo" style="background: #fff; width:435px">
                <li><a href="parcelamento-avulso" class="atual">Parcelamento</a></li>
                <li><a href="parcelamento-pre-definido">Pr&eacute;-definido</a></li>
            </ul>
            
            <div id="tab_parcelamento-avulso" class="parcelamento tab ativo">
                <div>
                    <label for="txt_parcela">Pagamento</label>
                    <input type="text" id="txt_entrada" name="txt_entrada" value="1" />
                    <span>+</span>
                    <input type="text" id="txt_parcela" name="txt_parcela" value="0" />
                </div>
                <div>
                    <label for="txt_1_vencimento">Vencimento</label>
                    <input type="text" id="txt_1_vencimento" name="txt_1_vencimento" class="calendario-mask" value="<?= $dataReferenciaParcelamento; ?>" />
                </div>
                <input type="button" class="btn_ok" name="btn_calcular" id="btn_calcular" title="ok" value="ok" />
            </div>
<?			$rsPerfil = mysql_query("SELECT tblsistema_pagamento_perfil.fldPerfil, tblsistema_pagamento_perfil_intervalo.fldFrequencia, tblsistema_calendario_intervalo.fldValor FROM tblsistema_pagamento_perfil
								INNER JOIN tblsistema_pagamento_perfil_intervalo ON tblsistema_pagamento_perfil.fldId = tblsistema_pagamento_perfil_intervalo.fldPerfil_Id
								INNER JOIN tblsistema_calendario_intervalo ON tblsistema_pagamento_perfil_intervalo.fldIntervalo_Id = tblsistema_calendario_intervalo.fldId
								ORDER BY tblsistema_pagamento_perfil.fldPerfil ASC, tblsistema_pagamento_perfil_intervalo.fldId");

?>			<div id="tab_parcelamento-pre-definido" class="parcelamento tab">
<? 				if(mysql_num_rows($rsPerfil) > 0) {
?>
               		<div>
                   	 <label for="sel_perfil">Perfil</label>
	<?					$x = 0;
                        while($rowPerfil = mysql_fetch_array($rsPerfil)) {
                            $perfis['Perfil'][$rowPerfil['fldPerfil']][$x] = $rowPerfil['fldFrequencia'] . ';' . $rowPerfil['fldValor'];
                            $x++;
                        }
                        
                        unset($rsPerfil, $rowPerfil, $x);
?>						<select id="sel_perfil" name="sel_perfil">
<?							foreach($perfis['Perfil'] as $indice => $dados) {
?>								<option value="<?=implode('|', $dados)?>"><?=$indice?></option>
<?								}
							unset($perfis, $indice, $dados);
?>          	    	</select>
                    </div>
                
                    <div>
                        <label for="txt_data_inicial">Data Inicial</label>
                        <input type="text" id="txt_data_inicial" name="txt_data_inicial" class="calendario" value="<?= $dataReferenciaParcelamento; ?>" />
                    </div>
                    <input type="button" class="btn_ok" name="btn_calcular_pre_definido" id="btn_calcular_pre_definido" title="ok" value="ok" />
                
<?		 		} else {
?>					<p style="text-align: center; font-size: 12px;">
						Voc&ecirc; n&atilde;o cadastrou nenhum perfil de pagamento. <a href="?p=configuracao&modo=pagamento#frm_perfis" title="Cadastrar um perfil de pagamento" class="link">Clique aqui para cadastrar</a>
					</p>
<?				}
?>			</div>
		</div>
	
        <div class="faturado">
            <label>
                <input type="checkbox" value="faturado" name="chk_faturado" id="chk_faturado" <?php if ($rowPedido['fldFaturado'] == "1") { echo "checked='checked'"; } ?>>
                Faturado
            </label>
        </div>
<?	}else{
		$disabled = ($rowPago['ValorPago'] > 0 )? '':'disabled="disabled"';
?>		<ul style="margin:10px">
			<li style="width:230px">
                <label>
                    <input type="radio" style="width:20px" value="estorno" name="rad_parcela_paga" id="rad_parcela_paga" <?=$disabled?> />
                    Estornar parcelas pagas
                </label>
            </li>
            <li>
                <label>
                    <input type="radio" style="width:20px" value="credito" name="rad_parcela_paga" id="rad_parcela_paga" checked="checked" <?=$disabled?> />
                    Gerar cr&eacute;dito ao cliente
                </label>
			</li>
        </ul>		
<?	}
?>        
	<ul class="parcelamento_cabecalho editar_parcelas">
		<li style="width:30px">n&deg;</li>
		<li style="width:70px">Valor</li>
		<li style="width:70px">Valor Pago</li>
		<li style="width:55px">Desconto</li>
		<li style="width:70px">Vencimento</li>
		<li style="width:100px">Pagamento</li>
		<li style="width:15px">&nbsp;</li>
	</ul>
	<div id="parcelas" class="editar_parcelas">
		
		<div id="hidden">
			<ul class="parcela_detalhe">
				<li>
					<input style="text-align:center" class="txt_parcela_numero" type="text" id="txt_parcela_numero_0" name="txt_parcela_numero_0" value="1" readonly="readonly" />
				</li>
				<li>
					<input class="txt_parcela_valor" type="text" id="txt_parcela_valor_0" name="txt_parcela_valor_0" value="" />
				</li>
				<li>
					<input class="txt_parcela_valor_pago" type="text" id="txt_parcela_valor_pago_0" name="txt_parcela_valor_pago_0" value="0,00" readonly="readonly" />
				</li>
				<li>
					<input class="txt_parcela_valor_pago_desconto" type="text" id="txt_parcela_valor_pago_desconto_0" name="txt_parcela_valor_pago_desconto_0" value="0,00" readonly="readonly" />
				</li>
				<li>
					<input class="txt_parcela_data" type="text" id="txt_parcela_data_0" name="txt_parcela_data_0" value="" />
				</li>
				<li>
					<select id="sel_pagamento_tipo_0" name="sel_pagamento_tipo_0" class="sel_pagamento_tipo" title="Forma de Pagamento">
<?						$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0");
						while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>							<option value="<?= $rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo'] ?></option>
<?						}
?>                  </select> 
				</li>
			</ul>
			<input type="hidden" name="hid_verificar_limite_credito" id="hid_verificar_limite_credito" value="<?=$verificar_limite_credito;?>">
		</div>
		
<?	
		$x = 0;
		while($rowIdParcelas = mysql_fetch_array($rsIdParcelas)) {
			$parcelaId[$x] = $rowIdParcelas['ID'];
			$x++;
		}
		
		unset($x, $rsIdParcelas, $rowIdParcelas);
		$x = 1;
		if($rowPago['ValorPago'] > 0) {
?>
			<ul class="parcela_detalhe pago">
				<li>
					<input style="text-align:center" class="txt_parcela_numero" type="text" id="txt_parcela_numero_<?=$x?>" name="txt_parcela_numero_<?=$x?>" value="<?=$x?>" readonly="readonly" />
					<input type="hidden" id="hid-parcelas-ids" name="hid-parcelas-ids" value="<?= implode(',', $parcelaId)?>" />
				</li>
				<li>
					<input class="txt_parcela_valor" type="text" id="txt_parcela_valor_<?=$x?>" name="txt_parcela_valor_<?=$x?>" value="<?=format_number_out($rowPago['ValorPago'] + $rowPago['ValorPagoDesconto'])?>" />
				</li>
				<li>
					<input class="txt_parcela_valor_pago" type="text" id="txt_parcela_valor_pago_<?=$x?>" name="txt_parcela_valor_pago_<?=$x?>" value="<?=format_number_out($rowPago['ValorPago'])?>" />
				</li>
				<li>
					<input class="txt_parcela_valor_pago_desconto" type="text" id="txt_parcela_valor_pago_desconto_<?=$x?>" name="txt_parcela_valor_pago_desconto_<?=$x?>" value="<?=format_number_out($rowPago['ValorPagoDesconto'])?>" />
				</li>
				<li>
					<input class="txt_parcela_data" type="text" id="txt_parcela_data_<?=$x?>" name="txt_parcela_data_<?=$x?>" value="<?=date('d/m/Y')?>" />
				</li>
				<li>
					<select id="sel_pagamento_tipo_<?=$x?>" name="sel_pagamento_tipo_<?=$x?>" class="sel_pagamento_tipo" title="Forma de Pagamento">
<?						$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0");
						while($rowPagamento = mysql_fetch_array($rsPagamento)){
?>							<option <?=($rowPagamento['fldId'] == 8) ? 'selected="selected"' : '' ?> value="<?= $rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo'] ?></option>
<?						}
?>                  </select>
				</li>
			</ul>
<?			$x++;
				/*
			 * A função round() com precisão de 2 casas decimais abaixo serve para evitar um problema com cálculos matemáticos no PHP.
			 * Precisamente quando o resultado de uma subtração seja 0.
			 * Um exemplo de como o PHP trabalha com o resultado de uma subtração de ponto flutuante: 305.33(75340675450) - 305.33(7534067545065231111)
			 * O PHP enxerga os últimos quatros números (1111) e gera o seguinte resultado: 5.6843418860808E-14
			 */
			$totalRestante = round($pedidoTotal - ($rowPago['ValorPago'] + $rowPago['ValorPagoDesconto']), 2);
			
			if($totalRestante > 0) {
?>					
                <ul class="parcela_detalhe">
                    <li>
                        <input style="text-align:center" class="txt_parcela_numero" type="text" id="txt_parcela_numero_<?=$x?>" name="txt_parcela_numero_<?=$x?>" value="<?=$x?>" readonly="readonly" />
                    </li>
                    <li>
                        <input class="txt_parcela_valor" type="text" id="txt_parcela_valor_<?=$x?>" name="txt_parcela_valor_<?=$x?>" value="<?=format_number_out($totalRestante);?>" />
                    </li>
                    <li>
                        <input class="txt_parcela_valor_pago" type="text" id="txt_parcela_valor_pago_<?=$x?>" name="txt_parcela_valor_pago_<?=$x?>" value="0,00" readonly="readonly" />
                    </li>
                    <li>
                        <input class="txt_parcela_valor_pago_desconto" type="text" id="txt_parcela_valor_pago_desconto_<?=$x?>" name="txt_parcela_valor_pago_desconto_<?=$x?>" value="0,00" readonly="readonly" />
                    </li>
                    <li>
                        <input class="txt_parcela_data" type="text" id="txt_parcela_data_<?=$x?>" name="txt_parcela_data_<?=$x?>" value="<?=date('d/m/Y')?>" />
                    </li>
                    <li>
						<select id="sel_pagamento_tipo_<?=$x?>" name="sel_pagamento_tipo_<?=$x?>" class="sel_pagamento_tipo" title="Forma de Pagamento">
<?						$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0");
						while($rowPagamento = mysql_fetch_array($rsPagamento)){
?>							<option value="<?= $rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo'] ?></option>
<?						}
?>                         </select>
					</li>
				</ul>
<?			}
		}else {
			//caso não haja nada pago, exibirá as parcelas normalmente
			$rsParcela = mysql_query("SELECT * FROM tblpedido_parcela
									 WHERE fldPedido_Id IN (" . join(",", $id_query_array) . ") AND fldExcluido = 0
									 ORDER BY fldVencimento");
			$nParcelas = mysql_num_rows($rsParcela);
			$x 	 	   = 1;
			while($rowParcela = mysql_fetch_array($rsParcela)) {
?>
			<ul class="parcela_detalhe">
				<li>
					<input style="text-align:center" class="txt_parcela_numero" type="text" id="txt_parcela_numero_<?=$x?>" name="txt_parcela_numero_<?=$x?>" value="<?=$x?>" readonly="readonly" />
				</li>
				<li>
					<input class="txt_parcela_valor" type="text" id="txt_parcela_valor_<?=$x?>" name="txt_parcela_valor_<?=$x?>" value="<?=format_number_out($rowParcela['fldValor']);?>" />
				</li>
				<li>
					<input class="txt_parcela_valor_pago" type="text" id="txt_parcela_valor_pago_<?=$x?>" name="txt_parcela_valor_pago_<?=$x?>" value="0,00" readonly="readonly" />
				</li>
				<li>
					<input class="txt_parcela_valor_pago_desconto" type="text" id="txt_parcela_valor_pago_desconto_<?=$x?>" name="txt_parcela_valor_pago_desconto_<?=$x?>" value="0,00" readonly="readonly" />
				</li>
				<li>
					<input class="txt_parcela_data" type="text" id="txt_parcela_data_<?=$x?>" name="txt_parcela_data_<?=$x?>" value="<?=format_date_out($rowParcela['fldVencimento']);?>" />
				</li>
				<li>
					<select id="sel_pagamento_tipo_<?=$x?>" name="sel_pagamento_tipo_<?=$x?>" class="sel_pagamento_tipo" title="Forma de Pagamento">
<?						$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0");
						while($rowPagamento = mysql_fetch_array($rsPagamento)){
?>							<option value="<?= $rowPagamento['fldId'] ?>"<?= ($rowPagamento['fldId'] == $rowParcela['fldPagamento_Id']) ? ' selected="selected"' : ''; ?>><?= $rowPagamento['fldTipo'] ?></option>
<?						}
?>	              </select> 
				</li>
			</ul>
<?				$x++;
			}
		}
?>
		<input type="hidden" name="hid_controle_parcela" id="hid_controle_parcela" value="<?=(isset($nParcelas)) ? $nParcelas : $x;?>" />
	</div> 
</div>




<script language="javascript">
	parcelamento_exibir = '<?=$parcelamento_exibir?>';
	if(parcelamento_exibir == '0'){
		$('ul.parcela_detalhe li').find('input').attr('readonly', 'readonly');
	}
</script>
