<div class="pedido_parcela">
	<div class="tabs">
		<ul class="menu_modo" style="background: #fff; width: 435px;">
			<li><a href="parcelamento-avulso" class="atual">Parcelamento</a></li>
			<li><a href="parcelamento-pre-definido">Pr&eacute;-definido</a></li>
		</ul>
		
		<div id="tab_parcelamento-avulso" class="parcelamento tab ativo">
			
			<div>
				<label for="txt_parcela">Pagamento</label>
				<input type="text" id="txt_entrada" name="txt_entrada" value="1" />
				<span>+</span>
				<input type="text" id="txt_parcela" name="txt_parcela" value="0" />
			</div>
			<div>
				<label for="txt_1_vencimento">Vencimento</label>
				<input type="text" id="txt_1_vencimento" name="txt_1_vencimento" class="calendario-mask" value="<?= date('d/m/Y') ?>" />
			</div>
			<input type="button" class="btn_ok" name="btn_calcular" id="btn_calcular" title="ok" value="ok" />
			
		</div>
		
<?php	$rsPerfil = mysql_query("SELECT tblsistema_pagamento_perfil.fldPerfil, tblsistema_pagamento_perfil_intervalo.fldFrequencia, tblsistema_calendario_intervalo.fldValor FROM tblsistema_pagamento_perfil
								INNER JOIN tblsistema_pagamento_perfil_intervalo ON tblsistema_pagamento_perfil.fldId = tblsistema_pagamento_perfil_intervalo.fldPerfil_Id
								INNER JOIN tblsistema_calendario_intervalo ON tblsistema_pagamento_perfil_intervalo.fldIntervalo_Id = tblsistema_calendario_intervalo.fldId
								ORDER BY tblsistema_pagamento_perfil.fldPerfil ASC, tblsistema_pagamento_perfil_intervalo.fldId");
?>
		<div id="tab_parcelamento-pre-definido" class="parcelamento tab">
<?php 		if(mysql_num_rows($rsPerfil) > 0) {
?>
				<div>
					<label for="sel_perfil">Perfil</label>
<?php				$x = 0;
					while($rowPerfil = mysql_fetch_array($rsPerfil)) {
						$perfis['Perfil'][$rowPerfil['fldPerfil']][$x] = $rowPerfil['fldFrequencia'] . ';' . $rowPerfil['fldValor'];
						$x++;
					}
					
					unset($rsPerfil, $rowPerfil, $x);
?>
					<select id="sel_perfil" name="sel_perfil">
<?						foreach($perfis['Perfil'] as $indice => $dados) {
?>							<option value="<?=implode('|', $dados)?>"><?=$indice?></option>
<?						}
						unset($perfis, $indice, $dados);
?>                  </select>
				</div>
				<div>
					<label for="txt_data_inicial">Data Inicial</label>
					<input type="text" id="txt_data_inicial" name="txt_data_inicial" class="calendario" value="<?= date('d/m/Y') ?>" />
				</div>
				<input type="button" class="btn_ok" name="btn_calcular_pre_definido" id="btn_calcular_pre_definido" title="ok" value="ok" />
				
<?php 		}
			else {
?>
				<p style="text-align: center; font-size: 12px;">
					Voc&ecirc; n&atilde;o cadastrou nenhum perfil de pagamento. <a href="?p=configuracao&modo=pagamento#frm_perfis" title="Cadastrar um perfil de pagamento" class="link">Clique aqui para cadastrar</a>
				</p>
<?php		}
?>		</div>
	</div>
	
	<div class="faturado">
		<label>
			<input type="checkbox" value="faturado" name="chk_faturado" id="chk_faturado" <?php if ($rowCompra['fldFaturado'] == "1") { echo "checked='checked'"; } ?>>
			Faturado
		</label>
	</div>
	
	<ul class="parcelamento_cabecalho">
		<li style="width:30px">n&deg;</li>
		<li style="width:114px;">Valor</li>
		<li style="width:115px;">Vencimento</li>
		<li style="width:146px">Pagamento</li>
		<li style="width:15px">&nbsp;</li>
	</ul>
	
	<div id="parcelas">
	
		<div id="hidden">
			<ul class="parcela_detalhe">
				<li>
					<input style="text-align:center" class="txt_parcela_numero" type="text" id="txt_parcela_numero_0" name="txt_parcela_numero_0" value="1" readonly="readonly" />
				</li>
				<li>
					<input class="txt_parcela_valor" type="text" id="txt_parcela_valor_0" name="txt_parcela_valor_0" value="" />
				</li>
				<li>
					<input class="txt_parcela_data" type="text" id="txt_parcela_data_0" name="txt_parcela_data_0" value="" />
				</li>
				<li>
					<select id="sel_pagamento_tipo_0" name="sel_pagamento_tipo_0" class="sel_pagamento_tipo" title="Forma de Pagamento">
<?						$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0");
						while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>							<option value="<?= $rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo'] ?></option>
<?						}
?>                  </select> 
				</li>
			</ul>
		</div>  
<?		$rsParcela = mysql_query("SELECT * FROM tblcompra_parcela WHERE fldCompra_Id = ".$compra_id." order by fldParcela");
		$rows = mysql_num_rows($rsParcela);
		$x = 1;
		while($rowParcela = mysql_fetch_array($rsParcela)){
?>						
			<ul class="parcela_detalhe">
				<li>
					<input style="text-align:center" class="txt_parcela_numero" type="text" id="txt_parcela_numero_<?=$x?>" name="txt_parcela_numero_<?=$x?>" value="<?=$rowParcela['fldParcela']?>" readonly="readonly" />
				</li>
				<li>
					<input class="txt_parcela_valor" type="text" id="txt_parcela_valor_<?=$x?>" name="txt_parcela_valor_<?=$x?>" value="<?=format_number_out($rowParcela['fldValor'])?>" />
				</li>
				<li>
					<input class="txt_parcela_data" type="text" id="txt_parcela_data_<?=$x?>" name="txt_parcela_data_<?=$x?>" value="<?=format_date_out($rowParcela['fldVencimento'])?>" />
				</li>
				<li>
					<select id="sel_pagamento_tipo_<?=$x?>" name="sel_pagamento_tipo_<?=$x?>" class="sel_pagamento_tipo" title="Forma de Pagamento">
<?						$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0");
						while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>							<option <?=($rowParcela['fldPagamento_Id'] == $rowPagamento['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowPagamento['fldId']?>"><?=$rowPagamento['fldTipo']?></option>
<?						}
?>                  </select> 
				</li>
			</ul>
<?					$x += 1;
		}
?>          <input type="hidden" name="hid_controle_parcela" id="hid_controle_parcela" value="<?=$rows?>" />
			<input type="hidden" name="hid_controle_estatico_parcela" id="hid_controle_estatico_parcela" value="<?=$rows?>" />
        
    </div>  
</div>    