VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   525
      Left            =   1740
      TabIndex        =   0
      Top             =   1320
      Width           =   1245
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   4140
      Top             =   120
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    Dim nFile           As Long
    Dim strFilename     As String
    Dim strFileLog      As String
    Dim strComando      As String
    Dim strParametros   As String
    Dim arrayParametros() As String
    
    
    
    
    
    nFile = FreeFile
    strFilename = "d:\wamp\www\myerp\impressao\cupom_fiscal.txt"
    strFileLog = App.Path & "\log\" & Format(Now(), "yyyymmdd_hhnnss") & ".txt"
    
    
    Open strFilename For Input As #nFile
    
    Do Until EOF(nFile)
        'L� uma linha e pega o comando
        Line Input #nFile, strComando
        
        'L� a pr�xima e pega os par�metros
        Line Input #nFile, strParametros
        'strParametros = Replace(strParametros, "|", ",")
        arrayParametros = Split(strParametros, "|")
        
        'MsgBox arrayParametros(0)
        
        
        
        Select Case strComando
            Case "AbreCupom"
                Mega_FI_AbreCupom arrayParametros(0)

            Case "VendeItem"
                Mega_FI_VendeItem arrayParametros(0), arrayParametros(1), arrayParametros(2), arrayParametros(3), arrayParametros(4), arrayParametros(5), arrayParametros(6), arrayParametros(7), arrayParametros(8)

            Case "IniciaFechamentoCupom"
                Mega_FI_IniciaFechamentoCupom arrayParametros(0), arrayParametros(1), arrayParametros(2)

            Case "EfetuaFormaPagamento"
                Mega_FI_EfetuaFormaPagamento arrayParametros(0), arrayParametros(1)
            
            Case "TerminaFechamentoCupom"
                Mega_FI_TerminaFechamentoCupom arrayParametros(0)


        End Select
    Loop
    
    Close nFile
    
    FileCopy strFilename, strFileLog
    
End Sub

Private Sub Form_Load()
    sPrinter = "daruma"
End Sub
