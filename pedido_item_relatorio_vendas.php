<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Mindware" />
       	
      	<title>myERP - Relat&oacute;rio de Vendas por Item</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print"  href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_calendario.css"></link>
        
	</head>
	<body>

<? 
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");

		$rowDados 		= mysql_fetch_array(mysql_query("select * from tblempresa_info"));
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rowUsuario 	= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		
	
		if($_GET['order']){
			switch($_GET['order']){
				case 'fornecedor' 	: $ordem = 'tblproduto.fldFornecedor_Id'; 	break;
				case 'marca' 		: $ordem = 'tblproduto.fldMarca_Id'; 		break;
				case 'categoria'	: $ordem = 'tblproduto.fldcategoria_Id';	break;
			}
			$order = $ordem.', ';
		}else{
			$order = $_SESSION['order_pedido_item'].', ';
		}
		
		$sSQL = "SELECT 
					tblproduto.fldCodigo, 
					tblproduto.fldNome,
					tblproduto.fldFornecedor_Id, 
					tblpedido_item.fldDescricao,
					SUM(tblpedido_item.fldQuantidade) as fldTotalQuantidade,
					SUM((tblpedido_item.fldValor * tblpedido_item.fldQuantidade)) as fldTotalVendas,
					tblpedido.fldStatus,
					(SELECT tblpedido_funcionario_servico.fldFuncionario_Id FROM tblpedido_funcionario_servico WHERE tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId AND tblpedido_funcionario_servico.fldFuncao_Tipo = 1 LIMIT 1) AS fldFuncionario_Id,
					
					tblendereco_rota.fldRota
        		FROM tblproduto 
        			INNER JOIN tblpedido_item ON tblproduto.fldId = tblpedido_item.fldProduto_Id
			        INNER JOIN tblpedido ON tblpedido_item.fldPedido_Id = tblpedido.fldId
					LEFT  JOIN tblcliente 		ON tblcliente.fldId 				= tblpedido.fldCliente_Id
					LEFT  JOIN tblendereco 		ON tblcliente.fldEndereco_Id 		= tblendereco.fldId
					LEFT  JOIN tblendereco_bairro ON tblendereco_bairro.fldId 		= tblendereco.fldBairro_Id
					
					LEFT 	JOIN tblendereco_rota ON tblendereco_rota.fldId 			= tblendereco_bairro.fldRota_Id
					
				" . $_SESSION['filtro_pedido_item']."
		        AND tblpedido.fldStatus != 1
		        AND tblpedido.fldStatus != 5
				".$_SESSION['txt_pedido_item_data_filtro']."
		        GROUP BY tblpedido_item.fldProduto_Id, tblpedido_item.fldDescricao ".$_SESSION['filtro_edido_item_having']." 
        		ORDER BY $order  tblpedido_item.fldDescricao";
        
        
		
       // echo $sSQL ;
        $rsPedidoItem 	= mysql_query($sSQL);
		$rowsItem 	= mysql_num_rows($rsPedidoItem);
		
		$n	 			= 1; #DEFINE O NUMERO DO BLOCO
		$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
		$limite 		= 42;
		
		$pgTotal 		= $rowsItem / $limite;
		$p = 1;
		

		$contadorCabecalho = '<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Vendas por Item</h1></td>
                <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
            </tr>';
		$tabelaCabecalho =' 
            <tr style="margin:0">
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
							<td style="width: 320px;">Rota: '.$rowsItem['fldRota'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
			<tr class="total">
				<td style="width:580px">&nbsp;</td>
				<td>Movimento '.$_SESSION['txt_pedido_item_data_inicial'].' a '.$_SESSION['txt_pedido_item_data_final'].'</td>
				<td style="width: 6px">&nbsp;</td>
			</tr>
            <tr>
                <td>
                    <table class="table_relatorio" summary="Relat&oacute;rio">
                        <tr style="border:none; margin:8px 0 3px 0">
                            <td style="width:75px;text-align:right;margin-right:10px;font-weight:bold">Código</td>
                            <td style="width:365px;text-align:left;font-weight:bold">Produto</td>
							<td class="valor" style="width:150px;text-align:left;font-weight:bold">Fornecedor</td>
							<td class="valor" style="width:65px;text-align:right;font-weight:bold">Qtde</td>
							<td class="valor" style="width:100px;text-align:right;margin:0;font-weight:bold">Valor Total</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';

?>

    <div id="no-print">
        <a class="print" href="#" onClick="window.print()">imprimir</a>
    </div>

	<table class="relatorio_print" style="page-break-before:avoid">
<?      print $contadorCabecalho.$tabelaCabecalho;

	######################################################################################################################################################################################
	########### ITENS ####################################################################################################################################################################

		while($rowPedidoItem = mysql_fetch_array($rsPedidoItem)){
			echo mysql_error(); 
			$rsFornecedor = mysql_query('SELECT fldNomeFantasia FROM tblfornecedor WHERE fldId ='.$rowPedidoItem['fldFornecedor_Id']);
			$rowFornecedor = mysql_fetch_array($rsFornecedor);
			$rodape_quantidade 	+= $rowPedidoItem['fldTotalQuantidade'];
			$rodape_valor		+= $rowPedidoItem['fldTotalVendas'];
?>			<tr>
				<td style="width:75px;text-align:right;margin-right:10px;"><?=$rowPedidoItem['fldCodigo']?></td>
				<td style="width:365px;text-align:left; display:inline; overflow:hidden"><?=$rowPedidoItem['fldDescricao']?></td>
				<td style="width:150px;text-align:left; display:inline; overflow:hidden"><?=substr($rowFornecedor['fldNomeFantasia'],0,20)?></td>
				<td class="valor" style="width:65px;text-align:right;"><?=format_number_out($rowPedidoItem['fldTotalQuantidade'])?></td>
				<td class="valor" style="width:95px;text-align:right;"><?=format_number_out($rowPedidoItem['fldTotalVendas'])?></td>
			</tr>
			
<?	#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
						if(($n == $limite) or ($x == $rowsItem)){
?>										</table>    
									</td>
								</tr>
							</table>		
<?        									
							if($x < $rowsItem){
								$p += 1;
								$contadorCabecalho = '<tr style="border-bottom: 2px solid">
						                <td style="width: 600px"><h1>Relat&oacute;rio de Vendas por Item</h1></td>
						                <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
						            </tr>';
								$n = 1;
								print '<table class="relatorio_print">'.$contadorCabecalho.$tabelaCabecalho;
							}
						}
						else {$x+= 1; $n += 1;}
		}
?>        
	             
	<table name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
        <tr>
            <td style="width:450px">&nbsp;</td>
            <td style="width:60px">Quantidade</td>
            <td style="width:80px; text-align:right"><?=format_number_out($rodape_quantidade)?></td>
            <td style="width:3px; border-left:1px solid">&nbsp;</td>
            <td style="width:70px">Total</td>
            <td style="width:80px; text-align:right"><?=format_number_out($rodape_valor)?></td>
        </tr>
    </table>        
	</body>
</html>	
