<?php
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_cliente_cod'] 				= "";
		$_SESSION['txt_cliente'] 	 				= "";
		$_SESSION['txt_cliente_cpf'] 				= "";
		$_SESSION['sel_cliente_status'] 			= "";
		$_SESSION['txt_cliente_municipio_codigo'] 	= "";
		$_SESSION['sel_cliente_funcionario'] 		= "";
		$_POST['chk_cliente_nome'] 					=  false;
		$_SESSION['txt_cliente_beneficio'] 			= "";
		$_SESSION['txt_cliente_veiculo'] 			= "";
		$_SESSION['txt_data_nascimento1'] 			= "";
		$_SESSION['txt_data_nascimento2'] 			= "";
		$_SESSION['txt_cliente_endereco'] 			= "";
		$_SESSION['txt_cliente_telefone'] 			= "";
	}
	else{
		$_SESSION['txt_cliente_cod'] 				= (isset($_POST['txt_cliente_cod']) 		? $_POST['txt_cliente_cod'] 		: $_SESSION['txt_cliente_cod']);
		$_SESSION['txt_cliente'] 					= (isset($_POST['txt_cliente']) 			? $_POST['txt_cliente'] 			: $_SESSION['txt_cliente']);
		$_SESSION['txt_cliente_cpf'] 				= (isset($_POST['txt_cpf_cliente']) 		? $_POST['txt_cpf_cliente'] 		: $_SESSION['txt_cpf_cliente']);
		$_SESSION['sel_cliente_status'] 			= (isset($_POST['sel_cliente_desabilitado'])? $_POST['sel_cliente_desabilitado']: $_SESSION['sel_cliente_status']);
		$_SESSION['txt_cliente_municipio_codigo'] 	= (isset($_POST['txt_municipio_codigo']) 	? $_POST['txt_municipio_codigo'] 	: $_SESSION['txt_cliente_municipio_codigo']);
		$_SESSION['sel_cliente_funcionario'] 		= (isset($_POST['sel_cliente_funcionario']) ? $_POST['sel_cliente_funcionario']	: $_SESSION['sel_cliente_funcionario']);
		$_SESSION['txt_cliente_beneficio'] 			= (isset($_POST['txt_cliente_beneficio']) 	? $_POST['txt_cliente_beneficio'] 	: $_SESSION['txt_cliente_beneficio']);
		$_SESSION['txt_cliente_veiculo'] 			= (isset($_POST['txt_cliente_veiculo']) 	? $_POST['txt_cliente_veiculo'] 	: $_SESSION['txt_cliente_veiculo']);
		$_SESSION['txt_data_nascimento1'] 			= (isset($_POST['txt_data_nascimento1']) 	? $_POST['txt_data_nascimento1']	: $_SESSION['txt_data_nascimento1']);
		$_SESSION['txt_data_nascimento2'] 			= (isset($_POST['txt_data_nascimento2']) 	? $_POST['txt_data_nascimento2']	: $_SESSION['txt_data_nascimento2']);
		$_SESSION['txt_cliente_endereco'] 			= (isset($_POST['txt_endereco']) 			? $_POST['txt_endereco'] 			: $_SESSION['txt_cliente_endereco']);
		$_SESSION['txt_cliente_telefone'] 			= (isset($_POST['txt_cliente_telefone']) 	? $_POST['txt_cliente_telefone'] 	: $_SESSION['txt_cliente_telefone']);
	}

?>
<form id="frm-filtro" action="index.php?p=cliente" method="post">
    <fieldset>
        <legend>Buscar por:</legend>
        <ul>
            <li style="height:40px">
<?				$codigo = ($_SESSION['txt_cliente_cod'] ? $_SESSION['txt_cliente_cod'] : "c&oacute;digo");
                ($_SESSION['txt_cliente_cod'] == "código") ? $_SESSION['txt_cliente_cod'] = '' : '';
?>      		<input style="width:50px" type="text" name="txt_cliente_cod" id="txt_cliente_cod" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo?>"/>
            </li>
            <li style="height:40px">
                <input type="checkbox" name="chk_cliente_nome" id="chk_cliente_nome" <?=($_POST['chk_cliente_nome'] == true ? print 'checked ="checked"' : '')?>/>
<?				$cliente = ($_SESSION['txt_cliente'] ? $_SESSION['txt_cliente'] : "nome");
                ($_SESSION['txt_cliente'] == "nome") ? $_SESSION['txt_cliente'] = '' : '';
?>     			<input style="width: 165px" type="text" name="txt_cliente" id="txt_cliente" onfocus="limpar (this,'nome');" onblur="mostrar (this, 'nome');" value="<?=$cliente?>"/>
                <small>marque para qualquer parte do campo</small>
            </li>
            <li style="height:40px">
<?				$telefone = ($_SESSION['txt_cliente_telefone'] ? $_SESSION['txt_cliente_telefone'] : "Telefone");
                ($_SESSION['txt_cliente_telefone'] == "Telefone") ? $_SESSION['txt_cliente_telefone'] = '' : '';
?>     			<input style="width: 80px" type="text" name="txt_cliente_telefone" id="txt_cliente_telefone" onfocus="limpar (this,'Telefone');" onblur="mostrar (this, 'Telefone');" class="fone-mask" value="<?=$telefone?>"/>
            </li>
            <li style="height:40px">
<?				$cpf = ($_SESSION['txt_cliente_cpf'] ? $_SESSION['txt_cliente_cpf'] : "CPF/CNPJ");
                ($_SESSION['txt_cliente_cpf'] == "CPF/CNPJ") ? $_SESSION['txt_cliente_cpf'] = '' : '';
?>     			<input style="width: 120px" type="text" name="txt_cpf_cliente" id="txt_cpf_cliente" onfocus="limpar (this,'CPF/CNPJ');" onblur="mostrar (this, 'CPF/CNPJ');" value="<?=$cpf?>"/>
                <small style="margin-left: 40px"> *apenas n&uacute;meros</small>
            </li>
			<li style="height:40px">
                <select id="sel_cliente_desabilitado" name="sel_cliente_desabilitado" style="width: 90px;" title="status do cliente">
                    <option value="">status</option>
<?					$rsStatus = mysql_query("SELECT * FROM tblcliente_status");
					while($rowStatus = mysql_fetch_array($rsStatus)){
?>						<option <?=($_SESSION['sel_cliente_status'] == $rowStatus['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowStatus['fldId']?>"><?=$rowStatus['fldStatus']?></option>
<?					}
?>				</select>
            </li>
			<li style="height:40px">
				<label for="txt_data_nascimento1">Nascimento</label>
<?				$data1 = ($_SESSION['txt_data_nascimento1'] ? $_SESSION['txt_data_nascimento1'] : "");
?>     			<input title="dd/mm" style="text-align:center;width: 50px" type="text" name="txt_data_nascimento1" id="txt_data_nascimento1" class="aniversario-mask" value="<?=$data1?>"/>
      		</li>
            <li style="height:40px">
<?				$data2 = ($_SESSION['txt_data_nascimento2'] ? $_SESSION['txt_data_nascimento2'] : "");
?>     			<input title="dd/mm" style="text-align:center;width: 50px" type="text" name="txt_data_nascimento2" id="txt_data_nascimento2" class="aniversario-mask" value="<?=$data2?>"/>
      		</li>
            <li style="height:40px; margin-top: 3px;">
<?				$municipio = ($_SESSION['txt_cliente_municipio_codigo'] ? $_SESSION['txt_cliente_municipio_codigo'] : "c&oacute;d. munic&iacute;pio");
                ($_SESSION['txt_cliente_municipio_codigo'] == "cód. município") ? $_SESSION['txt_cliente_municipio_codigo'] = '' : '';
?>     			<input style="width: 75px;" type="text" name="txt_municipio_codigo" id="txt_municipio_codigo" onfocus="limpar (this,'c&oacute;d. munic&iacute;pio');" onblur="mostrar (this,'c&oacute;d. munic&iacute;pio');" value="<?=$municipio?>"/>
                <a href="municipio_busca" title="Localizar" class="modal" rel="680-380"><img src="image/layout/search.gif" alt="localizar" /></a>
            </li>
            <li style="height:40px">
                <select id="sel_cliente_funcionario" name="sel_cliente_funcionario" >
                    <option value="">Funcion&aacute;rio</option>
<?					$rsFuncionario = mysql_query("select * from tblfuncionario where fldDisabled = '0' ORDER BY fldNome ASC");
                    while($rowFuncionario= mysql_fetch_array($rsFuncionario)){
?>						<option <?=($_SESSION['sel_cliente_funcionario'] == $rowFuncionario['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowFuncionario['fldId'] ?>"><?= $rowFuncionario['fldNome'] ?></option>
<?					}
?>				</select>
            </li>
<?			if($_SESSION['sistema_tipo'] == 'automotivo'){            
?>            	<li>
<?					$veiculo = ($_SESSION['txt_cliente_veiculo'] ? $_SESSION['txt_cliente_veiculo'] : "placa do ve&iacute;culo");
               		($_SESSION['txt_cliente_veiculo'] == "placa do veículo") ? $_SESSION['txt_cliente_veiculo'] = '' : '';
?>      			<input style="width: 100px" type="text" name="txt_cliente_veiculo" id="txt_cliente_veiculo" onfocus="limpar (this,'placa do ve&iacute;culo');" onblur="mostrar (this, 'placa do ve&iacute;culo');" value="<?=$veiculo?>"/>
            		<small>*igual ao cadastro</small>
                </li>
<?			}

			if($_SESSION['sistema_tipo'] == 'financeira'){            
?>            	<li>
<?					$beneficio = ($_SESSION['txt_cliente_beneficio'] ? $_SESSION['txt_cliente_beneficio'] : "num. benef&iacute;cio");
               		($_SESSION['txt_cliente_beneficio'] == "num. benefício") ? $_SESSION['txt_cliente_beneficio'] = '' : '';
?>      			<input style="width: 100px" type="text" name="txt_cliente_beneficio" id="txt_cliente_beneficio" onfocus="limpar (this,'num. benef&iacute;cio');" onblur="mostrar (this, 'num. benef&iacute;cio');" value="<?=$beneficio?>"/>
            	</li>
<?			}
?>         	<li>
<?				$endereco = ($_SESSION['txt_cliente_endereco'] ? $_SESSION['txt_cliente_endereco'] : "Endere&ccedil;o");
                ($_SESSION['txt_cliente_endereco'] == "Endereço") ? $_SESSION['txt_cliente_endereco'] = '' : '';
?>     			<input type="hidden" name="txt_endereco_codigo" id="txt_endereco_codigo" value="" />
				<input style="width: 300px" type="text" name="txt_endereco" id="txt_endereco" onfocus="limpar (this,'Endere&ccedil;o');" onblur="mostrar (this, 'Endere&ccedil;o');" value="<?=$endereco?>"/>
                <a href="endereco_busca" title="Localizar" class="modal" rel="680-380"><img src="image/layout/search.gif" alt="localizar" /></a>
                <small> *apenas rua</small>
            </li>
			<li style="float:right">
            	<button style="margin:0" type="submit" name="btn_exibir" title="Exibir">Exibir</button>
    		</li>
            <li style="float:right">
            	<button style="margin:0" type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
    		</li>
		</ul>            
    </fieldset>
</form>
<?
	/** inicializando a string sql para realizar a consulta ($filtro)
	 * assim sempre existirá a clásula where, mesmo sendo irrelevante neste ponto
	 */
	
	if(($_SESSION['txt_cliente_cod']) != ""){
		$filtro .= "AND tblcliente.fldCodigo = '".$_SESSION['txt_cliente_cod']."'";
	}

	if(($_SESSION['txt_cliente']) != ""){
		$cliente = addslashes($_SESSION['txt_cliente']); // no caso de aspas, pra nao dar erro na consulta
		if($_POST['chk_cliente_nome'] == true){
			$filtro .= "AND (tblcliente.fldNome LIKE '%$cliente%' OR tblcliente.fldNomeFantasia LIKE '%$cliente%')";
		}else{
			$filtro .= "AND (tblcliente.fldNome LIKE '$cliente%' OR tblcliente.fldNomeFantasia LIKE '$cliente%')";
		}
	}
	
	if(($_SESSION['txt_cliente_cpf']) != ""){
		
		$filtro .= "AND tblcliente.fldCPF_CNPJ = '".$_SESSION['txt_cliente_cpf']."'";
	}
	
	if(($_SESSION['sel_cliente_status']) != ""){
		
		$filtro .= "AND tblcliente.fldStatus_Id = '" . $_SESSION['sel_cliente_status'] . "'";
	}
		
	if(($_SESSION['txt_cliente_municipio_codigo']) != ""){
		
		$filtro .= "AND tblcliente.fldMunicipio_Codigo = '".$_SESSION['txt_cliente_municipio_codigo']."'";
	}
	
	if(($_SESSION['sel_cliente_funcionario']) != ""){
		
		$funcionario = addslashes($_SESSION['sel_cliente_funcionario']);
		$filtro .= "AND tblcliente.fldFuncionario_Id = '$funcionario'";
	}
	
	if(($_SESSION['txt_cliente_beneficio']) != ""){
		
		$filtro .= "AND tblcliente.fldBeneficioNum1 = '".$_SESSION['txt_cliente_beneficio']."' OR tblcliente.fldBeneficioNum2 = '".$_SESSION['txt_cliente_beneficio']."'";
	}	
	
	if(($_SESSION['txt_cliente_veiculo']) != ""){
		
		$filtro .= "AND tblcliente_veiculo.fldPlaca = '".$_SESSION['txt_cliente_veiculo']."'";
	}
	
	#################
	if($_SESSION['txt_data_nascimento1'] != "" || $_SESSION['txt_data_nascimento2'] != ""){
		
		$data_dados		= explode('/', $_SESSION['txt_data_nascimento1']); 
		$dia_busca 		= $data_dados[0]; //pega o dia que a pessoa digitou.
		$mes_busca 		= $data_dados[1]; //pega o mes que a pessoa digitou.
		$data_busca1 	= "{$mes_busca}-{$dia_busca}"; //junta tudo em uma variável chamada $data_busca, e separa os dois por um traço de modo contrário, para fazer a consulta no sql em breve.
		
		$data_dados 	= explode('/', $_SESSION['txt_data_nascimento2']); 
		$dia_busca 		= $data_dados[0]; //pega o dia que a pessoa digitou.
		$mes_busca 		= $data_dados[1]; //pega o mes que a pessoa digitou.
		$data_busca2 	= "{$mes_busca}-{$dia_busca}"; //junta tudo em uma variável chamada $data_busca, e separa os dois por um traço de modo contrário, para fazer a consulta no sql em breve.
		
		if($_SESSION['txt_data_nascimento1'] != "" && $_SESSION['txt_data_nascimento2'] != ""){
			$filtro .= " AND RIGHT(tblcliente.fldNascimento_Abertura, 5) BETWEEN '$data_busca1' AND '$data_busca2'" ;
		}elseif	($_SESSION['txt_data_nascimento1'] != "" && $_SESSION['txt_data_nascimento2'] == ""){
			$filtro .= " AND RIGHT(tblcliente.fldNascimento_Abertura, 5) >= '$data_busca1'";
		}elseif	($_SESSION['txt_data_nascimento1'] == "" && $_SESSION['txt_data_nascimento2'] != "" ){
			$filtro .= " AND RIGHT(tblcliente.fldNascimento_Abertura, 5) <= '$data_busca2'";
		}
	}
	
	if(($_SESSION['txt_cliente_endereco']) != ""){
		$filtro .= " AND tblcliente.fldEndereco LIKE '%".$_SESSION['txt_cliente_endereco']."%'";
	}
	
	if(($_SESSION['txt_cliente_telefone']) != ""){
		$filtro .= " and (tblcliente.fldTelefone1 LIKE '%".$_SESSION['txt_cliente_telefone']."%' OR tblcliente.fldTelefone2 LIKE '%".$_SESSION['txt_cliente_telefone']."%')";
	}
	
	//transferir para a sessão
	$_SESSION['filtro_cliente'] = $filtro;
	
?>