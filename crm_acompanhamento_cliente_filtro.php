<?php
	
	//recebendo a data do calendário de período
	if(isset($_POST['txt_calendario_data_inicial']) && isset($_POST['txt_calendario_data_final'])){
	    $_SESSION['txt_data1'] = (isset($_POST['txt_calendario_data_inicial'])) ? $_POST['txt_calendario_data_inicial'] : '';
	    $_SESSION['txt_data2'] = (isset($_POST['txt_calendario_data_final']))	? $_POST['txt_calendario_data_final'] 	: '';
	}
	
	$_SESSION['txt_data2'] 	= (isset($_SESSION['txt_data2'])) ? $_SESSION['txt_data2'] : date('d/m/Y', strtotime('+5 day', strtotime(date('Y-m-d'))));
	
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_acompanhamento_codigo'] 		= "";
		$_SESSION['txt_acompanhamento_cliente'] 	= "";
		$_SESSION['sel_acompanhamento_funcionario'] = "";
		$_SESSION['sel_acompanhamento_prioridade'] 	= "";
		$_SESSION['sel_acompanhamento_situacao'] 	= "";
		$_SESSION['sel_acompanhamento_tipo'] 		= "";
		$_SESSION['txt_data1'] 						= "";
		$_SESSION['txt_data2'] 						= date('d/m/Y', strtotime('+5 day', strtotime(date('Y-m-d'))));
		$_POST['chk_cliente'] 						= false;
	}else{
		$_SESSION['txt_acompanhamento_codigo'] 		= (isset($_POST['txt_acompanhamento_codigo']) 		? $_POST['txt_acompanhamento_codigo'] 		: $_SESSION['txt_acompanhamento_codigo']);
		$_SESSION['txt_acompanhamento_cliente'] 	= (isset($_POST['txt_acompanhamento_cliente']) 		? $_POST['txt_acompanhamento_cliente'] 		: $_SESSION['txt_acompanhamento_cliente']);
		$_SESSION['sel_acompanhamento_funcionario']	= (isset($_POST['sel_acompanhamento_funcionario']) 	? $_POST['sel_acompanhamento_funcionario'] 	: $_SESSION['sel_acompanhamento_funcionario']);
		$_SESSION['sel_acompanhamento_prioridade'] 	= (isset($_POST['sel_acompanhamento_prioridade']) 	? $_POST['sel_acompanhamento_prioridade'] 	: $_SESSION['sel_acompanhamento_prioridade']);
		$_SESSION['sel_acompanhamento_situacao'] 	= (isset($_POST['sel_acompanhamento_situacao']) 	? $_POST['sel_acompanhamento_situacao'] 	: $_SESSION['sel_acompanhamento_situacao']);
		$_SESSION['sel_acompanhamento_tipo'] 		= (isset($_POST['sel_acompanhamento_tipo']) 		? $_POST['sel_acompanhamento_tipo'] 		: $_SESSION['sel_acompanhamento_tipo']);
		$_SESSION['txt_data1'] 						= (isset($_POST['txt_data1'])						? $_POST['txt_data1']						: $_SESSION['txt_data1']);
		$_SESSION['txt_data2'] 						= (isset($_POST['txt_data2'])						? $_POST['txt_data2']						: $_SESSION['txt_data2']);
	}

?>
<form id="frm-filtro" action="index.php?p=<?=$_GET['p']?>" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
    	<li style="height:40px">
<?			$codigo_acompanhamento = ($_SESSION['txt_acompanhamento_codigo'] ? $_SESSION['txt_acompanhamento_codigo'] : "ticket");
			($_SESSION['txt_acompanhamento_codigo'] == "ticket") ? $_SESSION['txt_acompanhamento_codigo'] = '' : '';
?>      	<input style="width: 80px" type="text" name="txt_acompanhamento_codigo" id="txt_acompanhamento_codigo" onfocus="limpar (this,'ticket');" onblur="mostrar (this, 'ticket');" value="<?=$codigo_acompanhamento?>" />
		</li>
        
        <li style="height:40px">
        	<input type="checkbox" name="chk_cliente" id="chk_cliente" <?=($_POST['chk_cliente'] == true ? print 'checked ="checked"' : '')?>/>
<?			$cliente = ($_SESSION['txt_acompanhamento_cliente'] 	? $_SESSION['txt_acompanhamento_cliente'] : "cliente");
			($_SESSION['txt_acompanhamento_cliente'] == "cliente") 	? $_SESSION['txt_acompanhamento_cliente'] = '' : '';
?>      	<input style="width: 260px" type="text" name="txt_acompanhamento_cliente" id="txt_acompanhamento_cliente" onfocus="limpar (this,'cliente');" onblur="mostrar (this, 'cliente');" value="<?=$cliente?>" />
			<small>marque para qualquer parte do campo</small>
        </li>
		
        <li style="height:40px">
            <select style="width:220px" id="sel_acompanhamento_funcionario" name="sel_acompanhamento_funcionario">
                <option value="">funcion&aacute;rio</option>
<?				$rsFuncionario = mysql_query("SELECT * FROM tblfuncionario");
				while($rowFuncionario= mysql_fetch_array($rsFuncionario)){
?>					<option <?=($_SESSION['sel_acompanhamento_funcionario'] == $rowFuncionario['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowFuncionario['fldId'] ?>"><?= $rowFuncionario['fldNome'] ?></option>
<?				}
?>			</select>
		</li>
		
        <li style="height:40px">
      		<label for="txt_data1">Agendamento: </label>
<?			$data1 = ($_SESSION['txt_data1'] ? $_SESSION['txt_data1'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 80px" type="text" name="txt_data1" id="txt_data1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
		
		<li style="height:40px">
<?			$data2 = ($_SESSION['txt_data2'] ? $_SESSION['txt_data2'] : "");
?>     		<input title="Data final" style="text-align:center;width: 80px" type="text" name="txt_data2" id="txt_data2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,crm_acompanhamento_cliente" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>

		<li style="height: 40px;">
            <select style="width:150px" id="sel_acompanhamento_prioridade" name="sel_acompanhamento_prioridade">
                <option value="">prioridade</option>
<?				$rsPrioridade = mysql_query("SELECT * FROM tblcrm_acompanhamento_cliente_prioridade");
				while($rowPrioridade = mysql_fetch_array($rsPrioridade)){
?>					<option <?=($_SESSION['sel_acompanhamento_prioridade'] == $rowPrioridade['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowPrioridade['fldId'] ?>"><?= $rowPrioridade['fldPrioridade_Nome'] ?></option>
<?				}
?>			</select>
		</li>
		
		<li style="height: 40px;">
            <select style="width:150px" id="sel_acompanhamento_situacao" name="sel_acompanhamento_situacao">
                <option value="">situa&ccedil;&atilde;o</option>
				<option value="0">Todos</option>
<?				$rsSituacao = mysql_query("SELECT * FROM tblcrm_acompanhamento_cliente_situacao");
				while($rowSituacao = mysql_fetch_array($rsSituacao)){
?>					<option <?=($_SESSION['sel_acompanhamento_situacao'] == $rowSituacao['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowSituacao['fldId'] ?>"><?= $rowSituacao['fldSituacao_Nome'] ?></option>
<?				}
?>			</select>
		</li>
		
        <li>
            <select style="width:150px" id="sel_acompanhamento_tipo" name="sel_acompanhamento_tipo">
                <option value="">tipo</option>
<?				$rsTipo = mysql_query("SELECT fldId, fldTipo_Nome FROM tblcrm_acompanhamento_cliente_tipo WHERE fldExcluido = 0 ORDER BY fldTipo_Nome");
				while($rowTipo = mysql_fetch_array($rsTipo)){
?>					<option <?=($_SESSION['sel_acompanhamento_tipo'] == $rowTipo['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowTipo['fldId'] ?>"><?= $rowTipo['fldTipo_Nome'] ?></option>
<?				}
?>			</select>
		</li>
		
        <li style="margin-left:235px">
        	<button type="submit" name="btn_limpar" style="margin:0" title="Limpar Filtro">Limpar filtro</button>
        </li>
        <li>
	        <button type="submit" name="btn_exibir" style="margin:0" title="Exibir">Exibir</button>
        </li>
    </ul>
    
  </fieldset>
</form>

<?
	//Data de agendamento
	if(format_date_in($_SESSION['txt_data2']) != "") {
		if(format_date_in($_SESSION['txt_data2']) != "") {
			$filtro .= " AND (tblcrm_acompanhamento_agenda.fldData BETWEEN '".format_date_in($_SESSION['txt_data1'])."' AND '".format_date_in($_SESSION['txt_data2'])."' OR tblcrm_acompanhamento_agenda.fldData IS NULL)";
		}
		else {
			$filtro .= " AND tblcrm_acompanhamento_agenda.fldData = '".format_date_in($_SESSION['txt_data1'])."'";
		}
	}
	
	//Ticket/código de acompanhamento
	if(($_SESSION['txt_acompanhamento_codigo']) != ""){
		$filtro .= " AND tblcrm_acompanhamento_cliente_chamado.fldId = '".$_SESSION['txt_acompanhamento_codigo']."'";
	}
	
	//Nome do cliente
	if(($_SESSION['txt_acompanhamento_cliente']) != ""){
		$cliente = addslashes($_SESSION['txt_acompanhamento_cliente']); // no caso de aspas, pra nao dar erro na consulta
		
		if($_POST['chk_cliente'] == true){
			$filtro .= " AND (tblcliente.fldNome LIKE '%$cliente%' OR tblcliente.fldNomeFantasia LIKE '%$cliente%')";
		}else{
			$filtro .= " AND (tblcliente.fldNome LIKE '$cliente%' OR tblcliente.fldNomeFantasia LIKE '$cliente%')";
		}
	}
	
	//Funcionário encarregado
	if(($_SESSION['sel_acompanhamento_funcionario']) != ""){
		$filtro .= " AND tblcrm_acompanhamento_agenda.fldFuncionario_Id = '".$_SESSION['sel_acompanhamento_funcionario']."'";
	}
	
	//Prioridade
	if(($_SESSION['sel_acompanhamento_prioridade']) != ""){
		$filtro .= " AND tblcrm_acompanhamento_cliente_chamado.fldPrioridade_Id = '".$_SESSION['sel_acompanhamento_prioridade']."'";
	}
	
	//Situação
	if(($_SESSION['sel_acompanhamento_situacao']) != "" && ($_SESSION['sel_acompanhamento_situacao']) != '0'){
		$filtro .= " AND tblcrm_acompanhamento_cliente_chamado.fldSituacao_Id = '".$_SESSION['sel_acompanhamento_situacao']."'";
	}
	elseif(($_SESSION['sel_acompanhamento_situacao']) == '0'){
		$filtro .= " AND tblcrm_acompanhamento_cliente_chamado.fldSituacao_Id > 0";
	}
	else{
		$filtro .= " AND tblcrm_acompanhamento_cliente_chamado.fldSituacao_Id <> 3";
	}
	
	//Tipo/Marcador de acompanhamento
	if(($_SESSION['sel_acompanhamento_tipo']) != ""){
		$filtro .= " AND tblcrm_acompanhamento_agenda.fldTipo_Id = '".$_SESSION['sel_acompanhamento_tipo']."'";
	}
	
	//transferir para a sessão
	$_SESSION['filtro_acompanhamento'] = $filtro;

?>