<?Php

	if(isset($_GET['delete'])){
		$foto = $_GET['delete'];
		
		unlink("../myerp_arquivos_clientes/".$_SESSION["sistema_cliente_id"]."/".$cliente_id."/large/".$foto);  
		unlink("../myerp_arquivos_clientes/".$_SESSION["sistema_cliente_id"]."/".$cliente_id."/small/".$foto);  
	}

	$diretorio = "../myerp_arquivos_clientes/".$_SESSION["sistema_cliente_id"]."/".$cliente_id;
		
	if(isset($_POST["btn_upload"])){
		
		$file = $_FILES['file'];
		$filename = $file['name'];	
		$path     = $file['tmp_name'];
		
		
		if(!file_exists($diretorio)){
		  mkdir($diretorio);   
		  chmod($diretorio."/", 0777); 
		  
		  mkdir($diretorio."/large");   
		  chmod($diretorio."/large/", 0777); 
		  
		  mkdir($diretorio."/small");     
		  chmod($diretorio."/small/", 0777);   
		}
		
		//$diretorio = $diretorio."/large";
		//troca espacos por '_'
		if($_POST['txt_nome'] != ''){
			$nome = $_POST['txt_nome'];
			$nome = str_replace(" ", "_", $nome);
			$filename = $nome.".jpg";
		}
		
		$new_path = $diretorio."/large/".$filename;

		if(move_uploaded_file($path, $new_path)) {
			// Vamos usar a biblioteca WideImage para o redimensionamento das imagens
			require("lib/WideImage/WideImage.php");
			
			// Carrega a imagem enviada
			$original = WideImage::load($new_path);
			
			// Redimensiona a imagem original
			$original->resize(650, 650, 'outside', 'down')->saveToFile($new_path, null, 90);
			
			// Cria a miniatura
			$new_path = $diretorio."/small/".$filename;
			$original->resize(200, 180, 'outside', 'any')->saveToFile($new_path, null, 90); // Redimensiona e salva

		}
	}
?>

<div class="form">
	<h3>Inserir imagem</h3>
	<div id="upload">
        <form class="frm_detalhe" id="upload" name="upload" action="?p=cliente_detalhe&id=<?=$cliente_id?>&modo=arquivos" enctype="multipart/form-data" method="post">
            <small>A imagem ser&aacute; redimensionada automaticamente para 200x120px.</small>
            <input style="display:table" type="file" class="file" id="file" name="file" value="Procurar">
            <ul>
				<li style="margin:0; margin-right: 650px">
					<label for="txt_nome">Renomear imagem</label>
				</li>
                <li style="margin:0">
                	<input type="text" id="txt_nome" name="txt_nome" value="">
                </li>
                <li style="margin-top:0;">
                	<input style="margin-top: 0" type="submit" class="btn_enviar" name="btn_upload" value="Gravar" >
				</li>
			</ul>
        </form>
	</div>

<? 	if(file_exists("../myerp_arquivos_clientes/".$_SESSION["sistema_cliente_id"]."/".$cliente_id."/small")){    
?>
		<h3>Imagens adicionadas</h3>
		<ul id="image">
<?
		// Script para listar arquivos do diret�rio
		if ($handle=opendir($diretorio."/small")) {
			$x=0;
			//l� arquivos do diret�rio
			while (false!==($file=readdir($handle))){
				//l� apenas com esta extens�o
				if((mb_substr($file,strlen($file)-3,3) == "jpg")||(mb_substr($file,strlen($file)-3,3) == "JPG")){
					//evita leitura de . e ..
					if ($file!="." && $file!="..") {
						//armazena nomes dos arquivos na matriz
						$matrix[$x]=$file;
						$x++;
					} 
				}
			} 
			closedir($handle);
		}
		
		$x=0;
		while($matrix[$x]){
?>			<li>
				<div id="image">
                	<img id="<?=$x?>" src="<?=$diretorio?>/small/<?=$matrix[$x]?>" alt="<?=$diretorio?>/small/<?=$matrix[$x]?>" />
				</div>
                <p><?=$matrix[$x]?></p>                	
                <ul class="action">
                    <li><a class="detalhe" title="exibir" href="<?=$diretorio?>/large/<?=$matrix[$x]?>" rel="lightbox"></a></li>
                    <li><a class="print" href = javascript:abrirPOPUP('modal/cliente_arquivo_imprimir.php?id=<?=$cliente_id?>&file=<?=$matrix[$x]?>','350','100');></a></li>
                    <li><a class="excluir" title="excluir" href="?p=cliente_detalhe&id=<?=$cliente_id?>&modo=arquivos&delete=<?=$matrix[$x]?>" onclick="return confirm('Deseja excluir os registros selecionados?')" ></a></li>
                </ul>
                
			</li>
<?			$x++;
		}	
?>		</ul>	
<?	}    
?>
</div>
