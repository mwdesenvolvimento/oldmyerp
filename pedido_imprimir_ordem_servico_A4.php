<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Imprimir Venda</title>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Robots" content="none" />
    
        <link rel="stylesheet" type="text/css" media="all" href="style/impressao/style_pedido_imprimir_A4.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        
	</head>
	<body>
<?
	ob_start();
	session_start();
	//conectar ao db
	require_once("inc/con_db.php");
	require_once("inc/fnc_general.php");
	require_once("inc/fnc_ibge.php");
	
	$raiz 		= $_GET['raiz'];
	$pedido_id  = $_GET['id'];
	$data 		= date("Y-m-d");
	$rsPedido  	= mysql_query("SELECT SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) as fldPedidoValor,
								 tblpedido.*,tblpedido.fldId as fldPedidoId, tblcliente.*, tblcliente.fldId as fldClienteId, tblpedido.fldObservacao as fldObservacaoPedido
								 FROM tblpedido 
								 LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
								 INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								 WHERE tblpedido.fldId = $pedido_id GROUP BY tblpedido_item.fldPedido_Id");
	$rowPedido 	= mysql_fetch_array($rsPedido);
	
	$endereco 	= $rowPedido['fldEndereco'];
	$bairro 	= $rowPedido['fldBairro'];
	$municipio 	= fnc_ibge_municipio($rowPedido['fldMunicipio_Codigo']);
	$uf 	 	= fnc_ibge_uf_sigla($rowPedido['fldMunicipio_Codigo']);
	$CPFCNPJ 	= formatCPFCNPJTipo_out($rowPedido['fldCPF_CNPJ'], $rowPedido['fldTipo']);
	
	if($rowPedido['fldFuncionario_Id']){
		$rsFuncionario  = mysql_query("select * from tblfuncionario where fldId = ".$rowPedido['fldFuncionario_Id']);
		$rowFuncionario = mysql_fetch_array($rsFuncionario);
	}

	/*----------------------------------------------------------------------------------------------*/
	$rsEmpresa  		= mysql_query("SELECT * FROM tblempresa_info");
	$rowEmpresa 		= mysql_fetch_array($rsEmpresa);
	
	if($rowPedido['fldVeiculo_Id']){
		$rsVeiculo = mysql_query("SELECT tblpedido.fldVeiculo_Id,
								 tblcliente_veiculo.*
								 FROM tblcliente_veiculo RIGHT JOIN tblpedido ON tblcliente_veiculo.fldId = tblpedido.fldVeiculo_Id
								 WHERE tblpedido.fldId = $pedido_id");
		
		$rowVeiculo = mysql_fetch_array($rsVeiculo);
		echo mysql_error();
		$veiculo 	= $rowVeiculo['fldVeiculo'];
		$placa 		= $rowVeiculo['fldPlaca'];
	}
	
	/*----------------------------------------------------------------------------------------------*/
	#CRIANDO RODAPE  !!!########################################################################################################################################
	$sSQL = "SELECT SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor
			FROM tblpedido_parcela 
			LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
			WHERE tblpedido_parcela.fldPedido_Id = $pedido_id GROUP BY tblpedido_parcela.fldPedido_Id";
	$rsPedidoBaixa 	= mysql_query($sSQL);
	$rowPedidoBaixa	= mysql_fetch_array($rsPedidoBaixa);
	
	//Para trazer o desconto dos itens e calcular o subtotal
	$ItemDesconto 			= mysql_fetch_array(mysql_query("SELECT SUM((fldValor * fldQuantidade * fldDesconto) / 100) AS ItemDesconto FROM tblpedido_item WHERE tblpedido_item.fldPedido_Id = ".$rowPedido['fldPedidoId']));
	
	$total_pedido 			= $rowPedido['fldPedidoValor'];
	$subtotal_pedido		= $total_pedido - $ItemDesconto['ItemDesconto']; // considerando o desconto nos itens
	$desconto_pedido 		= $rowPedido['fldDesconto'];
	$desconto_reais_pedido 	= $rowPedido['fldDescontoReais'];
	$desconto 				= ($subtotal_pedido * $desconto_pedido) / 100;
	$total_descontos 		= $desconto + $desconto_reais_pedido;
	$total_pedido_apagar 	= ($subtotal_pedido - $total_descontos);
	$total_baixa 			= $rowPedidoBaixa['fldBaixaValor'];
	$valor_devedor 			= $total_pedido_apagar - $rowPedidoBaixa['fldBaixaValor'];
	
?>
    <div id="no-print">
        <div id="impressao_cabecalho">
            <ul id="bts">
                <li><a href="#" onclick ="window.print()"><span>Imprimir</span></a></li>
            </ul>
        </div>
	</div>
    <table id="pedido_imprimir">
<?

	//PEGA O STATUS DO PEDIDO
	$status_n 	= $rowPedido['fldStatus'];
	$rsStatus 	= mysql_query("SELECT * FROM tblpedido_status WHERE fldId = $status_n");
	$rowStatus 	= mysql_fetch_array($rsStatus);
	$txtStatus 	= $rowStatus['fldStatus'];
	//FIM DA VERIFICACAO DE STATUS

	if(fnc_sistema('impressao_mostrar_dados_empresa') == '1'){
		$empresa_razao_social = $rowEmpresa['fldRazao_Social'];
		$CPFCNPJ_Empresa 	= 'CNPJ '.formatCPFCNPJTipo_out($rowEmpresa['fldCPF_CNPJ'], $rowEmpresa['fldTipo']);
		$endereco_empresa 	= $rowEmpresa['fldEndereco'];
		$numero_empresa		= $rowEmpresa['fldNumero'];
		$bairro_empresa 	= $rowEmpresa['fldBairro'];
		$municipio_empresa	= fnc_ibge_municipio($rowEmpresa['fldMunicipio_Codigo']);
		$uf_empresa			= fnc_ibge_uf_sigla($rowEmpresa['fldMunicipio_Codigo']);
		$empresa_telefone1  = $rowEmpresa['fldTelefone1'];
		$empresa_telefone2  = $rowEmpresa['fldTelefone2'];
		$endereco_completo  = $endereco_empresa.', '.$numero_empresa.' - '.$bairro_empresa.' • '.$municipio_empresa.'/'.$uf_empresa;
	}else{
		$empresa_razao_social = '';
		$CPFCNPJ_Empresa 	= '';
		$endereco_empresa 	= '';
		$numero_empresa		= '';
		$bairro_empresa 	= '';
		$municipio_empresa	= '';
		$uf_empresa			= '';
		$empresa_telefone1  = '';
		$empresa_telefone2  = '';
	}
?>
		<tr><td>
            <table class="cabecalho">
                <tr style="width: 200px;height:120px; float:left;"><td><img src="image/layout/logo_empresa.jpg" alt="logo" /></td></tr>
                <tr>
                    <td><?=$empresa_razao_social?></td>
                    <td><?=$CPFCNPJ_Empresa?></td>
                    <td><?=$endereco_completo?></td>
                    <td><?=$empresa_telefone1.' '.$empresa_telefone2?></td>
                </tr>
            </table>
		</td></tr>
        <tr>
        	<td>
                <table class="pedido_imprimir_dados">
                	<tr><td>
                        <table class="pedido_dados" style="margin:10px 0 0 0">
                            <tr class="dados">
                                <td style="width:5cm"><strong>C&oacute;d. Venda: 	</strong><?=$rowPedido['fldPedidoId']?></td>
                                <td style="width:5cm"><strong>Data Venda: 		</strong><?=format_date_out($rowPedido['fldPedidoData'])?></td>
                                <td style="width:5cm"><strong>Status: <?=$txtStatus?> </strong></td>
								<td style="width:11.7cm"><strong>Cliente: 			</strong><?=$rowPedido['fldNome']?></td>
                                <td style="width:4cm"><strong>Telefone 1: 		</strong><?=$rowPedido['fldTelefone1']?></td>
                                <td style="width:4cm"><strong>Telefone 2: 		</strong><?=$rowPedido['fldTelefone2']?></td>
                                <td style="width:20cm"><strong>End.: 				</strong><?=$rowPedido['fldEndereco']. ', '. $rowPedido['fldNumero'].' - '.$rowPedido['fldBairro']?></td>
								<td style="width:6.5cm"><strong>Veiculo: </strong><?=$veiculo?></td>
								<td style="width:6.5cm"><strong>Placa: </strong><?=$placa?></td>
                            </tr>
                        </table> 
                    </td></tr>
                    <tr><td>
                        <table id="pedido_observacao" style="margin:10px 0;">
                            <tr style="background:#CCC;"><td>
                                <h2 style="padding:0px 5px;">Observa&ccedil;&atilde;o</h2>
                            </td></tr>
                            <tr><td>
								<p style="width:760px; word-wrap: break-word; height:70px; max-height:70px;"><?=$rowPedido['fldObservacaoPedido']?></p>
							</tr></td>
                        </table>
                	</td></tr>
					<tr>
                    	<td>
                        	<table class="pedido_descricao" style="margin-top:20px">
                                <tr class="descricao">
                                    <td style="width:20cm; text-align: center;">Mecanico: _____________________________________________    Inicio: _____ : ______   Fim: _____ : _____</td>
                                    <td style="width:20cm; text-align: center;">Mecanico: _____________________________________________    Inicio: _____ : ______   Fim: _____ : _____</td>
                                    <td style="width:20cm; text-align: center;">Mecanico: _____________________________________________    Inicio: _____ : ______   Fim: _____ : _____</td>
							</table>
						</td>
					</tr>

                    <table class="pedido_dados" style="margin:15px 0 0 0">
                        <tr style="font-size:12px; text-align:center; background:#DDD">
                            <td style="width:5cm; border:1px solid #000"><strong>Produto</strong></td>
                            <td style="width:5cm; border:1px solid #000"><strong>Descrição</strong></td>
                            <td style="width:2.5cm; border:1px solid #000"><strong>Custo</strong></td>
							<td style="width:3cm; border:1px solid #000"><strong>Preço</strong></td>
							<td style="width:1.5cm; border:1px solid #000"><strong>Qtd</strong></td>
							<td style="width:3cm; border:1px solid #000"><strong>Valor</strong></td>
                        </tr>
                        <? 
                        $i = 1;
                        while($i <= 20){ ?>
                        <tr style="font-size:12px; height:30px">
                            <td style="width:5cm; border:1px solid #000"></td>
                            <td style="width:5cm; border:1px solid #000"></td>
                            <td style="width:2.5cm; border:1px solid #000"></td>
							<td style="width:3cm; border:1px solid #000"></td>
							<td style="width:1.5cm; border:1px solid #000"></td>
							<td style="width:3cm; border:1px solid #000"></td>
                        </tr>
						<? $i++; } ?>
                    </table>

        		</table>
            </td>
		</tr>
	</table>
    </body>
</html>