
<?

	if(isset($_POST['params'][1])){
?>		
		<script type="text/javascript">
			
			var itemID = <?= $_POST['params'][1] ?>;
			$('#txt_pedido_nfe_volume_quantidade').val($('input[name=txt_pedido_nfe_transporte_volume_qtd_'+itemID+']').val());
			$('#txt_pedido_nfe_volume_especie').val($('input[name=txt_pedido_nfe_transporte_volume_especie_'+itemID+']').val());
			$('#txt_pedido_nfe_volume_marca').val($('input[name=txt_pedido_nfe_transporte_volume_marca_'+itemID+']').val());
			$('#txt_pedido_nfe_volume_numero').val($('input[name=txt_pedido_nfe_transporte_volume_numero_'+itemID+']').val());
			$('#txt_pedido_nfe_volume_peso_liquido').val($('input[name=txt_pedido_nfe_transporte_volume_peso_liquido_'+itemID+']').val());
			$('#txt_pedido_nfe_volume_peso_bruto').val($('input[name=txt_pedido_nfe_transporte_volume_peso_bruto_'+itemID+']').val());
			
        </script>
<?	}

	$controle = 0;
?>

    <form action="" method="post" class="frm_detalhe">
    	<ul>
            <li>
                <label for="txt_pedido_nfe_volume_quantidade">Quantidade</label>
                <input style="width: 70px" type="text" id="txt_pedido_nfe_volume_quantidade" name="txt_pedido_nfe_volume_quantidade" value=""/>
            </li>
            <li>
                <label for="txt_pedido_nfe_volume_especie">Esp&eacute;cie</label>
                <input type="text" id="txt_pedido_nfe_volume_especie" name="txt_pedido_nfe_volume_especie" value="" />
            </li>
            <li>
                <label for="txt_pedido_nfe_volume_marca">Marca</label>
                <input type="text" id="txt_pedido_nfe_volume_marca" name="txt_pedido_nfe_volume_marca" value="" />
            </li>
            <li>
                <label for="txt_pedido_nfe_volume_numero">Numera&ccedil;&atilde;o</label>
                <input style="width: 70px" type="text" id="txt_pedido_nfe_volume_numero" name="txt_pedido_nfe_volume_numero" value="" />
            </li>
            <li>
                <label for="txt_pedido_nfe_volume_peso_liquido">Peso L&iacute;quido (kg)</label>
                <input style="width: 118px" type="text" id="txt_pedido_nfe_volume_peso_liquido" name="txt_pedido_nfe_volume_peso_liquido" value="" />
            </li>
            <li>
                <label for="txt_pedido_nfe_volume_peso_bruto">Peso Bruto (kg)</label>
                <input style="width: 118px" type="text" id="txt_pedido_nfe_volume_peso_bruto" name="txt_pedido_nfe_volume_peso_bruto" value="" />
            </li>
		</ul>
    	
        <fieldset style="width: 580px">
        	<legend>Lacres</legend>
        	<ul style="margin-left: 45px">
            	<li>
                    <label for="txt_pedido_nfe_volume_lacre">N&ordm; Lacre</label>
                    <input style="width: 320px" type="text" id="txt_pedido_nfe_volume_lacre" name="txt_pedido_nfe_volume_lacre" value="" />
				</li>
                <li>
					<button class="btn_sub_small" name="btn_lacre_inserir" id="btn_lacre_inserir" title="Inserir" >ok</button>
                </li>
			</ul>                
            <div class="dados_listar" style="margin:0 auto;border:1px solid #999;margin-bottom:10px">
				<ul class="dados_cabecalho">
                    <li style="width:100px">item</li>
                    <li style="width:360px">Lacre</li>
                    <li style="width:20px; border:0">&nbsp;</li>
                </ul>
                <div class="dados_listar_dados lacre_listar"> 
                    <div id="hidden"> 
                        <ul class="volume_listar ul_lacre">
                            <li style="width:102px;border:0"><input type="text" style="width:100px; text-align:center" class="txt_pedido_nfe_volume_lacre_item" name="txt_pedido_nfe_volume_lacre_item" value="" readonly="readonly" /></li>
                            <li style="width:340px;border:0"><input type="text" style="width:338px" class="txt_pedido_nfe_volume_lacre_numero" name="txt_pedido_nfe_volume_lacre_numero" value="" readonly="readonly" /></li>
                            <li style="width:22px;height:20px"><a style="height:20px;background-position:3px" class="a_excluir" name="excluir" href="" title="Excluir item"></a></li>
                        </ul>
                    </div>
<?					if($_POST['params'][2] != ''){
						$volumeLacre = substr($_POST['params'][2], 0 ,strlen($_POST['params'][2]) - 1);
						$volumeLacre = explode(';',$volumeLacre);
						$controle = count($volumeLacre);
						foreach($volumeLacre as $lacre){
							$lacre = explode('=',$lacre);
?>							<ul class="volume_listar ul_lacre">
								<li style="width:102px;border:0"><input type="text" style="width:100px; text-align:center" class="txt_pedido_nfe_volume_lacre_item" name="txt_pedido_nfe_volume_lacre_item_<?=$lacre[0]?>" value="<?=$lacre[0]?>" readonly="readonly" /></li>
								<li style="width:340px;border:0"><input type="text" style="width:338px" class="txt_pedido_nfe_volume_lacre_numero" name="txt_pedido_nfe_volume_lacre_numero_<?=$lacre[0]?>" value="<?=$lacre[1]?>" readonly="readonly" /></li>
								<li style="width:22px;height:20px"><a style="height:20px;background-position:3px" class="a_excluir" name="excluir_<?=$lacre[0]?>" href="" title="Excluir item"></a></li>
							</ul>
<?						}
					}
?>               	<input type="hidden" name="hid_lacre_controle" id="hid_lacre_controle" value="<?=$controle?>" />     
                </div>
            </div>
		</fieldset>
        
        <ul style="float:right; margin-right: 12px">
            <li>
	            <input type="hidden" name="hid_volume" id="hid_volume" value="<?=$_POST['params'][1]?>" />     
                <input type="submit" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="gravar" title="gravar" />
            </li>
		</ul>             
    </form>
    
	<script type="text/javascript">
	
		$('#txt_pedido_nfe_volume_quantidade').select();
		//formatando apos inserir valor
		$('#txt_pedido_nfe_volume_quantidade, #txt_pedido_nfe_volume_peso_liquido, #txt_pedido_nfe_volume_peso_bruto').blur(function(){
			if($(this).val() != ''){
				$(this).val(float2br(br2float($(this).val()).toFixed(2)));
			}
		});
		
		$('#btn_enviar').click(function(event){
			event.preventDefault();
			
			volumeId = $('#hid_volume').val();
			if( volumeId != ''){
				
				//LACRES ###############################################################################################################################################################################
				controle = $('#hid_lacre_controle').val();
				n = 1;
				lacres = '';
				while(n <= controle){
					lacreitem 	= $('input[name=txt_pedido_nfe_volume_lacre_item_'+n+']').val();
					lacrenumero	= $('input[name=txt_pedido_nfe_volume_lacre_numero_'+n+']').val();
					lacres = lacres + lacreitem+"="+lacrenumero+";";
					n++;
				}
				
				$('input[name=txt_pedido_nfe_transporte_volume_qtd_'+volumeId+']').val($('#txt_pedido_nfe_volume_quantidade').val());
				$('input[name=txt_pedido_nfe_transporte_volume_especie_'+volumeId+']').val($('#txt_pedido_nfe_volume_especie').val());
				$('input[name=txt_pedido_nfe_transporte_volume_marca_'+volumeId+']').val($('#txt_pedido_nfe_volume_marca').val());
				$('input[name=txt_pedido_nfe_transporte_volume_numero_'+volumeId+']').val($('#txt_pedido_nfe_volume_numero').val());
				$('input[name=txt_pedido_nfe_transporte_volume_peso_liquido_'+volumeId+']').val($('#txt_pedido_nfe_volume_peso_liquido').val());
				$('input[name=txt_pedido_nfe_transporte_volume_peso_bruto_'+volumeId+']').val($('#txt_pedido_nfe_volume_peso_bruto').val());
				$('.hid_pedido_nfe_transporte_volume_lacre_'+volumeId+']').val(lacreVolume);
				$('a[name=edit_'+volumeId+']').attr('href', 'pedido_nfe_transporte_volume,'+volumeId+','+lacres);
				
			}else{
			
				$('.volume_listar:first').clone(true).appendTo('#dados_listar_volume');
				$('#hid_volume_controle').val(parseInt($('#hid_volume_controle').val()) + 1);
				
				//LACRES ###############################################################################################################################################################################
				controle = $('#hid_lacre_controle').val();
				n = 1;
				lacres = '';
				lacreVolume = '';
				while(n <= controle){
					lacreitem 	= $('input[name=txt_pedido_nfe_volume_lacre_item_'+n+']').val();
					lacreNumero	= $('input[name=txt_pedido_nfe_volume_lacre_numero_'+n+']').val();
					
					lacreVolume = lacreVolume + lacreNumero+",";
					lacres = lacres + lacreitem+"="+lacreNumero+";";
					n++;
				}
				
				$('.txt_pedido_nfe_transporte_volume_qtd:last').val($('#txt_pedido_nfe_volume_quantidade:last').val());
				$('.txt_pedido_nfe_transporte_volume_especie:last').val($('#txt_pedido_nfe_volume_especie:last').val());
				$('.txt_pedido_nfe_transporte_volume_marca:last').val($('#txt_pedido_nfe_volume_marca:last').val());
				$('.txt_pedido_nfe_transporte_volume_numero:last').val($('#txt_pedido_nfe_volume_numero:last').val());
				$('.txt_pedido_nfe_transporte_volume_peso_liquido:last').val($('#txt_pedido_nfe_volume_peso_liquido:last').val());
				$('.txt_pedido_nfe_transporte_volume_peso_bruto:last').val($('#txt_pedido_nfe_volume_peso_bruto:last').val());
				$('.hid_pedido_nfe_transporte_volume_lacre:last').val(lacreVolume);
				$('.edit_volume:last').attr('href', 'pedido_nfe_transporte_volume,'+$('#hid_volume_controle').val()+','+lacres);
				
				$('.hid_pedido_nfe_transporte_volume_lacre:last').attr('name', $('.hid_pedido_nfe_transporte_volume_lacre:last').attr('class') + "_" + $('#hid_volume_controle').val());
				$('.txt_pedido_nfe_transporte_volume_qtd:last').attr('name', $('.txt_pedido_nfe_transporte_volume_qtd:last').attr('class') + "_" + $('#hid_volume_controle').val());
				$('.txt_pedido_nfe_transporte_volume_especie:last').attr('name', $('.txt_pedido_nfe_transporte_volume_especie:last').attr('class') + "_" + $('#hid_volume_controle').val());
				$('.txt_pedido_nfe_transporte_volume_marca:last').attr('name', $('.txt_pedido_nfe_transporte_volume_marca:last').attr('class') + "_" + $('#hid_volume_controle').val());
				$('.txt_pedido_nfe_transporte_volume_numero:last').attr('name', $('.txt_pedido_nfe_transporte_volume_numero:last').attr('class') + "_" + $('#hid_volume_controle').val());
				$('.txt_pedido_nfe_transporte_volume_peso_liquido:last').attr('name', $('.txt_pedido_nfe_transporte_volume_peso_liquido:last').attr('class') + "_" + $('#hid_volume_controle').val());
				$('.txt_pedido_nfe_transporte_volume_peso_bruto:last').attr('name', $('.txt_pedido_nfe_transporte_volume_peso_bruto:last').attr('class') + "_" + $('#hid_volume_controle').val());
				$('.hid_pedido_nfe_transporte_volume_lacre:last').attr('name', $('.hid_pedido_nfe_transporte_volume_lacre:last').attr('class') + "_" + $('#hid_volume_controle').val());
				$('.edit:last').attr('name', 'edit_' + $('#hid_volume_controle').val());
				$('.excluir_volume:last').attr('name', 'excluir_volume_' + $('#hid_volume_controle').val());
				
			}
			$('div.modal-body').remove();
		});
		
		//#########################################################################################################################################################################################
		$('#btn_lacre_inserir').click(function(event){
			event.preventDefault();
			
			$('.ul_lacre:first').clone(true).appendTo('.lacre_listar');
			
			$('#hid_lacre_controle').val(parseInt($('#hid_lacre_controle').val()) + 1);
			
			$('.txt_pedido_nfe_volume_lacre_item:last').val($('#hid_lacre_controle').val());
			$('.txt_pedido_nfe_volume_lacre_numero:last').val($('#txt_pedido_nfe_volume_lacre:last').val());
			
			$('.txt_pedido_nfe_volume_lacre_item:last').attr('name', $('.txt_pedido_nfe_volume_lacre_item:last').attr('class') + "_" + $('#hid_lacre_controle').val());
			$('.txt_pedido_nfe_volume_lacre_numero:last').attr('name', $('.txt_pedido_nfe_volume_lacre_numero:last').attr('class') + "_" + $('#hid_lacre_controle').val());
			$('.a_excluir:last').attr('name', "excluir_"+$('#hid_lacre_controle').val());
			
			$('#txt_pedido_nfe_volume_lacre:last').val('');
			$('#txt_pedido_nfe_volume_lacre').select();
			
		});

		//#########################################################################################################################################################################################
		$('.a_excluir').click(function(event){
			event.preventDefault();
			var name = $(this).attr('name').split('_');
			controle = $('#hid_lacre_controle').val();
			n = 1;
			x = 1;
			
			while(n <= controle){
				if(name[1] != n){
					$('input[name=txt_pedido_nfe_volume_lacre_item_'+n+']').val(x);
					$('input[name=txt_pedido_nfe_volume_lacre_item_'+n+']').attr('name', 'txt_pedido_nfe_volume_lacre_item_'+x);
					$('input[name=txt_pedido_nfe_volume_lacre_numero_'+n+']').attr('name', 'txt_pedido_nfe_volume_lacre_numero_'+x);
					$('a[name=excluir_'+n+']').attr('name', 'excluir_'+x);
					x ++;
				}
				n ++;
			}
			
			$('#hid_lacre_controle').val(parseInt($('#hid_lacre_controle').val()) - 1);
			$(this).parents(".volume_listar").remove();
		});
		
	</script>