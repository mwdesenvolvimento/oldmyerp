<?php
	
	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	require_once('../inc/fnc_status.php');
	require_once('op_importar_vendas_filtro.php');
	
/**************************** ORDER BY *******************************************/
	
	$filtroOrder = 'tblpedido.fldId ';
	$class = 'desc';
	$order_sessao = explode(" ", $_SESSION['order_op_pedido']);
	if(isset($_POST['order'])){
		switch($_POST['order']){
			case 'codigo'	:  $filtroOrder = "tblpedido.fldId";   			break;
			case 'data'		:  $filtroOrder = "tblpedido.fldPedidoData";	break;
		}
		if($order_sessao[0] == $filtroOrder){$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';}
	}
	
	//definir icone para ordem
	$_SESSION['order_op_pedido'] = $filtroOrder.' '.$class;
/**************************** PAGINAÇÃO *******************************************/
	$sSQL = "SELECT 
				tblpedido.fldPedidoData,
				tblpedido.fldId as fldPedidoId,
				tblpedido.fldStatus,
				tblcliente.fldNome AS fldNomeCliente,
				GROUP_CONCAT(tblpedido_item.fldDescricao SEPARATOR ', ') AS fldProdutos
			FROM tblpedido_parcela
				RIGHT	JOIN tblpedido 		ON tblpedido.fldId 			= tblpedido_parcela.fldPedido_Id 
				INNER	JOIN tblcliente 	ON tblpedido.fldCliente_Id	= tblcliente.fldId
				LEFT	JOIN tblpedido_item ON tblpedido.fldId 			= tblpedido_item.fldPedido_Id
			WHERE tblpedido.fldExcluido = 0
				AND tblpedido.fldStatus = 2
				AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0)
				AND tblpedido.fldTipo_Id != 3
				". $_SESSION['filtro_op_importar']."
				ORDER BY " . $_SESSION['order_op_pedido'];
	
	$rsTotal = mysql_query($sSQL);
	echo mysql_error();
	$rowsTotal = mysql_num_rows($rsTotal);

	//definição dos limites
	$limite = 300;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " LIMIT " . $inicio . "," . $limite;
	$rsPedido = mysql_query($sSQL);
	
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#####################################################################################
?>
    <form class="table_form" id="frm_importar" action="?p=ordem_producao_importar" method="post">
    	<div id="table" style="width: 560px;">
            <div id="table_cabecalho" style="width: 560px;">
                <ul class="table_cabecalho"style="width: 635px;">
                    <li class="order" style="width:85px">
                    	<a <? ($filtroOrder == 'tblpedido.fldId') ? print "class='$class'" : '' ?> style="width:60px" href="codigo">C&oacute;d.</a>
                    </li>
                    <li style="width:210px;"><!--Cliente--><?=$_POST['params'][1]?></a></li>
                    <li style="width:210px;">Produto</li>
                    <li class="order" style="width: 75px;">
                    	<a <? ($filtroOrder == 'tblpedido.fldPedidoData') ? print "class='$class'" : '' ?> style="width:55px" href="data">Emiss&atilde;o</a>
                    </li>
                    <li style="width:0px; text-align:center;"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container" style="width: 635px;">       
                <table id="table_general" class="table_general" summary="Lista de pedidos">
                	<tbody>
<?					
						$id_array = array(); 
						$n = 0;
						
						$linha = "row";
						$rows = mysql_num_rows($rsPedido); 
						while($rowPedido = mysql_fetch_array($rsPedido)){ 
						
							$pedido_id 		= $rowPedido['fldPedidoId']; 
							$id_array[$n] 	= $pedido_id;
							$n += 1;
						
	?>						<tr class="<?= $linha; ?>">
								<td style="width:85px;"><span title="<?= fnc_status_pedido($rowPedido['fldPedidoId'], "tblpedido")?>" class="<?=fnc_status_pedido($rowPedido['fldPedidoId'], "tblpedido")?>"><?=str_pad($rowPedido['fldPedidoId'], 4, "0", STR_PAD_LEFT)?></span></td>
								<td	style="width:210px; text-align:left"><?=$rowPedido['fldNomeCliente']?></td>
								<td	style="width:210px; text-align:left"><?=substr($rowPedido['fldProdutos'], 0, 30);?></td>
								<td style="width:75px; text-align:center"><?=format_date_out($rowPedido['fldPedidoData'])?></td>
								<td style="width:0px;"><input type="checkbox" name="chk_pedido_importar_<?=$rowPedido['fldPedidoId']?>" id="chk_pedido_importar_<?=$rowPedido['fldPedidoId']?>" title="selecionar o registro posicionado" class="checkbox_pedido" /></td>
							</tr>
<?							$linha = ($linha == "row" ) ? "dif-row":"row";
						}
?>			 		</tbody>
				</table>
            </div>
			<input type="hidden" id="txt_ids_importar" name="txt_ids_importar" class="txt_ids_importar" value="">
			<input style="float:right; display:block; margin:10px auto; position: relative;" type="submit" class="btn_enviar" id="btn_importar_vendas" name="btn_importar_vendas" style="margin:0" title="Continuar com a importação" value="Continuar" />
	</form>
	
	<script type="text/javascript">
		
		$(document).ready(function(){
			$('li.order').find('a').click(function(e){
				e.preventDefault();
				$('div.modal-conteudo:last').load('modal/op_importar_vendas.php', {'order' : $(this).attr('href')});
			});
			//checar se alguma venda foi selecionada
			$('#btn_importar_vendas').click(function(){
				var check = $('form#frm_importar tr input:checked').length;
				if(check < 1) {
					alert('Você não selecionou nenhuma venda para ser importada!');
					return false;
				}
			});
			
			$('input[type="checkbox"]').change(function(){
				var id = $(this).attr('id');
				id = id.replace('chk_pedido_importar_', '')+',';
				var valorAtual = $('#txt_ids_importar').val();
				
				if($(this).attr('checked')){
					$('#txt_ids_importar').val(valorAtual+id);
				}
				else{
					var valorSubtraido = valorAtual.replace(id, '');
					$('#txt_ids_importar').val(valorSubtraido);
				}
			});
			
		});
		
	</script>