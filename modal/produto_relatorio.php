	<script type="text/javascript" src="js/produto.js"></script>
<?
	require("../inc/con_db.php");
	require("../inc/fnc_general.php");
	ob_start();
	session_start();
?>
    <form id="frm_relatorio" class="frm_detalhe" method="post" action="produto_relatorio.php" target="_blank">
        <ul>
			<li>
                <label for="sel_produto_tipo_relatorio">Tipo de Relat&oacute;rio:</label>
                <select class="sel_produto_tipo_relatorio" style="width:425px;margin:0" id="sel_produto_tipo_relatorio" name="sel_produto_tipo_relatorio" >
                    <option value="basico">B&aacute;sico</option>
                    <option value="valores">Valores</option>
                    <option value="tabela_preco">Tabela de Pre&ccedil;os</option>
                </select>
			</li>
            <div class="sel_tipo" style="display:none">
                <li>
                    <label for="sel_produto_exibir">Produtos</label>
                    <select class="sel_produto_exibir" style="width:150px;" id="sel_produto_exibir" name="sel_produto_exibir" disabled="disabled" >
                        <option value="todos">Todos</option>
                        <option value="vendidos">Vendidos</option>
                    </select>
                </li>
                <li>
                    <label for="txt_data1">Data in&iacute;cio</label>
                    <input disabled="disabled" class="calendario-mask" style="text-align:center;width:122px" type="text" name="txt_data1" id="txt_data1" value=""/>
                </li>
                <li>
                    <label for="txt_data2">Data t&eacute;rmino</label>
                    <input disabled="disabled" class="calendario-mask" style="text-align:center;width:122px" type="text" name="txt_data2" id="txt_data2" value=""/>
                </li>
                <li>
                    <label for="sel_produto_detalhes">Exibir</label>
                    <select disabled="disabled" class="sel_produto_detalhes" style="width:150px;" id="sel_produto_detalhes" name="sel_produto_detalhes" >
                        <option value="resumido">Resumido</option>
                        <option value="detalhado">Detalhado</option>
                        <option value="cliente">Detalhar por cliente</option>
                    </select>
                </li>
                <li>
                    <label for="sel_cliente_exibir">Cliente</label>
                    <select disabled="disabled" class="sel_cliente_exibir" style="width:265px;" id="sel_cliente_exibir" name="sel_cliente_exibir" >
                        <option value="todos">Todos</option>
    <?					$rsCliente = mysql_query("SELECT * FROM tblcliente ORDER BY fldNome");     
                        while($rowCliente = mysql_fetch_array($rsCliente)){
    ?>               	   	<option value="<?=$rowCliente['fldId']?>"><?=$rowCliente['fldNome']?></option>
    <?					}
    ?>              </select>
                </li>
            </div>
            <div id="display_basico" style="float:left">
            	<fieldset style=" width:420px;margin-left:5px">
            	<li>
                    <input type="checkbox" style="width:20px" name="chk_produto_exibir_valor" id="chk_produto_exibir_valor" checked="checked" />
                    <span> exibir valor</span>
				</li>
            	<li>
                    <input type="checkbox" style="width:20px" name="chk_produto_exibir_fornecedor" id="chk_produto_exibir_fornecedor" checked="checked" />
                    <span> exibir fornecedor</span>
				</li>
            	<li>
                    <input type="checkbox" style="width:20px" name="chk_produto_exibir_unidade" id="chk_produto_exibir_unidade" checked="checked" />
                    <span> exibir unidade medida</span>
				</li>
                </fieldset>
            </div>
			<li>
                <label for="sel_produto_ordem">Ordenar por:</label>
                <select class="sel_produto_ordem" style="width:150px" id="sel_produto_ordem" name="sel_produto_ordem">
                    <option value="">Manter ordem de listagem</option>
                    <option value="fornecedor">Fornecedor</option>
                </select>
			</li>
            <li id="li_tabela_preco" style="display:none;">
                <label for="sel_tabela_preco">Tabela:</label>
                <select id="sel_tabela_preco" style="width:130px" id="sel_tabela_preco" name="sel_tabela_preco">
                	<option value="0" selected="selected">Todas</option>
                <?	$sql = "SELECT * FROM tblproduto_tabela WHERE fldExcluido = 0";
					$sql = mysql_query($sql); 
					while($rsTabela = mysql_fetch_array($sql)){ ?>
                    <option value="<?=$rsTabela['fldId']?>"><?=$rsTabela['fldNome']?></option>
				<?	} ?>
                </select>
			</li>
            <li id="li_fornecedor" style="display:none;">
                <label for="sel_fornecedor">Fornecedor:</label>
                <select id="sel_fornecedor" style="width:130px" id="sel_fornecedor" name="sel_fornecedor">
                	<option value="0" selected="selected">Todas</option>
                <?	$sql = "SELECT * FROM tblfornecedor WHERE fldDisabled = '0' ORDER BY fldNomeFantasia asc";
					$sql = mysql_query($sql) or die(mysql_error()); 
					while($rsTabela = mysql_fetch_array($sql)){ ?>
                    <option value="<?=$rsTabela['fldId']?>"><?=$rsTabela['fldNomeFantasia']?></option>
				<?	} ?>
                </select>
			</li>
            
            <li style="float:right; margin:5px 10px 0 0"><input type="submit" name="btn_enviar" id="btn_enviar" class="btn_enviar" value="enviar" /></li>
        </ul>
    </form>
        
    <script>
    
        $("#sel_produto_tipo_relatorio").change(function(){
			if($(this).val() == 'basico'){
				$("div#display_basico").show();
			}else{
				$("div#display_basico").hide();
			}
			
			
            if($(this).val() == 'valores'){
                $(".sel_tipo").show();
                $("#sel_produto_exibir").attr('disabled', '');
            }else{
                $(".sel_tipo").hide();
            }
			
			if($(this).val() == 'tabela_preco'){
				$('#li_tabela_preco').show();
				$('#li_fornecedor').show();
			}else{
				$('#li_tabela_preco').hide();
				$('#li_fornecedor').hide();
			}
			
        })
    
        $("#sel_produto_exibir").change(function(){
			
            if($(this).val() == 'vendidos'){
                $("#sel_produto_detalhes").attr('disabled', '');
            }else{
                $("#sel_cliente_exibir").find('option[value="todos"]').attr('selected', 'selected');
                $("#sel_produto_detalhes").find('option[value="resumido"]').attr('selected', 'selected');
                $("#sel_cliente_exibir").attr('disabled', 'disabled');
                $("#sel_produto_detalhes").attr('disabled', 'disabled');
            }
        })
        
        $("#sel_produto_detalhes").change(function(){
            if($("#sel_produto_detalhes").val() == 'cliente'){
                $("#sel_cliente_exibir").attr('disabled', '');
            }else{
                $("#sel_cliente_exibir").find('option[value="todos"]').attr('selected', 'selected');
                $("#sel_cliente_exibir").attr('disabled', 'disabled');
            }
        })
    
    </script>