<p style="padding:10px; font-size:13px">Os icones ao lado esquerdo s&atilde;o utilizados no m&oacute;dulo de rota e tem significados diferentes</p>

<ul style="margin-top:10px">
	<li style="padding:10px 20px; width:360px; text-align:justify;"><img src="image/layout/bg_enable.gif" /> significa que o endere&ccedil;o j&aacute; est&aacute; cadastrado.</li>
	<li style="padding:10px 20px; width:360px; text-align:justify;"><img src="image/layout/bg_disable.gif" /> indica que o bairro do endere&ccedil;o est&aacute; registrado, possibilitando o cadastro do endere&ccedil;o.</li>
    <li style="padding:10px 20px; width:360px; text-align:justify;"><img src="image/layout/bg_disable_black.gif" /> indica que o endere&ccedil;o e o bairro n&atilde;o possuem nenhum v&iacute;nculo com o m&oacute;dulo de rota</li>
</ul>