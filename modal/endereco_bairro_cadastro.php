<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<?
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');

	
	$setor 		= 'auto';
	//se for editar um registro
	if(isset($_POST['params'][1])){
		
		$bairroId	= $_POST['params'][1];
		
		$rsBairro	= mysql_query("SELECT tblendereco_bairro.* FROM tblendereco_bairro WHERE fldId = $bairroId");
		$rowBairro 	= mysql_fetch_array($rsBairro);
		
		$bairro_id	= $rowBairro['fldIBGE_Id'];
		$bairro	 	= $rowBairro['fldBairro'];
		$setor	 	= $rowBairro['fldSetor'];
		$ordem 		= $rowBairro['fldOrdem'];
		$rotaId		= $rowBairro['fldRota_Id'];
		
	}
	echo mysql_error();
?>

	<span style="font-size:12px; color:red; margin:20px 0; display: block">Vincule o código IBGE do bairro ou faça um cadastro manual</span>

    <form id="frm_adicional" class="frm_detalhe" style="width: 700px" action="index.php?p=endereco&modo=bairro" method="post">
        <input type="hidden" id="hid_edit" name="hid_edit" value="<?=$bairroId?>" />
        <ul>
            <li style="margin-left:10px">
                <label for="txt_cod_bairro">Código</label>
                <input type="text" style="width:60px" id="txt_cod_bairro" name="txt_cod_bairro" value="<?=$bairro_id?>" />
                <a href="busca_bairro" title="Localizar" class="modal" rel="945-400"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                <a href="busca_cep_pesquisando" class="modal" id="link_pesquisa_cep" rel="200-100"></a>
            </li>
            <li>
                <label for="txt_bairro">Bairro</label>
                <input type="text" style="width:478px;" id="txt_bairro" name="txt_bairro" value="<?=$bairro?>" />
            </li>            
            <li>
                <label for="txt_setor">Setor</label>
                <input type="text" style="width:80px; text-align:center" id="txt_setor" name="txt_setor" onfocus="limpar (this,'auto');" onblur="mostrar (this, 'auto');" value="<?=$setor?>" />
            </li>
            <li style="margin-left:10px">
               	<label for="sel_rota">Rota</label>
                <select style="width:150px;" id="sel_rota" name="sel_rota">
                	<option value=""></option>
<?					$rsRota = mysql_query("SELECT * FROM tblendereco_rota WHERE fldExcluido = 0 ORDER BY fldRota");
					while($rowRota = mysql_fetch_array($rsRota)){
?>						<option <?= ($rotaId == $rowRota['fldId'])? "selected='selected'" : ''?> value="<?=$rowRota['fldId']?>"><?=$rowRota['fldRota']?></option>
<?					}
?>				</select>
            </li>
            <li>
                <label for="txt_ordem">Ordem</label>
                <input type="text" style="width:40px; text-align:right" id="txt_ordem" name="txt_ordem" value="<?=$ordem?>" />
            </li>
            <li>
                <label for="txt_municipio_codigo">C&oacute;d. Munic&iacute;pio</label>
                <input type="text" style=" width:100px" id="txt_municipio_codigo" name="txt_municipio_codigo" value="<?=$rowBairro['fldMunicipio_IBGE_Id']?>" />
                <a href="municipio_busca" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
            </li>
            <li>
                <label for="txt_municipio">Munic&iacute;pio</label>
                <input type="text" style="width:199px;" id="txt_municipio" name="txt_municipio" />
            </li>
            <li style="float:right; margin-right:10px">
                <input type="submit" class="btn_enviar" style="margin-top:12px" name="btn_enviar_bairro" id="btn_enviar_bairro" value="gravar" title="gravar" />
            </li>
         </ul>
    </form>

	<script type="text/javascript"> 
	
		$(document).ready(function(){
			$('#txt_municipio_codigo').focus().blur();
			$('#txt_bairro').focus();
		});
		
		$('#sel_rota').change(function(){
			var id = $(this).attr('value');
			$.post("modal/endereco_cadastro_filtro.php", {sel_bairro:id}, function(theXML){
				
				$('dados',theXML).each(function(){
					var ordem = $(this).find("ordem").text();
					
					$('#txt_ordem').val(ordem);
				});
			});
		});	
		
		$("input#btn_enviar_bairro").click(function(event){
			event.preventDefault();
		
			var setor 	 = $('#txt_setor').val();
			var hid_edit = $('#hid_edit').val();
			$.post("modal/endereco_cadastro_filtro.php", {txt_setor:setor, hid_edit:hid_edit}, function(theXML){
				
				$('dados',theXML).each(function(){
					var retorno = $(this).find("rows").text();
					if(retorno > 0 ){
						alert('Setor já cadastrado!');
						$('#txt_setor').select();
						return false;
					}else{
						$('form#frm_adicional').submit();
					}
				});
			});
		});	
	</script>            