<?php
	
	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');	
	
	if(isset($_POST['form'])){
		
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$status_id		= $form['hid_status_id'];
		$registro_id	= $form['hid_registro_id'];
		$historico_id	= $form['hid_historico_id'];
		$descricao		= $form['txt_descricao'];
		$data			= format_date_in($form['txt_data']);
		$hora			= $form['txt_hora'];
		$usuario_id 	= $_SESSION['usuario_id'];
		
		$data_atual		= date('Y-m-d');
		$hora_atual		= date('H:i:s');
		
		$SQL = "INSERT INTO tblfuncionario_servico_historico_registro 
				(fldData, fldHora, fldCadastro_Data, fldCadastro_Hora, fldDescricao, fldServico_Historico_Id, fldServico_Historico_Status_Id, fldUsuario_Id) 
				VALUES ('$data', '$hora', '$data_atual', '$hora_atual', '$descricao', $historico_id, $status_id, $usuario_id)";
		
		if(mysql_query($SQL)){
			
			$rsRegistro		= mysql_query("SELECT last_insert_id() as lastID");
			$LastId 		= mysql_fetch_array($rsRegistro);
			$last_id		= $LastId['lastID'];
			
			mysql_query("UPDATE tblfuncionario_servico_historico_registro SET fldCorrecao_Id = $last_id WHERE fldId = $registro_id");
?>			
			<img src="image/layout/carregando.gif" alt="carregando..." />
			<script type="text/javascript">
				var historico_id = '<?= $historico_id ?>';
				$('div.modal-conteudo:first').load('modal/funcionario_servico_historico_novo.php', {'historico_id' : historico_id});
				$('.modal-body:last').remove();
			</script> 
<?			die;

		}else{
			echo mysql_error();
			die();
		}
	}
	########################################################################################################################################################################
	$registro_id = $_POST['params'][1];
	
	$sql = "SELECT tblfuncionario_servico_historico_registro.fldData, tblfuncionario_servico_historico_registro.fldHora, tblfuncionario_servico_historico_registro.fldDescricao,
	 		tblfuncionario_servico_historico_registro.fldServico_Historico_Status_Id, tblfuncionario_servico_historico_registro.fldServico_Historico_Id, tblfuncionario_servico_historico_status.fldStatus
			FROM tblfuncionario_servico_historico_registro INNER JOIN tblfuncionario_servico_historico_status
			ON tblfuncionario_servico_historico_registro.fldServico_Historico_Status_Id = tblfuncionario_servico_historico_status.fldId
			WHERE tblfuncionario_servico_historico_registro.fldId = $registro_id";
	$rsRegistro  = mysql_query($sql);
	$rowRegistro = mysql_fetch_array($rsRegistro);
	
	$status_id 	 = $rowRegistro['fldServico_Historico_Status_Id'];
	$historico_id= $rowRegistro['fldServico_Historico_Id'];
	$data		 = $rowRegistro['fldData'];
	$hora		 = $rowRegistro['fldHora'];
	$descricao	 = $rowRegistro['fldDescricao'];
	$status		 = $rowRegistro['fldStatus'];
	########################################################################################################################################################################
	
	$sql = "SELECT fldData, fldHora FROM tblfuncionario_servico_historico_registro 
			WHERE fldServico_Historico_Id = $historico_id AND fldData >= '$data' AND fldHora > '$hora' AND fldCorrecao_Id IS NULL 
			ORDER BY fldId LIMIT 1 ";

	$rsRegistroMax  = mysql_query($sql);
	$rowRegistroMax = mysql_fetch_array($rsRegistroMax);
	echo mysql_error();
	$data_maximo = format_date_out($rowRegistroMax['fldData']);
	$hora_maximo = $rowRegistroMax['fldHora'];

	$sql = "SELECT fldData, fldHora FROM tblfuncionario_servico_historico_registro 
			WHERE fldServico_Historico_Id = $historico_id AND fldData <= '$data' AND fldHora < '$hora' AND fldCorrecao_Id IS NULL 
			ORDER BY fldId LIMIT 1 ";
	$rsRegistroMin  = mysql_query($sql);
	$rowRegistroMin = mysql_fetch_array($rsRegistroMin);
	echo mysql_error();
	$data_minimo = format_date_out($rowRegistroMin['fldData']);
	$hora_minimo = $rowRegistroMin['fldHora'];
	########################################################################################################################################################################
	
?>	
	<form class="frm_detalhe" id="frm_funcionario_historico_servico_correcao" action="" method="post">   
		<a class="modal modal_correcao" href="funcionario_servico_historico_novo,correcao,<?=$historico_id?>" style=" display:none" rel="700-370"></a>
        <input type="hidden" id="hid_status_id"	 	name="hid_status_id"	value="<?=$status_id?>" />
        <input type="hidden" id="hid_historico_id"	name="hid_historico_id"	value="<?=$historico_id?>" />
        <input type="hidden" id="hid_registro_id"	name="hid_registro_id"	value="<?=$registro_id?>" />
        <input type="hidden" id="hid_data_max"		name="hid_data_max"		value="<?=$data_maximo.' '.$hora_maximo?>" />
        <input type="hidden" id="hid_data_min"		name="hid_data_min"		value="<?=$data_minimo.' '.$hora_minimo?>" />
        <fieldset style="width:480px; margin:5px;">
        	<legend>Registro anterior</legend>
        	<ul>
                <li>
                    <input style="width:70px;background:#FFC" type="text" id="txt_data_registo" name="txt_data_registo" value="<?= format_date_out($data)?>" disabled="disabled"/>
                </li>
                <li>
                    <input style="width:50px;text-align:center;background:#FFC" type="text" id="txt_hora_registro" name="txt_hora_registro" value="<?= format_time_short($hora)?>" />
                </li>
                <li>
                    <input style="width:230px" type="text" id="txt_descricao_registro" name="txt_descricao_registro" value="<?=$descricao?>" disabled="disabled" />
                </li>
                <li>
                    <input style="width:70px;text-align:center;background:#FFC" class="servico_historico_<?=$status?>" type="text" id="txt_status_registro" name="txt_status_registro" value="<?=$status?>" disabled="disabled" />
                </li>
            </ul>
        </fieldset>
        <fieldset style="width:480px; margin:5px;margin-top:10px">
        	<legend>Registro corre&ccedil;&atilde;o</legend>
        	<ul>
                <li style="margin-top:20px">
                    <input style="width:70px;font-weight:bold;background:#FFC" type="text" id="txt_data" name="txt_data" value="" class="calendario-mask" />
                </li>
                <li style="margin-top:20px">
                    <input style="width:50px;font-weight:bold;background:#FFC; text-align:center" type="text" id="txt_hora" name="txt_hora" value="" />
                </li>
                <li>
                    <label for="txt_descricao">Descri&ccedil;&atilde;o</label>
                    <input type="text" style="width:270px" id="txt_descricao" name="txt_descricao" value="" />
                </li>
                <li style="margin-top:6px">
					<button style=" width:40px; height:30px; margin-top:10px" name="btn_inserir" id="btn_inserir" title="Inserir" >ok</button>
                </li>
            </ul>
        </fieldset>
	</form>
	
    <script type="text/javascript">
		$('#txt_data').focus();
		$('#frm_funcionario_historico_servico_correcao').submit(function(e){
			e.preventDefault();
			
			var data 	= $('#txt_data').val();
			var hora 	= $('#txt_hora').val();
			var dataMax	= $('#hid_data_max').val().split(' ');
			var dataMin	= $('#hid_data_min').val().split(' ');
			
			if(comparaData(data,dataMax[0],hora,dataMax[1]) == 'maior' || comparaData(data,dataMin[0],hora,dataMin[1]) == 'menor') {
				alert('Horário não deve ultrapassar outros registros.');
			}else if(comparaData(data,dataMax[0],hora,dataMax[1]) == 'igual' || comparaData(data,dataMin[0],hora,dataMin[1]) == 'igual'){
				alert('Já existe um registro para este horário.');
			}else if(data!='' && hora!=''){
				var form 	= $(this).serialize();
				$('div.modal-conteudo:last').load('modal/funcionario_servico_historico_correcao.php', {form : form});
			}else{
				alert('Inserir data e hora para novo registro!');
			}
		});
		
	</script>
