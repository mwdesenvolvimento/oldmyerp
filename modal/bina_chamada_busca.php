<?
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	require_once('../inc/fnc_ibge.php');
	require_once('../inc/fnc_status.php');
	
	#BUSCAR REGISTRO DO CLIENTE QUE JA FOI FEITO BUSCA
	if($_POST['table_action'] == 'telefone'){
		
		$cliente_id 	= $_POST['cliente_id'];	
		$sSQL 			= mysql_query("SELECT * FROM tblcliente WHERE fldId = $cliente_id");
		$row 			= mysql_fetch_array($sSQL);
		
		$cliente_id		= $row['fldId'];
		$Codigo 		= $row['fldCodigo'];
		$Nome 			= mysql_escape_string($row['fldNome']);
		$NomeFantasia 	= mysql_escape_string($row['fldNomeFantasia']);
		$RG 			= $row['fldRG_IE'];
		$Fone1 			= $row['fldTelefone1'];
		$Fone2 			= $row['fldTelefone2'];
		$Tipo 			= $row['fldTipo'];
		$Endereco 		= mysql_escape_string($row['fldEndereco']);
		$Numero 		= $row['fldNumero'];
		$Complemento 	= $row['fldComplemento'];
		$Bairro 		= mysql_escape_string($row['fldBairro']);
		$Cep 			= $row['fldCEP'];
		$Endereco_Cod 	= $row['fldEndereco_Id'];
		$Municipio_Cod 	= $row['fldMunicipio_Codigo'];
		$Genero 		= $row['fldGenero'];
		$Cadastro 		= format_date_out($row['fldCadastroData']);
		$CPF_CNPJ 		= formatCPFCNPJTipo_in($row['fldCPF_CNPJ'], $Tipo);
		$Municipio		= fnc_ibge_municipio($Municipio_Cod);
		$UF				= fnc_ibge_uf_sigla($Municipio_Cod);
		
		$xml 		= "\t\t<Codigo>$Codigo</Codigo>\n";
		$xml 		.= "\t\t<cliente_id>$cliente_id</cliente_id>\n";
		$xml 		.= "\t\t<Nome>$Nome</Nome>\n";
		$xml 		.= "\t\t<NomeFantasia>$NomeFantasia</NomeFantasia>\n";
		$xml 		.= "\t\t<RG>$RG</RG>\n";
		$xml 		.= "\t\t<Fone1>$Fone1</Fone1>\n";
		$xml 		.= "\t\t<Fone2>$Fone2</Fone2>\n";
		$xml 		.= "\t\t<Endereco>$Endereco</Endereco>\n";
		$xml 		.= "\t\t<Endereco_Cod>$Endereco_Cod</Endereco_Cod>\n";
		$xml 		.= "\t\t<Numero>$Numero</Numero>\n";
		$xml 		.= "\t\t<Complemento>$Complemento</Complemento>\n";
		$xml 		.= "\t\t<Bairro>$Bairro</Bairro>\n";
		$xml 		.= "\t\t<Cep>$Cep</Cep>\n";
		$xml 		.= "\t\t<Municipio_Cod>$Municipio_Cod</Municipio_Cod>\n";
		$xml 		.= "\t\t<Tipo>$Tipo</Tipo>\n";
		$xml 		.= "\t\t<Genero>$Genero</Genero>\n";
		$xml 		.= "\t\t<Cadastro>$Cadastro</Cadastro>\n";
		$xml 		.= "\t\t<CPF_CNPJ>$CPF_CNPJ</CPF_CNPJ>\n";
		$xml 		.= "\t\t<Municipio>$Municipio</Municipio>\n";
		$xml 		.= "\t\t<UF>$UF</UF>\n";

		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";
	}
	#BUSCAR CLIENTE 
	elseif($_GET['table_action'] == 'cliente'){
		#A PRIMEIRA, CASO SEJA UM CADASTRO NOVO, E HAJA O MESMO TELEFONE CADASTRADO PRA OUTRO CLIENTE (VAI SABER!), ELE PEGA PRIMEIRO ESSE QUE ACABOU DE SER CADASTRADO, PRA DEPOIS LISTAR O RESTO
		#AS DUAS ULTIMAS CONSULTAS PARA QUE, A PRIMEIRA TR�S OS TELEFONES QUE TERMINAM COM O TELEFONE DA CHAMADA, A SEGUNDA CONSULTA, TRAS QUALQUER PARTE QUE CONTENHA OS NUMEROS DA CHAMADA
		#ALGO COMO  38436359 OU 38635995, SE O CLIENTE BUSCAR APENAS POR 6359, VAI TRAZER PRIMEIRO O NUMERO QUE TERMINA COM, E O OUTRO SE TEM 6359 EM QUALQUER PARTE
		
		$telefone 	= str_replace('-', "", $_GET['telefone']);
		
		if(isset($_GET['cliente_id'])){
			$cliente_id = $_GET['cliente_id'];
			$filtro 	= "(SELECT * FROM tblcliente WHERE fldId = $cliente_id) UNION";
		}
		$sSQL 		= mysql_query("$filtro 
								  	(SELECT * FROM tblcliente WHERE replace(fldTelefone1, '-', '') LIKE '%$telefone' OR replace(fldTelefone2, '-', '') LIKE '%$telefone' AND fldId > 0)
									  UNION
									  ( SELECT * FROM tblcliente WHERE (replace(fldTelefone1, '-', '') LIKE '%$telefone%' OR replace(fldTelefone2, '-', '') LIKE '%$telefone%' AND fldId > 0))");
		
		$linha = "row";
		while($row = mysql_fetch_array($sSQL)){
?>			<tr class="<?=$linha;?>" id="<?=$row['fldId']?>" style="cursor:pointer" >
                <td style='width:55px; text-align:right'><?=$row['fldCodigo']?></td>
                <td style='width:150px'><?=substr($row['fldNome'],0,19)?></td>
                <td style='width:150px'><?=substr($row['fldNomeFantasia'],0,19)?></td>
                <td style='width:105px'><?=$row['fldTelefone1']?></td>
                <td style='width:105px'><?=$row['fldTelefone2']?></td>
            </tr>
<?			$linha	 = ($linha == "row") ? "dif-row" : "row";
			$ids 	.= $row['fldId'].", ";
 		}
	}
	#LISTAR VENDAS DO CLIETNE SELECIONADO
	elseif($_GET['table_action'] == 'venda_listar'){

		$cliente_id = $_GET['cliente_id'];
		$sSQL = mysql_query("SELECT SUM(tblpedido_parcela.fldValor * (tblpedido_parcela.fldExcluido * -1 + 1)) as fldTotal,
									tblpedido.fldPedidoData, tblpedido.fldId
									FROM tblpedido
									LEFT JOIN tblpedido_parcela ON tblpedido.fldId 	= tblpedido_parcela.fldPedido_Id
									WHERE tblpedido.fldExcluido = 0
									AND tblpedido.fldCliente_Id = $cliente_id
									GROUP BY tblpedido_parcela.fldPedido_Id ORDER BY fldCadastroData DESC LIMIT 10
								");
		
		$linha = "row";
		echo mysql_error();
		while($row = mysql_fetch_array($sSQL)){
			
			$descricaoVenda = '';
			$rsItem = mysql_query("SELECT * FROM tblpedido_item WHERE fldPedido_Id = ".$row['fldId']);
			while($rowItem = mysql_fetch_array($rsItem)){
				$descricaoVenda .= $rowItem['fldDescricao'].", ";
			}
			$descricaoVenda = substr($descricaoVenda, 0 ,strlen($descricaoVenda) - 2);
?>			<tr class="<?=$linha;?>" id="<?=$row['fldId']?>">
				<td class="cod" style="width:60px; text-align:right"><?=$row['fldId']?></td>
				<td style="width:470px;padding-left:5px"><?=$descricaoVenda?></td>
				<td style="width:80px; text-align:center"><?= format_date_out($row['fldPedidoData'])?></td>
				<td class="credito" style="width:60px; text-align:right"><?= format_number_out($row['fldTotal'])?></td>
				<td style="width:20px"><a class="btn_duplicar" href="index.php?p=pedido&modo=rapido_novo&amp;duplica=<?=$row['fldId']?>" title="copiar venda"></a></td>
            </tr>
<?			$linha	 = ($linha == "row" ?	$linha = "dif-row": $linha = "row");
 		}
	}
	#LISTAR PARCELAS EM ABERTO DO CLIENTE SELECIONADO
	elseif($_GET['table_action'] == 'parcela_listar'){

		$cliente_id = $_GET['cliente_id'];
		$sSQL = mysql_query("SELECT tblpedido_parcela.fldVencimento, tblpedido_parcela.fldValor, tblpedido_parcela.fldObservacao, tblpedido_parcela.fldParcela, tblpedido_parcela.fldId,
				SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa
				FROM 
				(tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id)
				INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
				WHERE tblpedido.fldCliente_Id = $cliente_id
				AND tblpedido_parcela.fldStatus = '1' 
				AND tblpedido_parcela.fldExcluido = '0'");
		
		$linha = "row";
		echo mysql_error();
		while($row = mysql_fetch_array($sSQL)){
			if($row['fldId']){
				$rowBaixa = mysql_fetch_array(mysql_query("SELECT fldDataRecebido, fldDevedor FROM tblpedido_parcela_baixa
														  WHERE tblpedido_parcela_baixa.fldExcluido = 0 AND fldParcela_Id=".$row['fldId']."
														  ORDER BY fldId DESC LIMIT 1"));
				echo mysql_error();
					
?>				<tr class="<?=$linha;?>" id="<?=$row['fldId']?>">
					<td style="width:100px; text-align:center" class="cod">
						<span title="<?= fnc_status_parcela($row['fldId'], 'tblpedido_parcela')?>" class="parcela_<?= fnc_status_parcela($row['fldId'], 'tblpedido_parcela')?>">
							<?= format_date_out($row['fldVencimento'])?>
						</span>
					</td>
					<td style="width:40px; text-align:center"><?=$row['fldParcela']?></td>
					<td style="width:100px; text-align:right"><?= format_number_out($row['fldValor'])?></td>
					<td style="width:100px; text-align:right"><?= format_number_out($row['fldValorBaixa'])?></td>
					<td style="width:80px; text-align:right"><?= format_date_out($rowBaixa['fldDataRecebido'])?></td>
					<td style="width:260px"><?=$row['fldObservacao']?></td>
				</tr>
<?				$linha	 = ($linha == "row" ?	$linha = "dif-row": $linha = "row");
			}
		}
	}

?>