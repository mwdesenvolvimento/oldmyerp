<script src="js/estoque.js"  charset="UTF-8"></script>

<?
	ob_start();
	session_start();
	
    require('../inc/con_db.php');
    require('../inc/fnc_general.php');
    require('../inc/fnc_estoque.php');
	
	$produto_id		= $_POST['params'][1];
	$saldo			= $_POST['params'][2];
	$estoque_id		= $_SESSION['sel_produto_estoque_id'];
    $rsEstoque 		= mysql_query("SELECT * FROM tblproduto_estoque WHERE fldId <> {$estoque_id}"); //pega os estoques.
	
	
/**************************** TRANSFERIR ESTOQUE **********************************/

	if(isset($_POST["form"])){
		
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
			
		$produto_id		  		= $form['hid_produto_id']; 
		$estoque_destino	  	= $form['sel_estoque']; 
		$validade	  			= (empty($form['sel_validade']) ? null : $form['sel_validade']);
		$quantidade				= format_number_in($form['txt_quantidade']); 

		$estoque_atual_id		= $_SESSION['sel_produto_estoque_id'];
		$usuario_logado_id		= $_SESSION['usuario_id'];
		
		$estoque_destino_dados	=	fnc_dados_estoque($estoque_destino); 
		$descricao				=	"Transferido para {$estoque_destino_dados['fldNome']}";
		
		//Primeira inserção do SQL, essa inserção é no estoque atual do produto, para determinar a saída, e informar que o produto foi transferido de estoque.
		fnc_estoque_movimento_lancar($produto_id, $descricao, $quantidade, $quantidade, '8', '', $estoque_atual_id, $estoque_destino, $validade);
		echo mysql_error();
	
?>
        <img src="image/layout/carregando.gif" alt="carregando..." />
        <script type="text/javascript">
			produto_id = '<?= $produto_id?>';
			window.location="index.php?p=produto_detalhe&id="+produto_id+"&modo=estoque";
        </script> 
<?		die;
	}
	
?>

<form action="" method="post" id="frm_estoque_transferir" class="frm_detalhe">
	<input type="hidden" value="<?=$produto_id?>" name="hid_produto_id" />
	<ul>
        <li>
        <label for="txt_quantidade">Quantidade</label>
            <input type="text" name="txt_quantidade" id="txt_quantidade" style="width: 80px;">
        <li>
        <label for="sel_estoque">Mover para estoque</label>
            <select name="sel_estoque">
<?				while($rowEstoque = mysql_fetch_array($rsEstoque)){
?>					<option value="<?=$rowEstoque['fldId'];?>"><?=$rowEstoque['fldNome'];?></option>
<?          	}
?>      	</select>
	    </li>
        <li>
            <label for="sel_validade">Validade</label>
            <select style="width:95px" id="sel_validade" name="sel_validade" >
            	<option class="<?=$saldo?>" value=""></option>
<?				$rsValidade = mysql_query("SELECT SUM(fldEntrada - fldSaida) as fldRestante, fldValidade_Data, fldProduto_Id from tblproduto_estoque_movimento
											WHERE fldProduto_Id = $produto_id AND fldEstoque_Id = $estoque_id AND fldValidade_Data IS NOT NULL GROUP BY fldProduto_Id, fldValidade_Data HAVING fldRestante > 0 ");
				while($rowValidade = mysql_fetch_array($rsValidade)){
?>            		<option class="<?=$rowValidade['fldRestante']?>" value="<?=$rowValidade['fldValidade_Data']?>" ><?= format_date_out($rowValidade['fldValidade_Data'])?></option>
<?				}
?>			
            </select>
		</li>
		<li><input type="submit" class="btn_enviar" name="btn_transferir" id="btn_transferir" value="Gravar" title="Gravar" /></li>
	</ul>
	
</form>


<script type="text/javascript">
	
	$('#txt_quantidade').select();
	
	$('#btn_transferir').click(function(event){
		event.preventDefault();
			
		quantidade 	= br2float($('#txt_quantidade').val());
		saldo		= $('#sel_validade').find('option').filter(':selected').attr('class');

		if(quantidade > saldo){
			alert('Quantidade insulficiente para validade selecionada.');
			$('#txt_quantidade').select();
		}else{
			if($('#txt_quantidade').val() == ""){
				alert("O campo de quantidade é obrigatório!");
				return false;
				$('#txt_quantidade').select();
				
			}else{				
				var form 	= $('#frm_estoque_transferir').serialize();
				$('div.modal-conteudo:last').load('modal/produto_estoque_transferir.php', {form : form});
			}
		}
		
	});	
	
</script>
