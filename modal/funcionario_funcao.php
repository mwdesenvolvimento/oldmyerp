<? 
	require("../inc/con_db.php"); 
	require("../inc/fnc_general.php"); 
	
	if(isset($_POST['form'])){
		
		$serialize 	= $_POST['form'];
		parse_str($serialize, $form);
		
		$funcao_id	= $form['hid_id'];
		$funcao		= $form['txt_funcao'];
		$comissao	= format_number_in($form['txt_comissao']);
		$tipo		= $form['sel_tipo'];
		$data 		= date('Y-m-d');
		if($funcao_id > 0){
			$sql = "UPDATE tblfuncionario_funcao SET fldFuncao = '$funcao', fldComissao = '$comissao', fldTipo = '$tipo' 
					WHERE fldId = $funcao_id";
		}else{
			$data 	= date('Y-m-d');
			$sql 	= "INSERT INTO tblfuncionario_funcao (fldFuncao, fldComissao, fldTipo, fldDataCadastro) VALUES ('$funcao', '$comissao', '$tipo', '$data')";
		}
		if(!mysql_query($sql)){ echo mysql_error(); die;};
?>
		<img src="image/layout/carregando.gif" align="carregando..." />
        <script type="text/javascript">
			window.location="index.php?p=funcionario&modo=funcao";
		</script> 
<?		die;		
	}
	
	$funcao_id 		= $_POST['params'][1];
	$codigo			= 'novo';
	if(isset($funcao_id)){
		$rsFuncao	= mysql_query("SELECT * FROM tblfuncionario_funcao WHERE fldId = $funcao_id");
		$rowFuncao 	= mysql_fetch_array($rsFuncao);
		$codigo		= str_pad($rowFuncao['fldId'], 4, "0", STR_PAD_LEFT);
	}
?>
    <form class="frm_detalhe" id="frm_funcao" name="frm_funcao" style="width:500px">
        <ul>
            <li>
                <label for="txt_cod">C&oacute;d.</label>
                <input type="text" style="width:100px" id="txt_cod" name="txt_cod" value="<?=$codigo?>" readonly="readonly" />
                <input type="hidden" id="hid_id" name="hid_id" value="<?=$funcao_id?>" />
            </li>
            <li>
                <label for="txt_funcao">Fun&ccedil;&atilde;o</label>
                <input type="text" style="width:360px" id="txt_funcao" name="txt_funcao" value="<?=$rowFuncao['fldFuncao']?>" />
            </li>
            <li>
                <label for="txt_comissao">Comiss&atilde;o (%)</label>
                <input type="text" style="width:100px" id="txt_comissao" name="txt_comissao" value="<?=format_number_out($rowFuncao['fldComissao'])?>" />
            </li>
            <li>
                <label for="sel_tipo">Tipo</label>
                <select style="width: 120px" id="sel_tipo" name="sel_tipo">
                    <option value="1" <?= ($rowFuncao['fldTipo'] == '1') ? "selected='selected'" : '' ?>>venda</option>
                    <option value="2" <?= ($rowFuncao['fldTipo'] == '2') ? "selected='selected'" : '' ?>>servi&ccedil;o</option>
                </select>
            </li>
            <li style="margin-left:125px"><input type="submit" class="btn_enviar" style="margin-top:16px" name="btn_enviar" id="btn_enviar" value="salvar" title="Salvar" /></li>
        </ul>
    </form>
    
    <script type="text/javascript">
		$('form#frm_funcao input#txt_funcao').focus();

		$('#txt_comissao').change(function(){
			$(this).val(float2br(ifNumberNull(br2float($(this).val())).toFixed(2)));
		});

		$('#frm_funcao').submit(function(e){
			e.preventDefault();
			var form 	= $(this).serialize();
			$('div.modal-conteudo').load('modal/funcionario_funcao.php', {form : form});
		});
		
    </script>
            