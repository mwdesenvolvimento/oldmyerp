<div id="principal">
<?	
	ob_start();
	session_start();
	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	require_once('../inc/fnc_financeiro.php');
	
	###################################################################################################################################################################################################
	if(isset($_POST["form"])){
		
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$funcionario_id = $form['hid_funcionario_id'];
		$movimento_tipo = $form['sel_movimento_tipo'];
		$valor			= format_number_in($form['txt_valor']);
		$pagamento_tipo	= $form['sel_pagamento_tipo'];
		$conta_id		= $form['sel_conta_id'];
		
		//GRAVA NA tblfuncionario_conta_fluxo
		$last_id = fnc_funcionario_conta_fluxo_lancar($funcionario_id, $movimento_tipo, $valor);
		
		if(isset($last_id)){
			//GRAVA NA TABELA DO CAIXA tblfinanceiro_conta_fluxo
			$fluxo_caixa_id = fnc_financeiro_conta_fluxo_lancar('', '', $valor, '', $pagamento_tipo, $last_id, $movimento_tipo, '', $conta_id);
		}
		
		//CHEQUES
		if(!empty($_SESSION['ref_timestamp'])){ //CASO A SESSAO ESTIVER EM BRANCO, PRA NAO ATUALIZAR TODOS OS CHEQUES 
			/*
			$destino_movimento_id = $movimento_tipo;
			$destino_id	= $fluxo_caixa_id;
			$timestamp 	= $_SESSION['ref_timestamp'];
			fnc_cheque_update('', $destino_id, '', '', $destino_movimento_id, $timestamp);
			*/
			##############################################################################3
			$timestamp	 = $_SESSION['ref_timestamp'];
			$movimento_id= $movimento_tipo;
			$registro_id = $fluxo_caixa_id;
			$conta_id	 = $conta_id;
			$saida		 = '1';
			fnc_cheque_movimento_lancar($timestamp, $movimento_id, $registro_id, $conta_id, $saida);
		}
		unset($_SESSION['ref_timestamp']);
		
?>
        <div class="alert_confirm" style="margin-2op:10px; padding:0">
            <p style="margin: 8px">Deseja imprimir recibo?</p>
            <ul style="margin-top:20px">
                <li style="margin-right:15px;"><a href="funcionario_detalhe_conta_fluxo_recibo.php?id=<?=$funcionario_id?>&filtro=<?=$last_id?>" target="_blank">Imprimir</a></li>
                <li style="margin-right:15px;"><a href="index.php?p=funcionario_detalhe&id=<?=$funcionario_id?>&modo=conta_fluxo&modo=conta_fluxo">Finalizar</a></li>
            </ul>
        </div>

<?		die;
	}
	###################################################################################################################################################################################################	
	
	$remote_ip 	= gethostbyname($REMOTE_ADDR);
	$_SESSION['ref_timestamp'] = $remote_ip.date("YmdHis");
	$funcionario_id = $_POST['params'][1];
?>	
	<div class="form" style="width:620px">
        <form class="frm_detalhe" id="frm_funcionario_fluxo_novo" action="" method="post">
        	<input type="hidden" name="hid_funcionario_id" id="hid_funcionario_id" value="<?=$funcionario_id?>" />
            <ul>
                <li>
					<label for="sel_movimento_tipo">Movimenta&ccedil;&atilde;o</label>
					<select style="width:230px" id="sel_movimento_tipo" name="sel_movimento_tipo">
<?						$rsTipo  = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_tipo WHERE fldReferencia_Id = '1'"); //referencia 1 = funcionario
						while($rowTipo = mysql_fetch_array($rsTipo)){
?>							<option title="<?= $rowTipo['fldTipo'] ?>" value="<?=$rowTipo['fldId'] ?>"><?=$rowTipo['fldTipo'] ?></option>
<? 						}
?>					</select>
				</li>
                <li>
                	<label for="sel_pagamento_tipo">Forma pag.</label>
					<select style="width:135px" id="sel_pagamento_tipo" name="sel_pagamento_tipo" >
<?						$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo");
                        while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>							<option value="<?=$rowPagamento['fldId']?>"><?=$rowPagamento['fldTipo']?></option>
<?						}
?> 					</select>
            	</li>
                <li>
                	<label for="sel_conta_id">Debitar em</label>
					<select style="width:130px" id="sel_conta_id" name="sel_conta_id" >
<?						$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta");
                        while($rowConta = mysql_fetch_array($rsConta)){
?>							<option value="<?=$rowConta['fldId'] ?>"><?= $rowConta['fldNome']?></option>
<?						}
?> 					</select>
            	</li>
                <li>
                    <label for="txt_valor">Valor</label>
                    <input type="text" style="width:80px; text-align:right" id="txt_valor" name="txt_valor" value="0,00" />
                </li>
                <li>
                	<a style="margin:0" class="modal btn_cheque" name="btn_cheque" id="btn_cheque" title="cheques" href="financeiro_cheque_listar,7" rel="780-410">inserir cheques</a>
                </li>
                <li style="float:right; margin-right:10px">
                    <input type="submit" style="margin:0" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="gravar" title="Gravar" />
                </li>
             </ul>
        </form>
	</div>
    
	<script type="text/javascript">
	
		$('#sel_tipo_movimento').focus();
				 
        $('#txt_valor').blur(function(){
			$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		});
		
        $('#btn_gravar').click(function(event){
            event.preventDefault();
			
			valor = br2float($('#txt_valor').val());
			if(valor > 0){
				$('#btn_gravar').attr('disabled', 'disabled');
				var form 	= $('#frm_funcionario_fluxo_novo').serialize();
				$('div.modal-conteudo:last').load('modal/funcionario_conta_fluxo_novo.php', {form : form});
			}else{
				alert("Valor inválido");
				$('#txt_valor').focus();
			}
        });	
        
    </script>