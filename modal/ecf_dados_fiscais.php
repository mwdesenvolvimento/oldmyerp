 
<?	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	if(isset($_POST["form"])){
		echo '<div style="height:230px">';
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$pedido_id = $form["hid_pedido_id"];
		$limite = $form['hid_limite'];
		for($x=1;$x <=$limite;$x++){
			$produto_nome		= $form["hid_produto_nome_{$x}"];
			$produto_id 		= $form["hid_produto_id_{$x}"];
			$produto_ncm 		= $form["txt_ncm_{$x}"];
			$produto_aliquota 	= $form["sel_aliquota_{$x}"];
			$produto_origem		= $form["sel_origem_{$x}"];
			
			$update_aliquota = "UPDATE tblproduto SET fldEcf_Aliquota_Id = '{$produto_aliquota}' WHERE fldId = {$produto_id}";
			$update_fiscal 	 = "UPDATE tblproduto_fiscal SET fldncm = '{$produto_ncm}', fldicms_orig = '{$produto_origem}' WHERE fldProduto_Id = {$produto_id}";
			
			if(mysql_query($update_aliquota) && mysql_query($update_fiscal)){
				echo "<small style='margin: 10px 50px;color:#00F'>Produto ".substr($produto_nome,0,50)." atualizado!</small>";
			}else{
				$erro = true;
				echo "<small style='margin: 10px 50px;color:#F00'>Erro ao atualizar ",substr($produto_nome,0,50).".</small>";
			}
			echo mysql_error();
		}
		echo '</div>';
		if(!$erro)
        echo '<a id="btn_imprimir" class="btn_enviar" style="float:right;margin-right:15px" title="imprimir" href="" >imprimir cupom</a>';
	}else{
	
	$pedido_id 	= $_POST['params'][1];
	$rsProduto = mysql_query("SELECT tblproduto.fldEcf_Aliquota_Id,tblproduto.fldCodigo,tblproduto.fldId, tblproduto.fldNome, tblproduto_fiscal.fldicms_orig, tblproduto_fiscal.fldncm FROM tblproduto 
								INNER JOIN tblpedido_item ON tblpedido_item.fldProduto_Id = tblproduto.fldId
								LEFT JOIN tblproduto_fiscal ON tblpedido_item.fldProduto_Id = tblproduto_fiscal.fldProduto_Id
								WHERE tblpedido_item.fldPedido_Id = {$pedido_id}");
	echo mysql_error();
		
?>

	<form class="table_form" id="frm_ecf_dados_fiscais" action="" method="post">
    	<div id="table" style="width:720px">
            <div id="table_cabecalho" style="width:720px">
                <ul class="table_cabecalho" style="width:720px">
                    <li style="width:50px; text-align:right;margin-right:10px">Código</li>
                    <li style="width:320px; text-align:left">Produto</li>
                    <li style="width:80px; text-align:center">NCM</li>
                    <li style="width:120px; text-align:center">Alíquota</li>
                    <li style="width:100px; text-align:center">Origem</li>
                </ul>
            </div>
            <div id="table_container" style="width:720px;height:200px">       
                <table id="table_general" class="table_general" summary="Listar produtos">
                	<tbody>
<?
						$n = 0;
						$linha = "row";
						while($rowProduto = mysql_fetch_array($rsProduto)){
							$produto_ncm 		= $rowProduto['fldncm'];
							$produto_aliquota 	= $rowProduto['fldEcf_Aliquota_Id'];
							$produto_origem 	= $rowProduto['fldicms_orig'];
							if(($produto_aliquota =='0' || is_null($produto_aliquota)) ||($produto_origem =='0' || is_null($produto_origem)) || ($produto_ncm == '' || is_null($produto_ncm))){
								$n ++;
?>								<tr class="<?= $linha; ?>">
									<input type="hidden" name="hid_produto_id_<?=$n?>" value="<?=$rowProduto["fldId"]?>" />
									<input type="hidden" name="hid_produto_nome_<?=$n?>" value="<?=$rowProduto["fldNome"]?>" />
                                    <td style="width:50px;text-align:right;padding-right:10px"><?=str_pad($rowProduto['fldCodigo'], 6, "0", STR_PAD_LEFT)?></td>
                                    <td	style="width:320px"><?=substr($rowProduto['fldNome'],0,50)?></td>
                                    <td style="width:80px"><input type="text" style="width:80px;text-align:right"  id="txt_ncm_<?=$n?>" name="txt_ncm_<?=$n?>" value="<?=$produto_ncm?>" /></td>
                                    <td style="width:120px">
                                        <select style="width:120px;text-align:right" name="sel_aliquota_<?=$n?>" id="sel_aliquota_<?=$n?>">
                                            <option value="">selecionar</option>
                                            <? $rsECF_aliquota = mysql_query("SELECT * FROM tblecf_aliquota WHERE fldDisabled = 0");
												while($rowAliquota = mysql_fetch_array($rsECF_aliquota)){ ?>
													<option <?=($produto_aliquota == $rowAliquota['fldId']) ? 'selected="selected"':''?> value="<?=$rowAliquota['fldId']?>"><?=$rowAliquota['fldDescricao']?></option>
                                            <? }?>
                                        </select>
                                    </td>
                                    </td><td style="width:100px">
                                        <select style="width:100px;text-align:right" name="sel_origem_<?=$n?>" id="sel_origem_<?=$n?>">
                                            <option value="">selecionar</option>
                                            <? $rsECF_origem 	= mysql_query("SELECT * FROM tblnfe_icms_campo_orig ORDER BY fldId");
												while($rowOrigem = mysql_fetch_array($rsECF_origem)){ ?>
													<option <?=($produto_origem ==  $rowOrigem['fldId']) ? 'selected="selected"':''?> value="<?=$rowOrigem['fldId']?>"><?=substr($rowOrigem['fldDescricao'],0,70)?></option>
                                            <? }?>
                                        </select>
                                    </td>
                                </tr>
<?	                      		$linha = ($linha == "row") ? "dif-row"  : "row";
                       		}
						}
?>		 			</tbody>
				</table>
            </div>
            <input type="hidden" name="hid_limite" value="<?=$n?>" />
            <input type="hidden" name="hid_pedido_id" value="<?=$pedido_id?>" />
        	<input type="submit" style="margin-top:15px; float:right" id="btn_gravar_dados" name="btn_gravar_dados" class="btn_gravar" value="gravar" />
        </div>
    </form>
            
            
<? }
?>            
           
	<script type="text/javascript">
		
		$('#frm_ecf_dados_fiscais').submit(function(e){
			e.preventDefault();

			var limite = '<?=$n?>';
			for(n=1;n<=limite;n++){
				var produto_ncm 	 = $("#txt_ncm_"+n).val();
				var produto_aliquota = $("#sel_aliquota_"+n).val();
				var produto_origem 	 = $("#sel_origem_"+n).val();
					
				if((produto_ncm == ''||produto_ncm.length < 6) || produto_aliquota == ''||produto_origem == ''){
					alert('Todos os campos devem ser preenchidos.');
					return false;
				}			
			}
			
			var form = $(this).serialize();
			$('div.modal-conteudo:last').load('modal/ecf_dados_fiscais.php', {form : form});
		});	
		
		/******************************************************************************/
		
		$('#btn_imprimir').click(function(event){
			event.preventDefault();
			  console.log(this);
			var pedido_id = '<?=$pedido_id?>';
			$('div.modal-body:last').prev('div.modal-body').find('div.modal-conteudo').load('modal/ecf_documento.php', {pedido_id : pedido_id});
			$('div.modal-body:last').remove();
			
		});
			
    </script>