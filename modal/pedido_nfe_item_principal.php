
    <ul>
        <li>
            <label for="txt_nfe_item_principal_codigo">C&oacute;digo</label>
            <input type="text" id="txt_nfe_item_principal_codigo" name="txt_nfe_item_principal_codigo" style="width: 100px;" value="" readonly="readonly" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_descricao">Descri&ccedil;&atilde;o</label>
            <input type="text" id="txt_nfe_item_principal_descricao" name="txt_nfe_item_principal_descricao" style="width: 440px;" value="" <?=($nfe_importar > 0) ? 'readonly="readonly"' : '' ?> />
        </li>
        <li>
            <label for="txt_nfe_item_principal_ncm">NCM</label>
            <input type="text" id="txt_nfe_item_principal_ncm" name="txt_nfe_item_principal_ncm" style="width: 176px;" value="<?=$ncm?>" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_ex_tipi">EX TIPI</label>
            <input type="text" id="txt_nfe_item_principal_ex_tipi" name="txt_nfe_item_principal_ex_tipi" style="width: 176px;" value="<?=$ex_tipi?>" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_cfop">CFOP</label>
			<input type="text" class="enter" style="width:80px; text-align:right" id="txt_nfe_item_principal_cfop" name="txt_nfe_item_principal_cfop" value="<?=$cfop?>" />
			<a title="Localizar" class="modal" href="cfop_busca" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
        </li>
        <li>
            <label for="txt_nfe_item_principal_unidade_comercial">Unidade Comercial</label>
            <input type="text" id="txt_nfe_item_principal_unidade_comercial" name="txt_nfe_item_principal_unidade_comercial" style="width:120px;text-align:right" maxlength="6" value="<?=substr($unidade_comercial,0,6)?>" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_valor_unitario_comercial">Valor Unit. Comercial</label>
            <input type="text" id="txt_nfe_item_principal_valor_unitario_comercial" name="txt_nfe_item_principal_valor_unitario_comercial" style="width: 200px;text-align:right" value="" <?=($nfe_importar > 0) ? 'readonly="readonly"' : '' ?> />
        </li>
        <li>
            <label for="txt_nfe_item_principal_desconto_percent">Desconto (%)</label>
            <input type="text" id="txt_nfe_item_principal_desconto_percent" name="txt_nfe_item_principal_desconto_percent" style="width: 80px;text-align:right" value="" <?=($nfe_importar > 0) ? 'readonly="readonly"' : '' ?> />
        </li>
        
        <li>
            <label for="txt_nfe_item_principal_quantidade_comercial">Quantidade Comercial</label>
            <input type="text" id="txt_nfe_item_principal_quantidade_comercial" name="txt_nfe_item_principal_quantidade_comercial" style="width: 182px;text-align:right" value="" <?=($nfe_importar > 0) ? 'readonly="readonly"' : '' ?> />
        </li>
        <li>
            <label for="txt_nfe_item_principal_unidade_tributavel">Unidade Tribut.</label>
            <input type="text" id="txt_nfe_item_principal_unidade_tributavel" name="txt_nfe_item_principal_unidade_tributavel" style="width: 176px;text-align:left" maxlength="6" value="<?=substr($unidade_tributavel,0,6)?>" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_quantidade_tributavel">Quantidade Tribut.</label>
            <input type="text" id="txt_nfe_item_principal_quantidade_tributavel" name="txt_nfe_item_principal_quantidade_tributavel" style="width: 176px;text-align:right" value="" <?=($nfe_importar > 0) ? 'readonly="readonly"' : '' ?> />
        </li>
        <li>
            <label for="txt_nfe_item_principal_valor_unidade_tributavel">Valor Unit. Tribut.</label>
            <input type="text" id="txt_nfe_item_principal_valor_unidade_tributavel" name="txt_nfe_item_principal_valor_unidade_tributavel" style="width: 176px;text-align:right" value="" <?=($nfe_importar > 0) ? 'readonly="readonly"' : '' ?> />
        </li>
        <li>
            <label for="txt_nfe_item_principal_total_seguro">Tot. Seguro</label>
            <input type="text" id="txt_nfe_item_principal_total_seguro" name="txt_nfe_item_principal_total_seguro" style="width: 176px;text-align:right" value="0,00" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_desconto">Desconto</label>
            <input type="text" id="txt_nfe_item_principal_desconto" name="txt_nfe_item_principal_desconto" style="width:176px;text-align:right" value="" <?=($nfe_importar > 0) ? 'readonly="readonly"' : '' ?> />
        </li>
        <li>
            <label for="txt_nfe_item_principal_frete">Frete</label>
            <input type="text" id="txt_nfe_item_principal_frete" name="txt_nfe_item_principal_frete" style="width: 176px;text-align:right" value="0,00" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_ean">EAN</label>
            <input type="text" id="txt_nfe_item_principal_ean" name="txt_nfe_item_principal_ean" style="width: 176px;" value="<?=$ean?>" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_ean_tributavel">EAN Tribut&aacute;vel</label>
            <input type="text" id="txt_nfe_item_principal_ean_tributavel" name="txt_nfe_item_principal_ean_tributavel" style="width: 176px;" value="<?=$ean_unidade_tributavel?>" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_outras_despesas_acessorias">Outras Desp. Acess&oacute;rias</label>
            <input type="text" id="txt_nfe_item_principal_outras_despesas_acessorias" name="txt_nfe_item_principal_outras_despesas_acessorias" style="width: 176px;text-align:right" value="0,00" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_valor_total_bruto">Valor Total Bruto</label>
            <input type="text" id="txt_nfe_item_principal_valor_total_bruto" name="txt_nfe_item_principal_valor_total_bruto" style="width: 176px;text-align:right" value="0,00" readonly="readonly" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_pedido_compra">Pedido de Compra</label>
            <input type="text" id="txt_nfe_item_principal_pedido_compra" name="txt_nfe_item_principal_pedido_compra" style="width: 176px;" value="" />
        </li>
        <li>
            <label for="txt_nfe_item_principal_numero_item_pedido_compra">N&ordm; Item do Pedido de Compra</label>
            <input type="text" id="txt_nfe_item_principal_numero_item_pedido_compra" name="txt_nfe_item_principal_numero_item_pedido_compra" style="width: 176px;text-align:right" value="" />
        </li>
        
        <!--
        <li>
			<label for="sel_nfe_item_principal_produto_especifico">Produto Especifico</label>
            <select name="sel_nfe_item_principal_produto_especifico" id="sel_nfe_item_principal_produto_especifico" style="width:176px">
            	<option value=""></option>
            	<option value="veiculo">ve&iacute;culo</option>
            	<option value="medicamento">medicamento</option>
            	<option value="armamento">armamento</option>
            	<option value="combustivel">combust&iacute;vel</option>
            </select>
        </li>
        -->
        <li>
            <label for="chk_nfe_item_principal_valor_total_bruto">Valor Total Bruto comp&otilde;e o Valor Total dos Produtos e Servi&ccedil;os</label>
            <input type="checkbox" id="chk_nfe_item_principal_valor_total_bruto" name="chk_nfe_item_principal_valor_total_bruto" style="width: 176px; float:left" checked="checked" value="1" />
        </li>
    </ul>
    
    
    
    <script type="text/javascript">
		
		//AO ALTERAR QUANTIDADE COMERCIAL, TB ALTERA A TRIBUTÁVEL
		$('#txt_nfe_item_principal_quantidade_comercial').live('blur',function(){
			$('#txt_nfe_item_principal_quantidade_tributavel').val($(this).val());
		});
		
		
		$('#txt_nfe_item_principal_valor_unitario_comercial').live('blur',function(){
			$('#txt_nfe_item_principal_valor_unidade_tributavel').val($(this).val());
		});
		
		
		//AO ALTERAR UNIDADE COMERCIAL, TB ALTERA O TRIBUTÁVEL
		$('#txt_nfe_item_principal_unidade_comercial').live('blur', function(){
			$('#txt_nfe_item_principal_unidade_tributavel').val($(this).val());
		});
		
		
	</script>
