
<script>
	$('a#venda').click(function(){
		
		cliente_id	= $('#hid_cliente_id').val();
		if(cliente_id > 0){
			$.get("modal/bina_chamada_busca.php", {table_action:'venda_listar', cliente_id:cliente_id}, function(valor){
				$("table#table_bina_vendas tbody").html(valor);
			});
			
			$.get("modal/bina_chamada_busca.php", {table_action:'parcela_listar', cliente_id:cliente_id}, function(valor){
				$("table#table_bina_parcelas tbody").html(valor);
			});
			
			$('a#btn_nova_venda').attr("href", "index.php?p=pedido&modo=rapido_novo&cliente_id="+cliente_id);

			return false;
		}
	});
</script>

<h3>&Uacute;ltimas vendas</h3>
<div id="table" style="width:735px">
	<div id="table_cabecalho" style="width:735px">
		<ul class="table_cabecalho" style="width:735px">
			<li style="width:60px;text-align:right">C&oacute;d.</li>
			<li style="width:470px;padding-left:5px">Descri&ccedil;&atilde;o</li>
			<li style="width:80px; text-align:center">Data</li>
			<li style="width:60px; text-align:right">Total</li>
			<li style="width:20px"></li>
		</ul>
	</div>
	<div style="width:735px; margin-bottom:10px;height:150px" id="table_container">       
		<table style="width:715px" id="table_bina_vendas" class="table_general" summary="Lista de vendas">
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>
<a style="float:right; margin-right:6px" id="btn_nova_venda" class="btn_novo" href="" title="novo">nova venda</a>

<h3 style="margin-top:50px">Parcelas em aberto</h3>
<div id="table" style="width:735px">
	<div id="table_cabecalho" style="width:735px">
		<ul class="table_cabecalho" style="width:735px">
			<li style="width:20px; text-align:center">&nbsp;</li>
			<li style="width:80px; text-align:center">Vencimento</li>
			<li style="width:40px; text-align:center">Parc.</li>
			<li style="width:100px; text-align:right">Valor</li>
			<li style="width:100px; text-align:right">Baixa</li>
			<li style="width:80px; text-align:right">Recebido</li>
			<li style="width:250px; text-align:right">Observa&ccedil;&atilde;o</li>
		</ul>
	</div>
	<div style="width:735px; margin-bottom:10px;height:120px" id="table_container">       
		<table style="width:715px" id="table_bina_parcelas" class="table_general" summary="Lista de vendas">
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>
