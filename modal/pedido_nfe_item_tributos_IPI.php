	<fieldset style="width:920px;display:inline;margin:10px">
    	<legend>IPI</legend>
        <ul>
            <li>
                <label for="sel_nfe_item_tributos_ipi_situacao_tributaria">Situa&ccedil;&atilde;o Tribut&aacute;ria</label>
                <select id="sel_nfe_item_tributos_ipi_situacao_tributaria" name="sel_nfe_item_tributos_ipi_situacao_tributaria" style="width:280px">
                	<option value=""></option>
<?					$sql = "SELECT * FROM tblnfe_ipi";
                    $rsIPI_Situacao = mysql_query($sql);
                    while($rowIPI_Situacao = mysql_fetch_array($rsIPI_Situacao)){
?>						<option <? $ipi_situacao_tributaria == $rowIPI_Situacao["fldId"] ? print 'selected="selected"' : '' ?> value="<?=$rowIPI_Situacao["fldId"]?>" title="<?=$rowIPI_Situacao["fldDescricao"]?>"><?=$rowIPI_Situacao["fldSituacaoTributaria"] . ' - ' . substr($rowIPI_Situacao["fldDescricao"],0,130)?></option>
<?					}
?>				</select>
            </li>
            <li>	
                <label for="txt_nfe_item_tributos_ipi_enquadramento_classe">Classe de Enquadramento (cigarros e bebidas)</label>
                <input type="text" style="width:280px;text-align:right" id="txt_nfe_item_tributos_ipi_enquadramento_classe" name="txt_nfe_item_tributos_ipi_enquadramento_classe" value="<?=$ipi_enquadramento_classe?>"/> 
            </li>
            <li>
                <label for="txt_nfe_item_tributos_ipi_enquadramento_codigo">C&oacute;digo de Enquadramento Legal</label>
                <input type="text" style="width:280px;text-align:right" id="txt_nfe_item_tributos_ipi_enquadramento_codigo" name="txt_nfe_item_tributos_ipi_enquadramento_codigo" value="<?=$ipi_enquadramento_codigo?>"/>
            </li>
            <li>
                <label for="txt_nfe_item_tributos_ipi_produtor_cnpj">CNPJ do Produtor</label>
                <input type="text" style="width:280px;text-align:right" id="txt_nfe_item_tributos_ipi_produtor_cnpj" name="txt_nfe_item_tributos_ipi_produtor_cnpj" value="<?=$ipi_produtor_cnpj?>" maxlength="17" />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_ipi_selo_controle_codigo">C&oacute;digo do Selo de Controle</label>
                <input type="text" style="width:280px;text-align:right" id="txt_nfe_item_tributos_ipi_selo_controle_codigo" name="txt_nfe_item_tributos_ipi_selo_controle_codigo" value="" maxlength="17" />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_ipi_selo_controle_quantidade">Quantidade do Selo de Controle</label>
                <input type="text" style="width:280px;text-align:right" id="txt_nfe_item_tributos_ipi_selo_controle_quantidade" name="txt_nfe_item_tributos_ipi_selo_controle_quantidade" value="" maxlength="17" />
            </li>
            <li id="li_calculo_ipi_js">
                <label for="sel_nfe_item_tributos_ipi_calculo_tipo">Tipo de C&aacute;lculo</label>
                <select name="sel_nfe_item_tributos_ipi_calculo_tipo" id="sel_nfe_item_tributos_ipi_calculo_tipo" <?=($ipiCalculoTipo != 'enable' )? "disabled='disabled'" : ''?> style="width:280px;">
                	<option value=""></option>
                    <option <? $ipi_calculo_tipo == 1 ? print 'selected="selected"' : '' ?> value="1">Percentual</option>
                    <option <? $ipi_calculo_tipo == 2 ? print 'selected="selected"' : '' ?> value="2">Valor</option>
                </select>
            </li>
            <li>
                <label for="txt_nfe_item_tributos_ipi_valor_base_calculo">Valor da Base de C&aacute;lculo</label>
                <input type="text" style="width:280px;text-align:right;<?=($ipi_calculo_tipo != 1)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_ipi_valor_base_calculo" name="txt_nfe_item_tributos_ipi_valor_base_calculo" <?=($ipi_calculo_tipo != 1)? "disabled='disabled'" : ''?> value="" maxlength="17" />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_ipi_aliquota">Al&iacute;quota</label>
                <input type="text" style="width:280px;text-align:right;<?=($ipi_calculo_tipo != 1)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_ipi_aliquota" name="txt_nfe_item_tributos_ipi_aliquota" <?=($ipi_calculo_tipo != 1)? "disabled='disabled'" :  "value='$ipi_aliquota'" ?>  />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total">Quantidade Total Unidade Padr&atilde;o</label>
                <input type="text" style="width:280px;text-align:right;<?=($ipi_calculo_tipo < 2)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total" name="txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total" <?=($ipi_calculo_tipo < 2)? "disabled='disabled'" : ''?>  value="" />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_ipi_unidade_valor">Valor por Unidade</label>
                <input type="text" style="width:280px;text-align:right;<?=($ipi_calculo_tipo < 2)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_ipi_unidade_valor" name="txt_nfe_item_tributos_ipi_unidade_valor" <?=($ipi_calculo_tipo < 2)? "disabled='disabled'" : "value='$ipi_unidade_valor'" ?> />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_ipi_valor">Valor do IPI</label>
                <input type="text" style="width:280px;text-align:right" id="txt_nfe_item_tributos_ipi_valor" name="txt_nfe_item_tributos_ipi_valor" value=""  readonly="readonly" />
            </li>
        </ul>
	</fieldset>
