<?	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');

	//se for editar um registro
	if(isset($_POST['params'][1])){
		$ruaId 		= $_POST['params'][1];
		$rsRua 		= mysql_query("SELECT tblendereco.*, tblendereco_bairro.fldSetor FROM tblendereco LEFT JOIN tblendereco_bairro
								  ON tblendereco.fldBairro_Id = tblendereco_bairro.fldId WHERE tblendereco.fldId = $ruaId");
		$rowRua 	= mysql_fetch_array($rsRua);
		$rua	 	= $rowRua['fldRua'];
		$setor	 	= $rowRua['fldSetor'];
		$cep	 	= $rowRua['fldCep'];
		$ordem 		= $rowRua['fldOrdem'];
		$bairroId	= $rowRua['fldBairro_Id'];
	}
	echo mysql_error();	?>

	<span style="font-size:12px; color:red; margin:20px 0; display: block">Vincule os c&oacute;digos IBGE ao endere&ccedil;o ou fa&ccedil;a um cadastro manual</span>

    <form id="frm_adicional" class="frm_detalhe" style="width: 730px" action="index.php?p=endereco" method="post">
        <input type="hidden" id="hid_edit" name="hid_edit" value="<?=$ruaId?>" />
        <ul>
            <li>
				<script>
                $(document).ready(function(){
                
                    $('#txt_cep').keyup(function(e){
                        /*********************************
                        **	backspace 	46				**
                        **	delete		8				**
                        **	numerico 	entre 48 e 57	**
                        *********************************/
                        
                        var code = (e.keyCode ? e.keyCode : e.which);
                        var texto 	= $(this).val();
                        texto 		= texto.replace('-', '');
                        texto 		= texto.replace('.', '');
                        texto 		= texto.replace('_', '');
                        var total_texto = texto.length;
                        
                        if (code == 46 || code == 8 || ((code >= 48 && code <= 57) || (e.keyCode >= 96 && e.keyCode <= 105))) {

                            if(total_texto == 8){
                            
                                $(this).blur();
                            
                                /**add "modal"****/
                                $('#link_pesquisa_cep').trigger('click');
                                /***************/

                                $.ajax({
                                    type: "GET",
                                    dataType: "json",
                                    url: "cep/busca_cep.php?tipo=cadastro_endereco&termo="+texto,
                                    success: function(data){
                                        if(!data.erro){
                                            //sucesso
                                            var rua 			= data.rua;
											var bairro_id		= data.bairro_id;
                                            var bairro 			= data.bairro;
											
											$.ajax({
												type: "GET",
												dataType: "json",
												url: "filtro_ajax.php?buscar_bairro_rota="+bairro_id,
												success: function(data){
													if(!data.erro){
														var id 	= data.bairro_id;
														$('#sel_bairro').find('option[value="'+id+'"]').attr('selected', true);
														$('#sel_bairro').change();
													}else{
														$('#sel_bairro').find('option:first').attr('selected', true);
														$('#txt_setor').val('');
														$('#txt_ordem').val('');
														alert('O bairro do CEP digitado nao foi encontrado no cadastro de rotas, selecione o bairro manualmente.');
													}
												}
											});
											$('#txt_rua').val(rua);
											$('#txt_cep').focus();
                                            $('a.modal-fechar:last').trigger('click');
                                        } else {
                                            //erro
											var continuar = confirm("Este CEP nao foi encontrado em nossa base de dados, deseja continuar?");
											if(!continuar){
												$('#txt_cep').val('');
												$('#txt_rua').val('');
											}
                                            $('a.modal-fechar:last').trigger('click');
                                        }
                                    }
                                });
                                
                            }
                            
                        }
                        
                    });

                });
                </script>
            
                <label for="txt_cep">Cep</label>
                <input type="text" style="width:70px" id="txt_cep" name="txt_cep" value="<?=$cep?>" />
                <a href="busca_cep" title="Localizar" class="modal" rel="945-400"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                <a href="busca_cep_pesquisando" class="modal" id="link_pesquisa_cep" rel="200-100"></a>
            </li>
            <li>
                <label for="txt_rua">Rua</label>
                <input type="text" style="width:280px" id="txt_rua" name="txt_rua" value="<?=$rua?>" />
            </li>
            <li>
                <label for="sel_bairro">Bairro</label>
                <select style="width:204px" id="sel_bairro" name="sel_bairro">
                	<option selected="selected"></option>
<?					$rsBairro = mysql_query("SELECT * FROM tblendereco_bairro WHERE fldExcluido = 0 ORDER BY fldBairro");
					while($rowBairro = mysql_fetch_array($rsBairro)){
?>						<option <?= ($bairroId == $rowBairro['fldId'])? "selected='selected'": ''?> value="<?=$rowBairro['fldId']?>"><?=$rowBairro['fldBairro']?></option>
<?					}
?>				</select>
            </li>
            <li>
                <label for="txt_setor">Setor</label>
                <input type="text" style="width:40px;text-align:right" id="txt_setor" name="txt_setor" value="<?=$setor?>" readonly="readonly" />
            </li>
            <li>
                <label for="txt_ordem">Ordem</label>
                <input type="text" style="width:40px; text-align:right" id="txt_ordem" name="txt_ordem" value="<?=$ordem?>" />
            </li>
            <li style="float:right; margin-right:10px">
                <input type="submit" class="btn_enviar" style="margin-top:12px" name="btn_enviar_endereco" id="btn_enviar_endereco" value="gravar" title="gravar" />
            </li>
         </ul>
    </form>

	<script type="text/javascript"> 
		/*$('#sel_bairro').find('option:last-child').attr("selected", "selected");
		$('#sel_bairro').change();*/
	
		$('#sel_bairro').change(function(){
			var id = $(this).attr('value');
			$.post("modal/endereco_cadastro_filtro.php", {sel_bairro:id}, function(theXML){
				$('dados',theXML).each(function(){
					var setor = $(this).find("setor").text();
					var ordem = $(this).find("ordem").text();
					$('#txt_setor').val(setor);
					$('#txt_ordem').val(ordem);
				});
			});
		});
	</script>