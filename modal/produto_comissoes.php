 <script>

	$(document).ready(function(){
		$("#txt_funcionario_codigo").focus();
	})

	$('#txt_funcionario_codigo').keyup(function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	$('#txt_funcionario_codigo').focus(function(){
		$.post("modal/funcionario_busca_retorno.php",
		function(data){
			if(data){
				$('#txt_funcionario_codigo').val(data);
				$("#txt_funcionario_codigo").blur();
				$("#txt_produto_comissao_funcionario_nome").focus();
			}
		});
	});
		
	
	$('#txt_funcionario_codigo').blur(function(){
		var funcionarioId = $(this).val();
		$.post(urlDestino, {busca_funcionario: funcionarioId}, function(theXML){
			$('dados',theXML).each(function(){
				var nome	 = $(this).find("nome").text();
				if ($('#txt_funcionario_codigo').val()!=''){$('#txt_produto_comissao_funcionario_nome').val(nome);}
				else{$('#txt_produto_comissao_funcionario_nome').val('');}
			});
		});
     });

</script>

<? if(isset($_POST['params'][1])) {

	require('../inc/con_db.php');
	require('../inc/fnc_general.php');

	$sqlProduto_Comissao = mysql_query("SELECT 
								tblproduto_comissao.*,
								tblfuncionario.fldNome AS fldFuncionario_Nome
							FROM tblproduto_comissao 
								INNER JOIN tblfuncionario ON tblfuncionario.fldId = tblproduto_comissao.fldFuncionario_Id
							WHERE tblproduto_comissao.fldId =".$_POST['params'][1]);

	$rowProduto_Comissao = mysql_fetch_assoc($sqlProduto_Comissao);	?>

	<form action="" method="post" class="frm_detalhe">

		<ul style="width: 540px;">
	    	<li>
	    		<input type="hidden" id="hid_registro_id" name="hid_registro_id" value="<?=$_POST['params'][1]?>">
	        	<label for="txt_funcionario_codigo">Funcionário</label>
	        	<input type="text" id="txt_funcionario_codigo" name="txt_funcionario_codigo" style="width: 80px;" value="<?=$rowProduto_Comissao['fldFuncionario_Id']?>" />
	        	<a href="funcionario_busca,1" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
			</li>
			<li>
				<label for="txt_produto_comissao_funcionario_nome">&nbsp;</label>
	        	<input type="text" id="txt_produto_comissao_funcionario_nome" name="txt_produto_comissao_funcionario_nome" style="width: 406px;" value="<?=$rowProduto_Comissao['fldFuncionario_Nome']?>" />
			</li>
			<span style="display:block; clear:both"></span>
			<li style="float:right;">
				<label for="txt_produto_comissao_valor">Comiss&atilde;o</label>
	        	<input type="text" id="txt_produto_comissao_valor" name="txt_produto_comissao_valor" style="width: 100px;" value="<?=format_number_out($rowProduto_Comissao['fldComissao_Valor']);?>" />
	        	<select id="sel_produto_comissao_tipo" name="sel_produto_comissao_tipo" style="margin-left:3px; width:50px">
	        		<option <?= ($rowProduto_Comissao['fldComissao_Tipo'] == '2') ? 'selected="selected"' : '';?> value="2">%</option>
	        		<option <?= ($rowProduto_Comissao['fldComissao_Tipo'] == '1') ? 'selected="selected"' : '';?> value="1">R$</option>
	        	</select>
	        	<input style="margin:-3px 0 0 5px" type="submit" class="btn_enviar" name="btn_editar_produto_comissao" id="btn_editar_produto_comissao" value="OK" title="OK" />
			</li>
		</ul>

	</form>

<? } else { ?> 

	<form action="" method="post" class="frm_detalhe">

		<ul style="width: 540px;">
	    	<li>
	        	<label for="txt_funcionario_codigo">Funcionário</label>
	        	<input type="text" id="txt_funcionario_codigo" name="txt_funcionario_codigo" style="width: 80px;" value="" />
	        	<a href="funcionario_busca,1" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
			</li>
			<li>
				<label for="txt_produto_comissao_funcionario_nome">&nbsp;</label>
	        	<input type="text" id="txt_produto_comissao_funcionario_nome" name="txt_produto_comissao_funcionario_nome" style="width: 406px;" value="" />
			</li>
			<span style="display:block; clear:both"></span>
			<li style="float:right;">
				<label for="txt_produto_comissao_valor">Comiss&atilde;o</label>
	        	<input type="text" id="txt_produto_comissao_valor" name="txt_produto_comissao_valor" style="width: 100px;" value="" />
	        	<select id="sel_produto_comissao_tipo" name="sel_produto_comissao_tipo" style="margin-left:3px; width:50px">
	        		<option value="2">%</option>
	        		<option value="1">R$</option>
	        	</select>
	        	<input style="margin:-3px 0 0 5px" type="submit" class="btn_enviar" name="btn_enviar_produto_comissao" id="btn_enviar_produto_comissao" value="OK" title="OK" />
			</li>
		</ul>

	</form>

<? } ?>