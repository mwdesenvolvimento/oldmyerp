<script type="text/javascript" src="js/myerp.js" charset="ISO-8859-1"></script>
<?
	ob_start();
	session_start();
	
	require('../inc/con_db.php');
	require('../inc/fnc_general.php');
	
	if(isset($_POST['form'])){
		
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$data_cadastro 	= date("Y-m-d");
		$data		 	= format_date_in($form['txt_data']);
		$hora			= $form['txt_hora'];
		$rota_id 		= $form['sel_rota'];
		$valor_saida	= format_number_in($form['txt_valor_saida']);
		$funcionario1	= $form['sel_funcionario_1'];
		$funcionario2	= $form['sel_funcionario_2'];
		$usuario_id		= $_SESSION['usuario_id'];
		
		$SQL = "INSERT INTO tblpedido_consignado
		(fldData_Cadastro, fldUsuario_Id, fldData, fldHora, fldRota_Id, fldValor_Saida, fldFuncionario1_Id, fldFuncionario2_Id)
		VALUES (
		'$data_cadastro',
		'$usuario_id',
		'$data',
		'$hora',
		'$rota_id',
		'$valor_saida',
		'$funcionario1',
		'$funcionario2'
		)";
		
		if(mysql_query($SQL)){
			$LastId 		= mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID"));
			$consignado_id 	= $LastId['lastID'];
		}else{
			echo mysql_error();
			die();
		}
		
		$n= 1;
		$limite 	= $form["hid_controle"];
		while($n 	<= $limite){
			
			$produto_id	 			= $form['hid_item_produto_id_'.$n];
			$produto_descricao		= $form['txt_item_nome_'.$n];
			$valor 					= format_number_in($form['txt_item_valor_'.$n]);
			$quantidade				= format_number_in($form['txt_item_quantidade_'.$n]);
			
			if($produto_id > 0){
				$SQL = "INSERT INTO tblpedido_consignado_item
				(fldProduto_Id, fldDescricao, fldValor, fldQuantidade, fldConsignado_Id)
				VALUES (
				'$produto_id',
				'$produto_descricao',
				'$valor',
				'$quantidade',
				'$consignado_id '
				)";
				
				if(mysql_query($SQL)){
					$n++;
				}else{
					echo mysql_error();
					die();
				}
			}
		}
?>
		<img src="image/layout/carregando.gif" align="carregando..." />
		<script type="text/javascript">
			window.location="index.php?p=pedido&modo=consignado";
		</script> 
<?		die;
	}
		
?>
	<form id="frm_pedido_consignado" name="frm_general" class="frm_detalhe" style="width:95%" action="" method="post">
    	<fieldset style="margin-bottom: 5px">
            <ul>
                <li>
                    <label for="txt_data">Data</label>
                    <input type="text" style="width:80px; text-align:center;background:#FFC" id="txt_data" name="txt_data" value="<?=date("d/m/Y")?>" />
                </li>
                <li>
                    <label for="txt_hora">Hora</label>
                    <input type="text" style="width:60px;text-align:center;background:#FFC" id="txt_hora" name="txt_hora" value="<?=date("H:i:s")?>" />
                </li>
                <li>
<?					if($_SESSION["sistema_rota_controle"] == 1){            
?>	                	<label for="sel_rota">Rota</label>
  						<select style="width:90px; text-align:left" id="sel_rota" name="sel_rota" >
                    		<option value="0">Selecionar</option>
<?							$rsRota = mysql_query("SELECT * FROM tblendereco_rota WHERE fldExcluido = 0 ORDER BY fldRota+0");
							while($rowRota = mysql_fetch_array($rsRota)){
?>								<option value='<?=$rowRota['fldId']?>'><?=$rowRota['fldRota']?></option>
<?							}
?>						</select>
<?					}
?>				</li>
            	<li style="width:350px; height:40px">
                    <label for="txt_valor_saida">Troco</label>
                    <input type="text" style="width:150px;text-align:right; font-size:18px;background:#FF6" id="txt_valor_saida" name="txt_valor_saida" value="" />
                </li>
                <li>
                    <label for="sel_funcionario_1">Funcionario 1</label>
                    <select style="width:255px" id="sel_funcionario_1" name="sel_funcionario_1" >
                        <option value="0">Selecionar</option>
<?						$rsFuncionario = mysql_query("SELECT fldId, fldNome FROM tblfuncionario WHERE fldDisabled = '0' ORDER BY fldNome");
						while($rowFuncionario = mysql_fetch_array($rsFuncionario)){
?>							<option value='<?=$rowFuncionario['fldId']?>'><?=$rowFuncionario['fldNome']?></option>
<?						}
?>					</select>
            	</li>
                <li>
                    <label for="sel_funcionario_2">Funcionario 2</label>
                    <select style="width:255px" id="sel_funcionario_2" name="sel_funcionario_2" >
                        <option value="0">Selecionar</option>
<?						$rsFuncionario = mysql_query("SELECT fldId, fldNome FROM tblfuncionario WHERE fldDisabled = '0' ORDER BY fldNome");
						while($rowFuncionario = mysql_fetch_array($rsFuncionario)){
?>							<option value='<?=$rowFuncionario['fldId']?>'><?=$rowFuncionario['fldNome']?></option>
<?						}
?>					</select>
                </li>
            </ul>
        </fieldset>
        <fieldset style="margin-bottom:10px; background:#E1E1E1">
            <ul id="produto_inserir">
                <li> 
                    <label for="txt_produto_codigo">C&oacute;digo</label>
                     <input type="text" class="codigo" style="width:100px" id="txt_produto_codigo" name="txt_produto_codigo" value="" />
                     <a href="produto_busca" title="Localizar" class="modal" rel="950-450"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                     <input type="hidden" id="hid_produto_id" name="hid_produto_id" value="" />
                </li>
                <li> 
                    <label for="txt_produto_nome">Produto</label>
                    <input type="text" style=" width:330px" id="txt_produto_nome" name="txt_produto_nome" value="" />
                </li>
                <li> 
                    <label for="txt_produto_valor" style="text-align:center">Valor</label>
                    <input type="text" style=" width:100px;text-align:right" id="txt_produto_valor" name="txt_produto_valor" value="" />
                </li>
                <li> 
                    <label for="txt_produto_quantidade" style="text-align:center">Quantidade</label>
                    <input type="text" style=" width:80px;text-align:right" id="txt_produto_quantidade" name="txt_produto_quantidade" value="" />
                </li>
                <li style="margin-top:6px">
                  	<button class="btn_sub_small" name="btn_inserir" id="btn_inserir" title="Inserir" >ok</button>
                </li>
            </ul>
       </fieldset>
       
       <div id="table" style="width:735px">
			<div id="table_cabecalho" style="width:735px">
                <ul class="table_cabecalho" style="width:735px">
                    <li style="width:100px;	text-align:right">C&oacute;d.</li>
                    <li style="width:365px; text-align:left">Produto</li>
                    <li style="width:100px; text-align:right">Valor</li>
                    <li style="width:100px; text-align:right">Qtde</li>
                    <li style="width:20px; text-align:right">&nbsp;</li>
                </ul>
            </div>
            <div style="width:735px;height:180px" id="table_container">       
                <table style="width:715px" id="table_pedido_consignado" class="table_general" summary="Lista de produtos">
                    <tbody>
                        <tr id="pedido_lista_item" class="lista_produto" style="display:none">
                            <td style="width:100px"><input type="text" 	style="width:100px;text-align:right" id="txt_item_codigo" 	 name="txt_item_codigo"		value="" readonly="readonly" /></td>
                            <td style="width:365px"><input type="text" 	style="width:350px;text-align:left"	 id="txt_item_nome"		 name="txt_item_nome"	 	value="" readonly="readonly" /></td>
                            <td style="width:100px"><input type="text" 	style="width:100px;text-align:right" id="txt_item_valor" 	 name="txt_item_valor" 		value="" readonly="readonly" class="item_valor" /></td>
                            <td style="width:100px"><input type="text" 	style="width:100px;text-align:right" id="txt_item_quantidade"name="txt_item_quantidade" value="" readonly="readonly" /></td>
                            <td style="width:20px"><a class="a_excluir" id="excluir" href="" title="Excluir item"></a></td>
                            <td style="display:none"><input type="hidden" id="hid_item_produto_id" name="hid_item_produto_id" value="" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
		</div>
        
		<div class="saldo" style="width:727px">
            <ul class="table_cabecalho" style="width:727px">
                <li style="width:130px"><span style="width:130px; text-align:left">Total de produtos</span></li>
                <li style="width:100px"><input style="width:150px" class="form_valor_total" type="text" name="txt_valor_total" id="txt_valor_total" value="" readonly="readonly" /></li>
            </ul>
        </div>
		<input type="hidden" id="hid_controle" name="hid_controle" value="0" />
        <input type="submit" style="margin-top:14px; float:right" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="salvar" title="Salvar" />
    </form>

	<script type="text/javascript">
	
        $('#txt_valor_saida').focus();	
		
		//##################################################################################################################################################################
		
		$('#txt_produto_codigo').blur(function(){
			if($('#txt_produto_codigo').val() != ''){
				codigo = $(this).val();
				$.get('filtro_ajax.php', {busca_produto:codigo}, function(theXML){
					$('dados',theXML).each(function(){
													
						var produto_id 		= $(this).find("produto_id").text();
						var produto 		= $(this).find("produto").text();
						var valor 			= $(this).find("valor").text();
						
						$('#hid_produto_id').val(produto_id);
						$('#txt_produto_nome').val(unescape(produto));
						$('#txt_produto_valor').val(valor);
						$('#txt_produto_quantidade').focus();
					});
				});
			}else{
				$('#hid_produto_id').val('');
				$('#txt_produto_nome').val('');
				$('#txt_produto_valor').val('');
			}
		});
		
		//##################################################################################################################################################################
		
		function calculoTotal(){
			total_valor = 0;
			$.each($("input.item_valor"),function(){
				
				item_valor	 	= br2float(ifNumberNull($(this).parents('tr').find('[name^=txt_item_valor_]').val())).toFixed(2);
				item_quantidade	= br2float(ifNumberNull($(this).parents('tr').find('[name^=txt_item_quantidade_]').val())).toFixed(2);
				
				total_item 		= item_valor * item_quantidade;
				total_valor		+= total_item;
			});
			
			$('#txt_valor_total').val(float2br(total_valor.toFixed(2)));
		}
		
		//##################################################################################################################################################################

		$('#btn_inserir').click(function(event){
			event.preventDefault();
			
			produto_codigo 		= $('#txt_produto_codigo').val();
			produto_id 			= $('#hid_produto_id').val();
			produto_nome		= $('#txt_produto_nome').val();
			produto_valor 		= $('#txt_produto_valor').val();
			produto_quantidade 	= $('#txt_produto_quantidade').val();
		
			if(produto_codigo != '' && produto_nome != '' && produto_quantidade != ''){
				controle = Number($('#hid_controle').val()) + 1;
				
				$('tr.lista_produto:first').clone(true).appendTo('#table_pedido_consignado').find('tbody');
				
				$('tr.lista_produto:last').find('#txt_item_codigo').attr(	 {id: 'txt_item_codigo_'+controle, 		name: 'txt_item_codigo_'+controle});
				$('tr.lista_produto:last').find('#hid_item_produto_id').attr({id: 'hid_item_produto_id_'+controle,	name: 'hid_item_produto_id_'+controle});
				$('tr.lista_produto:last').find('#txt_item_nome').attr(		 {id: 'txt_item_nome_'+controle, 		name: 'txt_item_nome_'+controle});
				$('tr.lista_produto:last').find('#txt_item_valor').attr(	 {id: 'txt_item_valor_'+controle, 		name: 'txt_item_valor_'+controle});
				$('tr.lista_produto:last').find('#txt_item_quantidade').attr({id: 'txt_item_quantidade_'+controle, 	name: 'txt_item_quantidade_'+controle});
				$('tr.lista_produto:last').find('#excluir').attr(			 {id: 'excluir_'+controle});
				
				$('#txt_item_codigo_'+controle).val(	produto_codigo);
				$('#hid_item_produto_id_'+controle).val(produto_id);
				$('#txt_item_nome_'+controle).val(		produto_nome);
				$('#txt_item_valor_'+controle).val(		produto_valor);
				$('#txt_item_quantidade_'+controle).val(produto_quantidade);
				
				$('tr.lista_produto:last').removeAttr('style');
				$('#hid_controle').val(controle);
				
				$('ul#produto_inserir li').find('input[name!=btn_inserir]').val('');
				$('#txt_produto_codigo').focus();
			}
			 calculoTotal();
		});
		
		//##################################################################################################################################################################
		
		$('.a_excluir').click(function(event){
			event.preventDefault();
			$(this).parents("tr.lista_produto").remove();	
			 calculoTotal();
		});
		
		//##################################################################################################################################################################
		
		$('#frm_pedido_consignado').submit(function(e){
			e.preventDefault();
			
			var form 	= $(this).serialize();
			$('div.modal-conteudo').load('modal/pedido_consignado_novo.php', {form : form});
		});
		
		//##################################################################################################################################################################
		
		$('#txt_valor_saida, #txt_produto_valor, #txt_produto_quantidade').blur(function(){
			$(this).val(float2br(br2float(ifNumberNull($(this).val())).toFixed(2)));
		});
		
	</script>