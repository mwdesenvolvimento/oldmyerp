<form id="frm_relatorio" class="frm_detalhe" method="post" action="estoque_relatorio.php" target="_blank">
    <ul style="padding:0 10px">
        <li>	
            <label for="sel_relatorio_tipo">Tipo de relat&oacute;rio</label>
            <select class="sel_relatorio_tipo" style="width:147px;" id="sel_relatorio_tipo" name="sel_relatorio_tipo" >
                <option value="simples">Simples</option>
                <option value="lucro">Lucro</option>
            </select>
        </li>
        
        <li>
            <label for="sel_relatorio_agrupar">Agrupar por</label>
            <select class="sel_relatorio_agrupar" style="width:147px" id="sel_relatorio_agrupar" name="sel_relatorio_agrupar" >
                <option value="quantidade">quantidade</option>
                <option value="validade">validade</option>
            </select>
        </li>
        
        <div id="div-quantidade">
			<li>
                <label for="txt_estoque_dia">Estoque do dia</label>
                <input type="text" style="width:140px" name="txt_estoque_dia" id="txt_estoque_dia" class="calendario-mask" value="<?=date("d/m/Y")?>">
            </li>
        
            <li>
                <label style="margin-top:16px"><input type="checkbox" name="chk_valor_compra" style="width:15px; float:left; margin-right:3px;" /> <span style="font-size:11px; font-weight:normal">Adicionar valor de compra</span></label>
            </li>
        </div>
        
        <div id="div-validade" style="display:none">
            <li>
                <label for="sel_relatorio_agrupar">Período</label>
                <input type="text" style="width:140px" name="txt_periodo_inicio" id="txt_periodo_inicio" class="calendario-mask">
                <input type="text" style="width:140px; margin-left:09px" name="txt_periodo_final" id="txt_periodo_final" class="calendario-mask">
            </li>
        </div>
        
        <span style="clear:both; display:block"></span>
        
        <li style="margin:6px 4px 0 0; float:right"><input type="submit" class="btn_enviar" name="btn_estoque_relatorio" id="btn_estoque_relatorio" value="Enviar" title="Enviar" /></li>
    </ul>
</form>


<script language="javascript">
	$("#sel_relatorio_tipo").change(function(){
		if($(this).val() == 'lucro' ){
			$("#sel_relatorio_agrupar").attr({"disabled" : "disabled"});
            $("#div-quantidade").css({"display" : "none"});
            $("#div-validade").css({"display" : "none"});
		}else{
			$("#sel_relatorio_agrupar").attr({"disabled" : ""});
            if($('#sel_relatorio_agrupar').val() == 'validade'){
                $("#div-quantidade").css({"display" : "none"});
            	$("#div-validade").css({"display" : "block"});
            }
		}
	});

    $('#sel_relatorio_agrupar').change(function(){
        if($('#sel_relatorio_agrupar').val() == 'validade'){
            $("#div-quantidade").css({"display" : "none"});
            $("#div-validade").css({"display" : "block"});
        }
        else{
            $("#div-quantidade").css({"display" : "block"});
            $("#div-validade").css({"display" : "none"});
        }
    })

</script>