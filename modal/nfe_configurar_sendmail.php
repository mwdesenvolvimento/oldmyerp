<?php	
	require_once('../inc/con_db.php');
	if(isset($_POST['form'])){
		
		$serialize 	= $_POST['form'];
		parse_str($serialize, $form);
		
		$email_porta	 = $form['txt_email_porta']; 
		$email_smtp		 = $form['txt_email_smtp'];
		$email_usuario	 = $form['txt_email_usuario'];
		$email_senha	 = $form['txt_email_senha'];
		
		if(mysql_query("UPDATE tblsendmail_config SET fldPorta = '{$email_porta}', fldSmtp = '{$email_smtp}', fldUsuario = '{$email_usuario}', fldSenha = '{$email_senha}' WHERE fldPadrao = 0")){
		
			// abre o arquivo colocando o ponteiro de escrita no final
			$arquivo = fopen('D:/wamp/www/myerp/nfephp/sendmail/sendmail.ini','r+');
			if ($arquivo) {
				while(true) {
					$linha = fgets($arquivo);
					if ($linha==null) break;
					
					// busca na linha atual o conteudo que vai ser alterado
					if(preg_match("/smtp_server/", $linha)) {
						$server_antiga = $linha;
						$server_nova = "smtp_server=".$email_smtp."\r\n";
						$string .= str_replace($server_antiga, $server_nova, $linha);
					}elseif(preg_match("/smtp_port/", $linha)) {
						$port_antiga = $linha;
						$port_nova = "smtp_port=".$email_porta."\r\n";
						$string .= str_replace($port_antiga, $port_nova, $linha);
					
					}elseif(preg_match("/auth_username/", $linha)) {
						$user_antiga = $linha;
						$user_nova = "auth_username=".$email_usuario."\r\n";
						$string .= str_replace($user_antiga, $user_nova, $linha);
					
					}elseif(preg_match("/auth_password/", $linha)) {
						$pass_antiga = $linha;
						$pass_nova = "auth_password=".$email_senha."\r\n";
						$string .= str_replace($pass_antiga, $pass_nova, $linha);
					
					}else {
						// vai colocando tudo numa nova string
						$string.= $linha;
					}
				}
				// move o ponteiro para o inicio pois o ftruncate() nao fara isso
				rewind($arquivo);
				// truca o arquivo apagando tudo dentro dele
				ftruncate($arquivo, 0);
				// reescreve o conteudo dentro do arquivo
				if (!fwrite($arquivo, $string)) die('Não foi possível atualizar o arquivo.');
				echo '	<div class="alert" style="margin-top:6px; width:380px">
							<p class="ok">Dados atualizados!<p>
						</div>';
				fclose($arquivo);
			}
		}
	}else{
		
		echo '	<small style="margin-left:22px;color:#F00">* Qualquer altera&ccedil;&atilde;o pode afetar o funcionamenteo de envio da Nfe.</small>
				<small style="margin-left:22px;color:#F00">* Consulte o suporte t&eacute;cnico antes de prosseguir.</small>';
	}

	$rsConfig = mysql_query("SELECT * FROM tblsendmail_config WHERE fldPadrao = 0");
	$rowConfig = mysql_fetch_array($rsConfig);
?>

	<form id="frm_sendmail_config" class="frm_detalhe" style="width:480px;" action="" method="post">
		<ul style=" margin-left:20px">
			<li> 
				<label for="txt_email_porta">Porta</label>
				<input type="text" style=" width:80px" id="txt_email_porta" name="txt_email_porta" value="<?=$rowConfig['fldPorta']?>" />
			</li>
			<li> 
				<label for="txt_email_smtp">SMTP</label>
				<input type="text" style="width:250px" id="txt_email_smtp" name="txt_email_smtp" value="<?=$rowConfig['fldSmtp']?>" />
			</li>
			<li>
				<label for="txt_email_usuario">Usu&aacute;rio</label>
				<input type="text" style="width:345px" id="txt_email_usuario" name="txt_email_usuario" value="<?=$rowConfig['fldUsuario']?>" />
			</li>
			<li>
				<label for="txt_email_senha">Senha</label>
				<input type="password" style="width:200px" id="txt_email_senha" name="txt_email_senha" value="<?=$rowConfig['fldSenha']?>" />
			</li>
            <li style="width:135px;height:40px">
            	<a style="color:#F00;text-decoration:underline;margin-top:20px" href="#" title="restaurar padr&atilde;o" id="btn_restaurar">restaurar padr&atilde;o</a>
            </li>
            <li style="margin-left:125px">
            	<input type="submit" style="margin:10px" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
            </li>
        </ul>
    </form>            
    
    
	<script language="javascript">    
		$('#frm_sendmail_config').submit(function(){
			var form 	= $(this).serialize();
			$('div.modal-conteudo').load('modal/nfe_configurar_sendmail.php', {form : form});
			return false;
		});
		
		
		$("#btn_restaurar").click(function(){
														   
			$.get('modal/nfe_filtro_sendmail.php', {btn_restaurar:true}, function(theXML){
				$('dados',theXML).each(function(){
				
					var email_porta 	= $(this).find("email_porta").text();
					var email_smtp	 	= $(this).find("email_smtp").text();
					var email_usuario	 = $(this).find("email_usuario").text();
					var email_senha	 	= $(this).find("email_senha").text();
		
					$('#txt_email_porta').val(email_porta);
					$('#txt_email_smtp').val(email_smtp);
					$('#txt_email_usuario').val(email_usuario);
					$('#txt_email_senha').val(email_senha);
				});
			});
		});
		
	</script>		