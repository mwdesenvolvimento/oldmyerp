<? 	require('../inc/con_db.php');
	require('../inc/fnc_general.php');
	require_once('../nfephp/libs/AutoToolsNFePHP.class.php');
	require_once('../nfephp/libs/ToolsNFePHP.class.php');
	
	$_NFe 		= new AutoToolsNFePHP;
	$nfe  		= new ToolsNFePHP;
	$tpAmb 		= fnc_sistema('nfe_ambiente_tipo');
	$ambiente 	= ($tpAmb == '1')? 'producao': 'homologacao' ;
	
	#confiro se ha arquivos pra consultar
	if($_GET['action'] == 'autoProt'){
		$retorno 	= 0;
		$dir		='../nfe_myerp/'.$ambiente.'/enviadas/';
		$dh = opendir($dir);
		while (($file = readdir($dh)) !== false){
			if ($file != '.' || $file != '..'){
				if(is_file($dir.$file)){
					if(end(explode(".",$file))=="xml"){
						$file_count ++;
					}
				}
			}
		}

		#Solicita o protocolo de uso e move as NFes para as
		#respectivas pastas (APROVADAS, REPROVADAS, DENEGADAS)
		if($file_count > 0){
			if($prot_retorno = $_NFe->autoProtNFe()){
				$total_notas = count($prot_retorno);
				for($x=0; $x < $total_notas; $x++){

					#consumo indevido
					#echo $prot_retorno[$x][1]['cStat'].'==';
					#echo $prot_retorno[$x][1]['xMotivo'];
					#ALTERA STATUS PARA AUTORIZADA OU REJEITADA
					//if(isset($prot_retorno[0]['cStat'])){
					if($prot_retorno[$x][1]['cStat'] == '656'){
						$retorno .= $prot_retorno[$x][1]['cStat'].'='.$prot_retorno[$x][1]['xMotivo'].',';
					}elseif(isset($prot_retorno[$x]['cStat'])){
						//if($prot_retorno[0]['cStat'] > 199 || $prot_retorno[$x][1]['cStat'] == '656'){
							//echo $prot_retorno[$x]['cStat'];
							//echo 'teste ';
							//print_r($prot_retorno[$x]);
						if($prot_retorno[$x]['cStat'] > 199){
							$rejeicao_motivo = $prot_retorno[$x]['xMotivo'];
							$nfe_status = '5'; // rejeitada
						//}elseif( $prot_retorno[0]['cStat'] == '100'){
						}elseif( $prot_retorno[$x]['cStat'] == '100'){
							$nfe_status = '4'; //aprovada
						}
						$nfe_chave  = $prot_retorno[$x]['nfepath'];
						mysql_query('UPDATE tblpedido_fiscal SET  fldnfe_status_id = "'.$nfe_status.'", fldnfe_protocolo = "'.$prot_retorno[$x]['nProt'] .'" WHERE fldchave = "'.$nfe_chave.'"');
						$nfe = $prot_retorno[$x]['nNfe'];
						$id  = substr($nfe_chave, 35, 8);
						$retorno .= (int)$id.'='.$nfe_status.',';
						if($rejeicao_motivo){
							$motivo	 .=	'NF-e '.$nfe.' = '.$rejeicao_motivo;
						}
						
					}
				}
			}
		}
		
		$xml = "\t\t<retorno>$retorno</retorno>\n";
		$xml .= "\t\t<motivo>$motivo</motivo>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n"; 
		
	}elseif($_GET['action'] == 'nfeProt'){ #VERIFICO UMA UNICA NOTA
	
		$nfe_id 	= $_GET['nfe_id'];
		$rsChave 	= mysql_query('SELECT fldnfe_recibo, fldchave FROM tblpedido_fiscal WHERE fldPedido_Id = '.$nfe_id);
		$rowChave 	= mysql_fetch_array($rsChave);
		echo mysql_error();
		$chave_nfe 	= $rowChave['fldchave'];
		$file_name 	= $rowChave['fldchave'].'-nfe.xml';
		
		####################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
		$modSOAP = '2'; //usando cURL
		$recibo = $rowChave['fldnfe_recibo']; //este é o numero do seu recibo mude antes de executar este script
		$chave 	= '';
		
		if ($prot_retorno = $nfe->getProtocol($recibo, $chave, $tpAmb, $modSOAP)){
			//echo $prot_retorno;
			//houve retorno mostrar dados
			$msg_retorno  = "\n[cStat] 	= ".$prot_retorno['cStat']."\n";
			$msg_retorno .= "[xMotivo] 	= ".$prot_retorno['xMotivo']."\n";
			if(isset($prot_retorno['aProt'][0]['cStat'])){
				$msg_retorno .= "[cStat] 	= ".$prot_retorno['aProt'][0]['cStat']."\n";
				$msg_retorno .= "[xMotivo] 	= ".$prot_retorno['aProt'][0]['xMotivo'];
			}
			//$msg_retorno = preg_replace("/\r?\n/", "\\n", addslashes($msg_retorno));
		} else {
			//não houve retorno mostrar erro de comunicação
			$msg_retorno  = $nfe->errMsg;
		}
		
		$old_file 		= '../nfe_myerp/'.$ambiente.'/enviadas/'.$chave_nfe.'-nfe.xml';
		$protFile 		= '../nfe_myerp/'.$ambiente.'/temporarias/'.$chave_nfe.'-prot.xml';
		//adiciona protocolo de retorno
			
		if($prot_retorno['aProt'][0]['cStat'] > 199 || $prot_retorno[$x][1]['cStat'] == '656'){
			$nfe_status = '5'; // rejeitada
			
			if(is_file($old_file)){
				$destino 		= '../nfe_myerp/'.$ambiente.'/enviadas/reprovadas/'.$chave_nfe.'-nfe.xml';
			}
		}elseif( $prot_retorno['aProt'][0]['cStat'] == '100'){
			$nfe_status = '4'; //aprovada
			if(is_file($old_file)){
				$destino 		= '../nfe_myerp/'.$ambiente.'/enviadas/aprovadas/'.$chave_nfe.'-nfe.xml';
			}
		}
		
		#GRAVANDO NUMERO DO PROTOCOLO NO BANCO
		mysql_query('UPDATE tblpedido_fiscal SET fldnfe_protocolo = "'.$prot_retorno['nProt'].'" WHERE fldchave = "'.$chave_nfe.'"');
		
		#ADICIONANDO PROTOCOLO DO SEFAZ
		$procnfe = $nfe->addProt($old_file,$protFile);
		 if ( file_put_contents($destino, $procnfe) ) {
			 if(is_file($destino)){
				//remover o arquivo antigo sem o protocolo
				unlink($old_file);
		 	}
		}
		if(isset($nfe_status)){
			mysql_query('UPDATE tblpedido_fiscal SET fldnfe_status_id = "'.$nfe_status.'" WHERE fldchave = "'.$chave_nfe.'"');
		}
		
		$xml = "\t\t<msg_retorno>$msg_retorno</msg_retorno>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n"; 
	
	
	}elseif($_GET['action'] == 'consulta_nfe_db'){
			
		$documento_id	= $_GET['documento_id'];
		$documento_tipo = ($_GET['documento_tipo'] == 'pedido') ? '1' : '2';
		$rsNFe  = mysql_query('SELECT fldchave FROM tblpedido_fiscal WHERE fldPedido_Id = '.$documento_id.' AND fldDocumento_Tipo = '.$documento_tipo);
		$rowNFe = mysql_fetch_array($rsNFe);
		
		$chave = $rowNFe['fldchave'];
		
		$tpAmb 		= fnc_sistema('nfe_ambiente_tipo');
		$ambiente 	= ($tpAmb == '1')? 'producao': 'homologacao' ;
			
		$file_name 		= $chave.'-nfe.xml';
		
		$dir_aprovada	= '../nfe_myerp/'.$ambiente.'/enviadas/aprovadas/'.$file_name;
		$dir_reprovada	= '../nfe_myerp/'.$ambiente.'/enviadas/reprovadas/'.$file_name;
		
		if(is_file($dir_aprovada)){
			mysql_query('UPDATE tblpedido_fiscal SET fldnfe_status_id = 4 WHERE fldPedido_Id = '.$pedido_id);
			$status = '4';
		}elseif(is_file($dir_reprovada)){
			mysql_query('UPDATE tblpedido_fiscal SET fldnfe_status_id = 5 WHERE fldPedido_Id = '.$pedido_id);
			$status = '5';
		}
	
		$xml = "\t\t<status>$status</status>\n";
		$xml .= "\t\t<documento_id>$documento_id</documento_id>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n"; 
	
	}
?>