<?php
	
	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
    
    //procedimento para inserir e atualizar registros
    if(isset($_POST['dados'])) {
		$data = $_POST['dados'];
		$fields = explode("&",$data);
		
		foreach($fields as $field) {
			$field_key_value = explode("=",$field);
			$key = urldecode($field_key_value[0]);
			$value = urldecode($field_key_value[1]);
			
			eval("$$key = \"$value\";");
		}
        
        //escapar a string vinda do usuário
        $txt_tipo_nome = mysql_real_escape_string($txt_tipo_nome);
        
        //gravar ou atualizar, caso exista um id de registro
        if(empty($hid_id)) $sql = "INSERT INTO tblcrm_acompanhamento_cliente_tipo VALUES(NULL, '". $txt_tipo_nome ."', 0)";
        else               $sql = "UPDATE tblcrm_acompanhamento_cliente_tipo SET fldTipo_Nome = '$txt_tipo_nome' WHERE fldId = $hid_id";
        
        mysql_query($sql);
	}
    
    //exibir os dados do registro na tela para fazer a edição
    if(isset($_POST['idRegistroEditar'])) {
        $rowAcompanhamentoTipo = mysql_fetch_array(mysql_query("SELECT * FROM tblcrm_acompanhamento_cliente_tipo WHERE fldId = " . $_POST['idRegistroEditar']));
    }
    
    //setar como excluído
    if(isset($_POST['idRegistroExcluir'])) {
        mysql_query("UPDATE tblcrm_acompanhamento_cliente_tipo SET fldExcluido = 1 WHERE fldId = " . $_POST['idRegistroExcluir']);
    }

?>

<div class="form" style="width:460px; padding: 0;">
    <form class="frm_detalhe" style="width: 440px" id="frm_crm_acompanhamento_cliente_tipo_novo" action="" method="post">
        <ul style="width:300px; margin: 7px 0 0 0; float:left;"> 
            <li>
                <label for="txt_tipo_nome">Tipo de acompanhamento</label>
                <input type="text" id="txt_tipo_nome" name="txt_tipo_nome" value="<?= (!empty($rowAcompanhamentoTipo)) ? $rowAcompanhamentoTipo['fldTipo_Nome'] : ''; ?>" />
            </li>
            <li>
                <input type="hidden" id="hid_id" name="hid_id" value="<?= (!empty($rowAcompanhamentoTipo)) ? $rowAcompanhamentoTipo['fldId'] : ''; ?>" />
            </li>
        </ul>
        
        <div style="float:right; margin: 20px 0 0 0;">
            <input type="submit" style="margin:0" class="btn_enviar atualizar_select" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
        </div>
    </form>
</div>

<?
    $sql = mysql_query("SELECT * FROM tblcrm_acompanhamento_cliente_tipo WHERE fldExcluido = 0 ORDER BY fldTipo_Nome");
    if(mysql_num_rows($sql) > 0) {
?>

    <div id="table" style="width:460px; margin-top: 20px;">
        <div id="table_container" style="width: 460px; height: 160px;">      
            <table id="table_general" class="table_general" summary="Tipos de acompanhamentos cadastrados!" style="width: 100%;">
<?			    $linha = "row";
                while($rowAcompanhamentoTipo = mysql_fetch_array($sql)) {
?>				<tr class="<?= $linha; ?>" style="width: 100%;">
                    <td style="width: 90%; padding: 0 0 0 10px;"><?=$rowAcompanhamentoTipo['fldTipo_Nome']?></td>
                    <td>
                        <a class="edit" title="Editar" href="<?=$rowAcompanhamentoTipo['fldId']?>"></a>
                    </td>
                    <td>
                        <a class="delete atualizar_select" title="Excluir" href="<?=$rowAcompanhamentoTipo['fldId']?>"></a>
                    </td>
                    </tr>
<?              $linha = ($linha == "row") ? "dif-row" : "row";
                }
?>		</table>
        </div>
    </div>

<?
    } //verificação de registro no banco
    else {
?>
    <h4 style="width: 450px; margin-top: 20px;">N&atilde;o h&aacute; Tipos de acompanhamento cadastrados!<br> Utilize o campo acima para cadastrar.</h4>
<?
    }
?>

<script type="text/javascript">
    
    $('input#txt_tipo_nome').focus();
    
    //inserir registro
    $('form#frm_crm_acompanhamento_cliente_tipo_novo').submit(function(e){
        e.preventDefault();
        
        var tipoNome = $(this).find('input#txt_tipo_nome').val();
        var retorno  = true; 
        
        if(tipoNome != '' && !(tipoNome.match(/^\s+$/))) {
            $('div.modal-conteudo:last').load('modal/crm_acompanhamento_cliente_tipo.php', { 'dados' : $(this).serialize() });
            retorno = true;
        }
        else {
            alert('Preencha o campo Tipo de acompanhamento!');
            $(this).find('input#txt_tipo_nome').focus();
            retorno = false;
        };
        
        if(retorno) { return true; }
        else { return false; }
    });
    
    //atualizar registro
    $('a.edit').click(function(e){
        e.preventDefault();
        
        $('div.modal-conteudo:last').load('modal/crm_acompanhamento_cliente_tipo.php', { 'idRegistroEditar' : $(this).attr('href') });
    });
    
    //excluir registro
    $('a.delete').click(function(e){
        e.preventDefault();
        
		var confirmacao = confirm('Confirma a exclusão do registro?');
		if(confirmacao == true) { $('div.modal-conteudo:last').load('modal/crm_acompanhamento_cliente_tipo.php', { 'idRegistroExcluir' : $(this).attr('href') }); }
    });
    
</script>