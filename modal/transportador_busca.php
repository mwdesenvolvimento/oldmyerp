<?
	ob_start();
	session_start();
?>

	<script type="text/javascript">
		//evita que o enter faça alguma acao					   
		$("input#buscar").keydown(function(event){
			if (event.keyCode == 13){
				return false;
			}			   
		});						   
		
		$('document').ready(function(){
				$('#buscar').focus();
				$('#buscar').select();
				$('#loading').hide();
				$('#buscar').click(function(){
						$('#buscar').val('');
				});
				
				$('#buscar').keypress(busca_keyPress);
				
				function busca_Scroll(deslocamento){
					//alert(deslocamento);
					var cursorAtual = parseInt($('#hid_busca_cursor').val());
					var cursorLimite = parseInt($('#hid_busca_controle').text() - 1);
					var mover = false;
					if(deslocamento<0){
						if(cursorAtual > 0){mover = true;}
					}
					else{
						if(cursorAtual < cursorLimite){mover = true;}
					}
					if(mover){
						$('a[title=busca_lista_item]').removeClass('cursor');
						cursorNovo = cursorAtual + deslocamento;
						//corrigir cursor se ultrapassar limites
						if(cursorNovo < 0){cursorNovo = 0;}
						if(cursorNovo > cursorLimite){cursorNovo = cursorLimite;}
						
						$('a[title=busca_lista_item]:eq('+cursorNovo+')').addClass('cursor');
						$('#hid_busca_cursor').val(cursorNovo);
						var strNome = $('li[title=nome]:eq('+cursorNovo+')').text();
						$('#buscar').val(strNome);
						
						//19 - altura da linha de exibição de cada registro
						$('#alvo').scrollTop($('#alvo').scrollTop() + 19 * deslocamento);
					}
					
				}
				
				function busca_keyPress (event) {
					console.log(this);
					//alert(event.keyCode);
					switch(event.keyCode){
						case 13: //enter
							var intCursor = $('#hid_busca_cursor').val();
							$('#parametro_transportador').load('modal/transportador_busca.php', {codigo_transportador: $('a[title=busca_lista_item]:eq('+intCursor+')').attr('href')} );
							$('div.modal-body').remove();
						case 38: //up
							busca_Scroll(-1);
							break;
							
						case 40: //down
							busca_Scroll(1);
							break;
							
						case 33: //page up
							busca_Scroll(-10);
							break;
							
						case 34: //page down
							busca_Scroll(10);
							break;
							
						default: //busca
							//reset da posição do cursor para acompanhar o scroll
							$('#hid_busca_cursor').val(0);
							
							
							$.post('modal/transportador_busca_consulta.php',
							{busca: $('#buscar').val()},
							function(data){
									if ($('#buscar').val()!=''){
											$('#alvo').show();
											$('#alvo').empty().html(data);
									}
									else{
											$('#alvo').empty();
									}
							});
							break;
					}
				}
			
			//pegar o id no href e retornar para essa mesma página via ajax o valor e gravar na sessão	
			$('a.selecionar').live('click',function(e){
				e.preventDefault();
				
				$('#parametro_transportador').load('modal/transportador_busca.php', {codigo_transportador: $(this).attr('href')} );
			});	
			
		});
	</script>
    	
	<div id="parametro_transportador">
<?
		if(isset($_POST['codigo_transportador'])){
			
			$_SESSION["transportador_codigo"] = $_POST['codigo_transportador'];
			unset($_POST['codigo_transportador']);
?>
			<script type="text/javascript">
				//$('div#modal-body').remove();
				$('div.modal-body:last').remove();
				$('input[name$="_transportador_codigo"]').focus();
			</script>
<?
			die();
		}
?>
	</div>
	
	<? require("../inc/con_db.php"); ?>

    <form id="frm_busca" name="frm_busca">
    <fieldset>
    	<legend>Busca de transportador</legend>
            <input type="text" id="buscar" name="buscar" value="Digite o nome do transportador" size="50" >
            <div id="loading"><img src="image/layout/carregando.gif"> </div>
            <ul id="busca_cabecalho">
            	<li style="width:60px">c&oacute;digo</li>
                <li style="width:230px">Nome</li>
                <li style="width:230px">Nome Fantasia</li>
                <li style="width:100px">Telefone</li>
            </ul>
			<input type="hidden" id="hid_busca_cursor" value="0" />
            <div id="alvo"></div>
    	</fieldset>
    </form>