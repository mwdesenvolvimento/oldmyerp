
    <p class="nfe_valido">Transmitindo NFe.</p>
    
    <form id="frm_nfe_assinar" class="frm_detalhe" action="" method="post">        	
        <textarea style="width:750px; height:180px" id="txa_erro" name="txa_erro" class="txa_nfe_erro">Gerando XML...</textarea>
        <input style="width:100px"  type="submit" id="btn_nfe_ok" name="btn_nfe_ok" value=" ok " />
    </form>
    
	<script language="javascript">
		$('a.modal-fechar:last').remove();
		$('#btn_nfe_ok').click(function(e){
			e.preventDefault();
			$('div.modal-body:last').remove();
		});
		
		$('#txa_erro').focus();
	</script>	
<?	
	require('../inc/con_db.php');
	require('../inc/fnc_general.php');
	require('../nfephp/libs/ConvertNFePHP.class.php');
	require('../nfephp/libs/ToolsNFePHP.class.php');
	require('../nfephp/libs/AutoToolsNFePHP.class.php');
	
	//show_prog_bar(200, rand(1, 100), 'blue', 'black');
	
	$_NFe = new AutoToolsNFePHP;

	$nfe_id = $_POST['params'][1];
	#TRANSFORMANDO EM TXT
	$documento_tipo = ($_POST['params'][2] == '2')? 'compra' : 'pedido';
	require('pedido_nfe_exportar.php');
	
####################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
	#CONVERTENDO TXT EM XML
	if (!$_NFe->autoTXTtoXML()){
		
		$erro = $_NFe->errMsg;
		$erro = preg_replace("/\r?\n/", "\\n", addslashes($erro));
		$msg_erro = '\n\nErro ao gerar XML.\n\n';
		$msg_erro .= $erro;
		
?>		<script language="javascript">
			var msg_erro = '<?= $msg_erro?>';
			$('#txa_erro').append(msg_erro);
		</script>
<?		die;
	}else{
		
		$msg_ok = 'OK\n';
		$msg_ok .= 'Assinando NFe...';
		
?>		<script language="javascript">
			var msg_ok = '<?= $msg_ok?>';
			$('#txa_erro').append(msg_ok);
		</script>
<?		
	}
	
####################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

	#Assinar XML
	#Pega os arquivos XML na pasta ENTRADAS e assina
	#apos assinados, os arquivos xml serao movidos para a pasta ASSINADAS
	if (!$_NFe->autoAssinaNFe()) {
		
		$msg_erro = '\n\nErro ao Assinar NFe.\n';
?>		<script language="javascript">

			var msg_erro = '<?= $msg_erro?>';
			$('#txa_erro').append(msg_erro);
		</script>
<?		die;

	}else{
		
		$msg_ok = 'OK\n';
		$msg_ok .= 'Validando NFe...';
		
?>		<script language="javascript">
			var msg_ok = '<?= $msg_ok?>';
			$('#txa_erro').append(msg_ok);
		</script>
<?
	}

####################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
	#Valida os arquivos XML da pasta ASSINADAS contra o schema XSD
	#Caso validados com sucesso serão movidos para a pasta VALIDADAS
	if (!$_NFe->autoValidNFe()) {
		
		$erro =  $_NFe->errMsg;
		$erro = preg_replace("/\r?\n/", "\\n", addslashes($erro));
		$msg_erro  = '\n\nErro ao Validar NFe.\n\n';
		$msg_erro .= $erro;
		
?>		<script language="javascript">
			 var  msg_erro = "<?=$msg_erro?>";
			$('#txa_erro').append(msg_erro);
		</script>
<?		die;

	}else{
		mysql_query('UPDATE tblpedido_fiscal SET fldchave = '.$chave_nfe.', fldnfe_Status_Id = "2" WHERE fldPedido_Id = '.$nfe_id);
		$msg_ok = 'OK\n';
		$msg_ok .= 'Enviando NFe...';
		
?>		<script language="javascript">
			var msg_ok = '<?= $msg_ok?>';
			$('#txa_erro').append(msg_ok);
		</script>
<? 		
	}

####################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
	
	#Enviar para a SEFAZ
	#Envia os arquivos da pasta VALIDADAS para a SEFAZ
	if(!$Recibo = $_NFe->autoSendNFe()) {
		//echo $_NFe->errMsg;
		$erro =  $_NFe->errMsg;
		$erro = preg_replace("/\r?\n/", "\\n", addslashes($erro));
		$msg_erro = '\n\nErro ao Enviar NFe.\n\n';
		$msg_erro .= $erro;
?>		<script language="javascript">
			var msg_erro = '<?= $msg_erro?>';
			$('#txa_erro').append(msg_erro);
		</script>
<?		die;
	}else{
		mysql_query("UPDATE tblpedido_fiscal SET fldnfe_recibo = '$Recibo', fldnfe_status_id = '3' WHERE fldPedido_Id = $nfe_id");
		$msg_ok = 'OK\n';
		$msg_ok .= 'Aguardando retorno do SEFAZ...';
		
?>		<script language="javascript">
			var msg_ok = '<?= $msg_ok?>';
			$('#txa_erro').append(msg_ok);
			var documento_tipo = '<?= $documento_tipo ?>';
			//mostra contagem regressiva para envio sefaz
			fnc_countdown();
			//envio para sefaz, a partir daqui se clicar em ok, recarrega pagina por causa dos status
			
			$('#btn_nfe_ok').click(function(e){
				e.preventDefault();
				window.location="index.php?p="+documento_tipo+"&modo=listar";	
			
			});
			
			
			$('a.modal-fechar').remove();
			
			//n fechar a janela modal ao teclar esc
			$('div.modal-body:last').keydown(function(event){
				if(event.keyCode == 27){
					return false;
				}
			});
		</script>
<?	}

####################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
	//sleep(10);
?>
<script language="javascript">
	
		setTimeout(function(){
			
			var nfe_id = '<?= $nfe_id?>';
			$.get('modal/pedido_nfe_consulta.php', {action:'nfeProt', nfe_id : nfe_id}, function(theXML){
				$('dados',theXML).each(function(){
				
					var retorno = $(this).find("msg_retorno").text();
					$('#txa_erro').append(retorno);
				});
			});
	
		}, 11000);
	</script>