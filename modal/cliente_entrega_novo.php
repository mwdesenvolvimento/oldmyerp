<?
	require("../inc/con_db.php");
	if(isset($_POST['params'][1])){
		$id 					= $_POST['params'][1];
		$rsClienteEntrega		= mysql_query("SELECT tblcliente_endereco_entrega.*,
		tbltransportador.fldId AS transportadora_id
		FROM tblcliente_endereco_entrega
		LEFT JOIN tbltransportador ON tblcliente_endereco_entrega.fldTransportador_Id = tbltransportador.fldId
		WHERE tblcliente_endereco_entrega.fldId = '$id'")
		or die (mysql_error());
		$rowClienteEntrega		= mysql_fetch_array($rsClienteEntrega);
		
		$UFCodigo = substr($rowClienteEntrega['fldMunicipio_Codigo'],0,2);
		$MunicipioCodigo = substr($rowClienteEntrega['fldMunicipio_Codigo'],2,5);
	}
?>

    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/init-validate.js"></script>
	<script type="text/javascript" src="js/myerp.js"></script>
	
    <form id="frm_adicional" class="frm_detalhe" style="width:700px;" action="" method="post">
        <ul>
            <li>
                <label for="txt_cliente_entrega_identificacao">Identifica&ccedil;&atilde;o</label>
                <input type="text" style="width:100px" id="txt_cliente_entrega_identificacao" name="txt_cliente_entrega_identificacao" value="<?=$rowClienteEntrega['fldIdentificacao']?>" />
            </li>
            <li>
                <label for="sel_cliente_entrega_transportador">Transportadora</label>
                <select name="sel_cliente_entrega_transportador" style="width: 120px;">
                    <option value="0">Selecionar</option>
<?					$rsTransportador = mysql_query("SELECT * FROM tbltransportador");
					while($rowTransportador = mysql_fetch_array($rsTransportador)){
?>						<option <?=($rowClienteEntrega['fldTransportador_Id'] == $rowTransportador['fldId'])? "selected='selected'" : '' ?> value="<?=$rowTransportador['fldId']?>"><?=$rowTransportador['fldNomeFantasia']?></option>;
<?					}
?>				</select>
            <li>
                <label for="txt_cliente_entrega_cpf_cnpj">CPF/CNPJ</label>
                <input type="text" style="width:120px;" id="txt_cliente_entrega_cpf_cnpj" name="txt_cliente_entrega_cpf_cnpj" value="<?=$rowClienteEntrega['fldCPF_CNPJ']?>" />
            </li>
            <li>
                <label for="txt_cliente_entrega_endereco">Endere&ccedil;o</label>
                <input type="text" style="width:310px;" id="txt_cliente_entrega_endereco" name="txt_cliente_entrega_endereco" value="<?=$rowClienteEntrega['fldEndereco']?>" />
            </li>
            <li>
                <label for="txt_cliente_entrega_bairro">Bairro</label>
                <input type="text" style=" width:123px;" id="txt_cliente_entrega_bairro" name="txt_cliente_entrega_bairro" value="<?=$rowClienteEntrega['fldBairro']?>" />
            </li>
            <li>
                <label for="txt_cliente_entrega_numero">N&uacute;mero</label>
                <input type="text" style=" width:60px;" id="txt_cliente_entrega_numero" name="txt_cliente_entrega_numero" value="<?=$rowClienteEntrega['fldNumero']?>" />
            </li>
            <li>
                <label for="txt_cliente_entrega_complemento">Complemento</label>
                <input type="text" style=" width:80px" id="txt_cliente_entrega_complemento" name="txt_cliente_entrega_complemento" value="<?=$rowClienteEntrega['fldComplemento']?>" />
            </li>
            <li>
                <label for="txt_cliente_entrega_cep">CEP</label>
                <input type="text" class="cep-mask" style=" width:90px" id="txt_cliente_entrega_cep" name="txt_cliente_entrega_cep" value="<?=$rowClienteEntrega['fldCep']?>" />
            </li>
            <li>
                <label for="sel_cliente_entrega_uf">UF</label>
                <select class="sel_uf" name="sel_cliente_entrega_uf" style="width: 70px;">
            	    <option value="0">UF</option>
<?					$rsUF = mysql_query("SELECT * FROM tblibge_uf");
					while($rowUF = mysql_fetch_array($rsUF)){
?>						<option <?=($UFCodigo == $rowUF['fldCodigo'])? "selected='selected'" : '' ?> value="<?=$rowUF['fldCodigo']?>"><?=$rowUF['fldSigla']?></option>;
<?					}
?>				</select>
			</li>
			<li>
				<label for="sel_cliente_entrega_municipio">Munic&iacute;pio</label>
				<select class="sel_municipio" name="sel_cliente_entrega_municipio">
<?					$rsMunicipio = mysql_query("SELECT * FROM tblibge_municipio");
					while($rowMunicipio = mysql_fetch_array($rsMunicipio)){
?>						<option <?=($MunicipioCodigo == $rowMunicipio['fldCodigo'] && $UFCodigo == $rowMunicipio['fldUF_Codigo'])? "selected='selected'" : '' ?> value="<?=$rowMunicipio['fldCodigo']?>"><?=$rowMunicipio['fldNome']?></option>;
<?					}
?>				</select>
		   	<li>
                <label for="sel_cliente_entrega_padrao">Tornar padr&atilde;o</label>
                <select name="sel_cliente_entrega_padrao" style="width: 100px;">
                    <option value="1" <?=($rowClienteEntrega['fldPadrao'] == 1)  ? 'selected="selected"' : "" ?>>Sim</option>
                    <option value="0" <?=($rowClienteEntrega['fldPadrao'] == 0)  ? 'selected="selected"' : "" ?>>N&atilde;o</option>
                </select>
            </li>
            <li>
                <?php if(isset($_POST['params'][1])) { ?>
                <input type="hidden" name="hid_edit" id="hid_edit" value="<?=$_POST['params'][1]?>" />
                <?php } ?>
                <input type="submit" class="btn_enviar" style="margin:14px 0 0 0;" name="btn_adicionar" id="btn_adicionar" value="gravar" title="gravar" />
            </li>
         </ul>
         <span style="text-align:left; font-size:11px; color:red; width:100%; display:block; margin:5px 0 0 5px">*obs: o endereço padrão será carregado automaticamente na nfe quando escolhido este cliente.</span>
    </form>				
		
	<script type="text/javascript">
            $('input[name=txt_cliente_entrega_identificacao]').focus();
    </script>