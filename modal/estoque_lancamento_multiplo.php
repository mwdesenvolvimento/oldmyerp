<script type="text/javascript" src="js/estoque_lancamento_multiplo.js"></script>

<form id="frm_estoque_lancamento_multiplo" action="" method="post" class="frm_detalhe">

    <ul style="width:790px">
        <li>
            <label for="txt_saida">Método</label>
            <select id="sel_metodo" style="width:75px;">
              <option value="entrada">Entrada</option>
              <option value="saida">Saida</option>
            </select>
        </li>
        <li>
            <label for="txt_quantidade">Quantidade</label>
            <input value="1" type="text" style="width:75px; text-align:right" id="txt_quantidade" name="txt_quantidade" />
        </li>
        <li>
            <label for="txt_produto_codigo">C&oacute;digo</label>
            <input type="text" class="codigo" style="width: 60px" id="txt_produto_codigo" name="txt_produto_codigo" value="" />
            <a href="produto_busca" title="Localizar" class="modal" rel="950-450">
              <img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
            <input type="hidden" id="hid_produto_id" name="hid_produto_id" value="" />
        </li>
        <li>
            <label for="txt_produto_nome">Produto</label>
            <input type="text" style="width:400px" id="txt_produto_nome" name="txt_produto_nome" />
        </li>
        <li>
            <button class="btn_sub_small" name="btn_item_inserir" id="btn_item_inserir" title="Inserir" >ok</button>
        </li>
    </ul>
    
    <span style="clear:both; display:block"></span>
    
    <div id="table" style="width:790px; margin-top:10px">
        <div id="table_cabecalho" style="width:790px">
            <ul class="table_cabecalho" style="width:790px">
                <li style="width:80px; text-align:center;">Cod</li>
                <li style="width:500px;">Desc</li>
                <li style="width:70px; text-align:center;">Entrada</li>
                <li style="width:70px; text-align:center;">Saida</li>
            </ul>
        </div>
        <div id="table_container" style="height:300px; width:790px">       
            <table id="table_general" class="table_general" summary="Lista de estoque">
                <tbody id="listagem">
                    <tr id="listagem_hidden" style="display:none">
                        <td style="width:80px; text-align:center" class="listagem_codigo"></td>
                        <td style="width:500px" class="listagem_desc"></td>
                        <td style="width:70px; text-align:center" class="listagem_entrada"></td>
                        <td style="width:70px; text-align:center" class="listagem_saida"></td>
                        <td style="width:34px"><a class="delete deletar_listagem" style="width:20px" title="Excluir" href="#"></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
    <div id="dados" style="display:none">
        <input type="text" id="txt_controle" value="0" />
        <div id="form_hidden">
            <input type="text" class="produto_id" />
            <input type="text" class="produto_entrada" value="0" />
            <input type="text" class="produto_saida" value="0" />
        </div>
    </div>
  
    <ul style="float:left; margin-top: 5px">
      <li id="totEntrada">Total entradas: 0</li>
      <li id="totSaida" style="margin-left: 30px;">Total saidas: 0</li>
    </ul>

    
    <ul style="float:right">
        <li style="margin: 0;"><input style="margin: 4px 0 0 0;" type="submit" class="btn_enviar" name="btn_estoque_lancamento_multiplo" id="btn_estoque_lancamento_multiplo" value="Aplicar" title="Aplicar" /></li>
    </ul>

</form>