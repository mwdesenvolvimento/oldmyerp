<?php
	ob_start();
	session_start();
?>
	<script type="text/javascript">
		//evita que o enter faça alguma acao		
		//NAO SEI PQ TEM ESSE BLOCO, COMENTADO 26/11/2012 - LUANA
		/*					   			   
		$("input#buscar").keydown(function(event){
			if (event.keyCode == 13){ return false;	}			   
		});						   
		*/
		$('document').ready(function(){
				$('#buscar').focus();
				//$('#buscar').select();
				//$('#buscar').click(function(){$('#buscar').val('');});

				$("#chk_busca_qualquer_parte").click(function(event){
					busca_keyPress(event);
					$('#buscar').focus();
				});
				
				$('#buscar').keyup(busca_keyPress);
				$('#buscar').bind('keydown', busca_teclas); //keypress permite rolar mas não funciona no chrome

				function busca_Scroll(deslocamento){
					//alert(deslocamento);
					var cursorAtual 	= parseInt($('#hid_busca_cursor').val());
					var cursorLimite 	= parseInt($('#hid_busca_controle').text() - 1);
					var mover = false;
					if(deslocamento<0){
						if(cursorAtual > 0){mover = true;}
					}else{
						if(cursorAtual < cursorLimite){mover = true;}
					}
					if(mover){
						$('a[title=busca_lista_cliente]').removeClass('cursor');
						cursorNovo = cursorAtual + deslocamento;
						//corrigir cursor se ultrapassar limites
						if(cursorNovo < 0){cursorNovo = 0;}
						if(cursorNovo > cursorLimite){cursorNovo = cursorLimite;}
						
						$('a[title=busca_lista_cliente]:eq('+cursorNovo+')').addClass('cursor');
						$('#hid_busca_cursor').val(cursorNovo);
						var strNome = $('li[title=nome]:eq('+cursorNovo+')').text();
						$('#buscar').val(strNome);
						
						//19 - altura da linha de exibição de cada registro
						$('#alvo').scrollTop($('#alvo').scrollTop() + 19 * deslocamento);
					}
				}

				function busca_teclas (event) {
					///console.log(this);
					//alert(event.keypress);
					switch(event.keyCode){
						case 13: //enter
							$('#buscar').focus();
							//alert($('#hid_busca_controle').text());
							//verificar se tem resultado na busca
							if($('#hid_busca_controle').text() == 0){
								alert('Atenção! Não há cliente selecionado!');
								return false;
							}
							else{
								var intCursor = $('#hid_busca_cursor').val();
								var cod_cliente =  $('a[title=busca_lista_cliente]:eq('+intCursor+')').attr('href');
								var placa_id = $('a[title=busca_lista_cliente]:eq('+intCursor+')').find('li#placa').attr('class');
								$('#parametro_cliente').load('modal/cliente_busca.php', {codigo_cliente: cod_cliente, placa_id: placa_id});
							}
							break;
						case 38: //up
						
							busca_Scroll(-1);
							break;
							
						case 40: //down
							busca_Scroll(1);
							break;
							
						case 33: //page up
							busca_Scroll(-10);
							break;
							
						case 34: //page down
							busca_Scroll(10);
							break;
						
					}
				}
				
				function busca_keyPress(event) {
					//console.log(this);
					//alert(event.keyCode);
					switch(event.keyCode){
						case 13: //enter
						case 38: //up
						case 40: //down
						case 33: //page up
						case 34: //page down
							break;

						default: //busca
							//reset da posição do cursor para acompanhar o scroll
							$('#hid_busca_cursor').val(0);

							bolQualquerParte = $("#chk_busca_qualquer_parte").is(":checked");

							if(bolQualquerParte){
							strBusca = '%' + $('#buscar').val() + '%';
							}
							else{
								strBusca = $('#buscar').val() + '%';
							}

							$.post('modal/cliente_busca_consulta.php',
							{busca: strBusca, rad:$('input:radio[name=rad_busca]:checked').val()},
							function(data){
								if ($('#buscar').val()!=''){
									$('#alvo').show();
									$('#alvo').empty().html(data);
								}else{
									$('#alvo').empty();
								}
							});
							break;
					}
				}
			
			//pegar o id no href e retornar para essa mesma página via ajax o valor e gravar na sessão	
			$('a.selecionar').live('click',function(e){
				e.preventDefault();
				placa_id = $(this).find('li#placa').attr('class');
				$('#parametro_cliente').load('modal/cliente_busca.php', {codigo_cliente: $(this).attr('href'), placa_id: placa_id});
			});	
		});
		
		
		$('#btn_cliente_novo').live('click', function(){
			$('.frm_busca_cliente').parents('.modal-body').remove();
		});
		
		
	</script>
    	
	<div id="parametro_cliente">
<?		if(isset($_POST['codigo_cliente'])){
			$_SESSION['cliente_placa_id'] = $_POST['placa_id'];
?>
			<script type="text/javascript">	
				codigo_cliente = "<?= $_POST['codigo_cliente']?>";
				$('input#txt_cliente_codigo').val(codigo_cliente);
				$('input#txt_cliente_codigo').focus();
				$('input#txt_cliente_codigo').change();
				$('#txt_cliente_nome').focus();
				$('.frm_busca_cliente').parents('.modal-body').remove();
				
				//var foco_retorno = $('a.modal-fechar:last').attr('rel');
				//alert(foco_retorno);
				//$('#txt_cliente_codigo').focus();
				
				
			</script>
<?		}
?>	</div>
	
<? require("../inc/con_db.php"); ?>

    <form id="frm_busca" name="frm_busca" class="frm_busca_cliente" style="width:940px">
    <fieldset>
    	<legend>Busca de cliente</legend>
           	<ul>
            	<li style="float:left" >
                	<input style="float:left; width:450px" type="text" id="buscar" name="buscar" value="Digite o nome do cliente" size="50" >
                </li>
                <li style="float:left; margin:13px 0 0 5px">
                	<input type="checkbox" style="width:auto; height:auto; margin:2px 5px 0 0; float:left;" id="chk_busca_qualquer_parte" name="chk_busca_qualquer_parte">
                	<label style="font-size:12px;" for="chk_busca_qualquer_parte">qualquer parte do campo</label>
                </li>
                <li style="float:right; margin-right:10px">
                	<a id="btn_cliente_novo" class="modal btn_novo_small" href="cliente_cadastro_rapido,cliente_codigo" title="novo" rel="750-380" ></a>
                </li>
			</ul>

			<ul style="clear:both; margin:0 0 0 10px">
                <li style="width: 70px; margin-top:0px; float:left;">
                    <input type="radio" style="width:auto; height:auto; margin:1px 3px 0 0; float:left" id="rad_busca_nome" name="rad_busca" value="nome" checked="checked" />
					<label style="font-size:12px;" for="rad_busca_nome">Nome</label>
                </li>
<?				if($_SESSION["sistema_tipo"] == "automotivo"){                
?>	            	<li style="width: 70px; margin:0; float:left;">
                    	<input type="radio" style="width:auto; height:auto; margin:1px 3px 0 0; float:left" id="rad_busca_placa" name="rad_busca" value="placa"/>
                    	<label style="font-size:12px" for="rad_busca_placa">Placa</label>
                    </li>
<?				}
?>              <li style="width: 90px; margin:0; float:left;">
                    <input type="radio" style="width:auto; height:auto; margin:1px 3px 0 0; float:left" id="rad_busca_telefone" name="rad_busca" value="telefone" />
                    <label style="font-size:12px" for="rad_busca_telefone">Telefone</label>
                </li>
                <li style="width: 60px; margin:0; float:left;">
                    <input type="radio" style="width:auto; height:auto; margin:1px 3px 0 0; float:left" id="rad_busca_cpf" name="rad_busca" value="cpf" />
                    <label style="font-size:12px" for="rad_busca_cpf">CPF</label>
                </li>
                <li style="width: 100px; margin:0; float:left;">
                    <input type="radio" style="width:auto; height:auto; margin:1px 3px 0 0; float:left" id="rad_busca_endereco" name="rad_busca" value="endereco" />
                    <label style="font-size:12px" for="rad_busca_endereco">Endere&ccedil;o</label>
                </li>
			</ul>
            
			<span style="clear:both; display:block"></span>

            <ul id="busca_cabecalho" style="width:940px; margin-top:10px;">
            	<li style="width:80px">C&oacute;digo</li>
                <li style="width:200px">Cliente</li>
                <li style="width:190px">Nome Fantasia</li>
                <li style="width:180px">Endere&ccedil;o</li>
                <li style="width:100px">Telefone 1</li>
                <li style="width:90px">CPF/CNPJ</li>
                <li style="width:90px"><?= ($_SESSION["sistema_tipo"] == "automotivo") ? 'Placa' : ''?></li>
            </ul>
			<input type="hidden" id="hid_busca_cursor" value="0" />
            <div id="alvo" style="width:940px; height:245px;"></div>
    	</fieldset>
    </form>