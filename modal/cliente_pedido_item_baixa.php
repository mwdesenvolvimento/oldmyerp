<?
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	require_once('../inc/fnc_financeiro.php');
	
	if(isset($_POST['form'])){
		
		$serialize 	= $_POST['form'];
		parse_str($serialize, $form);
		
		$cliente_id			 = $_POST['cliente_id']; 
		$item_id 			 = $form['hid_id'];
		$valor_produto_baixa = format_number_in($form['txt_valor_baixa']);
		$qtd_produto_baixa 	 = format_number_in($form['txt_quantidade_baixa']);
		$data				 = date("Y-m-d");
		
		mysql_query("INSERT INTO tblpedido_item_pago (fldQuantidade, fldItem_Id, fldDataPago, fldValorPago) VALUES ($qtd_produto_baixa, $item_id, '$data', $valor_produto_baixa)");
		$LastId 		= mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID"));
		$item_pago_id 	= $LastId['lastID'];
		
		$rsParcela 	= mysql_query("SELECT 	SUM((tblpedido_parcela_baixa.fldValor) * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa, 
											tblpedido_parcela.fldValor, tblpedido_parcela.fldPagamento_Id, tblpedido_parcela.fldPedido_Id, tblpedido_parcela.fldId as fldParcelaId
											FROM tblpedido_parcela 
											LEFT JOIN tblpedido_item ON tblpedido_parcela.fldPedido_Id = tblpedido_item.fldPedido_Id
											LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
											WHERE tblpedido_item.fldId = $item_id GROUP BY tblpedido_parcela.fldId ORDER BY tblpedido_parcela.fldParcela ");
		
		while($rowParcela = mysql_fetch_array($rsParcela)){
			$valorParcela 			= $rowParcela['fldValor'];
			$valorBaixa 			= $rowParcela['fldValorBaixa'];
			$valorRestanteParcela 	= $valorParcela - $valorBaixa;
			
			if($valor_produto_baixa > 0){
				if($valorRestanteParcela > 0){
					
					if($valor_produto_baixa	 > $valorRestanteParcela){
						$valorBaixa 		 = $valorRestanteParcela;
						$valor_produto_baixa = $valor_produto_baixa - $valorRestanteParcela;
						$devedor			 = '0';
					}else{
						$valorBaixa 		 = $valor_produto_baixa;
						$devedor			 = $valorRestanteParcela - $valor_produto_baixa;
						$valor_produto_baixa = 0;
					}
					
					$data 		= date('Y-m-d');
					
					$sqlInsert 	= mysql_query("INSERT INTO tblpedido_parcela_baixa
					(fldValor, fldJuros, fldMulta, fldDesconto, fldDevedor, fldDataCadastro, fldDataRecebido, fldParcela_Id, fldItem_Pago_Id) VALUES(
					'$valorBaixa',
					'0.00',
					'0.00',
					'0.00',
					'$devedor',
					'$data',
					'$data',
					'".$rowParcela['fldParcelaId']."',
					'$item_pago_id'
					)");
					$LastId 		= mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID"));
					$pagamento_id	= $rowParcela['fldPagamento_Id'];
					$rsPagamento 	= mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldId = $pagamento_id");
					$rowPagamento 	= mysql_fetch_array($rsPagamento);
					$conta 			= fnc_sistema("pedido_parcela_conta_".$rowPagamento['fldSigla']);
				
					$data 	 		= fnc_caixa_data($conta);
					$hora 			= date("H:i:s");
					$usuario_id 	= $_SESSION["usuario_id"];
					
					fnc_financeiro_conta_fluxo_lancar('', $valorBaixa, 0, '', $pagamento_id, $LastId['lastID'], 3, '', $conta);
						
					
				}
			}
		}
		
?>
		<script type="text/javascript">
			var cliente_id 	= <?= $cliente_id?>;
			window.location = "?p=cliente_detalhe&id="+cliente_id+"&modo=pedido&aba=item";
			$('#frm_quantidade_baixa').parents('.modal-body').remove();
		</script>
<?	}
	
	if(isset($_POST['params'])){
		//buscando os dados
		$dados = mysql_fetch_array(mysql_query("SELECT tblpedido_item.fldDescricao, tblpedido_item.fldValor, tblpedido_item.fldPedido_Id, tblpedido_item.fldQuantidade,
											   		SUM((tblpedido_item_pago.fldQuantidade) * (tblpedido_item_pago.fldExcluido * -1 + 1)) as fldQuantidadePaga
													FROM tblpedido_item 
													LEFT JOIN tblpedido_item_pago ON tblpedido_item.fldId = tblpedido_item_pago.fldItem_Id
													WHERE tblpedido_item.fldId = " . $_POST['params'][1]));
		echo mysql_error();
		#FACO UMA BUSCA COM O RESTANTE QUE TEM A RECEBER DAS PARCELAS DA VENDA, CASO O CLIENTE TENHA DADO BAIXA NAS PARCELAS, MAS QUEIRA DAR BAIXA NO ITEM EM SI, PRA NAO BUSCAR O VALOR DO ITEM QUE NA VERDADE JAH FOI DADO BAIXA.
		$rowParcela = mysql_fetch_array(mysql_query("SELECT SUM((tblpedido_parcela_baixa.fldValor) * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa, 
															(SELECT SUM(tblpedido_parcela.fldValor 	* (tblpedido_parcela.fldExcluido * -1 + 1)) FROM
																tblpedido_parcela WHERE tblpedido_parcela.fldPedido_Id = ".$dados['fldPedido_Id']."
															) AS fldValorPedido
															FROM tblpedido_parcela 
															LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
															WHERE tblpedido_parcela.fldPedido_Id = ".$dados['fldPedido_Id']));
		
		$restante 	 		= $rowParcela['fldValorPedido'] - $rowParcela['fldValorBaixa'];
		$valorRestante 		= ($dados['fldQuantidade'] 		- $dados['fldQuantidadePaga']) * $dados['fldValor'];
		
		$valor_baixa 		= ($restante >= $valorRestante)	? $valorRestante : $restante;
		$quantidadeRestante = $dados['fldQuantidade'] - $dados['fldQuantidadePaga'];
		$cliente_id  		= $_POST['params'][2];
		
	}
	
?>

<form id="frm_quantidade_baixa" action="" method="post" class="frm_detalhe">
	<h4 style="width:400px;margin-bottom:20px"><?=$dados['fldDescricao']?></h4>
	<ul style="width:280px;margin:0 auto;float:none">
		<li>
			<label for="txt_quantidade_baixa">Quantidade</label>
			<input type="text" id="txt_quantidade_baixa" name="txt_quantidade_baixa" style="width:100px;height:40px;text-align:center;font-size:25px;background-color:#FDEEEE" value="<?=format_number_out($quantidadeRestante)?>" />
			<input type="hidden" id="hid_id" 			name="hid_id" value="<?=$_POST['params'][1]?>" />
        </li>
        <li style="float:right">
			<label for="txt_valor_baixa">Valor</label>
			<input type="text" id="txt_valor_baixa" name="txt_valor_baixa" style="width:100px;height:40px;text-align:center;font-size:25px;background-color:#FFC" value="<?=format_number_out($valor_baixa)?>" />
		</li>
		<li style="float:right"><input type="submit" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="Baixar" title="Baixar" /></li>
	</ul>
	
</form>

<script type="text/javascript">
	
	$('#txt_quantidade_baixa').select();
	
	var totalQuantidade = '<?=$quantidadeRestante?>';
	var totalValor		= '<?=$valor_baixa?>';
	var itemValor		= '<?=$dados['fldValor']?>';
	var cliente_id		= '<?=$cliente_id?>';
	
	var totalQuantidade = Number(totalQuantidade).toFixed(2);
	var totalValor 		= Number(totalValor).toFixed(2);
	var itemValor 		= Number(itemValor).toFixed(2);

	
	$('#txt_quantidade_baixa').change(function(){
		var quantidade 	= br2float(float2br($(this).val())).toFixed(2);
		if(Number(quantidade) > Number(totalQuantidade) || quantidade == 0){
			quantidade = totalQuantidade;
		}
	
		var valor = quantidade * itemValor;
		if(Number(valor) > Number(totalValor)){
			valor = totalValor;
		}
		$('#txt_quantidade_baixa').val(float2br(quantidade));
		$('#txt_valor_baixa').val(float2br(valor.toFixed(2)));
		
	});

	$('#txt_valor_baixa').change(function(){
		var valor 	= br2float(float2br($(this).val())).toFixed(2);
		
		if(Number(valor) > Number(totalValor)){
			valor = totalValor;
		}
		
		var quantidade = Math.ceil(valor / itemValor);
		if(Number(quantidade) == 0 || Number(quantidade) > Number(totalQuantidade)){
			quantidade = totalQuantidade;
		}
		$('#txt_valor_baixa').val(float2br(valor));
		$('#txt_quantidade_baixa').val(float2br(quantidade.toFixed(2)));
	});

	$('#frm_quantidade_baixa').submit(function(){
		var form 	= $(this).serialize();
		$('div.modal-conteudo').load('modal/cliente_pedido_item_baixa.php', {form : form, cliente_id : cliente_id});
		return false;
	});

</script>
