
	<p class="nfe_valido">Carta de Corre&ccedil;&atilde;o NF-e.</p>
<?	
	date_default_timezone_set('America/Sao_Paulo'); //add 14-8-8, timezone dava problema as vezes
	ob_start();
	session_start();
	header('Content-type: text/html; charset=UTF-8');
	require('../inc/con_db.php');
	require('../inc/fnc_general.php');
	require_once('../nfephp/libs/ToolsNFePHP.class.php');
		
	if(isset($_POST['form'])){
		
		$campo 		= $_POST['campo'];
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$nfe_chave 		=  $form['hid_nfe_chave'];
		$correcao		=  $form['txa_nfe_correcao'];
		$sequencia		=  $form['txt_nfe_sequencia'];
		$correcao_data	=  explode(' ', $form['txt_data_hora']);
		$correcao_hora	=  $correcao_data[1];
		$correcao_data	=  format_date_in($correcao_data[0]);
		$correcao_data	=  $correcao_data.' '.$correcao_hora;
		
		$rsFiscal 		= mysql_query("SELECT fldId, fldambiente FROM tblpedido_fiscal WHERE fldchave = '$nfe_chave'");
		$rowFiscal 		= mysql_fetch_array($rsFiscal);
		$pedido_fiscal_id = $rowFiscal['fldId'];
		$tpAmb 			= $rowFiscal['fldambiente'];
		$modSOAP 		= '2';
				
		$nfe  	 = new ToolsNFePHP;
		$return  = $nfe->envCCe($nfe_chave, $correcao, $sequencia, $tpAmb, $modSOAP);
		$msg 	 = $nfe->errMsg;
		if($return == '135'){ #AUTORIZADA
			mysql_query("INSERT INTO tblpedido_fiscal_carta_correcao (fldCorrecao, fldSequencia, fldPedido_Fiscal_Id, fldData_Hora) VALUES('$correcao', '$sequencia', '$pedido_fiscal_id', '$correcao_data')");
			echo mysql_error();
			$msg  = "[cStat]   = 135";
			$msg .= "\n[xMotivo] = Evento registrado e vinculado a NF-e.";
		}
		
		#####################################################################################################
		
		$msg = preg_replace("/\r?\n/", "\\n", addslashes($msg));
?>		<script language="javascript">
			var msg_erro = '<?= $msg?>';
			$('#txa_nfe_retorno').append(msg_erro);
			$('#txa_nfe_retorno').focus();
		</script>

        <form id="frm_nfe_carta_correcao_retorno" class="frm_detalhe" action="" method="post" style="width:480px">    
			<textarea style="width:480px; height:250px" id="txa_nfe_retorno" name="txa_nfe_retorno" readonly="readonly" class="txa_nfe_erro"></textarea>
            <input style="width:100px"  type="submit" id="btn_nfe_ok" name="btn_nfe_ok" value=" ok " />
        </form>
        
        <script language="javascript">
			
			$('#btn_nfe_ok').click(function(e){
				e.preventDefault();
				window.location="index.php?p=pedido&modo=listar";	
			});
			
			$('a.modal-fechar').remove();
			//n fechar a janela modal ao teclar esc
			$('div.modal-body:last').keydown(function(event){
				if(event.keyCode == 27){
					return false;
				}
			});
		</script>
        
<?		die;	
	}
	
	$chave = $_POST['params'][1];
	$sSQL			= 'SELECT fldSequencia FROM tblpedido_fiscal_carta_correcao 
									INNER JOIN tblpedido_fiscal ON tblpedido_fiscal_carta_correcao.fldPedido_Fiscal_Id = tblpedido_fiscal.fldId
									WHERE fldchave ="'.$chave.'" ORDER BY tblpedido_fiscal_carta_correcao.fldId DESC LIMIT 1';
	$rsSequencia 	= mysql_query($sSQL);
	$rowSequencia	= mysql_fetch_assoc($rsSequencia);
	echo mysql_error();
	$sequencia		= $rowSequencia['fldSequencia'];
	$correcao_sequencia = ($sequencia > 0) ? $sequencia+1 : 1;
?>
    <form id="frm_nfe_carta_correcao" class="frm_detalhe" action="" method="post" style="width:480px">
	    <input type="hidden" name="hid_nfe_chave" id="hid_nfe_chave" value="<?=$chave?>" />   
    	<ul>
        	<li>
            	<label for="txt_data_hora">Data e Hora de Emiss&atilde;ao</label>
            	<input style="width:200px;text-align:right;font-weight:bold" name="txt_data_hora" id="txt_data_hora" value="<?=date("d/m/Y H:i:s")?>" />
            </li> 
        	<li>
            	<label for="txt_nfe_sequencia">Sequencia</label>
            	<input style="width:80px;text-align:right;font-weight:bold" name="txt_nfe_sequencia" id="txt_nfe_sequencia" value="<?=$correcao_sequencia?>" />
            </li>    	
	        <li>
            	<label for="txa_nfe_correcao">Corre&ccedil;&atilde;o</label>
                <textarea style="width:580px; height:200px" id="txa_nfe_correcao" name="txa_nfe_correcao" maxlength="255" ></textarea>
                <span style="font-size: 10px; font-style: italic;">(15 a 1000 caracteres)</span>
			</li>
            <li style=" width:590px">
            	<button style="float:right;margin:0" class="btn_gravar" id="btn_enviar" name="btn_enviar"> enviar</button>
            </li>
            
            <li class="img_load" style="margin-left:250px;display:none">
            	<img src="image/layout/carregando.gif" alt="carregando..." /><br />
            </li>
		</ul>        
    </form>
    
  
	<script type="text/javascript">
	
        $('#txt_nfe_chave').focus();
		
		$('#frm_nfe_carta_correcao').submit(function(e){
			e.preventDefault();
			
			$('li.img_load').show();
			var form 	= $(this).serialize();
			
			$('div.modal-conteudo').load('modal/pedido_nfe_carta_correcao.php', {form : form});
		});
		
	</script>