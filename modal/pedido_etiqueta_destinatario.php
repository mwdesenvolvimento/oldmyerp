<? 

    if($_GET['tipo'] == 'etiquetaImprimir'){
        $destinatario               = $_POST['txtDestinatario'];
        $destinatario2              = $_POST['txtDestinatario'];

        $destinatarioEndereco       = $_POST['txtDestinatarioEndereco'];
        $destinatarioEndereco2      = $_POST['txtDestinatarioEndereco'];
        $destinatarioEndereco3      = $_POST['txtDestinatarioEndereco'];

        $destinatarioBairro         = $_POST['txtDestinatarioBairro'];
        $destinatarioBairro2        = $_POST['txtDestinatarioBairro'];

        $destinatarioCep            = $_POST['txtDestinatarioCep'];

        $destinatarioCidade         = $_POST['txtDestinatarioCidade'];
        $destinatarioCidade2        = $_POST['txtDestinatarioCidade'];

        $destinatarioEstado         = $_POST['txtDestinatarioEstado'];

        $destinatarioObservacao     = $_POST['txtDestinatarioObservacao'];
        $destinatarioObservacao     = $_POST['txtDestinatarioObservacao'];
        $destinatarioObservacao2    = $_POST['txtDestinatarioObservacao'];

        $imprimirQuantidade         = $_POST['txtImprimirQuantidade'];

        require_once("../inc/con_db.php");
        require_once("../inc/fnc_identificacao.php");
        $impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
        if(!$impressao_local){ $impressao_local = fnc_estacao_impressora('todos'); }

        $timestamp  = md5(date("Ymd_His").rand(10,100));
        $local_file = "../impressao///inbox///imprimir_etiqueta_zebra_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
        $fp         = fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.

        $comando  = "";
        $comando .= $impressao_local."\r\n";  #\\NOME_DO_PC\COMPARTILHAMENTO IMPRESSORA
        $comando .= "\r\n";
        $comando .= "N\r\n";
        $comando .= "q900\r\n";
        $comando .= "Q224,01\r\n";
        $comando .= "R0,0\r\n";
        $comando .= "ZT\r\n";
        
        $comando .= "A795,165,2,4,1,1,N,DESTINATARIO.. ".$destinatario."\r\n";
        $comando .= "A795,165,2,4,1,1,N,".$destinatario2."\r\n";

        $comando .= "A795,165,2,4,1,1,N,ENDERECO...... ".$destinatarioEndereco."\r\n";
        $comando .= "A795,165,2,4,1,1,N,".$destinatarioEndereco2."\r\n";
        $comando .= "A795,165,2,4,1,1,N,".$destinatarioEndereco3."\r\n";

        $comando .= "A795,165,2,4,1,1,N,BAIRRO........ ".$destinatarioBairro."\r\n";
        $comando .= "A795,165,2,4,1,1,N,".$destinatarioBairro2."\r\n";

        $comando .= "A795,165,2,4,1,1,N,CEP........... ".$destinatarioCep."\r\n";

        $comando .= "A795,165,2,4,1,1,N,CIDADE........ ".$destinatarioCidade."\r\n";
        $comando .= "A795,165,2,4,1,1,N,".$destinatarioCidade2."\r\n";

        $comando .= "A795,165,2,4,1,1,N,ESTADO........ ".$destinatarioEstado."\r\n";

        $comando .= "A795,165,2,4,1,1,N,OBSERVACAO.... ".$destinatarioObservacao."\r\n";
        $comando .= "A795,165,2,4,1,1,N,".$destinatarioObservacao2."\r\n";

        $comando .= "P".$imprimirQuantidade; //qtd impressao

        $salva    = fwrite($fp, $comando);
        fclose($fp);

        if($salva){
            echo 'Imprimindo...';
        } else {
            echo 'Erro ao salvar arquivo';
        }

        die(); //impedir que carregue o restante
    }

    if($_GET['tipo'] == 'carregarFormulario'){
        
        require("../inc/con_db.php");
        $clienteId          = $_POST['clienteId'];
        $consultaCliente    = mysql_query("SELECT 
                            tblcliente.fldNome, CONCAT(tblcliente.fldEndereco, ', ', tblcliente.fldNumero) as fldEndereco, tblcliente.fldBairro, tblcliente.fldCEP,
                            tblibge_municipio.fldNome as fldCidade, tblibge_uf.fldNome as fldEstado
                            FROM tblcliente
                            LEFT JOIN tblibge_municipio ON substr(tblcliente.fldMunicipio_Codigo, 3) = tblibge_municipio.fldCodigo
                            LEFT JOIN tblibge_uf        ON tblibge_municipio.fldUF_Codigo = tblibge_uf.fldCodigo
                            WHERE tblcliente.fldId = '$clienteId'");
        $consultaCliente    = mysql_fetch_assoc($consultaCliente); ?>

        <script>
            $(document).ready(function(){
                $('#frmEtiquetaDestinatario').submit(function(e){ 
                    e.preventDefault(); 
                    //para nao deixar recarregar a pagina, e etc

                    var dados = $(this).serialize();
                     $.ajax({
                        type: "POST",
                        url: "modal/pedido_etiqueta_destinatario.php?tipo=etiquetaImprimir",
                        data: dados,
                        success: function(data){
                            alert(data);
                        }
                    });
                });
            });
        </script>

        <form id="frmEtiquetaDestinatario" action="paginaquenaoexiste.php" method="post" class="frm_detalhe" style="margin-left:5px">
            <ul>
                <li>
                    <label for="txtDestinatario">Destinat&aacute;rio</label>
                    <input value="<?=$consultaCliente['fldNome']?>" style="width:375px" type="text" id="txtDestinatario" name="txtDestinatario" />
                </li>
                <li>
                    <label for="txtDestinatarioEndereco">Endere&ccedil;o</label>
                    <input value="<?=$consultaCliente['fldEndereco']?>" style="width:375px" type="text" id="txtDestinatarioEndereco" name="txtDestinatarioEndereco" />
                </li>
                <li>
                    <label for="txtDestinatarioBairro">Bairro</label>
                    <input value="<?=$consultaCliente['fldBairro']?>" style="width:375px" type="text" id="txtDestinatarioBairro" name="txtDestinatarioBairro" />
                </li>
                <li>
                    <label for="txtDestinatarioCep">CEP</label>
                    <input value="<?=$consultaCliente['fldCEP']?>" style="width:375px" type="text" id="txtDestinatarioCep" name="txtDestinatarioCep" />
                </li>
                <li>
                    <label for="txtDestinatarioCidade">Cidade</label>
                    <input value="<?=$consultaCliente['fldCidade']?>" style="width:375px" type="text" id="txtDestinatarioCidade" name="txtDestinatarioCidade" />
                </li>
                <li>
                    <label for="txtDestinatarioEstado">Estado</label>
                    <input value="<?=$consultaCliente['fldEstado']?>" style="width:375px" type="text" id="txtDestinatarioEstado" name="txtDestinatarioEstado" />
                </li>
                <li>
                    <label for="txtDestinatarioObservacao">Observa&ccedil;&atilde;o</label>
                    <textarea style="width:375px" name="txtDestinatarioObservacao" id="txtDestinatarioObservacao"></textarea>
                </li>
                <li style="float:right; margin:0 10px 0 0">
                    <div style="float: left">
                        <label for="txtImprimirQuantidade" style="font-size:10px">Quantidade a imprimir</label>
                        <input value="1" style="width:181px" type="text" id="txtImprimirQuantidade" name="txtImprimirQuantidade" />
                    </div>
                    <input type="submit" class="btn_enviar" name="btnEtiquetaImprimir" id="btnEtiquetaImprimir" value="Gerar" title="Gerar" />
                </li>
            </ul>
        </form>

<?    

        die(); //para n ficar recarregando!

    } ?>

<script>
    $(document).ready(function(){
        $('#view').load('modal/pedido_etiqueta_destinatario.php?tipo=carregarFormulario', {clienteId: $('#hid_cliente_id').val()} );
    });
</script>

<div id="view"></div>