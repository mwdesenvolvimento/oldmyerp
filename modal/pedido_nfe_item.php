	<script type="text/javascript" src="js/general.js"		 ></script>
	<script type="text/javascript" src="js/produto_fiscal.js"></script>
<?Php
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	require_once('../inc/fnc_nfe.php');
	
	$produto_id 	= $_POST['params'][1];
	$item_id 		= (isset($_POST['params'][2]))? $_POST['params'][2] : '0';
	$nfe_importar 	= (isset($_POST['params'][3]))? $_POST['params'][3] : '0';

	if($item_id > 0){
?>
		<script type="text/javascript">
			var n  					= '<?= $item_id ?>';
			var importar 			= '<?= $nfe_importar ?>';

			var itemCodigo			= $('[name=txt_item_codigo_'+n+']').val();
			var itemDescricao		= $('[name=txt_item_nome_'+n+']').val();
			var itemValor			= $('[name=txt_item_valor_'+n+']').val();
			var itemQuantidade		= $('[name=txt_item_quantidade_'+n+']').val();
			var itemDescontoPercent	= $('[name=txt_item_desconto_'+n+']').val();
			
			
			//COPIAR DADOS
			$('#txt_nfe_item_principal_codigo').val(itemCodigo);
			$('#txt_nfe_item_principal_descricao').val(itemDescricao);
			
			$('#txt_nfe_item_principal_valor_unitario_comercial').val(itemValor);
			$('#txt_nfe_item_principal_quantidade_comercial').val(itemQuantidade);
			
			// ### NFE! ###############################################################################################################
			//A ORDEM DE CARREGAR OS CAMPOS PODE INFLUENCIAR EM APAGAR OU NAO ALGUNS OUTROS, POR ISSO, MANTER ESSA A NAO SER QUE AJA ALGUMA ANORMALIDADE
			var itemNCM							= $('[name=txt_nfe_pedido_item_principal_ncm_'+n+']').val();
			var itemExTipi						= $('[name=txt_nfe_pedido_item_principal_ex_tipi_'+n+']').val();
			var itemCFOP						= $('[name=txt_nfe_pedido_item_principal_cfop_'+n+']').val();
			
			var itemUnidadeComercial			= $('[name=txt_nfe_pedido_item_principal_unidade_comercial_'+n+']').val();
			var itemUnidadeTributavel			= $('[name=txt_nfe_pedido_item_principal_unidade_tributavel_'+n+']').val();
			var itemQtdeTributavel				= $('[name=txt_nfe_pedido_item_principal_quantidade_tributavel_'+n+']').val();
			var itemValorUnidTributavel			= $('[name=txt_nfe_pedido_item_principal_valor_unidade_tributavel_'+n+']').val();
			var itemSeguro						= $('[name=txt_nfe_pedido_item_principal_total_seguro_'+n+']').val();
			
			var itemDesconto					= $('[name=txt_nfe_pedido_item_principal_desconto_'+n+']').val();
			var itemFrete						= $('[name=txt_nfe_pedido_item_principal_frete_'+n+']').val();
			var itemEAN							= $('[name=txt_nfe_pedido_item_principal_ean_'+n+']').val();
			var itemEANTributavel				= $('[name=txt_nfe_pedido_item_principal_ean_tributavel_'+n+']').val();
			var itemOutDespesasAcess			= $('[name=txt_nfe_pedido_item_principal_outras_despesas_acessorias_'+n+']').val();
			var itemTotalBruto					= $('[name=txt_nfe_pedido_item_principal_valor_total_bruto_'+n+']').val();
			var itemPedidoCompra				= $('[name=txt_nfe_pedido_item_principal_pedido_compra_'+n+']').val();
			var itemNumPedidoCompra				= $('[name=txt_nfe_pedido_item_principal_numero_item_pedido_compra_'+n+']').val();
			//var itemProdutoEspecifico			= $('[name=hid_nfe_pedido_item_principal_produto_especifico_'+n+']').val();
			var itemCheckTotalBruto				= $('[name=chk_nfe_pedido_item_principal_valor_total_bruto_'+n+']').attr('checked');
			
			var itemRadTributo					= $('[name=hid_nfe_tributo_rad_'+n+']').val();
			$('input:radio[name=rad_imposto]:[value='+itemRadTributo+']').attr('checked', true);
			
			var ICMSSituacaoTributaria			= $('[name=hid_nfe_pedido_item_tributos_icms_situacao_tributaria_'+n+']').val();
			var ICMSOrigem						= $('[name=hid_nfe_pedido_item_tributos_icms_origem_'+n+']').val();
			var ICMSAliquotaAplicCalculoCred	= $('[name=txt_nfe_pedido_item_tributos_icms_pCredSN_'+n+']').val();
			var ICMSCredPodeSerAproveitado		= $('[name=txt_nfe_pedido_item_tributos_icms_vCredICMSSN_'+n+']').val();
			
			var ICMSModDetermBC					= $('[name=hid_nfe_pedido_item_tributos_icms_modBC_'+n+']').val();
			var ICMSBC							= $('[name=txt_nfe_pedido_item_tributos_icms_vBC_'+n+']').val();
			
			var ICMSReducaoBC					= $('[name=txt_nfe_pedido_item_tributos_icms_pRedBC_'+n+']').val();
			
			var ICMSAliquota					= $('[name=txt_nfe_pedido_item_tributos_icms_pICMS_'+n+']').val();
			var ICMSValor						= $('[name=txt_nfe_pedido_item_tributos_icms_vICMS_'+n+']').val();
			var ICMSBCOperacaoPropria			= $('[name=txt_nfe_pedido_item_tributos_icms_pBCOp_'+n+']').val();
			var ICMSMotivoDesoneracao			= $('[name=hid_nfe_pedido_item_tributos_icms_motDesICMS_'+n+']').val();
			
			var ICMSSTModDetermBC				= $('[name=hid_nfe_pedido_item_tributos_icms_st_modBCST_'+n+']').val();
			var ICMSSTReducaoBC					= $('[name=txt_nfe_pedido_item_tributos_icms_st_pRedBCST_'+n+']').val();
			var ICMSSTMargemValorAdic			= $('[name=txt_nfe_pedido_item_tributos_icms_st_pMVAST_'+n+']').val();
			var ICMSSTBC						= $('[name=txt_nfe_pedido_item_tributos_icms_st_vBCST_'+n+']').val();
			var ICMSSTAliquota					= $('[name=txt_nfe_pedido_item_tributos_icms_st_pICMSST_'+n+']').val();
			var ICMSSTValor						= $('[name=txt_nfe_pedido_item_tributos_icms_st_vICMSST_'+n+']').val();
			var ICMSSTUFCodigo					= $('[name=hid_nfe_pedido_item_tributos_icms_st_UFST_'+n+']').val();
			var ICMSSTBCRetidoUFRem				= $('[name=txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_'+n+']').val();
			var ICMSSTValorRetidoUFRem			= $('[name=txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_'+n+']').val();
			var ICMSSTBCRetidoUFDest			= $('[name=txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_'+n+']').val();
			var ICMSSTValorRetidoUFDest			= $('[name=txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_'+n+']').val();
			
			var COFINSSituacaoTributaria		= $('[name=hid_nfe_pedido_item_tributos_cofins_situacao_tributaria_'+n+']').val();
			var COFINSCalculoTipo				= $('[name=hid_nfe_pedido_item_tributos_cofins_calculo_tipo_'+n+']').val();
			var COFINSBC						= $('[name=txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_'+n+']').val();
			var COFINSAliquotaPercentual		= $('[name=txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_'+n+']').val();
			var COFINSAliquotaReais				= $('[name=txt_nfe_pedido_item_tributos_cofins_aliquota_reais_'+n+']').val();
			var COFINSQtdVendida				= $('[name=txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_'+n+']').val();
			var COFINSValor						= $('[name=txt_nfe_pedido_item_tributos_cofins_valor_'+n+']').val();
			
			var COFINSSTCalculoTipo				= $('[name=hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo_'+n+']').val();
			var COFINSSTBC						= $('[name=txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_'+n+']').val();
			var COFINSSTAliquotaPercentual		= $('[name=txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_'+n+']').val();
			var COFINSSTAliquotaReais			= $('[name=txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_'+n+']').val();
			var COFINSSTQtdVendida				= $('[name=txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_'+n+']').val();
			var COFINSSTValor					= $('[name=txt_nfe_pedido_item_tributos_cofins_st_valor_'+n+']').val();
			
			var	IIBC							= $('[name=txt_nfe_pedido_item_tributos_ii_valor_base_calculo_'+n+']').val();
			var IIValorAduaneiras				= $('[name=txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_'+n+']').val();
			var IIValorIOF						= $('[name=txt_nfe_pedido_item_tributos_ii_valor_iof_'+n+']').val();
			var IIValor							= $('[name=txt_nfe_pedido_item_tributos_ii_valor_'+n+']').val();
			
			var IPISituacaoTributaria			= $('[name=hid_nfe_pedido_item_tributos_ipi_situacao_tributaria_'+n+']').val();
			var IPIEnquadramentoClasse			= $('[name=txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_'+n+']').val();
			var IPIEnquadramentoCodigo			= $('[name=txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_'+n+']').val();
			var IPICNPJProdutor					= $('[name=txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_'+n+']').val();
			var IPISeloControleCodigo			= $('[name=txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_'+n+']').val();
			var IPISeloControleQtde				= $('[name=txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_'+n+']').val();
			var IPICalculoTipo					= $('[name=hid_nfe_pedido_item_tributos_ipi_calculo_tipo_'+n+']').val();
			var IPIBC							= $('[name=txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_'+n+']').val();
			var IPIAliquota						= $('[name=txt_nfe_pedido_item_tributos_ipi_aliquota_'+n+']').val();
			var IPIUnidPadraoQtdeTotal			= $('[name=txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_'+n+']').val();
			var IPIUnidadeValor					= $('[name=txt_nfe_pedido_item_tributos_ipi_unidade_valor_'+n+']').val();
			var IPIValor						= $('[name=txt_nfe_pedido_item_tributos_ipi_valor_'+n+']').val();
			
			var ISSQNTributacao					= $('[name=hid_nfe_pedido_item_tributos_issqn_tributacao_'+n+']').val();
			var ISSQNBC							= $('[name=txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_'+n+']').val();
			var ISSQNAliquota					= $('[name=txt_nfe_pedido_item_tributos_issqn_aliquota_'+n+']').val();
			var ISSQNServicoLista				= $('[name=hid_nfe_pedido_item_tributos_issqn_servico_lista_'+n+']').val();
			var ISSQNUFCodigo					= $('[name=hid_nfe_pedido_item_tributos_issqn_uf_'+n+']').val();
			var ISSQNMunicipioCodigo			= $('[name=hid_nfe_pedido_item_tributos_issqn_municipio_'+n+']').val();
			var ISSQNValor						= $('[name=txt_nfe_pedido_item_tributos_issqn_valor_'+n+']').val();
			
			var PISSituacaoTributaria			= $('[name=hid_nfe_pedido_item_tributos_pis_situacao_tributaria_'+n+']').val();
			var PISCalculoTipo					= $('[name=hid_nfe_pedido_item_tributos_pis_calculo_tipo_'+n+']').val();
			var PISBC							= $('[name=txt_nfe_pedido_item_tributos_pis_valor_base_calculo_'+n+']').val();
			var PISAliquotaPercentual			= $('[name=txt_nfe_pedido_item_tributos_pis_aliquota_percentual_'+n+']').val();
			var PISAliquotaReis					= $('[name=txt_nfe_pedido_item_tributos_pis_aliquota_reais_'+n+']').val();
			var PISAQuantidadeVendida			= $('[name=txt_nfe_pedido_item_tributos_pis_quantidade_vendida_'+n+']').val();
			var PISValor						= $('[name=txt_nfe_pedido_item_tributos_pis_valor_'+n+']').val();
			
			var PISSTCalculoTipo				= $('[name=hid_nfe_pedido_item_tributos_pis_st_calculo_tipo_'+n+']').val();
			var PISSTBC							= $('[name=txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_'+n+']').val();
			var PISSTAliquotaPercentual			= $('[name=txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_'+n+']').val();
			var PISSTAliquotaReais				= $('[name=txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_'+n+']').val();
			var PISSTAQuantidadeVendida			= $('[name=txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_'+n+']').val();
			var PISSTValor						= $('[name=txt_nfe_pedido_item_tributos_pis_st_valor_'+n+']').val();
			
			var InfoAdicionais					= $('[name=txt_nfe_item_pedido_informacoes_adicionais_'+n+']').val();

			/* COMBUSTIVEL */
			var Prod_Esp_Tipo_Produto			= $('[name=txt_nfe_item_produto_especifico_id_'+n+']').val();
			var Combustivel_Cod_ANP				= $('[name=txt_nfe_item_produto_especifico_combustivel_anp_'+n+']').val();
			var Combustivel_Cod_IF				= $('[name=txt_nfe_item_produto_especifico_combustivel_if_'+n+']').val();
			var Combustivel_Cod_UF				= $('[name=txt_nfe_item_produto_especifico_combustivel_uf_'+n+']').val();
			var Combustivel_Qtd_Faturada		= $('[name=txt_nfe_item_produto_especifico_combustivel_qtd_faturada_'+n+']').val();
			var Combustivel_Cide_BC				= $('[name=txt_nfe_item_produto_especifico_combustivel_cide_bc_'+n+']').val();
			var Combustivel_Cide_Aliquota		= $('[name=txt_nfe_item_produto_especifico_combustivel_cide_aliquota_'+n+']').val();
			var Combustivel_Cide_Valor			= $('[name=txt_nfe_item_produto_especifico_combustivel_cide_valor_'+n+']').val();
			
			$('#sel_prod_especifico_tipo').find('option[id="'+Prod_Esp_Tipo_Produto+'"]').attr({selected:'selected'});
			$('#sel_produto_especifico_combustivel_codanp').find('option[id="'+Combustivel_Cod_ANP+'"]').attr({selected:'selected'});
			$('#txt_produto_especifico_combustivel_codif').val(Combustivel_Cod_IF);
			$('#sel_produto_especifico_combustivel_uf').find('option[id="'+Combustivel_Cod_UF+'"]').attr({selected:'selected'});
			$('#txt_produto_especifico_combustivel_qtd_faturada').val(Combustivel_Qtd_Faturada);
			$('#txt_produto_especifico_combustivel_cide_bc').val(Combustivel_Cide_BC);
			$('#txt_produto_especifico_combustivel_cide_aliquota').val(Combustivel_Cide_Aliquota);
			$('#txt_produto_especifico_combustivel_cide_valor').val(Combustivel_Cide_Valor);
			/* COMBUSTIVEL */
			
			$('#txt_nfe_item_principal_ncm').val(itemNCM);
			$('#txt_nfe_item_principal_ex_tipi').val(itemExTipi);
			$('#txt_nfe_item_principal_cfop').val(itemCFOP);
			
			$('#txt_nfe_item_principal_unidade_comercial').val(itemUnidadeComercial);
			$('#txt_nfe_item_principal_unidade_tributavel').val(itemUnidadeTributavel);
			$('#txt_nfe_item_principal_quantidade_tributavel').val(itemQtdeTributavel);
			$('#txt_nfe_item_principal_valor_unidade_tributavel').val(itemValorUnidTributavel);
		   
			$('#txt_nfe_item_principal_total_seguro').val(itemSeguro);
			$('#txt_nfe_item_principal_desconto_percent').val(itemDescontoPercent);
			$('#txt_nfe_item_principal_desconto').val(itemDesconto);
			$('#txt_nfe_item_principal_frete').val(itemFrete);
			
			$('#txt_nfe_item_principal_ean').val(itemEAN);
			$('#txt_nfe_item_principal_ean_tributavel').val(itemEANTributavel);
			$('#txt_nfe_item_principal_outras_despesas_acessorias').val(itemOutDespesasAcess);
			$('#txt_nfe_item_principal_valor_total_bruto').val(itemTotalBruto);
			$('#txt_nfe_item_principal_pedido_compra').val(itemPedidoCompra);
			$('#txt_nfe_item_principal_numero_item_pedido_compra').val(itemNumPedidoCompra);
			
			//$('#sel_nfe_item_principal_produto_especifico').val(itemProdutoEspecifico);
			$('#chk_nfe_item_principal_valor_total_bruto').attr('checked', itemCheckTotalBruto);
			
			$('#sel_nfe_item_tributos_ipi_situacao_tributaria').val(IPISituacaoTributaria);
			$('#sel_nfe_item_tributos_ipi_situacao_tributaria').change();
			
			$('#sel_nfe_item_tributos_ipi_calculo_tipo').val(IPICalculoTipo);
			//se o tipo de calculo nao tiver sido definido, nao faz a��o, pois pode conflitar com os campos destravados da ST
			if(IPICalculoTipo > 0){
				$('#sel_nfe_item_tributos_ipi_calculo_tipo').change();
			}
			
			$('#txt_nfe_item_tributos_ipi_enquadramento_classe').val(IPIEnquadramentoClasse);
			$('#txt_nfe_item_tributos_ipi_enquadramento_codigo').val(IPIEnquadramentoCodigo);
			$('#txt_nfe_item_tributos_ipi_produtor_cnpj').val(IPICNPJProdutor);
			$('#txt_nfe_item_tributos_ipi_selo_controle_codigo').val(IPISeloControleCodigo);
			$('#txt_nfe_item_tributos_ipi_selo_controle_quantidade').val(IPISeloControleQtde);
			$('#txt_nfe_item_tributos_ipi_valor_base_calculo').val(IPIBC);
			$('#txt_nfe_item_tributos_ipi_aliquota').val(IPIAliquota);
			$('#txt_nfe_item_tributos_ipi_unidade_padrao_quantidade_total').val(IPIUnidPadraoQtdeTotal);
			$('#txt_nfe_item_tributos_ipi_unidade_valor').val(IPIUnidadeValor);
			$('#txt_nfe_item_tributos_ipi_valor').val(IPIValor);
			
			$('#sel_nfe_item_tributos_pis_situacao_tributaria').val(PISSituacaoTributaria);
			$('#sel_nfe_item_tributos_pis_situacao_tributaria').change();
			
			$('#sel_nfe_item_tributos_pis_calculo_tipo').val(PISCalculoTipo);
			//se o tipo de calculo nao tiver sido definido, nao faz a��o, pois pode conflitar com os campos destravados da ST
			if(PISCalculoTipo > 0){
				$('#sel_nfe_item_tributos_pis_calculo_tipo').change();
			}
			
			$('#txt_nfe_item_tributos_pis_valor_base_calculo').val(PISBC);
			$('#txt_nfe_item_tributos_pis_aliquota_percentual').val(PISAliquotaPercentual);
			$('#txt_nfe_item_tributos_pis_aliquota_reais').val(PISAliquotaReis);
			$('#txt_nfe_item_tributos_pis_quantidade_vendida').val(PISAQuantidadeVendida);
			$('#txt_nfe_item_tributos_pis_valor').val(PISValor);
			
			$('#sel_nfe_item_tributos_pis_st_calculo_tipo').val(PISSTCalculoTipo);
			$('#sel_nfe_item_tributos_pis_st_calculo_tipo').change();

			$('#txt_nfe_item_tributos_pis_st_valor_base_calculo').val(PISSTBC);
			$('#txt_nfe_item_tributos_pis_st_aliquota_percentual').val(PISSTAliquotaPercentual);
			$('#txt_nfe_item_tributos_pis_st_aliquota_reais').val(PISSTAliquotaReais);
			$('#txt_nfe_item_tributos_pis_st_quantidade_vendida').val(PISSTAQuantidadeVendida);
			$('#txt_nfe_item_tributos_pis_st_valor').val(PISSTValor);
			
			$('#sel_nfe_item_tributos_cofins_situacao_tributaria').val(COFINSSituacaoTributaria);
			$('#sel_nfe_item_tributos_cofins_situacao_tributaria').change();
			
			$('#sel_nfe_item_tributos_cofins_calculo_tipo').val(COFINSCalculoTipo);
			//se o tipo de calculo nao tiver sido definido, nao faz a��o, pois pode conflitar com os campos destravados da ST
			if(COFINSCalculoTipo > 0){
				$('#sel_nfe_item_tributos_cofins_calculo_tipo').change();
			}
			
			$('#sel_nfe_item_tributos_icms_situacao_tributaria').val(ICMSSituacaoTributaria);
			$('#sel_nfe_item_tributos_icms_situacao_tributaria').change();
			$('#txt_nfe_item_tributos_icms_pCredSN').blur();
			
			$('#sel_nfe_item_tributos_icms_origem').val(ICMSOrigem);
			$('#txt_nfe_item_tributos_icms_pCredSN').val(ICMSAliquotaAplicCalculoCred);
			$('#txt_nfe_item_tributos_icms_vCredICMSSN').val(ICMSCredPodeSerAproveitado);
			
			$('#sel_nfe_item_tributos_icms_modBC').val(ICMSModDetermBC);
			$('#txt_nfe_item_tributos_icms_vBC').val(ICMSBC);
			$('#txt_nfe_item_tributos_icms_pRedBC').val(ICMSReducaoBC);
			$('#txt_nfe_item_tributos_icms_pICMS').val(ICMSAliquota);
			$('#txt_nfe_item_tributos_icms_vICMS').val(ICMSValor);
			
			$('#txt_nfe_item_tributos_icms_pBCOp').val(ICMSBCOperacaoPropria);
			$('#sel_nfe_item_tributos_icms_motDesICMS').val(ICMSMotivoDesoneracao);
			$('#sel_nfe_item_tributos_icms_st_modBCST').val(ICMSSTModDetermBC);
			$('#txt_nfe_item_tributos_icms_st_pRedBCST').val(ICMSSTReducaoBC);
			$('#txt_nfe_item_tributos_icms_st_pMVAST').val(ICMSSTMargemValorAdic);
			$('#txt_nfe_item_tributos_icms_st_vBCST').val(ICMSSTBC);
			$('#txt_nfe_item_tributos_icms_st_pICMSST').val(ICMSSTAliquota);
			$('#txt_nfe_item_tributos_icms_st_vICMSST').val(ICMSSTValor);
			$('#hid_nfe_item_tributos_icms_st_UFST').val(ICMSSTUFCodigo);
			
			$('#txt_nfe_item_tributos_icms_st_vBCSTRet').val(ICMSSTBCRetidoUFRem);
			$('#txt_nfe_item_tributos_icms_st_vICMSSTRet').val(ICMSSTValorRetidoUFRem);
			$('#txt_nfe_item_tributos_icms_st_vBCSTDest').val(ICMSSTBCRetidoUFDest);
			$('#txt_nfe_item_tributos_icms_st_vICMSSTDest').val(ICMSSTValorRetidoUFDest);
			
			
			$('#txt_nfe_item_tributos_cofins_valor_base_calculo').val(COFINSBC);
			$('#txt_nfe_item_tributos_cofins_aliquota_percentual').val(COFINSAliquotaPercentual);
			$('#txt_nfe_item_tributos_cofins_aliquota_reais').val(COFINSAliquotaReais);
			$('#txt_nfe_item_tributos_cofins_quantidade_vendida').val(COFINSQtdVendida);
			$('#txt_nfe_item_tributos_cofins_valor').val(COFINSValor);
			
			$('#sel_nfe_item_tributos_cofins_st_calculo_tipo').val(COFINSSTCalculoTipo);
			$('#sel_nfe_item_tributos_cofins_st_calculo_tipo').change();
			
			$('#txt_nfe_item_tributos_cofins_st_valor_base_calculo').val(COFINSSTBC);
			$('#txt_nfe_item_tributos_cofins_st_aliquota_percentual').val(COFINSSTAliquotaPercentual);
			$('#txt_nfe_item_tributos_cofins_st_aliquota_reais').val(COFINSSTAliquotaReais);
			$('#txt_nfe_item_tributos_cofins_st_quantidade_vendida').val(COFINSSTQtdVendida);
			$('#txt_nfe_item_tributos_cofins_st_valor').val(COFINSSTValor);
			
			$('#txt_nfe_item_tributos_ii_valor_base_calculo').val(IIBC);
			$('#txt_nfe_item_tributos_ii_valor_aduaneiras').val(IIValorAduaneiras);
			$('#txt_nfe_item_tributos_ii_valor_iof').val(IIValorIOF);
			$('#txt_nfe_item_tributos_ii_valor').val(IIValor);
			
			$('#sel_nfe_item_tributos_issqn_tributacao').val(ISSQNTributacao);
			$('#txt_nfe_item_tributos_issqn_valor_base_calculo').val(ISSQNBC);
			$('#txt_nfe_item_tributos_issqn_aliquota').val(ISSQNAliquota);
			$('#sel_nfe_item_tributos_issqn_servico_lista').val(ISSQNServicoLista);
			
			$('#sel_nfe_item_tributos_issqn_uf').val(ISSQNUFCodigo);
			$('#sel_nfe_item_tributos_issqn_uf').change();
			$('#sel_nfe_item_tributos_issqn_municipio').val(ISSQNMunicipioCodigo);
			$('#txt_nfe_item_tributos_issqn_valor').val(ISSQNValor);
			
			
			$('#txt_nfe_item_informacoes_adicionais').val(InfoAdicionais);
			$('#txt_nfe_item_tributos_icms_pCredSN').blur();
			
    	</script>
<?		
	}else{
?>		<script type="text/javascript">
			$.each($("#pedido_nfe_item_tributos_ISSQN :input"),function() {
				$('#pedido_nfe_item_tributos_ISSQN :input').addClass('invisible');
			});
		</script>
        
        <script type="text/javascript">
			//BUSCO OS DADOS FISCAIS POR ESTADO **********************************************************************************************************/
			var produto_id 			= <?= $produto_id ?>;
			var codigo_estado = $('#hid_pedido_nfe_municipio_entrega').val();
			var emitente_estado = $('#hid_item_fiscal_emitente_estado').val();
			if(codigo_estado){
				codigo_estado = codigo_estado.substr(0,2);
				$.get("filtro_ajax.php", {item_fiscal_estado_busca:codigo_estado, produto_id: produto_id, emitente_estado : emitente_estado}, function(theXML){
					$('dados',theXML).each(function(){
					
						var aliquota_icms_st = $(this).find("aliquota").text();
						var mva_icms_st		 = $(this).find("mva").text();
						var cfop			 = $(this).find("cfop").text();
						
						$('#txt_nfe_item_principal_cfop').val(cfop);
						$('#txt_nfe_item_tributos_icms_st_pICMSST').val(aliquota_icms_st);
						$('#txt_nfe_item_tributos_icms_st_pMVAST').val(mva_icms_st);
					});
				});
			}
			/********************************************************************************************************************************************/
		</script>
<?
	}
	
?>

	<script type="text/javascript">
		setTimeout(function(){ //preciso esperar um pouquinho senao n d� tempo de pegar qtd, st_trib, etc...
			function calculo_ncm(){
				var ncm 	= $('#txt_nfe_item_principal_ncm').val();
				var st_trib = $('#sel_nfe_item_tributos_icms_situacao_tributaria').val();
				var origem 	= $('#sel_nfe_item_tributos_icms_origem').val();
				var valor 	= $('#txt_nfe_item_principal_valor_unitario_comercial').val();
				var qtd		= $('#txt_nfe_item_principal_quantidade_comercial').val();
				
				$.ajax({
					type: "POST",
					url: "filtro_ajax.php?calcular_tributos=ok",
					data: {ncm:ncm, st_trib:st_trib, origem:origem, valor:valor, qtd:qtd},
					success: function(data){
						var valor 	= parseFloat(data);
						valor 		= valor.toFixed(2);
						$('#txt_nfe_item_total_tributos').val(float2br(valor));
					}
				});
			}
		
			//calculo os tributos pelo ncm sempre que a modal � aberta (independente se � novo ou editar
			calculo_ncm();
			
			$("#txt_nfe_item_principal_ncm, #txt_nfe_item_principal_valor_unitario_comercial, #txt_nfe_item_principal_quantidade_comercial, #sel_nfe_item_tributos_icms_situacao_tributaria, #sel_nfe_item_tributos_icms_origem").change(function(){
				calculo_ncm(); 
			});
		}, 500)
	</script>
	
<?	
	//se for novo item fiscal ele busca os dados pre cadastrados o produto
	if(isset($produto_id) && $item_id == 0){
		$sql = "SELECT * FROM tblproduto_fiscal	WHERE fldProduto_Id = $produto_id";
		$rsProduto_Fiscal = mysql_query($sql);
		echo mysql_error();
		if(!mysql_num_rows($rsProduto_Fiscal)){
?>			<script type="text/javascript">
				alert("Produto sem cadastro fiscal.\r\nPreencher dados manualmente.");
			</script>
<?		}
		else{
			
			$rowProduto_Fiscal 					= mysql_fetch_array($rsProduto_Fiscal);
			
			$ean								= $rowProduto_Fiscal['fldean'];
			$ean_unidade_tributavel				= $rowProduto_Fiscal['fldean_unidade_tributavel'];
			$ex_tipi							= $rowProduto_Fiscal["fldex_tipi"];
			$genero								= $rowProduto_Fiscal["fldgenero"];
			$ncm								= $rowProduto_Fiscal["fldncm"];
			
			$unidade_comercial					= $rowProduto_Fiscal["fldunidade_comercial"];
			$unidade_tributavel					= $rowProduto_Fiscal["fldunidade_tributavel"];

			//icms
			$icms_regime 	   					= $rowProduto_Fiscal['fldicms_regime'];
			$icms_CST							= $rowProduto_Fiscal['fldicms_CST'];
			$icms_orig							= $rowProduto_Fiscal['fldicms_orig'];
			
			$icms_CSOSN							= $rowProduto_Fiscal['fldicms_CSOSN'];
			$icms_modBC							= $rowProduto_Fiscal['fldicms_modBC'];
			$icms_pRedBC						= format_number_out($rowProduto_Fiscal['fldicms_pRedBC']);
			$icms_pICMS							= format_number_out($rowProduto_Fiscal['fldicms_pICMS']);
			
			$icms_pBCOp							= format_number_out($rowProduto_Fiscal['fldicms_pBCOp']);
			$icms_modBCST						= $rowProduto_Fiscal['fldicms_modBCST'];
			$icms_pRedBCST		              	= format_number_out($rowProduto_Fiscal['fldicms_pRedBCST']);
			$icms_pMVAST						= format_number_out($rowProduto_Fiscal['fldicms_pMVAST']);
			$icms_pICMSST						= format_number_out($rowProduto_Fiscal['fldicms_pICMSST']);
			$icms_motDesICMS					= $rowProduto_Fiscal['fldicms_motDesICMS'];
			$icms_pCredSN						= format_number_out($rowProduto_Fiscal['fldicms_pCredSN']);
			//ipi
			$ipi_situacao_tributaria			= $rowProduto_Fiscal['fldipi_situacao_tributaria'];
			$ipi_enquadramento_classe			= $rowProduto_Fiscal['fldipi_enquadramento_classe'];
			$ipi_enquadramento_codigo			= $rowProduto_Fiscal['fldipi_enquadramento_codigo'];
			$ipi_produtor_cnpj					= $rowProduto_Fiscal['fldipi_produtor_cnpj'];
			$ipi_calculo_tipo					= $rowProduto_Fiscal['fldipi_calculo_tipo'];
			$ipi_aliquota						= format_number_out($rowProduto_Fiscal['fldipi_aliquota']);
			$ipi_unidade_valor					= format_number_out($rowProduto_Fiscal['fldipi_unidade_valor']);
			
			//pis
			$pis_situacao_tributaria			= $rowProduto_Fiscal['fldpis_situacao_tributaria'];
			$pis_calculo_tipo					= $rowProduto_Fiscal['fldpis_calculo_tipo'];
			$pis_aliquota_porcentagem			= format_number_out($rowProduto_Fiscal['fldpis_aliquota_porcentagem']);
			$pis_aliquota_reais					= format_number_out($rowProduto_Fiscal['fldpis_aliquota_reais']);
			
			//pisST
			$pisST_calculo_tipo					= $rowProduto_Fiscal['fldpis_st_calculo_tipo'];
			$pisST_aliquota_porcentagem			= format_number_out($rowProduto_Fiscal['fldpis_st_aliquota_porcentagem']);
			$pisST_aliquota_reais				= format_number_out($rowProduto_Fiscal['fldpis_st_aliquota_reais']);
			
			//cofins
			$cofins_situacao_tributaria			= $rowProduto_Fiscal['fldcofins_situacao_tributaria'];
			$cofins_calculo_tipo				= $rowProduto_Fiscal['fldcofins_calculo_tipo'];
			$cofins_aliquota_porcentagem		= format_number_out($rowProduto_Fiscal['fldcofins_aliquota_porcentagem']);
			$cofins_aliquota_reais				= format_number_out($rowProduto_Fiscal['fldcofins_aliquota_reais']);
			
			//cofinsST
			$cofinsST_calculo_tipo				= $rowProduto_Fiscal['fldcofins_st_calculo_tipo'];
			$cofinsST_aliquota_porcentagem		= format_number_out($rowProduto_Fiscal['fldcofins_st_aliquota_porcentagem']);
			$cofinsST_aliquota_reais			= format_number_out($rowProduto_Fiscal['fldcofins_st_aliquota_reais']);
			
			//travando campos de tributos ######################################################################################################################################################################################
			//IPI
			if($ipi_situacao_tributaria == 1 || $ipi_situacao_tributaria == 7 || $ipi_situacao_tributaria == 8 || $ipi_situacao_tributaria == 14){
				$ipiCalculoTipo = 'enable';
			}
			
			//PIS
			if($pis_situacao_tributaria == 3){
				
				$disabledPISAliqReais = "enable";
				$disabledPISQtdVendida = "enable";
				
			}elseif($pis_situacao_tributaria >= 9){
				
				$pisCalculoTipo = 'enable';
				if($pis_calculo_tipo == 1){
					
					$disabledPISAliqPorcentagem = "enable";
					$disabledPISBC = "enable";
					
				}elseif($pis_calculo_tipo == 2){
					
					$disabledPISAliqReais = "enable";
					$disabledPISQtdVendida = "enable";
					
				}
			}elseif($pis_situacao_tributaria < 9){
				if($pis_situacao_tributaria >= 3){
					
					$disabledPISAliqReais = "enable";
					$disabledPISQtdVendida = "enable";
					
				}elseif($pis_situacao_tributaria != 3){
					
					$disabledPISAliqPorcentagem = "enable";
					$disabledPISBC = "enable";
				}
			}
			
			//COFINS
			if($cofins_situacao_tributaria == 3){
				
				$disabledCOFINSAliqReais = "enable";
				$disabledCOFINSQtdVendida = "enable";
				
			}elseif($cofins_situacao_tributaria >= 9){
				
				$COFINSCalculoTipo = 'enable';
				
				if($cofins_calculo_tipo == 1){
					
					$disabledCOFINSAliqPorcentagem = "enable";
					$disabledCOFINSBC = "enable";
					
				}elseif($cofins_calculo_tipo == 2){
					
					$disabledCOFINSAliqReais = "enable";
					$disabledCOFINSQtdVendida = "enable";
				}
			}elseif($cofins_situacao_tributaria < 9){
				if($cofins_situacao_tributaria >= 3){
					
					$disabledCOFINSAliqReais = "enable";
					$disabledCOFINSQtdVendida = "enable";
					
				}elseif($cofins_situacao_tributaria != 3){
					
					$disabledCOFINSAliqPorcentagem = "enable";
					$disabledCOFINSBC = "enable";
				}
			}
		}
	}
	
	$rsEstado_Emitente = mysql_query('SELECT fldMunicipio_Codigo FROM tblempresa_info');
	$rowEstado_Emitente = mysql_fetch_array($rsEstado_Emitente);
	$emitente_estado = substr($rowEstado_Emitente['fldMunicipio_Codigo'],0,2);
			
?>
    <ul id="pedido_nfe_item_abas" class="menu_modo" style="width:950px">
        <li><a href="principal">Principal</a></li>
        <li><a href="tributos">Tributos</a></li>
        <li><a href="adicionais">Informa&ccedil;&otilde;es Adicionais</a></li>
    </ul>
	<div style=" height:425px;overflow-y:scroll">
		<form id="pedido_nfe_item_dados" action="" method="" class="frm_detalhe frm_nfe" style="width:100%; padding-bottom: 30px">
        	<input type="hidden" id="hid_item_fiscal_edit" name="hid_item_fiscal_edit" value="<?=$item_id?>"  />
        	<input type="hidden" id="hid_item_fiscal_emitente_estado" name="hid_item_fiscal_emitente_estado" value="<?=$emitente_estado?>"  />
			
			<div id="pedido_nfe_item_principal">
<?				require("pedido_nfe_item_principal.php");				
?>			</div>

			<script type="text/javascript">
				var vendaDecimal 		= $("#hid_venda_decimal").val();
				var quantidadeDecimal 	= $("#hid_quantidade_decimal").val();
				
               	var fltDesconto = 0.00;//esse desconto esta aqui, pq ele nao eh calculado no caso de importacao de venda comum, entao ele nao existe nesse caso, caso contrario, calcula logo abaixo e sobrescreve
				var itemId 		= <?= $item_id ?>;
				//se for novo item adicionado, e nao editando ou de importacao da venda
				if(itemId == 0){
					/*desconto*/
					var valor 		= br2float($('#txt_produto_valor').val());
					var desconto	= br2float($('#txt_produto_desconto').val());
					var quantidade 	= br2float($('#txt_produto_quantidade').val());
					var fltDesconto = (valor * desconto / 100) * quantidade;
					
					$('#txt_nfe_item_principal_codigo').val($('#txt_produto_codigo').val());
					$('#txt_nfe_item_principal_descricao').val($('#txt_produto_nome').val());
					$('#txt_nfe_item_principal_quantidade_comercial').val(float2br(br2float($('#txt_produto_quantidade').val()).toFixed(quantidadeDecimal)));
					$('#txt_nfe_item_principal_quantidade_tributavel').val(float2br(br2float($('#txt_produto_quantidade').val()).toFixed(quantidadeDecimal)));
					$('#txt_nfe_item_principal_valor_unitario_comercial').val(float2br(br2float($('#txt_produto_valor').val()).toFixed(vendaDecimal)));
					$('#txt_nfe_item_principal_valor_unidade_tributavel').val(float2br(br2float($('#txt_produto_valor').val()).toFixed(vendaDecimal)));
					$('#txt_nfe_item_principal_desconto_percent').val(float2br((desconto).toFixed(2)));
					$('#txt_nfe_item_principal_desconto').val(float2br((fltDesconto).toFixed(2)));
				}
				
				var quantidade 		= br2float($('#txt_nfe_item_principal_quantidade_comercial').val());
				var valor 			= br2float($('#txt_nfe_item_principal_valor_unitario_comercial').val());
				var frete			= br2float($('#txt_nfe_item_principal_frete').val());
				var despesasAcess	= br2float($('#txt_nfe_item_principal_outras_despesas_acessorias').val());
				var seguro			= br2float($('#txt_nfe_item_principal_total_seguro').val());
				var fltDesconto		= br2float(ifNumberNull($('#txt_nfe_item_principal_desconto').val()));
				var totalBruto		= quantidade * valor;
				
				$('#txt_nfe_item_principal_valor_total_bruto').val(float2br((totalBruto).toFixed(2)));
				
				//se for novo item adicionado
				if(itemId == 0){
					//CALCULANDO BASE DO IPI E ICMS ##########################################################################################################################
					var valorAliquotaIPI = br2float($('#txt_nfe_item_tributos_ipi_aliquota').val());
					var baseIPI = (valor * quantidade) + despesasAcess + seguro;																   
					var valorIPI = (valorAliquotaIPI / 100) * baseIPI;
					
					if($('#txt_nfe_item_tributos_ipi_valor_base_calculo').attr("disabled") == false){
						$('#txt_nfe_item_tributos_ipi_valor_base_calculo').val(float2br(baseIPI.toFixed(2)));
						if(!isNaN(valorIPI)){
							$('#txt_nfe_item_tributos_ipi_valor').val(float2br(valorIPI.toFixed(2)));
						}else{
							$('#txt_nfe_item_tributos_ipi_valor').val('');
						}
					}
					
					var baseICMS = totalBruto;
					if(!isNaN(baseICMS)){
						$('#txt_nfe_item_tributos_icms_vBC').val(float2br(baseICMS.toFixed(2)));
					}else{
						$('#txt_nfe_item_tributos_icms_vBC').val('');
					}
					
					if($('#txt_nfe_item_tributos_ipi_valor_base_calculo').attr("disabled") == false){
						var baseIPI = (valor * quantidade) + despesasAcess + seguro;
						$('#txt_nfe_item_tributos_ipi_valor_base_calculo').val(float2br((baseIPI).toFixed(2)));
					}
				}
                //###################################################################################################################################################//
    		</script>

			<div id="pedido_nfe_item_tributos">
<?				require("pedido_nfe_item_tributos.php");				
?>			</div>

			<script type="text/javascript">
				$('#sel_nfe_item_tributos_icms_situacao_tributaria').blur();
			</script>		
			
			<div id="pedido_nfe_item_adicionais">
<?				require("pedido_nfe_item_adicionais.php");				
?>			</div>
			<input style="float:right; margin-right:20px" type="submit" class="btn_enviar" name="btn_enviar_item_nfe" id="btn_enviar_item_nfe" value="OK" title="OK" />			
		</form>
	</div>
	<script type="text/javascript">
	
		var item_id = '<?= $item_id?>';
		
        $('form#pedido_nfe_item_dados div').hide();
        $('div#pedido_nfe_item_principal').show();
        
        $('ul#pedido_nfe_item_abas a[href=principal]').addClass('ativo');
        $('div#pedido_nfe_item_principal input:first').focus();
        
        $('ul#pedido_nfe_item_abas a').click(function(event){
            event.preventDefault();
            //marcar aba ativa
            $('ul#pedido_nfe_item_abas a').removeClass('ativo');
            $(this).addClass('ativo');
            
            //ocultar todas as abas
            $('form#pedido_nfe_item_dados div').hide();
            
            //exibir a selecionada
            var aba = $(this).attr('href');
            $('div#pedido_nfe_item_'+aba).show();
            $('div#pedido_nfe_item_'+aba+' input:first, div#pedido_nfe_item_'+aba+' textarea:first').focus();
            
			//se for tributos, ja seleciona a aba de ICMS
            if(aba == 'tributos'){
				$('ul#pedido_nfe_item_tributos_abas a').removeClass('ativo');
				$('ul#pedido_nfe_item_tributos_abas li').hide();
				 
				 //acoes js para carregar campos, calculos, etc ***********************************************************************************/
					$('#sel_nfe_item_tributos_icms_situacao_tributaria').change();
					$('#txt_nfe_item_tributos_icms_pCredSN').blur();	
					$('#txt_nfe_item_tributos_icms_st_pMVAST').blur();
				/*****************************************************************************************************************************************/
				
				if(item_id > 0){// SE TIVER EDITANDO, BUSCA O PRE DEFINIDO
			
					if(itemRadTributo == 'icms'){
						
						$('#pedido_nfe_item_tributos_abas a[href=ICMS]').addClass('ativo');
						$('#pedido_nfe_item_tributos_ICMS input:first').focus();
						$('div#pedido_nfe_item_tributos_ICMS').show();
                   		$('li.icms').show();
					
					}else if(itemRadTributo == 'issqn'){
						
						$('#pedido_nfe_item_tributos_abas a[href=PIS]').addClass('ativo');
						$('#pedido_nfe_item_tributos_PIS input:first').focus();
						$('div#pedido_nfe_item_tributos_PIS').show();
                   		$('li.issqn').show();
					}	
				}else{
					$('#pedido_nfe_item_tributos_abas a[href=ICMS]').addClass('ativo');
					$('#pedido_nfe_item_tributos_ICMS input:first').focus();
					$('div#pedido_nfe_item_tributos_ICMS').show();
					$('li.icms').show();
				}
            }
        });
        
		if(item_id > 0){// SE TIVER EDITANDO, BUSCA O PRE DEFINIDO
			
			if(itemRadTributo == 'icms'){
				$.each($("#pedido_nfe_item_tributos_ISSQN :input"),function() {
					$('#pedido_nfe_item_tributos_ISSQN :input').addClass('invisible');
				});
			}else if(itemRadTributo == 'issqn'){
				$.each($("#pedido_nfe_item_tributos_ISSQN :input"),function() {
					$('#pedido_nfe_item_tributos_ISSQN :input').removeClass('invisible');
				});
			}	
		}else{
			//DEIXANDO TODOS OS INPUTS DO ISSQN EM BRANCO
			$.each($("#pedido_nfe_item_tributos_ISSQN :input"),function() {
				$('#pedido_nfe_item_tributos_ISSQN :input').addClass('invisible');
			});
		}
    </script>          