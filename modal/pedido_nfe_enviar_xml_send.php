<?Php	
	include('../inc/con_db.php');
	include('../inc/fnc_general.php');
	include('../nfephp/libs/NFe/ToolsNFePHP.class.php');
	include('../nfephp/libs/NFe/AutoToolsNFePHP.class.php');
	include('../nfephp/libs/Common/MailNFePHP.class.php');
	
	if(isset($_GET['documento_id'])){
		
		$documento_id = $_GET['documento_id'];
		
		#BUSCANDO DADOS DA NOTA E CLIENTE
		$rsNfe = mysql_query("SELECT fldchave, fldserie, fldambiente, fldnfe_total, fldNome, fldEmail FROM tblpedido_fiscal 
								INNER JOIN tblpedido ON tblpedido_fiscal.fldpedido_id = tblpedido.fldId
								INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								WHERE tblpedido.fldId = {$documento_id}"); // ALTERAR AQUI PARA PEDIDO OU  COMPRA
		$rowNfe = mysql_fetch_array($rsNfe);
		echo mysql_error();
		
		$numero = str_pad($rowNfe['fldchave'], 9, "0", STR_PAD_LEFT);;
		$serie 	= str_pad($rowNfe['fldserie'], 3, "0", STR_PAD_LEFT);
		$vtotal = format_number_out($rowNfe['fldnfe_total'],2);
		$cliente = $rowNfe['fldNome'];
		$email = $rowNfe['fldEmail'];
		
	
		#BUSCANDO DADOS DA EMPRESA
		$rsEmpresa = mysql_query("SELECT fldRazao_Social FROM tblempresa_info");
		$rowEmpresa = mysql_fetch_array($rsEmpresa);
		
		$emitente_nome = $rowEmpresa['fldRazao_Social'];
		
		#BUSCANDO DADOS DE CONF DO SENDMAIL
		$rsConfig = mysql_query("SELECT * FROM tblsendmail_config WHERE fldPadrao = 0");
		$rowConfig = mysql_fetch_array($rsConfig);

		$port	 	= $rowConfig['fldPorta']; 
		$server	 	= $rowConfig['fldSmtp'];
		$emitente	= $rowConfig['fldUsuario'];
		$pass	 	= $rowConfig['fldSenha'];
		
		#CRIANDO XML E PDF PARA ENVIO
		$ambiente = ($rowNfe['fldambiente'] == '1')? 'producao' : 'homologacao';
		$dir 	= "../nfe_myerp/{$ambiente}/enviadas/aprovadas/";
		$file 	= $rowNfe['fldchave'].'-nfe.xml';
        $docxml = file_get_contents($dir.$file);
		

		$danfe 	= new DanfeNFePHP($docxml, 'P', 'A4', 'image/layout/logo_empresa.jpg', 'I', '');
		$id 	= $danfe->montaDANFE();
		$pdfName = $id.'.pdf';
			
		//carrega a DANFE como uma string para gravação no diretorio escolhido
		$pdfFile = (string) $danfe->printDANFE($dir.$pdfName,'S');
		
		//$reply = 'luanacleao@hotmail.com';
		$CONF_EMAIL=array(
			'mailAuth'=>'1',
			'mailFROM'=>$emitente_nome,
			'mailHOST'=> $server,
			'mailUSER'=>$emitente,
			'mailPASS'=> $pass,
			'mailIMAPport' => $port,
			'mailPROTOCOL'=>"",
			'mailFROMmail'=>$emitente,
			'mailFROMname'=>$emitente_nome,
			'mailREPLYTOmail'=>$email,
		   	'mailREPLYTOname'=>$emitente_nome,
			'mailERROR'=>"",
			'mailIMAPhost' => "",
			'mailIMAPport' => "",
			'mailIMAPsecurity' => "ssl"
		);
		
		//montar a matriz de dados para envio do email
		$aEMail = array('emitente'=>$emitente_nome,'para'=>$email,'contato'=>$cliente,'razao'=>'','numero'=>$numero,'serie'=>$serie,'vtotal'=>$vtotal);
		//inicalizar a classe de envio
		$nfeMail = new MailNFePHP($CONF_EMAIL);
		
		if ( !$nfeMail->sendNFe($docxml,$pdfFile,$file,$pdfName,$aEMail) ){
			$erro .=  $nfeMail->mailERROR;
			$this->errMsg = "Falha no envio do email ao destinatário!!\n";
			$this->errStatus = true;
		}
		
		if($erro){
			$erro = "\nFalha no envio do email ao destinatário!\n" .$erro;
		}else{
			$erro = "\nArquivo enviado com sucesso!";
		}
		$xml = "\t\t<erro>$erro</erro>\n";
		
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";
		
		
	}