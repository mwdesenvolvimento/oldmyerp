<?
	ob_start();
	session_start();
?>

	<script type="text/javascript">
		//evita que o enter faça alguma acao					   
		$("input#buscar").keydown(function(event){
			if (event.keyCode == 13){
				return false;
			}			   
		});						   
		
		$('document').ready(function(){
				$('#buscar').focus();
				$('#buscar').select();
				$('#loading').hide();
				$('#buscar').click(function(){
					$('#buscar').val('');
				});
				
				$('#buscar').keypress(busca_keyPress);
				function busca_Scroll(deslocamento){
					//alert(deslocamento);
					var cursorAtual 	= parseInt($('#hid_busca_cursor').val());
					var cursorLimite 	= parseInt($('#hid_busca_controle').text() - 1);
					var mover = false;
					if(deslocamento<0){
						if(cursorAtual > 0){mover = true;}
					}
					else{
						if(cursorAtual < cursorLimite){mover = true;}
					}
					if(mover){
						$('a[title=busca_lista_endereco]').removeClass('cursor');
						cursorNovo = cursorAtual + deslocamento;
						//corrigir cursor se ultrapassar limites
						if(cursorNovo < 0){cursorNovo = 0;}
						if(cursorNovo > cursorLimite){cursorNovo = cursorLimite;}
						
						$('a[title=busca_lista_endereco]:eq('+cursorNovo+')').addClass('cursor');
						$('#hid_busca_cursor').val(cursorNovo);
						var strNome = $('li[title=nome]:eq('+cursorNovo+')').text();
						$('#buscar').val(strNome);
						
						//19 - altura da linha de exibição de cada registro
						$('#alvo').scrollTop($('#alvo').scrollTop() + 19 * deslocamento);
					}
				}
				
				function busca_keyPress(event) {
					
					console.log(this);
					//alert(event.keyCode);
					switch(event.keyCode){
						case 13: //enter
							var intCursor = $('#hid_busca_cursor').val();
							$('#parametro_endereco').load('modal/endereco_busca.php', {codigo_endereco: $('a[title=busca_lista_endereco]:eq('+intCursor+')').attr('href')} );
						case 38: //up
							busca_Scroll(-1);
							break;
							
						case 40: //down
							busca_Scroll(1);
							break;
							
						case 33: //page up
							busca_Scroll(-10);
							break;
							
						case 34: //page down
							busca_Scroll(10);
							break;
							
						default: //busca
							//reset da posição do cursor para acompanhar o scroll
							$('#hid_busca_cursor').val(0);
							$.post('modal/endereco_busca_consulta.php',
							{busca: $('#buscar').val()},
							function(data){
								if ($('#buscar').val()!=''){
									$('#alvo').show();
									$('#alvo').empty().html(data);
								}
								else{
									$('#alvo').empty();
								}
							});
							break;
					}
				}
			
			//pegar o id no href e retornar para essa mesma página via ajax o valor e gravar na sessão	
			$('a.selecionar').live('click',function(e){
				e.preventDefault();
				$('#parametro_endereco').load('modal/endereco_busca.php', {codigo_endereco: $(this).attr('href')} );
			});	
		});
	</script>
    	
	<div id="parametro_endereco">
<?
		if(isset($_POST['codigo_endereco'])){
			
			$_SESSION["endereco_codigo"] = $_POST['codigo_endereco'];
			unset($_POST['codigo_endereco']);
?>
			<script type="text/javascript">
				$('.frm_busca_endereco').parents('.modal-body').remove();
				$('input#txt_endereco_codigo').focus();
			</script>
<?		}
?>
	</div>
	
	<? require("../inc/con_db.php"); ?>

    <form id="frm_busca" name="frm_busca" class="frm_busca_endereco">
    <fieldset>
    	<legend>Busca de endereco</legend>
            
           	<ul>
            	<li style="float:left" >
                	<input style="float:left" type="text" id="buscar" name="buscar" value="Digite o endereco" size="50" >
                </li>
			</ul>            
            <ul id="busca_cabecalho">
                <li style="width:285px">Endere&ccedil;o</li>
                <li style="width:285px">Bairro</li>
                <li style="width:60px; text-align:right">Setor</li>
            </ul>
			<input type="hidden" id="hid_busca_cursor" value="0" />
            <div id="alvo"></div>
    	</fieldset>
    </form>