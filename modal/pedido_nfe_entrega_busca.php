<?
	ob_start();
	session_start();
	
	require("../inc/fnc_general.php");
	require("../inc/con_db.php");
	require("../inc/fnc_ibge.php"); 
?>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/init-validate.js"></script>
	
    
	<div id="parametro_entrega">
<?
		if(isset($_POST['codigo_entrega'])){
			
			$sql = "SELECT * FROM tblcliente_endereco_entrega WHERE fldId = " .$_POST['codigo_entrega'];
			$entrega = mysql_query($sql);
			$result = mysql_fetch_array($entrega);
			$teste = $result['fldIdentificacao'];
?>
			<script type="text/javascript">
				
				var endereco	= "<?=$result['fldEndereco']?>";
				var numero		= "<?=$result['fldNumero']?>";
				var CPF_CNPJ	= "<?=$result['fldCPF_CNPJ']?>";
				var complemento	= "<?=$result['fldComplemento']?>";
				var bairro		= "<?=$result['fldBairro']?>";
				var cep			= "<?=$result['fldCep']?>";
				var munCodigo	= "<?=$result['fldMunicipio_Codigo']?>";
				var municipio	= "<?=fnc_ibge_municipio($result['fldMunicipio_Codigo'])?>";
				
				$('#txt_pedido_nfe_entrega_endereco').val(endereco);
				$('#txt_pedido_nfe_entrega_numero').val(numero);
				$('#txt_pedido_nfe_entrega_cpf_cnpj').val(CPF_CNPJ);
				$('#txt_pedido_nfe_entrega_complemento').val(complemento);
				$('#txt_pedido_nfe_entrega_bairro').val(bairro);
				$('#txt_pedido_nfe_entrega_cep').val(cep);
				$('#txt_municipio_codigo_pedido_nfe').val(munCodigo);
				$('#txt_pedido_nfe_municipio').val(municipio);
				
				$('div.modal-body').remove();
				$('input#txt_pedido_nfe_entrega_endereco').focus();
					
			</script>
<?
			die();
		}
?>
	</div>
	
    
	<script type="text/javascript">
		
		//evita que o enter faça alguma acao					   
		$("input#buscar").keydown(function(event){
			if (event.keyCode == 13){
				return false;
			}			   
		});	
		
		//pegar o id no href e retornar para essa mesma página via ajax o valor e gravar na sessão	
		$('a.selecionar').live('click',function(e){
			e.preventDefault();
			
			$('#parametro_entrega').load('modal/pedido_nfe_entrega_busca.php', {codigo_entrega: $(this).attr('href')} );
		});	
	</script>

	
        <form id="frm_busca" style="border:width:840px; margin-left:8px; float:left">
            <fieldset>
                <legend>Locais de entrega</legend>
                
                <ul id="busca_cabecalho" style="width:885px;">
                    <li style="width:80px;">Identifica&ccedil;&atilde;o</li>
                    <li style="width:110px;">CPF/CNPJ</li>
                    <li style="width:190px;">Endere&ccedil;o</li>
                    <li style="width:60px;">Numero</li>
                    <li style="width:110px;">Complem.</li>
                    <li style="width:100px;">Bairro</li>
                    <li style="width:73px;">CEP</li>
                    <li style="width:40px;">UF</li>
                    <li style="width:110px;">Municipio</li>
                </ul>
                <div id="alvo" style="width:885px">
<?            		if($_POST['params']['1']){			
						$sql = "SELECT * FROM tblcliente_endereco_entrega WHERE fldCliente_Id = " .$_POST['params']['1'] ." ORDER BY fldIdentificacao";
                		$entrega = mysql_query($sql);
?>    
               			<ul id="busca" style="width:885px;padding:0;float:left;margin:3px 0;">
<?							while($result = mysql_fetch_array($entrega)){
?>								<a id="selecionar_<?=$result['fldId']?>" class="selecionar" href="<?=$result['fldId']?>">
                                    <li style="width:80px"><?=$result['fldIdentificacao']?></li>
                                    <li style="width:110px"><?=$result['fldCPF_CNPJ']?></li>
                                    <li style="width:200px"><?=$result['fldEndereco']?></li>
                                    <li style="width:60px"><?=$result['fldNumero']?></li>
                                    <li style="width:100px"><?=$result['fldComplemento']?></li>
                                    <li style="width:100px"><?=$result['fldBairro']?></li>
                                    <li style="width:73px"><?=$result['fldCep']?></li>
                                    <li style="width:40px"><?=fnc_ibge_uf_sigla($result['fldMunicipio_Codigo'])?></li>
                                    <li style="width:120px"><?=fnc_ibge_municipio($result['fldMunicipio_Codigo'])?></li>
                                </a>
<?							}
?>						</ul>
<?					}
?>          	  </div>
    		</fieldset>
 	   </form>
