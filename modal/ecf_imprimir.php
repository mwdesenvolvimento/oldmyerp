﻿
<?	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	$documento 	= $_POST['documento'] ?: $_GET['documento'];
	$id					= $_POST['id'] ?: $_GET['id'];
	$tela	 			= $_POST['hid_tela'] ?: $_GET['hid_tela'];
	
	$sql		= "select * from tblpedido where fldId = $id";
	$rsVenda 	= mysql_query($sql);
	$rowVenda 	= mysql_fetch_array($rsVenda);
	
	
	
	
	$sql = "SELECT tblpedido_item.*, tblproduto.fldCodigo, tblecf_aliquota.fldecf_valor
			FROM tblpedido_item 
			INNER JOIN tblproduto on tblpedido_item.fldProduto_Id = tblproduto.fldId
			LEFT JOIN tblecf_aliquota ON tblproduto.fldecf_aliquota_id = tblecf_aliquota.fldId
			WHERE tblpedido_item.fldExcluido = 0 AND tblpedido_item.fldPedido_Id = $id";
	$rsVenda_Item = mysql_query($sql);
	
	//Executar validações iniciais antes de prosseguir
	
	if(!mysql_num_rows($rsVenda_Item)){
		die("N&atilde;o existem itens nesta venda!");
	}
	
	while($rowVenda_Item = mysql_fetch_array($rsVenda_Item)){
		$desconto_valor = $rowVenda_Item['fldDesconto'];
		$desconto_valor = number_format($desconto_valor,1,'','');
		
		//se houver algum problema deve cancelar a emissão do cupom
		//realizar verificação de aliquotas antes de enviar dados
		//criar rotina
		//----------------------------------------------------------------------
		
		//Coletar dados dos itens
		$itens[] = array(
			'codigo'			=> $rowVenda_Item['fldCodigo'],
			'descricao'			=> substr($rowVenda_Item['fldDescricao'],0,28),
			'aliquota'			=> $rowVenda_Item['fldecf_valor'],
			'quantidade_tipo'	=> 'F',
			'quantidade'		=> number_format($rowVenda_Item['fldQuantidade'],3,'.',''),
			'casas_decimais'	=> '2',
			'valor_unitario'	=> number_format($rowVenda_Item['fldValor'],2,'.',''),
			'desconto_tipo'		=> '%',
			'desconto_valor'	=> $desconto_valor
		);
	}
	
	//IniciaFechamentoCupom
	$acrescimo_desconto 			= 'D';
	if($rowVenda['fldDesconto']>0){
		$acrescimo_desconto_tipo		= '%';
		$acrescimo_desconto_valor		= number_format($rowVenda['fldDesconto'],2,',','');
	}
	else{
		$acrescimo_desconto_tipo		= '$';
		$acrescimo_desconto_valor		= number_format($rowVenda['fldDescontoReais'],2,',','');
	}
	
	//EfetuaFormaPagamento
	$sql = "select sum(fldValor * fldQuantidade - (fldValor * fldQuantidade * fldDesconto / 100)) as fldValorTotal
			from tblpedido_item
			where fldPedido_Id = $id group by fldPedido_Id";
	$rsVenda_Total 	= mysql_query($sql);
	$rowVenda_Total = mysql_fetch_array($rsVenda_Total);
	
	$pagamento_forma				= 'dinheiro';
	
	//calcular total e desconto
	$pagamento_total				= $rowVenda_Total['fldValorTotal'];
	if($acrescimo_desconto_tipo=='%'){
		$desconto = $pagamento_total * $rowVenda['fldDesconto'] / 100;
	}
	else{
		$desconto = $rowVenda['fldDescontoReais'];
	}
	$pagamento_total 				= $pagamento_total - $desconto;
	$pagamento_total				= number_format($pagamento_total,2,',','');
	
	//TerminaFechamentoCupom
	$fiscal_cupom_mensagem_final	= 'obrigado pela preferencia';

	//Gerar arquivo com comandos de impressão fiscal
	$output		= "AbreCupom\r\n$documento|\r\n";
	
	foreach($itens as $item){
		$output .= "VendeItem\r\n" . implode('|',$item) . "|\r\n";
	}
	
	$output		.= "IniciaFechamentoCupom\r\n$acrescimo_desconto|$acrescimo_desconto_tipo|$acrescimo_desconto_valor|\r\n";
	
	$output		.= "EfetuaFormaPagamento\r\n$pagamento_forma|$pagamento_total|\r\n";
	
	$output		.= "TerminaFechamentoCupom\r\n$fiscal_cupom_mensagem_final|\r\n";
	
	$remote_ip = gethostbyname($REMOTE_ADDR);
	$file_name = $remote_ip . '_' . date('Ymd_His');
	$file = "../ecf/inbox/$file_name.ecf"; // Definimos o local para salvar o arquivo de texto
	
	$fp 	= fopen($file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
	$salva 	= fwrite($fp, $output);
	fclose($fp);

?>
	<input type="hidden" id="hid_tela" name="hid_tela" value="<?=$tela?>" />
    
	<script type="text/javascript">
        var modo = $('#hid_tela').val();
		$('div.modal-body').remove();
		window.location="index.php?p=pedido&modo="+modo;
    </script>
    