<script type="text/javascript" src="js/general.js" charset="ISO-8859-1"></script>


<form id="frm_nfe_importar_venda_simples" name="frm_general" class="frm_detalhe" style="width:380px" action="index.php" method="get">
	<ul>
		<li class="form">
			<label for="txt_documento">C&oacute;d. Venda</label>
			<input type="hidden" id="p" name="p" value="pedido_nfe_importar" />
			<input type="text" style="width: 130px; text-align:right" id="id" name="id" onkeyup="numOnly(this)" />
		</li>
		<li>
			<input type="submit" style="margin-top:14px" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="ok" title="OK" />
		</li>
	</ul>
</form>

<script type="text/javascript">
	$('input[name=id]').focus();
	
	$('#frm_nfe_importar_venda_simples #btn_enviar').click(function(e){
		e.preventDefault();
		
		$.get('modal/nfe_importar_checar_venda_simples.php', { id : $('input[name=id]').val() }, function(resposta) {
                
			if(resposta.prosseguir === false) {
				alert('A venda que você está tentando converter já é uma NFe. Número ' + resposta.NFenumero + '!');
				$('input[name=id]').focus();
			}
			else{
				$('#frm_nfe_importar_venda_simples').submit();
			}
			
		}, 'json');
		
	});
</script>