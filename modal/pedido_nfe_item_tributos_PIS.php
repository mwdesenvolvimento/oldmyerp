	<fieldset style="width:920px;display:inline;margin:10px">
    	<legend>PIS</legend>
        <ul>
            <li>
                <label for="sel_nfe_item_tributos_pis_situacao_tributaria">Situa&ccedil;&atilde;o Tribut&aacute;ria</label>
                <select id="sel_nfe_item_tributos_pis_situacao_tributaria" name="sel_nfe_item_tributos_pis_situacao_tributaria" style="width:280px">
                	<option value=""></option>
<?					$sql = "SELECT * FROM tblnfe_pis";
                    $rsPIS_Situacao = mysql_query($sql);
                    while($rowPIS_Situacao = mysql_fetch_array($rsPIS_Situacao)){
?>						<option <?=$pis_situacao_tributaria==$rowPIS_Situacao["fldId"] ? print 'selected="selected"' : '' ?> value="<?=$rowPIS_Situacao["fldId"]?>" title="<?=$rowPIS_Situacao["fldDescricao"]?>"><?=$rowPIS_Situacao["fldSituacaoTributaria"] . ' - ' . substr($rowPIS_Situacao["fldDescricao"],0,130)?></option>
<?					}
?>				</select>
            </li>
            <li id="li_calculo_pis_js">
                <label for="sel_nfe_item_tributos_pis_calculo_tipo">Tipo de C&aacute;lculo</label>
                <select name="sel_nfe_item_tributos_pis_calculo_tipo" id="sel_nfe_item_tributos_pis_calculo_tipo" <?=($pisCalculoTipo != 'enable' )? "disabled='disabled'" : ''?> style="width:280px;">
                    <option value=""></option>
                    <option <?=$pis_calculo_tipo== 1 ? print 'selected="selected"' : '' ?> value="1">Percentual</option>
                    <option <?=$pis_calculo_tipo== 2 ? print 'selected="selected"' : '' ?> value="2">Valor</option>
                </select>
            </li>
            <li>
                <label for="txt_nfe_item_tributos_pis_valor_base_calculo">Valor da Base de C&aacute;lculo</label>
                <input type="text" style="width:280px;text-align:right;<?=($disabledPISBC != "enable")? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_pis_valor_base_calculo" name="txt_nfe_item_tributos_pis_valor_base_calculo" <?=($disabledPISBC != "enable" )? "disabled='disabled'" : "" ?> value="" />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_pis_aliquota_percentual">Al&iacute;quota (percentual)</label>
                <input type="text" style="width:280px;text-align:right;<?=($disabledPISAliqPorcentagem != "enable")? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_pis_aliquota_percentual" name="txt_nfe_item_tributos_pis_aliquota_percentual" <?=($disabledPISAliqPorcentagem != "enable" )? "disabled='disabled'" : "value='$pis_aliquota_porcentagem'" ?> />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_pis_aliquota_reais">Al&iacute;quota (reais)</label>
                <input type="text" style="width:280px;text-align:right;<?=($disabledPISAliqReais != "enable")? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_pis_aliquota_reais" name="txt_nfe_item_tributos_pis_aliquota_reais" <?=($disabledPISAliqReais != "enable" )? "disabled='disabled'" : "value='$pis_aliquota_reais'" ?>  />
            </li>
            <li>	
                <label for="txt_nfe_item_tributos_pis_quantidade_vendida">Quantidade Vendida</label>
                <input type="text" style="width:280px;text-align:right;<?=($disabledPISQtdVendida != "enable")? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_pis_quantidade_vendida" name="txt_nfe_item_tributos_pis_quantidade_vendida" <?=($disabledPISQtdVendida != "enable" )? "disabled='disabled'" : "" ?> value=""/> 
            </li>
            <li>
                <label for="txt_nfe_item_tributos_pis_valor">Valor do PIS</label>
                <input type="text" style="width:280px;text-align:right" id="txt_nfe_item_tributos_pis_valor" name="txt_nfe_item_tributos_pis_valor" readonly="readonly" value=""/>
            </li>
        </ul>
	</fieldset>


    <fieldset style="width:920px;display:inline;margin:10px">
        <legend>PIS ST</legend>
        <ul>
            <li id="li_calculo_pisST_js">
                <label for="sel_nfe_item_tributos_pis_st_calculo_tipo">Tipo de C&aacute;lculo</label>
                <select name="sel_nfe_item_tributos_pis_st_calculo_tipo" id="sel_nfe_item_tributos_pis_st_calculo_tipo" style="width:280px;">
                	<option value=""></option>
                    <option <?=$pisST_calculo_tipo== 1 ? print 'selected="selected"' : '' ?> value="1">Percentual</option>
                    <option <?=$pisST_calculo_tipo== 2 ? print 'selected="selected"' : '' ?> value="2">Valor</option>
                </select>
            </li>
            <li>
                <label for="txt_nfe_item_tributos_pis_st_valor_base_calculo">Valor da Base de C&aacute;lculo</label>
                <input type="text" style="width:280px;text-align:right;<?=($pisST_calculo_tipo != 1)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_pis_st_valor_base_calculo" name="txt_nfe_item_tributos_pis_st_valor_base_calculo" <?=($pisST_calculo_tipo  != 1)? "disabled='disabled'" : ''?> value="" maxlength="17" />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_pis_st_aliquota_percentual">Al&iacute;quota (percentual)</label>
                <input type="text" style="width:280px;text-align:right;<?=($pisST_calculo_tipo != 1)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_pis_st_aliquota_percentual" name="txt_nfe_item_tributos_pis_st_aliquota_percentual" <?=($pisST_calculo_tipo  != 1)? "disabled='disabled'" : "value='$pisST_aliquota_porcentagem'" ?> />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_pis_st_aliquota_reais">Al&iacute;quota (reais)</label>
                <input type="text" style="width:280px;text-align:right;<?=($pisST_calculo_tipo < 2)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_pis_st_aliquota_reais" name="txt_nfe_item_tributos_pis_st_aliquota_reais" <?=($pisST_calculo_tipo < 2 )? "disabled='disabled'" : "value='$pisST_aliquota_reais'"?> />
            </li>
            <li>	
                <label for="txt_nfe_item_tributos_pis_st_quantidade_vendida">Quantidade Vendida</label>
                <input type="text" style="width:280px;text-align:right;<?=($pisST_calculo_tipo < 2)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_pis_st_quantidade_vendida" name="txt_nfe_item_tributos_pis_st_quantidade_vendida" <?=($pisST_calculo_tipo < 2 )? "disabled='disabled'" : ''?> value="" /> 
            </li>
            <li>
                <label for="txt_nfe_item_tributos_pis_st_valor">Valor do PIS ST</label>
                <input type="text" style="width:280px;text-align:right" id="txt_nfe_item_tributos_pis_st_valor" name="txt_nfe_item_tributos_pis_st_valor" readonly="readonly" value=""/>
            </li>
            
        </ul>
	</fieldset>
