
	<script type="text/javascript" src="js/jquery-1.4.4.js"></script>
	<script type="text/javascript" src="js/produto.js" charset="ISO-8859-1"></script>
<?
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');

	ob_start();
	session_start();
	
	$tabelaId = $_POST['params'][1];
	if(isset($tabelaId)){
		
		$rsTabela = mysql_query("SELECT * FROM tblproduto_tabela WHERE fldId =".$tabelaId);
		$rowTabela = mysql_fetch_array($rsTabela);
	}
?>
    <form action="index.php?p=produto&modo=tabela" id="frm_produto_tabela" class="frm_detalhe" style="display:table;" method="post">
        <ul style="width:720px; position:relative; margin-bottom: 10px">
            <li>
                <label for="txt_nome">Nome</label>
                <input type="text" id="txt_nome" name="txt_nome" value="<?=$rowTabela['fldNome']?>" style="width:180px" />
                <input type="hidden" id="hid_id" name="hid_id" value="<?=$rowTabela['fldId']?>" />
            </li>
            <li>
                <label for="txt_descricao">Descri&ccedil;&atilde;o</label>
                <input type="text" id="txt_descricao" name="txt_descricao" value="<?=$rowTabela['fldDescricao']?>" style="width:380px" />
            </li>
            <li>
                <label for="txt_sigla">Sigla</label>
                <input type="text" id="txt_sigla" name="txt_sigla" value="<?=$rowTabela['fldSigla']?>" style="width:100px" maxlength="5" />
                <small>m&aacute;x. de 5 caracteres</small>
            </li>
            <li>
                <label for="txt_acrescimo">Acr&eacute;scimo</label>
                <input type="text" id="txt_acrescimo" name="txt_acrescimo" value="<?=format_number_out($rowTabela['fldAcrescimo'])?>" style="width:100px; text-align:right" />
            </li>
            <li>
                <label for="sel_acrescimo_tipo">&nbsp;</label>
                <select name="sel_acrescimo_tipo" id="sel_acrescimo_tipo" style="width:50px">
                	<option <?=($rowTabela['fldAcrescimo_Tipo'] == '1')? "selected='selected'" : '' ?> value="1">%</option>
                	<option <?=($rowTabela['fldAcrescimo_Tipo'] == '2')? "selected='selected'" : '' ?> value="2">R$</option>
                </select>
            </li>
            <li>
                <label for="sel_modo">Calcular acr&eacute;scimo a partir de</label>
                <select name="sel_modo" id="sel_modo" style="width:180px">
                	<option <?=($rowTabela['fldModo'] == '1')? "selected='selected'" : '' ?> value="1">valor de compra</option>
                	<option <?=($rowTabela['fldModo'] == '2')? "selected='selected'" : '' ?> value="2">valor de venda</option>
                </select>
            </li>
            <li style="float:right; margin-right:20px">
	            <input type="hidden" name="hid_edit" id="hid_edit" value="<?=$tabelaId?>" />
            	<input style="margin: 0" type="submit" class="btn_enviar" name="btn_salvar" id="btn_salvar" value="Salvar" title="Salvar" />
			</li>
        </ul>
    </form>
     
 
<script type="text/javascript">
	
	$('#txt_nome').select();
	
</script>
 