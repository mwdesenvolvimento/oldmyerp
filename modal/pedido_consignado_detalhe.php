<script type="text/javascript" src="js/myerp.js" charset="ISO-8859-1"></script>
<?
	ob_start();
	session_start();
	
	require('../inc/con_db.php');
	require('../inc/fnc_general.php');
	require('../inc/fnc_financeiro.php');
	
	if(isset($_POST['form'])){
		
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$consignado_id			= $form['txt_id'];
		
		$valor_retorno			= format_number_in($form['txt_total_retorno']);
		$despesa_funcionario1	= format_number_in($form['txt_funcionario_1_despesa']);
		$despesa_funcionario2	= format_number_in($form['txt_funcionario_2_despesa']);
		$despesa_outros			= format_number_in($form['txt_outras_despesas']);
		$descontos				= format_number_in($form['txt_valor_desconto']);
		
		$funcionario1_id		= $form['hid_funcionario_1'];
		$funcionario2_id		= $form['hid_funcionario_2'];

		$SQL = "UPDATE tblpedido_consignado SET 
				fldValor_Retorno 		= '$valor_retorno', 
				fldFuncionario1_Despesa = '$despesa_funcionario1', 
				fldFuncionario2_Despesa = '$despesa_funcionario2',
				fldDespesa_Outros 		= '$despesa_outros',
				fldDesconto 			= '$descontos', 
				fldFinalizado 			= '1'
				WHERE fldId 			= $consignado_id";
		
		if(mysql_query($SQL)){
			
			$conta_id 		 = fnc_sistema('pedido_parcela_conta_DN');
			
			//se nao foi finalizado ainda
			if($form['hid_finalizado'] == 0){
				$movimento_tipo = '8'; #VALE FUNCIONARIO
				$pagamento_tipo = '1'; #DINHEIRO
				
				if($despesa_funcionario1 > 0){
					//GRAVA NA tblfuncionario_conta_fluxo
					$last_id = fnc_funcionario_conta_fluxo_lancar($funcionario1_id, $movimento_tipo, $despesa_funcionario1, '', $consignado_id);
					
					if(isset($last_id)){
						//GRAVA NA TABELA DO CAIXA tblfinanceiro_conta_fluxo
						$fluxo_caixa_id = fnc_financeiro_conta_fluxo_lancar('', '', $despesa_funcionario1, '', $pagamento_tipo, $last_id, $movimento_tipo, '', $conta_id);
					}
				}
				
				if($despesa_funcionario2 > 0){
					//GRAVA NA tblfuncionario_conta_fluxo
					$last_id = fnc_funcionario_conta_fluxo_lancar($funcionario2_id, $movimento_tipo, $despesa_funcionario2, '', $consignado_id);
					
					if(isset($last_id)){
						//GRAVA NA TABELA DO CAIXA tblfinanceiro_conta_fluxo
						$fluxo_caixa_id = fnc_financeiro_conta_fluxo_lancar('', '', $despesa_funcionario2, '', $pagamento_tipo, $last_id, $movimento_tipo, '', $conta_id);
					}
				}
				
				
				if($despesa_outros > 0){
					fnc_financeiro_conta_fluxo_lancar('OUTRAS DESPESAS', '', $despesa_outros, 'CONSIGNADO '. str_pad($consignado_id, 4, "0", STR_PAD_LEFT), '1', $consignado_id, '1', '1', $conta_id); // 8 tipo vale de funcionario
				}
				//$descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta
			}
		}else{
			echo mysql_error();
			die();
		}
		//INSERINDO RETORNO DE ITENS CONSIGNADOS ################################################################################################################################
		$n = 1;
		$limite_item	= $form['hid_controle_item'];
		while($n <= $limite_item){
			
			$item_id	 			= $form['hid_item_id_'.$n];
			$qtd_aprazo				= format_number_in($form['txt_item_aprazo_'.$n]);
			$qtd_avista				= format_number_in($form['txt_item_avista_'.$n]);
			
			$SQL = "UPDATE tblpedido_consignado_item SET 
					fldQuantidade_Aprazo = '$qtd_aprazo', 
					fldQuantidade_Avista = '$qtd_avista' 
					WHERE fldId = $item_id";
			
			if(mysql_query($SQL)){
				$n++;
			}else{
				echo mysql_error();
				die();
			}
		}
		
		//INSERINDO ITENS RECEBIDOS (FIADO) ################################################################################################################################
		mysql_query("DELETE FROM tblpedido_consignado_item_recebido WHERE fldConsignado_Id = $consignado_id");
		$n = 1;
		$limite_item	= $form['hid_controle'];
		while($n <= $limite_item){
			
			$produto_id	 			= $form['hid_item_recebido_produto_id_'.$n];
			$produto_descricao		= $form['txt_item_recebido_nome_'.$n];
			$valor 					= format_number_in($form['txt_item_recebido_valor_'.$n]);
			$quantidade				= format_number_in($form['txt_item_recebido_quantidade_'.$n]);
			
			if($produto_id > 0){
				$SQL = "INSERT INTO tblpedido_consignado_item_recebido
				(fldProduto_Id, fldDescricao, fldValor, fldQuantidade, fldConsignado_Id)
				VALUES (
				'$produto_id',	'$produto_descricao',
				'$valor',		'$quantidade',
				'$consignado_id '
				)";
				
				if(mysql_query($SQL)){
					$n++;
				}else{
					echo mysql_error();
					die();
				}
			}
		}
?>
		<img src="image/layout/carregando.gif" align="carregando..." />
		<script type="text/javascript">
			window.location="index.php?p=pedido&modo=consignado";
		</script> 
<?		die;
	}
##########################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

	$consignado_id 	= $_POST['params'][1];
	$rsConsignado 	= mysql_query("SELECT * FROM tblpedido_consignado WHERE fldId = $consignado_id");
	$rowConsignado 	= mysql_fetch_array($rsConsignado);
	$valor_saida 	= $rowConsignado['fldValor_Saida'];
	$finalizado 	= $rowConsignado['fldFinalizado'];
	
?>	
    <form id="frm_pedido_consignado_fechar" name="frm_general" class="frm_detalhe" style="width:95%" action="" method="post">
        <fieldset style="margin-bottom: 5px">
            <ul>
                <li>
                    <label for="txt_id">C&oacute;d.</label>
                    <input type="text" style="width:80px; text-align:right" id="txt_id" name="txt_id" value="<?=str_pad($rowConsignado['fldId'], 5, '0', STR_PAD_LEFT)?>" readonly="readonly" />
                    <input type="hidden" id="hid_finalizado" name="hid_finalizado" value="<?=$finalizado?>" />
                </li>
                <li>
                    <label for="txt_data">Data</label>
                    <input type="text" style="width:80px; text-align:center;background:#FFC" id="txt_data" name="txt_data" value="<?= format_date_out($rowConsignado['fldData'])?>" readonly="readonly" />
                </li>
                <li>
                    <label for="txt_hora">Hora</label>
                    <input type="text" style="width:60px;text-align:center;background:#FFC"  id="txt_hora" name="txt_hora" value="<?=$rowConsignado['fldHora']?>" readonly="readonly" />
                </li>
                <li>
<?					if($_SESSION["sistema_rota_controle"] == 1){            
?>	                	<label for="sel_rota">Rota</label>
  						<select style="width:85px" id="sel_rota" name="sel_rota" >
                    		<option value="0">Selecionar</option>
<?							$rsRota = mysql_query("SELECT * FROM tblendereco_rota WHERE fldExcluido = 0");
							while($rowRota = mysql_fetch_array($rsRota)){
?>								<option value='<?=$rowRota['fldId']?>' <?= ($rowConsignado['fldRota_Id'] == $rowRota['fldId']) ? "selected = 'selected'" : '' ?>><?=$rowRota['fldRota']?></option>
<?							}
?>						</select>
<?					}
?>				</li>
				<li>
                	<label for="txt_valor_saida" >Troco</label>
                	<input type="text" style=" width:90px; text-align:right; font-size:18px; background:#FF6" id="txt_valor_saida" name="txt_valor_saida" value="<?= format_number_out($valor_saida)?>" readonly="readonly" />
                </li>
                
                <li> 
<?					if($finalizado == 1){
						$backg  = '#F00';
						$status = 'finalizado';
					}else{
						$backg  = '#FC0';
						$status = 'em andamento';
					}
?>
                    <input type="text" style="width:200px;height:30px;font-size:22px;text-align:center;background:<?=$backg?>;color:#000;font-weight:bold;margin:6px 0px 0px 90px" id="txt_status" name="txt_status" value="<?= strtoupper($status)?>" readonly="readonly"/>
                </li>
                
            </ul>
		</fieldset>
        
        <ul id="ul_exibir_item" class="menu_modo" style="width:760px;float:none;margin-bottom:5px">
            <li class="item_consignado"><a href="consignado">Itens</a></li>
            <li class="item_recebido"><a href="recebido">Recebidos</a></li>
        </ul>
        
        <div id="item_exibir_consignado">
            <div id="table" style="width:760px">
                <div id="table_cabecalho" style="width:760px">
                    <ul class="table_cabecalho" style="width:760px">
                        <li style="width:100px;	text-align:right">C&oacute;d.</li>
                        <li style="width:280px; text-align:left">Produto</li>
                        <li style="width:80px; text-align:right">Valor</li>
                        <li style="width:60px; text-align:right">Sa&iacute;da</li>
                        <li style="width:60px; text-align:right">Retorno</li>
                        <li style="width:60px; text-align:right">A Prazo</li>
                        <li style="width:60px; text-align:right">A Vista</li>
                    </ul>
                </div>
                <div style="width:760px;height:165px" id="table_container">       
                    <table style="width:740px" id="table_pedido_consignado" class="table_general" summary="Lista de produtos">
                        <tbody>
<?			        		$rsConsignadoItem = mysql_query("SELECT tblpedido_consignado_item.*, tblproduto.fldCodigo FROM tblpedido_consignado_item 
															INNER JOIN tblproduto ON tblpedido_consignado_item.fldProduto_Id = tblproduto.fldId WHERE fldConsignado_Id = $consignado_id");            
							while($rowConsignadoItem = mysql_fetch_array($rsConsignadoItem)){
								$n ++;
								$valor		= $rowConsignadoItem['fldValor'];
								$quantidade = $rowConsignadoItem['fldQuantidade'];
								$avista		= $rowConsignadoItem['fldQuantidade_Avista'];
								$aprazo	 	= $rowConsignadoItem['fldQuantidade_Aprazo'];
								$retorno	= $quantidade - ($avista + $aprazo);
								
								$valor_avista 		= $avista * $valor;
								$valor_aprazo 		= $aprazo * $valor;
								
								$total_valor_avista += $valor_avista;
								$total_valor_aprazo += $valor_aprazo;
?>
                                <tr id="pedido_lista_item" class="lista_produto">
                                    <td style="width:100px"><input 	type="text" 	style="width:100px;text-align:right" id="txt_item_codigo_<?=$n?>" 	 	name="txt_item_codigo_<?=$n?>"		value="<?=$rowConsignadoItem['fldCodigo']?>" 						readonly="readonly" /></td>
                                    <td style="width:280px"><input 	type="text" 	style="width:280px;text-align:left"	 id="txt_item_nome_<?=$n?>"		 	name="txt_item_nome_<?=$n?>"	 	value="<?=$rowConsignadoItem['fldDescricao']?>" 			readonly="readonly" /></td>
                                    <td style="width:80px"><input 	type="text" 	style="width:80px;text-align:right"  id="txt_item_valor_<?=$n?>" 	 	name="txt_item_valor_<?=$n?>" 		value="<?=format_number_out($rowConsignadoItem['fldValor'])?>" 		readonly="readonly" /></td>
                                    <td style="width:60px"><input 	type="text" 	style="width:60px;text-align:right"  id="txt_item_quantidade_<?=$n?>" 	name="txt_item_quantidade_<?=$n?>" 	value="<?=format_number_out($rowConsignadoItem['fldQuantidade'])?>" readonly="readonly" /></td>
                                    <td style="width:60px"><input 	type="text" 	style="width:60px;text-align:right; border:1px inset;background:#D2E9F7"name="txt_item_retorno_<?=$n?>" 	value="<?=format_number_out($retorno)?>" class="retorno"id="txt_item_retorno_<?=$n?>" /></td>
                                    <td style="width:60px"><input 	type="text"  	style="width:60px;text-align:right; border:1px inset;background:#CFC" 	name="txt_item_aprazo_<?=$n?>"		value="<?=format_number_out($aprazo)?>" class="aprazo"	id="txt_item_aprazo_<?=$n?>"  /></td>
                                    <td style="width:60px"><input 	type="text" 	style="width:60px;text-align:right" 									name="txt_item_avista_<?=$n?>" 		value="<?=format_number_out($avista)?>" class="avista" id="txt_item_avista_<?=$n?>" readonly="readonly" /></td>
                                    <td style="display:none"><input type="hidden"	 id="hid_item_id_<?=$n?>"			name="hid_item_id_<?=$n?>" 			value="<?=$rowConsignadoItem['fldId']?>" /></td>
                                </tr>
<?							}
?>							<input type="hidden" id="hid_controle_item" name="hid_controle_item" value="<?=$n?>" />
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div id="item_exibir_recebido">
            <fieldset style=" width:760px; margin-bottom:10px;background:#E1E1E1">
                <ul id="produto_inserir">
                    <li> 
                        <label for="txt_produto_codigo">C&oacute;digo</label>
                         <input type="text" class="codigo" style="width:100px" id="txt_produto_codigo" name="txt_produto_codigo" value="" />
                         <a href="produto_busca" title="Localizar" class="modal" rel="950-450"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                         <input type="hidden" id="hid_produto_id" name="hid_produto_id" value="" />
                    </li>
                    <li> 
                        <label for="txt_produto_nome">Produto</label>
                        <input type="text" style=" width:330px" id="txt_produto_nome" name="txt_produto_nome" value="" />
                    </li>
                    <li> 
                        <label for="txt_produto_valor" style="text-align:center">Valor</label>
                        <input type="text" style=" width:100px;text-align:right" id="txt_produto_valor" name="txt_produto_valor" value="" />
                    </li>
                    <li> 
                        <label for="txt_produto_quantidade" style="text-align:center">Quantidade</label>
                        <input type="text" style=" width:80px;text-align:right" id="txt_produto_quantidade" name="txt_produto_quantidade" value="" />
                    </li>
                    <li style="margin-top:6px">
                      	<button class="btn_sub_small" name="btn_inserir" id="btn_inserir" title="Inserir" >ok</button>
                    </li>
                </ul>
           </fieldset>
           
            <div id="table" style="width:760px">
                <div id="table_cabecalho" style="width:760px">
                    <ul class="table_cabecalho" style="width:760px">
                        <li style="width:80px;	text-align:right">C&oacute;d.</li>
                        <li style="width:320px; text-align:left">Produto</li>
                        <li style="width:100px;	text-align:right">Valor</li>
                        <li style="width:85px; text-align:right">Qtd recebida</li>
                        <li style="width:100px; text-align:right">Total</li>
                    </ul>
                </div>
                <div style="width:760px;height:100px" id="table_container">       
                    <table id="table_pedido_recebido" class="table_general" summary="Lista de produtos">
                        <tbody>
                        	<tr id="pedido_lista_item" class="lista_produto_recebido" style="display:none">
                                <td style="width:80px"><input type="text" 	style="width:80px;text-align:right"  id="txt_item_recebido_codigo" 	 	name="txt_item_recebido_codigo"		value="" readonly="readonly" /></td>
                                <td style="width:320px"><input type="text" 	style="width:320px;text-align:left"	 id="txt_item_recebido_nome"		name="txt_item_recebido_nome"	 	value="" readonly="readonly" /></td>
                                <td style="width:100px"><input type="text" 	style="width:100px;text-align:right" id="txt_item_recebido_valor" 	 	name="txt_item_recebido_valor" 		value="" readonly="readonly" /></td>
                                <td style="width:90px"><input type="text" 	style="width:90px;text-align:right"  id="txt_item_recebido_quantidade"	name="txt_item_recebido_quantidade" value="" readonly="readonly" /></td>
                                <td style="width:100px"><input type="text" 	style="width:100px;text-align:right" id="txt_item_recebido_total"		name="txt_item_recebido_total" 		value="" readonly="readonly" class="item_valor_recebido" /></td>
                                <td style="width:18px"><a class="a_excluir" id="excluir" href="" title="Excluir item"></a></td>
                                <td style="display:none"><input type="hidden" id="hid_item_recebido_produto_id_" name="hid_item_recebido_produto_id_" value="" /></td>
                            </tr>
<?							$n = 0;
							$rsConsignadoItem = mysql_query("SELECT tblpedido_consignado_item_recebido.*, tblproduto.fldCodigo FROM tblpedido_consignado_item_recebido 
															INNER JOIN tblproduto ON tblpedido_consignado_item_recebido.fldProduto_Id = tblproduto.fldId WHERE fldConsignado_Id = $consignado_id");            
							while($rowConsignadoItem = mysql_fetch_array($rsConsignadoItem)){
								$n++;
								
								$total_valor 			= $rowConsignadoItem['fldQuantidade'] * $rowConsignadoItem['fldValor'];
								$total_valor_recebido	+= $total_valor;
?>								
                                <tr id="pedido_lista_item" class="lista_produto_recebido">
                                    <td style="width:80px"><input type="text" 	style="width:80px;text-align:right"  id="txt_item_recebido_codigo_<?=$n?>" 	 	name="txt_item_recebido_codigo_<?=$n?>"		value="<?=$rowConsignadoItem['fldCodigo']?>" readonly="readonly" /></td>
                                    <td style="width:310px"><input type="text" 	style="width:315px;text-align:left"	 id="txt_item_recebido_nome_<?=$n?>"		name="txt_item_recebido_nome_<?=$n?>"	 	value="<?=$rowConsignadoItem['fldDescricao']?>" readonly="readonly" /></td>
                                    <td style="width:100px"><input type="text" 	style="width:100px;text-align:right" id="txt_item_recebido_valor_<?=$n?>" 	 	name="txt_item_recebido_valor_<?=$n?>" 		value="<?=format_number_out($rowConsignadoItem['fldValor'])?>" readonly="readonly" /></td>
                                    <td style="width:90px"><input type="text" 	style="width:90px;text-align:right"  id="txt_item_recebido_quantidade_<?=$n?>"	name="txt_item_recebido_quantidade_<?=$n?>" value="<?=format_number_out($rowConsignadoItem['fldQuantidade'])?>" readonly="readonly" /></td>
                                    <td style="width:100px"><input type="text" 	style="width:100px;text-align:right" id="txt_item_recebido_total_<?=$n?>"		name="txt_item_recebido_total_<?=$n?>" 		value="<?=format_number_out($total_valor)?>" readonly="readonly" class="item_valor_recebido" /></td>
                                	<td style="width:18px"><a class="a_excluir" id="excluir" href="" title="Excluir item"></a></td>
                                    <td style="display:none"><input type="hidden" id="hid_item_recebido_produto_id_<?=$n?>" name="hid_item_recebido_produto_id_<?=$n?>" value="<?=$rowConsignadoItem['fldProduto_Id']?>" /></td>
                                </tr>
<?							} 
?>                        </tbody>
                    </table>
                </div>
                <input type="hidden" id="hid_controle" name="hid_controle" value="<?=$n?>" />
            </div>
		</div>
        
        <div class="saldo" style="width:752px">
            <ul class="table_cabecalho" style="width:752px">
                <li style="width:70px; margin-left:180px">	<span style="text-align:center">A Prazo</span></li>
                <li style="width:100px"><input style="width:100px;text-align:right" class="form_baixa_total" type="text" name="txt_total_aprazo" 	id="txt_total_aprazo" 	value="<?=format_number_out($total_valor_aprazo)?>" 	readonly="readonly" /></li>
                <li style="width:70px">	<span style="text-align:center">A vista</span></li>
                <li style="width:100px"><input style="width:100px;text-align:right" class="form_baixa_total" type="text" name="txt_total_avista" 	id="txt_total_avista" 	value="<?=format_number_out($total_valor_avista)?>" 	readonly="readonly" /></li>
                <li style="width:80px">	<span style="text-align:center">Recebido</span></li>
                <li style="width:100px"><input style="width:100px;text-align:right" class="form_baixa_total" type="text" name="txt_total_recebido" 	id="txt_total_recebido" value="<?=format_number_out($total_valor_recebido)?>" 	readonly="readonly" /></li>
            </ul>
        </div>
       
        <fieldset style="width:558px;padding:5px;float:left;margin:5px;background: #FAEFEB">
            <ul>
                <li>
                    <label for="sel_funcionario_1">Funcionario 1</label>
                    <select style="width:355px" id="sel_funcionario_1" name="sel_funcionario_1" disabled="disabled" >
                        <option value="0">Selecionar</option>
<?						$rsFuncionario = mysql_query("SELECT fldId, fldNome FROM tblfuncionario WHERE fldDisabled = '0' ORDER BY fldNome");
						while($rowFuncionario = mysql_fetch_array($rsFuncionario)){
?>							<option value='<?=$rowFuncionario['fldId']?>'  <?= ($rowConsignado['fldFuncionario1_Id'] == $rowFuncionario['fldId']) ? "selected = 'selected'" : '' ?>><?=$rowFuncionario['fldNome']?></option>
<?						}
?>					</select>
                </li>
                <li> 
                    <label for="txt_funcionario_1_despesa" style="text-align:right">Despesa/Vale (R$)</label>
                    <input type="hidden" id="hid_funcionario_1" name="hid_funcionario_1" value="<?=$rowConsignado['fldFuncionario1_Id']?>" />
                    <input type="text" style=" width:170px;text-align:right" id="txt_funcionario_1_despesa" name="txt_funcionario_1_despesa" value="<?=format_number_out($rowConsignado['fldFuncionario1_Despesa'])?>" <?= ($finalizado == 1) ? 'readonly="readonly"' : '' ?> />
                </li>
                <li>
                    <label for="sel_funcionario_2">Funcionario 2</label>
                    <select style="width:355px" id="sel_funcionario_2" name="sel_funcionario_2" disabled="disabled" >
                        <option value="0">Selecionar</option>
<?						$rsFuncionario = mysql_query("SELECT fldId, fldNome FROM tblfuncionario WHERE fldDisabled = '0' ORDER BY fldNome");
						while($rowFuncionario = mysql_fetch_array($rsFuncionario)){
?>							<option value='<?=$rowFuncionario['fldId']?>' <?= ($rowConsignado['fldFuncionario2_Id'] == $rowFuncionario['fldId']) ? "selected = 'selected'" : '' ?>><?=$rowFuncionario['fldNome']?></option>
<?						}
?>					</select>
               	</li>
               	<li> 
                    <label for="txt_funcionario_2_despesa" style="text-align:right">Despesa/Vale (R$)</label>
                    <input type="hidden" id="hid_funcionario_2" name="hid_funcionario_2" value="<?=$rowConsignado['fldFuncionario2_Id']?>" />
                    <input type="text" style=" width:170px; text-align:right" id="txt_funcionario_2_despesa" name="txt_funcionario_2_despesa" value="<?=format_number_out($rowConsignado['fldFuncionario2_Despesa'])?>" <?= ($finalizado == 1) ? 'readonly="readonly"' : '' ?> />
                </li>
            </ul>
        </fieldset>
        <fieldset style="width:170px;padding:5px;float:left;margin:5px 0px 5px 5px;background: #E4F1ED">
            <ul>
                <li> 
                    <label for="txt_outras_despesas" style="text-align:right">Despesas/Outros</label>
                    <input type="text" style=" width:150px;text-align:right" id="txt_outras_despesas" name="txt_outras_despesas" value="<?=format_number_out($rowConsignado['fldDespesa_Outros'])?>" <?= ($finalizado == 1) ? 'readonly="readonly"' : '' ?> />
                </li>
                <li> 
                    <label for="txt_valor_desconto" style="text-align:right">Descontos</label>
                    <input type="text" style=" width:150px; text-align:right" id="txt_valor_desconto" name="txt_valor_desconto" value="<?=format_number_out($rowConsignado['fldDesconto'])?>" />
                </li>
            </ul>
        </fieldset>
<?
		$total_valor_consignado = $valor_saida + $total_valor_avista + $total_valor_recebido - $rowConsignado['fldFuncionario1_Despesa'] - $rowConsignado['fldFuncionario2_Despesa'] - $rowConsignado['fldDespesa_Outros'] - $rowConsignado['fldDesconto'];
		$total_valor_diferenca	= $total_valor_consignado - $rowConsignado['fldValor_Retorno'];
?>
        
        <fieldset style="width:570px; float:left; margin:5px">
			<ul>
                <li> 
                    <label for="txt_total_valor">Total</label>
                    <input type="text" style=" width:170px;font-size:18px;text-align:right;background:#FF6" id="txt_total_valor" name="txt_total_valor" value="<?=format_number_out($total_valor_consignado)?>" readonly="readonly" />
                </li>
                <li> 
                    <label for="txt_total_retorno">Dinhheiro/Retorno</label>
                    <input type="text" style=" width:170px;font-size:18px;text-align:right;background:#CFC; border:2px solid" id="txt_total_retorno" name="txt_total_retorno" value="<?= format_number_out($rowConsignado['fldValor_Retorno'])?>" />
                </li>
                <li> 
                    <label for="txt_total_diferenca">Diferen&ccedil;a</label>
                    <input type="text" style=" width:170px;font-size:18px;text-align:right" id="txt_total_diferenca" name="txt_total_diferenca" value="<?=format_number_out($total_valor_diferenca)?>" readonly="readonly" />
                </li>
            </ul>
       	</fieldset>
        
        <input type="submit" style="margin-top:14px; margin-left:50px" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="Gravar" title="Salvar" />
    </form>

	<script type="text/javascript">
	
		$('div#item_exibir_recebido').hide();
        $('div#item_exibir_consignado').show();
        $('ul#ul_exibir_item a[href=consignado]').addClass('ativo');
		
		$('#ul_exibir_item a').click(function(event){
			event.preventDefault();
			//marcar aba ativa
			$('ul#ul_exibir_item a').removeClass('ativo');
			$(this).addClass('ativo');
			
			//ocultar todas as abas
			$('div[id^=item_exibir_]').hide();
			
			//exibir a selecionada
			var aba = $(this).attr('href');
			$('div#item_exibir_'+aba).show();
			
			$(this).blur();
		});
		
		
		//##################################################################################################################################################################
		
		function calculoRecebido(){
			total_valor_recebido = 0;
			$.each($("input.item_valor_recebido"),function(){
				
				item_valor_recebido		= br2float(ifNumberNull($(this).val())).toFixed(2);
				total_valor_recebido 	+= Number(item_valor_recebido);
			});
			
			$('#txt_total_recebido').val(float2br(total_valor_recebido.toFixed(2)));
		};
		
		//##################################################################################################################################################################
		function calculoRetorno(){
			
			var total_troco 			= br2float(ifNumberNull($('#txt_valor_saida').val())).toFixed(2);
			
			var total_avista 			= br2float(ifNumberNull($('#txt_total_avista').val())).toFixed(2);
			var total_recebido 			= br2float(ifNumberNull($('#txt_total_recebido').val())).toFixed(2);
			
			var despesa_fucnionario1 	= br2float(ifNumberNull($('#txt_funcionario_1_despesa').val())).toFixed(2);
			var despesa_fucnionario2 	= br2float(ifNumberNull($('#txt_funcionario_2_despesa').val())).toFixed(2);
			var despesa_outros		 	= br2float(ifNumberNull($('#txt_outras_despesas').val())).toFixed(2);
			var total_descontos		 	= br2float(ifNumberNull($('#txt_valor_desconto').val())).toFixed(2);
			var total_valor		 		= br2float(ifNumberNull($('#txt_total_retorno').val())).toFixed(2);
			
			valor_retorno 				= Number(total_troco) + Number(total_avista) + Number(total_recebido) - Number(despesa_fucnionario1) - Number(despesa_fucnionario2) - Number(despesa_outros) - Number(total_descontos);
			valor_diferenca	 			= Number(valor_retorno) - Number(total_valor);
			
			
			$('#txt_total_valor').val(float2br(valor_retorno.toFixed(2)));
			$('#txt_total_diferenca').val(float2br(valor_diferenca.toFixed(2)));
		}
		
		//##################################################################################################################################################################

		function calculoAvista(){
			total_valor_aprazo = 0;
			total_valor_avista = 0;
			total_valor_retorno	 	= br2float(ifNumberNull($('#txt_total_retorno').val())).toFixed(2);
			$.each($("input.aprazo"),function(){
				
				valor_item		 	= br2float(ifNumberNull($(this).parents('tr').find('[name^=txt_item_valor_]').val())).toFixed(2);
				quantidade_avista	= br2float(ifNumberNull($(this).parents('tr').find('[name^=txt_item_avista_]').val())).toFixed(2);
				quantidade_aprazo 	= br2float(ifNumberNull($(this).val())).toFixed(2);
				
				valor_aprazo 		= valor_item * quantidade_aprazo;
				total_valor_aprazo += valor_aprazo;
				
				valor_avista 		= valor_item * quantidade_avista;
				total_valor_avista += valor_avista;
			});
			
			$('#txt_total_avista').val(float2br(total_valor_avista.toFixed(2)));
			$('#txt_total_aprazo').val(float2br(total_valor_aprazo.toFixed(2)));
			
		}
		
		//##################################################################################################################################################################
		$('#txt_produto_codigo').blur(function(){
			if($('#txt_produto_codigo').val() != ''){
				codigo = $(this).val();
				$.get('filtro_ajax.php', {busca_produto:codigo}, function(theXML){
					$('dados',theXML).each(function(){
													
						var produto_id 		= $(this).find("produto_id").text();
						var produto 		= $(this).find("produto").text();
						var valor 			= $(this).find("valor").text();
						
						$('#hid_produto_id').val(produto_id);
						$('#txt_produto_nome').val(unescape(produto));
						$('#txt_produto_valor').val(valor);
						$('#txt_produto_quantidade').focus();
					});
				});
			}else{
				$('#hid_produto_id').val('');
				$('#txt_produto_nome').val('');
				$('#txt_produto_valor').val('');
			}
		});
		
		//##################################################################################################################################################################

		$('#txt_total_avista, #txt_total_recebido, #txt_funcionario_1_despesa, #txt_funcionario_2_despesa,  #txt_valor_desconto, #txt_total_retorno, #txt_outras_despesas').change(function(){
			calculoRetorno();
		});
		
		//##################################################################################################################################################################

		$('#btn_inserir').click(function(event){
			event.preventDefault();
			
			produto_codigo 		= $('#txt_produto_codigo').val();
			produto_id 			= $('#hid_produto_id').val();
			produto_nome		= $('#txt_produto_nome').val();
			produto_valor 		= $('#txt_produto_valor').val();
			produto_quantidade 	= $('#txt_produto_quantidade').val();
		
			produto_total 		= br2float(produto_quantidade).toFixed(2) * br2float(produto_valor).toFixed(2);
			
			if(produto_codigo != '' && produto_nome != '' && produto_quantidade != ''){
				controle = Number($('#hid_controle').val()) + 1;
				
				$('tr.lista_produto_recebido:first').clone(true).appendTo('#table_pedido_recebido').find('tbody');
				
				$('tr.lista_produto_recebido:last').find('#txt_item_recebido_codigo').attr(		{id: 'txt_item_recebido_codigo_'+controle, 		name: 'txt_item_recebido_codigo_'+controle});
				$('tr.lista_produto_recebido:last').find('#hid_item_recebido_produto_id_').attr(			{id: 'hid_item_recebido_produto_id_'+controle,			name: 'hid_item_recebido_produto_id_'+controle});
				$('tr.lista_produto_recebido:last').find('#txt_item_recebido_nome').attr(		{id: 'txt_item_recebido_nome_'+controle, 		name: 'txt_item_recebido_nome_'+controle});
				$('tr.lista_produto_recebido:last').find('#txt_item_recebido_valor').attr(		{id: 'txt_item_recebido_valor_'+controle, 		name: 'txt_item_recebido_valor_'+controle});
				$('tr.lista_produto_recebido:last').find('#txt_item_recebido_quantidade').attr(	{id: 'txt_item_recebido_quantidade_'+controle, 	name: 'txt_item_recebido_quantidade_'+controle});
				$('tr.lista_produto_recebido:last').find('#txt_item_recebido_total').attr(		{id: 'txt_item_recebido_total_'+controle, 		name: 'txt_item_recebido_total_'+controle});
				$('tr.lista_produto_recebido:last').find('#excluir').attr(			 			{id: 'excluir_'+controle});
				
				$('#txt_item_recebido_codigo_'+controle).val(		produto_codigo);
				$('#hid_item_recebido_produto_id_'+controle).val(	produto_id);
				$('#txt_item_recebido_nome_'+controle).val(			produto_nome);
				$('#txt_item_recebido_valor_'+controle).val(		produto_valor);
				$('#txt_item_recebido_quantidade_'+controle).val(	produto_quantidade);
				$('#txt_item_recebido_total_'+controle).val(		float2br(produto_total.toFixed(2)));
				
				$('tr.lista_produto_recebido:last').removeAttr('style');
				$('#hid_controle').val(controle);
				
				$('ul#produto_inserir li').find('input[name!=btn_inserir]').val('');
				$('#txt_produto_codigo').focus();
			}
			
			calculoRecebido();
			calculoRetorno();
		});
		
		//##################################################################################################################################################################
		
		$('.a_excluir').click(function(event){
			event.preventDefault();
			controle 	= $('#hid_controle').val();
			$('#hid_controle').val(Number(controle) - 1);
			
			$(this).parents("tr.lista_produto_recebido").remove();
			
			 calculoRecebido();
			 calculoRetorno();
		});
		
		//##################################################################################################################################################################

		$('input.retorno').blur(function(){
			$(this).parents('tr').find('[name^=txt_item_avista_]').val('0,00')
			
			valor_item		 	= br2float(ifNumberNull($(this).parents('tr').find('[name^=txt_item_valor_]').val())).toFixed(2);
			quantidade_saida 	= br2float(ifNumberNull($(this).parents('tr').find('[name^=txt_item_quantidade_]').val())).toFixed(2);
			quantidade_retorno 	= br2float(ifNumberNull($(this).val())).toFixed(2);
			
			if(Number(quantidade_retorno) > Number(quantidade_saida)){
				quantidade_retorno = quantidade_saida;
			};
			
			quantidade_vendida 	= (quantidade_saida - quantidade_retorno);
			
			$(this).val(float2br(quantidade_retorno));
			$(this).parents('tr').find('[name^=txt_item_aprazo_]').val(float2br(quantidade_vendida.toFixed(2)));
			$(this).parents('tr').find('[name^=txt_item_aprazo_]').attr("readonly", false);
			
			calculoAvista();
		});

		//##################################################################################################################################################################

		$('input.aprazo').blur(function(){
			quantidade_saida 	= br2float(ifNumberNull($(this).parents('tr').find('[name^=txt_item_quantidade_]').val())).toFixed(2);
			quantidade_retorno 	= br2float(ifNumberNull($(this).parents('tr').find('[name^=txt_item_retorno_]').val())).toFixed(2);
			quantidade_aprazo 	= br2float(ifNumberNull($(this).val())).toFixed(2);
			
			if(quantidade_aprazo > (quantidade_saida - quantidade_retorno)){
				quantidade_aprazo = (quantidade_saida - quantidade_retorno);
			};
			
			quantidade_avista = (quantidade_saida - quantidade_retorno) - quantidade_aprazo;
			
			$(this).val(float2br(quantidade_aprazo));
			$(this).parents('tr').find('[name^=txt_item_avista_]').val(float2br(quantidade_avista.toFixed(2)));
		
			calculoAvista();
			calculoRetorno();
		});

		//##################################################################################################################################################################

		$('#txt_produto_valor, #txt_produto_quantidade, #txt_valor_retorno, #txt_outras_despesas, #txt_funcionario_1_despesa, #txt_funcionario_2_despesa, #txt_valor_desconto, #txt_total_retorno, input.retorno, input.avista ').blur(function(){
			$(this).val(float2br(br2float(ifNumberNull($(this).val())).toFixed(2)));
		});		

		//##################################################################################################################################################################

		$('#frm_pedido_consignado_fechar').submit(function(e){
			e.preventDefault();
			
			var form 	= $(this).serialize();
			$('div.modal-conteudo').load('modal/pedido_consignado_detalhe.php', {form : form});
		});
		
		//##################################################################################################################################################################
	</script>