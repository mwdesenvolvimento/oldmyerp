<?	
	ob_start();
	session_start();
	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	require_once('../inc/fnc_financeiro.php');
	
	###################################################################################################################################################################################################
	if(isset($_POST["form"])){
		
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$registro_id 	= $form['hid_movimento_id'];
		$descricao 		= mysql_escape_string($form['txt_descricao']);
		$entidade 		= mysql_escape_string($form['txt_entidade']);
		$marcador 		= $form['sel_marcador'];
		$pagamento_tipo = $form['sel_pagamento'];
		
		if($form['sel_tipo'] == '1'){
			$credito	= format_number_in($form['txt_valor']);
			$debito 	= '0,00';
		}elseif($form['sel_tipo'] == '2'){
			$credito 	= '0,00';
			$debito 	= format_number_in($form['txt_valor']);
		}
		
		$sqlInsert = mysql_query("UPDATE tblfinanceiro_conta_fluxo SET
		fldDescricao 			= '$descricao',
		fldEntidade	 			= '$entidade',
		fldPagamento_Tipo_Id 	= '$pagamento_tipo',
		fldCredito 				= '$credito',
		fldDebito 				= '$debito',
		fldMarcador_Id 			= '$marcador'
		WHERE fldId 			= $registro_id");
	
		
		
		if(!empty($_SESSION['ref_timestamp'])){ //CASO A SESSAO ESTIVER EM BRANCO, PRA NAO ATUALIZAR TODOS OS CHEQUES 
			/*
			$timestamp = $_SESSION['ref_timestamp'];
			fnc_cheque_update($origem_id, $destino_id, $entidade, $origem_movimento_id, $destino_movimento_id, $timestamp);
			*/
			###############################################################################################################################################
			
			if($form['sel_tipo'] ==  '2'){
				$saida = '1';
			}
			$rowConta 	 = mysql_fetch_assoc(mysql_query("SELECT fldConta_Id FROM tblfinanceiro_conta_fluxo WHERE fldId = {$registro_id}"));
			$timestamp	 = $_SESSION['ref_timestamp'];
			$movimento_id= '1';
			$registro_id = $registro_id;
			$conta_id	 = $rowConta['fldConta_Id'];
			fnc_cheque_movimento_lancar($timestamp, $movimento_id, $registro_id, $conta_id, $saida);
			
		}
		unset($_SESSION['ref_timestamp']);
?>
        <img src="image/layout/carregando.gif" alt="carregando..." />
        <script type="text/javascript">
			window.location="index.php?p=financeiro&modo=conta_fluxo";
        </script> 
<?		die;
	}
	###################################################################################################################################################################################################
	$registro_id = $_POST['params'][1];

	$sSQL = "SELECT tblfinanceiro_conta_fluxo.*, tblfinanceiro_conta_fluxo_tipo.fldTipo as fldTipo_Descricao, tblfinanceiro_conta_fluxo.fldDescricao as fldMovimento_Descricao, tblfinanceiro_conta_fluxo.fldId as fldRegistroId, tblfinanceiro_conta_fluxo_marcador.*, tblfinanceiro_conta.fldId
			FROM tblfinanceiro_conta_fluxo
			INNER JOIN tblfinanceiro_conta ON tblfinanceiro_conta_fluxo.fldConta_Id = tblfinanceiro_conta.fldId
			INNER JOIN tblfinanceiro_conta_fluxo_tipo ON tblfinanceiro_conta_fluxo.fldMovimento_Tipo = tblfinanceiro_conta_fluxo_tipo.fldId
			LEFT  JOIN tblfinanceiro_conta_fluxo_marcador ON tblfinanceiro_conta_fluxo.fldMarcador_Id = tblfinanceiro_conta_fluxo_marcador.fldId
			WHERE tblfinanceiro_conta_fluxo.fldId = $registro_id";
			
	$rsRegistro = mysql_query($sSQL);
	echo mysql_error();
	$rowRegistro = mysql_fetch_array($rsRegistro);

	if($rowRegistro['fldCredito'] > 0){ 
		$valor 	= $rowRegistro['fldCredito'];
		$tipo 	= "1";
	}elseif($rowRegistro['fldDebito'] > 0){ 
		$valor 	= $rowRegistro['fldDebito'];
		$tipo 	= "2";
	}
	
	$remote_ip 	= gethostbyname($REMOTE_ADDR);
	$_SESSION['ref_timestamp'] = $remote_ip.date("YmdHis");
	
	$contaId 	= $_SESSION['sel_conta_id'];
	$data_caixa = fnc_caixa_data($contaId);
	
	$input_disabled 	 = ($rowRegistro['fldData'] == $data_caixa && $rowRegistro['fldMovimento_Tipo'] == '1')? '' : 'disabled="disabled"';
	$link_disabled 		 = ($rowRegistro['fldData'] == $data_caixa && $rowRegistro['fldMovimento_Tipo'] == '1')? '' : 'pointer-events: none';
	$movimento_descricao = $rowRegistro['fldTipo_Descricao'];
	$movimento_entidade	 = $rowRegistro['fldEntidade'];

	switch($rowRegistro['fldMovimento_Tipo']){
		case '1': #movimento caixa
			$movimento_descricao = $rowRegistro['fldMovimento_Descricao'];
		break;
		case '2':
			$movimento_descricao = "Transferido para conta";
		break; 
		case '3': 
			$rsEntidade = mysql_query("SELECT tblcliente.fldNome, tblpedido.fldId as fldPedido_Id, tblpedido_parcela.fldParcela
											FROM tblpedido_parcela_baixa
											LEFT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId
											INNER JOIN tblpedido ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId
											INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
											WHERE tblpedido_parcela_baixa.fldId =".$rowRegistro['fldReferencia_Id']);
			$rowEntidade = mysql_fetch_array($rsEntidade);
			$movimento_entidade	= $rowEntidade['fldNome'];
			$label_referencia	= "Venda";
			$referencia_id		= $rowEntidade['fldPedido_Id'];
			$referencia_parcela	= $rowEntidade['fldParcela'];
			
		break;
		case '4':
			$rsEntidade = mysql_query("SELECT tblfornecedor.fldNomeFantasia, tblcompra.fldId as fldCompra_Id, tblcompra_parcela.fldParcela
											FROM tblcompra_parcela_baixa
											LEFT JOIN tblcompra_parcela ON tblcompra_parcela_baixa.fldParcela_Id = tblcompra_parcela.fldId
											LEFT JOIN tblcompra ON tblcompra_parcela.fldCompra_Id = tblcompra.fldId
											LEFT JOIN tblfornecedor ON tblcompra.fldFornecedor_Id = tblfornecedor.fldId
											WHERE tblcompra_parcela_baixa.fldId =".$rowRegistro['fldReferencia_Id']);
			$rowEntidade = mysql_fetch_array($rsEntidade);
			$movimento_entidade = $rowEntidade['fldNomeFantasia'];
			$label_referencia	= "Compra";
			$referencia_id		= $rowEntidade['fldCompra_Id'];
			$referencia_parcela	= $rowEntidade['fldParcela'];
		break;
		case '5':
			$movimento_entidade  = $rowRegistro['fldMovimento_Descricao'];
		break;
		case '6':
			$rsEntidade = mysql_query("SELECT tblfinanceiro_conta_pagar_programada.fldNome
											FROM tblfinanceiro_conta_pagar_programada_baixa
											LEFT JOIN tblfinanceiro_conta_pagar_programada 
											ON tblfinanceiro_conta_pagar_programada_baixa.fldContaProgramada_Id = tblfinanceiro_conta_pagar_programada.fldId
											WHERE tblfinanceiro_conta_pagar_programada_baixa.fldId =".$rowRegistro['fldReferencia_Id']);
			$rowEntidade = mysql_fetch_array($rsEntidade);
			$movimento_entidade  = $rowEntidade['fldNome'];
		break;
		case '7':
		case '8':
		case '9':
			$rsEntidade = mysql_query("SELECT tblfuncionario.fldNome
											FROM tblfuncionario_conta_fluxo
											LEFT JOIN tblfuncionario 
											ON tblfuncionario_conta_fluxo.fldFuncionario_Id = tblfuncionario.fldId
											WHERE tblfuncionario_conta_fluxo.fldId =".$rowRegistro['fldReferencia_Id']);
			$rowEntidade = mysql_fetch_array($rsEntidade);
			$movimento_entidade  = $rowEntidade['fldNome'];
		break;			
		
	}
	/*
	//tipo de movimentação (por id)
		1 = Movimento caixa
		2 = Transferência de conta
		3 = Recebimento de parcela
		4 = Pagamento de parcela
		5 = Estorno	
		6 = Pagamento Conta Programada
		7 = Comissao de Funcionario
		8 = Vale Funcionario
		9 = Pagamento de Funcinario
	*/
?>	
	<div class="form" style="width:860px">
        <form style="width:840px" class="frm_detalhe" id="frm_financeiro_detalhe" action="" method="post">
        	<input type="hidden" name="hid_movimento_id" id="hid_movimento_id" value="<?=$registro_id?>" />
            <ul>
            	<li>
                    <label for="txt_id">Id</label>
                    <input type="text" style="width:80px; text-align:right" id="txt_id" name="txt_id" value="<?=str_pad($rowRegistro['fldRegistroId'], 5,"0", STR_PAD_LEFT)?>" readonly="readonly" />
                </li>
            	<li>
                    <label for="txt_id">Data</label>
                    <input type="text" style="width:80px; text-align:center" id="txt_data" name="txt_data" value="<?=format_date_out($rowRegistro['fldData'])?>" readonly="readonly" />
                </li> 
<?				if($rowRegistro['fldMovimento_Tipo'] == '3' || $rowRegistro['fldMovimento_Tipo'] == '4'){                
?>					<li>
                        <label for="txt_referencia"><?=$label_referencia?></label>
                        <input type="text" style="width:80px; text-align:right" id="txt_referencia" name="txt_referencia" value="<?=str_pad($referencia_id, 5,"0", STR_PAD_LEFT)?>" readonly="readonly" />
                    </li>
                    <li>
                        <label for="txt_referencia_parcela">Parcela</label>
                        <input type="text" style="width:40px; text-align:right" id="txt_referencia_parcela" name="txt_referencia_parcela" value="<?=str_pad($referencia_parcela, 2,"0", STR_PAD_LEFT)?>" readonly="readonly" />
                    </li> 
<?				}
?>
           	</ul>
            <ul>               
                <li>
                    <label for="txt_descricao">Descri&ccedil;&atilde;o</label>
                    <input type="text" style="width:175px" id="txt_descricao" name="txt_descricao" value="<?=$movimento_descricao?>" <?=$input_disabled?> />
                </li> 
            	<li>
                    <label for="txt_entidade">Entidade</label>
                    <input type="text" style="width:210px" id="txt_entidade" name="txt_entidade" value="<?=$movimento_entidade?>" <?=$input_disabled?> />
                </li> 
              	<li>
					<label for="sel_marcador">Marcador</label>
					<select style="width:140px" id="sel_marcador" name="sel_marcador" <?=$input_disabled?>>
                    	<option value="0"></option>
<?						$rsMarcador  = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_marcador ORDER BY fldMarcador");
						while($rowMarcador = mysql_fetch_array($rsMarcador)){
?>							<option <?=($rowRegistro['fldMarcador_Id'] == $rowMarcador['fldId']) ? print "selected='selected'" : '' ?> title="<?=$rowMarcador['fldDescricao'] ?>" value="<?=$rowMarcador['fldId'] ?>"><?=$rowMarcador['fldMarcador'] ?></option>
<? 						}
?>					</select>
                </li> 
                <li>
                	<label for="txt_tipo">Tipo</label>
					<select style="width:70px" id="sel_tipo" name="sel_tipo" <?=$input_disabled?> >
                    	<option <?=($tipo == '1') ? print " selected='selected'" : '' ?> value="1">Cr&eacute;dito</option>
                    	<option <?=($tipo == '2') ? print " selected='selected'" : '' ?> value="2">D&eacute;bito</option>
					</select>
            	</li>
                <li>
                	<label for="sel_pagamento">Forma pag.</label>
					<select style="width:100px" id="sel_pagamento" name="sel_pagamento" <?=$input_disabled?> >
<?						$rsPagamento = mysql_query("select * from tblpagamento_tipo");
                        while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>							<option <?=($rowRegistro['fldPagamento_Tipo_Id'] == $rowPagamento['fldId']) ? print "selected='selected'" : '' ?> value="<?=$rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo']?></option>
<?						}
?> 					</select>
            	</li>
                <li>
                    <label for="txt_valor">Valor</label>
                    <input type="text" style="width:70px; text-align:right" id="txt_valor" name="txt_valor" value="<?=format_number_out($valor)?>" <?=$input_disabled?>/>
                </li>
                
<?				if($rowRegistro['fldData'] == $data_caixa && $rowRegistro['fldMovimento_Tipo'] == '1'){              
?>
                    <li> 
                        <a style="margin:0" class="modal btn_cheque" name="btn_cheque" id="btn_cheque" title="cheques" href="financeiro_cheque_listar,1,<?=$rowRegistro['fldRegistroId']?>" rel="780-420">inserir cheques</a>
                    </li>
<?				}
?>              <li style="float:right; margin-right:6px">
                    <input type="submit" style="margin:0" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="gravar" title="Gravar" <?=$input_disabled?> />
                </li>
			</ul>
        </form>
	</div>
    
    
	<script type="text/javascript">
       	$('#txt_descricao').focus();
			 
        $('#txt_valor').blur(function(){
			$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		});
        $('#btn_gravar').click(function(event){
            event.preventDefault();
            var form 	= $('#frm_financeiro_detalhe').serialize();
            $('div.modal-conteudo:last').load('modal/financeiro_conta_fluxo_detalhe.php', {form : form});
    
        });	
        
    </script>
    
    
