<?php
	
	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	$nfe_id = $_POST['params'][1];
	
	$sSQL = "SELECT 
				tblpedido.fldPedidoData,
				tblpedido.fldId as fldPedidoId,
				tblpedido.fldStatus,
				tblpedido.fldDesconto,
				tblpedido.fldDescontoReais,
				tblpedido.fldValor_Terceiros,
				tblcliente.fldNome AS fldNomeCliente
			FROM tblpedido
				INNER JOIN tblcliente 	ON tblpedido.fldCliente_Id 		= tblcliente.fldId
			WHERE tblpedido.fldPedido_Destino_Nfe_Id = '$nfe_id'";
	$rsPedido = mysql_query($sSQL);			
?>	
    <div id="table" style="width: 320px">
        <div id="table_cabecalho" style="width: 320px">
            <ul class="table_cabecalho" style="width: 320px">
                <li style="width:90px; padding-left:10px">C&oacute;d.</li>
                <li style="width:90px; text-align:right">Emiss&atilde;o</li>
                <li style="width:100px; text-align:right">Total</li>
            </ul>
        </div>
        <div id="table_container" style="width:320px">       
            <table id="table_general" class="table_general" summary="Lista de vendas de origem">
                <tbody>
<?	
					$linha = "row";
					while($rowPedido = mysql_fetch_array($rsPedido)){
						
						$subTotalPedido = 0;
						$pedido_id 		= $rowPedido['fldPedidoId'];
						$id_array[$n] 	= $pedido_id;
						$n += 1;
						
						$sqlItem = "SELECT SUM((fldValor * fldQuantidade) - (((fldValor * fldQuantidade) * fldDesconto) / 100)) AS fldTotalItem, COUNT(fldQuantidade) as fldTotalQuantidade
											  FROM tblpedido_item WHERE fldPedido_Id = $pedido_id ";
											  
						$sqlServico = "SELECT SUM(fldValor) as fldValorServico FROM	tblpedido_funcionario_servico WHERE fldFuncao_Tipo = 2 AND fldPedido_Id = $pedido_id ";
						
						$rowItem 			= mysql_fetch_array(mysql_query($sqlItem));
						$rowServico 		= mysql_fetch_array(mysql_query($sqlServico));
						
						$totalItem			= $rowItem['fldTotalItem'];
						$itemQtd			= $rowItem['fldTotalQuantidade'];	
						$subTotalPedido		= (($totalItem 		+ $rowPedido['fldComissao']) + $rowPedido['fldValor_Terceiros']) + $rowServico['fldValorServico'];
					
						$descPedido 		= $rowPedido['fldDesconto'];
						$descPedidoReais 	= $rowPedido['fldDescontoReais'];
						
						$descontoPedido 	= ($subTotalPedido 	* $descPedido) / 100;
						$totalPedido 		= $subTotalPedido 	- $descontoPedido;
						$totalPedido 		= $totalPedido 		- $descPedidoReais;
						
?>						<tr class="<?= $linha; ?>">
                            <td style="width:90px; 	padding-left:10px;"><a href="?p=pedido_detalhe&id=<?=$rowPedido['fldPedidoId']?>" title="abrir venda de origem" target="_blank"><?=str_pad($rowPedido['fldPedidoId'], 4, "0", STR_PAD_LEFT)?></a></td>
                            <td style="width:90px; 	text-align:right"><?=format_date_out($rowPedido['fldPedidoData'])?></td>
                            <td style="width:100px; text-align:right;"><?=format_number_out($totalPedido)?></td>
                        </tr>
<?								
						$linha = ($linha == "row" )?"dif-row":"row";
					}
?>		 		</tbody>
            </table>
        </div>
