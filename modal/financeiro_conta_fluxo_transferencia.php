<?	
	ob_start();
	session_start();
	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	require_once('../inc/fnc_financeiro.php');
 	
	###################################################################################################################################################################################################
	if(isset($_POST["form"])){
		
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$conta_destino	= $form['sel_conta'];
		$valor 			= format_number_in($form['txt_valor']);	
		$pagamento_tipo = $form['sel_pagamento'];
		$conta_origem	= $_SESSION['sel_conta_id'];
		$movimento_tipo = '2';
		
		//$descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta
		//debitando na conta origem
		fnc_financeiro_conta_fluxo_lancar('', '', $valor, '', $pagamento_tipo, '', $movimento_tipo, $marcador, $conta_origem,  '', $conta_destino);
		//creditando na conta destino
		fnc_financeiro_conta_fluxo_lancar('', $valor, '', '', $pagamento_tipo, '', $movimento_tipo, $marcador, $conta_destino, $conta_origem);
		
		echo mysql_error();
?>
        <img src="image/layout/carregando.gif" alt="carregando..." />
        <script type="text/javascript">
			window.location="index.php?p=financeiro&modo=conta_fluxo";
        </script> 
<?		die;
	}
	
?>	
	<div class="form" style="width:660px">
        <form class="frm_detalhe" id="frm_financeiro_transferencia" action="" method="post">
            <ul>
                <li>
                    <label for="txt_valor">Valor</label>
                    <input type="text" style="width:100px;text-align:right" id="txt_valor" name="txt_valor" value="" />
                </li> 
                <li>
                	<label for="sel_pagamento">Forma pag.</label>
					<select style="width:200px" id="sel_pagamento" name="sel_pagamento" >
<?						$rsPagamento = mysql_query("select * from tblpagamento_tipo");
                        while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>							<option value="<?=$rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo']?></option>
<?						}
?> 					</select>
            	</li>
            	<li>
                	<label for="sel_conta">Mover para conta</label>
					<select style="width:180px" id="sel_conta" name="sel_conta" >
<?						$rsConta = mysql_query("select * from tblfinanceiro_conta where fldId !=".$_SESSION['sel_conta_id']);
                        while($rowConta= mysql_fetch_array($rsConta)){
?>							<option value="<?=$rowConta['fldId'] ?>"><?=$rowConta['fldNome'] ?></option>
<?						}
?> 					</select>
            	</li>
                <li style="float:right; margin-right:10px; margin-top:0">
                    <input type="submit" style="margin-top:16px" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="gravar" title="Gravar" />
                </li>
             </ul>
        </form>
	</div>

<script type="text/javascript">
	$('#txt_valor').focus();	 
	$('#txt_valor').blur(function(){
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
	});
	$('#btn_gravar').click(function(event){
		event.preventDefault();
		valor		= br2float($('#txt_valor').val());
		if(valor > 0){
			$('#btn_gravar').attr('disabled', 'disabled');
			var form 	= $('#frm_financeiro_transferencia').serialize();
			$('div.modal-conteudo:last').load('modal/financeiro_conta_fluxo_transferencia.php', {form : form});
		}else{
			alert("Valor inválido!");
			$('#txt_valor').focus();
		}
	});	
	
</script>