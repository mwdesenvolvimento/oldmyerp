<script src="js/general.js"  charset="UTF-8"></script>

<?
	ob_start();
	session_start();
	
    require('../inc/con_db.php');
    require('../inc/fnc_general.php');
    require('../inc/fnc_estoque.php');
	
	$produto_id = $_POST['params'][1];
	$saldo		= $_POST['params'][2];
	$estoque_id	= $_SESSION['sel_produto_estoque_id'];

	if(isset($_POST["form"])){
		
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$produto_id = $form['hid_produto_id'];
		$quantidade = format_number_in($form['txt_quantidade']);
		$descricao 	= $form['txt_descricao'];
		
		if($form['sel_alteracao'] == 1){
			$entrada 	= $quantidade;
			$saida 		= '';
			$validade	= format_date_in($form['txt_validade']);
		
		}else{
			$entrada 	= '';
			$saida 		= $quantidade;
			$validade	= $form['sel_validade'];
		
		}
		
		fnc_estoque_movimento_lancar($produto_id, $descricao, $entrada, $saida, 7, '', $_SESSION['sel_produto_estoque_id'], $_SESSION['sel_produto_estoque_id'], $validade);
?>
        <img src="image/layout/carregando.gif" alt="carregando..." />
        <script type="text/javascript">
			produto_id = '<?= $produto_id?>';
			window.location="index.php?p=produto_detalhe&id="+produto_id+"&modo=estoque";
        </script> 
<?		die;
	}

?>
<form id="frm_estoque" action="" method="post" class="frm_detalhe">
	<input type="hidden" value="<?=$produto_id?>" name="hid_produto_id" />
	<ul>
        <li>
        	<label for="txt_quantidade">quantidade</label>
        	<input style="width:70px" type="text" id="txt_quantidade" name="txt_quantidade" />
		</li>
		<li>
        	<label for="sel_alteracao">&nbsp;</label>
        	<select style="width:80px" id="sel_alteracao" name="sel_alteracao" >
            	<option value="1">adicionar</option>
            	<option value="0">retirar</option>
			</select>
        </li>
        <li>
        	<label for="txt_descricao">Descri&ccedil;&atilde;o (opcional)</label>
        	<input style="width: 320px" type="text" id="txt_descricao" name="txt_descricao" maxlength="255" />
		</li>
        <li class="txt_validade">
        	<label for="txt_validade">Validade</label>
        	<input style="width:90px" type="text" id="txt_validade" name="txt_validade" class="calendario-mask" />
		</li>
        <li class="sel_validade">
        	<label for="sel_validade">Validade</label>
        	<select style="width:95px" id="sel_validade" name="sel_validade" >
            	<option class="<?=$saldo?>" value=""></option>
<?				$rsValidade = mysql_query("SELECT SUM(fldEntrada - fldSaida) as fldRestante, fldValidade_Data, fldProduto_Id from tblproduto_estoque_movimento
											WHERE fldProduto_Id = $produto_id AND fldEstoque_Id = $estoque_id  AND fldValidade_Data IS NOT NULL GROUP BY fldProduto_Id, fldValidade_Data HAVING fldRestante > 0 ");
				while($rowValidade = mysql_fetch_array($rsValidade)){
?>            		<option class="<?=$rowValidade['fldRestante']?>" value="<?=$rowValidade['fldValidade_Data']?>" ><?= format_date_out($rowValidade['fldValidade_Data'])?></option>
<?				}
?>			</select>
		</li>
		<li><input type="submit" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" /></li>
	</ul>
	
</form>

<script type="text/javascript">
	
	$('li.sel_validade').hide();
	$('#txt_quantidade').select();
	
	$('#sel_alteracao').change(function(){
		if($(this).val() == 0 ){
			$('li.txt_validade').hide();
			$('li.sel_validade').show();
		}else{
			$('li.txt_validade').show();
			$('li.sel_validade').hide();
		}
	});

	$('#frm_estoque').submit(function(e){
		e.preventDefault();
		
		var form 	= $(this).serialize();
			
		if(($('#sel_alteracao').val() == 0) && $('#hid_estoque_controle').val() == '2' ){
			quantidade 	= br2float($('#txt_quantidade').val());
			saldo		= $('#sel_validade').find('option').filter(':selected').attr('class');
			if(quantidade > saldo){
				alert('Quantidade insulficiente para validade selecionada.');
				return false;
			}
		}
		$('div.modal-conteudo:last').load('modal/produto_estoque_alterar.php', {form : form});
	});	
	
</script>