<div id="principal">
<?
	
	if(isset($_POST["btn_upload"])){
		
		$file = $_FILES['file'];
		$filename = $file['name'];	

		$path     = $file['tmp_name'];
		
		$nome = "logo_empresa.jpg";
		$new_path = "image/layout/".$nome;

		if(move_uploaded_file($path, $new_path)) {
			
			// Vamos usar a biblioteca WideImage para o redimensionamento das imagens
			require("lib/WideImage/WideImage.php");
			
			// Carrega a imagem enviada
			$original = WideImage::load($new_path);
			
			// Redimensiona a imagem original para 1024x768 caso ela seja maior que isto e salva
			$original->resize(200, 120, 'inside', 'any')->saveToFile($new_path, null, 90);

		}
	}
		
	if(isset($_POST["btn_enviar"])){
		$rsEmpresa = mysql_query("SELECT * FROM tblempresa_info");
		$rowsEmpresa = mysql_num_rows($rsEmpresa);
		
		$RazaoSocial = mysql_real_escape_string($_POST['txt_razao_social']);
		$NomeFantasia = mysql_real_escape_string($_POST['txt_nome_fantasia']);
		$Tipo = $_POST['sel_tipo'];
		$Fone1 = $_POST['txt_telefone1'];
		$Fone2 = $_POST['txt_telefone2'];
		$Email = $_POST['txt_email'];
		$Site = $_POST['txt_website'];
		$Endereco = mysql_real_escape_string($_POST['txt_endereco']);
		$Numero = $_POST['txt_numero'];
		$Bairro = $_POST['txt_bairro'];
		$CEP = $_POST['txt_cep'];
		$Municipio_Cod = $_POST['txt_municipio_codigo'];
		$IE	 = $_POST['txt_ie'];
		
		//ver se � fisica ou juridica e formatar cpf/cnpj
		if($Tipo == 1){
			$CPF_CNPJ = format_cpf_in($_POST['txt_cpf_cnpj']);
		}elseif($Tipo == 2){
			$CPF_CNPJ = format_cnpj_in($_POST['txt_cpf_cnpj']);
		}

		if($rowsEmpresa > 0){
			
			$sqlInsert = "UPDATE tblempresa_info SET
			fldRazao_social = '" . $RazaoSocial . "', 
			fldNome_Fantasia = '" . $NomeFantasia . "',
			fldTipo = '" . $Tipo . "', 
			fldCPF_CNPJ = '" . $CPF_CNPJ . "',
			fldTelefone1 = '" . $Fone1 . "',
			fldTelefone2 = '" . $Fone2 . "',
			fldEmail = '" . $Email . "',
			fldWebsite = '" . $Site . "',
			fldEndereco = '" . $Endereco . "',  
			fldNumero = '" . $Numero . "', 
			fldBairro = '" . $Bairro . "', 
			fldCEP = '" . $CEP . "', 
			fldMunicipio_Codigo = '" . $Municipio_Cod . "',
			fldIE = '" . $IE . "'";
			
		}else{
			
			$sqlInsert = "INSERT INTO tblempresa_info (fldRazao_social, fldNome_Fantasia, fldTipo, fldCPF_CNPJ, fldTelefone1, fldTelefone2, fldEmail, 
													   fldWebsite, fldEndereco, fldNumero, fldBairro, fldCEP, fldMunicipio_Codigo, fldIE) VALUES(
			'" . $RazaoSocial . "', 
			'" . $NomeFantasia . "',
			'" . $Tipo . "', 
			'" . $CPF_CNPJ . "',
			'" . $Fone1 . "',
			'" . $Fone2 . "',
			'" . $Email . "',
			'" . $Site . "',
			'" . $Endereco . "',  
			'" . $Numero . "', 
			'" . $Bairro . "', 
			'" . $CEPs . "', 
			'" . $Municipio_Cod . "', 
			'" . $IE . "'
			)";
		}
		
		if(mysql_query($sqlInsert)){
			echo mysql_error();
?>			<div class="alert" style="margin-top: 5px">
            	<p class="ok">Cadastro atualizado</p>
            </div>
<?		}
		else{
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
			</div>
<?			echo mysql_error();
		}
	}
	$rsEmpresa = mysql_query("SELECT * FROM tblempresa_info");	
	$row = mysql_fetch_array($rsEmpresa);
		
	if($row['fldTipo'] == 1 && $row['fldCPF_CNPJ'] != NULL){
		$CPF_CNPJ = format_cpf_out($row['fldCPF_CNPJ']);
	}elseif($row['fldTipo'] == 2 && $row['fldCPF_CNPJ'] != NULL){
		$CPF_CNPJ = format_cnpj_out($row['fldCPF_CNPJ']);
	}else{
		$CPF_CNPJ = '';
	}
	
?>		<div class="form">
            <form id="frm_empresa" class="frm_detalhe" style="width:890px" action="" method="post">
                <ul>
                    <li>
                        <label for="txt_razao_social">Raz&atilde;o Social</label>
                        <input type="text" style=" width:360px;" id="txt_razao_social" name="txt_razao_social" value="<?=$row['fldRazao_Social']?>" />
                    </li>
                    <li>
                        <label for="txt_nome_fantasia">Nome Fantasia</label>
                        <input type="text" style=" width:300px;" id="txt_nome_fantasia" name="txt_nome_fantasia" value="<?=$row['fldNome_Fantasia']?>" />
                    </li>
                    <li>
                        <label for="sel_tipo">Tipo</label>
                        <select style="width: 150px;" id="sel_tipo" name="sel_tipo" >
                            <option <?=($row['fldTipo'] == 1) ? 'selected="selected"' : '' ?> value="1">F&iacute;sica</option>
                            <option <?=($row['fldTipo'] == 2) ? 'selected="selected"' : '' ?> value="2">Jur&iacute;dica</option>
                        </select>
                    </li>
                    <li>
                        <label for="txt_cpf_cnpj">CPF/CNPJ</label>
                        <input type="text" style=" width:120px;" id="txt_cpf_cnpj" name="txt_cpf_cnpj" value="<?=$CPF_CNPJ?>" />
                    </li>
                    <li>
                        <label for="txt_ie">Insc. Estadual</label>
                        <input type="text" style=" width:120px;" id="txt_ie" name="txt_ie" value="<?=$row['fldIE']?>" />
                    </li>
                    <li>
                        <label for="txt_telefone1">Telefone (ddd)</label>
                        <input type="text" style=" width:120px;" id="txt_telefone1" name="txt_telefone1" value="<?=$row['fldTelefone1']?>" />
                    </li>
                    <li>
                        <label for="txt_telefone2">Telefone 2 (ddd)</label>
                        <input type="text" style=" width:120px;" id="txt_telefone2" name="txt_telefone2" value="<?=$row['fldTelefone2']?>" />
                    </li>
                    <li>
                        <label for="txt_email">E-mail</label>
                        <input type="text" style=" width:295px;" id="txt_email" name="txt_email" value="<?=$row['fldEmail']?>" />
                    </li>
                    <li>
                        <label for="txt_website">Website</label>
                        <input type="text" style=" width:235px;" id="txt_website" name="txt_website" value="<?=$row['fldWebsite']?>" />
                    </li>
                    <li>
                        <label for="txt_endereco">Endere&ccedil;o</label>
                        <input type="text" style=" width:325px;" id="txt_endereco" name="txt_endereco" value="<?=$row['fldEndereco']?>" />
                    </li>
                    <li>
                        <label for="txt_numero">N&uacute;mero</label>
                        <input type="text" style=" width:60px;" id="txt_numero" name="txt_numero" value="<?=$row['fldNumero']?>" />
                    </li>
                    <li>
                        <label for="txt_bairro">Bairro</label>
                        <input type="text" style=" width:172px;" id="txt_bairro" name="txt_bairro" value="<?=$row['fldBairro']?>" />
                    </li>
                    <li>
                        <label for="txt_cep">CEP</label>
                        <input type="text" style=" width:172px;" id="txt_cep" name="txt_cep" value="<?=$row['fldCEP']?>" />
                    </li>
                    <li>
                        <label for="txt_municipio_codigo">C&oacute;d. Munic&iacute;pio</label>
                        <input type="text" style=" width:80px;" id="txt_municipio_codigo" name="txt_municipio_codigo" value="<?=$row['fldMunicipio_Codigo']?>" />
                        <a href="municipio_busca" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
                    
<?					$Mcodigo = substr($row['fldMunicipio_Codigo'],2,5);

					$rsMunicipio = mysql_query("select * from tblibge_municipio where fldCodigo = '" . $Mcodigo . "'");
					$rowMunicipio = mysql_fetch_array($rsMunicipio);
					
					$UFCodigo = substr($row['fldMunicipio_Codigo'],0,2);
					
					$rsUF = mysql_query("select * from tblibge_uf where fldCodigo = '" . $UFCodigo . "'");
					$rowUF = mysql_fetch_array($rsUF);
?>                
                    <li>
                        <label for="txt_municipio">Munic&iacute;pio</label>
                        <input type="text" style=" width:415px; background:#EAEAEA;" id="txt_municipio" name="txt_municipio" readonly="readonly" value="<?=$rowMunicipio['fldNome']?>" />
                    </li>
                    <li>
                        <label for="txt_uf">UF</label>
                        <input type="text" style=" width:100px; background:#EAEAEA;" id="txt_uf" name="txt_uf" readonly="readonly" value="<?=$rowUF['fldSigla']?>" />
                    </li>
                    <li>
                    	<input type="submit" style="margin:0;margin-top:14px" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="salvar" title="Salvar" />
                    </li>
                </ul>
               
                
            </form>
            
            <h3>Logo da empresa</h3>
            
          	<div id="upload">
                <img src="image/layout/logo_empresa.jpg" align="logo" />
                
                <form class="frm_detalhe" id="upload" name="upload" action="" enctype="multipart/form-data" method="post">
                	<small>A imagem ser&aacute; redimensionada automaticamente para 200x120px.</small>
                    <input type="file" id="file" name="file" value="Procurar">
                    <input style="margin-top: 0; float:right" type="submit" class="btn_enviar" name="btn_upload" value="Gravar" >
                </form>
            </div>
        
		</div>
      
</div>