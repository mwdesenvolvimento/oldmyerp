	<script type="text/javascript">
		window.print();
	</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Imprimir Compra</title>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Robots" content="none" />
    
        <link rel="stylesheet" type="text/css" media="all" href="style/impressao/style_pedido_imprimir_A4.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print_A4.css" />
        
	</head>
	<body>

<?
		
	if (!isset($_SESSION['logado'])){
		session_start();
	}

	ob_start();
	session_start();
	
	//conectar ao db
	require_once("inc/con_db.php");
	require_once("inc/fnc_general.php");
	require_once("inc/fnc_ibge.php");

	$compra_id 	= $_GET['id'];
	$data 		= date("Y-m-d");
	
	$rsCompra 	= mysql_query("SELECT tblfornecedor.*, tblcompra.* 
							  FROM tblcompra INNER JOIN tblfornecedor ON tblcompra.fldFornecedor_Id = tblfornecedor.fldId
							  WHERE tblcompra.fldId = $compra_id");
	$rowCompra 	= mysql_fetch_array($rsCompra);
	
	$rsUsuario = mysql_query("select * from tblusuario where fldId = ".$_SESSION['usuario_id']);
	$rowUsuario = mysql_fetch_array($rsUsuario);
	echo mysql_error();
	
	$municipioFornecedor = fnc_ibge_municipio($rowCompra['fldMunicipio_Codigo']);
	$siglaUFFornecedor	 = fnc_ibge_uf_sigla($rowCompra['fldMunicipio_Codigo']);
	$FornecedorCPF_CNPJ  = formatCPFCNPJTipo_out($rowCompra['fldCPF_CNPJ'], $rowCompra['fldTipo']);	
	/*----------------------------------------------------------------------------------------------*/
	$rsEmpresa 			= mysql_query("SELECT * FROM tblempresa_info");
	$rowEmpresa 		= mysql_fetch_array($rsEmpresa);
	$CPF_CNPJ 			= formatCPFCNPJTipo_out($rowEmpresa['fldCPF_CNPJ'], $rowEmpresa['fldTipo']);	
	$municipioEmpresa 	= fnc_ibge_municipio($rowCompra['fldMunicipio_Codigo']);
	$siglaUFEmpresa	 	= fnc_ibge_uf_sigla($rowCompra['fldMunicipio_Codigo']);
	/*----------------------------------------------------------------------------------------------*/
	$endereco 	= $rowEmpresa['fldEndereco'];
	$numero 	= $rowEmpresa['fldNumero'];
	$bairro 	= $rowEmpresa['fldBairro'];
	$cidade 	= $rowMunicipio_Empresa['fldNome'];
	$uf 		= $rowUF_Empresa['fldSigla'];
	$site 		= $rowEmpresa['fldWebsite'];
	
	//VERIFICA STATUS -- LUCAS 20121023
	$status_n = $rowCompra['fldStatus'];
	$rsStatus = mysql_query("SELECT * FROM tblpedido_status WHERE fldId = $status_n");
	while($rowStatus = mysql_fetch_array($rsStatus))
	{
		
		$txtStatus = $rowStatus['fldStatus'];
		
	}
	//FIM VERIFICACAO STATUS
	
?>
    <div id="no-print">
        <div id="impressao_cabecalho">
            <ul id="bts">
                <li><a href="#" onclick ="window.print()"><span>Imprimir</span></a></li>
                <li><a href="index.php?p=pedido&amp;mensagem=ok"><span>Finalizar</span></a></li>
            </ul>
        </div>
	</div>
    
    <table id="pedido_imprimir">
		<tr>
            <td>
                <table class="cabecalho">
                    <tr style="width: 200px;height:120px; float:left;">
                        <td><img src="image/layout/logo_empresa.jpg" alt="logo" /></td>
                    </tr>
                    <tr>
                        <td><?=$rowEmpresa['fldRazao_Social']?></td>
                        <td>CNPJ <?=$CPF_CNPJ; ?></td>
                        <td><?=$endereco.", ".$numero." - ".$bairro." • ".$municipioEmpresa."/".$siglaUFEmpresa?></td>
                        <td>Fone: <?=$rowEmpresa['fldTelefone1']."  ".$rowEmpresa['fldTelefone2']?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td>
                <table class="pedido_imprimir_dados">
                	<tr>
                        <td>
                            <table class="pedido_dados">
                               
                                <tr class="dados">
                                    <td><strong>C&oacute;d. da Compra: </strong><?=$rowCompra['fldId']?></td>
                                    <td><strong>Data: 			</strong><?=format_date_out($data)?></td>
                                    <td><strong>Fornecedor: 	</strong><?=$rowCompra['fldRazaoSocial']?></td>
                                    <td><strong>Nome Fantasia: 	</strong><?=$rowCompra['fldNomeFantasia']?></td>
                                    <td><strong>CPF/CNPJ: 		</strong><?=$FornecedorCPF_CNPJ?></td>
                                    <td><strong>RG/IE: 			</strong><?=$rowCompra['fldIE']?></td>
                                    <td><strong>Telefone 1: 	</strong><?=$rowCompra['fldTelefone1']?></td>
                                    <td><strong>Telefone 2: 	</strong><?=$rowCompra['fldTelefone2']?></td>
                                    <td><strong>E-mail: 		</strong><?=$rowCompra['fldEmail']?></td>
                                    <td><strong>End.: 			</strong><?=$rowCompra['fldEndereco']?>, <?=$rowCompra['fldNumero']?></td>
                                    <td><strong>Bairro: 		</strong><?=$rowCompra['fldBairro']?></td>
                                    <td><strong>CEP: 			</strong><?=$rowCompra['fldCEP']?></td>
                                    <td><strong>Cidade: 		</strong><?=$municipioFornecedor?></td>
                                    <td><strong>UF: 			</strong><?=$siglaUFFornecedor?></td>
                                    <td><strong>Status: 		</strong><?=$txtStatus?></td> <!-- LUCAS 20121023 -->
								</tr>
        					</table> <? //cabecalho ?>
        				</td>
                    </tr>
                    <tr>
                    	<td>
                            <table class="pedido_descricao">
                                <tr>
                                    <td><h2>Produtos:</h2></td>
                                </tr>
                                <tr class="descricao">
                                    <td style="width:60px">Qtde</td>
                                    <td style="width:60px">C&oacute;d.</td>
<?									if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema_impressao_campo WHERE fldCampo = 'marca' AND fldImpressao = 'compra'"))){				                                        
?>                                     	<td style="width:260px">Descric&atilde;o</td>
										<td style="width:125px">Marca</td>
<?									}else{
?>										<td style="width:390px">Descric&atilde;o</td>
<?									}
?>                                  <td style="text-align:right">Valor Uni.</td>
                                    <td style="width:60px;text-align:right">Descto(%)</td>
                                    <td style="text-align:right">Valor Total</td>
                                </tr> <? // cabecalho descricao ?>
<?				
								$limite = 45; //45
								$n = 1;
								$rsItem = mysql_query("SELECT * FROM tblcompra_item WHERE fldCompra_Id = $compra_id");
								$rowsItem = mysql_num_rows($rsItem);
								while($rowItem = mysql_fetch_array($rsItem)){
									$x+= 1;
									
									$rsProduto = mysql_query("SELECT tblmarca.fldNome as fldMarca, tblproduto.*
															 FROM tblproduto LEFT JOIN tblmarca ON tblproduto.fldMarca_Id = tblmarca.fldId
															 WHERE tblproduto.fldId = ".$rowItem['fldProduto_Id']);
									$rowProduto = mysql_fetch_array($rsProduto);
								
									$valor 			= $rowItem['fldValor'];
									$qtde 			= $rowItem['fldQuantidade'];
									$desconto 		= $rowItem['fldDesconto'];
									$total 			= $valor * $qtde;
									$descontoItem 	= ($total * $desconto) / 100;
									$totalItem 		= $total - $descontoItem;
?>                          		
									<tr class="pedido_item">	
										<td style="width:60px"><?=$rowItem['fldQuantidade']?></td>
                                        <td style="width:60px"><?=$rowProduto['fldCodigo']?></td>
<?										if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema_impressao_campo WHERE fldCampo = 'marca' AND fldImpressao = 'compra'"))){				                                        
?>                                      	<td style="width:260px"><?=$rowItem['fldDescricao']?></td>
											<td style="width:125px">&nbsp;<?=substr($rowProduto['fldMarca'],0,20)?></td>
<?										}else{
?>											<td style="width:390px"><?=$rowItem['fldDescricao']?></td>
<?										}
?>                                      <td style="text-align:right"><?=format_number_out($rowItem['fldValor'])?></td>
                                        <td style="width:60px;text-align:right"><?=format_number_out($desconto)?></td>
                                        <td style="text-align:right"><?=format_number_out($totalItem)?></td>
                    				</tr>
<?										$total_item += $totalItem;
				
										if($n == $limite){ // se chegou no limite por pagina fecha as tables e pula pra prox. pagina
											$limite = 68; //70
											$n = 1;
?>														</table>  <? // fecha pedido_descricao?>
                                                    </td>
                                                </tr>
                                            <tr style="page-break-after: always"><td></td></tr>
                                            <tr>
                                
<?											if($x < $rowsItem){ // se na proxima pagina, o numero de registros ateh agora ($x) for menor que total de registros, abre as tables de novo e continua o loop
?>													<td>
                                                        <table class="pedido_descricao">
<?											}
										}elseif($n < $limite and $x == $rowsItem){
?>														</table> <? // fecha pedido_descricao?>
													</td>
												</tr>
<?                                         
											if($n > 50){ //no caso da ultima pagina, pode ter ateh 40 registros e exibir junto com o rodape de pagamento, caso contrario, jago pra próxima pagina
?>                                          	<tr style="page-break-after: always"><td></td></tr>
<?											}
?>											<tr>
												<td>
<?
										}else{
                     		 				 $n +=1;
                   						}
									}
						
									$total_pedido 			= $total_item;
									$desconto_pedido 		= $rowCompra['fldDesconto'];
									$desconto_reais_pedido 	= $rowCompra['fldDescontoReais'];
									$desconto 				= ($total_pedido * $desconto_pedido) / 100;
									$total_pedido_apagar 	= ($total_pedido - $desconto) - $desconto_reais_pedido;
									$total_descontos 		= $desconto + $desconto_reais_pedido;
?>					 			
                                <table id="pedido_observacao">
                                    <tr>
                                        <td>
                                            <h2>Observa&ccedil;&atilde;o</h2>
                                            <p><?=$rowCompra['fldObservacao']?></p>
                                        </td>
                                    </tr>                                
                                </table>
							</td>
						</tr>
                                
						<tr>
							<td>
                                <table id="pedido_pagamento">
                                	<tr>
                                    	<td>
                                   			<table class="parcelas">
                                                <tr>
                                                    <td><h2>Parcelas</h2></td>
                                                </tr>
                                                <tr>
                                                	<td>
                                        				<table class="parcela_desc">
<?					
															$sSQL = "SELECT * FROM tblcompra_parcela WHERE fldcompra_id =".$compra_id." ORDER BY fldParcela";
															$rsParcela 	= mysql_query($sSQL);
															$rows 		= mysql_num_rows($rsParcela);
															$limite 	= ceil($rows / 2);
															$x = 1;
															$rsParcela = mysql_query($sSQL);
															//VAI VERIFICAR SE X É IGUAL AO LIMITE, METADE DOS REGISTROS, E ENTÃO PULAR PARA A DIV DO LADO
															while($rowParcela = mysql_fetch_array($rsParcela)){
																$rsBaixa 		= mysql_query("SELECT sum(fldValor * (fldExcluido * -1 + 1)) as fldTotal FROM tblcompra_parcela_baixa WHERE fldParcela_Id =".$rowParcela['fldId']);
																$rowBaixa 		= mysql_fetch_array($rsBaixa);
																
																$devedor = $rowParcela['fldValor'] - $rowBaixa['fldTotal'];
																$totalBaixa += $rowBaixa['fldTotal'];
																
?>  	            											<tr>
                                                                    <td class="parcela"><?=$rowParcela['fldParcela']?></td>                
                                                                    <td><?=format_date_out($rowParcela['fldVencimento'])?></td>    
                                                                    <td style="text-align:right"><?=format_number_out($rowParcela['fldValor'])?></td>              
                                                                    <td style="text-align:right">PG: <?=format_number_out($rowBaixa['fldTotal'])?></td>              
                                                                </tr>
<?																if($x == $limite or $x == $rows){
?>																		</table>
<?																	if($x < $rows){
?>																		<table class="parcela_desc">
<?																	} 
																}$x++;
															}
															if(! mysql_num_rows($rsParcela)){
?>																</table>															
<?															}
                       										$total_pedido_apagar = $total_pedido_apagar - $totalBaixa;
?>          										</td>
												</tr>
											</table>
										</td>
									</tr>
                                    <tr class="pedido_total" style="float: right">
                                        <td><strong>Total: </strong><span>R$ <?=format_number_out($total_pedido)?></span></td>
                                        <td><strong>Desconto: </strong><span>R$ <?=format_number_out($total_descontos)?></span></td>
                                        <td><strong>Pago: </strong><span>R$ <?=format_number_out($totalBaixa)?></span></td>
                                        <td><strong>A pagar: </strong><span>R$ <?=format_number_out($total_pedido_apagar)?></span></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>