<?
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		
		$nfe_serie 				= $_POST['txt_nfe_serie'];
		$nfe_numero 			= $_POST['txt_nfe_proxima_nota'];
		
		//$icms_aliquota_cal_cred = $_POST['txt_nfe_icms_aliquota_credito'];
		$tipo_ambiente			= $_POST['sel_ambiente'];
		$natureza_operacao_id	= $_POST['sel_nfe_operacao_natureza'];
		$pagamento_id			= $_POST['sel_nfe_pagamento_forma'];
		$frete_id 				= $_POST['sel_nfe_transporte_modalidade'];
		$transportador_id	 	= $_POST['sel_nfe_transportador'];
		$info_fisco		 		= $_POST['txa_pedido_nfe_fisco'];
		$info_contribuinte 		= $_POST['txa_pedido_nfe_contribuite'];
		
		$sql 	= mysql_query("SELECT count(*) AS fldCount FROM tblnfe_perfil");
		$count 	= mysql_fetch_array($sql);
		
		if(!$count['fldCount']){
			$SQL = "INSERT INTO tblnfe_perfil (fldNatureza_Operacao_Id, fldPagamento_Id, fldFrete_Id, fldTransportador_Id, fldInfo_Fisco, fldInfo_Contribuinte) 
					VALUES('$natureza_operacao_id', '$pagamento_id', '$frete_id', '$transportador_id', '$info_fisco', '$info_contribuinte')";
		}else{
			$SQL = "UPDATE tblnfe_perfil SET fldNatureza_Operacao_Id = '$natureza_operacao_id', fldPagamento_Id = '$pagamento_id', fldFrete_Id = '$frete_id', fldTransportador_Id = '$transportador_id', fldInfo_Fisco = '$info_fisco', fldInfo_Contribuinte = '$info_contribuinte' ";
		}
		
		if(mysql_query($SQL)){
			
			fnc_sistema_update('nfe_numero_nota', $nfe_numero);
			fnc_sistema_update('nfe_serie', $nfe_serie);
			fnc_sistema_update('nfe_ambiente_tipo', $tipo_ambiente);
?>		
			<div class="alert" style="margin-top:6px">
                <p class="ok">Dados atualizados!<p>
            </div>	
<?		}
	}

	$rsPerfil 			= mysql_query("SELECT * FROM tblnfe_perfil");
	$rowPerfil 			= mysql_fetch_array($rsPerfil); 
	
	$natureza_operacao	= $rowPerfil['fldNatureza_Operacao_Id'];
	$pagamento_id		= $rowPerfil['fldPagamento_Id'];
	$frete_id 			= $rowPerfil['fldFrete_Id'];
	$transportador_id 	= $rowPerfil['fldTransportador_Id'];
	$info_fisco		 	= $rowPerfil['fldInfo_Fisco'];
	$info_contribuinte 	= $rowPerfil['fldInfo_Contribuinte'];
	
	$prox_numero_nota 	= fnc_sistema('nfe_numero_nota');
	$tipo_ambiente		= fnc_sistema('nfe_ambiente_tipo');
	
	//$icms_aliquota_cal_cred	= $rowPerfil['fldICMS_Aliquota_Calculo_Credito'];
?>	
	<ul class="header_bar" style="margin-top:5px">
		<li><a class="btn_nfe_inutilizada modal" href="nfe_numeros_excluidos" rel="800-335" title="visualizar n&uacute;meros inutilizados">N&uacute;meros inutilizados</a></li>
	</ul>            
    
	<div class="form">
		<form class="frm_detalhe" style="width:890px;text-align:right" id="frm_pedido_perfil_fiscal" action="?p=pedido_nfe&modo=perfil" method="post">
			<fieldset style="margin: 10px">
            	<legend>Perfil NFe</legend>
				<ul style="width:940px">
                	<li>
                    	<label for="sel_ambiente">Tipo de ambiente</label>
                        <select id="sel_ambiente" name="sel_ambiente" style="width:150px; margin-right:750px">
                        	<option value="1" <?= ($tipo_ambiente == '1') ? 'selected="selected"': '' ?>>produ&ccedil;&atilde;o</option>
                        	<option value="2" <?= ($tipo_ambiente == '2') ? 'selected="selected"': '' ?>>homologa&ccedil;&atilde;o</option>
                        </select>
                    </li>
               		<li>
                        <label for="txt_nfe_serie">S&eacute;rie</label>
                        <input type="text" style="width:60px" id="txt_nfe_serie" name="txt_nfe_serie" value="<?=fnc_sistema('nfe_serie')?>"/>
                    </li>
               		<li>
                        <label for="txt_nfe_proxima_nota">Pr&oacute;x. Nota</label>
                        <input type="text" style="width:60px" id="txt_nfe_proxima_nota" name="txt_nfe_proxima_nota" value="<?=$prox_numero_nota?>"/>
                	</li>
                    <li>
                        <label for="sel_nfe_operacao_natureza">Natureza da Opera&ccedil;&atilde;o</label>
                        <select class="sel_nfe_operacao_natureza" style="width:200px" id="sel_nfe_operacao_natureza" name="sel_nfe_operacao_natureza" >
<?								$rsNfeOperacao = mysql_query("SELECT * FROM tblnfe_natureza_operacao");
                            	while($rowNfeOperacao = mysql_fetch_array($rsNfeOperacao)){                            
?>                   	       		<option value="<?=$rowNfeOperacao['fldId']?>"  <?= ($rowNfeOperacao['fldId'] == $natureza_operacao) ? 'selected="selected"' : '' ?> ><?=$rowNfeOperacao['fldNatureza_Operacao']?></option>
<?								}
?>                 			</select>
                    </li>
					<li>
                        <label for="sel_nfe_pagamento_forma">Forma Pagamento</label>
                        <select style="width:205px" id="sel_nfe_pagamento_forma" name="sel_nfe_pagamento_forma" class="sel_nfe_pagamento_forma" title="Forma Pagamento">
	                        <option value=""></option>	
                            <option <?=($pagamento_id == '0') ? 'selected="selected"' : ''?> value="0">Pagamento &agrave; vista</option>
                            <option <?=($pagamento_id == '1') ? 'selected="selected"' : ''?> value="1">Pagamento &agrave; prazo</option>
                            <option <?=($pagamento_id == '2') ? 'selected="selected"' : ''?> value="2">Outros</option>
                        </select> 
                    </li>
				</ul>
                <fieldset style="margin: 10px">
                    <legend>Transporte</legend>
					<ul style="width:100%;">
						<li>
                            <label for="sel_nfe_transporte_modalidade">Modalidade do frete</label>
                            <select style="width: 295px" id="sel_nfe_transporte_modalidade" name="sel_nfe_transporte_modalidade" class="sel_nfe_transporte_modalidade" title="Modalidade do frete">
	                            <option value="0"></option>
<?								$rsTransporte = mysql_query("SELECT * FROM tblnfe_pedido_transporte");
                                while($rowTransporte = mysql_fetch_array($rsTransporte)){
?>									<option <?=($frete_id == $rowTransporte['fldId']) ? 'selected="selected"' : ''?> value="<?= $rowTransporte['fldId'] ?>"><?= $rowTransporte['fldCodigo'] ?> - <?= $rowTransporte['fldDescricao'] ?></option>
<?								}
?>                    		</select> 
                        </li>
                        <li>
                            <label for="sel_nfe_transportador">Transportador</label>
                            <select style="width: 350px" id="sel_nfe_transportador" name="sel_nfe_transportador" class="sel_nfe_transportador" title="Transportador">
                                <option value="0"></option>
<?								$rsTransportador 		= mysql_query("SELECT * FROM tbltransportador ORDER BY fldNome ASC");
                                while($rowTransportador = mysql_fetch_array($rsTransportador)){
?>									<option <?=($transportador_id == $rowTransportador['fldId']) ? 'selected="selected"' : ''?> value="<?= $rowTransportador['fldId'] ?>"><?= $rowTransportador['fldNome'] ?></option>
<?								}
?>                    		</select> 
                       </li> 
					</ul>
				</fieldset>
                
                <fieldset style="margin: 10px">
                    <legend>Info. Adicionais</legend>
					<ul style="width:100%;">
                        <li style="width: 450px">
                            <label for="txa_pedido_nfe_fisco">Fisco</label>
                            <textarea style=" width:440px; height:100px" id="txa_pedido_nfe_fisco" name="txa_pedido_nfe_fisco"><?=$info_fisco?></textarea>
                        </li>
                        <li style="width: 450px">
                            <label for="txa_pedido_nfe_contribuite">Contribuinte</label>
                            <textarea style=" width:440px; height:100px" id="txa_pedido_nfe_contribuite" name="txa_pedido_nfe_contribuite"><?=$info_contribuinte?></textarea>
                        </li>
					</ul>
				</fieldset>
				
                <input type="submit" style="margin:10px; float:right" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
			</fieldset>
        </form>
    </div>
	
    <a id="inutilizar_numero" rel="600-350"></a>
    
	<script type="text/javascript">
		$('#btn_gravar').click(function(event){
			event.preventDefault();
			var txt_numero	= $("#txt_nfe_proxima_nota").val();
			var bd_numero	= <?= $prox_numero_nota ?>;
			
			anchor = $('#inutilizar_numero');
			if(txt_numero > bd_numero){
				$("a#inutilizar_numero").attr({"href": 'nfe_perfil_inutilizar_numero,'+txt_numero});
				winModal(anchor);
				return false;
			}else if(bd_numero > txt_numero){
				$.post('pedido_nfe_perfil_retorno.php',{bd_numero:bd_numero, txt_numero:txt_numero},function(retorno){
					if(retorno > 0){
						alert('Número de NFe já inutilizado!');
						return false;
					}
					else{
						alert('Por segurança você não pode alterar para um número menor que o atual!');
						return false;
					}
				});	
			}else{
				$("#frm_pedido_perfil_fiscal").submit();
			}
		});
	</script>