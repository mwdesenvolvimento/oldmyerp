<script type="text/javascript">
	window.print();
</script>

    <title>Impress&atilde;o de recibo</title>
    <link rel="stylesheet" type="text/css" href="style/impressao/style_pedido_imprimir_recibo_recibo1.css" />
    <link rel="stylesheet" type="text/css" media="all" href="style/impressao/style_pedido_imprimir_recibo1.css" />
    <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
	
	<div id="no-print">
    <div id="impressao_cabecalho">
        <ul id="bts">
            <li><a href="#" onClick="window.print()"><span>Imprimir</span></a></li>
        </ul>
    </div>
</div>

<?	ob_start();
	session_start();
	
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	
	$filtro = $_GET['filtro'];
	$id_parcela =  $_GET['id'];
	
	if(isset($filtro)){
		$sql = "SELECT 	tblpedido_parcela_baixa.*, tblpedido_parcela_baixa.fldValor as fldValorBaixa, 
					tblpedido_parcela.fldValor as fldValorParcela, 
					tblpedido_parcela.* from tblpedido_parcela INNER JOIN tblpedido_parcela_baixa
					ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
				   	WHERE tblpedido_parcela_baixa.fldId in (".$filtro.")";
	}elseif(isset($id_parcela)){
		
		$sql = "SELECT tblpedido.*, tblpedido_parcela.fldValor as fldValorParcela,
									   tblpedido_parcela.fldId as fldParcela_Id,
									   MAX(tblpedido_parcela_baixa.fldDataRecebido) as fldDataRecebido,
									   tblpedido_parcela.* FROM tblpedido_parcela INNER JOIN tblpedido
									   ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId
									   INNER JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
									   WHERE tblpedido_parcela.fldId IN (".$id_parcela.") GROUP BY tblpedido_parcela.fldId";
			
	}
	
	$rsBaixa = mysql_query($sql);
	$rowBaixa = mysql_fetch_array($rsBaixa);
	echo mysql_error();
	
	$rows = mysql_num_rows(mysql_query("select * from tblpedido_parcela where fldPedido_Id=".$rowBaixa['fldPedido_Id']));
	$rowCliente = mysql_fetch_array(mysql_query("select tblcliente.*, tblpedido.fldId from tblcliente inner join tblpedido
													on tblcliente.fldId = tblpedido.fldCliente_Id where tblpedido.fldId=".$rowBaixa['fldPedido_Id']));
	
	echo mysql_error();
	if(isset($filtro)){
		$sumSql = mysql_query("SELECT SUM(fldValor) as fldTotal FROM tblpedido_parcela_baixa WHERE fldId in(".$filtro.")");
	}elseif(isset($id_parcela)){
		$sumSql = mysql_query("SELECT SUM(fldValor) as fldTotal FROM tblpedido_parcela_baixa WHERE fldParcela_Id in(".$id_parcela.")");
	}
	$sumSql = mysql_fetch_array($sumSql);
	
	$rsBaixa = mysql_query($sql);
	
?>

<div class="cliente_recibo_001">
 
    <ul class="dados_recibo_parcela">
    	<li style="width: 4cm;">&nbsp;</li>
        <li><?=format_number_out($sumSql['fldTotal'])?></li>
        <li><?=format_date_out($rowBaixa['fldDataRecebido'])?></li>
    </ul>
    
    <ul class="dados_recibo_parcela_cliente">
        <li><?=acentoRemover($rowCliente['fldNome'])?></li>
    </ul>
    
   	<ul class="dados_recibo_parcela_valor">
        <li><?= utf8_encode(valorExtenso($sumSql['fldTotal'],1,"baixa"))?></li>
    </ul>
     
   	<div id="dados_recibo_parcela_descricao">
    	<ul class="dados_descricao_parcela_cabecalho">
        	<li class="dados_cabecalho">pedido</li>
        	<li class="dados_cabecalho">parcela</li>
        	<li class="dados_cabecalho">valor</li>
        </ul>
<?		while($rowBaixa = mysql_fetch_array($rsBaixa)){
	echo mysql_error();
		//se for por selecao de parcela manualmente,  busca o valor das baixa em outra consulta
			if(isset($id_parcela)){
				$rsBaixaValor = mysql_query("SELECT SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa, 
											   tblpedido_parcela_baixa.*,
											   tblpedido_parcela.fldValor as fldValorParcela,
											   tblpedido_parcela.* FROM tblpedido_parcela INNER JOIN tblpedido_parcela_baixa
											   ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
											   WHERE tblpedido_parcela.fldId =".$rowBaixa['fldParcela_Id']." GROUP BY tblpedido_parcela_baixa.fldParcela_Id");
			
				$rowBaixaValor = mysql_fetch_array($rsBaixaValor);
				echo mysql_error();
				
				$baixaValor = $rowBaixaValor['fldValorBaixa'];
			}else{
				$baixaValor =  $rowBaixa['fldValorBaixa'];
			}
				 
				 
?>	    	<ul class="dados_recibo_parcela_descricao">
		   		<li><?=$rowBaixa['fldPedido_Id']?></li>
				<li><?=$rowBaixa['fldParcela']."/".$rows?></li>
				<li><?=format_number_out($baixaValor)?></li>
			</ul>
<?		}
?>  	            
   </div>
</div>

</body>
</html>
