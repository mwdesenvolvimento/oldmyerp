<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
      	<title>myERP - Imprimir venda</title>
        
	</head>
	<body>
<?php
		if (!isset($_SESSION['logado'])){
			session_start();
		}
	
		ob_start();
		session_start();
		
		require_once("inc/con_db.php");
		require_once("inc/fnc_general.php");
		require_once("inc/fnc_imprimir.php");
		require_once("inc/fnc_identificacao.php");
		require_once("inc/fnc_ibge.php");
	
		$impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
		if(!$impressao_local){
			$impressao_local = fnc_estacao_impressora('todos');
		}
		$texto 				= $impressao_local."\r\n";
		
		$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
		$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
		$usuario_sessao 	= $_SESSION['usuario_id'];
		$pedido_id  		= $_GET['id'];
		$raiz 				= $_GET['raiz'];
		$data 				= date("Y-m-d");
		
		############################################################################################################################################################
		$rsPedido  	= mysql_query("SELECT  SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) as fldPedidoValor,
								 tblpedido.*, tblpedido.fldObservacao AS fldOBS, tblpedido.fldId as fldPedidoId, tblcliente.*
								 FROM tblpedido 
								 LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
								 INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								 WHERE tblpedido.fldId = $pedido_id GROUP BY tblpedido_item.fldPedido_Id");
		$rowPedido 	= mysql_fetch_array($rsPedido);
		
		$rsEmpresa  = mysql_query("SELECT * FROM tblempresa_info");
		$rowEmpresa = mysql_fetch_array($rsEmpresa);
		
		#CRIANDO RODAPE  !!!########################################################################################################################################
		#PARCELAS
		
		$sSQL = "SELECT SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor
				FROM tblpedido_parcela 
				LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
				WHERE tblpedido_parcela.fldPedido_Id = $pedido_id GROUP BY tblpedido_parcela.fldPedido_Id";
		$rsPedidoBaixa 	= mysql_query($sSQL);
		$rowPedidoBaixa	= mysql_fetch_array($rsPedidoBaixa);
		
		$total_pedido 			= $rowPedido['fldPedidoValor'];
		$desconto_pedido 		= $rowPedido['fldDesconto'];
		$desconto_reais_pedido 	= $rowPedido['fldDescontoReais'];
		$desconto 				= ($total_pedido * $desconto_pedido) / 100;
		$total_descontos 		= $desconto + $desconto_reais_pedido;
		$total_pedido_apagar 	= ($total_pedido - $total_descontos);
		
		$valorBaixa 			= $rowPedidoBaixa['fldBaixaValor'];
		$valorDevedor 			= $total_pedido_apagar - $rowPedidoBaixa['fldBaixaValor'];
		$total_pedido 			= format_number_out($total_pedido);
		$desconto_pedido 		= format_number_out($desconto_pedido);
		$desconto_reais_pedido	= format_number_out($desconto_reais_pedido);
		$valorBaixa 			= format_number_out($valorBaixa);
		$valorDevedor 			= format_number_out($valorDevedor);
		$total_pedido_apagar 	= Format_number_out($total_pedido_apagar);
		
		#############################################################################################################################################################
		$rodape 	.= "--------------------------------------------------------------------------------\r\n";
		$rodapeLinha +=1;
		
		$rodape 	.= format_margem_print(("SUBTOTAL ".$total_pedido), 26, 'direita');
		$rodape 	.= format_margem_print(("DESC % ".$desconto_pedido), 26, 'direita');
		$rodape 	.= format_margem_print("DESC R$ ".$desconto_reais_pedido, 28, 'direita')."\r\n";
		$rodapeLinha +=1;
		
		$rodape 	.= format_margem_print("<b>TOTAL ".$total_pedido_apagar."</b>", 26, 'direita');
		$rodape 	.= format_margem_print(("PAGO   ".$valorBaixa), 26, 'direita');
		$rodape 	.= format_margem_print("DEVEDOR R$ ".$valorDevedor, 28, 'direita')."\r\n";
		$rodapeLinha +=1;
		
		$rodape 	.= "--------------------------------------------------------------------------------\r\n";
		$rodapeLinha +=1;
		
		$rodape .= "<b>";
		$rodape .= format_margem_print('PARC',4,'esquerda').'|';
		$rodape .= format_margem_print('VENC',5,'esquerda').'|';
		$rodape .= format_margem_print('VALOR',7,'direita').'|';
		$rodape .= format_margem_print('PAGO',7,'direita').'|';
		$rodape .= format_margem_print('FORM',4,'direita').'|';
		$rodape .= format_margem_print('DEVEDOR',7,'direita')."</b>\r\n";
		$rodapeLinha +=1;
		
		$rodape .="--------------------------------------------------------------------------------\r\n";
		$rodapeLinha +=1;
		
		# PARCELAS ###############################################################################################################################################
		$sSQL = "SELECT tblpedido_parcela.*, tblpagamento_tipo.fldSigla as fldForma_Pagamento, SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldTotalBaixa
						FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
						LEFT JOIN tblpagamento_tipo ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
						WHERE tblpedido_parcela.fldPedido_Id = $pedido_id GROUP BY tblpedido_parcela.fldId ORDER BY tblpedido_parcela.fldParcela";
		$rsParcela = mysql_query($sSQL);
		
		$n = 1;
		$x = 1;
		$total = mysql_num_rows($rsParcela);
		$limite = ceil($total / 2);
		while($rowParcela = mysql_fetch_array($rsParcela)){
			
			$pago = $rowParcela['fldTotalBaixa'];
			if($rowParcela['fldTotalBaixa'] == 0 or $rowParcela['fldTotalBaixa'] == NULL ){
				$pago = "0.00";
			}
			$devedor 	= $rowParcela['fldValor'] - $pago;
			$parcela 	= str_pad($rowParcela['fldParcela'], 2, "0", STR_PAD_LEFT);
			$venc	 	= format_date_out4($rowParcela['fldVencimento']);
			$valor 	 	= format_number_out($rowParcela['fldValor']);
			$pago 	 	= format_number_out($pago);
			$forma_pg 	= $rowParcela['fldForma_Pagamento'];
			$devedor 	= format_number_out($devedor);
			
			$Col[$x][$n] = array(
				'parcela' 	=> $parcela,
				'venc' 		=> $venc,
				'valor' 	=> $valor,
				'pargo' 	=> $pago,
				'forma_pg' 	=> $forma_pg,
				'devedor' 	=> $devedor
			);
			
			if($n == $limite){
				$x = 2;
				$n = 1;
			}else{
				$n ++;
			}
		}
		$n = 1;
		$x = 1;
		$col = count($Col[$x]);
		while($col >= $n){
			$y += 38;
			$rodape 	.= format_margem_print($Col[$x][$n]['parcela'],3,'direita').' ';
			$rodape 	.= format_margem_print($Col[$x][$n]['venc'],5,'direita').' ';
			$rodape 	.= format_margem_print($Col[$x][$n]['valor'],7,'direita').' ';
			$rodape 	.= format_margem_print($Col[$x][$n]['pargo'],7,'direita').' ';
			$rodape 	.= format_margem_print($Col[$x][$n]['forma_pg'],3,'direita').' ';
			$rodape 	.= format_margem_print($Col[$x][$n]['devedor'],7,'direita').' | ';
			if($y == 76){
				$rodape 	.="\r\n";
				$rodapeLinha +=1;
				$y = 0; #zera caracater
			}
			($x == 2) ? $n++ : '';
			$x = ($x == 1) ? 2 : 1;	
		}
		
		#se forem paercelas numero impar, vai faltar caracter no total da linha pra pular a linha, se nao vai zerar 
		if($y == 0){
			$rodape 	.= "\r\n";
			$rodapeLinha +=1;
		}else{
			$rodape 	.= "\r\n\r\n";
			$rodapeLinha +=2;
		}
		
		# ASS CLIENTE E OBSERVACAO ##############################################################################################################################					
		$fldOBS = acentoRemover($rowPedido['fldOBS']);
		$limiteObs = 48;
		$xObs = 0;
		$rodape .="--------------------------------------------------------------------------------\r\n"; //aa
		$rodape .= format_margem_print("OBS: ".substr($fldOBS, 0, 43),49,'esquerda')."\r\n";
		
		if(strlen($fldOBS) > $limiteObs){
			$xObs = 1;
			while($xObs <= 3)
			{
				if($xObs == 1){$rodape .= format_margem_print(substr($fldOBS, $xObs * $limiteObs, $limiteObs),49,'esquerda')."\r\n";}
				else {$rodape .= format_margem_print(substr($fldOBS, $xObs * $limiteObs, $limiteObs),49,'esquerda');}
				if($xObs == 2){$rodape .= format_margem_print('------------------------------',0, 'direita')."\r\n";}
				if($xObs == 3){$rodape .= format_margem_print('CIENTE',32, 'centro')."\r\n";}
				$xObs+=1;
			}
		}
		else
		{
			$rodape .= "\r\n";
			$rodape .= format_margem_print('------------------------------',80, 'direita')."\r\n";
			$rodape .= format_margem_print('',40, 'direita');
			$rodape .= format_margem_print('CIENTE',50, 'centro')."\r\n";
		}
		$rodapeLinha +=6;
		# MENSAGENS #############################################################################################################################################
		
		$rsMensagem = mysql_query("SELECT * FROM tblsistema_impressao_mensagem WHERE fldImpressao = 'venda' ORDER BY fldOrdem");
		while($rowMensagem = mysql_fetch_array($rsMensagem)){
			$mensagem = strtoupper(acentoRemover($rowMensagem['fldMensagem']));
			$x = 0; //quebrando linha a cada 76 caracteres
			while(strlen(substr($mensagem,$x * 76, 76)) > 0){
				$rodape .= "<b>".format_margem_print(substr($mensagem,$x * 76, 76)."</b>",80, 'centro')."\r\n";
				$rodapeLinha +=1;
				$x +=1;
			}
		}
		$rodapeLinha +=1;
	
		# CABECALHO TXT ###############################################################################################################################################
		$cabecalho 	.= "<b>";
		$nomeEmpresa = acentoRemover(strtoupper($rowEmpresa['fldNome_Fantasia']));
		$cabecalho 	.= format_margem_print('---- ****  '.$nomeEmpresa.'  **** ----',80,'centro')."\r\n";
		$cabecalho 	.= "</b>";
		$cabecalhoLinha += 1;
		
		$enderecoEmpresa = substr($rowEmpresa['fldEndereco'],0,41);
		$cabecalho 	.= format_margem_print(strtoupper($enderecoEmpresa).' '.$rowEmpresa['fldNumero'],46,'esquerda');
		$cabecalho 	.= format_margem_print($rowEmpresa['fldTelefone1'],17,'esquerda');
		$cabecalho 	.= format_margem_print($telefone2,17,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		$cabecalho .= format_margem_print("VENDA:    <b>".str_pad($rowPedido['fldPedidoId'], 5, "0", STR_PAD_LEFT)."</b>",30,'esquerda');
		$cabecalho .= format_margem_print("DATA: ".format_date_out(date("Y-m-d")),25,'esquerda');
		$cabecalho .= format_margem_print("<b>DATA DA VENDA: ".format_date_out($rowPedido['fldPedidoData'])."</b>",25,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		$nome = substr($rowPedido['fldNome'],0,63);
		$cabecalho .= format_margem_print("CLIENTE:  <b>".str_pad($rowPedido['fldCodigo'], 6, "0", STR_PAD_LEFT).' '.acentoRemover(strtoupper($nome))."</b>",80,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		
		if(fnc_sistema('pedido_exibir_endereco') > 0){

			$endereco_pedido = explode(', ', $rowPedido['fldEnderecoPedido']);

			if($endereco_pedido == $rowPedido['fldEndereco'] || $rowPedido['fldEnderecoPedido'] == ''){
				//$rowEndereco = mysql_fetch_array(mysql_query("SELECT fldEndereco, fldNumero, fldBairro FROM tblcliente WHERE fldId = $cliente_id"));
				$endereco 	 = format_margem_print($rowPedido['fldEndereco'].', '.$rowPedido['fldNumero'],50,'esquerda');
				if(fnc_sistema('pedido_exibir_endereco') != '2'){
					$endereco 	 .= format_margem_print($rowPedido['fldBairro'],20,'esquerda');
					
				}

				$cabecalho .= "END: ".strtoupper(acentoRemover($endereco))."\r\n";
				$cabecalhoLinha += 1;
			}else{
				//$rowEndereco = mysql_fetch_array(mysql_query("SELECT fldEndereco, fldNumero, fldBairro FROM tblcliente WHERE fldId = $cliente_id"));
				$endereco 	 = format_margem_print($rowPedido['fldEndereco'].', '.$rowPedido['fldNumero'],50,'esquerda');
				if(fnc_sistema('pedido_exibir_endereco') != '2'){
					$endereco 	 .= format_margem_print($rowPedido['fldBairro'],20,'esquerda');
				}
				$cabecalho .= "\r\nEND: ".strtoupper(acentoRemover($endereco))."\r\n";
				$cabecalhoLinha += 1;
			}
		}
		
		
		
		$CPFCNPJ 	= formatCPFCNPJTipo_out($rowPedido['fldCPF_CNPJ'], $rowPedido['fldTipo']);
		$cabecalho .= format_margem_print("CPF/CNPJ: ".$CPFCNPJ,30,'esquerda');
		$cabecalho .= format_margem_print("RG/IE: ".$rowPedido['fldRG_IE'],50,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
			
		$cabecalho .= format_margem_print("TELEFONE: ".$rowPedido['fldTelefone1'],30,'esquerda');
		$cabecalho .= format_margem_print($rowPedido['fldTelefone1'],25,'esquerda')."\r\n";
		$cabecalhoLinha += 1;
		
		if($_SESSION['sistema_tipo'] == 'automotivo'){
			if(isset($rowPedido['fldVeiculo_Id'])){
				$rsVeiculo = mysql_query("SELECT fldVeiculo, fldPlaca FROM tblcliente_veiculo WHERE fldId = ".$rowPedido['fldVeiculo_Id']);
				$rowVeiculo = mysql_fetch_array($rsVeiculo);
				
				$cabecalho 		.= format_margem_print("PLACA: ".$rowVeiculo['fldPlaca']." ",30,'esquerda');
				$cabecalho 		.= format_margem_print("VEICULO: ".$rowVeiculo['fldVeiculo'],50,'esquerda')."\r\n";
				$cabecalhoLinha += 1;
			}
		}
		
		$cabecalho 	.="--------------------------------------------------------------------------------\r\n";
		$cabecalhoLinha += 1;
		
		$cabecalho 	.= "<b>";
		$cabecalho 	.= format_margem_print(' COD',12,'centro').'|';
		$cabecalho 	.= format_margem_print('PRODUTO',32,'esquerda').'|';
		$cabecalho 	.= format_margem_print('QTDE',8,'direita').'|';
		$cabecalho 	.= format_margem_print('VALOR',8,'direita').'|';
		$cabecalho 	.= format_margem_print('DESC',7,'direita').'|';
		$cabecalho 	.= format_margem_print('TOTAL',8,'direita')."\r\n";
		$cabecalho 	.= "</b>";
		$cabecalhoLinha += 1;
		
		$cabecalho 	.="--------------------------------------------------------------------------------\r\n";
		$cabecalhoLinha += 1;
		
		############################################################################################################################################################
		$limite = fnc_sistema('matricial_limite_linhas');
		$linhasRestantes = $limite - ($cabecalhoLinha + $rodapeLinha);
		$texto .= $cabecalho;
		############################################################################################################################################################
		#ITEM
			$rsItem = mysql_query("SELECT tblpedido_item.*, tblproduto.fldCodigo 
								  FROM tblpedido_item INNER JOIN tblproduto ON tblpedido_item.fldProduto_Id = tblproduto.fldId
								  WHERE tblpedido_item.fldPedido_Id = ".$rowPedido['fldPedidoId']);
			$totalItens = mysql_num_rows($rsItem);
			while($rowItem = mysql_fetch_array($rsItem)){
				$quantidade 	= $rowItem['fldQuantidade'];
				$valor 			= $rowItem['fldValor'];
				$total 			= $valor * $quantidade;
				$desconto 		= $rowItem['fldDesconto'];
				$descontoItem 	= ($total * $desconto) / 100;
				$totalItem 		= $total - $descontoItem;
		
				# GRAVA TXT ########################################################################################################################################
				//AQUI BUSCAR DADOS DA NFE
				$rowItemNFe  = mysql_fetch_array(mysql_query("SELECT * FROM tblpedido_item_fiscal WHERE fldItem_Id= " .$rowItem['fldId']));
				$rowNCM  	 = mysql_fetch_array(mysql_query("SELECT fldncm FROM tblproduto_fiscal WHERE fldProduto_Id= " .$rowItem['fldProduto_Id']));
				$complemento = (isset($rowItemNFe['fldinformacoes_adicionais'])) ? ' - '.$rowItemNFe['fldinformacoes_adicionais']: '' ;
				$descricao	 = substr($rowItem['fldDescricao'].$complemento, 0, 30);
				//($rowNCM['fldncm'] == '') ? '' : $descricao  .= '['.$rowNCM['fldncm'].']';

				$limiteDescricao = 32;
				$x = 0;
				$texto	.= format_margem_print($rowItem['fldCodigo'],12)."|";
				$texto 	.= format_margem_print(substr(acentoRemover($descricao),$x * $limiteDescricao, $limiteDescricao),$limiteDescricao).'|';
				$texto	.= format_margem_print(format_number_out($quantidade,$quantidadeDecimal),8, 'direita')."|";
				$texto	.= format_margem_print(format_number_out($valor,$vendaDecimal),8, 'direita')."|";
				$texto	.= format_margem_print(format_number_out($desconto),7, 'direita')."|";
				$texto	.= format_margem_print(format_number_out($totalItem),8, 'direita') . "\r\n";
				$linha 	+= 1;
				$item 	+= 1;
				
				if($item == $totalItens && strlen($descricao) <= $limiteDescricao){
					while($linha <= $linhasRestantes){
						$texto .= "            |                                |        |        |       |\r\n";
						$linha  += 1;
					}
					$texto .= $rodape;
					
				}elseif(strlen(substr($descricao,$x * $limiteDescricao, $limiteDescricao)) > 0 && $linhasRestantes == $linha ||  $linhasRestantes == $linha){
					$texto .= $rodape;	
					$texto .= "\r\n\r\n\r\n";	
					$texto .= $cabecalho;
					$linha  = 0;
				}
				
				# AQUI EM BAIXO, SE DESCRICAO DO PRODUTO FOR MT GRANDE, TERMINA DE ESCREVER DEPOIS DOS VALORE, ASSIM CAI ALINHADO ABAIXO
				if(strlen($descricao) > $limiteDescricao){
					$x = 1;
					while(strlen(substr($descricao,$x * $limiteDescricao, $limiteDescricao)) > 0){
						$texto 		.= "             ".format_margem_print(substr(acentoRemover($descricao),$x * $limiteDescricao, $limiteDescricao),$limiteDescricao)."\r\n";
						$linha +=1;
						$x +=1;
						
						if($item == $totalItens && strlen(substr($descricao,$x * $limiteDescricao, $limiteDescricao)) == 0){
							while($linha < $linhasRestantes){
								$texto .= "            |                                |        |        |       |\r\n";
								$linha  += 1;
							}
							$texto .= $rodape;
						
						}elseif(strlen(substr($descricao,$x * $limiteDescricao, $limiteDescricao)) > 0 && $linhasRestantes <= $linha ||  $linhasRestantes <= $linha){
							$texto .= $rodape;
							$texto .= "\r\n\r\n\r\n";	
							$texto .= $cabecalho;
							$linha = 0;
						}
					}
				}				
			}
	 	############################################################################################################################################################
		$timestamp  = date("Ymd_His");
		$local_file = "impressao///inbox///imprimir_pedido_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		//$local_file = "impressao\inbox\imprimir_pedido_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		$fp 		= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
		$salva		= fwrite($fp, $texto);
		$texto 		= fread($fp, filesize($local_file));
		
		//transformamos as quebras de linha em etiquetas <br>
		$texto = nl2br($texto);
		print $texto;

		unset($total_item);
		if($_GET['np'] > 0){
			require('pedido_imprimir_np_nfiscal.php');
		}
		fclose($fp);
?> 
    <script type="text/javascript">
   		window.close();
	</script>
		
	</body>
</html>        