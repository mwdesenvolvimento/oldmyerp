<?php

    ob_start();
    session_start();

    require("inc/con_db.php");
    require("inc/fnc_general.php");
    require("inc/fnc_ibge.php");

	(isset($_GET['d1'])) 		? $dataInicio 		= format_date_in($_GET['d1']) : '';
	(isset($_GET['d2'])) 		? $dataFinal 		= format_date_in($_GET['d2']) : '';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Produtos</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css"></link>
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_calendario.css"></link>
        
        <script type="text/javascript" src="js/general.js"></script>
        <script type="text/javascript" src="js/jquery-1.4.4.js"></script>
        <script type="text/javascript" src="js/jquery.click-calendario-min.js"></script>
        <script type="text/javascript" src="js/init-click-calendario.js"></script>
		<script type="text/javascript" src="js/produto.js"></script>
        <script type="text/javascript" src="js/jquery.maskedinput-1.2.2.js"></script>
        <script type="text/javascript" src="js/init-validate.js"></script>
        
	</head>
	<body>
    
        <div id="no-print">
            <a class="print" href="#" onClick="window.print()">imprimir</a>
        </div>
		
        <? 
        
        $rsDados = mysql_query("select * from tblempresa_info");
        $rowDados = mysql_fetch_array($rsDados);
        
        $CPF_CNPJDados = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
        $rsUsuario = mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']);
        $rowUsuario = mysql_fetch_array($rsUsuario);
            
        $periodo = "<td style='width: 320px;font-size:13px;font-weight:bold'>Per&iacute;odo: ".format_date_out($dataInicio)." - ".format_date_out($dataFinal)."</td>";

        ##############################################################################################################################################################################################

        $limite = 42;
        
        $tabelaCabecalho =' 
                <tr style="border-bottom: 2px solid">
                    <td style="width: 600px"><h1>Relat&oacute;rio de Produtos</h1></td>
                    <td style="width: 200px"></td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 550px" name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
                            <tr>
                                <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                                <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                                <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                                <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                                '.$periodo.'
                            </tr>
                        </table>    
                    </td>
                    <td>        
                        <table class="dados_impressao">
                            <tr>
                                <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                                <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                                <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">';
                        
?>     
 
        <table class="relatorio_print" style="page-break-before:avoid">

<?      print $tabelaCabecalho;
        $tabelaCabecalho = ' <table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho;

        $sqlFuncionarios = mysql_query("SELECT * FROM tblproduto_comissao 
                                        LEFT JOIN tblfuncionario ON tblfuncionario.fldId = tblproduto_comissao.fldFuncionario_Id GROUP BY fldFuncionario_Id");

        $totalRegistroFunc = mysql_num_rows($sqlFuncionarios);
        $x = 0;
        $n = 0;

        while($rowFuncionarios = mysql_fetch_assoc($sqlFuncionarios)){

?>          <tr style="width: 810px;  background:#ccc">
                <td style="width:auto; margin-left: 5px; font-size:14px; font-weight:bold;"><?=$rowFuncionarios['fldNome'];?></td>
            </tr>

<?          $x += 1;
            $n += 1;
            $comissao_total_final = 0;

            /*


SELECT
 tblpedido_parcela_baixa.fldParcela_Id,
 tblpedido_parcela.fldVencimento,
 tblpedido_parcela.fldPedido_Id,
 tblpedido_parcela.fldId,
 tblpedido_item.fldProduto_Id,
 SUM((tblpedido_item.fldValor + tblpedido_item.fldDesconto)) AS fldTotalReceber_Item,
 SUM((tblpedido_item.fldValor * (tblpedido_parcela_baixa.fldValor + tblpedido_parcela_baixa.fldDesconto)) / tblpedido_parcela.fldValor) AS fldTotalRecebido_Item,
 IF(tblproduto_comissao.fldComissao_Tipo = '2',
 SUM( (tblproduto_comissao.fldComissao_Valor / 100) * ((tblpedido_item.fldValor * (tblpedido_parcela_baixa.fldValor + tblpedido_parcela_baixa.fldDesconto)) / tblpedido_parcela.fldValor) ),
 SUM(tblproduto_comissao.fldComissao_Valor)) AS fldComissao_Receber

FROM tblpedido_parcela

 LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId
 LEFT JOIN tblpedido_item ON tblpedido_item.fldPedido_Id = tblpedido_parcela.fldPedido_Id
 LEFT JOIN tblproduto_comissao ON tblproduto_comissao.fldProduto_Id = tblpedido_item.fldProduto_Id
 LEFT JOIN tblproduto ON tblproduto.fldId = tblpedido_item.fldProduto_Id

WHERE fldVencimento BETWEEN '2013-03-01' AND '2013-03-31'
GROUP BY tblpedido_item.fldProduto_Id

            */

            $qPsroduto_Comissao = mysql_query("SELECT

                                             tblpedido_parcela_baixa.fldParcela_Id,
                                             tblpedido_parcela_baixa.fldDataRecebido,

                                             SUM((tblpedido_item.fldValor + tblpedido_item.fldDesconto)) AS fldTotalReceber_Item,
                                             SUM((tblpedido_item.fldValor * (tblpedido_parcela_baixa.fldValor + tblpedido_parcela_baixa.fldDesconto)) / tblpedido_parcela.fldValor) AS fldTotalRecebido_Item,
                                             IF(tblproduto_comissao.fldComissao_Tipo = '2',
                                             SUM( (tblproduto_comissao.fldComissao_Valor / 100) * ((tblpedido_item.fldValor * (tblpedido_parcela_baixa.fldValor + tblpedido_parcela_baixa.fldDesconto)) / tblpedido_parcela.fldValor) ),
                                             SUM(tblproduto_comissao.fldComissao_Valor)) AS fldComissao_Receber,

                                             tblpedido_parcela.fldPedido_Id,
                                             tblpedido_item.fldProduto_Id,
                                             tblproduto_comissao.fldFuncionario_Id,
                                             tblproduto_comissao.fldComissao_Valor,
                                             tblproduto_comissao.fldComissao_Tipo,
                                             tblproduto.fldNome,
                                             tblproduto.fldValorVenda

                                            FROM tblpedido_parcela_baixa

                                             LEFT JOIN tblpedido_parcela ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
                                             LEFT JOIN tblpedido_item ON tblpedido_item.fldPedido_Id = tblpedido_parcela.fldPedido_Id
                                             LEFT JOIN tblproduto_comissao ON tblproduto_comissao.fldProduto_Id = tblpedido_item.fldProduto_Id
                                             LEFT JOIN tblproduto ON tblproduto.fldId = tblpedido_item.fldProduto_Id

                                            WHERE tblpedido_parcela_baixa.fldExcluido != 1 
                                            AND tblproduto_comissao.fldFuncionario_Id != 'null'
                                            AND tblpedido_parcela.fldVencimento BETWEEN '$dataInicio' AND '$dataFinal'
                                            AND tblproduto_comissao.fldFuncionario_Id = ".$rowFuncionarios['fldId']."

                                            GROUP BY tblproduto_comissao.fldFuncionario_Id, tblpedido_item.fldProduto_Id");

            $totalRegistroProd = mysql_num_rows($qProduto_Comissao);

            $xn = 0;

            if($totalRegistroProd != 0){ ?>

                <tr style="width: 810px;">
                    <td>
                        <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio" style="border:0">
                            <tr style="border:none">
                                <td class="valor" style="width:350px"></td>
                                <td class="valor" style="width:100px;">Total Recebido</td>
                                <td class="valor" style="width:100px">Total Previsto</td>
                                <td class="valor" style="width:100px;">Comissão Unit.</td>
                                <td class="valor" style="width:100px">Total Comissão</td>
                            </tr>
                        </table>
                    </td>
                </tr>

<?              $n += 1;

                while($rowComissao = mysql_fetch_assoc($qProduto_Comissao)){

                    if($rowComissao['fldComissao_Tipo'] == '2'){
                        $comissao_unit = ($rowComissao['fldComissao_Valor'] / 100) * $rowComissao['fldValorVenda'];
                    }else{
                        $comissao_unit = $rowComissao['fldComissao_Valor'];
                    }

                    $total_comissao = $rowComissao['fldComissao_Receber'];

?>                  <tr style="border:none">
                        <td style="width:345px; margin-left: 5px;"><?=$rowComissao['fldNome'];?></td>
                        <td class="valor" style="width:100px;"><?=format_number_out($rowComissao['fldTotalReceber_Item']);?></td>
                        <td class="valor" style="width:100px"><?=format_number_out($rowComissao['fldTotalRecebido_Item']);?></td>
                        <td class="valor" style="width:100px;"><?=format_number_out($comissao_unit);?></td>
                        <td class="valor" style="width:100px"><b><?=format_number_out($total_comissao)?></b></td>
                    </tr>

<?                  $xn += 1;
                    $n += 1;
                    $comissao_total_final += $total_comissao;

                    if(($n == $limite)){
?>                              </table>
                                </td>
                            </tr>
                        </table>
<?                      $n = 1;
                        if($xn < $totalRegistroProd){
                             print $tabelaCabecalho;
                        }
                    }

                } ?>


                <tr style="border:none">
                    <td style="width:625px"></td>
                    <td style="width:90px; margin-left: 5px;"><i>Total a Pagar:</i></td>
                    <td class="valor" style="width:50px"><b><?=format_number_out($comissao_total_final)?></b></td>
                </tr>


<?              $n += 1;
                if(($n == $limite)){
?>                              </table>
                            </td>
                        </tr>
                    </table>
<?                      $n = 1;
                    if($xn < $totalRegistroProd){
                         print $tabelaCabecalho;
                    }
                }

                if(($x == $totalRegistroFunc)){
?>                              </table>
                            </td>
                        </tr>
                    </table>
<?                      $n = 1;
                    if($x < $totalRegistroProd){
                         print $tabelaCabecalho;
                    }
                }
            }
        }
?>
	</body>
</html>