<?
	require("financeiro_contas_pagar_programada_filtro.php");
	
	//a��es em grupo
	if(isset($_POST["hid_action"])){
		require("financeiro_contas_pagar_programada_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}

	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "excluido"){
?>		
        <div class="alert">
			<p class="ok">Registro exclu&iacute;do!<p>
        </div>
<?	}

/**************************** ORDER BY *******************************************/
	$filtroOrder = 'fldNome';
	$class 		  = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_contas_programada']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "fldId";  												break;
			case 'nome'			:  $filtroOrder = "fldNome"; 												break;
			case 'marcador'		:  $filtroOrder = "tblfinanceiro_conta_fluxo_marcador.fldMarcador"; 		break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_contas_programada'] = (!$_SESSION['order_contas_programada'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_contas_programada'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=financeiro_contas_pagar&amp;modo=programada$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_contas_programada']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class

/**************************** PAGINA��O *******************************************/
	$sSQL = "SELECT tblfinanceiro_conta_pagar_programada.*, 
	tblsistema_calendario_intervalo.fldNome as IntervaloNome,
	tblfinanceiro_conta_fluxo_marcador.fldMarcador as MarcadorNome,
	tblpagamento_tipo.fldTipo as TipoPagamento
	FROM tblfinanceiro_conta_pagar_programada,
	tblsistema_calendario_intervalo, 
	tblfinanceiro_conta_fluxo_marcador, tblpagamento_tipo
	WHERE tblfinanceiro_conta_pagar_programada.fldIntervalo_Tipo = tblsistema_calendario_intervalo.fldId 
	AND tblfinanceiro_conta_pagar_programada.fldExcluido = '0' 
	AND tblfinanceiro_conta_fluxo_marcador.fldId = tblfinanceiro_conta_pagar_programada.fldMarcador 
	AND tblfinanceiro_conta_pagar_programada.fldPagamento_Id = tblpagamento_tipo.fldId " . $_SESSION["filtro_contas_programada"] . "
	ORDER BY " . $_SESSION['order_contas_programada'];
	
	$rsTotal = mysql_query($sSQL);
	$rowsTotal = mysql_num_rows($rsTotal);
	echo mysql_error();
	
	//defini��o dos limites
	$limite = 50;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " LIMIT " . $inicio . "," . $limite;
	$rsContaProgramada = mysql_query($sSQL);
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#########################################################################################
?>
    <form class="table_form" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li style="width:10px">&nbsp;</li>
                    <li class="order" style="text-align:center;">
                    	<a <?= ($filtroOrder == 'fldId') 											? "class='$class'" : '' ?> style="text-align:center; width:40px;" href="<?=$raiz?>codigo">C&oacute;d.</a>
                    </li>
                    <li class="order" style="width:290px">
                    	<a <?= ($filtroOrder == 'fldNome') 											? "class='$class'" : '' ?> style="width:190px" href="<?=$raiz?>nome">Nome</a>
                    </li>
					<li class="order" style="width:140px;">
						<a <?= ($filtroOrder == 'tblfinanceiro_conta_fluxo_marcador.fldMarcador') 	? "class='$class'" : '' ?> style="width:60px" href="<?=$raiz?>marcador">Marcador</a>
					</li>
					<li style="width:100px;">Pagamento</li>
                    <li style="width:83px;">Frequ&ecirc;ncia</li>
					<li style="width:60px; text-align: center;">Intervalo</li>
					<li style="width:120px; text-align: center;">Valor Aproximado</li>
                    <li style="width:29px">&nbsp;</li>
                    <li style="width:25px;"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de contas programadas">
<?					
					$id_array = array();
					$n = 0;
					
					$linha = "row";
					$rows = mysql_num_rows($rsContaProgramada);
					while ($rowContaProgramada = mysql_fetch_array($rsContaProgramada)){
						
						$id_array[$n] = $rowContaProgramada["fldId"];
						$n += 1;
				
?>						<tr class="<?= $linha; ?>">
<?				 	 		$icon 	= ($rowContaProgramada["fldDisabled"] ? "bg_disable" : "bg_enable");
							$titulo = ($rowContaProgramada["fldDisabled"] ? "desabilitado" : "habilitado");
?>							<td style="width:20px;"><img src="image/layout/<?=$icon?>.gif" alt="status" title="<?=$titulo?>" /></td>
							<td class="cod" style="width:40px;"><?=str_pad($rowContaProgramada['fldId'], 4, "0", STR_PAD_LEFT)?></td>
                            <td style="width:305px;"><?=$rowContaProgramada['fldNome']?></td>
							<td style="width:150px;"><?= $rowContaProgramada['MarcadorNome'] ?></td>
							<td style="width:100px;"><?= $rowContaProgramada['TipoPagamento'] ?></td>
							<td style="width:78px;"><?= $rowContaProgramada['IntervaloNome'] ?></td>
							<td style="width:60px; text-align: center;"><?= $rowContaProgramada['fldIntervalo_Frequencia'] ?></td>
							<td style="width:120px; text-align: center;"><?=format_number_out($rowContaProgramada['fldValor'])?></td>
							<td style="width:auto;"><a class="edit" href="index.php?p=financeiro_contas_pagar_programada_detalhe&amp;id=<?=$rowContaProgramada['fldId']?>"></a></td>
                            <td style="width:16px;"><input type="checkbox" name="chk_contas_programada_<?=$rowContaProgramada['fldId']?>" id="chk_contas_programada_<?=$rowContaProgramada['fldId']?>" title="selecionar o registro posicionado" /></td>
                        </tr>

<?                  	$linha = ($linha == "row" ?	$linha = "dif-row": $linha = "row");
					}
?>		 		</table>
            </div>
			
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="index.php?p=financeiro_contas_pagar_programada_novo">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" value="excluir" title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
					<li><input type="submit" name="btn_action" id="btn_habilitar" value="habilitar" title="Habilitar registro(s) selecionado(s)" /></li>
                    <li><input type="submit" name="btn_action" id="btn_desabilitar" value="desabilitar" title="Desabilitar registro(s) selecionado(s)" /></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=financeiro_contas_pagar_programada";
				include("paginacao.php")
?>		
            </div>
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>    
        </div>
	</form>